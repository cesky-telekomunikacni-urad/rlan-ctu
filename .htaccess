<IfModule mod_rewrite.c>
    RewriteEngine on

    # Is main domain without www (example.com => www.example.com)
    RewriteCond %{HTTP_HOST} !^localhost [NC]
    RewriteCond %{HTTP_HOST} !^www\. [NC]
    RewriteCond %{HTTP_HOST} !^[^.]+\.[^.]+\..*$ [NC]
    RewriteCond %{HTTP_HOST} ^(.*)$
    RewriteRule ^(.*)$ https://www.%1/$1 [R=301,L]

    # Is subdomain with www (www.test.example.com => test.example.com)
    RewriteCond %{HTTP_HOST} !^localhost [NC]
    RewriteCond %{HTTP_HOST} ^www\.([^.]+\.[^.]+\..*)$ [NC]
    RewriteRule ^(.*)$ https://%1/$1 [R=301,L]

    # Force https
    RewriteCond %{HTTP_HOST} !^localhost [NC]
    RewriteCond %{HTTPS} !=on
    RewriteRule ^(.*)$ https://%{HTTP_HOST}/$1 [R=301,L]

    RewriteCond %{REQUEST_URI} ^/admin/(assets|img|css|fonts|js)/
    RewriteRule ^admin/assets/(.*)$ /admin/web/assets/$1 [L]
    RewriteRule ^admin/css/(.*)$ /admin/web/source_assets/css/$1 [L]
    RewriteRule ^admin/fonts/(.*)$ /admin/web/source_assets/fonts/$1 [L]
    RewriteRule ^admin/js/(.*)$ /admin/web/source_assets/js/$1 [L]
    RewriteRule ^admin/img/(.*)$ /admin/web/source_assets/img/$1 [L]

    RewriteCond %{REQUEST_URI} ^/admin/
    RewriteCond %{REQUEST_URI} !^/admin/web/(assets|source_assets)
    RewriteRule ^.*$ admin/web/index.php [L]

    RewriteCond %{REQUEST_URI} ^/cdn/
    RewriteRule ^.*$ cdn/web/index.php [L]

    RewriteCond %{REQUEST_URI} ^/api/(assets|img|css|fonts|js)/
    RewriteRule ^api/assets/(.*)$ /api/web/assets/$1 [L]
    RewriteRule ^api/css/(.*)$ /api/web/source_assets/css/$1 [L]
    RewriteRule ^api/fonts/(.*)$ /api/web/source_assets/fonts/$1 [L]
    RewriteRule ^api/js/(.*)$ /api/web/source_assets/js/$1 [L]
    RewriteRule ^api/img/(.*)$ /api/web/source_assets/img/$1 [L]

    RewriteCond %{REQUEST_URI} ^/api/
    RewriteCond %{REQUEST_URI} !^/api/web/(assets|source_assets)
    RewriteRule ^.*$ api/web/index.php [L]

    RewriteCond %{REQUEST_URI} ^/(assets|css|js|img)/
    RewriteRule ^assets/(.*)$ /frontend/web/assets/$1 [L]
    RewriteRule ^css/(.*)$ /frontend/web/css/$1 [L]
    RewriteRule ^js/(.*)$ /frontend/web/js/$1 [L]
    RewriteRule ^img/(.*)$ /frontend/web/source_assets/img/$1 [L]

    RewriteCond %{REQUEST_URI} !^/admin/
    RewriteCond %{REQUEST_URI} !^/api/
    RewriteCond %{REQUEST_URI} !^/.well-known/

    RewriteCond %{REQUEST_FILENAME} !-f [OR]
    RewriteCond %{REQUEST_URI} ^/((.env)|(.git/(.*))|(.gitignore)|(.gitlab-ci.yml)|(CHANGELOG.md)|(codeception.yml)|(composer.json)|(composer.lock)|(docker-compose.yml)|(easy-coding-standard.yml)|(init)|(package.json)|(package-lock.json)|(README.md)|(yii))
    RewriteRule ^.*$ /frontend/web/index.php [L]
</IfModule>

<IfModule mod_expires.c>
    ExpiresActive on
    ExpiresDefault                                      "access plus 1 month"

  # CSS
    ExpiresByType text/css                              "access plus 1 year"

  # Data interchange
    ExpiresByType application/atom+xml                  "access plus 1 hour"
    ExpiresByType application/rdf+xml                   "access plus 1 hour"
    ExpiresByType application/rss+xml                   "access plus 1 hour"

    ExpiresByType application/json                      "access plus 0 seconds"
    ExpiresByType application/ld+json                   "access plus 0 seconds"
    ExpiresByType application/schema+json               "access plus 0 seconds"
    ExpiresByType application/vnd.geo+json              "access plus 0 seconds"
    ExpiresByType application/xml                       "access plus 0 seconds"
    ExpiresByType text/xml                              "access plus 0 seconds"

  # Favicon (cannot be renamed!) and cursor images
    ExpiresByType image/vnd.microsoft.icon              "access plus 1 week"
    ExpiresByType image/x-icon                          "access plus 1 week"

  # HTML
    ExpiresByType text/html                             "access plus 3600 seconds"

  # JavaScript
    ExpiresByType application/javascript                "access plus 1 year"
    ExpiresByType application/x-javascript              "access plus 1 year"
    ExpiresByType text/javascript                       "access plus 1 year"

  # Manifest files
    ExpiresByType application/manifest+json             "access plus 1 week"
    ExpiresByType application/x-web-app-manifest+json   "access plus 0 seconds"
    ExpiresByType text/cache-manifest                   "access plus 0 seconds"

  # Media files
    ExpiresByType audio/ogg                             "access plus 1 month"
    ExpiresByType image/bmp                             "access plus 1 month"
    ExpiresByType image/gif                             "access plus 1 month"
    ExpiresByType image/jpeg                            "access plus 1 month"
    ExpiresByType image/png                             "access plus 1 month"
    ExpiresByType image/svg+xml                         "access plus 1 month"
    ExpiresByType image/webp                            "access plus 1 month"
    ExpiresByType video/mp4                             "access plus 1 month"
    ExpiresByType video/ogg                             "access plus 1 month"
    ExpiresByType video/webm                            "access plus 1 month"

  # Web fonts

    # Embedded OpenType (EOT)
    ExpiresByType application/vnd.ms-fontobject         "access plus 1 month"
    ExpiresByType font/eot                              "access plus 1 month"

    # OpenType
    ExpiresByType font/opentype                         "access plus 1 month"

    # TrueType
    ExpiresByType application/x-font-ttf                "access plus 1 month"

    # Web Open Font Format (WOFF) 1.0
    ExpiresByType application/font-woff                 "access plus 1 month"
    ExpiresByType application/x-font-woff               "access plus 1 month"
    ExpiresByType font/woff                             "access plus 1 month"

    # Web Open Font Format (WOFF) 2.0
    ExpiresByType application/font-woff2                "access plus 1 month"

  # Other
    ExpiresByType text/x-cross-domain-policy            "access plus 1 week"
</IfModule>

<IfModule mod_deflate.c>
    # Insert filters / compress text, html, javascript, css, xml:
    AddOutputFilterByType DEFLATE text/plain
    AddOutputFilterByType DEFLATE text/html
    AddOutputFilterByType DEFLATE text/xml
    AddOutputFilterByType DEFLATE text/css
    AddOutputFilterByType DEFLATE text/vtt
    AddOutputFilterByType DEFLATE text/x-component
    AddOutputFilterByType DEFLATE application/xml
    AddOutputFilterByType DEFLATE application/xhtml+xml
    AddOutputFilterByType DEFLATE application/rss+xml
    AddOutputFilterByType DEFLATE application/js
    AddOutputFilterByType DEFLATE application/javascript
    AddOutputFilterByType DEFLATE application/x-javascript
    AddOutputFilterByType DEFLATE application/x-httpd-php
    AddOutputFilterByType DEFLATE application/x-httpd-fastphp
    AddOutputFilterByType DEFLATE application/atom+xml
    AddOutputFilterByType DEFLATE application/json
    AddOutputFilterByType DEFLATE application/ld+json
    AddOutputFilterByType DEFLATE application/vnd.ms-fontobject
    AddOutputFilterByType DEFLATE application/x-font-ttf
    AddOutputFilterByType DEFLATE application/font-woff2
    AddOutputFilterByType DEFLATE application/x-font-woff
    AddOutputFilterByType DEFLATE application/x-web-app-manifest+json font/woff
    AddOutputFilterByType DEFLATE font/woff
    AddOutputFilterByType DEFLATE font/opentype
    AddOutputFilterByType DEFLATE image/svg+xml
    AddOutputFilterByType DEFLATE image/x-icon

    # Exception: Images
    SetEnvIfNoCase REQUEST_URI \.(?:gif|jpg|jpeg|png|svg)$ no-gzip dont-vary

    # Drop problematic browsers
    BrowserMatch ^Mozilla/4 gzip-only-text/html
    BrowserMatch ^Mozilla/4\.0[678] no-gzip
    BrowserMatch \bMSI[E] !no-gzip !gzip-only-text/html

    # Make sure proxies don't deliver the wrong content
    Header append Vary User-Agent env=!dont-vary
</IfModule>

#Alternative caching using Apache's "mod_headers", if it's installed.
#Caching of common files - ENABLED
<IfModule mod_headers.c>
    <FilesMatch "\.(ico|pdf|flv|swf|js|css|gif|png|jpg|jpeg|txt)$">
        Header set Cache-Control "max-age=2592000, public"
    </FilesMatch>
</IfModule>

<IfModule mod_headers.c>
    <FilesMatch "\.(js|css|xml|gz)$">
        Header append Vary Accept-Encoding
    </FilesMatch>
</IfModule>

# Set Keep Alive Header
<ifModule mod_headers.c>
    Header always set Connection keep-alive
</ifModule>

# DISABLE pagespeed
<IfModule pagespeed_module>
    ModPagespeed off
</IfModule>

# If your server don't support ETags deactivate with "None" (and remove header)
<IfModule mod_expires.c>
    <IfModule mod_headers.c>
        Header unset ETag
    </IfModule>
    FileETag None
</IfModule>

<IfModule mod_headers.c>
    <FilesMatch ".(js|css|xml|gz|html|woff|woff2|ttf)$">
        Header append Vary: Accept-Encoding
    </FilesMatch>
</IfModule>

# ----------------------------------------------------------------------
# | 6g Firewall for Security - Do not change this part @Update 2018
# ----------------------------------------------------------------------

# 6G FIREWALL/BLACKLIST - Version 2018
# @ https://perishablepress.com/6g/

# 6G:[USER AGENTS]
<IfModule mod_setenvif.c>
	SetEnvIfNoCase User-Agent ([a-z0-9]{2000,}) bad_bot
	SetEnvIfNoCase User-Agent (archive.org|binlar|casper|checkpriv|choppy|clshttp|cmsworld|diavol|dotbot|extract|feedfinder|flicky|g00g1e|harvest|heritrix|httrack|kmccrew|loader|miner|nikto|nutch|planetwork|postrank|purebot|pycurl|seekerspider|siclab|skygrid|sqlmap|sucker|turnit|vikspider|winhttp|xxxyy|youda|zmeu|zune) bad_bot

	# Apache < 2.3
	<IfModule !mod_authz_core.c>
		Order Allow,Deny
		Allow from all
		Deny from env=bad_bot
	</IfModule>

	# Apache >= 2.3
	<IfModule mod_authz_core.c>
		<RequireAll>
			Require all Granted
			Require not env bad_bot
		</RequireAll>
	</IfModule>
</IfModule>

### @see https://scotthelme.co.uk/hardening-your-http-response-headers
### UPDATE 2018

## No-Referrer-Header
<IfModule mod_headers.c>
    Header always set Referrer-Policy "no-referrer"
</IfModule>

## X-FRAME-OPTIONS-Header
<IfModule mod_headers.c>
    Header always set X-Frame-Options "sameorigin"
</IfModule>

## X-XSS-PROTECTION-Header
<IfModule mod_headers.c>
    Header always set X-XSS-Protection "1; mode=block"
</IfModule>

## X-Content-Type-Options-Header
<IfModule mod_headers.c>
    Header always set X-Content-Type-Options "nosniff"
</IfModule>

## Strict-Transport-Security-Header - if you are using https on your website, comment this block out
<IfModule mod_headers.c>
   Header always set Strict-Transport-Security "max-age=31536000; includeSubDomains; preload"
</IfModule>