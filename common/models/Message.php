<?php
/*
* Copyright 2023 DactylGroup s.r.o.
*
* Licensed under the EUPL, Version 1.2 or – as soon they
will be approved by the European Commission - subsequent
versions of the EUPL (the "Licence");
* You may not use this work except in compliance with the
Licence.
* You may obtain a copy of the Licence at:
*
* https://joinup.ec.europa.eu/software/page/eupl5
*
* Unless required by applicable law or agreed to in
writing, software distributed under the Licence is
distributed on an "AS IS" basis,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
express or implied.
* See the Licence for the specific language governing
permissions and limitations under the Licence.
*/
?>


<?php

namespace common\models;

use dactylcore\core\helpers\Date;
use modules\user\common\models\User;
use Yii;

/**
 * @property int $id
 * @property int $id_author
 * @property int $id_thread
 * @property string $content
 * @property string $status
 * @property int $deleted
 * @property int $created_at
 * @property int $updated_at
 *
 * @property User $author
 * @property Thread $thread
 */
class Message extends \dactylcore\core\db\ActiveRecord
{
    const MESSAGE_STATUS_NEW = 'new';
    const MESSAGE_STATUS_READ = 'read';
    const FLAG_MY = 'my';
    const FLAG_THEIRS = 'theirs';

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return merge(parent::rules(), [
            [['id_author', 'id_thread', 'deleted', 'created_at', 'updated_at'], 'integer'],
            [['content'], 'string'],
            [['content'], 'required'],
            [['status'], 'string', 'max' => 255],
            [['status'], 'default', 'value' => self::MESSAGE_STATUS_NEW],
            [
                ['id_author'],
                'exist',
                'skipOnError' => true,
                'targetClass' => User::class,
                'targetAttribute' => ['id_author' => 'id'],
            ],
            [
                ['id_thread'],
                'exist',
                'skipOnError' => true,
                'targetClass' => Thread::class,
                'targetAttribute' => ['id_thread' => 'id'],
            ],
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return merge(parent::attributeLabels(), [
            'id' => _tF('id', 'messaging'),
            'id_author' => _tF('id_author', 'messaging'),
            'id_thread' => _tF('id_thread', 'messaging'),
            'content' => _tF('content', 'messaging'),
            'status' => _tF('status', 'messaging'),
            'deleted' => _tF('deleted', 'messaging'),
            'created_at' => _tF('created_at', 'messaging'),
            'updated_at' => _tF('updated_at', 'messaging'),
        ]);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAuthor()
    {
        return $this->hasOne(User::class, ['id' => 'id_author']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getThread()
    {
        return $this->hasOne(Thread::class, ['id' => 'id_thread']);
    }

    public function getStatus()
    {
        if ($this->status) {
            return $this->status;
        } else {
            return self::MESSAGE_STATUS_NEW;
        }
    }

    public function classShowAs()
    {
        $idUser = Yii::$app->getUser()->id;

        return $this->id_author == $idUser ? self::FLAG_MY : self::FLAG_THEIRS;
    }

    public function getFormatterDate()
    {
        return Yii::$app->formatter->asDatetime($this->created_at);
    }

    /**
     * Formats created_at attribute according to actual date:
     *  - if today, return only time
     *  - if this year, return month and day
     *  - full date otherwise
     *
     * @return string
     */
    public function getDateTimeFormatted()
    {
        $date = (new Date())->setTimestamp($this->created_at);

        $date->setTimezone(new \DateTimeZone('Europe/Prague'));

        if ($date->isToday()) {
            return $date->format('G:i');
        }

        if ($date->isThisYear()) {
            return $date->format('F d');
        }
        return $date->format('F d, Y');
    }

    public function isForMe()
    {
        return $this->id_author != Yii::$app->user->id;
    }

    public function markAsRead()
    {
        $this->status = self::MESSAGE_STATUS_READ;
        $this->save();
    }

    public function isRead()
    {
        return $this->status == self::MESSAGE_STATUS_READ;
    }

    /**
     * Checks if next message is from the same author and wit interval of 5minutes to omit displaying
     * timestamp in chat window.
     *
     * @param $nextMessage Message
     *
     * @return bool - if true show timestamp, otherwise hide timestamp
     */
    public function showTimestamp($nextMessage)
    {
        $showTimestamp = true;

        $messageDate = (new Date())->setTimestamp($this->created_at);
        $messageDateFormat = $messageDate->format('d m');
        $nextMessageDate = (new Date())->setTimestamp($nextMessage->created_at)->format('d m');

        if ($messageDateFormat == $nextMessageDate && $this->id_author == $nextMessage->id_author) {
            $timeDifference = $nextMessage->created_at - $this->created_at;
            if ($timeDifference < 300 || !$messageDate->isToday()) {
                $showTimestamp = false;
            }
        }

        return $showTimestamp;
    }

    /**
     * Decides whether to show that message has been read by opposite user according to next message.
     *
     * @param $nextMessage Message
     *
     * @return bool
     */
    public function showIsRead($nextMessage)
    {
        $showIsRead = false;

        if ($this->isForMe()) {
            if (!$this->isRead()) {
                $this->markAsRead();
            }
        } else {
            if ($this->isRead() &&
                (($nextMessage && !$nextMessage->isForMe() && !$nextMessage->isRead()) ||
                    (is_null($nextMessage)))
            ) {
                $showIsRead = true;
            }
        }

        return $showIsRead;
    }
}
