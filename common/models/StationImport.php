<?php
/*
* Copyright 2023 DactylGroup s.r.o.
*
* Licensed under the EUPL, Version 1.2 or – as soon they
will be approved by the European Commission - subsequent
versions of the EUPL (the "Licence");
* You may not use this work except in compliance with the
Licence.
* You may obtain a copy of the Licence at:
*
* https://joinup.ec.europa.eu/software/page/eupl5
*
* Unless required by applicable law or agreed to in
writing, software distributed under the Licence is
distributed on an "AS IS" basis,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
express or implied.
* See the Licence for the specific language governing
permissions and limitations under the Licence.
*/
?>


<?php


namespace common\models;

use dactylcore\log\LogManager;
use frontend\controllers\StationController;
use PhpOffice\PhpSpreadsheet\IOFactory;
use PhpOffice\PhpSpreadsheet\Worksheet\Worksheet;
use yii\web\NotFoundHttpException;

class StationImport
{
    const RESULT_SUCCESS = 0;
    const RESULT_ERROR = -1;
    const RESULT_WARNING = 1;
    const RESULT_LIMIT = 2;
    const USER_LIMIT = 100;
    //
    protected $out = [];
    protected $userId;
    //
    static array $typeMapping
        = [
            'wifi_5_2' => StationController::TYPE_52,
            'wifi_5_8' => StationController::TYPE_58_AP,
            'fsptp' => StationController::TYPE_FS,
            'fs' => StationController::TYPE_FS,
            'wigig' => StationController::TYPE_WIGIG,
        ];
    //
    /******************************************
     * FUNCTIONS FOR USER IMPORT
     ******************************************/


    /**
     * User import from FE (by sheet file upload)
     *
     * @param $filePath - path to excel to import from
     *
     * @return array - results array
     * @throws NotFoundHttpException - user not logged
     * @throws \PhpOffice\PhpSpreadsheet\Exception
     */
    public function userImport($filePath): array
    {
        $reader = IOFactory::createReaderForFile($filePath);
        $reader->setReadDataOnly(true);
        $spreadsheet = $reader->load($filePath);
        $sheet = $spreadsheet->getActiveSheet();

        $user = loggedUser();
        if ($user === null) {
            throw new NotFoundHttpException();
        }

        $this->userId = $user->getId();
        $this->out = [];

        // Import Excel rows starts on row 3, because of headers
        $rowIndex = 3;

        /**
         * // Import Excel sheet columns mapping
         * $columns = [
         * 'A' => 'Docasne Uzivatelske ID (doplnil si uzivatel)',
         * 'B' => 'GPS LAT',
         * 'C' => 'GPS LNG',
         * 'E' => 'MAC',
         * 'F' => 'výr.číslo',
         * 'G' => 'Název Stanice',
         * 'H' => 'Typ stanice (flag fs, fsptp, wigig, wifi_5_2, wifi_5_8)',
         * 'I' => 'Hlavni smer vyzarovani [stupne]',
         * 'J' => 'Kmitocet f [MHz]',
         * 'K' => 'sirka kanalu [MHz]',
         * 'L' => 'EIRP [dBm]', // pro FS PtP se neuvadi
         * 'M' => 'Zisk anteny [dBi]', // pro WiGig se neuvadi
         * 'N' => 'vykon FS PtP [dB]', // (u WiGig se neuvadi)
         * 'P' => 'pozadovany pomer C/I [dB]',  // Jen u FS!
         * 'Q' => 'Nas novy sloupec s akci',
         * ];
         */

        $updateActionFunc = function ($sheet, $rowIndex, &$macStation)
        {
            $gps = $this->getGPS($sheet, $rowIndex);
            if (empty($gps)) {
                $this->out[$rowIndex] = ['status' => self::RESULT_ERROR, 'station' => 0];
                $this->out[$rowIndex]['error'][] = _tF('gps_error', 'user-import');
            } else {
                $type = $this->getType($sheet->getCell("H{$rowIndex}")->getValue());

                $query = Station::find()
                                ->andWhere(['lat' => $gps[0]])
                                ->andWhere(['lng' => $gps[1]])
                                ->andWhere(['id_user' => $this->userId])
                                ->andWhere(['deleted' => 0]);

                if ($query->count() == 1) {
                    $macStation = $query->one();
                    $this->importStation($sheet, $rowIndex, $type, $macStation);
                } elseif ($query->count() < 1) {
                    $this->out[$rowIndex] = ['status' => self::RESULT_ERROR, 'station' => 0];
                    $this->out[$rowIndex]['error'][] = _tF('not_existed_error', 'user-import');
                } else {
                    // Update one station by MAC if exists
                    if (in_array($type, [StationController::TYPE_52, StationController::TYPE_58_AP])) {
                        $macAddress = $sheet->getCell("E{$rowIndex}")->getValue();
                        $macStation = $query->andWhere([
                            'mac_address' => $macAddress,
                            'type' => $type,
                        ])->one();

                        if ($macStation !== null && $macAddress !== null) {
                            $this->importStation($sheet, $rowIndex, $type, $macStation);
                            return;
                        }
                    }

                    $this->out[$rowIndex] = ['status' => self::RESULT_ERROR, 'station' => 0];
                    $this->out[$rowIndex]['error'][] = _tF('not_unique_error', 'user-import');
                }
            }
        };

        while ($this->checkActiveRow($sheet, $rowIndex)) {
            $macStation = null;

            // Check action column
            $action = mb_strtolower($sheet->getCell("Q{$rowIndex}")->getValue());
            switch ($action) {
                case 'upravena':
                case 'uprava':
                    $updateActionFunc($sheet, $rowIndex, $macStation);
                    break;
                case 'nova':
                    $gps = $this->getGPS($sheet, $rowIndex);
                    if (empty($gps)) {
                        $this->out[$rowIndex] = ['status' => self::RESULT_ERROR, 'station' => 0];
                        $this->out[$rowIndex]['error'][] = _tF('gps_error', 'user-import');
                    } else {
                        $type = $this->getType($sheet->getCell("H{$rowIndex}")->getValue());
                        $result = $this->importStation($sheet, $rowIndex, $type);
                        if ($type == StationController::TYPE_FS && $result) {
                            $rowIndex++;
                        }
                    }
                    break;
                case 'smazat':
                    $updateActionFunc($sheet, $rowIndex, $macStation);

                    // if no error happened in update, proceed to delete
                    if (!(isset($this->out[$rowIndex]['error']) && count($this->out[$rowIndex]['error']) > 0)) {
                        if ($macStation) {
                            /* @var $macStation Station */
                            if (!$macStation->delete()) {
                                $this->out[$rowIndex] = ['status' => self::RESULT_ERROR, 'station' => 0];
                                $this->out[$rowIndex]['error'][] = _tF('delete_error_caused_by_failed_delete',
                                    'user-import');
                            } else {
                                $this->out[$rowIndex] = [
                                    'status' => self::RESULT_SUCCESS,
                                    'station' => $macStation->id,
                                    'delete' => true,
                                ];
                            }
                        }
                    } else {
                        $this->out[$rowIndex]['delete'] = true;
                    }

                    break;
                case '':
                    $this->out[$rowIndex] = ['status' => self::RESULT_WARNING, 'station' => 0];
                    break;
                default:
                    $this->out[$rowIndex] = ['status' => self::RESULT_ERROR, 'station' => 0];
                    $this->out[$rowIndex]['error'][] = _tF('unknown_action', 'user-import', ['action' => $action]);
            }

            $rowIndex++;
            // Import Excel stars on row 3 because of headers, that's why we need add 3 to the limit
            if ($rowIndex > self::USER_LIMIT + 3) {
                $this->out[$rowIndex]['status'] = self::RESULT_LIMIT;
                break;
            }

            LogManager::getInstance()->saveOperations(false);
        }
        return $this->out;
    }

    /**
     * @param $sheet
     * @param $rowIndex
     * @param $type
     *
     * @return int  - return status
     */
    protected function importStation($sheet, $rowIndex, $type, ?Station $station = null)
    {
        /**
         * When we need to change the type (5.8 to 5.2)
         * Only Published 5.8 type can be changed!
         */
        if (!is_null($station)
            && $station->type == StationController::TYPE_58_AP
            && mb_strtolower($type) == StationController::TYPE_52
            && $station->isPublished()) {

            if ($station->changeType58to52()) {
                $this->out[$rowIndex] = ['status' => self::RESULT_SUCCESS, 'station' => $station->id];
                return true;
            } else {
                $this->out[$rowIndex] = ['status' => self::RESULT_ERROR, 'station' => 0];
                $this->out[$rowIndex]['error'][] = _tF('type cannot be changed', 'station');
                return false;
            }
        }
        /** END type change */

        if ($station !== null && $type !== $station->type) {
            $this->out[$rowIndex] = ['status' => self::RESULT_ERROR, 'station' => 0];
            $this->out[$rowIndex]['error'][] = _tF('update_unmatched_types', 'user-import', [
                'excel-type' => mb_strtoupper($type),
                'station-type' => mb_strtoupper($station->type),
            ]);
            return false;
        }

        switch (mb_strtolower($type)) {
            case StationController::TYPE_FS:
                if ($station === null) {
                    return $this->createFs($sheet, $rowIndex);
                } else {
                    return $this->updateFs($sheet, $rowIndex, $station);
                }
            case StationController::TYPE_WIGIG:
                return $this->importWigig($sheet, $rowIndex, $station);
            case StationController::TYPE_52:
                if (hasAccessTo('app-frontend_station_add_52ghz')) {
                    return $this->import52($sheet, $rowIndex, $station);
                }
                break;
            case StationController::TYPE_58_AP:
            case StationController::TYPE_58_TG:
                if (hasAccessTo('app-frontend_station_add_access_point')) {
                    return $this->import58Ap($sheet, $rowIndex, $station);
                }
                break;
        }

        $this->out[$rowIndex] = ['status' => self::RESULT_ERROR, 'station' => 0];
        $this->out[$rowIndex]['error'][] = _tF('unknown_type', 'user-import', ['type' => $type]);

        return false;
    }

    protected function import52(Worksheet $sheet, int $rowIndex, ?Station $station = null): bool
    {
        $mappingMain = [
            'lat' => 'B',
            'lng' => 'C',
            'name' => 'G',
            'macAddress' => 'E',
        ];
        $mainData = $this->mapData($mappingMain, $sheet, $rowIndex);

        // Add station
        if ($station === null) {
            $modelStation = (new Station());
            $modelStation->status = Station::STATUS_DRAFT;
            $modelStation->hardware_identifier = Station::MAC_ADDRESS;
            $modelStationType = new Station52();
        } else {
            $modelStation = $station;
            $modelStationType = $modelStation->getStationTypeObject();
        }

        $modelStation->type = StationController::TYPE_52;
        $modelStation->id_user = $this->userId;
        $modelStation->setChecksum();
        $modelStationType->setChecksum();

        foreach ([StationController::STEP_1_NAME, StationController::STEP_2_LOCATION, StationController::STEP_3_PARAMS,]
                 as $step) {
            $modelStation->setScenario($step);
            if ($modelStation->load(['Station' => $mainData])) {
                if ($modelStation->isNewRecord) {
                    $modelStation->initRegistrations();
                }

                if ($modelStation->save()) {
                    $modelStationType->id_station = $modelStation->id;

                    if ($modelStationType->save()) {
                        if (!$modelStationType->hasErrors()) {
                            if ($step == StationController::STEP_3_PARAMS) {
                                $modelStation->moveToWaiting();
                            }
                            $step += 1;
                            $modelStation->storeStationProgress($step);
                        }
                    } else {
                        $this->out[$rowIndex] = ['status' => self::RESULT_ERROR, 'station' => 0];
                        $this->out[$rowIndex]['error'] = $modelStationType->getErrors();
                        if (isset($modelStation->id) && $station === null) {
                            Station::deleteStation($modelStation->id);
                        }
                        return false;
                    }
                } else {
                    $this->out[$rowIndex] = ['status' => self::RESULT_ERROR, 'station' => 0];
                    $this->out[$rowIndex]['error'] = $modelStation->getErrors();
                    if (isset($modelStation->id) && $station === null) {
                        Station::deleteStation($modelStation->id);
                    }
                    return false;
                }
            }
        }

        $this->out[$rowIndex] = ['status' => self::RESULT_SUCCESS, 'station' => $modelStation->id];
        return true;
    }

    /**
     * @param Worksheet $sheet
     * @param int $rowIndex
     * @param Station|null $station
     *
     * @return bool
     * @throws \yii\db\Exception
     */
    protected function import58Ap(Worksheet $sheet, int $rowIndex, ?Station $station = null): bool
    {
        $mappingMain = [
            'lat' => 'B',
            'lng' => 'C',
            'name' => 'G',
            'macAddress' => 'E',
        ];
        $mainData = $this->mapData($mappingMain, $sheet, $rowIndex);

        // Add station
        if ($station === null) {
            $modelStation = (new Station());
            $modelStation->status = Station::STATUS_DRAFT;
            $modelStation->hardware_identifier = Station::MAC_ADDRESS;
            $modelStationType = new Station58();
        } else {
            $modelStation = $station;
            $modelStationType = $modelStation->getStationTypeObject();
        }

        $modelStation->type = StationController::TYPE_58_AP;
        $modelStation->id_user = $this->userId;
        $modelStation->setChecksum();

        $modelStationType->is_ap = 1;
        $modelStationType->setChecksum();

        foreach ([StationController::STEP_1_NAME, StationController::STEP_2_LOCATION, StationController::STEP_3_PARAMS,]
                 as $step) {
            $modelStation->setScenario($step);
            if ($modelStation->load(['Station' => $mainData])) {
                if ($modelStation->isNewRecord) {
                    $modelStation->initRegistrations();
                }

                if ($modelStation->save()) {
                    $modelStationType->id_station = $modelStation->id;

                    if ($modelStationType->save()) {
                        if (!$modelStationType->hasErrors()) {
                            if ($step == StationController::STEP_3_PARAMS) {
                                $modelStation->moveToWaiting();
                            }
                            $step += 1;
                            $modelStation->storeStationProgress($step);
                        }
                    } else {
                        $this->out[$rowIndex] = ['status' => self::RESULT_ERROR, 'station' => 0];
                        $this->out[$rowIndex]['error'] = $modelStationType->getErrors();
                        if (isset($modelStation->id) && $station === null) {
                            Station::deleteStation($modelStation->id);
                        }
                        return false;
                    }
                } else {
                    $this->out[$rowIndex] = ['status' => self::RESULT_ERROR, 'station' => 0];
                    $this->out[$rowIndex]['error'] = $modelStation->getErrors();
                    if (isset($modelStation->id) && $station === null) {
                        Station::deleteStation($modelStation->id);
                    }
                    return false;
                }
            }
        }

        $this->out[$rowIndex] = ['status' => self::RESULT_SUCCESS, 'station' => $modelStation->id];
        return true;
    }

    /**
     * @param Worksheet $sheet
     * @param int $rowIndex
     * @param Station|null $station
     *
     * @return int
     * @throws \yii\db\Exception
     */
    protected function importWigig(Worksheet $sheet, int $rowIndex, ?Station $station = null): int
    {

        $mappingMain = [
            'lat' => 'B',
            'lng' => 'C',
            'mac_address' => 'E',
            'macAddress' => 'E',
            'serial_number' => 'F',
            'serialNumber' => 'F',
            'name' => 'G',
            'channel_width' => 'K',
            'antenna_volume' => 'M',
            'power' => 'N',
        ];

        $mappingMainType = [
            'direction' => 'I',
            'eirp' => 'L',
        ];


        $mainData = $this->mapData($mappingMain, $sheet, $rowIndex);
        $typeData = $this->mapData($mappingMainType, $sheet, $rowIndex);

        // Default direction
        if ($typeData['direction'] == null) {
            $typeData['direction'] = 0;
        }

        // Add Station
        if ($station !== null) {
            $mainStation = $station;
            $mainTypeStation = $mainStation->getStationTypeObject();
        } else {
            $mainStation = (new Station());
            $mainTypeStation = (new StationWigig());
        }

        // EIRP data set
        $typeData['eirp_method'] = StationWigig::EIRP_METHOD_MANUAL;
        if ($mainData['antenna_volume'] && $mainData['power']) {
            $typeData['eirp'] = $mainData['antenna_volume'] + $mainData['power'];
            $typeData['eirp_method'] = StationWigig::EIRP_METHOD_AUTO;
        }

        // Round the direction
        $typeData['direction'] = round($typeData['direction']);

        $mainStation->status = Station::STATUS_DRAFT;
        $mainStation->hardware_identifier = Station::MAC_ADDRESS;
        $mainStation->setChecksum();
        $mainTypeStation->setChecksum();

        // Load methods from controller
        foreach ([StationController::STEP_1_NAME, StationController::STEP_2_LOCATION, StationController::STEP_3_PARAMS,]
                 as $step) {
            $mainStation->setScenario($step);
            $mainTypeStation->setScenario($step);
            $mainStation->type = StationController::TYPE_WIGIG;
            $mainStation->setUserId($this->userId);

            if ($mainStation->load(['Station' => $mainData])) {
                if ($mainStation->isNewRecord) {
                    $mainStation->initRegistrations();
                }
                $mainStation->typeStation = $mainTypeStation;

                if ($step == StationController::STEP_3_PARAMS && empty($mainStation->mac_address)) {
                    $mainStation->hardware_identifier = Station::SERIAL_NUMBER;
                }

                if ($mainStation->save()) {
                    $mainTypeStation->id_station = $mainStation->id;

                    if ($mainTypeStation->load(['StationWigig' => $typeData])) {
                        $mainTypeStation->setPtmp($mainStation->antenna_volume);
                        if ($mainTypeStation->getScenario() == StationController::STEP_2_LOCATION) {
                            $mainTypeStation->setAngleDirections();
                        }
                        if ($mainTypeStation->getScenario() == StationController::STEP_3_PARAMS) {
                            if ($mainTypeStation->eirp_method == StationWigig::EIRP_METHOD_MANUAL) {
                                $mainStation->power = null;
                                $mainStation->antenna_volume = null;
                                $mainStation->save();
                            } elseif ($mainStation->antenna_volume != null && $mainStation->power != null) {
                                $mainTypeStation->eirp = $mainStation->power + $mainStation->antenna_volume;
                            } else {
                                $mainTypeStation->eirp = null;
                            }
                        }
                        if (!$mainTypeStation->save()) {
                            $this->out[$rowIndex] = ['status' => self::RESULT_ERROR, 'station' => 0];
                            $this->out[$rowIndex]['error'] = $mainTypeStation->getErrors();
                            if ($station === null) {
                                Station::deleteStation($mainStation->id);
                            }
                            return false;
                        }
                    } else {
                        $this->out[$rowIndex] = ['status' => self::RESULT_ERROR, 'station' => 0];
                        $this->out[$rowIndex]['error'][] = _tf('load_error', 'user-import');
                        if (isset($mainStation->id) && $station === null) {
                            Station::deleteStation($mainStation->id);
                        }
                        return false;
                    }
                } else {
                    $this->out[$rowIndex] = ['status' => self::RESULT_ERROR, 'station' => 0];
                    $this->out[$rowIndex]['error'] = $mainStation->getErrors();
                    if (isset($mainStation->id) && $station === null) {
                        Station::deleteStation($mainStation->id);
                    }
                    return false;
                }
            } else {
                $this->out[$rowIndex] = ['status' => self::RESULT_ERROR, 'station' => 0];
                $this->out[$rowIndex]['error'] = $mainStation->getErrors();
                if (isset($mainStation->id) && $station === null) {
                    Station::deleteStation($mainStation->id);
                }
                return false;
            }
        }

        $mainStation->storeStationProgress(StationController::STEP_4_SUMMARY);
        $mainStation->moveToWaiting();
        $this->out[$rowIndex] = ['status' => self::RESULT_SUCCESS, 'station' => $mainStation->id];
        return true;
    }

    /**
     * Updates only one station of pair
     *
     * @param Worksheet $sheet
     * @param int $rowIndex
     * @param Station $station
     *
     * @return bool
     * @throws \PhpOffice\PhpSpreadsheet\Exception
     * @throws \yii\db\Exception
     */
    protected function updateFs(Worksheet $sheet, int $rowIndex, Station $station): bool
    {
        $mainStation = $station;
        $mainStationType = $mainStation->getStationTypeObject();
        $mainStation->pointerToPairStation = $mainStation->getPairStation();

        $mapping = [
            'lat' => 'B',
            'lng' => 'C',
            'mac_address' => 'E',
            'macAddress' => 'E',
            'serial_number' => 'F',
            'serialNumber' => 'F',
            'name' => 'G',
            'channel_width' => 'K',
            'antenna_volume' => 'M',
            'power' => 'N',
        ];

        $data = $this->mapData($mapping, $sheet, $rowIndex);

        // Set serial, if MAC is not filled
        if (!$data['mac_address']) {
            $mainStation->hardware_identifier = Station::SERIAL_NUMBER;
        }

        $data = [
            Station::FORM_NAME => $data,
        ];

        $dataFs = [
            StationFs::FORM_NAME => [
                'frequency' => $sheet->getCell("J{$rowIndex}")->getValue(),
                'ratio_signal_interference' => $this->getClosestCI($sheet->getCell("P{$rowIndex}")->getValue()),
            ],
        ];

        // User
        $mainStation->id_user = $this->userId;

        foreach ([StationController::STEP_1_NAME, StationController::STEP_2_LOCATION, StationController::STEP_3_PARAMS,]
                 as $step) {

            $mainStation->setScenario($step);
            if (!$mainStation->load($data) || !$mainStation->save()) {
                $this->out[$rowIndex] = ['status' => self::RESULT_ERROR, 'station' => 0];
                $this->out[$rowIndex]['error'] = $mainStation->getErrors();
                return false;
            }

            if ($step == StationController::STEP_3_PARAMS &&
                (!$mainStationType->load($dataFs) || !$mainStationType->save())) {
                $this->out[$rowIndex] = ['status' => self::RESULT_ERROR, 'station' => 0];
                $this->out[$rowIndex]['error'] = $mainStationType->getErrors();
                return true;
            }
        }

        $mainStation->moveToWaiting();
        $mainStation->storeStationProgress(StationController::STEP_4_SUMMARY);
        $this->out[$rowIndex] = ['status' => self::RESULT_SUCCESS, 'station' => $mainStation->id];
        return true;
    }

    /**
     * Creates whole new pair of FS's
     *
     * @param Worksheet $sheet
     * @param int $rowIndex
     *
     * @return bool
     * @throws \PhpOffice\PhpSpreadsheet\Exception
     * @throws \yii\db\Exception
     */
    protected function createFs(Worksheet $sheet, int $rowIndex): bool
    {
        $rowIndexA = $rowIndex;
        $rowIndexB = $rowIndex + 1;

        // Check second line (second FS)
        if (mb_strtolower($sheet->getCell("Q{$rowIndexB}")->getValue()) !== 'nova') {
            $this->out[$rowIndex] = ['status' => self::RESULT_ERROR, 'station' => 0];
            $this->out[$rowIndex]['error'][] = _tF('pair_action_error', 'user-import');
            return false;
        }

        if (empty($this->getGPS($sheet, $rowIndexB))) {
            $this->out[$rowIndex] = ['status' => self::RESULT_ERROR, 'station' => 0];
            $this->out[$rowIndex]['error'][] = _tF('pair_gps_error', 'user-import');
            return false;
        }

        if ($this->getType($sheet->getCell("H{$rowIndexB}")->getValue()) !== StationController::TYPE_FS) {
            $this->out[$rowIndex] = ['status' => self::RESULT_ERROR, 'station' => 0];
            $this->out[$rowIndex]['error'][] = _tF('pair_type_error', 'user-import');
            return false;
        }

        $fsPair = new StationFsPair();
        $fsPair->initNew();

        $mapping = [
            'lat' => 'B',
            'lng' => 'C',
            'mac_address' => 'E',
            'macAddress' => 'E',
            'serial_number' => 'F',
            'serialNumber' => 'F',
            'name' => 'G',
            'channel_width' => 'K',
            'antenna_volume' => 'M',
            'power' => 'N',
        ];

        $dataA = $this->mapData($mapping, $sheet, $rowIndexA);
        $dataB = $this->mapData($mapping, $sheet, $rowIndexB);

        // Set serial, if MAC is not filled
        if (!$dataA['mac_address']) {
            $fsPair->stationA->hardware_identifier = Station::SERIAL_NUMBER;
        }
        if (!$dataB['mac_address']) {
            $fsPair->stationB->hardware_identifier = Station::SERIAL_NUMBER;
        }


        $data = [
            Station::FORM_NAME => [
                Station::POSITION_A => $dataA,
                Station::POSITION_B => $dataB,
            ],
        ];

        $dataFs = [
            StationFs::FORM_NAME => [
                Station::POSITION_A => [
                    'frequency' => $sheet->getCell("J{$rowIndexA}")->getValue(),
                    'ratio_signal_interference' => $this->getClosestCI($sheet->getCell("P{$rowIndexA}")
                                                                             ->getValue()),
                ],
                Station::POSITION_B => [
                    'frequency' => $sheet->getCell("J{$rowIndexB}")->getValue(),
                    'ratio_signal_interference' => $this->getClosestCI($sheet->getCell("P{$rowIndexB}")
                                                                             ->getValue()),
                ],
            ],
        ];

        // User
        $fsPair->setUser($this->userId);

        foreach ([StationController::STEP_1_NAME, StationController::STEP_2_LOCATION, StationController::STEP_3_PARAMS,]
                 as $step) {
            $fsPair->setStep($step);

            if (!$fsPair->setData($data) || !$fsPair->save()) {
                $this->out[$rowIndexA] = ['status' => self::RESULT_ERROR, 'station' => 0];
                $this->out[$rowIndexB] = ['status' => self::RESULT_ERROR, 'station' => 0];
                $this->out[$rowIndexA]['error'] = ($fsPair->stationA->hasErrors()) ? $fsPair->stationA->getErrors() :
                    [_tF('pair_station_errors', 'user-import')];
                $this->out[$rowIndexB]['error'] = ($fsPair->stationB->hasErrors()) ? $fsPair->stationB->getErrors() :
                    [_tF('pair_station_errors', 'user-import')];
                if ($step > StationController::STEP_1_NAME) {
                    Station::deleteStation($fsPair->stationA->id);
                }
                return true;
            }

            if ($step == StationController::STEP_3_PARAMS &&
                (!$fsPair->setFsData($dataFs, $step) || !$fsPair->FsSave())) {
                $this->out[$rowIndexA] = ['status' => self::RESULT_ERROR, 'station' => 0];
                $this->out[$rowIndexB] = ['status' => self::RESULT_ERROR, 'station' => 0];
                $this->out[$rowIndexA]['error'] = ($fsPair->stationA->getStationTypeObject()->hasErrors()) ?
                    $fsPair->stationA->getStationTypeObject()->getErrors() :
                    [_tF('pair_station_errors', 'user-import')];
                $this->out[$rowIndexB]['error'] = ($fsPair->stationB->getStationTypeObject()->hasErrors()) ?
                    $fsPair->stationB->getStationTypeObject()->getErrors() :
                    [_tF('pair_station_errors', 'user-import')];
                Station::deleteStation($fsPair->stationA->id);
                return true;
            }
        }

        $fsPair->stationA->moveToWaiting();
        $fsPair->stationA->storeStationProgress(StationController::STEP_4_SUMMARY);
        $fsPair->stationB->storeStationProgress(StationController::STEP_4_SUMMARY);
        $this->out[$rowIndexA] = ['status' => self::RESULT_SUCCESS, 'station' => $fsPair->stationA->id];
        $this->out[$rowIndexB] = ['status' => self::RESULT_SUCCESS, 'station' => $fsPair->stationB->id];
        return true;
    }

    /**
     * @param $ciOrig
     *
     * @return mixed|null
     */
    protected function getClosestCI($ciOrig)
    {
        $ciValues = array_keys(StationFs::$qamValues);
        $ci = min($ciValues);

        try {
            if ((int)($ciOrig)) {
                $closest = null;
                foreach ($ciValues as $ciValue) {
                    if ($closest === null || abs($ciOrig - $closest) > abs($ciValue - $ciOrig)) {
                        $closest = $ciValue;
                    }
                }
                $ci = $closest;
            }
            return $ci;
        } catch (\Exception $e) {
            // If number evaluation collapse, use minimal value
            return min($ciValues);
        }
    }

    /**
     * Check if some mandatory columns are filled. If not, row is empty (and empty row == EOF)
     *
     * @param $sheet
     * @param $rowIndex
     *
     * @return bool
     */
    protected function checkActiveRow($sheet, $rowIndex)
    {
        $allNextRowsAreEmpty = true;
        for ($i = 0; $i <= 2; $i++) {
            $tmpIndex = $rowIndex + $i;
            $rowIsEmpty = !(
                $sheet->getCell("B{$tmpIndex}")->getValue() ||
                $sheet->getCell("C{$tmpIndex}")->getValue() ||
                $sheet->getCell("H{$tmpIndex}")->getValue() ||
                $sheet->getCell("Q{$tmpIndex}")->getValue()
            );

            $allNextRowsAreEmpty &= $rowIsEmpty;
        }

        return !$allNextRowsAreEmpty;
    }

    /**
     * Map data from row (sheet) to structure
     *
     * @param $mapping
     * @param $sheet
     * @param $rowIndex
     *
     * @return array
     */
    protected function mapData($mapping, $sheet, $rowIndex)
    {
        $out = [];
        foreach ($mapping as $attribute => $column) {
            // Have to remove spaces on the end of numbers
            $value = trim($sheet->getCell("{$column}{$rowIndex}")->getValue());

            $out[$attribute] = $value;

            if ($attribute == 'direction') {
                if ($out[$attribute] == 360) {
                    $out[$attribute] = 0;
                }
            }
        }
        return $out;
    }

    /**
     * Count decimal GPS from columns
     *
     * @param $sheet
     * @param $rowIndex
     *
     * @return array
     */
    protected function getGPS($sheet, $rowIndex)
    {
        $lat = $sheet->getCell("B{$rowIndex}")->getValue();
        $lng = $sheet->getCell("C{$rowIndex}")->getValue();

        if (!$lat || !$lng) {
            return [];
        }

        return [$lat, $lng];
    }

    /**
     * Translate type from sheet
     *
     * @param $type
     *
     * @return string|null
     */
    protected function getType($type)
    {
        if (!array_key_exists(mb_strtolower($type), self::$typeMapping)) {
            return $type;
        }
        return self::$typeMapping[mb_strtolower($type)];
    }
}