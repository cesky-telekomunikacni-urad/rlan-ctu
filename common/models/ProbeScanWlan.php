<?php
/*
* Copyright 2023 DactylGroup s.r.o.
*
* Licensed under the EUPL, Version 1.2 or – as soon they
will be approved by the European Commission - subsequent
versions of the EUPL (the "Licence");
* You may not use this work except in compliance with the
Licence.
* You may obtain a copy of the Licence at:
*
* https://joinup.ec.europa.eu/software/page/eupl5
*
* Unless required by applicable law or agreed to in
writing, software distributed under the Licence is
distributed on an "AS IS" basis,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
express or implied.
* See the Licence for the specific language governing
permissions and limitations under the Licence.
*/
?>


<?php

namespace common\models;

use yii\behaviors\TimestampBehavior;
use yii\db\ActiveRecord;

/**
 * This is the model class for table "probe_scan_wlan".
 *
 * @property integer $id
 * @property integer $id_probe_scan
 * @property string $bssid
 * @property double $rssi
 * @property string $ssid
 * @property string $capabilities
 * @property integer $frequency
 * @property integer $created_at
 * @property integer $updated_at
 *
 * @property ProbeScan $probeScan
 */
class ProbeScanWlan extends \dactylcore\core\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName(): string
    {
        return 'probe_scan_wlan';
    }

    /**
     * @inheritdoc
     */
    public function rules(): array
    {
        return [
            [['id_probe_scan', 'bssid', 'rssi', 'capabilities', 'frequency'], 'required'],
            [['id_probe_scan', 'created_at', 'updated_at'], 'integer'],
            [['rssi', 'frequency'], 'number'],
            [['bssid', 'ssid', 'capabilities'], 'string', 'max' => 512],
            [['id_probe_scan'], 'exist', 'skipOnError' => true, 'targetClass' => ProbeScan::class, 'targetAttribute' => ['id_probe_scan' => 'id']],
            [['deleted'], 'default', 'value' => 0],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels(): array
    {
        return [
            'id' => _tF('id', 'probe'),
            'id_probe_scan' => _tF('probe_scan', 'probe'),
            'bssid' => _tF('bssid', 'probe'),
            'rssi' => _tF('rssi', 'probe'),
            'ssid' => _tF('ssid', 'probe'),
            'capabilities' => _tF('capabilities', 'probe'),
            'frequency' => _tF('frequency', 'probe'),
            'created_at' => _tF('created_at', 'probe'),
            'updated_at' => _tF('updated_at', 'probe'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getProbeScan()
    {
        return $this->hasOne(ProbeScan::class, ['id' => 'id_probe_scan']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getProbe()
    {
        return $this->hasOne(Probe::class, ['id' => 'id_probe'])->via('probeScan');
    }


    public function behaviors()
    {
        return [
            [
                'class' => TimestampBehavior::class,
                'attributes' => [
                    ActiveRecord::EVENT_BEFORE_INSERT => ['created_at', 'updated_at'],
                    ActiveRecord::EVENT_BEFORE_UPDATE => ['updated_at'],
                ],
            ],
        ];
    }

    public function getScanTime(){
        return $this->probeScan->time;
    }

    public function getScanAddress(){
        return $this->probeScan->address;
    }

    public function getProbename(){
        return $this->probeScan->probe->name;
    }
}
