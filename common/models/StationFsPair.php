<?php
/*
* Copyright 2023 DactylGroup s.r.o.
*
* Licensed under the EUPL, Version 1.2 or – as soon they
will be approved by the European Commission - subsequent
versions of the EUPL (the "Licence");
* You may not use this work except in compliance with the
Licence.
* You may obtain a copy of the Licence at:
*
* https://joinup.ec.europa.eu/software/page/eupl5
*
* Unless required by applicable law or agreed to in
writing, software distributed under the Licence is
distributed on an "AS IS" basis,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
express or implied.
* See the Licence for the specific language governing
permissions and limitations under the Licence.
*/
?>


<?php


namespace common\models;

use frontend\controllers\StationController;
use League\Geotools\Coordinate\Coordinate;
use League\Geotools\Geotools;
use League\Geotools\GeotoolsInterface;


class StationFsPair
{
    /**
     * Maximal distnace in KM which two FS stations can have
     * between themselves.
     *
     * @var int
     */
    const MAX_DISTANCE = 10;

    /**
     * @var Station
     */
    public $stationA = null;
    /**
     * @var Station
     */
    public $stationB = null;
    public $isNewRecord = false;
    protected $updatedAtCheck;
    /**
     * @var mixed|null
     */
    protected $updatedAtCheckType;

    public function initNew()
    {
        $this->stationA = new Station();
        $this->stationA->type = StationController::TYPE_FS;
        $this->stationA->status = Station::STATUS_DRAFT;
        $this->stationA->pair_position = Station::POSITION_A;
        $this->stationA->hardware_identifier = Station::MAC_ADDRESS;

        $this->stationB = new Station();
        $this->stationB->type = StationController::TYPE_FS;
        $this->stationB->status = Station::STATUS_DRAFT;
        $this->stationB->pair_position = Station::POSITION_B;
        $this->stationB->hardware_identifier = Station::MAC_ADDRESS;
    }

    public function findById($id)
    {
        $this->stationA = Station::findOne(['id' => $id]);
        $this->stationB = $this->stationA->getPairStation();
        $this->stationB->pointerToPairStation = $this->stationA;
        $this->stationA->pointerToPairStation = $this->stationB;

        $this->updatedAtCheck = $this->stationA->updated_at + $this->stationB->updated_at;
    }

    public function setUser($userId)
    {
        $this->stationA->setUserId($userId);
        $this->stationB->setUserId($userId);
    }

    public function setData($data)
    {
        $dataA = array_map('trim', $data[Station::FORM_NAME][Station::POSITION_A]);
        $dataB = array_map('trim', $data[Station::FORM_NAME][Station::POSITION_B]);

        $this->stationA->setAttributes($dataA);
        $this->stationB->setAttributes($dataB);

        if ($this->stationA->isNewRecord) {
            $this->isNewRecord = true;
            $this->stationA->initRegistrations();
        }
        if ($this->stationB->isNewRecord) {
            $this->stationB->initRegistrations();
        }

        $vA = $this->stationA->validate();
        $vB = $this->stationB->validate();

        // Check the pair distance
        if ($vA && $vB && $this->stationA->scenario == StationController::STEP_2_LOCATION) {
            $coordA = new Coordinate([
                $this->stationA->lat,
                $this->stationA->lng,
            ]);
            $coordB = new Coordinate([
                $this->stationB->lat,
                $this->stationB->lng,
            ]);
            $geotools = new Geotools();
            $distanceObj = $geotools->distance()
                                    ->setFrom($coordA)
                                    ->setTo($coordB)
                                    ->in(GeotoolsInterface::KILOMETER_UNIT);

            // Resolve difference between this and JS distance counting method
            $distance = ceil($distanceObj->flat() - 0.1);

            if ($distance > self::MAX_DISTANCE) {
                $this->stationB->addError('lat', _tF('distance_error', 'station', ['distance' => self::MAX_DISTANCE]));
                return false;
            }
        }

        return $vA && $vB;
    }

    public function save()
    {
        $this->stationA->pair_position = Station::POSITION_A;
        $this->stationB->pair_position = Station::POSITION_B;

        $a = $this->stationA->save();
        $this->stationB->id_station_pair = $this->stationA->id;

        $b = $this->stationB->save();
        $this->stationA->id_station_pair = $this->stationB->id;
        $a = $this->stationA->save();

        return $a && $b;
    }

    public function setFsData(array $data, $step): bool
    {
        if ($this->isNewRecord) {
            $this->isNewRecord = true;
            $fsA = new StationFs();
            $fsA->id_station = $this->stationA->id;
            $fsB = new StationFs();
            $fsB->id_station = $this->stationB->id;
        } else {
            $fsA = $this->stationA->getStationTypeObject();
            $fsB = $this->stationB->getStationTypeObject();
        }

        $fsA->setScenario($step);
        $fsB->setScenario($step);

        if (isset($data[StationFs::FORM_NAME])) {
            $fsA->setAttributes(array_map('trim', $data[StationFs::FORM_NAME][Station::POSITION_A]));
            $fsB->setAttributes(array_map('trim', $data[StationFs::FORM_NAME][Station::POSITION_B]));
        }

        $vA = $fsA->validate();
        $vB = $fsB->validate();
        if ($vA && $vB) {
            if ($fsB->frequency && $fsA->frequency) {
                StationFs::findAndSetClosestLattgas($fsA, $fsB);
            }
            $ret = true;
        } else {
            $ret = false;
        }

        $this->stationA->typeStation = $fsA;
        $this->stationB->typeStation = $fsB;
        $this->updatedAtCheckType = $fsA->updated_at + $fsB->updated_at;
        $this->setStep($step);
        return $ret;
    }

    public function FsSave(): bool
    {
        return $this->stationA->typeStation->save() && $this->stationB->typeStation->save();
    }

    public function hasTypeErrors(): bool
    {
        return $this->stationA->typeStation->hasErrors() || $this->stationB->typeStation->hasErrors();
    }

    public function hasStationErrors(): bool
    {
        return $this->stationA->hasErrors() || $this->stationB->hasErrors();
    }

    public function setStep($step)
    {
        $this->stationA->setScenario($step);
        if ($this->stationA->typeStation) {
            $this->stationA->typeStation->setScenario($step);
        }
        $this->stationB->setScenario($step);
        if ($this->stationB->typeStation) {
            $this->stationB->typeStation->setScenario($step);
        }
    }

    public function setNextScenarios()
    {
        $this->stationA->setNextScenario();
        if ($this->stationA->typeStation) {
            $this->stationA->typeStation->setNextScenario();
        }
        $this->stationB->setNextScenario();

        if ($this->stationB->typeStation) {
            $this->stationB->typeStation->setNextScenario();
        }
    }

    public function hasChanged()
    {
        $tmpUpdatedAtCheck = $this->stationA->updated_at + $this->stationB->updated_at;
        return $this->updatedAtCheck != $tmpUpdatedAtCheck ||
            (($this->stationA->typeStation && $this->stationB->typeStation) &&
                $this->updatedAtCheckType !=
                ($this->stationA->typeStation->updated_at + $this->stationB->typeStation->updated_at));
    }
}