<?php
/*
* Copyright 2023 DactylGroup s.r.o.
*
* Licensed under the EUPL, Version 1.2 or – as soon they
will be approved by the European Commission - subsequent
versions of the EUPL (the "Licence");
* You may not use this work except in compliance with the
Licence.
* You may obtain a copy of the Licence at:
*
* https://joinup.ec.europa.eu/software/page/eupl5
*
* Unless required by applicable law or agreed to in
writing, software distributed under the Licence is
distributed on an "AS IS" basis,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
express or implied.
* See the Licence for the specific language governing
permissions and limitations under the Licence.
*/
?>


<?php

namespace common\models;

use Yii;
use dactylcore\core\db\ActiveQuery;
use dactylcore\core\db\ActiveRecord;
use dactylcore\user\common\models\User;

/**
 * @property int $id
 * @property int $id_user_alliance
 * @property int $id_user
 * @property string $invitation_token
 * @property string $status
 * @property int $deleted
 * @property int $created_at
 * @property int $invited_at
 * @property int $updated_at
 *
 * @property User $user
 * @property UserAlliance $userAlliance
 */
class UserAllianceConnector extends ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return merge(parent::rules(), [
            [['id_user_alliance', 'id_user', 'status', 'invited_at'], 'required'],
            [['id_user_alliance', 'id_user', 'invited_at'], 'integer'],
            [['invitation_token'], 'string', 'max' => 255],
            [['status'], 'string', 'max' => 32],
            [
                ['id_user'],
                'exist',
                'skipOnError' => true,
                'targetClass' => User::class,
                'targetAttribute' => ['id_user' => 'id'],
            ],
            [
                ['id_user_alliance'],
                'exist',
                'skipOnError' => true,
                'targetClass' => UserAlliance::class,
                'targetAttribute' => ['id_user_alliance' => 'id'],
            ],
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return merge(parent::attributeLabels(), [
            'id' => _tF('id', 'user-alliance-connector'),
            'id_user_alliance' => _tF('id_user_alliance', 'user-alliance-connector'),
            'id_user' => _tF('id_user', 'user-alliance-connector'),
            'invitation_token' => _tF('invitation_token', 'user-alliance-connector'),
            'status' => _tF('status', 'user-alliance-connector'),
            'invited_at' => _tF('invited_at', 'user-alliance-connector'),
        ]);
    }

    /**
     * @return ActiveQuery
     */
    public function getUser()
    {
        return $this->hasOne(User::class, ['id' => 'id_user']);
    }

    /**
     * @return ActiveQuery
     */
    public function getUserAlliance()
    {
        return $this->hasOne(UserAlliance::class, ['id' => 'id_user_alliance']);
    }

    public function getLogModelName(): string
    {
        return $this->id;
    }

    public function getLogModelOperationData(): array
    {
        return ['name' => $this->getLogModelName()];
    }

}
