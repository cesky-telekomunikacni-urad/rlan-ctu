<?php
/*
* Copyright 2023 DactylGroup s.r.o.
*
* Licensed under the EUPL, Version 1.2 or – as soon they
will be approved by the European Commission - subsequent
versions of the EUPL (the "Licence");
* You may not use this work except in compliance with the
Licence.
* You may obtain a copy of the Licence at:
*
* https://joinup.ec.europa.eu/software/page/eupl5
*
* Unless required by applicable law or agreed to in
writing, software distributed under the Licence is
distributed on an "AS IS" basis,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
express or implied.
* See the Licence for the specific language governing
permissions and limitations under the Licence.
*/
?>


<?php


namespace common\models;

use dactylcore\core\data\ActiveDataProvider;
use frontend\controllers\StationController;
use PhpOffice\PhpSpreadsheet\IOFactory;
use PhpOffice\PhpSpreadsheet\Style\Alignment;
use PhpOffice\PhpSpreadsheet\Style\Fill;
use PhpOffice\PhpSpreadsheet\Style\NumberFormat;
use yii\db\Exception;
use yii2tech\spreadsheet\Column;
use yii2tech\spreadsheet\Spreadsheet;

class StationExport
{
    /**
     * @var int Starting row of the spreadsheet
     */
    public int $startRow = 2;

    /**
     * Array of columns for spreadsheet
     *
     * @return array
     */
    public static function getColumnsForExport()
    {

        return [
            [
                'attribute' => 'id',
                'label' => 'Docasne ID uzivatele (nepovinne)',
            ],
            [
                'attribute' => 'lat',
                'label' => 'GPS LAT stupne',
                'value' => function (Station $model)
                {
                    // We need to make string from LAT, because excel cuts decimal digits to 12 (we need 14)
                    // Leave the space on the end!!!
                    return str_replace(",", ".", (string)$model->lat . " ");
                },
            ],
            [
                'attribute' => 'lng',
                'label' => 'GPS LON stupne',
                'value' => function (Station $model)
                {
                    // We need to make string from LNG, because excel cuts decimal digits to 12 (we need 14)
                    // Leave the space on the end!!!
                    return str_replace(",", ".", (string)$model->lng . " ");
                },
            ],
            [
                'label' => 'Vyska nad morem [m] (nepovinne)',
                'value' => function ()
                {
                    return "void";
                },
            ],
            [
                'attribute' => 'mac_address',
                'label' => 'MAC',
            ],
            [
                'attribute' => 'serial_number',
                'label' => 'vyr.cislo',
            ],
            [
                'attribute' => 'name',
                'label' => 'Nazev Stanice',
            ],
            [
                'attribute' => 'type',
                'label' => 'Typ stanice',
                'value' => function (Station $model)
                {
                    $type = array_search($model->type, StationImport::$typeMapping);
                    return ($type !== false) ? $type : $model->type;
                },
            ],
            [
                'attribute' => 'direction',
                'label' => 'Hlavni smer vyzarovani [stupne]',
                'value' => function (Station $model)
                {
                    if ($model->type == StationController::TYPE_WIGIG) {
                        return $model->getStationTypeObject()->direction;
                    }
                    return null;
                },
            ],
            [
                'attribute' => 'frequency',
                'label' => 'Kmitocet f [MHz]',
                'value' => function (Station $model)
                {
                    if ($model->type == StationController::TYPE_FS) {
                        return $model->getStationTypeObject()->frequency;
                    }
                    return null;
                },
            ],
            [
                'attribute' => 'channel_width',
                'label' => 'sirka kanalu [MHz]',
            ],
            [
                'attribute' => 'eirp',
                'label' => 'EIRP [dBm]',
                'value' => function (Station $model)
                {
                    if ($model->type == StationController::TYPE_WIGIG) {
                        return $model->getStationTypeObject()->eirp;
                    }
                    return null;
                },
            ],
            [
                'attribute' => 'antenna_volume',
                'label' => 'Zisk anteny [dBi]',
            ],
            [
                'attribute' => 'power',
                'label' => 'privedeny vykon [dBm]',
            ],
            [
                'attribute' => 'id_station_pair',
                'label' => 'ID protistanice (nepovinne)',
            ],
            [
                'attribute' => 'ratio_signal_interference',
                'label' => 'pomer C/I [dB]',
                'value' => function (Station $model)
                {
                    if ($model->type == StationController::TYPE_FS) {
                        return $model->getStationTypeObject()->ratio_signal_interference;
                    }
                    return null;
                },
            ],
            [
                'label' => 'Stanice je Nova nebo Upravena?',
                'value' => function ()
                {
                    return 'uprava';
                },
            ],
            [
                'label' => 'Neobsazeno',
            ],
            [
                'label' => 'Neobsazeno',
            ],
            [
                'label' => 'Neobsazeno',
            ],
            [
                'label' => 'Neobsazeno',
            ],
        ];
    }

    /**
     * Prepare the export file of all user stations
     *
     * @param int $userId
     *
     * @return false|string - result of the temp file creation
     * @throws Exception
     */
    public function userStationsExport($userId = 0)
    {
        if (!$userId) {
            throw new Exception(_tF("non_existing_user", "user-export"));
        }

        ini_set('memory_limit', '1024M');

        $dataProvider = new ActiveDataProvider([
            'query' => Station::find()->andWhere(['id_user' => $userId]),
            'pagination' => [
                'pageSize' => 500,
            ],
            'sort' => [
                'defaultOrder' => [
                    'id' => SORT_ASC,
                ],
            ],
        ]);

        $spreadsheet = new Spreadsheet([
            'title' => _t("stations", 'user-export') . "_" . date('Y-m-d_His'),
            'dataProvider' => $dataProvider,
            'columns' => static::getColumnsForExport(),
            'startRowIndex' => $this->startRow,
            'writerType' => 'Xlsx',
        ]);

        $spreadsheet->getDocument()->getDefaultStyle()->getNumberFormat()->setFormatCode('@');

        $this->styleSheetAsImport($spreadsheet);

        $tempFileName = tempnam(sys_get_temp_dir(), 'EXS');

        // Save file to temporary dir, to download it later (cannot be send because of the Pjax)
        $spreadsheet->save($tempFileName);
        return $tempFileName;
    }

    /**
     *  Stylovani exportniho souboru XLS
     *
     * @param $spreadsheet Spreadsheet
     */
    protected function styleSheetAsImport(&$spreadsheet)
    {
        $spreadsheet->renderCell('A1', 'EXPORT',
            [
                'font' => [
                    'bold' => true,
                ],
                'fill' => [
                    'fillType' => Fill::FILL_SOLID,
                    'color' => ['rgb' => 'ffc000'],
                ],
            ]);

        foreach (range('A', 'W') as $column) {
            $spreadsheet->applyCellStyle("{$column}2", [
                'font' => ['bold' => true],
                'alignment' => [
                    'horizontal' => Alignment::HORIZONTAL_CENTER,
                    'vertical' => Alignment::VERTICAL_CENTER,
                    'wrapText' => true,
                ],
            ]);
        }
    }
}