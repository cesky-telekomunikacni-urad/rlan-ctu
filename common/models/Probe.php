<?php
/*
* Copyright 2023 DactylGroup s.r.o.
*
* Licensed under the EUPL, Version 1.2 or – as soon they
will be approved by the European Commission - subsequent
versions of the EUPL (the "Licence");
* You may not use this work except in compliance with the
Licence.
* You may obtain a copy of the Licence at:
*
* https://joinup.ec.europa.eu/software/page/eupl5
*
* Unless required by applicable law or agreed to in
writing, software distributed under the Licence is
distributed on an "AS IS" basis,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
express or implied.
* See the Licence for the specific language governing
permissions and limitations under the Licence.
*/
?>


<?php

namespace common\models;

use yii\behaviors\TimestampBehavior;
use yii\db\ActiveRecord;

/**
 * This is the model class for table "probe".
 *
 * @property integer $id
 * @property string $name
 * @property string $status
 * @property string $description
 * @property string $access_token
 * @property integer $deleted
 * @property integer $created_at
 * @property integer $updated_at
 *
 * @property ProbeScan[] $probeScans
 */
class Probe extends \dactylcore\core\db\ActiveRecord
{
    const STATUS_INACTIVE = 0;
    const STATUS_ACTIVE = 1;

    /**
     * @inheritdoc
     */
    public static function tableName(): string
    {
        return 'probe';
    }

    public static function getStatusNames(): array
    {
        return [
            static::STATUS_ACTIVE => _tF('active', 'probe'),
            static::STATUS_INACTIVE => _tF('inactive', 'probe'),
        ];
    }

    public function getLabelMode(): string
    {
        return [
            static::STATUS_ACTIVE => 'success',
            static::STATUS_INACTIVE => 'danger',
        ][$this->status];
    }

    public function getStatusName()
    {
        return static::getStatusNames()[$this->status];
    }

    /**
     * @inheritdoc
     */
    public function rules(): array
    {
        return [
            [['name', 'description', 'access_token', 'status'], 'required'],
            [['description'], 'string'],
            [['deleted', 'created_at', 'updated_at'], 'integer'],
            [['deleted'], 'default', 'value' => 0],
            [['name', 'access_token'], 'string', 'max' => 255],
            [['access_token'], 'unique'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels(): array
    {
        return [
            'id' => _tF('id', 'probe'),
            'name' => _tF('name', 'probe'),
            'status' => _tF('status', 'probe'),
            'description' => _tF('description', 'probe'),
            'access_token' => _tF('access_token', 'probe'),
            'deleted' => _tF('deleted', 'probe'),
            'created_at' => _tF('created_at'),
            'updated_at' => _tF('updated_at'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getProbeScans(): \yii\db\ActiveQuery
    {
        return $this->hasMany(ProbeScan::class, ['id_probe' => 'id']);
    }

    public function behaviors(): array
    {
        return [
            [
                'class' => TimestampBehavior::class,
                'attributes' => [
                    ActiveRecord::EVENT_BEFORE_INSERT => ['created_at', 'updated_at'],
                    ActiveRecord::EVENT_BEFORE_UPDATE => ['updated_at'],
                ],
            ],
        ];
    }

    public function renewToken(): bool
    {
        $this->access_token = \Yii::$app->security->generateRandomString();
        return $this->save();
    }
}
