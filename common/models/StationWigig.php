<?php
/*
* Copyright 2023 DactylGroup s.r.o.
*
* Licensed under the EUPL, Version 1.2 or – as soon they
will be approved by the European Commission - subsequent
versions of the EUPL (the "Licence");
* You may not use this work except in compliance with the
Licence.
* You may obtain a copy of the Licence at:
*
* https://joinup.ec.europa.eu/software/page/eupl5
*
* Unless required by applicable law or agreed to in
writing, software distributed under the Licence is
distributed on an "AS IS" basis,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
express or implied.
* See the Licence for the specific language governing
permissions and limitations under the Licence.
*/
?>


<?php

namespace common\models;

use DateTime;
use frontend\controllers\StationController;
use League\Geotools\Geotools;
use Yii;
use yii\helpers\ArrayHelper;

/**
 * @property int $id_station_wigig
 * @property int $id_station
 * @property int $direction
 * @property string $eirp
 * @property string $eirp_method
 * @property string $front_to_back_ratio
 * @property boolean $is_ptmp
 * @property float $angle_point_lat
 * @property float $angle_point_lng
 *
 * @property Station $station
 */
class StationWigig extends \dactylcore\core\db\ActiveRecord
{
    public const EIRP_METHOD_MANUAL = 'manual';
    public const EIRP_METHOD_AUTO = 'auto';
    //
    public const MINIMUM_ANTENNA_VOLUME_FOR_PAIRING = 25;
    public const MAXIMUM_DISTANCE_IN_KM_FOR_PAIRING = 10;
    // Constant set by the client
    protected const EIRP_PTMP_TRESHOLD = 40;
    protected $updatedAtCheck;

    public function setChecksum()
    {
        $this->updatedAtCheck = $this->updated_at;
    }

    public function hasChanged()
    {
        return $this->updated_at != $this->updatedAtCheck;
    }

    public function getLogGroupTo(): string
    {
        return Station::class;
    }

    public function setAngleDirections()
    {
        $this->station->refresh();

        $geotools = new Geotools();
        $coordA = new \League\Geotools\Coordinate\Coordinate([$this->station->lat, $this->station->lng]);
        $destinationPoint = $geotools->vertex()->setFrom($coordA)->destination($this->direction, 50000);
        $this->angle_point_lat = $destinationPoint->getLatitude();
        $this->angle_point_lng = $destinationPoint->getLongitude();
    }

    public static function getExcludedAttributes(): array
    {
        $ignore = ['id_station_wigig', 'eirp_method'];
        return ArrayHelper::merge(parent::getExcludedAttributes(), $ignore);
    }

    // attributes which's change moves status from finished to waiting
    public static $dangerousAttributes = ['is_ptmp', 'direction'];

    public function init()
    {
        $this->eirp_method = self::EIRP_METHOD_AUTO;
        parent::init();
    }

    public function save($runValidation = true, $attributeNames = null)
    {
        // invalidate status when changed : position, direction, PTMP OR EIRP went up
        if ($this->station->status == Station::STATUS_FINISHED
            && (array_intersect(array_keys($this->getDirtyAttributes()), self::$dangerousAttributes)
                || $this->eirpChangedUp()
            )) {

            $this->station->moveToWaiting();
            $this->station->save();
        }
        return parent::save($runValidation = true, $attributeNames = null);
    }

    public function setNextScenario()
    {
        $nexts = [
            StationController::STEP_1_NAME => StationController::STEP_2_LOCATION,
            StationController::STEP_2_LOCATION => StationController::STEP_3_PARAMS,
            StationController::STEP_3_PARAMS => StationController::STEP_4_SUMMARY,
        ];
        $currentScenario = $this->getScenario();
        $next = $currentScenario == StationController::STEP_4_SUMMARY ? StationController::STEP_4_SUMMARY :
            $nexts[$currentScenario];
        $this->setScenario($next);
    }

    public function eirpChangedUp()
    {
        return $this->getOldAttributes()['eirp'] < $this->eirp;
    }

    public function scenarios()
    {
        $default = ['deleted', 'created_at', 'updated_at'];
        return ArrayHelper::merge(parent::scenarios(), [
            StationController::STEP_1_NAME => ['id_station', ...$default],
            StationController::STEP_2_LOCATION => ['direction', 'angle_point_lat', 'angle_point_lng', ...$default],
            StationController::STEP_3_PARAMS => ['eirp_method', 'is_ptmp', 'eirp', 'front_to_back_ratio', ...$default],
            StationController::SCENARIO_DELETE => [...$default],
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return merge(parent::rules(), [
            [['id_station', 'direction'], 'required'],
            [['id_station'], 'integer'],
            [['eirp_method', 'angle_point_lat', 'angle_point_lng'], 'safe'],
            [['is_ptmp'], 'boolean'],
            [
                ['is_ptmp'],
                function ()
                {
                    if ($this->is_ptmp && $this->station->id_station_pair) {
                        $this->addError('is_ptmp', _tF('wigig-ptp-ptmp-pair-error', 'station'));
                    }
                },
            ],
            [
                'direction',
                'in',
                'range' => range(0, 360),
                'message' => _tF('angle has to be in range 0 to 360', 'station'),
            ],
            [
                ['direction', 'eirp', 'front_to_back_ratio'],
                'number',
                'message' => _tF('must be a number separated by point', 'station'),
            ],
            [
                ['id_station'],
                'exist',
                'skipOnError' => true,
                'targetClass' => Station::class,
                'targetAttribute' => ['id_station' => 'id'],
            ],
            [
                'eirp',
                'required',
                'when' => function ($model, $attribute)
                {
                    return $model->eirp_method == self::EIRP_METHOD_MANUAL;
                },
            ],
            [
                'eirp',
                function ($attribute, $params, $validator)
                {
                    $invalid = false;
                    if (!$this->isNewRecord && $this->is_ptmp && ($this->eirp < -20 || $this->eirp > 40)) {
                        // ptmp -> <-20 , 40 >
                        $this->addError($attribute, _tF('eirp has to be in range -20 to 40', 'station'));
                        $invalid = true;
                    } elseif (!$this->isNewRecord && !$this->is_ptmp && ($this->eirp < -20 || $this->eirp > 55)) {
                        // ptp -> <-20 , 55 >
                        $this->addError($attribute, _tF('eirp has to be in range -20 to 55', 'station'));
                        $invalid = true;
                    }

                    if ($invalid && $this->eirp_method == self::EIRP_METHOD_AUTO) {
                        setErrorFlash(_tF('eirp too much for ' . ($this->is_ptmp ? 'ptmp' : 'ptp'), 'station'));
                    }
                },
                'isEmpty' => function ($value)
                {
                    return $value === null;
                },
            ],
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return merge(parent::attributeLabels(), [
            'id_station_wigig' => _tF('id_station_wigig', 'station'),
            'id_station' => _tF('id_station', 'station'),
            'direction' => _tF('direction', 'station'),
            'eirp' => _tF('eirp', 'station'),
            'is_ptmp' => _tF('is_ptmp', 'station'),
            'front_to_back_ratio' => _tF('front_to_back_ratio', 'station'),
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function attributeHints()
    {
        return merge(parent::attributeLabels(), [
            'direction' => _tF('choose angle', 'station_hint'),
            'eirp' => _tF('eirp', 'station_hint'),
            'front_to_back_ratio' => _tF('front_to_back_ratio', 'station_hint'),
        ]);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getStation()
    {
        return $this->hasOne(Station::class, ['id' => 'id_station']);
    }

    public function load($data, $formName = null)
    {
        $out = parent::load($data, $formName);
        if ($this->station && $this->station->stationPair) {
            $this->is_ptmp = false;
        } else {
            if ($this->eirp_method == self::EIRP_METHOD_AUTO) {
                $this->is_ptmp = $this->station && $this->station->antenna_volume < c('PTMP_THRESHOLD');
            } else {
                $this->is_ptmp = $this->eirp <= self::EIRP_PTMP_TRESHOLD;
            }
        }

        $this->front_to_back_ratio = ($this->station && $this->station->antenna_volume > c('PTMP_THRESHOLD')) ?
            c('FRONT_TO_BACK_RATIO_HIGH') : c('FRONT_TO_BACK_RATIO_LOW');

        return $out;
    }

    public function setPtmp($antennaVolume)
    {
        if ($this->eirp_method == self::EIRP_METHOD_AUTO) {
            $this->is_ptmp = $antennaVolume < c('PTMP_THRESHOLD');
        } else {
            $this->is_ptmp = $this->eirp <= self::EIRP_PTMP_TRESHOLD;
        }
    }

    public static function verifyPairingCompatibility(?Station $station1, ?Station $station2, &$error = null): bool
    {
        // stations must exist
        if (!$station1 || !$station2) {
            $error = _tF('Both stations must exist', 'station');
            return false;
        }

        // owner check
        if (!$station1->canEdit() || !$station2->canEdit()) {
            $error = _tF('You must own both stations', 'station');
            return false;
        }

        // both must be wigig
        if (!($station1->type == StationController::TYPE_WIGIG && $station2->type == StationController::TYPE_WIGIG)) {
            $error = _tF('Wigig can only be paired to wigig', 'station');
            return false;
        }

        // station2 has pair already, and its not me
        if ($station2->id_station_pair && $station2->id_station_pair != $station1->id) {
            $error = _tF('Station for pairing is already paired to another station', 'station');
            return false;
        }

        // both must have antenna volume >=25
        if (
            $station1->antenna_volume < static::MINIMUM_ANTENNA_VOLUME_FOR_PAIRING ||
            $station2->antenna_volume < static::MINIMUM_ANTENNA_VOLUME_FOR_PAIRING
        ) {
            $error = _tF('Both stations must have antenna volume at least {value}', 'station', [
                'value' => static::MINIMUM_ANTENNA_VOLUME_FOR_PAIRING,
            ]);
            return false;
        }

        // distance between them must be <=10km
        $dist = Station::countDistanceFromStations($station1, $station2);
        if ($dist > static::MAXIMUM_DISTANCE_IN_KM_FOR_PAIRING) {
            $error = _tF('max distance length crossed', 'station');
            return false;
        }

        return true;
    }
}
