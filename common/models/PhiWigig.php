<?php
/*
* Copyright 2023 DactylGroup s.r.o.
*
* Licensed under the EUPL, Version 1.2 or – as soon they
will be approved by the European Commission - subsequent
versions of the EUPL (the "Licence");
* You may not use this work except in compliance with the
Licence.
* You may obtain a copy of the Licence at:
*
* https://joinup.ec.europa.eu/software/page/eupl5
*
* Unless required by applicable law or agreed to in
writing, software distributed under the Licence is
distributed on an "AS IS" basis,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
express or implied.
* See the Licence for the specific language governing
permissions and limitations under the Licence.
*/
?>


<?php

namespace common\models;

use Yii;

/**
 * @property int $id
 * @property string $phi
 * @property string $p
 * @property string $ad
 * @property int $ptmp
 * @property int $deleted
 * @property int $created_at
 * @property int $updated_at
 */
class PhiWigig extends \dactylcore\core\db\ActiveRecord
{

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return merge(parent::rules(), [
            [['phi', 'p', 'ad'], 'number'],
            [['ptmp', 'deleted', 'created_at', 'updated_at'], 'integer'],
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return merge(parent::attributeLabels(), [
            'id' => _tF('id', 'phi-wigig'),
            'phi' => _tF('phi', 'phi-wigig'),
            'p' => _tF('p', 'phi-wigig'),
            'ad' => _tF('ad', 'phi-wigig'),
            'ptmp' => _tF('ptmp', 'phi-wigig'),
            'deleted' => _tF('deleted', 'phi-wigig'),
            'created_at' => _tF('created_at', 'phi-wigig'),
            'updated_at' => _tF('updated_at', 'phi-wigig'),
        ]);
    }
}
