<?php
/*
* Copyright 2023 DactylGroup s.r.o.
*
* Licensed under the EUPL, Version 1.2 or – as soon they
will be approved by the European Commission - subsequent
versions of the EUPL (the "Licence");
* You may not use this work except in compliance with the
Licence.
* You may obtain a copy of the Licence at:
*
* https://joinup.ec.europa.eu/software/page/eupl5
*
* Unless required by applicable law or agreed to in
writing, software distributed under the Licence is
distributed on an "AS IS" basis,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
express or implied.
* See the Licence for the specific language governing
permissions and limitations under the Licence.
*/
?>


<?php

namespace common\models;

use frontend\controllers\StationController;
use Yii;
use yii\helpers\ArrayHelper;

/**
 * @property int $id_station_fs
 * @property int $id_station
 * @property int $ratio_signal_interference
 * @property float $frequency
 * @property float $specific_run_down
 *
 * @property Station $station
 */
class StationFs extends \dactylcore\core\db\ActiveRecord
{
    public CONST FORM_NAME = 'StationFs';

    // default value
    public $default_ratio = 12;

    public function getLogGroupTo(): string
    {
        return Station::class;
    }

    public static function getExcludedAttributes(): array
    {
        $ignore = ['id_station_fs'];
        return ArrayHelper::merge(parent::getExcludedAttributes(), $ignore);
    }


    public static $qamValues = [
        12 => '4QAM: C/I = 12dB',
        18 => '16QAM: C/I = 18dB',
        21 => '32QAM: C/I = 21dB',
        25 => '64QAM: C/I = 25dB',
        28 => '128QAM: C/I = 28dB',
        31 => '256QAM: C/I = 31dB',
        34 => '512QAM: C/I = 34dB',

    ];


    // attributes which's change moves status from finished to waiting
    public static $dangerousAttributes = ['frequency', 'specific_run_down'];

    public function save($runValidation = true, $attributeNames = null)
    {
        if (
            $this->station->status == Station::STATUS_FINISHED &&
            array_intersect(array_keys($this->getDirtyAttributes()), self::$dangerousAttributes)
        ) {
            $this->station->moveToWaiting();
            $this->station->save();

            if ($this->station->status != $this->station->stationPair->status) {
                $this->station->stationPair->moveToWaiting();
                $this->station->stationPair->save();
            }
        }
        return parent::save($runValidation = true, $attributeNames = null);
    }

    public function translateQam()
    {
        if(!isset($this->ratio_signal_interference)){
            $this->ratio_signal_interference = $this->default_ratio;
        }
        return self::$qamValues[$this->ratio_signal_interference];
    }

    public function scenarios()
    {
        $default = ['deleted', 'created_at', 'updated_at'];
        return ArrayHelper::merge(parent::scenarios(), [
            StationController::STEP_1_NAME => ['id_station', ...$default],
            StationController::STEP_2_LOCATION => [...$default],
            StationController::STEP_3_PARAMS => ['ratio_signal_interference',  'frequency', 'specific_run_down', ...$default],
            StationController::SCENARIO_DELETE => [...$default],
        ]);
    }

    public function setNextScenario()
    {
        $nexts = [
            StationController::STEP_1_NAME => StationController::STEP_2_LOCATION,
            StationController::STEP_2_LOCATION => StationController::STEP_3_PARAMS,
            StationController::STEP_3_PARAMS => StationController::STEP_4_SUMMARY,
        ];
        $currentScenario = $this->getScenario();
        $next = $currentScenario == StationController::STEP_4_SUMMARY ? StationController::STEP_4_SUMMARY : $nexts[$currentScenario];
        $this->setScenario($next);
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return merge(parent::rules(), [
            [['id_station', 'frequency', 'ratio_signal_interference'], 'required'],
            [['id_station'], 'integer'],
            [['ratio_signal_interference',  'specific_run_down'], 'number', 'message' => _tF('must be a number separated by point', 'station')],
            [
                'frequency',
                function ($attribute, $params, $validator) {
                    if (!$this->isNewRecord && ($this->frequency < 57000 || $this->frequency > 66000)) {
                        $this->addError($attribute, _tF('frequency has to be in range 57GHz to 66Ghz', 'station'));
                    }
                },
                'isEmpty' => function($value) {return $value === null;}
            ],
            [['id_station'], 'exist', 'skipOnError' => true, 'targetClass' => Station::class, 'targetAttribute' => ['id_station' => 'id']],
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return merge(parent::attributeLabels(), [
            'id_station_fs' => _tF('id_station_fs', 'station'),
            'id_station' => _tF('id_station', 'station'),
            'ratio_signal_interference' => _tF('ratio_signal_interference_label', 'station'),
            'frequency' => _tF('frequency_label', 'station'),
            'specific_run_down' => _tF('specific_run_down', 'station'),
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function attributeHints()
    {
        return merge(parent::attributeHints(), [
            '[a]ratio_signal_interference' => _tF('qam', 'station_hint'),
            '[b]ratio_signal_interference' => _tF('qam', 'station_hint'),
            '[a]frequency' => _tF('frequency', 'station_hint'),
            '[b]frequency' => _tF('frequency', 'station_hint'),
        ]);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getStation()
    {
        return $this->hasOne(Station::class, ['id' => 'id_station']);
    }

    public static function findAndSetClosestLattgas(StationFs &$fsA, StationFs &$fsB)
    {
        $qTemplate = "
        (SELECT lattgas as ltgs
        FROM (
            (SELECT `lattgas`, ABS(`frequency` - :val:) AS distance
              FROM `gases`
              WHERE `frequency` >= :val:
              order BY frequency
              LIMIT 1)
             UNION
             (SELECT `lattgas`, ABS(`frequency` - :val:) AS distance
              FROM `gases`
              WHERE `frequency` <= :val:
              order BY frequency DESC
              LIMIT 1)
             order by distance
             Limit 1) tmp)";

        $qA = str_replace(':val:', ($fsA->frequency / 1000), $qTemplate);
        $qB = str_replace(':val:', ($fsB->frequency / 1000), $qTemplate);
        $q = "{$qA}  UNION {$qB}";
        $data = \Yii::$app->db->createCommand($q)->queryAll();

        $valA = $data[0]['ltgs'];
        $valB = (count($data) == 1) ? $valA : $data[1]['ltgs'];
        $fsA->specific_run_down = $valA;
        $fsB->specific_run_down = $valB;
    }
}
