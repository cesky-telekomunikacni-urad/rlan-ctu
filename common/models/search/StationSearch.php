<?php
/*
* Copyright 2023 DactylGroup s.r.o.
*
* Licensed under the EUPL, Version 1.2 or – as soon they
will be approved by the European Commission - subsequent
versions of the EUPL (the "Licence");
* You may not use this work except in compliance with the
Licence.
* You may obtain a copy of the Licence at:
*
* https://joinup.ec.europa.eu/software/page/eupl5
*
* Unless required by applicable law or agreed to in
writing, software distributed under the Licence is
distributed on an "AS IS" basis,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
express or implied.
* See the Licence for the specific language governing
permissions and limitations under the Licence.
*/
?>


<?php

namespace common\models\search;

use common\models\StationFsPair;
use yii\helpers\Html;
use frontend\controllers\StationController;
use League\Geotools\Coordinate\Coordinate;
use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use yii\db\Expression;
use yii\helpers\ArrayHelper;
use common\models\Station;

/**
 * StationSearch represents the model behind the search form of `common\models\Station`.
 */
class StationSearch extends Station
{
    public $conflictIds = [];
    public $registeredAt;
    public $validTo;
    public $protectedTo;
    public $typeShortCut;

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [
                ['id', 'id_user', 'registered_at', 'valid_to', 'protected_to', 'deleted', 'created_at', 'updated_at'],
                'integer',
            ],
            [['mac_address', 'serial_number'], 'string'],
            [
                [
                    'type',
                    'status',
                    'published_by',
                    'name',
                    'registeredAt',
                    'validTo',
                    'protectedTo',
                    'hasConflicts',
                    'typeShortCut',
                ],
                'safe',
            ],
            [['lng', 'lat', 'antenna_volume', 'channel_width', 'power'], 'number'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        return ArrayHelper::merge(Model::scenarios(), []);
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @param array $filter
     *
     * @return ActiveDataProvider
     */
    public function search($params, $filter = [])
    {
        $query = Station::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (Yii::$app->request->isPjax) {
            $queryParams = Yii::$app->request->queryParams;
            $this->load($queryParams);

            $query->andFilterWhere(['like', 'id', $this->id])
                ->andFilterWhere(['like', 'name', $this->name]);

            // Filter by published way. FS only have master
            $query->andFilterWhere(['published_by' => $this->published_by]);

            if ($this->typeShortCut) {
                if ($this->typeShortCut == 'fs') {
                    $query->andFilterWhere(['type' => 'fs']);
                } else {
                    $query->leftJoin('station_wigig', 'station_wigig.id_station=station.id')
                        ->andFilterWhere(['station_wigig.is_ptmp' => ($this->typeShortCut == 'wigig_ptmp')]);
                }
            }

            return $dataProvider;
        }

//        if($this->registeredAt){
//            $toSearch = explode(" - ", $this->registeredAt);
//            $from = \DateTime::createFromFormat("d.m.Y", $toSearch[0]);
//            $to = \DateTime::createFromFormat("d.m.Y", $toSearch[0]);
//            $toSearchString = $to->format("Y-m-d");
//            $fromSearchString = $from->format("Y-m-d");
//            $this->registeredAt = "{$fromSearchString} - {$toSearchString}";
//        }

        if (!$this->validate()) {
            return $dataProvider;
        }

        $query->andFilterWhere([
            'id_user' => $this->id_user,
            'lng' => $this->lng,
            'lat' => $this->lat,
            'antenna_volume' => $this->antenna_volume,
            'published_by' => $this->published_by,
            'channel_width' => $this->channel_width,
            'power' => $this->power,
            'registered_at' => $this->registered_at,
            'valid_to' => $this->valid_to,
            'protected_to' => $this->protected_to,
            'deleted' => $this->deleted,
            'created_at' => $this->created_at,
            'updated_at' => $this->updated_at,
        ]);

        if ($this->typeShortCut) {
            if ($this->typeShortCut == 'fs') {
                $query->andFilterWhere(['type' => 'fs']);
            } else {
                $query->leftJoin('station_wigig', 'station_wigig.id_station=station.id')
                    ->andFilterWhere(['station_wigig.is_ptmp' => ($this->typeShortCut == 'wigig_ptmp')]);
            }
        }

        $query->andFilterWhere(['like', 'type', $this->type])
            ->andFilterWhere(['like', 'status', $this->status])
            ->andFilterWhere(['like', 'name', $this->name])
            ->andFilterWhere(['like', 'id', $this->id])
            ->andFilterWhere(['like', 'mac_address', $this->mac_address])
            ->andFilterWhere(['like', 'serial_number', $this->serial_number])
            ->andFilterWhere($filter);

        $this->filterDateRange($query, 'registeredAt', 'registered_at', 'd.m.Y');
        $this->filterDateRange($query, 'validTo', 'valid_to', 'd.m.Y');
        $this->filterDateRange($query, 'protectedTo', 'protected_to', 'd.m.Y');

        return $dataProvider;
    }

    /**
     * returns query which is filtered by filters and by location in 3.5km radius
     *
     * @param $mainModel
     * @param array $filter - filter
     *
     * @return array|\yii\db\ActiveRecord[]
     * @throws \yii\db\Exception
     */
    public function searchCloseStations($mainModel, $filter = [])
    {
        // TODO it runs comparison with all the stations even when it is requested in paginated grid...
        $q = $this->search($filter)->query;
        $positions = [];

        if ($mainModel->type == StationController::TYPE_FS) {
            $pair = self::getModelsFs($mainModel->id);
            $positions[] = [$pair->stationA->lat, $pair->stationA->lng];
            $positions[] = [$pair->stationB->lat, $pair->stationB->lng];
        } else {
            $wigig = $mainModel;
            $positions[] = [$wigig->lat, $wigig->lng];
        }

        // 3.5km as max distance of GPS coordinates
        // 40075/360 = 111.3194 = 1 point = 111km
        // 111.3194/3.5 = 31.6056x in one point
        // 1/31.6056 = 0.031441048
        $maxDistance = 3.5; //Kms

        $params = ['or'];
        foreach ($positions as $position) {
            $lat = $position[0];
            $lng = $position[1];

            $params[] = ['<', "ACOS(COS(RADIANS({$lat})) * COS(RADIANS(`lat`)) * COS(RADIANS({$lng}) - RADIANS(`lng`))  + SIN(RADIANS({$lat})) * SIN(RADIANS(`lat`))  ) * 6371", $maxDistance];

        }

        $q->andFilterWhere($params);
        $q->andFilterWhere(['status' => Station::STATUS_FINISHED]);
        $q->andFilterWhere(['!=', 'id_master', $mainModel->id_master]);

        $closestStations = $q->indexBy('id')->all();

        $usedIds = [];
        $requestedIds = [];
        foreach ($closestStations as $id => $item) {
            /**
             * @var Station $item
             */
            $requestedIds[] = $item->id_station_pair;
            $usedIds[] = $id;
        }
        $missingIds = array_diff($requestedIds, $usedIds);

        $missingStations = Station::find()->where(['IN', 'id', $missingIds])->indexBy('id')->all();

        /**
         * @var $closeStation Station
         */
        foreach ($closestStations as $closeStation) {
            if ($closeStation->isFs() && !$closeStation->antenna_volume) {
                continue;
            }
            $closeStation->compareWith($mainModel);
            if ($closeStation->passiveInterference || $mainModel->passiveInterference) {
                $this->conflictIds[] = $closeStation->id;
            }

            $this->anyInConflict |= ($closeStation->hasConflicts || $mainModel->hasConflicts);
        }


        $out = $closestStations + $missingStations;

        return $out;
    }

    public static function findStationsInPerimeter($lat, $lng, $perimeter)
    {
        $q = Station::find();

        // 3.5km as max distance of GPS coordinates
        // 40075/360 = 111.3194 = 1 point = 111km
        // 111.3194/3.5 = 31.6056x in one point
        // 1/111.3194 = 0.031441048
        $oneKm = 0.00898316;

        $q->andFilterWhere(['<', "ACOS(COS(RADIANS({$lat})) * COS(RADIANS(`lat`)) * COS(RADIANS({$lng}) - RADIANS(`lng`))  + SIN(RADIANS({$lat})) * SIN(RADIANS(`lat`))  ) * 6371", (double)$perimeter]);
        $q->andFilterWhere(['status' => Station::STATUS_FINISHED]);

        // 60ghz only
        $q->andFilterWhere(['IN', 'type', [Station::COMPARE_TYPE_FS, Station::COMPARE_TYPE_WIGIG]]);

        $closestStations = $q->indexBy('id')->asArray()->all();

        $usedIds = [];
        $requestedIds = [];
        foreach ($closestStations as $id => $item) {
            /**
             * @var Station $item
             */
            $requestedIds[] = $item['id_station_pair'];
            $usedIds[] = $id;
        }
        $missingIds = array_diff($requestedIds, $usedIds);

        $missingStations = Station::find()->where(['IN', 'id', $missingIds])->asArray()->indexBy('id')->all();

        $out = $closestStations + $missingStations;

        return $out;
    }

    /**
     * returns query which is filtered by filters and by location in radius specified for toll gates
     *
     * @param $mainModel
     * @param array $filter - filter
     *
     * @return array|\yii\db\ActiveRecord[]
     * @throws \yii\db\Exception
     */
    public function searchTollGateVicinity($mainModel, $filter = [])
    {
        $q = $this->search($filter)->query;

        $lng = null;
        $lat = null;

        if ($mainModel->type == StationController::TYPE_58_AP) {
            $lat = $mainModel->lat;
            $lng = $mainModel->lng;
        } else {
            $lat = $mainModel->lat;
            $lng = $mainModel->lng;
        }

        // 3.5km as max distance of GPS coordinates
        // 40075/360 = 111.3194 = 1 point = 111km
        // 111.3194/3.5 = 31.6056x in one point
        // 1/31.6056 = 0.031441048
        $metreRadius = c('TOLL_GATE_RADIUS');
        $kmRadius = intval($metreRadius) / 1000;

        $params = ['or'];
        $params[] = ['<', "ACOS(COS(RADIANS({$lat})) * COS(RADIANS(`lat`)) * COS(RADIANS({$lng}) - RADIANS(`lng`))  + SIN(RADIANS({$lat})) * SIN(RADIANS(`lat`))  ) * 6371", $kmRadius];
        $conditionType = [
            'or',
            ['=', 'type', StationController::TYPE_58_AP],
            ['=', 'type', StationController::TYPE_58_TG],
        ];

        $q->andFilterWhere($params);
        $q->andFilterWhere(['status' => Station::STATUS_FINISHED]);
        $q->andFilterWhere(['!=', 'id_master', $mainModel->id_master]);
        $q->andFilterWhere($conditionType);

        $closestStations = $q->indexBy('id')->all();

        /**
         * @var $closeStation Station
         */
        foreach ($closestStations as $closeStation) {
            if ($mainModel->type == StationController::TYPE_58_AP) {
                if ($closeStation->type == StationController::TYPE_58_TG) {
                    $mainModel->hasConflicts = true;
                }
            } else {
                if ($closeStation->type == StationController::TYPE_58_AP) {
                    $closeStation->hasConflicts = true;
                }
            }

            $this->anyInConflict |= ($closeStation->hasConflicts || $mainModel->hasConflicts);
        }

        if ($mainModel->type == StationController::TYPE_58_AP) {
            $this->checkConflictsWithExclusionZones($lng, $lat);
        }

        return $closestStations;
    }

    /**
     * Checks if 5.8GHz AP is in conflict with any of the exclusion zones defined in config.
     * Retrieves and parses config values, and then loops through all zones if not overlapping
     *
     * @param $lng - lng of station to check conflicts
     * @param $lat - lat of station to check conflicts
     */
    public function checkConflictsWithExclusionZones($lng, $lat) : void {

        $excludedZones = self::parseExclusionZones58GHz();

        if (count($excludedZones) != 0) {
            $collisionDistanceKm = floatval(c('EXCLUDED_ZONE_58_GHZ_DIAMETER'));

            $stationPosition = new Coordinate([$lat, $lng]);
            $geotools = new \League\Geotools\Geotools();

            foreach ($excludedZones as $zoneCoordinates) {
                $zoneCenter = new Coordinate([$zoneCoordinates[1], $zoneCoordinates[0]]);
                $distance = $geotools->distance()->setFrom($stationPosition)->setTo($zoneCenter);
                if ($distance->in('km')->vincenty() < $collisionDistanceKm) {
                    $this->anyInConflict = true;
                }
            }
        }
    }

    public static function getModelsFs(int $id = null): StationFsPair
    {
        $pair = new StationFsPair();

        if (!$id) {
            $pair->initNew();
            return $pair;
        } else {
            $pair->findById($id);
            return $pair;
        }
    }

    public static function countUnpublishedStations($idUser)
    {
        return Station::find()->where('`id` = `id_master`', [])
            ->andWhere([
                'id_user' => $idUser,
            ])
            ->andWhere([
                'IN',
                'status',
                [
                    Station::STATUS_WAITING,
                    Station::STATUS_DRAFT,
                    Station::STATUS_UNPUBLISHED,
                ]
            ])
            ->andWhere([
                'IN',
                'type',
                [
                    StationController::TYPE_FS,
                    StationController::TYPE_WIGIG,
                ],
            ])
            ->count();
    }

    /**
     * Function search wigigs to pair
     *
     * @param $idStation
     * @param $lat
     * @param $lng
     * @param null $term
     * @param int $page
     * @param int $limit
     *
     * @return array
     */
    public static function searchWigigsToPair($idStation, $lat, $lng, $term = null, $page = 1, $limit = 20)
    {
        $offset = ($page - 1) * $limit;

        $myStation = Station::findOne($idStation);
        if (!$myStation) {
            return [];
        }

        $query = Station::find()
            ->select([
                'id',
                'text' => 'name',
                'lat' => 'lat',
                'lng' => 'lng',
            ])
            ->join('LEFT JOIN', 'station_wigig', 'station_wigig.id_station = id')
            ->andWhere(['!=', 'id', $idStation])
            ->andWhere(['type' => StationController::TYPE_WIGIG])
            ->andWhere(['status' => Station::STATUS_FINISHED])
            ->andWhere(['or', ['id_station_pair' => null], ['id_station_pair' => $idStation]])
            ->andWhere(['id_user' => $myStation->id_user])
            ->andWhere(['station_wigig.is_ptmp' => false])
            ->filterHaving(['or', ['LIKE', 'name', $term], ['LIKE', 'id', $term]])
            ->offset($offset)
            ->orderBy(['name' => SORT_ASC]);


        //TODO: Konstnta 10 KM na prohledavani zparovatelnych wigigu
        // 10km as max distance of GPS coordinates
        // 40075/360 = 111.3194 = 1 point = 111km
        // 111.3194/10 = 11,1319x in one point
        // 1/11,1319 = 0,089831566
        $maxDistance = 10;  //10KM of max distance
        $query->andWhere(['<', "ACOS(COS(RADIANS({$lat})) * COS(RADIANS(`lat`)) * COS(RADIANS({$lng}) - RADIANS(`lng`))  + SIN(RADIANS({$lat})) * SIN(RADIANS(`lat`))  ) * 6371", $maxDistance]);


        if ($limit !== null) {
            $query->limit($limit);
        }

        $data = [];
        foreach ($query->asArray()->each() as $station) {
            $data[] = [
                'id' => $station['id'],
                'text' => Html::tag('span', $station['text'], [
                    'data' => [
                        'lat' => $station['lat'],
                        'lng' => $station['lng'],
                    ]
                ])
            ];
        }

        return $data;
    }

    /**
     * Parses exclusion zones coordinates from config into array of [lng,lat] tuples.
     * Ignores corrupted entries.
     *
     * @return array - [lng, lat] tuples in decimal format eg. [16.1241,49.14352]
     *                  on error or no valid entry returns an empty array
     */
    public static function parseExclusionZones58GHz(): array
    {

        $configString = c('EXCLUDED_ZONES_58_GHZ');
        $coordinatesArray = [];

        if ($configString == null) {
            return $coordinatesArray;
        }

        $configLines = explode("\n", $configString);

        foreach ($configLines as $line) {
            $parsedLine = explode(",", $line);
            $lat = trim($parsedLine[0]);
            $lng = trim($parsedLine[1]);

            if (self::isCorrectCoordinateFormat($lat) && self::isCorrectCoordinateFormat($lng)) {
                $coordinatesArray[] = [$lng, $lat];
            }
        }

        return $coordinatesArray;

    }

    /**
     * Verifies that GPS coordinate is in the right decimal format -> xx.abc eg. 48.12543
     *
     * @param string $coordinate
     * @return int
     */
    protected static function isCorrectCoordinateFormat(string $coordinate): int
    {
        return (preg_match("/^[0-9][0-9]\.[0-9]+$/", $coordinate));
    }
}
