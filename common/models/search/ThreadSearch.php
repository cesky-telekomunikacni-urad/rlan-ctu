<?php
/*
* Copyright 2023 DactylGroup s.r.o.
*
* Licensed under the EUPL, Version 1.2 or – as soon they
will be approved by the European Commission - subsequent
versions of the EUPL (the "Licence");
* You may not use this work except in compliance with the
Licence.
* You may obtain a copy of the Licence at:
*
* https://joinup.ec.europa.eu/software/page/eupl5
*
* Unless required by applicable law or agreed to in
writing, software distributed under the Licence is
distributed on an "AS IS" basis,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
express or implied.
* See the Licence for the specific language governing
permissions and limitations under the Licence.
*/
?>


<?php

namespace common\models\search;

use Yii;
use yii\base\Model;
use dactylcore\core\data\ActiveDataProvider;
use yii\helpers\ArrayHelper;
use common\models\Thread;

/**
 * ThreadSearch represents the model behind the search form of `common\models\Thread`.
 */
class ThreadSearch extends Thread
{
    public $status;

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
//            , 'id_station', 'id_station_disturber',
            [
                [
                    'id',
                    'id_recipient',
                    'id_sender',
                    'show_sender_info',
                    'show_recipient_info',
                    'deleted',
                    'created_at',
                    'updated_at',
                ],
                'integer',
            ],
            [['reason', 'subject', 'status_sender', 'status_recipient', 'status'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        return ArrayHelper::merge(Model::scenarios(), []);
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Thread::find();
        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            return $dataProvider;
        }

        $query->andFilterWhere([
            'id' => $this->id,
            'id_recipient' => $this->id_recipient,
            'id_sender' => $this->id_sender,
            'id_station' => $this->id_station,
            'id_station_disturber' => $this->id_station_disturber,
            'show_sender_info' => $this->show_sender_info,
            'show_recipient_info' => $this->show_recipient_info,
            'deleted' => $this->deleted,
            'created_at' => $this->created_at,
            'updated_at' => $this->updated_at,
        ]);

        $query->andFilterWhere(['like', 'reason', $this->reason])
            ->andFilterWhere(['like', 'subject', $this->subject]);

        return $dataProvider;
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @param $idUser
     * @return ActiveDataProvider
     */
    public function searchUsersThreads($params, $idUser)
    {
        $userId = Yii::$app->getUser()->id;

        $query = Thread::find()->andWhere(['or', ['id_recipient' => $idUser ],['id_sender' => $idUser]]);
        $query->addSelect("*, IF(`id_recipient` = '{$userId}', `status_recipient`, `status_sender`) AS status");

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            return $dataProvider;
        }

        $query->andFilterWhere([
            'id' => $this->id,
            'id_recipient' => $this->id_recipient,
            'id_sender' => $this->id_sender,
            'id_station' => $this->id_station,
            'id_station_disturber' => $this->id_station_disturber,
            'show_sender_info' => $this->show_sender_info,
            'show_recipient_info' => $this->show_recipient_info,
            'deleted' => $this->deleted,
            'created_at' => $this->created_at,
            'updated_at' => $this->updated_at,
        ]);

        if (array_key_exists('ThreadSearch', $params) && array_key_exists('reason', $params['ThreadSearch'])) {
            $query->andFilterWhere(['like', 'reason', $this->reason]);

        }
        if (array_key_exists('ThreadSearch', $params) && array_key_exists('subject', $params['ThreadSearch'])) {

            $query->andFilterWhere(['like', 'subject', $this->subject]);
        }
        if (array_key_exists('ThreadSearch', $params) && array_key_exists('status', $params['ThreadSearch'])) {
            $query->andFilterWhere(['or', ['status_recipient' => $this->status, 'id_recipient' => $idUser], ['status_sender' => $this->status, 'id_sender' => $idUser]]);
        }

//        $query->andFilterWhere(['like', 'reason', $this->reason])
//            ->andFilterWhere(['like', 'subject', $this->subject]);

        $query->orderBy([new \yii\db\Expression("FIELD (`status`, 'new','read','archived')")]);
        return $dataProvider;
    }
}
