<?php
/*
* Copyright 2023 DactylGroup s.r.o.
*
* Licensed under the EUPL, Version 1.2 or – as soon they
will be approved by the European Commission - subsequent
versions of the EUPL (the "Licence");
* You may not use this work except in compliance with the
Licence.
* You may obtain a copy of the Licence at:
*
* https://joinup.ec.europa.eu/software/page/eupl5
*
* Unless required by applicable law or agreed to in
writing, software distributed under the Licence is
distributed on an "AS IS" basis,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
express or implied.
* See the Licence for the specific language governing
permissions and limitations under the Licence.
*/
?>


<?php


namespace common\models\search;


use common\models\ProbeScanWlan;
use dactylcore\core\data\ActiveDataProvider;

class ProbeScanWlanSearch extends ProbeScanWlan
{
    public string $name = "";
    public string $address = "";
    public $time = null;


    public function rules() : array
    {
        return [
            [['id_probe_scan', 'created_at', 'updated_at'], 'integer'],
            [['rssi', 'frequency'], 'number'],
            [['bssid', 'ssid', 'capabilities'], 'string', 'max' => 512],
            [['name', 'time', 'address'], 'string'],
        ];
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params): ActiveDataProvider
    {
        $query = ProbeScanWlan::find()
            ->joinWith('probe')
            ->joinWith('probeScan')
            ->andWhere(['probe_scan_wlan.deleted' => 0]);

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'sort' => [
                'defaultOrder' => ['id' => SORT_DESC],
                'attributes' => [
                    'id',
                    'bssid',
                    'rssi',
                    'ssid',
                    'capabilities',
                    'frequency',
                    'name' => [
                        'asc' => ['probe.name' => SORT_ASC],
                        'desc' => ['probe.name' => SORT_DESC],
                    ],
                    'address' => [
                        'asc' => ['probe_scan.address' => SORT_ASC],
                        'desc' => ['probe_scan.address' => SORT_DESC],
                    ],
                    'time' => [
                        'asc' => ['probe_scan.time' => SORT_ASC],
                        'desc' => ['probe_scan.time' => SORT_DESC],
                    ],
                ]
            ],
        ]);


        $this->load($params);

        if (!$this->validate()) {
            return $dataProvider;
        }

        $query->andFilterWhere(['like', 'bssid', $this->bssid]);
        $query->andFilterWhere(['like', 'rssi', $this->rssi]);
        $query->andFilterWhere(['like', 'ssid', $this->ssid]);
        $query->andFilterWhere(['like', 'capabilities', $this->capabilities]);
        $query->andFilterWhere(['like', 'frequency', $this->frequency]);
        $query->andFilterWhere(['like', 'probe.name', $this->name]);
        $query->andFilterWhere(['like', 'probe_scan.address', $this->address]);
        $query->andFilterWhere(['like', 'probe_scan.time', $this->time]);


        return $dataProvider;
    }


}