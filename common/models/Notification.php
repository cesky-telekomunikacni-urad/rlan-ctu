<?php
/*
* Copyright 2023 DactylGroup s.r.o.
*
* Licensed under the EUPL, Version 1.2 or – as soon they
will be approved by the European Commission - subsequent
versions of the EUPL (the "Licence");
* You may not use this work except in compliance with the
Licence.
* You may obtain a copy of the Licence at:
*
* https://joinup.ec.europa.eu/software/page/eupl5
*
* Unless required by applicable law or agreed to in
writing, software distributed under the Licence is
distributed on an "AS IS" basis,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
express or implied.
* See the Licence for the specific language governing
permissions and limitations under the Licence.
*/
?>


<?php

namespace common\models;

use Yii;
use dactylcore\core\db\ActiveQuery;
use dactylcore\core\db\ActiveRecord;
use yii\helpers\Json;

/**
 * @property int $id
 * @property string $text
 * @property int $displayed_from
 * @property int $displayed_to
 * @property int $enabled
 * @property int $deleted
 * @property int $created_at
 * @property int $updated_at
 */
class Notification extends ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return merge(parent::rules(), [
            [['text'], 'required'],
            [['displayed_from', 'displayed_to', 'enabled'], 'integer'],
            [['text'], 'string', 'max' => 1000],
            [['displayedFrom', 'displayedTo'], 'string', 'max' => 16, 'min' => 16],
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return merge(parent::attributeLabels(), [
            'id' => _tF('id', 'notification'),
            'text' => _tF('text', 'notification'),
            'displayed_from' => _tF('displayed_from', 'notification'),
            'displayed_to' => _tF('displayed_to', 'notification'),
            'displayedFrom' => _tF('displayed_from', 'notification'),
            'displayedTo' => _tF('displayed_to', 'notification'),
            'enabled' => _tF('enabled', 'notification'),
        ]);
    }

    public function getLogModelName(): string
    {
        return $this->id;
    }

    public function getLogModelOperationData(): array
    {
        return ['name' => $this->getLogModelName()];
    }

    public function getDisplayedFrom()
    {
        if (!$this->displayed_from) {
            return null;
        }
        return Yii::$app->formatter->asDatetime($this->displayed_from, 'php:d.m.Y H:i');
    }

    public function getDisplayedTo()
    {
        if (!$this->displayed_to) {
            return null;
        }
        return Yii::$app->formatter->asDatetime($this->displayed_to, 'php:d.m.Y H:i');
    }

    public function setDisplayedFrom($value)
    {
        $date = new \DateTime($value, new \DateTimeZone(Yii::$app->formatter->timeZone));
        $value = $value == "" ? null : Yii::$app->formatter->asTimestamp($date);
        if ($value) {
            $this->displayed_from = $value;
        }
    }

    public function setDisplayedTo($value)
    {
        $date = new \DateTime($value, new \DateTimeZone(Yii::$app->formatter->timeZone));
        $value = $value == "" ? null : Yii::$app->formatter->asTimestamp($date);
        if ($value) {
            $this->displayed_to = $value;
        }
    }

    /**
     * @return Notification[]
     */
    public static function findAllSuitable($cookieJson = '[]')
    {
        $seenIds = Json::decode($cookieJson);

        return static::find()
                     ->andWhere(['NOT IN', 'id', $seenIds])
                     ->andWhere(['enabled' => 1])
                     ->andWhere([
                         'OR',
                         [
                             'AND',
                             ['displayed_from' => null],
                             ['displayed_to' => null],
                         ],
                         [
                             'AND',
                             ['IS NOT', 'displayed_from', null],
                             ['<=', 'displayed_from', time()],
                             ['displayed_to' => null],
                         ],
                         [
                             'AND',
                             ['IS NOT', 'displayed_to', null],
                             ['>=', 'displayed_to', time()],
                             ['displayed_from' => null],
                         ],
                         [
                             'AND',
                             ['IS NOT', 'displayed_from', null],
                             ['IS NOT', 'displayed_to', null],
                             ['<=', 'displayed_from', time()],
                             ['>=', 'displayed_to', time()],
                         ],
                     ])
                     ->orderBy(['id' => SORT_ASC])
                     ->all();
    }
}
