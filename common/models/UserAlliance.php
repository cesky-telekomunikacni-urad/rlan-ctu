<?php
/*
* Copyright 2023 DactylGroup s.r.o.
*
* Licensed under the EUPL, Version 1.2 or – as soon they
will be approved by the European Commission - subsequent
versions of the EUPL (the "Licence");
* You may not use this work except in compliance with the
Licence.
* You may obtain a copy of the Licence at:
*
* https://joinup.ec.europa.eu/software/page/eupl5
*
* Unless required by applicable law or agreed to in
writing, software distributed under the Licence is
distributed on an "AS IS" basis,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
express or implied.
* See the Licence for the specific language governing
permissions and limitations under the Licence.
*/
?>


<?php

namespace common\models;

use console\tasks\UserAllianceMailJob;
use dactylcore\core\db\ActiveQuery;
use dactylcore\core\db\ActiveRecord;
use dactylcore\log\LogManager;
use dactylcore\log\LogOperation;
use modules\user\common\models\User;
use Yii;
use yii\data\ActiveDataProvider;

/**
 * @property int $id
 * @property string $name
 * @property string $description
 * @property int $deleted
 * @property int $created_at
 * @property int $updated_at
 *
 * @property User[] $users
 */
class UserAlliance extends ActiveRecord
{
    public const STATUS_INVITED = 'invited';
    public const STATUS_MEMBER = 'member';
    public const STATUS_ADMIN = 'admin';
    public const STATUS_EXCLUDED = 'excluded';

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return merge(parent::rules(), [
            [['name'], 'required'],
            [['description'], 'string'],
            [['name'], 'string', 'max' => 255],
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return merge(parent::attributeLabels(), [
            'id' => _tF('id', 'user-alliance'),
            'name' => _tF('name', 'user-alliance'),
            'description' => _tF('description', 'user-alliance'),
            'deleted' => _tF('deleted', 'user-alliance'),
            'created_at' => _tF('created_at', 'user-alliance'),
            'updated_at' => _tF('updated_at', 'user-alliance'),
        ]);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUserAllianceConnectors()
    {
        return $this->hasMany(UserAllianceConnector::class, ['id_user_alliance' => 'id']);
    }

    /**
     * {@inheritDoc}
     */
    public function beforeValidate()
    {
        if ($this->isNewRecord) {
            $this->created_at = Yii::$app->formatter->asTimestamp(time());
        }
        $this->updated_at = Yii::$app->formatter->asTimestamp(time());

        return parent::beforeValidate();
    }

    /**
     * {@inheritDoc}
     */
    public function beforeDelete()
    {
        foreach ($this->getUsers()->each() as $user) {
            UserAllianceMailJob::pushJob([
                'allianceName' => $this->name,
                'userId' => $user->id,
                'notificationType' => UserAllianceMailJob::NOTIFICATION_DELETED,
            ]);

            Yii::$app->db->createCommand()->update('user_alliance_connector', [
                'deleted' => 1,
            ], ['id_user_alliance' => $this->id])->execute();
        }

        return parent::beforeDelete();
    }

    /**
     * GETTERS
     */

    /**
     * Users in alliance
     *
     * @return \yii\db\ActiveQuery
     * @throws \yii\base\InvalidConfigException
     */
    public function getUsers()
    {
        return $this->hasMany(User::class, ['id' => 'id_user'])
                    ->viaTable('user_alliance_connector', ['id_user_alliance' => 'id'])
                    ->innerJoin('user_alliance_connector',
                        'user_alliance_connector.id_user = user.id')
                    ->andWhere([
                        'OR',
                        ['user_alliance_connector.status' => UserAlliance::STATUS_MEMBER],
                        ['user_alliance_connector.status' => UserAlliance::STATUS_ADMIN],
                    ]);
    }


    /**
     * CUSTOM functions
     */

    /**
     * Add user to alliance in admin role
     *
     * @return bool - wether user was added
     */
    public function addUserAsAdmin($idUser)
    {
        $user = User::findOne($idUser);

        if ($user === null || $user->isInAlliance()) {
            return false;
        }

        $this->addUserToAlliance($idUser, self::STATUS_ADMIN);
        return true;
    }

    /**
     * Protected adding of user with specific role
     *
     * @param $idUser
     * @param $role
     * @param $token
     *
     * @throws \yii\db\Exception
     */
    protected function addUserToAlliance($idUser, $role, $token = null)
    {
        $now = time();
        Yii::$app->db->createCommand()->insert('user_alliance_connector', [
            'id_user_alliance' => $this->id,
            'id_user' => $idUser,
            'status' => $role,
            'invitation_token' => $token,
            'created_at' => $now,
            'invited_at' => $now,
            'updated_at' => $now,
        ])->execute();
    }

    /**
     * Get list of users in alliance
     *
     * @return ActiveQuery
     */
    public function getUserList()
    {
        return User::find()
                   ->select([
                       'user.id',
                       'companyName',
                       'first_name',
                       'last_name',
                       'type' => 'user.type',
                       'email',
                       'membership' => 'user_alliance_connector.status',
                   ])
                   ->leftJoin('user_alliance_connector', 'user_alliance_connector.id_user = user.id')
                   ->andWhere([
                       'OR',
                       ['user_alliance_connector.status' => UserAlliance::STATUS_MEMBER],
                       ['user_alliance_connector.status' => UserAlliance::STATUS_ADMIN],
                   ])
                   ->andWhere(['user_alliance_connector.id_user_alliance' => $this->id])
                   ->andWhere(['user_alliance_connector.deleted' => 0])
                   ->asArray();
    }

    /**
     * Send one invitation
     *
     * @param $idUser
     *
     * @return bool
     * @throws \yii\base\Exception
     */
    public function sendInvitationTo($idUser)
    {
        $inviteToken = \Yii::$app->security->generateRandomString(64);

        if (!$idUser) {
            return false;
        }

        try {
            $this->addUserToAlliance($idUser, self::STATUS_INVITED, $inviteToken);

            UserAllianceMailJob::pushJob([
                'allianceName' => $this->name,
                'userId' => $idUser,
                'notificationType' => UserAllianceMailJob::NOTIFICATION_INVITATION,
                'inviteToken' => $inviteToken,
            ]);
        } catch (\Exception $e) {
            \Yii::error($e->getMessage());
            return false;
        }

        return true;
    }

    /**
     * Check if user is Admin in alliance
     *
     * @param $userId
     *
     * @return bool
     * @throws \yii\db\Exception
     */
    public function isUserAdmin($userId)
    {
        if (!$userId) {
            return false;
        }

        try {
            $result = Yii::$app->db
                ->createCommand("SELECT * FROM `user_alliance_connector` WHERE `id_user_alliance`={$this->id} AND `id_user`={$userId} AND `status`=\"" .
                    self::STATUS_ADMIN . '" AND `deleted`=0')
                ->queryOne();
        } catch (\Exception $e) {
            Yii::error($e->getMessage());
            return false;
        }

        return !empty($result);
    }

    public function isUserMember($userId)
    {
        if (!$userId) {
            return false;
        }

        try {
            $result = Yii::$app->db
                ->createCommand("SELECT * FROM `user_alliance_connector` WHERE `id_user_alliance`={$this->id} AND `id_user`={$userId} AND (`status`=\"" .
                    self::STATUS_ADMIN . '" OR `status`="' . self::STATUS_MEMBER . '")  AND `deleted`=0')
                ->queryOne();
        } catch (\Exception $e) {
            Yii::error($e->getMessage());
            return false;
        }

        return !empty($result);
    }

    /**
     * @param $idUser
     *
     * @return bool
     */
    public function promoteMember($idUser)
    {
        if (!$idUser) {
            return false;
        }

        try {
            Yii::$app->db->createCommand()
                         ->update('user_alliance_connector', [
                             'status' => self::STATUS_ADMIN,
                         ], [
                             'id_user' => $idUser,
                             'id_user_alliance' => $this->id,
                             'status' => self::STATUS_MEMBER,
                             'deleted' => 0,
                         ])
                         ->execute();
        } catch (\Exception $e) {
            Yii::error($e->getMessage());
            return false;
        }

        //Log action
        $logManager = LogManager::getInstance();
        $log = (new LogOperation());
        $log->operationType = ActiveRecord::OPERATION_TYPE_EVENT;
        $log->dataChanged = true;
        $log->modelId = loggedUser()->getName();
        $log->operations = [
            'promoting_member' => [
                'member {email} promoted in {name} by {admin}',
                [
                    'name' => loggedUser()->alliance->name,
                    'email' => $idUser,
                    'admin' => loggedUser()->email,
                ],
            ],
        ];
        $logManager->registerOperation($log);
        $logManager->saveOperations(null);

        return true;
    }


    /**
     * @param $idUser
     *
     * @return bool
     */
    public function degradeMember($idUser)
    {
        if (!$idUser) {
            return false;
        }

        // There has to be at least one admin
        if ($this->isUserAdmin($idUser)) {
            $adminCount = Yii::$app->db
                ->createCommand(
                    "SELECT COUNT(`id`) FROM `user_alliance_connector` WHERE id_user_alliance={$this->id} and `status` = \"" .
                    self::STATUS_ADMIN . '" AND `deleted`=0')->queryScalar();

            // There has to be at least one admin
            if ($adminCount <= 1) {
                return false;
            }
        }

        try {
            Yii::$app->db->createCommand()
                         ->update('user_alliance_connector', [
                             'status' => self::STATUS_MEMBER,
                         ], [
                             'id_user' => $idUser,
                             'id_user_alliance' => $this->id,
                             'status' => self::STATUS_ADMIN,
                             'deleted' => 0,
                         ])
                         ->execute();
        } catch (\Exception $e) {
            Yii::error($e->getMessage());
            return false;
        }
        return true;
    }

    /**
     * @param $idUser
     *
     * @return bool
     */
    public function excludeMember($idUser)
    {
        if (!$idUser) {
            return false;
        }

        if ($this->isUserAdmin($idUser)) {
            $adminCount = Yii::$app->db
                ->createCommand(
                    "SELECT COUNT(`id`) FROM `user_alliance_connector` WHERE id_user_alliance={$this->id} and `status` = \"" .
                    self::STATUS_ADMIN . '" AND `deleted`=0')->queryScalar();

            // There has to be at least one admin
            if ($adminCount <= 1) {
                return false;
            }
        }

        try {
            Yii::$app->db->createCommand()->update('user_alliance_connector', [
                'status' => self::STATUS_EXCLUDED,
            ], ['id_user' => $idUser, 'id_user_alliance' => $this->id, 'deleted' => 0])->execute();

            UserAllianceMailJob::pushJob([
                'allianceName' => $this->name,
                'userId' => $idUser,
                'notificationType' => UserAllianceMailJob::NOTIFICATION_EXCLUDED,
            ]);
        } catch (\Exception $e) {
            Yii::error($e->getMessage());
            return false;
        }
        return true;
    }

    /**
     * @param $idUser
     *
     * @return bool
     */
    public function resignMember($idUser)
    {
        if (!$idUser) {
            return false;
        }

        try {
            Yii::$app->db->createCommand()->update('user_alliance_connector', [
                'deleted' => 1,
            ], ['id_user' => $idUser, 'id_user_alliance' => $this->id, 'deleted' => 0])->execute();
        } catch (\Exception $e) {
            Yii::error($e->getMessage());
            return false;
        }
        return true;
    }

    public function getAllStations() {

        $userListIds = [];
        foreach ($this->getUserList()->each() as $userArray) {
            array_push($userListIds, $userArray['id']);
        }

        $query = Station::find()
                        ->andWhere(['IN', 'id_user', $userListIds]);

        return  new ActiveDataProvider([
            'query' => $query,
            'pagination' => false,
        ]);
    }


    /****** STATIC FUNCTIONS  **************/

    /**
     * User accepts invitation and becomes member
     *
     * @param $token
     *
     * @return bool
     */
    public static function acceptInvitation($token)
    {
        if (!$token) {
            return false;
        }

        try {
            Yii::$app->db->createCommand()->update('user_alliance_connector', [
                'status' => self::STATUS_MEMBER,
                'invitation_token' => null,
            ], ['invitation_token' => $token, 'deleted' => 0])->execute();
        } catch (\Exception $e) {
            Yii::error($e->getMessage());
            return false;
        }
        return true;
    }

    /**
     * Get userID from invitation
     *
     * @param $token
     *
     * @return array|false|\yii\db\DataReader
     * @throws \yii\db\Exception
     */
    public static function getUserIdByInvitation($token)
    {
        if (!$token) {
            return null;
        }

        $userId = Yii::$app->db
            ->createCommand("SELECT `id_user` FROM `user_alliance_connector` WHERE `invitation_token`=\"{$token}\" AND `deleted`=0")
            ->queryOne();

        if ($userId) {
            return $userId['id_user'];
        }
        return null;
    }

    /**
     * @param $userId1
     * @param $userId2
     *
     * @return bool
     */
    public static function areInOneAlliance($userId1, $userId2)
    {
        if (!$userId1 || !$userId2) {
            return false;
        }

        // Get alliance of First user
        try {
            $idAlli1 = Yii::$app->db
                ->createCommand(
                    "SELECT `id_user_alliance` FROM `user_alliance_connector` WHERE id_user={$userId1} AND (`status`=\"" .
                    self::STATUS_ADMIN . '" OR `status`="' . self::STATUS_MEMBER . '") AND `deleted`=0')->queryScalar();
        } catch (\Exception $e) {
            Yii::error($e->getMessage());
            return false;
        }

        // Get alliance of second user
        try {
            $idAlli2 = Yii::$app->db
                ->createCommand(
                    "SELECT `id_user_alliance` FROM `user_alliance_connector` WHERE id_user={$userId2} AND (`status`=\"" .
                    self::STATUS_ADMIN . '" OR `status`="' . self::STATUS_MEMBER . '") AND `deleted`=0')->queryScalar();
        } catch (\Exception $e) {
            Yii::error($e->getMessage());
            return false;
        }

        if ($idAlli1 === false || $idAlli2 === false) {
            return false;
        }

        return $idAlli1 === $idAlli2;
    }
}
