<?php
/*
* Copyright 2023 DactylGroup s.r.o.
*
* Licensed under the EUPL, Version 1.2 or – as soon they
will be approved by the European Commission - subsequent
versions of the EUPL (the "Licence");
* You may not use this work except in compliance with the
Licence.
* You may obtain a copy of the Licence at:
*
* https://joinup.ec.europa.eu/software/page/eupl5
*
* Unless required by applicable law or agreed to in
writing, software distributed under the Licence is
distributed on an "AS IS" basis,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
express or implied.
* See the Licence for the specific language governing
permissions and limitations under the Licence.
*/
?>


<?php

namespace common\models;

use frontend\controllers\StationController;
use Yii;
use dactylcore\core\db\ActiveQuery;
use dactylcore\core\db\ActiveRecord;
use yii\helpers\ArrayHelper;

/**
 * @property int $id_station_58
 * @property int $id_station
 * @property int $is_ap
 * @property int $deleted
 * @property int $created_at
 * @property int $updated_at
 *
 * @property Station $station
 */
class Station58 extends ActiveRecord
{

    protected $updatedAtCheck;

    public static function tableName() {
        return 'station_58';
    }

    public function setChecksum()
    {
        $this->updatedAtCheck = $this->updated_at;
    }

    public function hasChanged()
    {
        return $this->updated_at != $this->updatedAtCheck;
    }

    public function scenarios()
    {
        return ArrayHelper::merge(parent::scenarios(), [
            StationController::SCENARIO_DELETE => [],
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return merge(parent::rules(), [
            [['id_station'], 'required'],
            [['id_station', 'is_ap', 'deleted', 'created_at', 'updated_at'], 'integer'],
            [
                ['id_station'],
                'exist',
                'skipOnError' => true,
                'targetClass' => Station::class,
                'targetAttribute' => ['id_station' => 'id'],
            ],
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return merge(parent::attributeLabels(), [
            'id_station_58' => _tF('id_station_58', 'station-58'),
            'id_station' => _tF('id_station', 'station-58'),
            'is_ap' => _tF('is_ap', 'station-58'),
            'deleted' => _tF('deleted', 'station-58'),
            'created_at' => _tF('created_at', 'station-58'),
            'updated_at' => _tF('updated_at', 'station-58'),
        ]);
    }

    /**
     * @return ActiveQuery
     */
    public function getStation()
    {
        return $this->hasOne(Station::class, ['id' => 'id_station']);
    }
}
