<?php
/*
* Copyright 2023 DactylGroup s.r.o.
*
* Licensed under the EUPL, Version 1.2 or – as soon they
will be approved by the European Commission - subsequent
versions of the EUPL (the "Licence");
* You may not use this work except in compliance with the
Licence.
* You may obtain a copy of the Licence at:
*
* https://joinup.ec.europa.eu/software/page/eupl5
*
* Unless required by applicable law or agreed to in
writing, software distributed under the Licence is
distributed on an "AS IS" basis,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
express or implied.
* See the Licence for the specific language governing
permissions and limitations under the Licence.
*/
?>


<?php

namespace common\models;

use Box\Spout\Writer\Common\Creator\WriterEntityFactory;
use common\models\search\StationSearch;
use dactylcore\core\helpers\Date;
use dactylcore\core\validators\EitherAttribute;
use dactylcore\log\common\components\LogArchiveComponent;
use dactylcore\log\console\tasks\RemoveExportFileJob;
use dactylcore\user\frontend\models\User;
use DateTime;
use frontend\controllers\StationController;
use NumberFormatter;
use Yii;
use yii\db\ActiveQuery;
use yii\db\Exception;
use yii\db\QueryInterface;
use yii\helpers\ArrayHelper;
use yii\helpers\Json;
use yii\helpers\StringHelper;

/**
 * @property int $id
 * @property int $id_user
 * @property int $id_station_pair
 * @property int $id_master
 * @property string $type
 * @property double $lng
 * @property double $lat
 * @property float $antenna_volume
 * @property float $channel_width
 * @property float $power
 * @property string $pair_position
 * @property int $registered_at
 * @property int $valid_to
 * @property int $protected_to
 * @property string $status
 * @property int $hardware_identifier
 * @property string $mac_address
 * @property string $serial_number
 * @property string $macAddress
 * @property string $serialNumber
 * @property int $deleted
 * @property int $created_at
 * @property int $updated_at
 * @property string $name
 * @property string $published_by
 * @property string $waiting_api_changes_json
 *
 * @property User $user
 * @property StationFs[] $stationFs
 * @property Station $stationPair
 * @property StationWigig[] $stationWigigs
 * @property StationWigig $stationWigig
 * @property Station58[] $station58s
 * @property Station58 $station58
 * @property boolean isExpired
 */
class Station extends \dactylcore\core\db\ActiveRecord
{
    use CalculatorTrait;

    public const STATUS_DRAFT = 'draft';
    public const STATUS_WAITING = 'waiting';
    public const STATUS_FINISHED = 'finished';
    public const STATUS_EXPIRED = 'expired';
    public const STATUS_UNPUBLISHED = 'unpublished';
    public const POSITION_A = 'a';
    public const POSITION_B = 'b';
    public const FORM_NAME = 'Station';
    public const AVAILABLE_STATUSES
        = [
            self::STATUS_DRAFT,
            self::STATUS_WAITING,
            self::STATUS_FINISHED,
            self::STATUS_EXPIRED,
            self::STATUS_UNPUBLISHED,
        ];
    public const COLORS_STATUSES
        = [
            self::STATUS_DRAFT => '#',
            self::STATUS_WAITING => '#',
            self::STATUS_FINISHED => '#',
            self::STATUS_EXPIRED => '#',
            self::STATUS_UNPUBLISHED => '#',
        ];
    public const GRID_ICON_TYPE_NA = 'na';
    public const GRID_ICON_TYPE_OK = 'ok';
    public const GRID_ICON_TYPE_WARNING = 'warning';
    public const GRID_ICON_TYPE_DANGER = 'danger';
    public const GRID_DATE_CLASS_NA = 'na';
    public const GRID_DATE_CLASS_OK = 'ok';
    public const GRID_DATE_CLASS_WARNING = 'warning';
    public const GRID_DATE_CLASS_DANGER = 'danger';
    public const PERIOD_WARNING_VALID = 'REGISTRATION_PERIOD_WARNING';
    public const PERIOD_WARNING_PROT = 'PROTECTION_PERIOD_WARNING';
    const COMPARE_TYPE_WIGIG = 'wigig';
    const COMPARE_TYPE_WIF = 'wf';
    const COMPARE_TYPE_FS = 'fs';
    public $updateStep = StationController::STEP_1_NAME;
    public const PUBLISHED_BY_DECLARATION = 'declaration';
    public const PUBLISHED_BY_PUBLICATION = 'publication';
    public $typeStation = null;
    private $statusChange = '';
    public $anyInConflict = false;
    const BOUNDARIES_EAST = 18.87;
    const BOUNDARIES_WEST = 12;
    const BOUNDARIES_NORTH = 51.0618;
    const BOUNDARIES_SOUTH = 48.53;
    protected $updatedAtCheck;
    /**
     * note if this object was compared from perspective of A or B station of FS
     * @var string
     */
    public $comparedFrom = 'a';
    // interference from perspective of new station

    // passive = I as a new station am interfered
    public $passiveInterference = false;
    public $passiveIsolationIncreaseRequested = 0;
    // active = I as a new station interfere old ones
    public $activeInterference = false;
    public $activeIsolationIncreaseRequested = 0;
    public $hasConflicts = false;
    public $overlapping = true;
    /**
     * pointer to station pair object. Used in setting of pair station in StationPair class
     * @var Station|null
     */
    public $pointerToPairStation = null;
    public const MAC_ADDRESS = 0;
    public const SERIAL_NUMBER = 1;
    public static $statusOrder
        = [
            self::STATUS_DRAFT => 1,
            self::STATUS_WAITING => 2,
            self::STATUS_UNPUBLISHED => 3,
            self::STATUS_FINISHED => 4,
            self::STATUS_EXPIRED => 5,
        ];

    public function setUserId($userId = null)
    {
        if (!$this->id_user) {
            $this->id_user = $userId;
        }
    }

    public function fields()
    {
        return [
            'id',
            'id_station_pair',
            'lat',
            'type',
            'typeName',
            'formattedId',
            'name',
            'lng',
            'pair_position',
            'id_user',
            'angle' => function (Station $model) {
                if (!$model->type == StationController::TYPE_WIGIG) {
                    return $this->getStationTypeObject()->direction;
                }
                return null;
            },
        ];
    }

    public function setName($val)
    {
        $this->name = $val;
    }

    public function getName()
    {
        if ($this->isFs()) {
            $letter = strtoupper($this->pair_position);
            return "{$this->name} {$letter}";
        }
        return $this->name;
    }

    public function setChecksum()
    {
        $this->updatedAtCheck = $this->updated_at;
    }

    public function hasChanged()
    {
        return $this->updated_at != $this->updatedAtCheck;
    }

    public static function formatNumber($val)
    {
        if (is_numeric($val)) {
            return Yii::$app->formatter->asDecimal($val, null, [NumberFormatter::MIN_FRACTION_DIGITS => 0]);
        }
        return $val;
    }

    public static function getExcludedAttributes(): array
    {
        $ignore = ['id_master', 'id_station', 'eirp_method', 'id_station', 'id_station_pair', 'pair_position'];
        return ArrayHelper::merge(parent::getExcludedAttributes(), $ignore);
    }

    public function getLogModelName(): string
    {
        return _tF($this->type, 'station') . " #" . $this->formatThisId();
    }

    public function getLogRoute(): array
    {
        return ['/station/update', 'id' => $this->id];
    }

    public function getDateTimeAttributeFormatted($attribute, $format = '%B %e, %Y')
    {
        return _tF(strftime($format, $this->$attribute));
    }

    public function getUserIdPadded($padLength = 6, $padWith = '0', $padType = STR_PAD_LEFT): string
    {
        return str_pad($this->id_user, $padLength, $padWith, $padType);
    }

    public function isMyStation($idUser = null)
    {
        $idUser = $idUser ?: Yii::$app->getUser()->id;
        return $idUser == $this->id_user && $idUser != null;
    }

    public function canEdit()
    {
        if ($this->id_user == null) {
            return false;
        }
        return $this->id_user == Yii::$app->getUser()->id ||
            hasAccessTo('app-frontend_station_update-all') ||
            UserAlliance::areInOneAlliance($this->id_user, Yii::$app->getUser()->id);
    }

    /**
     * {@inheritDoc}
     */
    public function defineOperations($operationType, $changedAtts, $modelData = [], $modelName = ''): array
    {
        $out = [];
        $logAbleChanges = ['waiting-finished'];
        if ($this->statusChange && in_array($this->statusChange, $logAbleChanges)) {
            $out[$this->statusChange] = ["{$this->statusChange} {name}", ['name' => $this->getLogModelName()]];
        } else {
            $out = array_merge(parent::defineOperations($operationType, $changedAtts,
                ['name' => $this->getLogModelName()], 'Station'), $out);
        }
        return $out;
    }

    /**
     * the same as getLogModelName but it returns array [att => val]
     * @return array
     */
    public function getLogModelOperationData(): array
    {
        return ['name' => $this->getLogModelName()];
    }

    public function getLogGroupTo(): string
    {
        return '';
    }

    public static $dangerousAttributes = ['lng', 'lat', 'power', 'id_station_pair'];

    /* TODO edit scenarions with new station types - 5.2ghz remaining*/
    public function scenarios()
    {
        $default = ['deleted', 'created_at', 'updated_at'];
        switch ($this->type) {
            case StationController::TYPE_FS:
            case StationController::TYPE_WIGIG:
                return ArrayHelper::merge(parent::scenarios(), [
                    StationController::STEP_1_NAME => ['name', ...$default],
                    StationController::STEP_2_LOCATION => ['lat', 'lng', 'id_station_pair', ...$default],
                    StationController::STEP_3_PARAMS => [
                        'antenna_volume',
                        'channel_width',
                        'power',
                        'hardware_identifier',
                        'serial_number',
                        'mac_address',
                        'serialNumber',
                        'macAddress',
                    ],
                    StationController::SCENARIO_DELETE => [],
                    StationController::SCENARIO_UNPUBLISH => ['status', 'published_by', ...$default],
                ]);
            case StationController::TYPE_58_AP:
            case StationController::TYPE_58_TG:
            case StationController::TYPE_52:
                return ArrayHelper::merge(parent::scenarios(), [
                    StationController::STEP_1_NAME => ['name', ...$default],
                    StationController::STEP_2_LOCATION => ['lat', 'lng', ...$default],
                    StationController::STEP_4_SUMMARY => ['name', 'lat', 'lng', ...$default],
                    StationController::STEP_3_PARAMS => ['mac_address', 'macAddress',],
                    StationController::SCENARIO_DELETE => [],
                    StationController::SCENARIO_UNPUBLISH => ['status', 'published_by', ...$default],
                ]);
            default:
                return parent::scenarios();
        }
    }

    public function setNextScenario()
    {
        $nexts = [
            StationController::STEP_1_NAME => StationController::STEP_2_LOCATION,
            StationController::STEP_2_LOCATION => StationController::STEP_3_PARAMS,
            StationController::STEP_3_PARAMS => StationController::STEP_4_SUMMARY,
        ];
        $currentScenario = $this->getScenario();
        $next = $currentScenario == StationController::STEP_4_SUMMARY ? StationController::STEP_4_SUMMARY :
            $nexts[$currentScenario];
        $this->setScenario($next);
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return merge(parent::rules(), [
            [['waiting_api_changes_json'], 'string'],
            [
                [
                    'id_user',
                    'registered_at',
                    'valid_to',
                    'protected_to',
                    'deleted',
                    'created_at',
                    'updated_at',
                    'id_station_pair',
                    'id_master',
                    'hardware_identifier',
                    'updateStep',
                ],
                'integer',
            ],
            [['lat', 'lng', 'status', 'id_user', 'name'], 'required'],
            [
                'channel_width',
                'required',
                'when' => function () {
                    return !in_array($this->type, [
                        StationController::TYPE_58_AP,
                        StationController::TYPE_58_TG,
                        StationController::TYPE_52,
                    ]);
                },
            ],
            [
                ['lng', 'lat', 'antenna_volume', 'channel_width', 'power'],
                'number',
                'message' => _tF('must be a number separated by point', 'station'),
            ],

            [
                'lat',
                'compare',
                'operator' => '<=',
                'compareValue' => self::BOUNDARIES_NORTH,
                'message' => _tF('latitude has to be in czechia', 'station'),
            ],
            [
                'lat',
                'compare',
                'operator' => '>=',
                'compareValue' => self::BOUNDARIES_SOUTH,
                'message' => _tF('latitude has to be in czechia', 'station'),
            ],

            [
                'lng',
                'compare',
                'operator' => '<=',
                'compareValue' => self::BOUNDARIES_EAST,
                'message' => _tF('longitude has to be in czechia', 'station'),
            ],
            [
                'lng',
                'compare',
                'operator' => '>=',
                'compareValue' => self::BOUNDARIES_WEST,
                'message' => _tF('longitude has to be in czechia', 'station'),
            ],

            [['type', 'status', 'pair_position', 'name'], 'string', 'max' => 255],
            [
                'antenna_volume',
                function ($attribute, $params, $validator) {
                    if (!$this->isNewRecord &&
                        $this->isFs() &&
                        ($this->antenna_volume < 30 || $this->antenna_volume > 60)) {
                        $this->addError($attribute, _tF('antenna gain has to be in range 30 to 60', 'station'));
                    } elseif (!$this->isNewRecord &&
                        $this->isFs() &&
                        $this->power &&
                        ($this->antenna_volume + $this->power < 0 || $this->antenna_volume + $this->power > 55)) {
                        $this->addError($attribute, _tF('eirp_max_55', 'station'));
                        $this->addError('power', _tF('eirp_max_55', 'station'));
                    } elseif (!$this->isNewRecord &&
                        !$this->isFs() &&
                        ($this->antenna_volume < 0 || $this->antenna_volume > 60)) {
                        $this->addError($attribute, _tF('antenna gain has to be in range 0 to 60', 'station'));
                    }
                },
                'isEmpty' => function ($value) {
                    return $value === null;
                },
            ],
            [
                'channel_width',
                function ($attribute, $params, $validator) {
                    if (!$this->isNewRecord && ($this->channel_width < 50 || $this->channel_width > 2200)) {
                        $this->addError($attribute, _tF('channel_width has to be in range 50 to 2200', 'station'));
                    }
                },
                'isEmpty' => function ($value) {
                    return $value === null;
                },
            ],
            [
                'id_station_pair',
                function ($attribute, $params, $validator) {
                    $t = $this->type;

                    if (
                        $this->id_station_pair &&
                        is_numeric($this->id_station_pair) &&
                        (!in_array($t, [StationController::TYPE_WIGIG, StationController::TYPE_FS]))
                    ) {
                        $this->addError($attribute,
                            _tF('Only Wigig or FS stations can be paired', 'station'));
                    }

                    if (!isAppApi() || $this->isNewRecord) {
                        return true;
                    }

                    if ($t == StationController::TYPE_WIGIG) {
                        $pairStation = Station::findOne($this->id_station_pair);
                        if ($pairStation) {
                            if (!StationWigig::verifyPairingCompatibility($this, $pairStation, $error)) {
                                $this->addError($attribute, $error);
                            }
                        }
                    }
                },
            ],
            [
                ['id_user'],
                'exist',
                'skipOnError' => true,
                'targetClass' => User::class,
                'targetAttribute' => ['id_user' => 'id'],
            ],
            [
                ['macAddress'],
                'required',
                'when' => function (Station $model, $attribute) {
                    return (
                        ($model->getScenario() == StationController::STEP_3_PARAMS
                            && $model->hardware_identifier == Station::MAC_ADDRESS)
                        || (in_array($model->type, [
                            StationController::TYPE_58_AP,
                            StationController::TYPE_58_TG,
                            StationController::TYPE_52,
                        ]))
                    );
                },
            ],
            [
                ['mac_address'],
                function ($attribute) {
                    $duplicitingStationId = null;

                    if (!$this->isMacAddressUnique($this->$attribute, $duplicitingStationId)) {
                        if (!$duplicitingStationId) {
                            $this->addError('macAddress',
                                _tF('unique_mac_error_with_id',
                                    'station', [
                                        'value' => $this->$attribute,
                                        'id' => '',
                                        'idUser' => '',
                                    ]));
                            return;
                        }

                        $s = Station::findOne($duplicitingStationId);
                        $loggedUserIsStationOwner = $s->id_user == loggedUser()->id;

                        $this->addError('macAddress',
                            _tF($loggedUserIsStationOwner ? 'unique_mac_error_with_id' :
                                'unique_mac_error_with_id_and_user',
                                'station', [
                                    'value' => $this->$attribute,
                                    'id' => $duplicitingStationId,
                                    'idUser' => $s->id_user,
                                ]));
                        if (isAppApi() && $loggedUserIsStationOwner) {
                            Yii::$app->response->headers->set('Dupliciting-station-id', $duplicitingStationId);
                        }
                    }
                },
                'when' => function (Station $model) {
                    return (in_array($model->type, [
                        StationController::TYPE_58_AP,
                        StationController::TYPE_58_TG,
                        StationController::TYPE_52,
                        StationController::TYPE_FS,
                        StationController::TYPE_WIGIG,
                    ])
                    );
                },
            ],
            [
                ['mac_address'],
                EitherAttribute::class,
                'exclusive' => true,
                'compareAttribute' => 'serial_number',
                'when' => function (Station $model, $attribute) {
                    return ($model->getScenario() == StationController::STEP_3_PARAMS &&
                        in_array($model->type, [
                            StationController::TYPE_WIGIG,
                            StationController::TYPE_FS,
                        ])
                    );
                },
            ],
            [
                ['mac_address', 'macAddress'],
                function ($attribute) {
                    {
                        $valueRx = '[0-9a-fA-F]{2}';
                        if (preg_match("/^({$valueRx}:){5}{$valueRx}$/", $this->$attribute) === 0) {
                            $this->addError($attribute, _tF('Invalid MAC format', 'station'));
                        }
                    }
                },
            ],
            [
                ['serialNumber'],
                'required',
                'when' => function (Station $model, $attribute) {
                    return (
                        $model->getScenario() == StationController::STEP_3_PARAMS
                        && $model->hardware_identifier == Station::SERIAL_NUMBER
                    );
                },
            ],
            [
                ['serial_number'],
                EitherAttribute::class,
                'exclusive' => true,
                'compareAttribute' => 'mac_address',
                'when' => function (Station $model, $attribute) {
                    return ($model->getScenario() == StationController::STEP_3_PARAMS &&
                        in_array($model->type, [
                            StationController::TYPE_WIGIG,
                            StationController::TYPE_FS,
                        ])
                    );
                },
            ],
            [
                ['serial_number', 'serialNumber'],
                function ($attribute) {
                    if (strlen($this->$attribute) > 12 || preg_match("/^\w+$/", $this->$attribute) === 0) {
                        $this->addError($attribute, _tF('Invalid format of serial number', 'station'));
                    }
                },
            ],
        ]);
    }

    public function prolongRegistration()
    {
        $transaction = Yii::$app->db->beginTransaction();
        $success = true;

        $pairStation = $this->getPairStation();
        $this->initRegistrations(false);

        if ($this->isExpired) {
            $this->status = static::STATUS_FINISHED;
        }

        if ($this->isFs() && $pairStation) {
            $pairStation->initRegistrations(false);
            if ($pairStation->isExpired) {
                $pairStation->status = static::STATUS_FINISHED;
            }

            $success &= $pairStation->save(true, ['valid_to', 'protected_to', 'registered_at', 'status']);
        }

        $success &= $this->save(true, ['valid_to', 'protected_to', 'registered_at', 'status']);

        if ($success) {
            $transaction->commit();
        } else {
            $transaction->rollBack();
        }

        return $success;
    }

    public function formatThisId()
    {
        return str_pad($this->id_master, 7, '0', STR_PAD_LEFT);
    }

    public function getFormattedId()
    {
        return $this->formatThisId();
    }

    public static function formatId($id)
    {
        return str_pad($id, 7, '0', STR_PAD_LEFT);
    }

    public static function deFormatId($id)
    {
        return $id ? ltrim($id, "0") : $id;
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return merge(parent::attributeLabels(), [
            'id' => _tF('id', 'station'),
            'id_user' => _tF('id_user', 'station'),
            'type' => _tF('type', 'station'),
            'lng' => _tF('lng', 'station'),
            'lat' => _tF('lat', 'station'),
            'antenna_volume' => _tF('antenna_volume', 'station'),
            'channel_width' => _tF('channel_width', 'station'),
            'power' => _tF('power', 'station'),
            'registered_at' => _tF('registered_at', 'station'),
            'valid_to' => _tF('valid_to', 'station'),
            'protected_to' => _tF('protected_to', 'station'),
            'status' => _tF('status', 'station'),
            'deleted' => _tF('deleted', 'station'),
            'created_at' => _tF('created_at', 'station'),
            'updated_at' => _tF('updated_at', 'station'),
            'typeName' => _tF('typeName', 'station'),
            'typeShortCut' => _tF('typeShortCut', 'station'),
            'name' => _tF('station_name', 'station'),
            'published_by' => _tF('published_by', 'station'),
            'hasConflicts' => _tF('hasConflicts', 'station'),
            'mac_address' => _tF('mac_address', 'station'),
            'serial_number' => _tF('serial_number', 'station'),
            'macAddress' => _tF('mac_address', 'station'),
            'serialNumber' => _tF('serial_number', 'station'),
            'id_station_pair' => _tF('id_station_pair', 'station'),

            'passiveInterference' => _tF('passiveInterference', 'station'),
            'passiveIsolationIncreaseRequested' => _tF('passiveIsolationIncreaseRequested', 'station'),

            'activeInterference' => _tF('activeInterference', 'station'),
            'activeIsolationIncreaseRequested' => _tF('activeIsolationIncreaseRequested', 'station'),
        ]);
    }

    public function attributeHints()
    {
        return merge(parent::attributeHints(), [
            'antenna_volume' => _tF('antenna_volume', 'station_hint'),
            'channel_width' => _tF('channel_width', 'station_hint'),
            'power' => _tF('power', 'station_hint'),
            'macAddress' => _tF('mac_address_hint', 'station'),
            'serialNumber' => _tF('serial_number_hint', 'station'),

            '[a]channel_width' => _tF('channel_width', 'station_hint'),
            '[a]power' => _tF('power', 'station_hint'),
            '[a]ratio_signal_interference' => _tF('qam', 'station_hint'),
            '[a]macAddress' => _tF('mac_address_hint', 'station'),
            '[a]serialNumber' => _tF('serial_number_hint', 'station'),

            '[b]channel_width' => _tF('channel_width', 'station_hint'),
            '[b]power' => _tF('power', 'station_hint'),
            '[b]ratio_signal_interference' => _tF('qam', 'station_hint'),
            '[b]macAddress' => _tF('mac_address_hint', 'station'),
            '[b]serialNumber' => _tF('serial_number_hint', 'station'),
        ]);
    }

    /**
     * @return ActiveQuery
     */
    public function getUser()
    {
        return $this->hasOne(User::class, ['id' => 'id_user']);
    }

    /**
     * @return ActiveQuery
     */
    public function getStationFs()
    {
        return $this->hasMany(StationFs::class, ['id_station' => 'id']);
    }

    /**
     * @return ActiveQuery
     */
    public function getThreads()
    {
        $a = $this->hasMany(Thread::class, ['id_station' => 'id']);
        return $a;
    }

    /**
     * @return ActiveQuery
     */
    public function getDisturbingThreads()
    {
        return $this->hasMany(Thread::class, ['id_station_disturber' => 'id']);
    }

    /**
     * @return ActiveQuery
     */
    public function getStationWigig()
    {
        return $this->hasMany(StationWigig::class, ['id_station' => 'id']);
    }

    /**
     * @return ActiveQuery
     */
    public function getStation58()
    {
        return $this->hasMany(Station58::class, ['id_station' => 'id']);
    }

    /**
     * @return ActiveQuery
     */
    public function getStation52()
    {
        $a = $this->hasMany(Station52::class, ['id_station' => 'id']);
        return $a;
    }

    public function getStationTypeObject()
    {
        // caching
        if ($this->typeStation === null) {
            switch ($this->type) {
                case StationController::TYPE_WIGIG:
                    $q = $this->getStationWigig();
                    break;
                case StationController::TYPE_FS:
                    $q = $this->getStationFs();
                    break;
                case StationController::TYPE_58_AP:
                case StationController::TYPE_58_TG:
                    $q = $this->getStation58();
                    break;
                case StationController::TYPE_52:
                    $q = $this->getStation52();
                    break;
                default:
                    throw new Exception(_tF('Invalid station type', 'station'));
            }
            $this->typeStation = $q->one();
        }
        return $this->typeStation;
    }

    /**
     * Used in export
     *
     * @param $dbModelWithRelations
     *
     * @return mixed
     * @throws Exception
     */
    public static function getStationTypeObject_static($dbModelWithRelations)
    {
        $result = null;

        $dbModelWithRelations = (object)$dbModelWithRelations;

        switch ($dbModelWithRelations->type) {
            case StationController::TYPE_WIGIG:
                $result = $dbModelWithRelations->stationWigig;
                break;
            case StationController::TYPE_FS:
                $result = $dbModelWithRelations->stationFs;
                break;
            case StationController::TYPE_58_AP:
            case StationController::TYPE_58_TG:
                $result = $dbModelWithRelations->station58;
                break;
            default:
                throw new Exception(_tF('Invalid station type', 'station'));
        }

        return $result ? (object)$result[0] : $result;
    }

    /**
     * @param bool $updateRegisteredAt set current timestamp for attribute registered_at
     */
    public function initRegistrations($updateRegisteredAt = true)
    {
        $registerPeriod = \HConfig::getValue('REGISTRATION_PERIOD');
        $protectedPeriod = \HConfig::getValue('PROTECTION_PERIOD');

        $registerIntv = new \DateInterval("P{$registerPeriod}M");
        $registerTo = (new \DateTime('NOW'))->add($registerIntv);

        $protectedIntv = new \DateInterval("P{$protectedPeriod}M");
        $protectedTo = (new \DateTime('NOW'))->add($protectedIntv);

        $this->valid_to = Yii::$app->formatter->asTimestamp($registerTo);
        $this->protected_to = Yii::$app->formatter->asTimestamp($protectedTo);

        if ($updateRegisteredAt) {
            $this->registered_at = Yii::$app->formatter->asTimestamp(time());
        }
    }

    public function getTypeName()
    {
        switch ($this->type) {
            case StationController::TYPE_FS:
                $name = _tF('type_fs', 'station');
                $position = strtoupper($this->pair_position);
                $formattedId = Station::formatId($this->id_master);
                return "{$name} {$position} #{$formattedId}";
            case StationController::TYPE_WIGIG:
                return ($this->getStationTypeObject()->is_ptmp ? _tF('type_wigig_ptmp', 'station') :
                        _tF('type_wigig_ptp', 'station')) . " #" . $this->formatThisId();
            case StationController::TYPE_58_AP:
                return _tF('type_58_ap', 'station') . " #" . $this->formatThisId();
            case StationController::TYPE_58_TG:
                return _tF('type_58_tg', 'station') . " #" . $this->formatThisId();
            case StationController::TYPE_52:
                return _tF('type_52', 'station') . " #" . $this->formatThisId();
            default:
                throw new Exception(_tF('Invalid station type', 'station'));
        }
    }

    public static function getStaticTypeName($type, $id, $stationTypeObject = null)
    {
        switch ($type) {
            case StationController::TYPE_FS:
                return _tF('type_fs', 'station');
            case StationController::TYPE_WIGIG:
                if (!$stationTypeObject) {
                    $station = self::findOne($id);
                    if ($station) {
                        $stationTypeObject = $station->getStationTypeObject();
                    }
                }

                if ($stationTypeObject) {
                    return
                        $stationTypeObject->is_ptmp
                            ?
                            _tF('type_wigig_ptmp', 'station')
                            :
                            _tF('type_wigig_ptp', 'station');
                } else {
                    return _tF('type_wigig_ptp', 'station');
                }
            case StationController::TYPE_58_AP:
                return _tF('type_58_ap', 'station');
            case StationController::TYPE_58_TG:
                return _tF('type_58_tg', 'station');
            case StationController::TYPE_52:
                return _tF('type_52', 'station');
            default:
                return '';
        }
    }

    /**
     * @return bool
     */
    public function getIsExpired()
    {
        return $this->status == static::STATUS_EXPIRED;
    }

    public function getTypeShortCut()
    {
        switch ($this->type) {
            case StationController::TYPE_FS:
                $name = _tF('type_fs', 'station');
                return "{$name}";
            case StationController::TYPE_WIGIG:
                return (($this->getStationTypeObject() && $this->getStationTypeObject()->is_ptmp) ?
                    _tF('type_wigig_ptmp', 'station') :
                    _tF('type_wigig_ptp', 'station'));
            case StationController::TYPE_58_AP:
                return _tF('type_58_ap', 'station');
            case StationController::TYPE_58_TG:
                return _tF('type_58_tg', 'station');
            case StationController::TYPE_52:
                return _tF('type_52', 'station');
            default:
                throw new Exception(_tF('Invalid station type', 'station'));
        }
    }

    /**
     * @return ActiveQuery
     */
    public function getStationPair()
    {
        return $this->hasOne(Station::class, ['id' => 'id_station_pair']);
    }

    /**
     * @return Station
     */
    public function getPairStation()
    {
        if ($this->isFs()) {
            if ($this->pointerToPairStation) {
                return $this->pointerToPairStation;
            }
            return $this->stationPair;
        }
        return null;
    }

    public function isFs(): bool
    {
        return ($this->type == StationController::TYPE_FS);
    }

    public function isMaster(): bool
    {
        return ($this->id_master == $this->id);
    }

    public function save($runValidation = true, $attributeNames = null)
    {
        deleteCommonCache(\api\versions\v1\controllers\StationController::CACHE_GEO_STATIONS);
        deleteCommonCache(\api\versions\v1\controllers\StationController::CACHE_LINES);

        // changing status to waiting if it was modified. but excepting publication
        if ($this->status == self::STATUS_FINISHED
            && (array_intersect(array_keys($this->getDirtyAttributes()), self::$dangerousAttributes)
                || $this->channelWidthDownWigig()
                || $this->channelWidthChangedFs()
            )
        ) {
            $this->status = self::STATUS_WAITING;

            if ($this->isFs()) {
                $this->getPairStation()->status = self::STATUS_WAITING;
            }
        }

        if ($this->isNewRecord) {
            parent::save($runValidation = true, $attributeNames = null);
        }
        $this->id_master = $this->type == StationController::TYPE_FS && $this->pair_position == self::POSITION_B ?
            $this->id_station_pair : $this->id;

        return parent::save($runValidation, $attributeNames);
    }

    public function channelWidthDownWigig(): bool
    {
        return $this->type == StationController::TYPE_WIGIG
            && ($this->getOldAttribute('channel_width') > $this->channel_width);
    }

    public function channelWidthChangedFs(): bool
    {
        return $this->type == StationController::TYPE_FS
            && ($this->getOldAttribute('channel_width') != $this->channel_width);
    }

    public function moveToWaiting(): bool
    {
        if ($this->status != self::STATUS_WAITING) {
            return $this->moveToStatus(self::STATUS_WAITING);
        }
        return false;
    }

    public function moveToFinished(): bool
    {
        if (
            (self::$statusOrder[$this->status] < self::$statusOrder[self::STATUS_FINISHED])
            ||
            ($this->isForProlongation())
        ) {
            $this->prolongRegistration();
            return $this->moveToStatus(self::STATUS_FINISHED);
        }
        return false;
    }

    public function moveToUnpublished(): bool
    {
        $this->setScenario(StationController::SCENARIO_UNPUBLISH);
        if ($this->status == self::STATUS_FINISHED) {
            $this->storeStationProgress(StationController::STEP_1_NAME);
            $this->published_by = null;
            return $this->moveToStatus(self::STATUS_UNPUBLISHED);
        }
        Yii::error('Cannot unpublish: ', $this->getErrors());
        return false;
    }

    public function moveToStatus(string $status): bool
    {
        $oldStatus = $this->status;
        $this->status = $status;
        $this->statusChange = "{$oldStatus}-{$this->status}";

        $ret = $this->save();

        if ($this->isFs() && $this->id_master == $this->id) {
            $pair = $this->getPairStation();
            $pair->setScenario($this->getScenario());
            $ret &= $pair->moveToStatus($status);
        }
        return $ret;
    }

    public function isWaiting(): bool
    {
        return $this->status == self::STATUS_WAITING;
    }

    public function isDraft(): bool
    {
        return $this->status == self::STATUS_DRAFT;
    }

    public function isPublished(): bool
    {
        return $this->status == self::STATUS_FINISHED;
    }

    public function isUnpublished(): bool
    {
        return $this->status == self::STATUS_UNPUBLISHED;
    }

    /**
     * returns true if this stations has some conflicts around
     * @return bool
     */
    public function hasConflictsWithOthers(&$conflictStations = [])
    {
// Code prepared here for debugging only
//        if (YII_ENV_DEV && YII_DEBUG) {
//            $conflictStations[] = Station::findOne(3);
//            $conflictStations[] = Station::findOne(8);
//            return true;
//        }

        $search = new StationSearch();

        switch ($this->type) {
            case StationController::TYPE_WIGIG:
            case StationController::TYPE_FS:
                $closeStations = $search->searchCloseStations($this, true);
                break;
            case StationController::TYPE_58_AP:
                $closeStations = $search->searchTollGateVicinity($this, true);
                break;
            case StationController::TYPE_52:
            case StationController::TYPE_58_TG:
                return false;
            default:
                $closeStations = null;
        }

        foreach ($closeStations as $station) {
            /**
             * @var $station Station
             */
            if ($station->hasConflicts) {
                $conflictStations[] = $station;

                return true;
            }
        }
        return false;
    }

    public function toBePublished(): bool
    {
        return $this->isWaiting() && !$this->hasConflicts;
    }

    public static function deleteStation($id)
    {
        $model = Station::findOne(['id' => $id]);
        if ($model->isFs()) {
            $pair = StationSearch::getModelsFs($model->id_master);
            return $pair->stationA->delete() && $pair->stationB->delete();
        } else {
            // check the wigig pair
            if ($model->stationPair) {
                $model->id_station_pair = null;
                $model->saveMyPartner();
            }
            return $model->delete();
        }
    }

    public function isForProlongation(): bool
    {
        if (!$this->canEdit() || !in_array($this->status, [self::STATUS_FINISHED, self::STATUS_EXPIRED])) {
            return false;
        }

        $validWithoutTime = (new Date())->setTimestamp($this->valid_to)->setTime(0, 0)->getTimestamp();
        $timeWarningReg = strtotime("+" . c('REGISTRATION_PERIOD_WARNING') . " months", time());
        return ($validWithoutTime < $timeWarningReg);
    }

    /**
     * Shortcut to DDtoDMS_static()
     *
     * @param $lat - specify which coordinate to format
     *
     * @return array - array of gps in DMS format
     */
    public function DDtoDMS($lat): array
    {
        $lat ? $dec = $this->lat : $dec = $this->lng;

        return static::DDtoDMS_static($dec);
    }

    /**
     * Converts decimal format to DMS ( Degrees / minutes / seconds )
     *
     * @param $dec - specify coordinate value
     *
     * @return array - array of gps in DMS format
     */
    public static function DDtoDMS_static($dec): array
    {
        $deg = floor($dec);
        $temp = $dec - $deg;

        $temp = $temp * 3600;
        $min = floor($temp / 60);
        $sec = $temp - ($min * 60);

        return [
            "deg" => $deg,
            "min" => $min,
            "sec" => $sec,
        ];
    }

    /**
     * @param $lat - specify which coordinate to format
     *
     * @return string - Formatted coordinate into DMS format
     */
    public function gpsFormatted($lat): string
    {
        $gps = $lat ? $this->lat : $this->lng;

        return number_format((float)$gps, 8, '.', '') . "°";
    }

    public function showInterference($newStationType, $oldStationType, $situation = 'passive')
    {
        if (!$this->overlapping) {
            return false;
        }

        if ($situation == 'general') {
            if ($newStationType == StationController::TYPE_WIGIG && $oldStationType == StationController::TYPE_WIGIG) {
                return false;
            }

            return true;
        }

        if ($situation == 'passive') {
            return !(($newStationType == StationController::TYPE_FS &&
                $oldStationType == StationController::TYPE_WIGIG));
        } else {
            return !(($newStationType == StationController::TYPE_WIGIG &&
                $oldStationType == StationController::TYPE_FS));
        }
    }

    public function setMacAddress($value)
    {
        if ($this->hardware_identifier == self::MAC_ADDRESS) {
            $this->serial_number = null;
            $this->mac_address = $value;
        }
    }

    public function getMacAddress()
    {
        return $this->mac_address;
    }

    public function setSerialNumber($value)
    {
        if ($this->hardware_identifier == self::SERIAL_NUMBER) {
            $this->mac_address = null;
            $this->serial_number = $value;
        }
    }

    public function getSerialNumber()
    {
        return $this->serial_number;
    }

    public function delete()
    {
        $out = true;
        $this->setScenario(StationController::SCENARIO_DELETE);

        // unlink pairs if wiwig
        if ($this->type == self::COMPARE_TYPE_WIGIG) {
            foreach (Station::findAll(['id_station_pair' => $this->id]) as $s) {
                $s->id_station_pair = null;
                $out &= $s->save(true, ['id_station_pair']);
            }
        }

        $stationType = $this->getStationTypeObject();
        if ($stationType !== null) {
            $stationType->setScenario(StationController::SCENARIO_DELETE);
            $out &= $stationType->delete();
        }

        $out &= self::deleteModels($this->getThreads()->all());
        $out &= self::deleteModels($this->getDisturbingThreads()->all());

        return $out && parent::delete();
    }

    public function isReadyToBePublished()
    {
        $paramsCheck = true;
        if ($this->isFs()) {
            $paramsCheck = $this->antenna_volume != null && $this->channel_width != null;
        }
        return (($this->status == self::STATUS_WAITING ||
                $this->status == self::STATUS_FINISHED ||
                $this->status == self::STATUS_EXPIRED) && $paramsCheck);
    }

    /**
     * Generates session key for progress saving using unique station ID.
     *
     * @return string
     */
    protected function stationProgressKey(): string
    {
        return "station{$this->id}Session";
    }

    /**
     * Tries to load session data containing number of last visited step in station creation process.
     *
     * @return int - step number, or (default) first step if session data unavailable
     */
    public function loadStationProgress(): int
    {
        $sessionKey = $this->stationProgressKey();
        if ($data = Yii::$app->session->get($sessionKey)) {
            $unSerialized = unserialize($data);
            if ($unSerialized['timeout'] >= (new DateTime())) {
                return $unSerialized['data'];
            }
        }
        return StationController::STEP_1_NAME;
    }

    /**
     * Stores session data containing number of last visited step in station creation process.
     *
     * @param $step - actual step number
     *
     */
    public function storeStationProgress($step)
    {
        $expirationTime = (new DateTime())->modify("+4 hour");

        $toStore = ['timeout' => $expirationTime, 'data' => $step];
        $sessionKey = $this->stationProgressKey();

        Yii::$app->session->set($sessionKey, serialize($toStore));
    }

    /**
     * Checks if provided MAC address is unique against other records
     *
     * @param string $value
     *
     * @return bool
     */
    public function isMacAddressUnique($value, &$duplicitingStationId = null)
    {
        if (empty($value)) {
            return true;
        }

        $q = static::find()
            ->select('id')
            ->where(['mac_address' => $value])
            ->andWhere(['in', 'status', [
                self::STATUS_FINISHED
            ]]);

        switch ($this->type) {
            case StationController::TYPE_WIGIG:
            case StationController::TYPE_FS:
                $q->andWhere([
                    'type' => [
                        StationController::TYPE_WIGIG,
                        StationController::TYPE_FS,
                    ],
                ]);
                break;
            case StationController::TYPE_52:
                $q->andWhere([
                    'type' => [
                        StationController::TYPE_52,
                    ],
                ]);
                break;
            case StationController::TYPE_58_AP:
            case StationController::TYPE_58_TG:
                $q->andWhere([
                    'type' => [
                        StationController::TYPE_58_AP,
                        StationController::TYPE_58_TG,
                    ],
                ]);
                break;
        }

        if ($this->type == StationController::TYPE_FS && $this->stationPair && $this->stationPair->mac_address) {
            if ($this->mac_address === $this->stationPair->mac_address) {
                $duplicitingStationId = $this->stationPair->id ?? null;
                return false;
            }
        }


        $records = $q->asArray()->all();

        // duplicity is allowed only between types, so if we find some duplicate records, its bad, except one case when its update

        if (count($records) == 0) { // No found duplicities
            return true;
        } elseif (count($records) == 1) {
            if ($records[0]['id'] == $this->id) { // If updating record
                return true;
            }
            if ($this->type == StationController::TYPE_FS && $this->stationPair && $this->stationPair->mac_address) { // If updating record
                if ($records[0]['id'] == $this->stationPair->id) {
                    return true;
                }
            }
        }

        $duplicitingStationId = $records[0]['id'] ?? null;

        // Otherwise
        return false;
    }

    /**
     * Checks if provided Serial number is unique against other records
     *
     * @param string $value
     *
     * @return bool
     */
    public function isSerialNumberUnique($value)
    {
        $recordWithProvidedValue = static::find()
            ->where(['serial_number' => $value])
            ->select('id')
            ->asArray()
            ->one();

        return $recordWithProvidedValue ? $recordWithProvidedValue['id'] == $this->id : true;
    }

    public static function getColumnsForExport()
    {
        return [
            [
                'attribute' => 'id',
                'label' => 'ID stanice',
            ],
            [
                'attribute' => 'userId',
                'label' => 'ID uzivatele',
            ],
            [
                'label' => 'akce',
                'value' => function ($model) {
                    return '';
                },
            ],
            [
                'label' => 'GPS LAT stupne',
                'value' => function ($model) {
                    return static::DDtoDMS_static($model['lat'])['deg'] ?? '';
                },
            ],
            [
                'label' => 'GPS LAT minuty',
                'value' => function ($model) {
                    return static::DDtoDMS_static($model['lat'])['min'] ?? '';
                },
            ],
            [
                'label' => 'GPS LAT vteriny',
                'value' => function ($model) {
                    return round(static::DDtoDMS_static($model['lat'])['sec'] ?? '', 2);
                },
            ],
            [
                'label' => 'GPS LON stupne',
                'value' => function ($model) {
                    return static::DDtoDMS_static($model['lng'])['deg'] ?? '';
                },
            ],
            [
                'label' => 'GPS LON minuty',
                'value' => function ($model) {
                    return static::DDtoDMS_static($model['lng'])['min'] ?? '';
                },
            ],
            [
                'label' => 'GPS LON vteriny',
                'value' => function ($model) {
                    return round(static::DDtoDMS_static($model['lng'])['sec'] ?? '', 2);
                },
            ],
            [
                'attribute' => 'mac_address',
                'label' => 'MAC',
                'value' => function ($model) {
                    return strtoupper($model['mac_address']);
                },
            ],
            [
                'attribute' => 'serial_number',
                'label' => 'výr.číslo',
                'value' => function ($model) {
                    return strtoupper($model['serial_number']);
                },
            ],
            [
                'attribute' => 'name',
                'label' => 'Název Stanice',
                'value' => function ($model) {
                    if (($model['type'] == StationController::TYPE_FS)) {
                        $letter = strtoupper($model['pair_position']);
                        return "{$model['name']} {$letter}";
                    }
                    return $model['name'];
                },
            ],
            [
                'label' => 'Výška nad mořem [m]',
                'value' => function ($model) {
                    return '';
                },
            ],
            [
                'attribute' => 'type',
                'label' => 'Typ stanice (flag FS_PtP, WiGig_PtP, WiGig_PtMP)',
                'value' => function ($model) {
                    $tmp = static::getStaticTypeName($model['type'], $model['id'],
                        (object)['is_ptmp' => $model['is_ptmp']]);
                    return str_replace(' ', '_', $tmp);
                },
            ],
            [
                'label' => 'Hlavni smer vyzarovani [stupne]',
                'value' => function ($model) {
                    $result = $model['direction'] ?? '';
                    return is_numeric($result) ? floatval($result) : '';
                },
            ],
            [
                'label' => 'Kmitocet f [MHz]',
                'value' => function ($model) {
                    $result = $model['frequency'] ?? '';
                    return is_numeric($result) ? floatval($result) : '';
                },
            ],

            [
                'attribute' => 'channel_width',
                'label' => 'sirka kanalu [MHz]',
                'value' => function ($model) {
                    $result = $model['channel_width'];
                    return is_numeric($result) ? floatval($result) : '';
                },
            ],

            [
                'label' => 'EIRP [dBm]; pro FS PtP se neuvadi',
                'value' => function ($model) {
                    if ($model['type'] != Station::COMPARE_TYPE_FS) {
                        $result = $model['antenna_volume'] + $model['power'];
                        return is_numeric($result) ? floatval($result) : '';
                    }
                    return '';
                },
            ],

            [
                'attribute' => 'antenna_volume',
                'label' => 'Zisk anteny [dBi]; pro WiGig se neuvadi',
                'value' => function ($model) {
                    if ($model['type'] != Station::COMPARE_TYPE_WIGIG) {
                        $result = $model['antenna_volume'];
                        return is_numeric($result) ? floatval($result) : '';
                    }
                    return '';
                },
            ],

            [
                'attribute' => 'power',
                'label' => 'vykon FS PtP [dB] (u WiGig se neuvadi)',
                'value' => function ($model) {
                    if ($model['type'] != Station::COMPARE_TYPE_WIGIG) {
                        $result = $model['power'];
                        return is_numeric($result) ? floatval($result) : '';
                    }
                    return '';
                },
            ],

            [
                'attribute' => 'registered_at',
                'label' => 'Datum REGISTROVANO',
                'value' => function ($model) {
                    return Yii::$app->formatter->asDate($model['registered_at'], "php:d/m/Y");
                },
            ],

            [
                'attribute' => 'valid_to',
                'label' => 'Datum PLATNE DO',
                'value' => function ($model) {
                    return Yii::$app->formatter->asDate($model['valid_to'], "php:d/m/Y");
                },
            ],

            [
                'attribute' => 'protected_to',
                'label' => 'Datum CHRANENO',
                'value' => function ($model) {
                    return Yii::$app->formatter->asDate($model['protected_to'], "php:d/m/Y");
                },
            ],

            [
                'attribute' => 'published_by',
                'label' => 'Publikování',
                'value' => function ($model) {
                    return _t($model['published_by'], 'station');
                },
            ],

            [
                'attribute' => 'status',
                'label' => 'Status koordinace (Stav)',
                'value' => function ($model) {
                    return _t("status_{$model['status']}", 'station');
                },
            ],

            [
                'label' => 'por. c. protistanice (jen pro par FS PtP)',
                'value' => function ($model) {
                    if ($model['type'] == Station::COMPARE_TYPE_FS) {
                        return $model['id_station_pair'];
                    }
                    return '';
                },
            ],

            [
                'label' => 'pozadovany pomer C/I [dB]',
                'value' => function ($model) {
                    $result = $model['ratio_signal_interference'] ?? '';
                    return is_numeric($result) ? floatval($result) : '';
                },
            ],

            [
                'label' => 'Neobsazený příznak pro budoucnost (string)',
                'value' => function ($model) {
                    return '';
                },
            ],

            // User
            [
                'attribute' => 'userId',
                'label' => 'ID uzivatele',
            ],
            [
                'attribute' => 'userName',
                'label' => 'Jmeno / Jmeno firmy',
            ],
            [
                'attribute' => 'userEmail',
                'label' => 'Email',
            ],
            [
                'attribute' => 'userType',
                'label' => 'Typ (osobni/firemni)',
                'value' => function ($model) {
                    return $model['userType'] == User::USER_TYPE_COMPANY ? 'firemni' : 'osobni';
                },
            ],
            [
                'attribute' => 'userRole',
                'label' => 'Role',
//                'value' => function ($model)
//                {
//                    return $model['user']['userRole']['name'];
//                },
            ],
            [
                'attribute' => 'userVatNumber',
                'label' => 'ICO',
            ],
            [
                'attribute' => 'userTaxNumber',
                'label' => 'DIC',
            ],
            [
                'attribute' => 'userStreet',
                'label' => 'Ulice',
            ],
            [
                'attribute' => 'userCity',
                'label' => 'Mesto',
            ],
            [
                'attribute' => 'userZipCode',
                'label' => 'PSC',
            ],
            [
                'attribute' => 'userAdditionalInfo',
                'label' => 'Dodatocne info',
            ],
            [
                'attribute' => 'userCreated_at',
                'label' => 'Vytvoren',
                'value' => function ($model) {
                    return \Yii::$app->formatter->asDate($model['userCreatedAt'], "php:d/m/Y");
                },
            ],
        ];
    }

    /**
     * Create stations excel export into file
     *
     * @param string $domain
     * @param string $fileType
     * @param QueryInterface $query
     *
     * @return string Exported file link
     * @throws \Box\Spout\Common\Exception\IOException
     * @throws \Box\Spout\Writer\Exception\WriterNotOpenedException
     */
    public static function exportAll(string $domain, string $fileType, QueryInterface $query)
    {
        //
        // Prepare data

        $columns = static::getColumnsForExport();
        $query->select([
            'station.*',

            'station_wigig.direction',
            'station_wigig.is_ptmp',
            'station_wigig.is_ptmp',

            'station_fs.ratio_signal_interference',
            'station_fs.frequency',

            'user.id AS userId',
            'TRIM(  CONCAT(  IFNULL(first_name,""), " ", IFNULL(last_name,""), " ", IFNULL(companyName,"") )) AS userName',
            'user.email AS userEmail',
            'user.type AS userType',
            'user_role.name AS userRole',
            'user.vatNumber AS userVatNumber',
            'user.taxNumber AS userTaxNumber',
            'user.street AS userStreet',
            'user.city AS userCity',
            'user.zipCode AS userZipCode',
            'user.additionalInfo AS userAdditionalInfo',
            'user.created_at AS userCreatedAt',
        ])
            ->joinWith('stationWigig', false)
            ->joinWith('stationFs', false)
            ->joinWith('station58', false)
            ->joinWith('user', false)
            ->joinWith('user.userRole', false)
            ->asArray();

        // Prepare output file and writer
        $filename = "stations_" . date('Y-m-d_His') . "_" . Yii::$app->security->generateRandomString(6) . '.' . mb_strtolower($fileType);
        $filepath = \Yii::getAlias('@uploads') . '/exports/' . $filename;
        if (file_exists($filepath)) {
            unlink($filepath);
        }

        if ($fileType == LogArchiveComponent::FILE_TYPE_XLSX) {
            $writer = WriterEntityFactory::createXLSXWriter();
        } else {
            $writer = WriterEntityFactory::createCSVWriter();
        }
        $writer->openToFile($filepath);

        //
        // Add header row

        $headerCols = [];
        foreach ($columns as $col) {
            if (isset($col['label'])) {
                $label = $col['label'];
            } else {
                $label = (new \admin\models\search\StationSearch)->getAttributeLabel($col['attribute']);
            }
            $headerCols[] = $label;
        }
        $writer->addRow(WriterEntityFactory::createRowFromArray($headerCols));

        //
        // Add stations rows

        foreach ($query->each(5000) as $model) {
            $values = [];
            foreach ($columns as $col) {
                if (isset($col['value'])) {
                    $value = $col['value']($model);
                } else {
                    $value = $model[$col['attribute']];
                }
                $values[] = $value;
            }

            $writer->addRow(WriterEntityFactory::createRowFromArray($values));
        }

        $writer->close();

        // Remove file after time
        Yii::$app->queue->delay(env('EXPORTS_TTL_MIN', 30) * 60)->push(new RemoveExportFileJob([
            'filePath' => $filepath,
        ]));

        return $domain . "/uploads/exports/{$filename}";
    }

    /**
     * Save my wigig partner
     *
     * @return bool
     * @throws Exception
     */
    public function saveMyPartner($savingPartnerStatus = self::STATUS_WAITING)
    {
        // Check WIGIG pair
        if (!$this->isFs()) {
            if ($this->id_station_pair !== null) {
                // Check my partner. It must be close and be still mine, Wigig and Published
                $partner = $this->stationPair;
                if ($partner->isFs() ||
                    $partner->status != Station::STATUS_FINISHED ||
                    ($partner->id_station_pair != null && $partner->id_station_pair != $this->id) ||
                    $partner->id_user != $this->id_user ||
                    self::countDistance($this->lat, $this->lng, $partner->lat, $partner->lng) > 10) {

                    return false;
                } else {
                    // Save partner to couple with myself
                    $partner->id_station_pair = $this->id;
                    /** @var StationWigig $partnerTypeObj */
                    $partnerTypeObj = $partner->getStationTypeObject();
                    $thisDirection = ($this->getStationTypeObject()->direction - 180);
                    $partnerTypeObj->direction = ($thisDirection) < 0 ? $thisDirection + 360 : $thisDirection;
                    $partnerTypeObj->setAngleDirections();
                    $this->valid_to = $partner->valid_to;
                    $this->protected_to = $partner->protected_to;

                    // Move partner to waiting state
                    return ($partnerTypeObj->save() && $partner->moveToStatus($savingPartnerStatus));
                }
            } else {
                // Set the partner "free"
                /* @var $station Station */
                foreach (Station::find()->andWhere(['id_station_pair' => $this->id])->each() as $station) {
                    $station->id_station_pair = null;

                    $station->save();
                    $station->moveToStatus(self::STATUS_FINISHED);
                }
                return true;
            }
        }
        return true;
    }

    public function getNamePlaceholder()
    {
        switch ($this->type) {
            case StationController::TYPE_FS:
            case StationController::TYPE_WIGIG:
            case StationController::TYPE_52:
                return _tF('untitled station', 'station');
            case StationController::TYPE_58_AP:
                return _tF('untitled access point', 'station');
            case StationController::TYPE_58_TG:
                return _tF('untitled toll gate', 'station');
            default:
                throw new Exception(_tF('Invalid station type', 'station'));
        }
    }

    public function getStationName()
    {

        switch ($this->type) {
            case StationController::TYPE_FS:
            case StationController::TYPE_WIGIG:
            case StationController::TYPE_58_TG:
            case StationController::TYPE_52:
                return $this->name ?? $this->getNamePlaceholder();
            case StationController::TYPE_58_AP:
                $modelType = $this->getStationTypeObject();
                return $this->name ?? $this->getNamePlaceholder();
            default:
                return $this->getNamePlaceholder();
        }
    }

    public function changeType58to52()
    {
        if ($this->type == StationController::TYPE_58_AP) {
            $transaction = Yii::$app->db->beginTransaction();

            $objectType = $this->getStationTypeObject();
            if (is_null($objectType) || !$objectType->delete()) {
                $transaction->rollBack();
                return false;
            }

            $objectType = new Station52();
            $objectType->setChecksum();

            $objectType->id_station = $this->id;
            $this->type = StationController::TYPE_52;
            $this->initRegistrations(false);

            if ($this->save(true, ['type', 'valid_to', 'protected_to']) && $objectType->save()) {
                $transaction->commit();
                return true;
            } else {
                $transaction->rollBack();
            }
        }

        return false;
    }

    public function getWaitingApiChangesAsArray()
    {
        $result = [];
        if (!empty($this->waiting_api_changes_json)) {
            $result = Json::decode($this->waiting_api_changes_json);
        }

        return $result;
    }

    public function hasWaitingApiChanges()
    {
        return !empty($this->getWaitingApiChangesAsArray());
    }
}
