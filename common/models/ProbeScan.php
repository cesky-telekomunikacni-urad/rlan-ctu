<?php
/*
* Copyright 2023 DactylGroup s.r.o.
*
* Licensed under the EUPL, Version 1.2 or – as soon they
will be approved by the European Commission - subsequent
versions of the EUPL (the "Licence");
* You may not use this work except in compliance with the
Licence.
* You may obtain a copy of the Licence at:
*
* https://joinup.ec.europa.eu/software/page/eupl5
*
* Unless required by applicable law or agreed to in
writing, software distributed under the Licence is
distributed on an "AS IS" basis,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
express or implied.
* See the Licence for the specific language governing
permissions and limitations under the Licence.
*/
?>


<?php

namespace common\models;

use yii\behaviors\TimestampBehavior;
use yii\db\ActiveRecord;

/**
 * This is the model class for table "probe_scan".
 *
 * @property integer $id
 * @property integer $id_probe
 * @property string $address
 * @property string $time
 * @property integer $created_at
 * @property integer $updated_at
 *
 * @property ProbeScanWlan[] $probeScanWlans
 * @property Probe[] $probe
 */
class ProbeScan extends \dactylcore\core\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName(): string
    {
        return 'probe_scan';
    }

    /**
     * @inheritdoc
     */
    public function rules(): array
    {
        return [
            [['address', 'time', 'id_probe'], 'required'],
            [['created_at', 'updated_at'], 'integer'],
            [['address'], 'string', 'max' => 512],
            [['time'], 'string', 'max' => 19],
            [['deleted'], 'default', 'value' => 0],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels(): array
    {
        return [
            'id' => _tF('id', 'probe'),
            'id_probe' => _tF('probe', 'probe'),
            'address' => _tF('address', 'probe'),
            'time' => _tF('time', 'probe'),
            'created_at' => _tF('created_at', 'probe'),
            'updated_at' => _tF('updated_at', 'probe'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getProbe(): \yii\db\ActiveQuery
    {
        return $this->hasOne(Probe::class, ['id' => 'id_probe']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getProbeScanWlans(): \yii\db\ActiveQuery
    {
        return $this->hasMany(ProbeScanWlan::class, ['id_probe_scan' => 'id']);
    }

    public function behaviors(): array
    {
        return [
            [
                'class' => TimestampBehavior::class,
                'attributes' => [
                    ActiveRecord::EVENT_BEFORE_INSERT => ['created_at', 'updated_at'],
                    ActiveRecord::EVENT_BEFORE_UPDATE => ['updated_at'],
                ],
            ],
        ];
    }
}
