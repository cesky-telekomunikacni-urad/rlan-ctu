<?php
/*
* Copyright 2023 DactylGroup s.r.o.
*
* Licensed under the EUPL, Version 1.2 or – as soon they
will be approved by the European Commission - subsequent
versions of the EUPL (the "Licence");
* You may not use this work except in compliance with the
Licence.
* You may obtain a copy of the Licence at:
*
* https://joinup.ec.europa.eu/software/page/eupl5
*
* Unless required by applicable law or agreed to in
writing, software distributed under the Licence is
distributed on an "AS IS" basis,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
express or implied.
* See the Licence for the specific language governing
permissions and limitations under the Licence.
*/
?>


<?php

namespace common\models;

use dactylcore\core\helpers\Date;
use modules\user\common\models\User;
use Yii;

/**
 * @property int $id
 * @property int $id_recipient
 * @property int $id_sender
 * @property int $id_station
 * @property int $id_station_disturber
 * @property string $reason
 * @property string $subject
 * @property string $status_sender
 * @property string $status_recipient
 * @property int $deleted
 * @property bool $show_sender_info
 * @property bool $show_recipient_info
 * @property int $created_at
 * @property int $updated_at
 *
 * @property Message[] $messages
 * @property User $recipient
 * @property User $sender
 * @property Station $station
 * @property Station $stationDisturber
 */
class Thread extends \dactylcore\core\db\ActiveRecord
{

    const STATUS_NEW = 'new';
    const STATUS_READ = 'read';
    const STATUS_ARCHIVED = 'archived';

    const REASON_GENERIC = 'generic';
    const REASON_CONFLICT = 'conflict';

    const ROLE_SENDER = 'sender';
    const ROLE_RECIPIENT = 'recipient';

    public $status = self::STATUS_NEW;
    public $reason = self::REASON_GENERIC;

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return merge(parent::rules(), [
            [
                [
                    'id_recipient',
                    'id_sender',
                    'id_station',
                    'id_station_disturber',
                    'deleted',
                    'created_at',
                    'updated_at',
                ],
                'integer',
            ],
            [['id_station'], 'required'],
            [['show_sender_info', 'show_recipient_info'], 'boolean'],
            [['reason', 'subject', 'status_sender', 'status_recipient'], 'string', 'max' => 255],
            [
                ['id_recipient'],
                'exist',
                'skipOnError' => true,
                'targetClass' => User::class,
                'targetAttribute' => ['id_recipient' => 'id'],
            ],
            [
                ['id_sender'],
                'exist',
                'skipOnError' => true,
                'targetClass' => User::class,
                'targetAttribute' => ['id_sender' => 'id'],
            ],
            [
                ['id_station'],
                'exist',
                'skipOnError' => true,
                'targetClass' => Station::class,
                'targetAttribute' => ['id_station' => 'id'],
            ],
            [
                ['id_station_disturber'],
                'exist',
                'skipOnError' => true,
                'targetClass' => Station::class,
                'targetAttribute' => ['id_station_disturber' => 'id'],
            ],
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return merge(parent::attributeLabels(), [
            'id' => _tF('id', 'messaging'),
            'id_recipient' => _tF('id_recipient', 'messaging'),
            'id_sender' => _tF('id_sender', 'messaging'),
            'id_station' => _tF('id_station', 'messaging'),
            'id_station_disturber' => _tF('id_station_disturber', 'messaging'),
            'reason' => _tF('reason', 'messaging'),
            'subject' => _tF('subject', 'messaging'),
            'status_sender' => _tF('status', 'messaging'),
            'status_recipient' => _tF('status', 'messaging'),
            'deleted' => _tF('deleted', 'messaging'),
            'created_at' => _tF('created_at', 'messaging'),
            'updated_at' => _tF('updated_at', 'messaging'),
            'show_sender_info' => _tF('show_sender_info', 'messaging'),
            'show_recipient_info' => _tF('show_recipient_info', 'messaging'),
        ]);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getMessages()
    {
        return $this->hasMany(Message::class, ['id_thread' => 'id']);
    }

    public function getLatestMessageContent() {
        $messages = $this->messages;
        $message = end($messages);
        return strip_tags($message->content);
    }

    public function getLatestMessageTimeFormatted() {
        $messages = $this->messages;
        $message = end($messages);
        return $message->getDateTimeFormatted();
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getRecipient()
    {
        return $this->hasOne(User::class, ['id' => 'id_recipient']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getSender()
    {
        return $this->hasOne(User::class, ['id' => 'id_sender']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getStation()
    {
        return $this->hasOne(Station::class, ['id' => 'id_station']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getStationDisturber()
    {
        return $this->hasOne(Station::class, ['id' => 'id_station_disturber']);
    }

    public static function findUnreadThreads()
    {
        $userId = Yii::$app->getUser()->id;
        $out = Thread::find()
                     ->rightJoin('message', 'message.id_thread = thread.id')
                     ->where([
                         'or',
                         ['id_recipient' => $userId, 'status_recipient' => Thread::STATUS_NEW],
                         ['id_sender' => $userId, 'status_sender' => Thread::STATUS_NEW],
                     ])
                     ->count();
        return $out;
    }

    public function isNewForMe(): bool
    {
        return $this->isForMe(self::STATUS_NEW);

    }

    public function isReadForMe(): bool
    {
        return $this->isForMe(self::STATUS_READ);
    }

    public function isReadForOppositeUser(): bool
    {
        if ($this->iAm() == self::ROLE_RECIPIENT) {
            return $this->status_sender == self::STATUS_READ;
        } else {
            return $this->status_recipient == self::STATUS_READ;
        }
    }

    public function isArchivedForMe(): bool
    {
        return $this->isForMe(self::STATUS_ARCHIVED);
    }

    public function isForMe($status)
    {
        if ($this->iAm() == self::ROLE_RECIPIENT) {
            return $this->status_recipient == $status;
        } else {
            return $this->status_sender == $status;
        }
    }

    public function statusForMe()
    {
        if ($this->iAm() == self::ROLE_RECIPIENT) {
            return $this->status_recipient;
        } else {
            return $this->status_sender;
        }
    }

    public function getOppositeUsersName($idUser = null)
    {
        $id = 0;

        $idUser = $idUser ?: Yii::$app->getUser()->id;

        if ($idUser == $this->id_sender) {
            if ($this->show_recipient_info) {
                return $this->recipient ? $this->recipient->getName() : _tF('user doesnt exist', 'messaging');
            } else {
                $id = $this->id_recipient;
            }
        }
        if ($idUser == $this->id_recipient) {
            if ($this->show_sender_info) {
                return $this->sender ? $this->sender->getName() : _tF('user doesnt exist', 'messaging');
            } else {
                $id = $this->id_sender;
            }
        }

        return _tF('anonymous user {id}', 'messaging', ['id' => $id]);
    }

    public function getOppositeUser($idUser)
    {
        if ($idUser == $this->id_sender) {
            return $this->recipient;
        } elseif ($idUser == $this->id_recipient) {
            return $this->sender;
        } else {
            throw new \Exception("user {$idUser} has no role in thread {$this->id}");

        }
    }

    public function getUsersInfo($idUser = null)
    {

        /**
         * @var $user User
         */
        $idUser = $idUser ?: Yii::$app->getUser()->id;

        if ($idUser == $this->id_sender) {
            if ($this->show_recipient_info) {
                $user = $this->recipient;
            } else {
                return [];
            }
        }
        if ($idUser == $this->id_recipient) {
            if ($this->show_sender_info) {
                $user = $this->sender;
            } else {
                return [];
            }
        }

        return ['email' => $user ? $user->email : ''];
    }

    public function iAm($idUser = null)
    {
        $idUser = $idUser ?: Yii::$app->getUser()->id;

        if ($idUser == $this->id_sender) {
            return self::ROLE_SENDER;
        } elseif ($idUser == $this->id_recipient) {
            return self::ROLE_RECIPIENT;
        } else {
            throw new \Exception("user {$idUser} has no role in thread {$this->id}");
        }
    }

    public function setAsRead($role = null)
    {
        $role = $role ?: $this->iAm();
        $statusTarget = "status_{$role}";
        $this->$statusTarget = self::STATUS_READ;
        return $this->save();
    }

    public function setAsNew($role = null)
    {
        $role = $role ?: $this->iAm();
        $statusTarget = "status_{$role}";
        $this->$statusTarget = self::STATUS_NEW;
        return $this->save();
    }

    public function delete()
    {
        $msgs = $this->getMessages()->all();
        return self::deleteModels($msgs) && parent::delete();
    }

    public function getOppositeRole()
    {
        $idUser = Yii::$app->getUser()->id;

        if ($idUser == $this->id_sender) {
            return self::ROLE_RECIPIENT;
        } elseif ($idUser == $this->id_recipient) {
            return self::ROLE_SENDER;
        } else {
            throw new \Exception("user {$idUser} has no role in thread {$this->id}");
        }
    }

    public static function getPaddedId($id, $padLength = 6, $padWith = '0', $padType = STR_PAD_LEFT) {
        return str_pad($id, $padLength, $padWith, $padType);
    }

    public function getOppositeUserIdPadded($padLength = 6, $padWith = '0', $padType = STR_PAD_LEFT) {
        $idUser = Yii::$app->getUser()->id;

        if ($idUser == $this->id_sender) {
            return  static::getPaddedId($this->id_recipient, $padLength, $padWith, $padType);
        } elseif ($idUser == $this->id_recipient) {
            return  static::getPaddedId($this->id_sender, $padLength, $padWith, $padType);
        } else {
            throw new \Exception("user {$idUser} has no role in thread {$this->id}");
        }
    }

    public function getOppositeUserIdentification() {
        $idUser = Yii::$app->getUser()->id;

        if ($idUser == $this->id_sender) {
            if ($this->show_recipient_info) {
                return $this->getOppositeUser($idUser)->email;
            }
            else {
                return $this->getOppositeUserIdPadded();
            }
        } elseif ($idUser == $this->id_recipient) {
            if ($this->show_sender_info) {
                return $this->getOppositeUser($idUser)->email;
            }
            else {
                return $this->getOppositeUserIdPadded();
            }
        } else {
            throw new \Exception("user {$idUser} has no role in thread {$this->id}");
        }

    }

}


