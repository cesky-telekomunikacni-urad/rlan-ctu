<?php
/*
* Copyright 2023 DactylGroup s.r.o.
*
* Licensed under the EUPL, Version 1.2 or – as soon they
will be approved by the European Commission - subsequent
versions of the EUPL (the "Licence");
* You may not use this work except in compliance with the
Licence.
* You may obtain a copy of the Licence at:
*
* https://joinup.ec.europa.eu/software/page/eupl5
*
* Unless required by applicable law or agreed to in
writing, software distributed under the Licence is
distributed on an "AS IS" basis,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
express or implied.
* See the Licence for the specific language governing
permissions and limitations under the Licence.
*/
?>


<?php

use dactylcore\menu\web\MenuItemType;

$commonConfig = [
    'name' => 'ČTÚ RLAN',
    'idProject' => 'rlan',
    'params' => require __DIR__ . '/params.php',
    'modules' => [
        'dc-menu' => [
            'class' => dactylcore\menu\Module::class,
        ],
        'dc-page' => [
            'class' => dactylcore\page\Module::class,
            'elements' => [
                'wysiwyg' => [
                    'class' => dactylcore\page\elements\wysiwyg\WysiwygElement::class,
                    'assetBundleClass' => frontend\assets\PageContentAsset::class,
                ],
            ]
        ],
    ],
    'bootstrap' => [
        function () {
            if (isAppConsole()) {
                return;
            }

            Yii::$app->getModule('dc-user')->restrictLoginForVerifiedUsersOnly = true;

            Yii::$app->getModule('dc-user')->getAccessManager()->addPermissions([
                'app-frontend' => [
                    'station' => [
                        'update',
                        'prolong-registration',
                        'update-all',
                        'delete',
                        'add_toll_gate',
                        'add_52ghz',
                        'add_access_point',
                    ],
                ]
            ]);

            // Add menu positions to the menu manager

            Yii::$app->getModule('dc-menu')->get('menuManager')->addPositions([
                1 => _tF('public-header', 'menu'),
                2 => _tF('protected-header', 'menu'),
                3 => _tF('footer menu', 'menu'),
            ]);

            Yii::$app->getModule('dc-menu')->get('menuManager')->addItemTypes([
                'logout' => Yii::createObject([
                    'class' => dactylcore\menu\web\MenuItemType::class,
                    'inputType' => MenuItemType::INPUT_TYPE_STATIC,
                    'name' => _tF('logout', 'menu_item'),
                    'data' => '/dc-user/user/logout',
                    'linkOptions' => ['data-method' => 'post']
                ]),
            ]);

            Yii::$app->getModule('dc-menu')->get('menuManager')->addItemTypes([
                'login' => Yii::createObject([
                    'class' => dactylcore\menu\web\MenuItemType::class,
                    'inputType' => MenuItemType::INPUT_TYPE_STATIC,
                    'name' => _tF('login', 'menu_item'),
                    'data' => '/dc-user/user/login',
                    'linkOptions' => ['data-livebox' => 1],
                ]),
            ]);

            Yii::$app->getModule('dc-menu')->get('menuManager')->addItemTypes([
                'register' => Yii::createObject([
                    'class' => dactylcore\menu\web\MenuItemType::class,
                    'inputType' => MenuItemType::INPUT_TYPE_STATIC,
                    'name' => _tF('register', 'menu_item'),
                    'data' => '/dc-user/user/register',
                    'linkOptions' => ['data-livebox' => 1],
                ]),
            ]);

            Yii::$app->getModule('dc-menu')->get('menuManager')->addItemTypes([
                'conversations' => Yii::createObject([
                    'class' => dactylcore\menu\web\MenuItemType::class,
                    'inputType' => MenuItemType::INPUT_TYPE_STATIC,
                    'name' => _tF('conversations', 'menu'),
                    'data' => 'messaging/index',
                    'linkOptions' => ['class' => 'dk--nav-bar--conversations']
                ]),
            ]);
            Yii::$app->getModule('dc-menu')->get('menuManager')->addItemTypes([
                'my-stations' => Yii::createObject([
                    'class' => dactylcore\menu\web\MenuItemType::class,
                    'inputType' => MenuItemType::INPUT_TYPE_STATIC,
                    'name' => _tF('create', 'menu'),
                    'data' => 'station/registration-stations',
                    'linkOptions' => ['class' => 'dk--nav-bar--create']
                ]),
            ]);
        }
    ],
    'components' => [
        'reCaptcha' => [
            'name' => 'reCaptcha',
            'class' => himiklab\yii2\recaptcha\ReCaptcha::class,
            'siteKey' => '6LdqEDUUAAAAAAllnigtBffLqiR7wvdnlG_Cx2U8',
            'secret' => '6LdqEDUUAAAAAFpZr-cE-1btaY1kV07AU2K8XQDG',
        ],
    ],
];

return $commonConfig;
