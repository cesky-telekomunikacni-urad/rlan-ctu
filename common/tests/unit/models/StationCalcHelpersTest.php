<?php
/*
* Copyright 2023 DactylGroup s.r.o.
*
* Licensed under the EUPL, Version 1.2 or – as soon they
will be approved by the European Commission - subsequent
versions of the EUPL (the "Licence");
* You may not use this work except in compliance with the
Licence.
* You may obtain a copy of the Licence at:
*
* https://joinup.ec.europa.eu/software/page/eupl5
*
* Unless required by applicable law or agreed to in
writing, software distributed under the Licence is
distributed on an "AS IS" basis,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
express or implied.
* See the Licence for the specific language governing
permissions and limitations under the Licence.
*/
?>


<?php

namespace common\tests\unit\models;

use Codeception\Test\Unit;
use common\models\Station;
use common\models\StationFs;
use common\models\StationWigig;
use Mockery\Mock;


class StationCalcHelpersTest extends Unit
{
    /**
     * @var \UnitTester
     */
    protected $tester;

    /**
     * @var $stationW Station|Mock
     */
    protected $stationW;

    /**
     * @var $stationW StationWigig|Mock
     */
    protected $wigig;

    /**
     * @var $fsA StationFs|Mock
     */
    protected $fsA;

    /**
     * @var $fsB StationFs|Mock
     */
    protected $fsB;

    /**
     * @var $stationB Station|Mock
     */
    protected $stationB;

    /**
     * @var $stationA Station|Mock
     */
    protected $stationA;

    protected $cDone = false;

    protected $fsData = [
        1520 => [
            'a' =>
                [
                    'id' => 15,
                    'lat' => 50.0421103835,
                    'lng' => 14.5599478483,
                    'antenna_volume' => 35,
                    'power' => 5,
                    'channel_width' => 200,
                    'type' => 'fs',
                    'fs' => [
                        'frequency' => 61000,
                        'specific_run_down' => 13,
                        'ratio_signal_interference' => 34,
                    ],
                    'out' => [
                        'passing' => true,
                        'diff' => -3
                    ]
                ]
            ,
            'b' =>
                [
                    'id' => 20,
                    'lat' => 50.0429338217,
                    'lng' => 14.5583224297,
                    'antenna_volume' => 40,
                    'power' => 5,
                    'channel_width' => 200,
                    'type' => 'fs',
                    'fs' => [
                        'frequency' => 61000,
                        'specific_run_down' => 13,

                        'ratio_signal_interference' => 34,
                    ],
                    'out' => [
                        'passing' => true,
                        'diff' => 0
                    ]
                ]
        ],
        1452 => [
            'a' =>
                [
                    'id' => 14,
                    'lat' => 50.042527467,
                    'lng' => 14.5598351955,
                    'antenna_volume' => 30,
                    'power' => 5,
                    'channel_width' => 500,
                    'type' => 'fs',
                    'fs' => [
                        'frequency' => 61000,
                        'specific_run_down' => 13,

                        'ratio_signal_interference' => 34,
                    ],
                    'out' => [
                        'passing' => false,
                        'diff' => 8.6
                    ]
                ]
            ,
            'b' =>
                [
                    'id' => 52,
                    'lat' => 50.0413835049,
                    'lng' => 14.5616215467,
                    'antenna_volume' => 30,
                    'power' => 5,
                    'channel_width' => 500,
                    'type' => 'fs',
                    'fs' => [
                        'frequency' => 61000,
                        'ratio_signal_interference' => 34,
                        'specific_run_down' => 13,

                    ],
                    'out' => [
                        'passing' => true,
                        'diff' => 0
                    ]
                ]
        ],
        612 => [
            'a' =>
                [
                    'id' => 6,
                    'lat' => 50.0413694233,
                    'lng' => 14.5586174726,
                    'antenna_volume' => 45,
                    'power' => 5,
                    'channel_width' => 200,
                    'type' => 'fs',
                    'fs' => [
                        'frequency' => 61000,
                        'specific_run_down' => 13,

                        'ratio_signal_interference' => 28,
                    ],
                    'out' => [
                        'passing' => true,
                        'diff' => -22
                    ]
                ]
            ,
            'b' =>
                [
                    'id' => 12,
                    'lat' => 50.0425066799,
                    'lng' => 14.5593953133,
                    'antenna_volume' => 43,
                    'power' => 10,
                    'channel_width' => 200,
                    'type' => 'fs',
                    'fs' => [
                        'frequency' => 61000,
                        'ratio_signal_interference' => 34,
                        'specific_run_down' => 13,

                    ],
                    'out' => [
                        'passing' => true,
                        'diff' => 0
                    ]
                ]
        ],
    ];

    protected function _before()
    {
        $this->prepareStationPair();
    }

    protected function prepareStationPair()
    {
        /**
         * @var $stationW Station|Mock
         */
        $this->stationW = \Mockery::mock(Station::class)->makePartial();
        $this->stationW->shouldReceive('hasAttribute')->andReturn(true);
        $this->wigig = \Mockery::mock(StationWigig::class)->makePartial();
        $this->wigig->shouldReceive('hasAttribute')->andReturn(true);

        $this->stationW->shouldReceive('getStationTypeObject')->andReturn($this->wigig);

        $this->stationA = \Mockery::mock(Station::class)->makePartial();
        $this->fsA = \Mockery::mock(StationFs::class)->makePartial();
        $this->fsA->shouldReceive('hasAttribute')->andReturn(true);
        $this->stationA->shouldReceive('getStationTypeObject')->andReturn($this->fsA);
        $this->stationA->shouldReceive('hasAttribute')->andReturn(true);

        $this->stationB = \Mockery::mock(Station::class)->makePartial();
        $this->fsB = \Mockery::mock(StationFs::class)->makePartial();
        $this->fsB->shouldReceive('hasAttribute')->andReturn(true);
        $this->stationB->shouldReceive('hasAttribute')->andReturn(true);

        $this->stationB->shouldReceive('getStationTypeObject')->andReturn($this->fsB);

        $this->stationA->stationPair = $this->stationB;
        $this->stationB->stationPair = $this->stationA;

        $this->initWigig();

    }

    protected function initWigig()
    {
        $this->stationW->lng = 14.559545517;
        $this->stationW->lat = 50.0424999744;
//        $this->stationW->antenna_volume = 22;
//        $this->stationW->power = 8;
        $this->stationW->channel_width = 2000;
        $this->stationW->type = 'wigig';

        $this->wigig->eirp = 40;
        $this->wigig->direction = 190;
//        $this->wigig->front_to_back_ratio = 25;
        $this->wigig->is_ptmp = true;
    }

    protected function makeFsFromConfig(&$stationFs, &$fs, $config)
    {
        foreach ($config as $key => $val) {
            if ($key != 'fs' || $key != 'out') {
                $stationFs->$key = $val;
            }
        }

        foreach ($config['fs'] as $key => $val) {
            $fs->$key = $val;
        }
    }

    public function testDistance()
    {
        $dist = Station::countDistance(49.276498, 16.599354, 49.289555, 16.589366);
        $this->assertEquals(1.6244, round($dist, 4));
    }

    public function testCountPhi()
    {
        $phi = Station::countAngleWigigFs(50.0425066799, 14.5593953133, 50.0413694233, 14.5586174726, 50.0420279056, 14.55914855);
        $this->assertEquals(7.1038, round($phi, 4));
    }

    public function testCountOmega()
    {
        $this->assertEquals(180, Station::countOmega(-3,-3,0,0, 45));
        $this->assertEquals(90, Station::countOmega(3,-3,0,0, 45));
        $this->assertEquals(90, Station::countOmega(3,-3,0,0, 45));
        $this->assertEquals(55, Station::countOmega(-3,3,0,0, 190));

        $this->assertEquals(125, Station::countOmega(-3,3,0,0, 10));
        $this->assertEquals(65, Station::countOmega(-3,3,0,0, 70));
//        $this->assertEquals(35, Station::countOmega(-3,3,0,0, 100));
        $this->assertEquals(35, Station::countOmega(-3,3,0,0, 170));
        $this->assertEquals(55, Station::countOmega(-3,3,0,0, 190));
        $this->assertEquals(90, Station::countOmega(-3,3,0,0, 225));
        $this->assertEquals(125, Station::countOmega(-3,3,0,0, 260));
        $this->assertEquals(145, Station::countOmega(-3,3,0,0, 280));
        $this->assertEquals(145, Station::countOmega(-3,3,0,0, 350));
    }

    public function testCountAdPhiWigig()
    {
        $this->assertEquals(0, Station::countAdPhiWigig(0.24, false));
        $this->assertEquals(1, Station::countAdPhiWigig(0.36, false));
        $this->assertEquals(11, Station::countAdPhiWigig(7.2, false));
        $this->assertEquals(14, Station::countAdPhiWigig(8, false));
        $this->assertEquals(17, Station::countAdPhiWigig(30, false));
        $this->assertEquals(21, Station::countAdPhiWigig(35, false));
        $this->assertEquals(21, Station::countAdPhiWigig(40, false));

        $this->assertEquals(0, Station::countAdPhiWigig(0, true));
        $this->assertEquals(0, Station::countAdPhiWigig(82, true));
        $this->assertEquals(0, Station::countAdPhiWigig(82, true));
        $this->assertEquals(8, Station::countAdPhiWigig(100, true));
        $this->assertEquals(15, Station::countAdPhiWigig(130, true));
        $this->assertEquals(8, Station::countAdPhiWigig(262, true));
        $this->assertEquals(5, Station::countAdPhiWigig(263, true));
    }

    public function testCountAdPhi()
    {
        $this->assertEquals(27.8, Station::countAdPhi(17, 35));
        $this->assertEquals(31, Station::countAdPhi(18, 35));
        $this->assertEquals(27.8, Station::countAdPhi(346, 35));
        $this->assertEquals(31, Station::countAdPhi(342, 35));

        $this->assertEquals(52.4, Station::countAdPhi(17, 80));
        $this->assertEquals(55.5, Station::countAdPhi(18, 80));
        $this->assertEquals(52.4, Station::countAdPhi(346, 80));
        $this->assertEquals(55.5, Station::countAdPhi(342, 80));
    }
}