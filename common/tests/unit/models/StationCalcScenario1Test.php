<?php
/*
* Copyright 2023 DactylGroup s.r.o.
*
* Licensed under the EUPL, Version 1.2 or – as soon they
will be approved by the European Commission - subsequent
versions of the EUPL (the "Licence");
* You may not use this work except in compliance with the
Licence.
* You may obtain a copy of the Licence at:
*
* https://joinup.ec.europa.eu/software/page/eupl5
*
* Unless required by applicable law or agreed to in
writing, software distributed under the Licence is
distributed on an "AS IS" basis,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
express or implied.
* See the Licence for the specific language governing
permissions and limitations under the Licence.
*/
?>


<?php

namespace common\tests\unit\models;

use Codeception\Test\Unit;
use common\models\Station;
use common\models\StationFs;
use common\models\StationWigig;
use Mockery\Mock;


class stationCalcScenario1Test extends Unit
{
    /**
     * @var \UnitTester
     */
    protected $tester;

    /**
     * @var $stationW Station|Mock
     */
    protected $stationW;

    /**
     * @var $stationW StationWigig|Mock
     */
    protected $wigig;

    /**
     * @var $fsA StationFs|Mock
     */
    protected $fsA;

    /**
     * @var $fsB StationFs|Mock
     */
    protected $fsB;

    /**
     * @var $stationB Station|Mock
     */
    protected $stationB;

    /**
     * @var $stationA Station|Mock
     */
    protected $stationA;

    protected $cDone = false;

    protected $fsData = [
        1520 => [
            'a' =>
                [
                    'id' => 15,
                    'lat' => 50.0421103835,
                    'lng' => 14.5599478483,
                    'antenna_volume' => 35,
                    'power' => 5,
                    'channel_width' => 200,
                    'type' => 'fs',
                    'fs' => [
                        'frequency' => 61000,
                        'specific_run_down' => 13,
                        'ratio_signal_interference' => 34,
                    ],
                    'out' => [
                        'inConflict' => true,
                        'diff' =>  1.5263
                    ]
                ]
            ,
            'b' =>
                [
                    'id' => 20,
                    'lat' => 50.0429338217,
                    'lng' => 14.5583224297,
                    'antenna_volume' => 40,
                    'power' => 5,
                    'channel_width' => 200,
                    'type' => 'fs',
                    'fs' => [
                        'frequency' => 61000,
                        'specific_run_down' => 13,

                        'ratio_signal_interference' => 34,
                    ],
                    'out' => [
                        'inConflict' => false,
                        'diff' => -17.2994
                    ]
                ]
        ],
        1452 => [
            'a' =>
                [
                    'id' => 14,
                    'lat' => 50.042527467,
                    'lng' => 14.5598351955,
                    'antenna_volume' => 30,
                    'power' => 5,
                    'channel_width' => 500,
                    'type' => 'fs',
                    'fs' => [
                        'frequency' => 61000,
                        'specific_run_down' => 13,

                        'ratio_signal_interference' => 34,
                    ],
                    'out' => [
                        'inConflict' => true,
                        'diff' => 10.957
                    ]
                ]
            ,
            'b' =>
                [
                    'id' => 52,
                    'lat' => 50.0413835049,
                    'lng' => 14.5616215467,
                    'antenna_volume' => 30,
                    'power' => 5,
                    'channel_width' => 500,
                    'type' => 'fs',
                    'fs' => [
                        'frequency' => 61000,
                        'ratio_signal_interference' => 34,
                        'specific_run_down' => 13,

                    ],
                    'out' => [
                        'inConflict' => false,
                        'diff' => 0
                    ]
                ]
        ],
        612 => [
            'a' =>
                [
                    'id' => 6,
                    'lat' => 50.0413694233,
                    'lng' => 14.5586174726,
                    'antenna_volume' => 45,
                    'power' => 5,
                    'channel_width' => 200,
                    'type' => 'fs',
                    'fs' => [
                        'frequency' => 61000,
                        'specific_run_down' => 13,

                        'ratio_signal_interference' => 28,
                    ],
                    'out' => [
                        'inConflict' => false,
                        'diff' => -17.2994
                    ]
                ]
            ,
            'b' =>
                [
                    'id' => 12,
                    'lat' => 50.0425066799,
                    'lng' => 14.5593953133,
                    'antenna_volume' => 43,
                    'power' => 10,
                    'channel_width' => 200,
                    'type' => 'fs',
                    'fs' => [
                        'frequency' => 61000,
                        'ratio_signal_interference' => 34,
                        'specific_run_down' => 13,

                    ],
                    'out' => [
                        'inConflict' => false,
                        'diff' => 0
                    ]
                ]
        ],
    ];

    protected function _before()
    {
        $this->prepareStationPair();
    }

    protected function prepareStationPair()
    {
        /**
         * @var $stationW Station|Mock
         */
        $this->stationW = \Mockery::mock(Station::class)->makePartial();
        $this->stationW->shouldReceive('hasAttribute')->andReturn(true);
        $this->wigig = \Mockery::mock(StationWigig::class)->makePartial();
        $this->wigig->shouldReceive('hasAttribute')->andReturn(true);

        $this->stationW->shouldReceive('getStationTypeObject')->andReturn($this->wigig);

        $this->stationA = \Mockery::mock(Station::class)->makePartial();
        $this->fsA = \Mockery::mock(StationFs::class)->makePartial();
        $this->fsA->shouldReceive('hasAttribute')->andReturn(true);
        $this->stationA->shouldReceive('getStationTypeObject')->andReturn($this->fsA);
        $this->stationA->shouldReceive('hasAttribute')->andReturn(true);

        $this->stationB = \Mockery::mock(Station::class)->makePartial();
        $this->fsB = \Mockery::mock(StationFs::class)->makePartial();
        $this->fsB->shouldReceive('hasAttribute')->andReturn(true);
        $this->stationB->shouldReceive('hasAttribute')->andReturn(true);

        $this->stationB->shouldReceive('getStationTypeObject')->andReturn($this->fsB);

        $this->stationA->stationPair = $this->stationB;
        $this->stationB->stationPair = $this->stationA;

        $this->initWigig();

    }

    protected function initWigig()
    {
        $this->stationW->lng = 14.559545517;
        $this->stationW->lat = 50.0424999744;
//        $this->stationW->antenna_volume = 22;
//        $this->stationW->power = 8;
        $this->stationW->channel_width = 2000;
        $this->stationW->type = 'wigig';

        $this->wigig->eirp = 40;
        $this->wigig->direction = 190;
//        $this->wigig->front_to_back_ratio = 25;
        $this->wigig->is_ptmp = true;
    }

    public function testScenario1Station15()
    {
        $item = $this->fsData[1520];

        $this->makeFsFromConfig($this->stationA, $this->fsA, $item['a']);
        $this->makeFsFromConfig($this->stationB, $this->fsB, $item['b']);

        $comp = $this->stationA->compareWith($this->stationW);
        $this->assertEquals($item['a']['out']['inConflict'], $comp['passiveInterference']);
        $this->assertEquals($item['a']['out']['diff'], round($comp['difference'], 4));
    }

    public function testScenario1Station14()
    {
        $item = $this->fsData[1452];

        $this->makeFsFromConfig($this->stationA, $this->fsA, $item['a']);
        $this->makeFsFromConfig($this->stationB, $this->fsB, $item['b']);

        $comp = $this->stationA->compareWith($this->stationW);
        $this->assertEquals($item['a']['out']['inConflict'], $comp['passiveInterference']);
        $this->assertEquals($item['a']['out']['diff'], round($comp['difference'], 4));
    }

    public function testScenario1Station6()
    {
        $item = $this->fsData[612];

        $this->makeFsFromConfig($this->stationA, $this->fsA, $item['a']);
        $this->makeFsFromConfig($this->stationB, $this->fsB, $item['b']);

        $comp = $this->stationA->compareWith($this->stationW);
        $this->assertEquals($item['a']['out']['inConflict'], $comp['passiveInterference']);
        $this->assertEquals($item['a']['out']['diff'], round($comp['difference'], 4));
    }

    protected function makeFsFromConfig(&$stationFs, &$fs, $config)
    {
        foreach ($config as $key => $val) {
            if ($key != 'fs' || $key != 'out') {
                $stationFs->$key = $val;
            }
        }

        foreach ($config['fs'] as $key => $val) {
            $fs->$key = $val;
        }
    }
}