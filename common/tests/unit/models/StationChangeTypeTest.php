<?php
/*
* Copyright 2023 DactylGroup s.r.o.
*
* Licensed under the EUPL, Version 1.2 or – as soon they
will be approved by the European Commission - subsequent
versions of the EUPL (the "Licence");
* You may not use this work except in compliance with the
Licence.
* You may obtain a copy of the Licence at:
*
* https://joinup.ec.europa.eu/software/page/eupl5
*
* Unless required by applicable law or agreed to in
writing, software distributed under the Licence is
distributed on an "AS IS" basis,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
express or implied.
* See the Licence for the specific language governing
permissions and limitations under the Licence.
*/
?>


<?php

namespace common\tests\unit\models;

use Codeception\Test\Unit;
use common\models\Station;
use common\models\StationImport;
use frontend\controllers\StationController;
use modules\user\common\models\User;


class StationChangeTypeTest extends Unit
{
    public function testChangeType()
    {
        // Superadmin
        $identity = User::findOne(['id' => 1]);
        \Yii::$app->user->login($identity);

        // Import file with only one station -> type 5.8 GHz
        $importer = new StationImport();
        $out = $importer->userImport(__DIR__ . "/../../fixtures/import/importToTypeChange.xlsx");

        $this->assertNotEquals(0, $out[3]['station']);
        $this->assertEquals(0, $out[3]['status']);
        $this->assertArrayNotHasKey('error', $out[3], 'Stanice by mela byt bez chyby!');

        /** @var Station $insertedStation */
        $insertedStation = Station::find()->where(['id' => $out[3]['station']])->one();

        $this->assertTrue($insertedStation->isReadyToBePublished());
        $this->assertTrue($insertedStation->moveToFinished());

        // Import file with change of type to 5.2 GHz
        $out = $importer->userImport(__DIR__ . "/../../fixtures/import/importTypeChange.xlsx");
        $this->assertNotEquals(0, $out[3]['station']);
        $this->assertEquals(0, $out[3]['status']);
        $this->assertArrayNotHasKey('error', $out[3], 'Stanice by mela byt bez chyby!');

        /** @var Station $insertedStation */
        $insertedStation = Station::find()->where(['id' => $out[3]['station']])->one();
        $this->assertEquals(StationController::TYPE_52, $insertedStation->type);
        $this->assertTrue($insertedStation->isPublished());
        $insertedStation->getStationTypeObject()->delete();
        $insertedStation->delete();
    }
}