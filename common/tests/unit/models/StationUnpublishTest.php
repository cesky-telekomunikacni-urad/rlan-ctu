<?php
/*
* Copyright 2023 DactylGroup s.r.o.
*
* Licensed under the EUPL, Version 1.2 or – as soon they
will be approved by the European Commission - subsequent
versions of the EUPL (the "Licence");
* You may not use this work except in compliance with the
Licence.
* You may obtain a copy of the Licence at:
*
* https://joinup.ec.europa.eu/software/page/eupl5
*
* Unless required by applicable law or agreed to in
writing, software distributed under the Licence is
distributed on an "AS IS" basis,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
express or implied.
* See the Licence for the specific language governing
permissions and limitations under the Licence.
*/
?>


<?php

namespace common\tests\unit\models;

use Codeception\Test\Unit;
use common\models\Station;
use common\models\StationImport;
use modules\user\common\models\User;


class StationUnpublishTest extends Unit
{
    public function testUnpublishState()
    {
        // Superadmin
        $identity = User::findOne(['id' => 1]);
        \Yii::$app->user->login($identity);

        $now = time();
        $importer = new StationImport();
        $out = $importer->userImport(__DIR__ . "/../../fixtures/import/userImportUnpublished.xlsx");


        $this->assertNotEquals(0, $out[3]['station']);
        $this->assertEquals(0, $out[3]['status']);
        $this->assertArrayNotHasKey('error', $out[3], 'Stanice by mela byt bez chyby!');

        /** @var Station $insertedStation */
        $insertedStation = Station::find()->where(['id' => $out[3]['station']])->one();
        $this->assertFalse($insertedStation->isPublished());
        $this->assertFalse($insertedStation->moveToUnpublished());
        $this->assertTrue($insertedStation->isReadyToBePublished());
        $this->assertTrue($insertedStation->moveToFinished());
        $this->assertTrue($insertedStation->moveToUnpublished());
        $this->assertTrue($insertedStation->isUnpublished());
        $insertedStation->getStationTypeObject()->delete();
        $insertedStation->delete();
    }
}