<?php
/*
* Copyright 2023 DactylGroup s.r.o.
*
* Licensed under the EUPL, Version 1.2 or – as soon they
will be approved by the European Commission - subsequent
versions of the EUPL (the "Licence");
* You may not use this work except in compliance with the
Licence.
* You may obtain a copy of the Licence at:
*
* https://joinup.ec.europa.eu/software/page/eupl5
*
* Unless required by applicable law or agreed to in
writing, software distributed under the Licence is
distributed on an "AS IS" basis,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
express or implied.
* See the Licence for the specific language governing
permissions and limitations under the Licence.
*/
?>


<?php


namespace common\tests\unit\models;

use Codeception\Test\Unit;
use common\models\Station;
use common\models\StationImport;
use common\models\StationWigig;
use frontend\controllers\StationController;
use modules\user\common\models\User;


class UserImportTest extends Unit
{
    const ANGLE_DIRECTIONS_COMPARING_ROUNDING_TO = 5; //decimal places

    public function testUserStationImportSuccess()
    {
        // Superadmin
        $identity = User::findOne(['id' => 1]);
        \Yii::$app->user->login($identity);

        $now = time();
        $importer = new StationImport();
        $out = $importer->userImport(__DIR__ . "/../../fixtures/import/userImportOK.xlsx");

        /**
         * Add new FS pair
         */
        $this->assertNotEquals(0,  $out[3]['station']);
        $this->assertEquals(0,  $out[3]['status']);
        $this->assertArrayNotHasKey('error',  $out[3], 'Stanice by mela byt bez chyby!');
        $insertedStation = Station::find()->where(['id' =>  $out[3]['station']])->one();


        $this->assertEquals('a', $insertedStation->pair_position);
        $this->assertEquals(StationController::TYPE_FS, $insertedStation->type);
        $this->assertEquals(12.56902500000000, $insertedStation->lng);
        $this->assertEquals(50.70338383000000, $insertedStation->lat);
        $this->assertEquals(42.00, $insertedStation->antenna_volume);
        $this->assertEquals(2160.00, $insertedStation->channel_width);
        $this->assertEquals(10.00, $insertedStation->power);
        $this->assertGreaterThan($now-1, $insertedStation->registered_at);
        // In da year
        $this->assertGreaterThan($now+31535999, $insertedStation->valid_to);
        $this->assertGreaterThan($now+45532799, $insertedStation->protected_to);
        $this->assertEquals(Station::STATUS_WAITING, $insertedStation->status);
        $this->assertEquals('Prvni testovaci nazev', $insertedStation->name);
        $this->assertEquals(1, $insertedStation->hardware_identifier);
        $this->assertEquals('B02F0B7CC895', $insertedStation->serial_number);
        $this->assertNull($insertedStation->mac_address);
        $this->assertEquals(58050.00, $insertedStation->getStationTypeObject()->frequency);
        $this->assertEquals(12, $insertedStation->getStationTypeObject()->ratio_signal_interference);

        $pairStation = $insertedStation->stationPair;
        $this->assertEquals('b', $pairStation->pair_position);
        $this->assertEquals(StationController::TYPE_FS, $pairStation->type);
        $this->assertEquals(12.47580277000000, $pairStation->lng);
        $this->assertEquals(50.74764444000000, $pairStation->lat);
        $this->assertEquals(37.00, $pairStation->antenna_volume);
        $this->assertEquals(2160.00, $pairStation->channel_width);
        $this->assertEquals(10.00, $pairStation->power);
        $this->assertEquals(Station::STATUS_WAITING, $pairStation->status);
        $this->assertEquals('Druhy testovaci nazev', $pairStation->name);
        $this->assertEquals(0, $pairStation->hardware_identifier);
        $this->assertEquals('04:D6:AA:AB:98:10', $pairStation->mac_address);
        $this->assertNull($pairStation->serial_number);
        $this->assertEquals(60000.00, $pairStation->getStationTypeObject()->frequency);
        $this->assertEquals(12, $pairStation->getStationTypeObject()->ratio_signal_interference);

        /**
         * WIGIG add
         */

        $this->assertNotEquals(0, $out[6]['station']);
        $this->assertEquals(0, $out[6]['status']);
        $this->assertArrayNotHasKey('error', $out[6], 'Stanice by mela byt bez chyby!');

        $insertedStation = Station::find()->where(['id' => $out[6]['station']])->one();
        $this->assertEquals(StationController::TYPE_WIGIG, $insertedStation->type);
        $this->assertEquals(18.37868611000000, $insertedStation->lng);
        $this->assertEquals(50.76965277000000, $insertedStation->lat);
        $this->assertEquals(10.00, $insertedStation->antenna_volume);
        $this->assertEquals(2160.00, $insertedStation->channel_width);
        $this->assertEquals(16.00, $insertedStation->power);
        $this->assertEquals(Station::STATUS_WAITING, $insertedStation->status);
        $this->assertEquals('Treti testovaci nazev', $insertedStation->name);
        $this->assertEquals(0, $insertedStation->hardware_identifier);
        $this->assertEquals('00:0C:42:00:1B:47', $insertedStation->mac_address);
        $this->assertNull($insertedStation->serial_number);

        // Check type object
        $typeStation = $insertedStation->getStationTypeObject();
        $this->assertNotNull($typeStation);

        $this->assertEquals(StationWigig::EIRP_METHOD_AUTO, $typeStation->eirp_method);
        $this->assertEquals(199, $typeStation->direction);
        $this->assertEquals(26.00, $typeStation->eirp);
        $this->assertEquals(1, $typeStation->is_ptmp);
        $this->assertEquals(round(50.34473976315800, static::ANGLE_DIRECTIONS_COMPARING_ROUNDING_TO),
            round($typeStation->angle_point_lat, static::ANGLE_DIRECTIONS_COMPARING_ROUNDING_TO));
        $this->assertEquals(round(18.14954486960900, static::ANGLE_DIRECTIONS_COMPARING_ROUNDING_TO),
            round($typeStation->angle_point_lng, static::ANGLE_DIRECTIONS_COMPARING_ROUNDING_TO));

        /**
         * WIGIG Update
         */
        $this->assertNotEquals(0, $out[7]['station']);
        $this->assertEquals(0, $out[7]['status']);
        $this->assertArrayNotHasKey('error', $out[7], 'Stanice by mela byt bez chyby!');
        $insertedStation = Station::find()->where(['id' => $out[7]['station']])->one();

        $this->assertEquals(2160.00, $insertedStation->channel_width);


        /**
         * 5.8 AP add
         */

        $this->assertNotEquals(0, $out[8]['station']);
        $this->assertEquals(0, $out[8]['status']);
        $this->assertArrayNotHasKey('error', $out[8], 'Stanice by mela byt bez chyby!');

        $insertedStation = Station::find()->where(['id' => $out[8]['station']])->one();
        $this->assertEquals(StationController::TYPE_58_AP, $insertedStation->type);
        $this->assertEquals(18.37868622000000, $insertedStation->lng);
        $this->assertEquals(50.76965288000000, $insertedStation->lat);
        $this->assertEquals(Station::STATUS_WAITING, $insertedStation->status);
        $this->assertEquals('Ctvrty testovaci nazev', $insertedStation->name);

        // Check type object
        $typeStation = $insertedStation->getStationTypeObject();
        $this->assertNotNull($typeStation);
        $this->assertEquals(1, $typeStation->is_ap);



        /**
         * 5.2 AP add
         */

        $this->assertNotEquals(0, $out[9]['station']);
        $this->assertEquals(0, $out[9]['status']);
        $this->assertArrayNotHasKey('error', $out[9], 'Stanice by mela byt bez chyby!');

        $insertedStation = Station::find()->where(['id' => $out[9]['station']])->one();
        $this->assertEquals(StationController::TYPE_52, $insertedStation->type);
        $this->assertEquals(18.37868622000000, $insertedStation->lng);
        $this->assertEquals(50.76965288000000, $insertedStation->lat);
        $this->assertEquals(Station::STATUS_WAITING, $insertedStation->status);
        $this->assertEquals('Paty testovaci nazev edit', $insertedStation->name);

        // Check type object
        $typeStation = $insertedStation->getStationTypeObject();
        $this->assertNotNull($typeStation);
    }

    public function testUserStationImportFail()
    {
        $identity = User::findOne(1);
        \Yii::$app->user->login($identity);

        $importer = new StationImport();
        $out = $importer->userImport(__DIR__ . "/../../fixtures/import/userImportNOK.xlsx");

        $this->assertEquals(0,  $out[3]['station']);
        $this->assertEquals(StationImport::RESULT_ERROR,  $out[3]['status']);
        $this->assertArrayHasKey('error',  $out[3]);
        $this->assertNotEmpty( $out[3]['error']);

        $this->assertEquals(0, $out[4]['station']);
        $this->assertEquals(StationImport::RESULT_ERROR, $out[4]['status']);
        $this->assertArrayHasKey('error', $out[4]);
        $this->assertNotEmpty($out[4]['error']);

        $this->assertEquals(0, $out[5]['station']);
        $this->assertEquals(StationImport::RESULT_ERROR, $out[5]['status']);
        $this->assertArrayHasKey('error', $out[5]);
        $this->assertNotEmpty($out[5]['error']);

        $this->assertEquals(0, $out[6]['station']);
        $this->assertEquals(StationImport::RESULT_WARNING, $out[6]['status']);
        $this->assertArrayNotHasKey('error', $out[6]);

        $this->assertEquals(0, $out[7]['station']);
        $this->assertEquals(StationImport::RESULT_ERROR, $out[7]['status']);
        $this->assertArrayHasKey('error', $out[7]);
        $this->assertNotEmpty($out[7]['error']);

        $this->assertEquals(0, $out[8]['station']);
        $this->assertEquals(StationImport::RESULT_ERROR, $out[8]['status']);
        $this->assertArrayHasKey('error', $out[8]);
        $this->assertNotEmpty($out[8]['error']);
    }
}