<?php
/*
* Copyright 2023 DactylGroup s.r.o.
*
* Licensed under the EUPL, Version 1.2 or – as soon they
will be approved by the European Commission - subsequent
versions of the EUPL (the "Licence");
* You may not use this work except in compliance with the
Licence.
* You may obtain a copy of the Licence at:
*
* https://joinup.ec.europa.eu/software/page/eupl5
*
* Unless required by applicable law or agreed to in
writing, software distributed under the Licence is
distributed on an "AS IS" basis,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
express or implied.
* See the Licence for the specific language governing
permissions and limitations under the Licence.
*/
?>


<?php

namespace common\tests\unit\models;

use Codeception\Test\Unit;
use common\models\Station;


class StationOverlappingTest extends Unit
{
    /**
     * @var \UnitTester
     */
    protected $tester;



    protected function _before()
    {
    }


    public function testOverlapping()
    {
        $fromLeftTrue = Station::isOverlapping(61000, 200, 60500, 900);
        $fromLeftFalse = Station::isOverlapping(61000, 200, 60500, 200);
        $fromRightTrue = Station::isOverlapping(60000, 800, 60500, 400);
        $fromRightFalse = Station::isOverlapping(60000, 200, 60500, 200);
        $firstInSecond = Station::isOverlapping(60000, 100, 60500, 1600);
        $secondInFirst = Station::isOverlapping(60000, 1000, 60100, 100);
        $same = Station::isOverlapping(60000, 500, 60000, 500);
        $touch = Station::isOverlapping(60000, 1000, 61000, 1000);

        $this->assertTrue($fromLeftTrue);
        $this->assertTrue($fromRightTrue);
        $this->assertTrue($firstInSecond);
        $this->assertTrue($secondInFirst);
        $this->assertFalse($fromLeftFalse);
        $this->assertFalse($fromRightFalse);
        $this->assertTrue($same);
        $this->assertFalse($touch);
    }

}