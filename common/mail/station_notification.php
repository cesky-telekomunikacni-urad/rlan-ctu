<?php
/*
* Copyright 2023 DactylGroup s.r.o.
*
* Licensed under the EUPL, Version 1.2 or – as soon they
will be approved by the European Commission - subsequent
versions of the EUPL (the "Licence");
* You may not use this work except in compliance with the
Licence.
* You may obtain a copy of the Licence at:
*
* https://joinup.ec.europa.eu/software/page/eupl5
*
* Unless required by applicable law or agreed to in
writing, software distributed under the Licence is
distributed on an "AS IS" basis,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
express or implied.
* See the Licence for the specific language governing
permissions and limitations under the Licence.
*/
?>


<?php
/**
 * @var string $notificationHeadline
 * @var string $notificationContent
 * @var array $stations
 */

use common\models\Station;
use frontend\widgets\dactylkit\DactylKit;

?>

<table width="514" border="0" align="center" cellpadding="0" cellspacing="0"
       style="border-collapse: collapse; font-family: Arial; font-size: 14px; line-height: 22px; color: #444444; margin-bottom: 0;">
  <tr>
    <td style="border-radius: 2px; background-color: #ffffff; line-height: 22px;" class="shadow">
      <div
          style="border-radius: 2px; background-color: #ffffff; padding: 40px 40px 24px 40px; 	color: rgba(0,0,0,0.87);	font-size: 16px;	line-height: 24px;">
        <h3>
            <?= $notificationHeadline ?>
        </h3>
        <p>
            <?= $notificationContent ?>
        </p>
        <ul>
            <?php foreach ($stations as $station): ?>
              <li>
                <a href="<?= url(['/station/station', 'id' => $station['id']], true) ?>" target="_blank">
                    <?= empty($station['name']) ? Station::findOne($station['id'])->getTypeName() : $station['name'] ?>
                </a>
              </li>
            <?php endforeach; ?>
        </ul>
      </div>
      <a href="<?= url(['/'], true) ?>" class="dk--btn dk--btn--primary"
         style="margin:40px;display: inline-flex;align-items: center;justify-content: space-between;flex-direction: row;
            padding: 12px;border-radius:4px;border: none;-webkit-transition: .5s;transition: .5s;white-space: nowrap;
            background-color:rgba(53,47,132,1);box-shadow:0px 2px 4px rgba(26, 32, 44, 0.2), 0px 0px 2px rgba(26, 32, 44, 0.25), 0px 0px 1px rgba(26, 32, 44, 0.3);
            "
         onmouseover="this.style.boxShadow='0px 4px 8px rgba(26, 32, 44, 0.2), 0px 2px 4px rgba(26, 32, 44, 0.25), 0px 1px 2px rgba(26, 32, 44, 0.3);'">
                <span class="dk--btn__label" style="font-size:14px;font-family:'SF Pro Text',sans-serif;font-weight:600;
                line-height:16px;text-align: center;padding: 0 4px;color:#fff;">
                    <?= _tF('stations', 'station') ?>
                </span>
      </a>
    </td>
  </tr>
</table>