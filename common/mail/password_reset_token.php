<?php
/*
* Copyright 2023 DactylGroup s.r.o.
*
* Licensed under the EUPL, Version 1.2 or – as soon they
will be approved by the European Commission - subsequent
versions of the EUPL (the "Licence");
* You may not use this work except in compliance with the
Licence.
* You may obtain a copy of the Licence at:
*
* https://joinup.ec.europa.eu/software/page/eupl5
*
* Unless required by applicable law or agreed to in
writing, software distributed under the Licence is
distributed on an "AS IS" basis,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
express or implied.
* See the Licence for the specific language governing
permissions and limitations under the Licence.
*/
?>


<?php

/* @var $this \yii\web\View */
/* @var $token string */

?>

<table width="514" border="0" align="center" cellpadding="0" cellspacing="0"
       style="border-collapse: collapse; font-family: Arial; font-size: 14px; line-height: 22px; color: #444444; margin-bottom: 0;">
    <tr>
        <td style="border-radius: 2px; background-color: #ffffff; line-height: 22px;" class="shadow">
            <table style="border-radius: 2px; background-color: #ffffff; padding: 40px 40px 24px 40px; 	color: rgba(0,0,0,0.87);	font-size: 16px;	line-height: 24px;">
                <tr>
                    <td>
                        <p>
                            <?= _tF('pass_change_message',
                                'reset_password_email', [
                                    'password' => $password,
                                ]); ?>
                        </p>
                    </td>
                </tr>
            </table>
        </td>
    </tr>
</table>