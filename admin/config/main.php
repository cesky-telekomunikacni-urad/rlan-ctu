<?php
/*
* Copyright 2023 DactylGroup s.r.o.
*
* Licensed under the EUPL, Version 1.2 or – as soon they
will be approved by the European Commission - subsequent
versions of the EUPL (the "Licence");
* You may not use this work except in compliance with the
Licence.
* You may obtain a copy of the Licence at:
*
* https://joinup.ec.europa.eu/software/page/eupl5
*
* Unless required by applicable law or agreed to in
writing, software distributed under the Licence is
distributed on an "AS IS" basis,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
express or implied.
* See the Licence for the specific language governing
permissions and limitations under the Licence.
*/
?>


<?php

use dactylcore\core\admin\components\MenuItems;
use dactylcore\page\frontend\models\Page;

$adminConfig = [
    'params' => require __DIR__ . '/params.php',
    'bootstrap' => [
        'dc-menu',
        'dc-page',
        function () {
            // Add Permissios

            Yii::$app->getModule('dc-user')->getAccessManager()->addPermissions([
                'dc-system' => [
                    'config' => [
                        'update_content',
                        'update_stations',
                    ],
                ],
            ]);

            Yii::$app->getModule('dc-user')->getAccessManager()->addPermissions([
                'dc-system' => [
                    'config' => [
                        'update_app-frontend',
                    ],
                ],
            ]);

            if (hasAccessTo('dc-system_config_update_content')) {
                Yii::$app->menuItems->addSubItems(MenuItems::SIDEBAR_MENU, 'dc-system', [
                    'dc-system_config_update_content' => [
                        'label' => _tF('content', 'config'),
                        'url' => ['/dc-system/config/update', 'permission' => 'content'],
                        'order' => 40,
                    ],
                ]);
            }

            if (hasAccessTo('dc-system_config_update_stations')) {
                Yii::$app->menuItems->addSubItems(MenuItems::SIDEBAR_MENU, 'dc-system', [
                    'dc-system_config_update_stations' => [
                        'label' => _tF('stations', 'config'),
                        'url' => ['/dc-system/config/update', 'permission' => 'stations'],
                        'order' => 40,
                    ],
                ]);
            }

            $pageLogged = Page::find()->andWhere(['id' => c('MAIN_PAGE_ID_LOGGED')])->one();
            $pageNotLogged = Page::find()->andWhere(['id' => c('MAIN_PAGE_ID_NOT_LOGGED')])->one();
            $pageApiTesting = Page::find()->andWhere(['id' => c('TESTING_API_MAIN_SITE')])->one();

            Yii::$app->getModule('dc-system')->getConfigManager()->addActiveFieldSettings([
                'MAIN_PAGE_ID_NOT_LOGGED' => [
                    'function' => 'widget',
                    'params' => [
                        'class' => \dactylcore\core\widgets\admin\select2\Select2::class,
                        'config' => [
                            'hideSearch' => false,
                            'data' => $pageNotLogged ? [$pageNotLogged->id => $pageNotLogged->title] : ['' => ''],
                            'pluginOptions' => [
                                'ajax' => [
                                    'url' => url(['/dc-page/page/page-dropdown-data', 'type' => 1]),
                                    'dataType' => 'json',
                                ],
                            ],
                        ],
                    ],
                ],
                'MAIN_PAGE_ID_LOGGED' => [
                    'function' => 'widget',
                    'params' => [
                        'class' => \dactylcore\core\widgets\admin\select2\Select2::class,
                        'config' => [
                            'hideSearch' => false,
                            'data' => $pageLogged ? [$pageLogged->id => $pageLogged->title] : ['' => ''],
                            'pluginOptions' => [
                                'ajax' => [
                                    'url' => url(['/dc-page/page/page-dropdown-data', 'type' => 1]),
                                    'dataType' => 'json',
                                ],
                            ],
                        ],
                    ],
                ],
                'DECLARATION_TEXT' => [
                    'function' => 'wysiwyg',
                    'params' => [
                    ],
                ],
                'HELP_YT_VIDEO' => [
                    'function' => 'textInput',
                    'params' => [
                        'options' => [
                            'type' => 'url',
                            'placeholder' => 'youtu.be/...',
                        ],
                    ],
                ],
                'HELP_MANUAL_PDF' => [
                    'function' => 'fileInput',
                    'params' => [
                        'options' => [
                            'limit' => 1,
                        ],
                    ],
                ],
                'EXCLUDED_ZONES_58_GHZ' => [
                    'function' => 'textarea',
                ],
                'EXCLUDED_ZONE_58_GHZ_DIAMETER' => [
                    'function' => 'textInput',
                ],
                'TESTING_API_MAIN_SITE' => [
                    'function' => 'widget',
                    'params' => [
                        'class' => \dactylcore\core\widgets\admin\select2\Select2::class,
                        'config' => [
                            'hideSearch' => false,
                            'data' => $pageApiTesting ? [$pageApiTesting->id => $pageApiTesting->title] : ['' => ''],
                            'pluginOptions' => [
                                'ajax' => [
                                    'url' => url(['/dc-page/page/page-dropdown-data', 'type' => 1]),
                                    'dataType' => 'json',
                                ],
                            ],
                        ],
                    ],
                ],
            ]);

            Yii::$app->getModule('dc-user')->getAccessManager()->addPermissions([
                'app-admin' => [
                    'station' => [
                        'index',
                        'create',
                        'update',
                        'delete',
                    ],
                ],
            ]);
            Yii::$app->getModule('dc-user')->getAccessManager()->addPermissions([
                'app-admin' => [
                    'phi' => [
                        'index',
                        'create',
                        'update',
                        'delete',
                    ],
                ],
            ]);

            Yii::$app->getModule('dc-user')->getAccessManager()->addPermissions([
                'app-admin' => [
                    'phi-wigig' => [
                        'index',
                        'create',
                        'update',
                        'delete',
                    ],
                ],
            ]);

            Yii::$app->getModule('dc-user')->getAccessManager()->addPermissions([
                'app-admin' => [
                    'probe' => [
                        'index',
                        'create',
                        'update',
                        'delete',
                    ],
                ],
            ]);

            Yii::$app->getModule('dc-user')->getAccessManager()->addPermissions([
                'app-admin' => [
                    'probe-scan' => [
                        'index',
                    ],
                ],
            ]);

            Yii::$app->getModule('dc-user')->getAccessManager()->addPermissions([
                'app-admin' => [
                    'user-alliance' => [
                        'index',
                        'view',
                    ],
                ],
            ]);

            if (hasAccessTo('app-admin_station_index')) {
                Yii::$app->menuItems->addItems(MenuItems::SIDEBAR_MENU, [
                    'station' => [
                        'icon' => '<i class="material-icons">wifi_tethering</i>',
                        'label' => _tF('stations_admin_name', 'station'),
                        'url' => ['/station/index'],
                        'order' => 2,
                    ],
                ]);
            }
            if (hasAccessTo('app-admin_phi_index')) {
                Yii::$app->menuItems->addItems(MenuItems::SIDEBAR_MENU, [
                    'phi' => [
                        'icon' => '<i class="material-icons">signal_cellular_null</i>',
                        'label' => _tF('phi', 'phi'),
                        'url' => ['/phi/index'],
                        'order' => 3,
                    ],
                ]);
            }
            if (hasAccessTo('app-admin_phi-wigig_index')) {
                Yii::$app->menuItems->addItems(MenuItems::SIDEBAR_MENU, [
                    'phiwigig' => [
                        'icon' => '<i class="material-icons">signal_cellular_4_bar</i>',
                        'label' => _tF('phi_wigig', 'phi_wigig'),
                        'url' => ['/phi-wigig/index'],
                        'order' => 4,
                    ],
                ]);
            }
            if (hasAccessTo('app-admin_probe_index')) {
                Yii::$app->menuItems->addItems(MenuItems::SIDEBAR_MENU, [
                    'probes' => [
                        'icon' => '<i class="material-icons">speaker_phone</i>',
                        'label' => _tF('probes', 'probe'),
                        'url' => ['/probe/index'],
                        'order' => 5,
                    ],
                ]);
            }
            if (hasAccessTo('app-admin_probe-scan_index')) {
                Yii::$app->menuItems->addItems(MenuItems::SIDEBAR_MENU, [
                    'probes-scans' => [
                        'icon' => '<i class="material-icons">perm_scan_wifi</i>',
                        'label' => _tF('probe scans', 'probe'),
                        'url' => ['/probe-scan/index'],
                        'order' => 6,
                    ],
                ]);
            }

            if (hasAccessTo('app-admin_notification_index')) {
                Yii::$app->menuItems->addItems(MenuItems::SIDEBAR_MENU, [
                    'notifocations' => [
                        'icon' => '<i class="material-icons">notifications</i>',
                        'label' => _tF('notifications', 'notification'),
                        'url' => ['/notification/index'],
                        'order' => 6,
                    ],
                ]);
            }

            // User menu part
            // Add menu items
            $menuItems = [];

            if (hasAccessTo('dc-user_user_index')) {
                $menuItems['dc-user_user_index'] = [
                    'label' => _tF('users', 'user'),
                    'url' => ['/dc-user/user/index'],
                    'order' => 10,
                ];
            }

            if (hasAccessTo('dc-user_user-role_index')) {
                $menuItems['dc-user_user-role_index'] = [
                    'label' => _tF('roles', 'user_role'),
                    'url' => ['/dc-user/user-role/index'],
                    'order' => 20,
                ];
            }

            if (hasAccessTo('dc-user_gdpr-agreement_index')) {
                $menuItems['dc-user_gdpr-agreement_index'] = [
                    'label' => _tF('gdpr_agreements', 'gdpr-agreement'),
                    'url' => ['/dc-user/gdpr-agreement/index'],
                    'order' => 30,
                ];
            }

            if (hasAccessTo('app-admin_user-alliance_index')) {
                $menuItems['app-admin_user-alliances_index'] = [
                    'label' => _tF('user_alliances', 'user-alliance'),
                    'url' => ['/user-alliance/index'],
                    'order' => 40,
                ];
            }

            Yii::$app->menuItems->addItems(MenuItems::SIDEBAR_MENU, [
                'dc-user' => [
                    'icon' => '<i class="material-icons">people</i>',
                    'label' => _tF('users', 'user'),
                    'items' => $menuItems,
                    'order' => 50,
                ],
            ]);

            Yii::$app->getModule('dc-user')->getAccessManager()->addPermissions([
                'dc-user' => [
                    'user' => [
                        'export',
                    ],
                ],
                'app-admin' => [
                    'station' => [
                        'export',
                    ],
                ],
            ]);

            Yii::$app->getModule('dc-user')->getAccessManager()->addPermissions([
                'app-admin' => [
                    'notification' => [
                        'index',
                        'create',
                        'update',
                        'delete',
                    ],
                ],
            ]);

            \admin\assets\Admin2Asset::register(Yii::$app->view);
        },
    ],
    'modules' => [
        'dc-menu' => [
            'class' => dactylcore\menu\admin\Module::class,
        ],
        'dc-page' => [
            'class' => dactylcore\page\admin\Module::class,
        ],
    ],
];

return $adminConfig;