<?php
/*
* Copyright 2023 DactylGroup s.r.o.
*
* Licensed under the EUPL, Version 1.2 or – as soon they
will be approved by the European Commission - subsequent
versions of the EUPL (the "Licence");
* You may not use this work except in compliance with the
Licence.
* You may obtain a copy of the Licence at:
*
* https://joinup.ec.europa.eu/software/page/eupl5
*
* Unless required by applicable law or agreed to in
writing, software distributed under the Licence is
distributed on an "AS IS" basis,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
express or implied.
* See the Licence for the specific language governing
permissions and limitations under the Licence.
*/
?>


<?php
// Uses
use dactylcore\core\helpers\Html;
use dactylcore\core\web\AdminView;
use dactylcore\core\helpers\Url;
use common\models\Probe;

// Sources
/* @var $this AdminView */
/* @var $model Probe */

// Settings
$pageId = 'probe';
$formId = "{$pageId}-form";
$action = $model->isNewRecord ? 'create' : 'update';
$moduleId = Yii::$app->controller->module->id;
$this->title = _tF("{$action}_probe", 'probe');
$permission = "{$moduleId}_probe_{$action}";

// Breadcrumbs
$this->breadcrumbs[] = [
    'label' => _tF('probes', 'probe'),
    'url' => ['index'],
];

$this->breadcrumbs[] = [
    'label' => $this->title,
    'url' => currentUrl(),
];

// Buttons
$this->buttons[] = Html::a(_tF('cancel'), Url::getPreviousIndexUrl(), $permission, [
    'class' => 'btn btn-secondary',
]);

$this->buttons[] = Html::button(_tF('save_and_close', 'default'), $permission, [
    'type' => 'submit',
    'form' => $formId,
    'class' => 'btn btn-primary',
]);
?>


<!-- FORM CONTAINER BEGIN -->
<?= $this->beginContainer(false, ['id' => "{$pageId}-{$action}"]); ?>
<?= $this->render('partial/form', [
    'model' => $model,
    'pageId' => $pageId,
    'title' => $this->title,
    'formId' => $formId,
]) ?>
<?= $this->endContainer() ?>
<!-- FORM CONTAINER END -->