<?php
/*
* Copyright 2023 DactylGroup s.r.o.
*
* Licensed under the EUPL, Version 1.2 or – as soon they
will be approved by the European Commission - subsequent
versions of the EUPL (the "Licence");
* You may not use this work except in compliance with the
Licence.
* You may obtain a copy of the Licence at:
*
* https://joinup.ec.europa.eu/software/page/eupl5
*
* Unless required by applicable law or agreed to in
writing, software distributed under the Licence is
distributed on an "AS IS" basis,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
express or implied.
* See the Licence for the specific language governing
permissions and limitations under the Licence.
*/
?>


<?php
// Uses
use dactylcore\core\helpers\Html;
use dactylcore\core\widgets\admin\gridview\GridView;
use dactylcore\core\data\ActiveDataProvider;
use dactylcore\core\widgets\admin\gridview\CheckboxColumn;
use dactylcore\core\widgets\common\gridview\ActionColumn;
use dactylcore\core\web\AdminView;
use common\models\Probe;


// Sources
/* @var $this AdminView */
/* @var $model Probe */
/* @var $dataProvider ActiveDataProvider */

// Settings
$pageId = 'probe';
$this->title = _tF('probes', 'probe');
$moduleId = Yii::$app->controller->module->id;

// Breadcrumbs
$this->breadcrumbs[] = [
    'label' => $this->title,
    'url' => currentUrl(),
];

// Buttons
$this->buttons[] = Html::a(_tF('create_probe', 'probe'), ['create'], "{$moduleId}_probe_create", [
    'class' => 'btn btn-primary',
]);
?>

<div class="<?= $pageId ?>-index">
    <div class="portlet">
        <div class="body">
            <?= GridView::widget([
                'id' => "{$pageId}-grid",
                'dataProvider' => $dataProvider,
                'responsive' => true,
                'responsiveWrap' => true,
                'floatHeader' => true,
                'floatHeaderOptions' => [
                    'scrollingTop' => '0',
                    'position' => 'absolute',
                    'top' => 56,
                ],
                'doubleScroll' => true,
                'columns' => [
                    ['class' => CheckboxColumn::class],
                    'id',
                    'name',
                    'description:ntext',
                    'access_token',
                    'status',
                    // 'deleted',
                    // 'created_at',
                    // 'updated_at',
                    [
                        'class' => ActionColumn::class,
                        'template' => '{update} {delete}',
                        'contentOptions' => [
                            'style' => 'min-width: 165px;'
                        ]
                    ]
                ]
            ]); ?>
        </div>
    </div>
</div>
