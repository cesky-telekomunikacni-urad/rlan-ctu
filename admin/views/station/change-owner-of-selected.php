<?php
/*
* Copyright 2023 DactylGroup s.r.o.
*
* Licensed under the EUPL, Version 1.2 or – as soon they
will be approved by the European Commission - subsequent
versions of the EUPL (the "Licence");
* You may not use this work except in compliance with the
Licence.
* You may obtain a copy of the Licence at:
*
* https://joinup.ec.europa.eu/software/page/eupl5
*
* Unless required by applicable law or agreed to in
writing, software distributed under the Licence is
distributed on an "AS IS" basis,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
express or implied.
* See the Licence for the specific language governing
permissions and limitations under the Licence.
*/
?>


<?php
// Uses
use dactylcore\core\helpers\Html;
use dactylcore\core\web\AdminView;
use dactylcore\core\helpers\Url;
use dactylcore\core\widgets\admin\activeform\ActiveForm;
use dactylcore\core\widgets\admin\select2\Select2;

// Sources
/* @var $this AdminView */
/* @var $model common\models\Station */
/* @var $count Integer */

// Settings
$pageId = 'station-move';
$formId = $pageId . '-form';
$this->title = _tF('owner_change', 'station');
$permission = Yii::$app->controller->module->id . '_station_update';

// Breadcrumbs
$this->breadcrumbs[] = [
    'label' => _tF('stations', 'station'),
    'url' => ['index'],
];

$this->breadcrumbs[] = [
    'label' => _tF('owner_change', 'station'),
    'url' => currentUrl(),
];

// Buttons
$this->buttons[] = Html::a(_tF('cancel'), Url::getPreviousIndexUrl(), $permission, [
    'class' => 'btn btn-secondary',
]);

$this->buttons[] = Html::button(_tF('update'), $permission, [
    'type' => 'submit',
    'form' => $formId,
    'class' => 'btn btn-primary',
]);

\admin\assets\StationAsset::register($this);
?>

<?= $this->beginContainer(false, ['id' => $pageId . '-update']); ?>

<?php $form = ActiveForm::begin([
    'id' => $pageId . '-form',
]); ?>


<div class="portlet">
    <div class="body">
        <div class="row">
            <div class="col-md-6 col-md-offset-3">
                <?= $form->field($model, 'id_user')
                    ->label(_tF('station_move_new_owner_info', 'station', ['count' => $count]))
                    ->widget(Select2::class, [
                        'hideSearch' => false,
                        'data' => [],
                        'pluginOptions' => [
                            'allowClear' => false,
                            'placeholder' => '',
                            'ajax' => [
                                'url' => url(['/dc-user/user/user-dropdown-data2']),
                                'dataType' => 'json',
                                'data' => new \yii\web\JsExpression(
                                    <<<JS
                                            function (params) {
                                                return {
                                                    term: params.term,
                                                    page: params.page || 1
                                                }
                                            }
JS
                                ),
                            ],
                        ],
                    ]) ?>
            </div>
        </div>
    </div>
</div>

<div class="form-buttons">
    <?= $form->cancelButton(Url::getPreviousIndexUrl()); ?>
    <?= \yii\helpers\Html::submitButton(_tF('yes_move', 'station'), [
        'class' => 'btn btn-primary',
        'data-confirm' => _tF('confirm_station_move', 'station'),
    ]); ?>
</div>

<?php ActiveForm::end(); ?>
<?= $this->endContainer() ?>

<?php
$this->registerJs(<<<JS
    $('#dynamicmodel-id_user').on('change',   function() {
        ignoreUnsavedForm=true;
    });
JS
);