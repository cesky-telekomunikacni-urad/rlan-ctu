<?php
/*
* Copyright 2023 DactylGroup s.r.o.
*
* Licensed under the EUPL, Version 1.2 or – as soon they
will be approved by the European Commission - subsequent
versions of the EUPL (the "Licence");
* You may not use this work except in compliance with the
Licence.
* You may obtain a copy of the Licence at:
*
* https://joinup.ec.europa.eu/software/page/eupl5
*
* Unless required by applicable law or agreed to in
writing, software distributed under the Licence is
distributed on an "AS IS" basis,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
express or implied.
* See the Licence for the specific language governing
permissions and limitations under the Licence.
*/
?>


<?php
// Uses
use common\models\Station;
use dactylcore\core\helpers\Html;
use dactylcore\core\widgets\admin\gridview\GridView;
use dactylcore\core\data\ActiveDataProvider;
use dactylcore\core\widgets\common\bootstrap\Html as BootHtml;
use dactylcore\core\widgets\admin\activeform\ActiveForm;
use dactylcore\core\helpers\Url;

// Sources
/* @var $this \dactylcore\core\web\AdminView */
/* @var $formModel \admin\models\ExportStationsInRadiusForm */
/* @var $exportedFileurl string */

// Settings
$pageId = 'station';
$this->title = _tF('export_stations_in_radius', 'station');
$permission = Yii::$app->controller->module->id . '_station_create';

// Breadcrumbs
$this->breadcrumbs[] = [
    'label' => _tF('stations', 'station'),
    'url' => ['index'],
];

$this->breadcrumbs[] = [
    'label' => $this->title,
    'url' => ['export-stations-in-radius'],
];

\admin\assets\StationAsset::register($this);
?>

    <div class="<?= $pageId ?>-export-in-radius">

        <?php $form = ActiveForm::begin([
            'id' => 'export-stations-in-radius-form',
            'method' => 'GET',
            'pjax' => false,
        ]); ?>

        <div class="portlet">
            <div class="body">

                <div class="row">
                    <div class="col-md-3">
                        <?= $form->field($formModel, 'lat')->textInput() ?>
                    </div>
                    <div class="col-md-3">
                        <?= $form->field($formModel, 'lng')->textInput() ?>
                    </div>
                    <div class="col-md-3">
                        <?= $form->field($formModel, 'radius')->textInput() ?>
                    </div>
                    <div class="col-md-3">
                        <?= \yii\helpers\Html::submitButton(_tF('export_form_button_label', 'station'),
                            ['class' => 'btn btn-primary']) ?>
                    </div>
                </div>

            </div>
        </div>

        <?php if (isset($exportedFileurl)): ?>

            <div class="portlet result hidden" style="display: none">
                <div class="body">
                    <?= _tF('export_success_message', 'station') ?><br>
                    <?= \yii\helpers\Html::a($exportedFileurl, $exportedFileurl, [
                        'data-pjax' => 0,
                        'target' => '_blank',
                    ]) ?>
                </div>
            </div>

        <?php endif; ?>

        <?php ActiveForm::end(); ?>

    </div>

<?php
$this->registerJs(<<<JS

    $(document).on('submit', '#export-stations-in-radius-form', function(){
        $('.portlet.result').remove();
        dcUI.block('#export-stations-in-radius-form');
    });

     if ($('.portlet.result.hidden').length > 0){
         $('.portlet.result.hidden').removeClass('hidden').fadeIn(500, function() {
             setTimeout(function(){
                   $('.portlet.result a').get(0).click();
             }, 500);
         });
     }
     
JS
);

