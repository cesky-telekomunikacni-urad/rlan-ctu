<?php
/*
* Copyright 2023 DactylGroup s.r.o.
*
* Licensed under the EUPL, Version 1.2 or – as soon they
will be approved by the European Commission - subsequent
versions of the EUPL (the "Licence");
* You may not use this work except in compliance with the
Licence.
* You may obtain a copy of the Licence at:
*
* https://joinup.ec.europa.eu/software/page/eupl5
*
* Unless required by applicable law or agreed to in
writing, software distributed under the Licence is
distributed on an "AS IS" basis,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
express or implied.
* See the Licence for the specific language governing
permissions and limitations under the Licence.
*/
?>


<?php
// Uses
use common\models\Station;
use dactylcore\core\helpers\Html;
use dactylcore\core\helpers\Url;
use dactylcore\core\widgets\admin\gridview\GridView;
use dactylcore\core\data\ActiveDataProvider;
use dactylcore\core\widgets\common\bootstrap\Html as BootHtml;

// Sources
/* @var $this \dactylcore\core\web\AdminView */
/* @var $searchModel common\models\search\StationSearch */
/* @var $dataProvider ActiveDataProvider */

// Settings
$pageId = 'station';
$this->title = _tF('stations', 'station');
$permission = Yii::$app->controller->module->id . '_station_create';

// Breadcrumbs
$this->breadcrumbs[] = [
    'label' => $this->title,
    'url' => currentUrl(),
];

// Buttons
$this->buttons[] = Html::a(_tF('export_all', 'station'),
    array_merge([$pageId . '/export-xls'], Yii::$app->request->queryParams),
    'app-admin_station_export', [
        'class' => 'btn btn-secondary export-btn',
        'data-method' => 'post',
        'data-ajax' => "1",
        'data-pjax' => '0',
        'data-confirm' => _tF('export_all_confirm', 'log'),
        'data-base-url' => Url::to([$pageId . '/export-xls']),
    ]
);
$this->buttons[] = Html::a(_tF('export_all_csv', 'station'),
    array_merge([$pageId . '/export-csv'], Yii::$app->request->queryParams),
    'app-admin_station_export', [
        'class' => 'btn btn-secondary export-btn',
        'data-method' => 'post',
        'data-ajax' => "1",
        'data-pjax' => '0',
        'data-confirm' => _tF('export_all_confirm', 'log'),
        'data-base-url' => Url::to([$pageId . '/export-csv']),
    ]
);

$this->buttons[] = Html::a(_tF('export_stations_in_radius', 'station'), ['export-stations-in-radius'],
    Yii::$app->controller->module->id . '_station_index', [
        'class' => 'btn btn-secondary',
    ]
);

$this->buttons[] = Html::a(_tF('change_owner_to_selected', 'station'),
    array_merge([$pageId . '/move-selected-to-owner'], Yii::$app->request->queryParams),
    Yii::$app->controller->module->id . '_station_index', [
        'class' => 'btn btn-secondary export-btn',
        'data-base-url' => Url::to([$pageId . '/move-selected-to-owner']),
        'data-pjax' => '0',
    ]
);

\admin\assets\StationAsset::register($this);
\admin\assets\Admin2Asset::register($this);

if (!$searchModel->lng) {
    $searchModel->lng = $searchModel::GPS_LNG_MIN . "," . $searchModel::GPS_LNG_MAX;
}
if (!$searchModel->lat) {
    $searchModel->lat = $searchModel::GPS_LAT_MIN . "," . $searchModel::GPS_LAT_MAX;
}

?>

    <div class="<?= $pageId ?>-index">
        <div class="portlet">
            <div class="body">
                <?= GridView::widget([
                    'id' => $pageId . '-grid',
                    'dataProvider' => $dataProvider,
                    'filterModel' => $searchModel,
                    'formatter' => ['class' => 'yii\i18n\Formatter', 'nullDisplay' => ''],
                    'responsive' => true,
                    'responsiveWrap' => true,
                    'floatHeader' => true,
                    'pjaxSettings' =>[
                        'options'=>[
                            'id'=>'station-grid-pjax',
                        ]
                    ],
                    'floatHeaderOptions' => [
                        'scrollingTop' => '0',
                        'position' => 'absolute',
                        'top' => 56,
                    ],
                    'doubleScroll' => true,
                    'columns' => [
                        [
                            'attribute' => 'id_master',
                            'label' => _tU('id'),
                            'headerOptions' => [
                                'style' => 'min-width: 115px;',
                            ],
                            'filterOptions' => [
                                'style' => 'min-width: 115px;',
                            ],
                            'contentOptions' => [
                                'style' => 'min-width: 115px;',
                            ],
                            'value' => function (Station $model) {
                                return $model->getFormattedId();
                            },
                        ],
                        [
                            'attribute' => 'name',
                            'value' => function ($model) {
                                return $model->getName();
                            },
                        ],
                        [
                            'attribute' => 'type',
                            'filterType' => GridView::FILTER_SELECT2,
                            'filterWidgetOptions' => [
                                'data' => [
                                    'fs' => 'FS',
                                    'wigig' => 'Wigig',
                                    '52ghz' => "5.2",
                                    'ap_58ghz' => "5.8 AP",
                                    'tg_58ghz' => "5.8 TG",
                                ],
                                'hideSearch' => true,

                            ],
                            'headerOptions' => [
                                'style' => 'width: 30%;',
                            ],
                            'filterOptions' => [
                                'style' => 'width: 30%;',
                            ],
                            'contentOptions' => [
                                'style' => 'width: 30%;',
                            ],
                        ],
                        [
                            'attribute' => 'macAddressOrSerialNumber',
                            'label' => $searchModel->getAttributeLabel('macAddressOrSerialNumber'),
                            'encodeLabel' => false,
                            'format' => 'raw',
                            'value' => function ($model) {
                                $result = [];
                                if (!empty($model->mac_address)) {
                                    $result[] = $model->mac_address;
                                }
                                if (!empty($model->serial_number)) {
                                    $result[] = $model->serial_number;
                                }

                                return implode("<br>", $result);
                            },
                            'headerOptions' => [
                                'style' => 'min-width: 180px;',
                            ],
                            'filterOptions' => [
                                'style' => 'min-width: 180px;',
                            ],
                            'contentOptions' => [
                                'style' => 'min-width: 180px;',
                            ],
                        ],
                        [
                            'attribute' => 'id_user',
                            'value' => function (Station $station) {
                                $changeOwnerLink = Html::a(
                                    '<i class="material-icons">edit</i>',
                                    ['change-owner', 'id' => $station->id_master],
                                    'app-admin_station_update',
                                    [
                                        'title' => _tF('change_owner_tooltip', 'station'),
                                        'data-toggle' => 'tooltip',
                                        'data-pjax' => 0,
                                    ]
                                );

                                $userLink = Html::a(
                                    $station->user->getNameAndCompanyName() .
                                    (YII_DEBUG ? "<br>{$station->user->email}" : ''),
                                    ['dc-user/user/index', 'UserSearch' => ['id' => $station->id_user]],
                                    'dc-user_user_index',
                                    [
                                        'data-pjax' => 0,
                                    ]
                                );

                                return $changeOwnerLink . $userLink;
                            },
                            'filterType' => GridView::FILTER_SELECT2,
                            'filterWidgetOptions' => [
                                'data' => function (Station $model) {
                                    return $model->user == null ? [] :
                                        [$model->id_user => $model->user->getNameAndCompanyName()];
                                },
                                'hideSearch' => false,
                                'pluginOptions' => [
                                    'allowClear' => true,
                                    'placeholder' => '',
                                    'ajax' => [
                                        'url' => url(['/dc-user/user/user-dropdown-data2']),
                                        'dataType' => 'json',
                                        'data' => new \yii\web\JsExpression(
                                            <<<JS
                                    function (params) {
                                        return {
                                            term: params.term,
                                            page: params.page || 1
                                        }
                                    }
JS
                                        ),
                                    ],
                                ],
                            ],
                            'headerOptions' => [
                                'style' => 'min-width: 200px;',
                            ],
                            'filterOptions' => [
                                'style' => 'min-width: 200px;',
                            ],
                            'contentOptions' => [
                                'style' => 'min-width: 200px;',
                                'class' => 'col-id_user',
                            ],
                        ],
                        [
                            'attribute' => 'status',
                            'filterType' => GridView::FILTER_SELECT2,
                            'filterWidgetOptions' => [
                                'data' => [
                                    Station::STATUS_DRAFT => _tF("status_" . Station::STATUS_DRAFT, 'station'),
                                    Station::STATUS_WAITING => _tF("status_" . Station::STATUS_WAITING, 'station'),
                                    Station::STATUS_UNPUBLISHED => _tF("status_" . Station::STATUS_UNPUBLISHED, 'station'),
                                    Station::STATUS_FINISHED => _tF("status_" . Station::STATUS_FINISHED, 'station'),
                                    Station::STATUS_EXPIRED => _tF("status_" . Station::STATUS_EXPIRED, 'station'),
                                ],
                                'hideSearch' => true,

                            ],
                            'value' => function ($model) {
                                return BootHtml::badge(_tF("status_{$model->status}", 'station'),
                                    $model->status == Station::STATUS_FINISHED ? BootHtml::TYPE_SUCCESS :
                                        BootHtml::TYPE_DEFAULT);
                            },
                        ],
                        [
                            'attribute' => 'published_by',
                            'filterType' => GridView::FILTER_SELECT2,
                            'filterWidgetOptions' => [
                                'data' => [
                                    Station::PUBLISHED_BY_PUBLICATION => _tF(Station::PUBLISHED_BY_PUBLICATION, 'station'),
                                    Station::PUBLISHED_BY_DECLARATION => _tF(Station::PUBLISHED_BY_DECLARATION, 'station'),
                                ],
                                'hideSearch' => true,

                            ],
                            'value' => function ($model) {
                                return _tF($model->published_by, 'station');
                            },
                        ],
                        [
                            'attribute' => 'lng',
                            'value' => function ($model) {
                                return number_format($model->lng, 6);
                            },
                            'filterType' => GridView::FILTER_SLIDER,
                            'filterWidgetOptions' => [
                                'pluginOptions' => [
                                    'min' => $searchModel::GPS_LNG_MIN,
                                    'max' => $searchModel::GPS_LNG_MAX,
                                    'step' => 0.0000001,
                                    'range' => true,
                                ],
                            ],
                            'headerOptions' => [
                                'style' => 'width: 130px;',
                            ],
                            'filterOptions' => [
                                'style' => 'width: 130px;',
                            ],
                            'contentOptions' => [
                                'style' => 'width: 130px;',
                            ],
                        ],
                        [
                            'attribute' => 'lat',
                            'value' => function ($model) {
                                return number_format($model->lat, 6);
                            },
                            'filterType' => GridView::FILTER_SLIDER,
                            'filterWidgetOptions' => [
                                'pluginOptions' => [
                                    'min' => $searchModel::GPS_LAT_MIN,
                                    'max' => $searchModel::GPS_LAT_MAX,
                                    'step' => 0.0000001,
                                    'range' => true,
                                ],
                            ],
                            'headerOptions' => [
                                'style' => 'width: 130px;',
                            ],
                            'filterOptions' => [
                                'style' => 'width: 130px;',
                            ],
                            'contentOptions' => [
                                'style' => 'width: 130px;',
                            ],
                        ],
                        'antenna_volume',
                        'channel_width',
                        'power',
                        [
                            'attribute' => 'frequency',
                            'label' => _tF('frequency', 'station'),
                            'value' => function ($model) {
                                return $model->getStationTypeObject()->frequency ?? null;
                            },
                        ],
                        [
                            'attribute' => 'direction',
                            'label' => _tF('direction', 'station'),
                            'value' => function ($model) {
                                return $model->getStationTypeObject()->direction ?? null;
                            },
                        ],
                        [
                            'attribute' => 'registered_at',
                            'value' => function ($model) {
                                $date = Yii::$app->formatter->asDate($model->registered_at);
                                return BootHtml::badge($date, BootHtml::TYPE_INFO);
                            },
                            'headerOptions' => [
                                'title' => _tF('registered_at', 'station_hint'),
                                'data-toggle' => 'tooltip',
                                'data-placement' => 'top',
                            ],

                            'label' => _tF('registered_at', 'station'),
                            'filterType' => GridView::FILTER_DATE_RANGE,
                            'filterWidgetOptions' => [
                                'attribute' => 'registeredAt',
                                'pluginOptions' => [
                                    'locale' => ['format' => 'd.m.Y'],
                                ],
                            ],
                        ],
                        [
                            'attribute' => 'valid_to',
                            'value' => function ($model) {
                                $date = Yii::$app->formatter->asDate($model->valid_to);
                                return BootHtml::badge($date, BootHtml::TYPE_INFO);
                            },
                            'headerOptions' => [
                                'title' => _tF('valid_to', 'station_hint'),
                                'data-toggle' => 'tooltip',
                                'data-placement' => 'top',
                            ],

                            'label' => _tF('valid_to', 'station'),
                            'filterType' => GridView::FILTER_DATE_RANGE,
                            'filterWidgetOptions' => [
                                'attribute' => 'validTo',
                                'pluginOptions' => [
                                    'locale' => ['format' => 'd.m.Y'],
                                ],
                            ],
                        ],
                        [
                            'attribute' => 'protected_to',
                            'value' => function ($model) {
                                $date = Yii::$app->formatter->asDate($model->protected_to);
                                return BootHtml::badge($date, BootHtml::TYPE_INFO);
                            },
                            'headerOptions' => [
                                'title' => _tF('protected_to', 'station_hint'),
                                'data-toggle' => 'tooltip',
                                'data-placement' => 'top',
                            ],

                            'label' => _tF('protected_to', 'station'),
                            'filterType' => GridView::FILTER_DATE_RANGE,
                            'filterWidgetOptions' => [
                                'attribute' => 'protectedTo',
                                'pluginOptions' => [
                                    'locale' => ['format' => 'd.m.Y'],
                                ],
                            ],
                        ],
                        [
                            'attribute' => 'created_at',
                            'value' => function (Station $model) {
                                $date = Yii::$app->formatter->asDatetime($model->created_at);
                                return BootHtml::badge($date, BootHtml::TYPE_INFO);
                            },
                            'headerOptions' => [
                                'title' => _tF('created_at', 'station_hint'),
                                'data-toggle' => 'tooltip',
                                'data-placement' => 'top',
                            ],

                            'label' => _tF('created_at'),
                        ],
                        [
                            'attribute' => 'updated_at',
                            'value' => function (Station $model) {
                                $date = Yii::$app->formatter->asDatetime($model->updated_at);
                                return BootHtml::badge($date, BootHtml::TYPE_INFO);
                            },
                            'headerOptions' => [
                                'title' => _tF('updated_at', 'station_hint'),
                                'data-toggle' => 'tooltip',
                                'data-placement' => 'top',
                            ],

                            'label' => _tF('updated_at'),
                        ],
                        [
                            'class' => 'dactylcore\core\widgets\common\gridview\ActionColumn',
                            'template' => '',
                            'contentOptions' => [
                                'style' => 'min-width: 165px;',
                            ],
                        ],
                    ],
                ]); ?>
            </div>
        </div>
    </div>

<?php
$this->registerJs(<<<JS
    $(document).on('pjax:end',   function() {
        var buttons = $('.export-btn');
        buttons.each(function() {
            var button = $(this);
            button.attr('href', button.data('base-url') + window.location.search);
        });
    });
JS
);