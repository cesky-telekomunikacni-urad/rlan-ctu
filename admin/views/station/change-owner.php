<?php
/*
* Copyright 2023 DactylGroup s.r.o.
*
* Licensed under the EUPL, Version 1.2 or – as soon they
will be approved by the European Commission - subsequent
versions of the EUPL (the "Licence");
* You may not use this work except in compliance with the
Licence.
* You may obtain a copy of the Licence at:
*
* https://joinup.ec.europa.eu/software/page/eupl5
*
* Unless required by applicable law or agreed to in
writing, software distributed under the Licence is
distributed on an "AS IS" basis,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
express or implied.
* See the Licence for the specific language governing
permissions and limitations under the Licence.
*/
?>


<?php
// Uses
use dactylcore\core\helpers\Html;
use dactylcore\core\web\AdminView;
use dactylcore\core\helpers\Url;
use dactylcore\core\widgets\admin\activeform\ActiveForm;
use dactylcore\core\widgets\admin\select2\Select2;

// Sources
/* @var $this AdminView */
/* @var $model common\models\Station */

// Settings
$pageId = 'station';
$formId = $pageId . '-form';
$this->title = $model->name . " - " . _tF('owner_change', 'station');
$permission = Yii::$app->controller->module->id . '_station_update';

// Breadcrumbs
$this->breadcrumbs[] = [
    'label' => _tF('stations', 'station'),
    'url' => ['index'],
];

$this->breadcrumbs[] = [
    'label' => "{$model->getFormattedId()} {$model->name}",
    'url' => ['index', 'StationSearch' => ['id_master' => $model->id_master]],
];

$this->breadcrumbs[] = [
    'label' => _tF('owner_change', 'station'),
    'url' => currentUrl(),
];

// Buttons
$this->buttons[] = Html::a(_tF('cancel'), Url::getPreviousIndexUrl(), $permission, [
    'class' => 'btn btn-secondary',
]);

$this->buttons[] = Html::button(_tF('update'), $permission, [
    'type' => 'submit',
    'form' => $formId,
    'class' => 'btn btn-primary',
]);

\admin\assets\StationAsset::register($this);
?>

<?= $this->beginContainer(false, ['id' => $pageId . '-update']); ?>

<?php $form = ActiveForm::begin([
    'id' => $pageId . '-form',
]); ?>


<div class="portlet">
    <div class="body">
        <div class="row">
            <div class="col-md-5">
                <?= $form->field($model, 'name')
                         ->label(_tF('old_owner', 'station'))
                         ->textInput([
                             'disabled' => true,
                             'value' => $model->user->getNameWithMoreInfo(),
                         ]) ?>
            </div>
            <div class="col-md-2 arrow">
                <span class="material-icons">arrow_right_alt</span>
            </div>
            <div class="col-md-5">
                <?= $form->field($model, 'id_user')
                         ->label(_tF('new_owner', 'station'))
                         ->widget(Select2::class, [
                             'hideSearch' => false,
                             'data' => function ($model)
                             {
                                 /* @var $model \dactylcore\page\common\models\Page */
                                 return $model->user == null ? [] : [$model->id_user => $model->user->getNameWithMoreInfo()];
                             },
                             'pluginOptions' => [
                                 'allowClear' => false,
                                 'placeholder' => '',
                                 'ajax' => [
                                     'url' => url(['/dc-user/user/user-dropdown-data2']),
                                     'dataType' => 'json',
                                     'data' => new \yii\web\JsExpression(
                                         <<<JS
                                    function (params) {
                                        return {
                                            term: params.term,
                                            page: params.page || 1
                                        }
                                    }
JS
                                     ),
                                 ],
                             ],
                         ]) ?>
            </div>
        </div>
    </div>
</div>

<div class="form-buttons">
    <?= $form->cancelButton(Url::getPreviousIndexUrl()); ?>
    <?= $form->activeSubmitButton($model); ?>
</div>

<?php ActiveForm::end(); ?>


<?= $this->endContainer() ?>
