<?php
/*
* Copyright 2023 DactylGroup s.r.o.
*
* Licensed under the EUPL, Version 1.2 or – as soon they
will be approved by the European Commission - subsequent
versions of the EUPL (the "Licence");
* You may not use this work except in compliance with the
Licence.
* You may obtain a copy of the Licence at:
*
* https://joinup.ec.europa.eu/software/page/eupl5
*
* Unless required by applicable law or agreed to in
writing, software distributed under the Licence is
distributed on an "AS IS" basis,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
express or implied.
* See the Licence for the specific language governing
permissions and limitations under the Licence.
*/
?>


<?php
// Uses
use dactylcore\core\helpers\Html;
use dactylcore\core\widgets\admin\gridview\GridView;
use dactylcore\core\data\ActiveDataProvider;

// Sources
/* @var $this \dactylcore\core\web\AdminView */
/* @var $dataProvider ActiveDataProvider */

// Settings
$pageId = 'phi-wigig';
$this->title = _tF('phi_wigigs', 'phi-wigig');
$permission = Yii::$app->controller->module->id . '_phi-wigig_create';

// Breadcrumbs
$this->breadcrumbs[] = [
    'label' => $this->title,
    'url' => currentUrl(),
];

// Buttons
$this->buttons[] = Html::a(_tF('create_phi_wigig', 'phi-wigig'), ['create'], $permission, [
    'class' => 'btn btn-primary',
]);
?>

<div class="<?= $pageId ?>-index">
    <div class="portlet">
        <div class="body">
            <?= GridView::widget([
                'id' => $pageId . '-grid',
                'dataProvider' => $dataProvider,
                'responsive' => true,
                'responsiveWrap' => true,
                'floatHeader' => true,
                'floatHeaderOptions' => [
                    'scrollingTop' => '0',
                    'position' => 'absolute',
                    'top' => 56,
                ],
                'doubleScroll' => true,
                'columns' => [
                    'id',
                    'phi',
                    'p',
                    'ad',
                    'ptmp',
                    [
                        'class' => 'dactylcore\core\widgets\common\gridview\ActionColumn',
                        'template' => '{update} {delete}',
                        'contentOptions' => [
                            'style' => 'min-width: 165px;'
                        ]
                    ]
                ]
            ]); ?>
        </div>
    </div>
</div>
