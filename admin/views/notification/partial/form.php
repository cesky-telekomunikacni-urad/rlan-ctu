<?php
/*
* Copyright 2023 DactylGroup s.r.o.
*
* Licensed under the EUPL, Version 1.2 or – as soon they
will be approved by the European Commission - subsequent
versions of the EUPL (the "Licence");
* You may not use this work except in compliance with the
Licence.
* You may obtain a copy of the Licence at:
*
* https://joinup.ec.europa.eu/software/page/eupl5
*
* Unless required by applicable law or agreed to in
writing, software distributed under the Licence is
distributed on an "AS IS" basis,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
express or implied.
* See the Licence for the specific language governing
permissions and limitations under the Licence.
*/
?>


<?php
// Uses
use artkost\yii2\trumbowyg\Trumbowyg;
use dactylcore\core\web\AdminView;
use dactylcore\core\widgets\admin\activeform\ActiveForm;
use dactylcore\core\helpers\Url;
use dactylcore\core\widgets\admin\tinymce\TinyMce;
use kartik\datetime\DateTimePicker;
use yii\helpers\Html;
use common\models\Notification;
use modules\page\elements\wysiwyg\TrumbowygAsset;

// Sources
/* @var $this AdminView */
/* @var $form ActiveForm */
/* @var $formId string */
/* @var $pageId string */
/* @var $model Notification */

$saveButton = Html::button(_tF("save", "default"), [
    "type" => "submit",
    "form" => $formId,
    "class" => "btn btn-primary",
    "value" => "noRedirect",
]);

$this->buttons[] = $saveButton;

TrumbowygAsset::register($this);
?>

<?php $form = ActiveForm::begin([
    'id' => "{$pageId}-form",
]); ?>

    <input name="redirect" type="hidden" value="1" class="redirect">

    <div class="portlet">
        <div class="body">
            <div class="row">
                <div class="col-md-8">
                    <?= Trumbowyg::widget([
                        'id' => 'notification-text',
                        'name' => 'Notification[text]',
                        'value' => $model->text,
                        'settings' => [
                            'autogrow' => true,
                            'plugins' => [
                                'colors',
                                'history',
                            ],
                            'btns' => [
                                ['historyUndo', 'historyRedo'],
                                ['strong', 'em', 'del'],
                                ['link'],
                                ['removeformat'],
                            ],
                        ],
                    ]);
                    ?>
                </div>
                <div class="col-md-4">
                    <?= $form->field($model, 'enabled')->switchInput() ?>

                    <?= $form->field($model, 'displayedFrom')->widget(DateTimePicker::class, $op = [
                        'pluginOptions' => [
                            'format' => 'dd.mm.yyyy hh:ii',
                        ],
                    ]) ?>
                    <?= $form->field($model, 'displayedTo')->widget(DateTimePicker::class, $op) ?>
                </div>
            </div>
        </div>
    </div>

    <div class="form-buttons">
        <?= $form->cancelButton(Url::getPreviousIndexUrl()); ?>
        <?= $model->isNewRecord
            ? $form->activeSubmitButton($model, _tF('save_and_close', 'default'))
            : $form->activeSubmitButton($model, '', _tF('save_and_close', 'default')); ?>
        <?= $saveButton ?>
    </div>

<?php ActiveForm::end(); ?>