<?php
/*
* Copyright 2023 DactylGroup s.r.o.
*
* Licensed under the EUPL, Version 1.2 or – as soon they
will be approved by the European Commission - subsequent
versions of the EUPL (the "Licence");
* You may not use this work except in compliance with the
Licence.
* You may obtain a copy of the Licence at:
*
* https://joinup.ec.europa.eu/software/page/eupl5
*
* Unless required by applicable law or agreed to in
writing, software distributed under the Licence is
distributed on an "AS IS" basis,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
express or implied.
* See the Licence for the specific language governing
permissions and limitations under the Licence.
*/
?>


<?php
// Uses
use dactylcore\core\helpers\Html;
use dactylcore\core\widgets\admin\gridview\GridView;
use dactylcore\core\data\ActiveDataProvider;
use dactylcore\core\widgets\admin\gridview\CheckboxColumn;
use dactylcore\core\widgets\common\gridview\ActionColumn;
use dactylcore\core\widgets\admin\gridview\BooleanColumn;
use dactylcore\core\widgets\admin\gridview\TimestampColumn;
use dactylcore\core\web\AdminView;
use common\models\Notification;
use admin\models\search\NotificationSearch;

// Sources
/* @var $this AdminView */
/* @var $model Notification */
/* @var $searchModel NotificationSearch */
/* @var $dataProvider ActiveDataProvider */

// Settings
$pageId = 'notification';
$this->title = _tF('notifications', 'notification');
$moduleId = Yii::$app->controller->module->id;

// Breadcrumbs
$this->breadcrumbs[] = [
    'label' => $this->title,
    'url' => currentUrl(),
];

// Buttons
$this->buttons[] = Html::a(_tF('create_notification', 'notification'), ['create'], "{$moduleId}_notification_create", [
    'class' => 'btn btn-primary',
]);
?>

<div class="<?= $pageId ?>-index">
    <div class="portlet">
        <div class="body">
            <?= GridView::widget([
                'id' => "{$pageId}-grid",
                'dataProvider' => $dataProvider,
                'filterModel' => $searchModel,
                'formatter' => [
                    'class' => 'yii\i18n\Formatter',
                    'nullDisplay' => '',
                ],
                'responsive' => true,
                'responsiveWrap' => true,
                'floatHeader' => true,
                'floatHeaderOptions' => [
                    'scrollingTop' => '0',
                    'position' => 'absolute',
                    'top' => 56,
                ],
                'doubleScroll' => true,
                'columns' => [
                    'id' => [
                        'attribute' => 'id',
                        'value' => function (Notification $model)
                        {
                            return $model->id;
                        },
                    ],
                    'text' => [
                        'attribute' => 'text',
                        'value' => function (Notification $model)
                        {
                            return stringPreview($model->text, 200);
                        },
                    ],
                    'enabled' => [
                        'class' => BooleanColumn::class,
                        'attribute' => 'enabled',
                    ],
                    'displayed_from' => [
                        'attribute' => 'displayed_from',
                        'value' => function (Notification $model)
                        {
                            return $model->displayedFrom;
                        },
                        'filter' => '',
                    ],
                    'displayed_to' => [
                        'attribute' => 'displayed_to',
                        'value' => function (Notification $model)
                        {
                            return $model->displayedTo;
                        },
                        'filter' => '',
                    ],
                    [
                        'class' => ActionColumn::class,
                        'template' => '{update} {delete}',
                        'contentOptions' => [
                            'style' => 'min-width: 165px;',
                        ],
                    ],
                ],
            ]); ?>
        </div>
    </div>
</div>
