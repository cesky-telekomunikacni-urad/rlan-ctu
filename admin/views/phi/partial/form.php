<?php
/*
* Copyright 2023 DactylGroup s.r.o.
*
* Licensed under the EUPL, Version 1.2 or – as soon they
will be approved by the European Commission - subsequent
versions of the EUPL (the "Licence");
* You may not use this work except in compliance with the
Licence.
* You may obtain a copy of the Licence at:
*
* https://joinup.ec.europa.eu/software/page/eupl5
*
* Unless required by applicable law or agreed to in
writing, software distributed under the Licence is
distributed on an "AS IS" basis,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
express or implied.
* See the Licence for the specific language governing
permissions and limitations under the Licence.
*/
?>


<?php

use dactylcore\core\web\AdminView;
use dactylcore\core\widgets\admin\activeform\ActiveForm;
use dactylcore\core\helpers\Url;

// Sources
/* @var $this AdminView */
/* @var $model common\models\Phi */
/* @var $form ActiveForm */

?>

<?php $form = ActiveForm::begin([
    'id' => $pageId . '-form',
]); ?>


<div class="portlet">
    <div class="body">
        <?= $form->field($model, 'phi12')->textInput(['maxlength' => true]) ?>

        <?= $form->field($model, 'phi34')->textInput(['maxlength' => true]) ?>

        <?= $form->field($model, 'g_phi')->textInput(['maxlength' => true]) ?>

        <?= $form->field($model, 'ad_phi')->textInput(['maxlength' => true]) ?>

        <?= $form->field($model, 'g_min')->textInput(['maxlength' => true]) ?>

        <?= $form->field($model, 'g_max')->textInput(['maxlength' => true]) ?>

        <?= $form->field($model, 'note')->textInput(['maxlength' => true]) ?>

    </div>
</div>

<div class="form-buttons">
    <?= $form->cancelButton(Url::getPreviousIndexUrl()); ?>
    <?= $form->activeSubmitButton($model); ?>    
</div>

<?php ActiveForm::end(); ?>