<?php
/*
* Copyright 2023 DactylGroup s.r.o.
*
* Licensed under the EUPL, Version 1.2 or – as soon they
will be approved by the European Commission - subsequent
versions of the EUPL (the "Licence");
* You may not use this work except in compliance with the
Licence.
* You may obtain a copy of the Licence at:
*
* https://joinup.ec.europa.eu/software/page/eupl5
*
* Unless required by applicable law or agreed to in
writing, software distributed under the Licence is
distributed on an "AS IS" basis,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
express or implied.
* See the Licence for the specific language governing
permissions and limitations under the Licence.
*/
?>


<?php

use common\models\search\ProbeScanWlanSearch;
use dactylcore\core\web\FrontendView;
use dactylcore\core\widgets\admin\gridview\GridView;
use yii\data\ActiveDataProvider;

/* @var $this FrontendView */
/* @var $dataProvider ActiveDataProvider */
/* @var $searchModel ProbeScanWlanSearch */

?>

<?php
# Settings
$pageId = 'probe-scan-wlan';
?>

<?= GridView::widget([
    'id' => $pageId . '-grid',
    'dataProvider' => $dataProvider,
    'filterModel' => $searchModel,
    'tableOptions' => [
        'class' => 'foo-table-spot',
    ],
    'responsive' => true,
    'responsiveWrap' => true,
    'floatHeader' => true,
    'floatHeaderOptions' => [
        'scrollingTop' => '0',
        'position' => 'absolute',
        'top' => 56,
    ],
    'doubleScroll' => true,
    'headerRowOptions' => ['class' => 'footable-header'],
    'filterRowOptions' => ['class' => 'footable-disabled'],
    'pjaxSettings' => [
        'options' => [
            'forceType' => 'GET',
            'enablePushState' => true,
            'enableReplaceState' => false,
        ],
    ],
    'panel' => [
        'type' => GridView::TYPE_DEFAULT,
    ],
    'panelTemplate' => <<< HTML
<div class="panel {type}">
    {items}
</div>
HTML
    ,
    'columns' => [
        [
            'label' => _tF('probe', 'probe'),
            'attribute' => 'name',
            'value' => function ($model) {
                return $model->probeName;
            }
        ],
        [
            'label' => _tF('address', 'probe'),
            'headerOptions' => ['data-breakpoints' => 'xs'],
            'attribute' => 'address',
            'value' => function ($model) {
                return $model->scanAddress;
            }
        ],
        [
            'label' => _tF('time', 'probe'),
            'headerOptions' => ['data-breakpoints' => 'xs'],
            'attribute' => 'time',
            'value' => function ($model) {
                return $model->scanTime;
            }
        ],
        [
            'headerOptions' => ['data-breakpoints' => 'xs'],
            'attribute' => 'bssid',
        ],
        [
            'headerOptions' => ['data-breakpoints' => 'xs'],
            'attribute' => 'rssi',
        ],
        [
            'headerOptions' => ['data-breakpoints' => 'xs'],
            'attribute' => 'ssid',
        ],
        [
            'headerOptions' => ['data-breakpoints' => 'xs'],
            'attribute' => 'capabilities',
        ],
        [
            'headerOptions' => ['data-breakpoints' => 'xs'],
            'attribute' => 'frequency',
        ],
    ],
]); ?>
