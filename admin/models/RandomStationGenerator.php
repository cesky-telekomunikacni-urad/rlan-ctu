<?php
/*
* Copyright 2023 DactylGroup s.r.o.
*
* Licensed under the EUPL, Version 1.2 or – as soon they
will be approved by the European Commission - subsequent
versions of the EUPL (the "Licence");
* You may not use this work except in compliance with the
Licence.
* You may obtain a copy of the Licence at:
*
* https://joinup.ec.europa.eu/software/page/eupl5
*
* Unless required by applicable law or agreed to in
writing, software distributed under the Licence is
distributed on an "AS IS" basis,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
express or implied.
* See the Licence for the specific language governing
permissions and limitations under the Licence.
*/
?>


<?php

namespace admin\models;

use common\models\Station;
use common\models\Station52;
use common\models\Station58;
use common\models\StationFs;
use common\models\StationFsPair;
use common\models\StationWigig;
use frontend\controllers\StationController;
use Yii;
use yii\helpers\Json;

/**
 * Class for generating stations. It can be used only in testing env.
 * No other use is available!!!
 */
class RandomStationGenerator
{
    /**
     * Function for generating stations but only in case of API testing!
     * This func can be called only in API testing env!
     *
     * @param int $idUser
     *
     * @return array
     */
    public static function generateRandomStations(int $idUser): array
    {
        // Only in API testing environment we need generate stations!
        if (!env("API_TESTING_ENVIRONMENT")) {
            return [];
        }

        $today = date('Y-m-d');

        // First 5 stations are special

        $t = Yii::$app->db->beginTransaction();

        // Expired Wigig
        $stations[0] = self::buildWigig(
            $idUser, self::getRandomMAC(), self::getRandomName(0), Station::STATUS_EXPIRED,
            date('U', strtotime($today . ' - 4 days')),
            date('U', strtotime($today . ' - 1 days')),
        );

        // Expired FS
        $stations[1] = self::buildFs(
            $idUser, self::getRandomMAC(), self::getRandomMAC(1),
            self::getRandomName(1), Station::STATUS_EXPIRED,
            date('U', strtotime($today . ' - 4 days')),
            date('U', strtotime($today . ' - 1 days')),
        );

        // Close to expire 58
        $stations[2] = self::build58(
            $idUser, self::getRandomMAC(), self::getRandomName(2), Station::STATUS_FINISHED,
            date('U', strtotime($today . ' - 2 days')),
            date('U', strtotime($today . ' + 2 days')),
        );

        // Unpublished 58
        $stations[3] = self::build52(
            $idUser, self::getRandomMAC(), self::getRandomName(3), Station::STATUS_UNPUBLISHED
        );

        // Other 5 are normal and finished
        $stations[4] = self::buildWigig($idUser, self::getRandomMAC(), self::getRandomName(4));
        $stations[5] = self::buildFs(
            $idUser, self::getRandomMAC(), self::getRandomMAC(1), self::getRandomName(5)
        );
        $stations[6] = self::build58($idUser, self::getRandomMAC(), self::getRandomName(6));
        $stations[7] = self::build52($idUser, self::getRandomMAC(), self::getRandomName(7));

        if (count($stations) != count(array_filter($stations))) {
            $t->rollBack();
            return [];
        }

        $t->commit();
        return $stations;
    }

    protected static function getRandomName(int $number): string
    {
        return mb_strtoupper(substr(md5(mt_rand()), 0, 3)) . '_GENERATED_' . $number;
    }

    protected static function getRandomMAC($incrementAddition = 0): string
    {
        $incrementMacFunc = function ($mac_address, $increment = 1, $return_partition = ':')
        {
            $mac_address = preg_replace('![^0-9a-f]!', '', $mac_address);

            $parts = str_split($mac_address, 2);
            foreach ($parts as $i => $hex) {
                $parts[$i] = hexdec($hex);
            }

            $increase = true;
            if ($increment < 0) {
                $increase = false;
            }

            $parts[5] += $increment;
            for ($i = 5; $i >= 1; $i--) {
                if ($increase) {
                    while ($parts[$i] > 255) {
                        $parts[$i] -= 256;
                        $parts[$i - 1] += 1;
                    }
                } else {
                    while ($parts[$i] < 0) {
                        $parts[$i] += 256;
                        $parts[$i - 1] -= 1;
                    }
                }
            }

            foreach ($parts as $i => $dec) {
                $parts[$i] = str_pad(dechex($dec), 2, 0, STR_PAD_LEFT);
            }

            return implode($return_partition, $parts);
        };

        $lastId = Station::find()->max("id");

        $lastId = (is_null($lastId)) ? "0" : $lastId;
        $lastId = (int)$lastId;
        $lastId++;

        $result = $incrementMacFunc('00:00:00:00:00:00', $lastId + $incrementAddition);

        return $result;
    }

    protected static function getRandomLatLng(): array
    {
        return [
            'lng' => mt_rand(
                    (Station::BOUNDARIES_WEST + 1) * 100000000,
                    (Station::BOUNDARIES_EAST - 1) * 100000000)
                / 100000000,
            'lat' => mt_rand(
                    (Station::BOUNDARIES_SOUTH + 0.7) * 100000000,
                    (Station::BOUNDARIES_NORTH - 0.7) * 100000000)
                / 100000000,
        ];
    }

    protected static function buildStation(
        array $data,
        int $idUser,
        string $mac,
        string $name,
        string $status = Station::STATUS_FINISHED,
        string $validTo = null,
        string $protectedTo = null
    ): ?Station {

        $data['station']['id_user'] = $idUser;
        $data['station']['mac_address'] = $mac;
        $data['station']['name'] = $name;
        $randGps = self::getRandomLatLng();
        $data['station']['lng'] = $randGps['lng'];
        $data['station']['lat'] = $randGps['lat'];

        $station = new Station();
        $today = date('Y-m-d');
        $station->setAttributes($data['station']);

        if (!$validTo) {
            $validTo = date('U', strtotime($today . ' + 3 days'));
        }
        if (!$protectedTo) {
            $protectedTo = date('U', strtotime($today . ' + 5 days'));
        }

        $station->valid_to = $validTo;
        $station->protected_to = $protectedTo;

        $station->status = $status;
        if (!$station->save()) {
            Yii::error("Cannot generate station with error: {$station->getFirstErrorMessage()}, attr=" .
                Json::encode($station->attributes));
            return null;
        }
        return $station;
    }

    protected static function buildWigig(
        int $idUser,
        string $mac,
        string $name,
        string $status = Station::STATUS_FINISHED,
        string $validTo = null,
        string $protectedTo = null
    ): ?int {
        $data = json_decode(file_get_contents(__DIR__ . '/fixtures/stationWigig.json'), true);

        $transaction = Yii::$app->db->beginTransaction();
        $station = self::buildStation($data, $idUser, $mac, $name, $status, $validTo, $protectedTo);
        if (is_null($station)) {
            $transaction->rollBack();
            return null;
        }

        $wigig = new StationWigig();
        $wigig->setAttributes($data['wigig']);

        $wigig->id_station = $station->id;
        $wigig->save();
        if (!$wigig->save()) {
            $transaction->rollBack();
            Yii::error("Cannot generate station with error: {$wigig->getFirstErrorMessage()}");
            return null;
        }

        if ($status !== Station::STATUS_EXPIRED) {
            $station->moveToStatus($status);
        }
        $transaction->commit();

        return $station->id;
    }

    protected static function buildFs(
        int $idUser,
        string $macA,
        string $macB,
        string $name,
        string $status = Station::STATUS_FINISHED,
        string $validTo = null,
        string $protectedTo = null
    ): ?int {
        $data = json_decode(file_get_contents(__DIR__ . '/fixtures/stationFs.json'), true);

        $data['a']['id_user'] = $idUser;
        $data['b']['id_user'] = $idUser;
        $data['a']['mac_address'] = $macA;
        $data['b']['mac_address'] = $macB;
        $data['a']['name'] = $name;
        $data['b']['name'] = $name;
        $randGps = self::getRandomLatLng();
        $data['a']['lng'] = $randGps['lng'];
        $data['a']['lat'] = $randGps['lat'];
        $data['b']['lng'] = $randGps['lng'] + 0.03;
        $data['b']['lat'] = $randGps['lat'] - 0.004;

        $transaction = Yii::$app->db->beginTransaction();
        $pair = new StationFsPair();
        $pair->initNew();
        $pair->setData([Station::FORM_NAME => [Station::POSITION_A => $data['a'], Station::POSITION_B => $data['b'],]]);
        $today = date('Y-m-d');

        if (!$validTo) {
            $validTo = date('U', strtotime($today . ' + 1 days'));
        }
        if (!$protectedTo) {
            $protectedTo = date('U', strtotime($today . ' + 2 days'));
        }

        $pair->stationA->valid_to = $validTo;
        $pair->stationB->valid_to = $validTo;

        $pair->stationA->protected_to = $protectedTo;
        $pair->stationB->protected_to = $protectedTo;

        $pair->stationA->status = $status;
        $pair->stationB->status = $status;
        $pair->stationB->pointerToPairStation = $pair->stationA;
        $pair->stationA->pointerToPairStation = $pair->stationB;
        if (!$pair->save()) {
            $transaction->rollBack();
            Yii::error("Cannot generate station with error: {$pair->stationA->getFirstErrorMessage()} | {$pair->stationB->getFirstErrorMessage()}");
            return null;
        }

        $pair->setFsData([
            StationFs::FORM_NAME => [
                Station::POSITION_A => $data['typeA'],
                Station::POSITION_B => $data['typeB'],
            ],
        ], StationController::STEP_1_NAME);
        $pair->setFsData([
            StationFs::FORM_NAME => [
                Station::POSITION_A => $data['typeA'],
                Station::POSITION_B => $data['typeB'],
            ],
        ], StationController::STEP_3_PARAMS);

        if (!$pair->FsSave()) {
            $transaction->rollBack();
            Yii::error("Cannot generate station with error: {$pair->stationA->typeStation->getFirstErrorMessage()} | {$pair->stationB->typeStation->getFirstErrorMessage()}");
            return null;
        }

        if ($status !== Station::STATUS_EXPIRED) {
            $pair->stationA->moveToStatus($status);
            $pair->stationB->moveToStatus($status);
        }
        $transaction->commit();

        return $pair->stationA->id;
    }

    protected static function build58(
        int $idUser,
        string $mac,
        string $name,
        string $status = Station::STATUS_FINISHED,
        string $validTo = null,
        string $protectedTo = null
    ): ?int {
        $data = json_decode(file_get_contents(__DIR__ . '/fixtures/station58.json'), true);

        $transaction = Yii::$app->db->beginTransaction();
        $station = self::buildStation($data, $idUser, $mac, $name, $status, $validTo, $protectedTo);
        if (is_null($station)) {
            $transaction->rollBack();
            return null;
        }

        $obj58 = new Station58();
        $obj58->setAttributes($data['58']);

        $obj58->id_station = $station->id;
        if (!$obj58->save()) {
            $transaction->rollBack();
            Yii::error("Cannot generate station with error: {$obj58->getFirstErrorMessage()}");
            return null;
        }

        if ($status !== Station::STATUS_EXPIRED) {
            $station->moveToStatus($status);
        }
        $transaction->commit();

        return $station->id;
    }

    protected static function build52(
        int $idUser,
        string $mac,
        string $name,
        string $status = Station::STATUS_FINISHED,
        string $validTo = null,
        string $protectedTo = null
    ): ?int {
        $data = json_decode(file_get_contents(__DIR__ . '/fixtures/station52.json'), true);

        $transaction = Yii::$app->db->beginTransaction();
        $station = self::buildStation($data, $idUser, $mac, $name, $status, $validTo, $protectedTo);
        if (is_null($station)) {
            $transaction->rollBack();
            return null;
        }

        $obj52 = new Station52();
        $obj52->setAttributes($data['52']);

        $obj52->id_station = $station->id;
        if (!$obj52->save()) {
            $transaction->rollBack();
            Yii::error("Cannot generate station with error: {$obj52->getFirstErrorMessage()}");
            return null;
        }

        if ($status !== Station::STATUS_EXPIRED) {
            $station->moveToStatus($status);
        }
        $transaction->commit();

        return $station->id;
    }
}
