<?php
/*
* Copyright 2023 DactylGroup s.r.o.
*
* Licensed under the EUPL, Version 1.2 or – as soon they
will be approved by the European Commission - subsequent
versions of the EUPL (the "Licence");
* You may not use this work except in compliance with the
Licence.
* You may obtain a copy of the Licence at:
*
* https://joinup.ec.europa.eu/software/page/eupl5
*
* Unless required by applicable law or agreed to in
writing, software distributed under the Licence is
distributed on an "AS IS" basis,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
express or implied.
* See the Licence for the specific language governing
permissions and limitations under the Licence.
*/
?>


<?php

namespace admin\models;

use common\models\Station;
use dactylcore\log\console\tasks\RemoveExportFileJob;

class ExportStationsInRadiusForm extends \dactylcore\core\base\Model
{
    public $lng;
    public $lat;
    public $radius;

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['lat', 'lng', 'radius'], 'required'],
            [
                ['lng', 'lat', 'radius'],
                'number',
                'message' => _tF('must be a number separated by point', 'station'),
            ],

            [
                'lat',
                'compare',
                'operator' => '<=',
                'compareValue' => Station::BOUNDARIES_NORTH,
                'message' => _tF('latitude has to be in czechia', 'station'),
            ],
            [
                'lat',
                'compare',
                'operator' => '>=',
                'compareValue' => Station::BOUNDARIES_SOUTH,
                'message' => _tF('latitude has to be in czechia', 'station'),
            ],

            [
                'lng',
                'compare',
                'operator' => '<=',
                'compareValue' => Station::BOUNDARIES_EAST,
                'message' => _tF('longitude has to be in czechia', 'station'),
            ],
            [
                'lng',
                'compare',
                'operator' => '>=',
                'compareValue' => Station::BOUNDARIES_WEST,
                'message' => _tF('longitude has to be in czechia', 'station'),
            ],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return merge(parent::attributeLabels(), [
            'lng' => _tF('lng', 'station'),
            'lat' => _tF('lat', 'station'),
            'radius' => _tF('radius', 'station'),
        ]);
    }

    public function generateCSV()
    {
        //
        // Prepare file
        $filename = "export_" . date('Y-m-d_His');
        $ext = 'csv';
        $hash = \Yii::$app->security->generateRandomString(6);
        $filepath = \Yii::getAlias('@uploads') . "/exports/{$filename}_{$hash}.{$ext}";

        //
        // Prepare data
        $query = Station::find()
                        ->select([
                            'station.id',
                            'station.lat',
                            'station.lng',
                            'station.id_station_pair',
                            'station.id_master',
                            'LPAD(station.id_master, 7, 0) AS idMasterFormatted',
                            'station.type AS stationType',
                            'station.name AS stationName',
                            'station.id_user',
                            'TRIM(  CONCAT(  IFNULL(user.first_name,""), " ", IFNULL(user.last_name,""), " ", IFNULL(user.companyName,""))  ) AS userFullname',
                            'user.email AS userEmail',
                        ])
                        ->joinWith('user', false)
                        ->where(['station.status' => Station::STATUS_FINISHED]) // TODO .. ma to fungovat jen na 5.8ghz stanice, ty ale nemame jeste poradne rozjete, ergo davam tu todo aby se tam pozdeji doplnil typ
                        ->andWhere([
                '<',
                "ACOS(COS(RADIANS({$this->lat})) * COS(RADIANS(`station`.`lat`)) * COS(RADIANS({$this->lng}) - RADIANS(`station`.`lng`))  + SIN(RADIANS({$this->lat})) * SIN(RADIANS(`station`.`lat`))  ) * 6371",
                $this->radius,
            ])
                        ->asArray()
                        ->groupBy('station.id_master')
                        ->orderBy('station.id_user ASC, station.id_master ASC');

        //
        // Group data by user
        $groupedByUser = [];
        foreach ($query->each(1000) as $r) {
            $r = (object)$r;

            if (!isset($groupedByUser[$r->id_user])) {
                $groupedByUser[$r->id_user] = [];
            }

            $groupedByUser[$r->id_user][] = $r;
        }

        //
        // Generate CSV
        $delimiter = ',';
        $i = 1;
        file_put_contents($filepath, "\xEF\xBB\xBF" . implode($delimiter, [
                '#',
                'Owner ID',
                'Owner',
                'Email',
                'Stations',
            ]) . "\n");
        $fp = fopen($filepath, 'a');
        foreach ($groupedByUser as $idUser => $stations) {
            $stationsIds = map($stations, 'idMasterFormatted', 'idMasterFormatted', true);

            $rowCols = [
                $i,
                (string)$stations[0]->id_user,
                (string)$stations[0]->userFullname,
                (string)$stations[0]->userEmail,
                (string)implode(',', $stationsIds),
            ];
            fputcsv($fp, $rowCols, $delimiter);

            $i++;
        }
        fclose($fp);

        $fileurl = domain() . "/uploads/exports/" . basename($filepath);

        // Remove file after time
        \Yii::$app->queue->delay(env('EXPORTS_TTL_MIN', 30) * 60)->push(new RemoveExportFileJob([
            'filePath' => $filepath,
        ]));

        return $fileurl;
    }
}
