<?php
/*
* Copyright 2023 DactylGroup s.r.o.
*
* Licensed under the EUPL, Version 1.2 or – as soon they
will be approved by the European Commission - subsequent
versions of the EUPL (the "Licence");
* You may not use this work except in compliance with the
Licence.
* You may obtain a copy of the Licence at:
*
* https://joinup.ec.europa.eu/software/page/eupl5
*
* Unless required by applicable law or agreed to in
writing, software distributed under the Licence is
distributed on an "AS IS" basis,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
express or implied.
* See the Licence for the specific language governing
permissions and limitations under the Licence.
*/
?>


<?php

namespace admin\models\search;

use dactylcore\core\admin\traits\TSearchModel;
use dactylcore\core\db\ActiveQuery;
use common\models\UserAlliance;

/**
 * UserAllianceSearch represents the model behind the search form of `common\models\UserAlliance`.
 */
class UserAllianceSearch extends UserAlliance
{
    use TSearchModel;

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id', 'deleted', 'created_at', 'updated_at'], 'integer'],
            [['name', 'description'], 'safe'],
        ];
    }

    /**
     * Creates search query for active data provider based on params
     *
     * @param array $params
     * @return ActiveQuery
     * @throws \Throwable
     */
    protected function getQuery($params): ActiveQuery
    {
        $query = UserAlliance::find();

        $this->load($params);

        if (!$this->validate()) {
            return $query;
        }

        $query->andFilterWhere([
            'id' => $this->id,
        ]);

        $query->andFilterWhere(['like', 'name', $this->name])
            ->andFilterWhere(['like', 'description', $this->description]);

        return $query;
    }
}
