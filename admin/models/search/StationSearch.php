<?php
/*
* Copyright 2023 DactylGroup s.r.o.
*
* Licensed under the EUPL, Version 1.2 or – as soon they
will be approved by the European Commission - subsequent
versions of the EUPL (the "Licence");
* You may not use this work except in compliance with the
Licence.
* You may obtain a copy of the Licence at:
*
* https://joinup.ec.europa.eu/software/page/eupl5
*
* Unless required by applicable law or agreed to in
writing, software distributed under the Licence is
distributed on an "AS IS" basis,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
express or implied.
* See the Licence for the specific language governing
permissions and limitations under the Licence.
*/
?>


<?php

namespace admin\models\search;

use common\models\StationFsPair;
use dactylcore\core\db\ActiveQuery;
use frontend\controllers\StationController;
use Yii;
use yii\base\Model;
use dactylcore\core\data\ActiveDataProvider;
use yii\helpers\ArrayHelper;
use common\models\Station;

class StationSearch extends \common\models\search\StationSearch
{
    public const GPS_LNG_MIN = 12;
    public const GPS_LNG_MAX = 19;
    public const GPS_LAT_MIN = 47;
    public const GPS_LAT_MAX = 52;
    //
    public $macAddressOrSerialNumber;

    public $direction = null;
    public $frequency = null;

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [
                ['id', 'id_user', 'registered_at', 'valid_to', 'protected_to', 'deleted', 'created_at', 'updated_at'],
                'integer',
            ],
            [['mac_address', 'serial_number', 'lng', 'lat'], 'string'],
            [
                [
                    'type',
                    'status',
                    'published_by',
                    'name',
                    'registeredAt',
                    'validTo',
                    'protectedTo',
                    'hasConflicts',
                    'typeShortCut',
                ],
                'safe',
            ],
            [['antenna_volume', 'channel_width', 'power', 'frequency'], 'number'],
            [['id_master', 'direction'], 'integer'],
            ['macAddressOrSerialNumber', 'string'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return merge(parent::attributeLabels(), [
            'macAddressOrSerialNumber' => _tF('mac_address', 'station') . ' /<br>' . _tF('serial_number', 'station'),
        ]);
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @param array $filter
     *
     * @return ActiveDataProvider
     */
    public function search($params, $filter = [])
    {
        $query = Station::find()
            ->joinWith('stationWigig', false)
            ->joinWith('stationFs', false)
            ->addSelect('station.*, station_wigig.direction, station_fs.frequency');

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            return $dataProvider;
        }

        if ($this->typeShortCut) {
            if ($this->typeShortCut == 'fs') {
                $query->andFilterWhere(['type' => 'fs']);
            } else {
                $query->leftJoin('station_wigig', 'station_wigig.id_station=station.id')
                    ->andFilterWhere(['station_wigig.is_ptmp' => ($this->typeShortCut == 'wigig_ptmp')]);
            }
        }

        $query->andFilterWhere([
            'station.id_user' => $this->id_user,
            'station.antenna_volume' => $this->antenna_volume,
            'station.published_by' => $this->published_by,
            'station.channel_width' => $this->channel_width,
            'station.power' => $this->power,
            'station.registered_at' => $this->registered_at,
            'station.valid_to' => $this->valid_to,
            'station.protected_to' => $this->protected_to,
            'station.id' => $this->id,
            'station.id_master' => $this->id_master,
            'station_wigig.direction' => $this->direction,
            'station_fs.frequency' => $this->frequency,
        ]);

        $query->andFilterWhere(['like', 'station.type', $this->type])
            ->andFilterWhere(['like', 'station.status', $this->status])
            ->andFilterWhere(['like', 'station.name', $this->name]);

        if ($this->macAddressOrSerialNumber) {
            $query->andFilterWhere([
                'OR',
                ['like', 'station.mac_address', $this->macAddressOrSerialNumber],
                ['like', 'station.serial_number', $this->macAddressOrSerialNumber],
            ]);
        }

        if ($this->lng) {
            $tmp = explode(',', $this->lng);
            $query->andFilterWhere(['>=', 'station.lng', (float)$tmp[0]]);
            $query->andFilterWhere(['<=', 'station.lng', (float)$tmp[1]]);
        }
        if ($this->lat) {
            $tmp = explode(',', $this->lat);
            $query->andFilterWhere(['>=', 'station.lat', (float)$tmp[0]]);
            $query->andFilterWhere(['<=', 'station.lat', (float)$tmp[1]]);
        }

        $this->filterDateRange($query, 'registeredAt', 'station.registered_at', 'd.m.Y');
        $this->filterDateRange($query, 'validTo', 'station.valid_to', 'd.m.Y');
        $this->filterDateRange($query, 'protectedTo', 'station.protected_to', 'd.m.Y');

        return $dataProvider;
    }
}
