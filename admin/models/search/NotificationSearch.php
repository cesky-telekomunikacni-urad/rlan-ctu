<?php
/*
* Copyright 2023 DactylGroup s.r.o.
*
* Licensed under the EUPL, Version 1.2 or – as soon they
will be approved by the European Commission - subsequent
versions of the EUPL (the "Licence");
* You may not use this work except in compliance with the
Licence.
* You may obtain a copy of the Licence at:
*
* https://joinup.ec.europa.eu/software/page/eupl5
*
* Unless required by applicable law or agreed to in
writing, software distributed under the Licence is
distributed on an "AS IS" basis,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
express or implied.
* See the Licence for the specific language governing
permissions and limitations under the Licence.
*/
?>


<?php

namespace admin\models\search;

use dactylcore\core\admin\traits\TSearchModel;
use dactylcore\core\db\ActiveQuery;
use common\models\Notification;

/**
 * NotificationSearch represents the model behind the search form of `common\models\Notification`.
 */
class NotificationSearch extends Notification
{
    use TSearchModel;

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id', 'displayed_from', 'displayed_to', 'enabled', 'created_at', 'updated_at'], 'integer'],
            [['text'], 'safe'],
            [['createdAt', 'updatedAt'], 'string'],
        ];
    }

    /**
     * Creates search query for active data provider based on params
     *
     * @param array $params
     * @return ActiveQuery
     * @throws \Throwable
     */
    protected function getQuery($params): ActiveQuery
    {
        $query = Notification::find();

        $this->load($params);

        if (!$this->validate()) {
            return $query;
        }

        $query->andFilterWhere([
            'id' => $this->id,
            'displayed_from' => $this->displayed_from,
            'displayed_to' => $this->displayed_to,
            'enabled' => $this->enabled,
        ]);

        $query->andFilterWhere(['like', 'text', $this->text]);

        $query->filterDateRange($this->createdAt, 'created_at');
        $query->filterDateRange($this->updatedAt, 'updated_at');

        return $query;
    }
}
