<?php
/*
* Copyright 2023 DactylGroup s.r.o.
*
* Licensed under the EUPL, Version 1.2 or – as soon they
will be approved by the European Commission - subsequent
versions of the EUPL (the "Licence");
* You may not use this work except in compliance with the
Licence.
* You may obtain a copy of the Licence at:
*
* https://joinup.ec.europa.eu/software/page/eupl5
*
* Unless required by applicable law or agreed to in
writing, software distributed under the Licence is
distributed on an "AS IS" basis,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
express or implied.
* See the Licence for the specific language governing
permissions and limitations under the Licence.
*/
?>


<?php

namespace admin\controllers;

use Yii;
use yii\web\NotFoundHttpException;

use dactylcore\core\web\AdminController;
use dactylcore\core\admin\traits\TAdminCrudActions;
use admin\models\search\NotificationSearch;
use common\models\Notification;

/**
 * NotificationController implements the CRUD actions for Notification model. */
class NotificationController extends AdminController
{
    /**
     * Contains all basic CRUD actions definitions
     */
    use TAdminCrudActions;

    /**
     * @property string $modelClass
     */
    public $modelClass = Notification::class;
    
    /**
     * @property string $searchModelClass
     */
    public $searchModelClass = NotificationSearch::class;
    
    /**
     * @property string $translationCategory
     */
    public $translationCategory = 'notification';
}
