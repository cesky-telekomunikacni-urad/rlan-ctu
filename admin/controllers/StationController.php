<?php
/*
* Copyright 2023 DactylGroup s.r.o.
*
* Licensed under the EUPL, Version 1.2 or – as soon they
will be approved by the European Commission - subsequent
versions of the EUPL (the "Licence");
* You may not use this work except in compliance with the
Licence.
* You may obtain a copy of the Licence at:
*
* https://joinup.ec.europa.eu/software/page/eupl5
*
* Unless required by applicable law or agreed to in
writing, software distributed under the Licence is
distributed on an "AS IS" basis,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
express or implied.
* See the Licence for the specific language governing
permissions and limitations under the Licence.
*/
?>


<?php

namespace admin\controllers;

use admin\models\ExportStationsInRadiusForm;
use admin\models\search\StationSearch;
use common\models\Station;
use console\tasks\ExportAllStationsJob;
use yii\base\DynamicModel;
use yii\web\Response;
use dactylcore\log\common\components\LogArchiveComponent;
use dactylcore\user\admin\models\User;
use Yii;
use dactylcore\core\web\AdminController;
use yii\db\QueryInterface;
use yii\filters\AccessControl;
use yii\web\NotFoundHttpException;

/**
 * StationController implements the CRUD actions for Station model.
 */
class StationController extends AdminController
{
    public function behaviors()
    {
        return \yii\helpers\ArrayHelper::merge(parent::behaviors(), [
            'access' => [
                'class' => AccessControl::class,
                'rules' => [
                    [
                        'allow' => true,
                        'actions' => ['change-owner', 'move-selected-to-owner'],
                        'matchCallback' => function ($rule, $action) {
                            return hasAccessTo('app-admin_station_update');
                        },
                    ],
                    [
                        'allow' => true,
                        'actions' => ['export-stations-in-radius', 'export-xls', 'export-csv'],
                        'matchCallback' => function ($rule, $action) {
                            return hasAccessTo('app-admin_station_index');
                        },
                    ],
                ],
            ],
        ]);
    }

    /**
     * Lists all Station models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new StationSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * @return mixed
     */
    public function actionExportStationsInRadius()
    {
        $formModel = new ExportStationsInRadiusForm();

        if ($formModel->load(Yii::$app->request->get()) && $formModel->validate()) {

            $exportedFileurl = $formModel->generateCSV();

            setSuccessFlash(_tF('successfully_exported', 'station'));

            return $this->render('export-stations-in-radius', [
                'formModel' => $formModel,
                'exportedFileurl' => $exportedFileurl,
            ]);
        }

        return $this->render('export-stations-in-radius', [
            'formModel' => $formModel,
        ]);
    }

    /**
     * Creates a new Station model.
     * If creation is successful, the browser will be redirected to the 'index' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = $this->getModel();

        if (\Yii::$app->request->isPost) {
            if ($this->save($model)) {
                return $this->redirectToPreviousIndex();
            }
        }

        return $this->render('create', [
            'model' => $model,
        ]);
    }

    /**
     * Finds the Station model based on its primary key value or create new if id is null.
     * If the model is not found, a 404 HTTP exception will be thrown.
     *
     * @param integer $id
     *
     * @return Station the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function getModel(int $id = null)
    {
        if (is_null($id)) {
            return (new Station());
        }

        $model = Station::find()->andWhere(['id' => $id])->one();
        if ($model !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException(_t('The requested page does not exist.'));
        }
    }

    /**
     * Saves Station model filled with post data.
     *
     * @param Station $model saved model
     *
     * @return bool success or not
     * @throws \yii\db\Exception database exception
     */
    protected function save(Station &$model): bool
    {
        $data = Yii::$app->request->post();

        if ($model->load($data)) {
            $isNewRecord = $model->isNewRecord;
            $transaction = Yii::$app->db->beginTransaction();

            if ($model->save()) {
                $transaction->commit();

                if ($isNewRecord) {
                    setSuccessFlash(_tF('flash_successfully_created', 'station'));
                } else {
                    setSuccessFlash(_tF('flash_successfully_updated', 'station'));
                }

                return true;
            }

            $transaction->rollBack();
        } else {
            if (!$model->hasErrors()) {
                setErrorFlash(_tF('flash_error'));
            }
        }

        return false;
    }

    /**
     * Updates an existing Station model.
     * If update is successful, the browser will be redirected to the 'index' page.
     *
     * @param integer $id
     *
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->getModel($id);

        if (\Yii::$app->request->isPost) {
            if ($this->save($model)) {
                return $this->redirectToPreviousIndex();
            }
        }

        return $this->render('update', [
            'model' => $model,
        ]);
    }

    /**
     * Changes owner for existing Station model.
     * If update is successful, the browser will be redirected to the 'index' page.
     *
     * @param integer $id
     *
     * @return mixed
     */
    public function actionChangeOwner($id)
    {
        $model = $this->getModel($id);

        if (\Yii::$app->request->isPost) {

            if ($model->load(Yii::$app->request->post())) {
                $transaction = Yii::$app->db->beginTransaction();

                $models = $model::findAll(['id_master' => $model->id_master]);
                $allSaved = true;
                foreach ($models as $m) {
                    $m->id_user = $model->id_user;
                    $saved = $m->save(true, ['id_user', 'updated_at']);
                    $allSaved &= $saved;

                    if (!$saved) {
                        Yii::error('Cannot save new station owner: ' . json_encode($m->errors));
                    }
                }

                if ($allSaved) {
                    $transaction->commit();
                    setSuccessFlash(_tF('flash_successfully_updated', 'station'));
                    return $this->redirectToPreviousIndex();;
                } else {
                    setErrorFlash(_tF('flash_error'));
                }

                $transaction->rollBack();
            } else {
                if (!$model->hasErrors()) {
                    setErrorFlash(_tF('flash_error'));
                }
            }
        }

        return $this->render('change-owner', [
            'model' => $model,
        ]);
    }

    /**
     * Deletes an existing Station model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     *
     * @param integer $id
     *
     * @return mixed
     */
    public function actionDelete($id)
    {
        $model = $this->getModel($id);

        if ($model->delete()) {
            setSuccessFlash(_tF('flash_successfully_deleted', 'station'));
            return true;
        } else {
            if (!$model->hasErrors()) {
                setErrorFlash(_tF('flash_error'));
            } else {
                setErrorFlash($model->getFirstErrorMessage());
            }
        }

        return false;
    }

    /**
     * @return bool
     */
    public function actionExportXls()
    {
        $searchModel = new StationSearch();
        $dataProvider = $searchModel->search(\Yii::$app->request->queryParams);

        return $this->export(LogArchiveComponent::FILE_TYPE_XLSX, $dataProvider->query);
    }

    /**
     * @return bool
     */
    public function actionExportCsv()
    {
        $searchModel = new StationSearch();
        $dataProvider = $searchModel->search(\Yii::$app->request->queryParams);

        return $this->export(LogArchiveComponent::FILE_TYPE_CSV, $dataProvider->query);
    }

    /**
     * @param string $fileType
     * @return bool
     */
    protected function export(string $fileType, QueryInterface $query)
    {
        if (!in_array($fileType, LogArchiveComponent::FILE_TYPES)) {
            setErrorFlash(_tF('export_wrong_type', 'log'));
            return false;
        }

        ExportAllStationsJob::pushJob([
            'email' => loggedUser()->email,
            'domain' => domain(),
            'fileType' => $fileType,
            'query' => $query,
        ]);
        setInfoFlash(_tF('export_request_info', 'log'));

        return true;
    }


    /**
     * Changes owner for existing Station models.
     * If update is successful, the browser will be redirected to the 'index' page.
     *
     * @return string|Response
     * @throws \yii\db\Exception
     */
    public function actionMoveSelectedToOwner()
    {
        $searchModel = new StationSearch();
        $dataProvider = $searchModel->search(\Yii::$app->request->queryParams);
        $userModel = new DynamicModel(['id_user']);
        $userModel->setAttributeLabel('id_user', _tF('new_owner', 'station'));
        $userModel->addRule(['id_user'], 'required');
        $userModel->addRule(['id_user'], 'exist', [
            'targetClass' => User::class,
            'targetAttribute' => 'id',
        ]);

        if (\Yii::$app->request->isPost) {
            if ($userModel->load(\Yii::$app->request->post()) && $userModel->validate()) {
                $transaction = Yii::$app->db->beginTransaction();

                $allSaved = true;
                foreach ($dataProvider->query->each() as $station) {
                    $models = Station::findAll(['id_master' => $station->id_master]);

                    foreach ($models as $model) {
                        $model->id_user = $userModel->id_user;
                        $saved = $model->save(true, ['id_user', 'updated_at']);
                        $allSaved &= $saved;

                        if (!$saved) {
                            Yii::error('Cannot save new station owner: ' . json_encode($station->errors));
                        }
                    }
                }

                if ($allSaved) {
                    $transaction->commit();
                    setSuccessFlash(_tF('flash_successfully_moved', 'station'));

                    return $this->redirectToPreviousIndex();
                } else {
                    setErrorFlash(_tF('flash_error'));
                }

                $transaction->rollBack();
            }
        }

        return $this->render('change-owner-of-selected', [
            'count' => $dataProvider->getTotalCount(),
            'model' => $userModel,
        ]);
    }
}
