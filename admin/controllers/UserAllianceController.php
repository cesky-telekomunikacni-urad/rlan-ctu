<?php
/*
* Copyright 2023 DactylGroup s.r.o.
*
* Licensed under the EUPL, Version 1.2 or – as soon they
will be approved by the European Commission - subsequent
versions of the EUPL (the "Licence");
* You may not use this work except in compliance with the
Licence.
* You may obtain a copy of the Licence at:
*
* https://joinup.ec.europa.eu/software/page/eupl5
*
* Unless required by applicable law or agreed to in
writing, software distributed under the Licence is
distributed on an "AS IS" basis,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
express or implied.
* See the Licence for the specific language governing
permissions and limitations under the Licence.
*/
?>


<?php

namespace admin\controllers;

use dactylcore\core\data\ActiveDataProvider;
use dactylcore\core\web\AdminController;
use dactylcore\core\admin\traits\TAdminCrudActions;
use admin\models\search\UserAllianceSearch;
use common\models\UserAlliance;
use yii\filters\AccessControl;

/**
 * UserAllianceController implements the CRUD actions for UserAlliance model. */
class UserAllianceController extends AdminController
{
    /**
     * Contains all basic CRUD actions definitions
     */
    use TAdminCrudActions;

    /**
     * @property string $modelClass
     */
    public $modelClass = UserAlliance::class;
    /**
     * @property string $searchModelClass
     */
    public $searchModelClass = UserAllianceSearch::class;
    /**
     * @property string $translationCategory
     */
    public $translationCategory = 'user-alliance';

    public function behaviors()
    {
        return \yii\helpers\ArrayHelper::merge(parent::behaviors(), [
            'access' => [
                'class' => AccessControl::class,
                'rules' => [
                    [
                        'allow' => true,
                        'actions' => ['index', 'view'],
                        'matchCallback' => function ($rule, $action)
                        {
                            return true;
                            return hasAccessTo('app-admin_user-alliances_index');
                        },
                    ],
                ],
            ],
        ]);
    }

    public function actionView($id)
    {
        /** @var UserAlliance $model */
        $model = $this->getModel($id);

        $dataProvider = new ActiveDataProvider([
            'query' => $model->getUsers(),
        ]);

        return $this->render('view', [
            'model' => $model,
            'dataProvider' => $dataProvider,
        ]);
    }
}
