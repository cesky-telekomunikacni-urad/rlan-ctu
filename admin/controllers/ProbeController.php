<?php
/*
* Copyright 2023 DactylGroup s.r.o.
*
* Licensed under the EUPL, Version 1.2 or – as soon they
will be approved by the European Commission - subsequent
versions of the EUPL (the "Licence");
* You may not use this work except in compliance with the
Licence.
* You may obtain a copy of the Licence at:
*
* https://joinup.ec.europa.eu/software/page/eupl5
*
* Unless required by applicable law or agreed to in
writing, software distributed under the Licence is
distributed on an "AS IS" basis,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
express or implied.
* See the Licence for the specific language governing
permissions and limitations under the Licence.
*/
?>


<?php

namespace admin\controllers;

use common\models\PhiWigig;
use dactylcore\core\web\AdminController;
use dactylcore\core\admin\traits\TAdminCrudActions;
use yii\data\ActiveDataProvider;
use common\models\Probe;
use yii\web\NotFoundHttpException;

/**
 * ProbeController implements the CRUD actions for Probe model. */
class ProbeController extends AdminController
{
    /**
     * Lists all Probe models.
     * @return mixed
     */
    public function actionIndex()
    {
        $dataProvider = new ActiveDataProvider([
            'query' => Probe::find(),
        ]);

        return $this->render('index', [
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Creates a new Probe model.
     * If creation is successful, the browser will be redirected to the 'index' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new Probe();
        $model->access_token = \Yii::$app->security->generateRandomString();
        $model->status = Probe::STATUS_INACTIVE;

        if ($model->load(\Yii::$app->request->post())) {

            if ($model->save()) {
                setSuccessFlash(_tF('create_successful_message', 'probe'));
                if (\Yii::$app->request->post('redirect')) {
                    return $this->redirectToPreviousIndex();
                }
            } else {
                setErrorFlash(_tF('create_error_message', 'probe'));
            }
        }


        return $this->render('create_update', [
            'model' => $model,
        ]);
    }

    /**
     * Finds the Probe model based on its primary key value or create new if id is null.
     * If the model is not found, a 404 HTTP exception will be thrown.
     *
     * @param integer $id
     *
     * @return Probe the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function getModel(int $id = null)
    {
        if (is_null($id)) {
            return (new Probe());
        }

        $model = Probe::find()->andWhere(['id' => $id])->one();
        if ($model !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException(_t('The requested page does not exist.'));
        }
    }


    /**
     * Updates an existing Probe model.
     * If update is successful, the browser will be redirected to the 'index' page.
     *
     * @param integer $id
     *
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->getModel($id);

        if (\Yii::$app->request->isPost) {
            if ($model->save()) {
                if (\Yii::$app->request->post('redirect')) {
                    return $this->redirectToPreviousIndex();
                }
            }
        }

        return $this->render('create_update', [
            'model' => $model,
        ]);
    }

    /**
     * Deletes an existing Probe model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     *
     * @param integer $id
     *
     * @return mixed
     */
    public function actionDelete($id)
    {
        $model = $this->getModel($id);

        if ($model->delete()) {
            setSuccessFlash(_tF('flash_successfully_deleted', 'probe'));
            return true;
        } else {
            if (!$model->hasErrors()) {
                setErrorFlash(_tF('flash_error'));
            } else {
                setErrorFlash($model->getFirstErrorMessage());
            }
        }

        return false;
    }


}
