<?php
/*
* Copyright 2023 DactylGroup s.r.o.
*
* Licensed under the EUPL, Version 1.2 or – as soon they
will be approved by the European Commission - subsequent
versions of the EUPL (the "Licence");
* You may not use this work except in compliance with the
Licence.
* You may obtain a copy of the Licence at:
*
* https://joinup.ec.europa.eu/software/page/eupl5
*
* Unless required by applicable law or agreed to in
writing, software distributed under the Licence is
distributed on an "AS IS" basis,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
express or implied.
* See the Licence for the specific language governing
permissions and limitations under the Licence.
*/
?>


<?php

namespace admin\controllers;

use yii\data\ActiveDataProvider;
use common\models\Phi;
use Yii;
use dactylcore\core\web\AdminController;
use yii\web\NotFoundHttpException;

/**
 * PhiController implements the CRUD actions for Phi model.
 */
class PhiController extends AdminController
{
    /**
     * Lists all Phi models.
     * @return mixed
     */
    public function actionIndex()
    {
        $dataProvider = new ActiveDataProvider([
            'query' => Phi::find(),
        ]);

        return $this->render('index', [
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Creates a new Phi model.
     * If creation is successful, the browser will be redirected to the 'index' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = $this->getModel();

        if (\Yii::$app->request->isPost) {
            if ($this->save($model)) {
                return $this->redirectToPreviousIndex();
            }
        }

        return $this->render('create', [
            'model' => $model,
        ]);
    }

    /**
     * Finds the Phi model based on its primary key value or create new if id is null.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Phi the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function getModel(int $id = null)
    {
        if (is_null($id)) {
            return (new Phi());
        }

        $model = Phi::find()->andWhere(['id' =>$id])->one();
        if ($model !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException(_t('The requested page does not exist.'));
        }
    }

    /**
     * Saves Phi model filled with post data.
     *
     * @param Phi $model saved model
     * @return bool success or not
     * @throws \yii\db\Exception database exception
     */
    protected function save(Phi &$model): bool
    {
        $data = Yii::$app->request->post();

        if ($model->load($data)) {
            $isNewRecord = $model->isNewRecord;
            $transaction = Yii::$app->db->beginTransaction();

            if ($model->save()) {
                $transaction->commit();

                if ($isNewRecord) {
                    setSuccessFlash(_tF('flash_successfully_created', 'phi'));
                } else {
                    setSuccessFlash(_tF('flash_successfully_updated', 'phi'));
                }

                return true;
            }

            $transaction->rollBack();
        } else {
            if (!$model->hasErrors()) {
                setErrorFlash(_tF('flash_error'));
            }
        }

        return false;
    }

    /**
     * Updates an existing Phi model.
     * If update is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->getModel($id);

        if (\Yii::$app->request->isPost) {
            if ($this->save($model)) {
                return $this->redirectToPreviousIndex();
            }
        }

        return $this->render('update', [
            'model' => $model,
        ]);
    }

    /**
     * Deletes an existing Phi model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $model = $this->getModel($id);

        if ($model->delete()) {
            setSuccessFlash(_tF('flash_successfully_deleted', 'phi'));
            return true;
        } else {
            if (!$model->hasErrors()) {
                setErrorFlash(_tF('flash_error'));
            } else {
                setErrorFlash($model->getFirstErrorMessage());
            }
        }

        return false;
    }
}
