<?php
/*
* Copyright 2023 DactylGroup s.r.o.
*
* Licensed under the EUPL, Version 1.2 or – as soon they
will be approved by the European Commission - subsequent
versions of the EUPL (the "Licence");
* You may not use this work except in compliance with the
Licence.
* You may obtain a copy of the Licence at:
*
* https://joinup.ec.europa.eu/software/page/eupl5
*
* Unless required by applicable law or agreed to in
writing, software distributed under the Licence is
distributed on an "AS IS" basis,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
express or implied.
* See the Licence for the specific language governing
permissions and limitations under the Licence.
*/
?>


<?php
// Uses
use dactylcore\core\db\ActiveRecord;
use dactylcore\core\helpers\Html;
use dactylcore\core\helpers\Url;
use dactylcore\core\widgets\admin\gridview\GridView;
use dactylcore\core\data\ActiveDataProvider;
use dactylcore\log\admin\assets\LogAsset;
use dactylcore\log\admin\models\Log;
use dactylcore\log\admin\models\search\LogSearch;

// Sources
/* @var $this \dactylcore\core\web\AdminView */
/* @var $searchModel LogSearch */

/* @var $dataProvider ActiveDataProvider */

use dactylcore\core\widgets\common\bootstrap\Html as BootHtml;
use dactylcore\log\common\components\LogArchiveComponent;

// Settings
$pageId = 'log';
$this->title = _tF('logs', 'log');

// Breadcrumbs
$this->breadcrumbs[] = [
    'label' => $this->title,
    'url' => currentUrl(),
];

// Buttons
$this->buttons[] = Html::a(_tF('export_all', 'log'),
    array_merge(['export-logs-xls'], Yii::$app->request->queryParams),
    Yii::$app->controller->module->id . '_log_index', [
        'class' => 'btn btn-secondary export-btn',
        'data-base-url' => Url::to(['export-logs-xls']),
        'data-method' => 'post',
        'data-ajax' => "1",
        'data-pjax' => '0',
        'data-confirm' => _tF('export_all_confirm', 'log'),
    ]
);
$this->buttons[] = Html::a(_tF('export_all_csv', 'log'),
    array_merge(['export-logs-csv'], Yii::$app->request->queryParams),
    Yii::$app->controller->module->id . '_log_index', [
        'class' => 'btn btn-secondary export-btn',
        'data-base-url' => Url::to(['export-logs-csv']),
        'data-method' => 'post',
        'data-ajax' => "1",
        'data-pjax' => '0',
        'data-confirm' => _tF('export_all_confirm', 'log'),
    ]
);

LogAsset::register($this);
$operationBadges = [
    ActiveRecord::OPERATION_TYPE_CREATE => BootHtml::TYPE_SUCCESS,
    ActiveRecord::OPERATION_TYPE_UPDATE => BootHtml::TYPE_INFO,
    ActiveRecord::OPERATION_TYPE_DELETE => BootHtml::TYPE_WARNING,
    ActiveRecord::OPERATION_TYPE_EVENT => BootHtml::TYPE_DEFAULT,
];
$filterArrays = Log::getFilterArrays();
?>

    <div class="<?= $pageId ?>-index">
        <div class="portlet">
            <div class="body">
                <?= GridView::widget([
                    'id' => $pageId . '-grid',
                    'dataProvider' => $dataProvider,
                    'filterModel' => $searchModel,
                    'responsive' => true,
                    'responsiveWrap' => true,
                    'floatHeader' => true,
                    'floatHeaderOptions' => [
                        'scrollingTop' => '0',
                        'position' => 'absolute',
                        'top' => 56,
                    ],
                    'doubleScroll' => true,
                    'columns' => [
                        'created_at' => [
                            'attribute' => 'created_at',
                            'format' => ['datetime', 'php:Y-m-d H:i:s'],
                            'filterType' => GridView::FILTER_DATE_RANGE,
                            'filterWidgetOptions' => [
                                'attribute' => 'createdAtRange',
                                'convertFormat' => true,
                                'pluginOptions' => [
                                    'locale' => ['format' => LogSearch::DATE_FORMAT_READABLE],
                                ],
                            ],
                            'contentOptions' => [
                                'style' => 'width: 210px;'
                            ],
                        ],
                        [
                            'attribute' => 'operation_type',
                            'value' => function (Log $model) use ($operationBadges) {
                                return BootHtml::badge(_t($model->operation_type, 'log'), $operationBadges[$model->operation_type]);
                            },
                            'filterType' => GridView::FILTER_SELECT2,
                            'filter' => Log::getOperationTypes(),
                            'headerOptions' => [
                                'style' => 'width: 80px;',
                            ],
                            'filterOptions' => [
                                'style' => 'width: 80px;',
                            ],
                            'contentOptions' => [
                                'style' => 'width: 80px;',
                            ],
                        ],
                        [
                            'label' => _tF('event', 'log'),
                            'value' => function (Log $model) {
                                $attrs = $model->getDataAttributes();
                                if (empty($attrs['keys'])) {
                                    return implode("\n<br>", $model->getTranslatedOperations());
                                } else {
                                    $count = count($attrs['keys']);
                                    $operations = $model->getTranslatedOperations();

                                    if ($count == 1) {
                                        $attrNumTranslation = _t('updated_one_attribute', 'log');
                                    } elseif ($count < 5) {
                                        $attrNumTranslation = _t('updated_{attrNum}_below_five_attribute', 'log', ['attrNum' => $count]);
                                    } else {
                                        $attrNumTranslation = _t('updated_{attrNum}_above_five_attribute', 'log', ['attrNum' => $count]);
                                    }

                                    $attrNumTranslation = "<span class='atts'>{$attrNumTranslation}</span>";
                                    $operations[] = $attrNumTranslation;
                                    $text = implode("\n<br>", $operations);
                                    return (hasAccessTo('dc-log_log_attributes')) ?
                                        Html::a($text, url(['attributes', 'id' => $model->id]), 'dc-log_log_index', [
                                            'data-livebox' => 1,
                                            'data-livebox-width' => '90%'
                                        ]) : $text;
                                }
                            },
                            'attribute' => 'eventData',
                            'headerOptions' => [
                                'style' => 'width: 180px;',
                            ],
                            'filterOptions' => [
                                'style' => 'width: 180px;',
                            ],
                            'contentOptions' => [
                                'style' => 'width: 180px;',
                            ],
                        ],
                        [
                            'attribute' => 'id_user',
                            'value' => function (Log $model) {
                                $icons = [
                                    'user' => 'people',
                                    'system' => 'computer',
                                    'console' => 'chrome_reader_mode',
                                ];
                                $tag = Html::tag('i', $icons[$model->actor], [
                                    'class' => 'material-icons',
                                    'style' => 'max-width: 24px; margin-right: 8px',
                                    'data-toggle' => 'tooltip',
                                    'title' => _tF($model->actor)
                                ]);
                                $userName = $model->user
                                    ? $model->user->name
                                    : ($model->user_name ?? '');

                                return $userName ? "<div style='display: flex; align-items: center'>{$tag}{$userName}</div>" : '';
                            },
                            'filterType' => GridView::FILTER_SELECT2,
                            'filterWidgetOptions' => [
                                'data' => Log::getAllUsers(),
                                'hideSearch' => false,
                                'pluginOptions' => [
                                    'allowClear' => true,
                                    'placeholder' => '',
                                    'ajax' => [
                                        'url' => url(['/dc-user/user/user-dropdown-data2']),
                                        'dataType' => 'json',
                                        'data' => new \yii\web\JsExpression(
                                            <<<JS
                                    function (params) {
                                        return {
                                            term: params.term,
                                            page: params.page || 1
                                        }
                                    }
JS
                                        ),
                                    ],
                                ],
                            ],
                            'headerOptions' => [
                                'style' => 'width: 170px;',
                            ],
                            'filterOptions' => [
                                'style' => 'width: 170px;',
                            ],
                            'contentOptions' => [
                                'style' => 'width: 170px;',
                            ],
                        ],
                        [
                            'attribute' => 'app',
                            'label' => _tF('source', 'log'),
                            'value' => function (Log $model) {
                                return _t($model->app, 'log');
                            },
                            'filterType' => GridView::FILTER_SELECT2,
                            'filter' => Log::getApplicationTypeLabels(),
                            'headerOptions' => [
                                'style' => 'width: 140px;',
                            ],
                            'filterOptions' => [
                                'style' => 'width: 140px;',
                            ],
                            'contentOptions' => [
                                'style' => 'width: 140px;',
                            ],
                        ],
                        [
                            'attribute' => 'model',
                            'label' => _tF('type_of_event_source', 'log'),
                            'value' => function (Log $model) {
                                return _t($model->model, 'log');
                            },
                            'filterType' => GridView::FILTER_SELECT2,
                            'filterWidgetOptions' => [
                                'data' => $filterArrays['loggedModels'],
                                'hideSearch' => false,
                                'pluginOptions' => [
                                    'allowClear' => true,
                                ],
                            ],
                            'headerOptions' => [
                                'style' => 'width: 150px;',
                            ],
                            'filterOptions' => [
                                'style' => 'width: 150px;',
                            ],
                            'contentOptions' => [
                                'style' => 'width: 150px;',
                            ],
                        ],
                        [
                            'attribute' => 'model_name',
                            'label' => _tF('event_source_name', 'log'),
                            'format' => 'raw',
                            'value' => function (Log $model) {
                                // if model_name contains fully qualified model name
                                if (strpos($model->model_name, '\\')) {
                                    $name = explode('\\', $model->model_name);
                                    return $name[count($name) - 1];
                                }
                                return $model->model_name;
                            }
                        ],
                        [
                            'class' => dactylcore\core\widgets\common\gridview\ActionColumn::class,
                            'template' => '{link} {anonymize}',
                            'header' => '#',
                            'contentOptions' => [
                                'style' => 'max-width: 40px; !important'
                            ],
                            'buttons' => [
                                'link' => function ($url, Log $model) {
                                    $options = [
                                        'data' => [
                                            'pjax' => 0,
                                            'toggle' => 'tooltip',
                                        ],
                                        'title' => _tF('view'),
                                        'target' => '_blank',
                                    ];
                                    $route = $model->getRoute();
                                    $tag = Html::tag('i', 'open_in_new', ['class' => 'material-icons']);
                                    return $route ? \yii\helpers\Html::a($tag, url($route), $options) : '';
                                },
                                'anonymize' => function ($url, $model, $_, $grid) {

                                    $options = [
                                        'data' => [
                                            'ajax' => 1,
                                            'method' => 'post',
                                            'reload-pjax-containers' => json_encode([$grid->pjaxSettings['options']['id']]),
                                            'confirm' => _tF('anonymize_confirm', 'log'),
                                            'title' => _tF('anonymize_model', 'log'),
                                            'toggle' => 'tooltip',
                                            'placement' => 'top',
                                        ],
                                        'title' => _tF('anonymize_model', 'log'),
                                    ];
                                    $tag = Html::tag('i', 'visibility_off', ['class' => 'material-icons']);
                                    return Html::a($tag, url(['anonymize-model', 'id' => $model->id]), 'dc-log_log_anonymize-model', $options);
                                }
                            ]
                        ]
                    ]
                ]); ?>
            </div>
        </div>
    </div>

<?php
$this->registerJs(<<<JS
    $(document).on('pjax:end',   function() {
        var buttons = $('.export-btn');
        buttons.each(function() {
            var button = $(this);
            button.attr('href', button.data('base-url') + window.location.search);
        });
    });
JS
);