<?php
/*
* Copyright 2023 DactylGroup s.r.o.
*
* Licensed under the EUPL, Version 1.2 or – as soon they
will be approved by the European Commission - subsequent
versions of the EUPL (the "Licence");
* You may not use this work except in compliance with the
Licence.
* You may obtain a copy of the Licence at:
*
* https://joinup.ec.europa.eu/software/page/eupl5
*
* Unless required by applicable law or agreed to in
writing, software distributed under the Licence is
distributed on an "AS IS" basis,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
express or implied.
* See the Licence for the specific language governing
permissions and limitations under the Licence.
*/
?>


<?php
// Uses
use dactylcore\core\helpers\Html;
use dactylcore\core\widgets\admin\gridview\GridView;
use dactylcore\core\widgets\common\gridview\ActionColumn;
use dactylcore\core\widgets\admin\gridview\TimestampColumn;
use dactylcore\core\web\AdminView;
use dactylcore\log\common\models\LogApi;
use dactylcore\core\widgets\common\bootstrap\Html as BootHtml;
use yii\data\ArrayDataProvider;

// Sources
/* @var $this AdminView */
/* @var $dataProvider ArrayDataProvider */

// Settings
$pageId = 'log-archive';
$this->title = _tF('log_archive_files', 'log-archive');
$moduleId = Yii::$app->controller->module->id;

// Breadcrumbs
$this->breadcrumbs[] = [
    'label' => $this->title,
    'url' => currentUrl(),
];

\dactylcore\log\admin\assets\ApiLogAsset::register($this);
?>

<div class="<?= $pageId ?>-index">
    <div class="portlet">
        <div class="body">
            <?= GridView::widget([
                'id' => "{$pageId}-grid",
                'dataProvider' => $dataProvider,
                'formatter' => ['class' => 'yii\i18n\Formatter', 'nullDisplay' => ''],
                'responsive' => true,
                'responsiveWrap' => true,
                'floatHeader' => true,
                'floatHeaderOptions' => [
                    'scrollingTop' => '0',
                    'position' => 'absolute',
                    'top' => 56,
                ],
                'doubleScroll' => true,
                'columns' => [
                    'name' => [
                        'attribute' => 'name',
                        'label' => _tF('log_archive_file_list_name', 'log-archive'),
                        'value' => function ($model) {
                            return Html::a($model['name'], url(['/dc-log/log-archive/download', 'name' => $model['name']]),
                                'dc-log_log_archive', ['data-pjax' => 0]);
                        }
                    ],
                    'month' => [
                        'attribute' => 'month',
                        'label' => _tF('log_archive_file_list_month', 'log-archive'),
                        'value' => function ($model) {
                            // Translate month
                            return Yii::$app->formatter->asDate($model['month'], 'php:F Y');
                        },
                    ],
                    'size' => [
                        'attribute' => 'size',
                        'label' => _tF('log_archive_file_list_size', 'log-archive'),
                        'value' => function ($model) {
                            // Translate month
                            Yii::$app->formatter->sizeFormatBase = 1000;
                            return Yii::$app->formatter->asShortSize($model['size'], 1);
                        },
                    ],
                ],
            ]); ?>
        </div>
    </div>
</div>
