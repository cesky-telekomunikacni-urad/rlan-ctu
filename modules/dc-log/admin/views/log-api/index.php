<?php
/*
* Copyright 2023 DactylGroup s.r.o.
*
* Licensed under the EUPL, Version 1.2 or – as soon they
will be approved by the European Commission - subsequent
versions of the EUPL (the "Licence");
* You may not use this work except in compliance with the
Licence.
* You may obtain a copy of the Licence at:
*
* https://joinup.ec.europa.eu/software/page/eupl5
*
* Unless required by applicable law or agreed to in
writing, software distributed under the Licence is
distributed on an "AS IS" basis,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
express or implied.
* See the Licence for the specific language governing
permissions and limitations under the Licence.
*/
?>


<?php
// Uses
use dactylcore\core\helpers\Html;
use dactylcore\core\helpers\Url;
use dactylcore\core\widgets\admin\gridview\GridView;
use dactylcore\core\data\ActiveDataProvider;
use dactylcore\core\widgets\common\gridview\ActionColumn;
use dactylcore\core\widgets\admin\gridview\TimestampColumn;
use dactylcore\core\web\AdminView;
use dactylcore\log\common\models\LogApi;
use dactylcore\log\admin\models\search\LogApiSearch;
use dactylcore\core\widgets\common\bootstrap\Html as BootHtml;

// Sources
/* @var $this AdminView */
/* @var $model LogApi */
/* @var $searchModel LogApiSearch */
/* @var $dataProvider ActiveDataProvider */

// Settings
$pageId = 'log-api';
$this->title = _tF('log_apis', 'log-api');
$moduleId = Yii::$app->controller->module->id;

// Buttons
$this->buttons[] = Html::a(_tF('export_all', 'log'),
    array_merge(['export-logs-xls'], Yii::$app->request->queryParams),
    Yii::$app->controller->module->id . '_log_index', [
        'class' => 'btn btn-secondary export-btn',
        'data-base-url' => Url::to(['export-logs-xls']),
        'data-method' => 'post',
        'data-ajax' => "1",
        'data-pjax' => '0',
        'data-confirm' => _tF('export_all_confirm', 'log'),
    ]
);
$this->buttons[] = Html::a(_tF('export_all_csv', 'log'),
    array_merge(['export-logs-csv'], Yii::$app->request->queryParams),
    Yii::$app->controller->module->id . '_log_index', [
        'class' => 'btn btn-secondary export-btn',
        'data-base-url' => Url::to(['export-logs-csv']),
        'data-method' => 'post',
        'data-ajax' => "1",
        'data-pjax' => '0',
        'data-confirm' => _tF('export_all_confirm', 'log'),
    ]
);

// Breadcrumbs
$this->breadcrumbs[] = [
    'label' => $this->title,
    'url' => currentUrl(),
];

\dactylcore\log\admin\assets\ApiLogAsset::register($this);
?>

    <div class="<?= $pageId ?>-index">
        <div class="portlet">
            <div class="body">
                <?= GridView::widget([
                    'id' => "{$pageId}-grid",
                    'dataProvider' => $dataProvider,
                    'filterModel' => $searchModel,
                    'formatter' => ['class' => 'yii\i18n\Formatter', 'nullDisplay' => ''],
                    'responsive' => true,
                    'responsiveWrap' => true,
                    'floatHeader' => true,
                    'floatHeaderOptions' => [
                        'scrollingTop' => '0',
                        'position' => 'absolute',
                        'top' => 56,
                    ],
                    'doubleScroll' => true,
                    'columns' => [
                        'id' => [
                            'attribute' => 'id',
                            'value' => function (LogApi $model) {
                                return $model->id;
                            },
                        ],
                        'created_at' => [
                            'class' => TimestampColumn::class,
                            'attribute' => 'created_at',
                        ],
                        'response_code' => [
                            'attribute' => 'response_code',
                            'value' => function (LogApi $model) {
                                return BootHtml::badge($model->response_code,
                                    ($model->response_code >= 200 && $model->response_code <= 300));
                            },
                        ],
                        'request_method' => [
                            'attribute' => 'request_method',
                            'value' => function (LogApi $model) {
                                return $model->request_method;
                            },
                        ],
                        'url' => [
                            'attribute' => 'url',
                            'value' => function (LogApi $model) {
                                return "<div>{$model->url}</div>";
                            },
                        ],
                        'id_user' => [
                            'attribute' => 'id_user',
                            'value' => function (LogApi $model) {
                                return $model->user == null ? null :
                                    (Html::a($model->id_user,
                                        url(['/dc-user/user/index', 'UserSearch' => ['id' => $model->id_user]]),
                                        'dc-user_user_index', [
                                            'title' => $model->user->name,
                                            'data-toggle' => 'tooltip',
                                            'data-pjax' => 0,
                                        ]));
                            },
                            'filterType' => GridView::FILTER_SELECT2,
                            'filterWidgetOptions' => [
                                'hideSearch' => false,
                                'data' => function (LogApi $model) {
                                    return $model->user == null ? [] :
                                        [$model->id_user => $model->user->name];
                                },
                                'pluginOptions' => [
                                    'allowClear' => true,
                                    'placeholder' => '',
                                    'ajax' => [
                                        'url' => url(['/dc-log/log-api/get-all-users']),
                                        'dataType' => 'json',
                                        'data' => new \yii\web\JsExpression(
                                            <<<JS
                                            function (params) {
                                                return {
                                                    term: params.term,
                                                    page: params.page || 1
                                                }
                                            }
                                        JS
                                        ),
                                    ],
                                ],
                            ],
                        ],
                        'access_token' => [
                            'attribute' => 'access_token',
                            'value' => function (LogApi $model) {
                                return $model->access_token;
                            },
                            'visible' => false,
                        ],
                        'request_data' => [
                            'attribute' => 'request_data',
                            'value' => function (LogApi $model) {
                                $tmp = stringPreview($model->request_data, 50, ['words' => false]);
                                $tmp = \yii\helpers\Html::a($tmp, url(['/dc-log/log-api/detail', 'id' => $model->id]), [
                                    'data-livebox' => 1,
                                    'data-livebox-fullscreen' => 1,
                                    'data-livebox-class' => 'apilog',
                                ]);

                                return $tmp;
                            },
                            'headerOptions' => [
                                'style' => 'min-width: 200px;',
                            ],
                            'filterOptions' => [
                                'style' => 'min-width: 200px;',
                            ],
                            'contentOptions' => [
                                'style' => 'min-width: 200px; word-break: break-all;',
                            ],
                        ],
                        'response_data' => [
                            'attribute' => 'response_data',
                            'value' => function (LogApi $model) {
                                $tmp = stringPreview($model->getResponseDataAsJson(), 50, ['words' => false]);
                                $tmp = \yii\helpers\Html::a($tmp, url(['/dc-log/log-api/detail', 'id' => $model->id]), [
                                    'data-livebox' => 1,
                                    'data-livebox-fullscreen' => 1,
                                    'data-livebox-class' => 'apilog',
                                ]);

                                return $tmp;
                            },
                            'headerOptions' => [
                                'style' => 'min-width: 180px;',
                            ],
                            'filterOptions' => [
                                'style' => 'min-width: 180px;',
                            ],
                            'contentOptions' => [
                                'style' => 'min-width: 180px;',
                            ],
                        ],
                        'lang' => [
                            'attribute' => 'lang',
                            'value' => function (LogApi $model) {
                                return $model->lang;
                            },
                        ],
                        'request_ip' => [
                            'attribute' => 'request_ip',
                            'value' => function (LogApi $model) {
                                return $model->request_ip;
                            },
                        ],
                        [
                            'class' => ActionColumn::class,
                            'template' => '{anonymize}',
                            'contentOptions' => [
                                'style' => 'min-width: 165px;',
                            ],
                            'buttons' => [
                                'anonymize' => function ($url, $model, $_, $grid) {
                                    $options = [
                                        'data' => [
                                            'ajax' => 1,
                                            'method' => 'post',
                                            'reload-pjax-containers' => json_encode([$grid->pjaxSettings['options']['id']]),
                                            'confirm' => _tF('anonymize_confirm', 'log'),
                                            'title' => _tF('anonymize', 'log'),
                                            'toggle' => 'tooltip',
                                            'placement' => 'top',
                                        ],
                                        'title' => _tF('anonymize', 'log'),
                                    ];
                                    $tag = Html::tag('i', 'visibility_off', ['class' => 'material-icons']);
                                    return Html::a($tag, url(['/dc-log/log-api/anonymize', 'id' => $model->id]),
                                        'dc-log_log_anonymize-model', $options);
                                },
                            ],
                        ],
                    ],
                ]); ?>
            </div>
        </div>
    </div>

    <style>
        #log-api-grid td[data-col-seq="url"] > div {
            width: 200px;
        }
    </style>

<?php
$this->registerJs(<<<JS
    $(document).on('pjax:end',   function() {
        var buttons = $('.export-btn');
        buttons.each(function() {
            var button = $(this);
            button.attr('href', button.data('base-url') + window.location.search);
        });
    });
JS
);