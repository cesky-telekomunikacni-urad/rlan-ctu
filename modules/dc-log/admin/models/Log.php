<?php
/*
* Copyright 2023 DactylGroup s.r.o.
*
* Licensed under the EUPL, Version 1.2 or – as soon they
will be approved by the European Commission - subsequent
versions of the EUPL (the "Licence");
* You may not use this work except in compliance with the
Licence.
* You may obtain a copy of the Licence at:
*
* https://joinup.ec.europa.eu/software/page/eupl5
*
* Unless required by applicable law or agreed to in
writing, software distributed under the Licence is
distributed on an "AS IS" basis,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
express or implied.
* See the Licence for the specific language governing
permissions and limitations under the Licence.
*/
?>


<?php


namespace modules\log\admin\models;


use dactylcore\core\db\ActiveRecord;
use dactylcore\core\helpers\Html;
use dactylcore\log\LogOperation;
use dactylcore\user\common\models\User;

class Log extends \dactylcore\log\admin\models\AbstractLog
{
    /**
     * Returns array of users who have some log entry with name for filtering
     * @return array
     */
    public static function getAllUsers(): array
    {
        $filter = [];

        // queries only users that have log
        $userLogsSubquery = Log::find()->select(['id_user'])->distinct();
        $users = User::find()
            ->select(['id', " IF(user.first_name =  '', user.companyName, CONCAT(COALESCE(first_name,''), ' ', COALESCE(last_name,''))) AS user_name"])
            ->andWhere(['id' => $userLogsSubquery])
            ->asArray();


        foreach ($users->each() as $user) {
            $filter[$user['id']] = $user['user_name'];
        }

        $filter['system'] = _tF('system', 'log');
        $filter['console'] = _tF('console', 'log');

        return $filter;
    }
}