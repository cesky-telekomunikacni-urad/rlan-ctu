<?php
/*
* Copyright 2023 DactylGroup s.r.o.
*
* Licensed under the EUPL, Version 1.2 or – as soon they
will be approved by the European Commission - subsequent
versions of the EUPL (the "Licence");
* You may not use this work except in compliance with the
Licence.
* You may obtain a copy of the Licence at:
*
* https://joinup.ec.europa.eu/software/page/eupl5
*
* Unless required by applicable law or agreed to in
writing, software distributed under the Licence is
distributed on an "AS IS" basis,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
express or implied.
* See the Licence for the specific language governing
permissions and limitations under the Licence.
*/
?>


<?php

namespace modules\page\elements\wysiwyg;

use dactylcore\core\web\AssetBundle;

class TrumbowygAsset extends AssetBundle
{
    public $sourcePath = '@modules/dc-page-wysiwyg/assets';
    public $css
        = [
            'scss/wysiwyg.scss',
        ];
    public $depends
        = [
            'dactylcore\page\elements\wysiwyg\WysiwygAsset',
        ];
}
