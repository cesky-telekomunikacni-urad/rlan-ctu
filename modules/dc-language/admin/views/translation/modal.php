<?php
/*
* Copyright 2023 DactylGroup s.r.o.
*
* Licensed under the EUPL, Version 1.2 or – as soon they
will be approved by the European Commission - subsequent
versions of the EUPL (the "Licence");
* You may not use this work except in compliance with the
Licence.
* You may obtain a copy of the Licence at:
*
* https://joinup.ec.europa.eu/software/page/eupl5
*
* Unless required by applicable law or agreed to in
writing, software distributed under the Licence is
distributed on an "AS IS" basis,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
express or implied.
* See the Licence for the specific language governing
permissions and limitations under the Licence.
*/
?>


<?php
// Uses
use artkost\yii2\trumbowyg\Trumbowyg;
use dactylcore\core\helpers\Url;
use dactylcore\core\widgets\admin\activeform\ActiveForm;
use dactylcore\language\common\models\Language;

// Sources
/* @var $this \dactylcore\core\web\AdminView */
/* @var $model \dactylcore\language\admin\forms\TranslationModalForm */
/* @var $form \dactylcore\core\widgets\admin\activeform\ActiveForm */

$tmpLabel1 = _tF('translation', 'translation');
$tmpLabel2 = Language::findOne(['id' => $model->id_language])->name;
$valueLabel = "{$tmpLabel1} - {$tmpLabel2}";
?>

<?php $form = ActiveForm::begin([
    'id' => 'translation-modal-form',
    'action' => url('translation/update'),
]); ?>

    <div class="portlet">
        <div class="header"><h3><?= _tF('translation_edit', 'translation') ?></h3></div>
        <div class="body">
            <div class="row">
                <div class="col-md-6">
                    <?= $form->field($model, 'application')->textInput(['readonly' => true]); ?>
                </div>
                <div class="col-md-6">
                    <?= $form->field($model, 'category')->textInput(['readonly' => true]); ?>
                </div>
            </div>

            <?= $form->field($model, 'key')->textInput(['readonly' => true]); ?>

            <?= $form
                ->field($model, 'value')
                ->label($valueLabel)
                ->widget(Trumbowyg::class, [
                    'settings' => [
                        'plugins' => [
                            'history',
                        ],
                        'btns' => [
                            ['historyUndo', 'historyRedo'],
                            ['strong', 'em', 'del', 'underscore', 'link'],
                            ['removeformat'],
                            ['fullscreen'],
                        ],
                        'removeformatPasted' => true,
                    ],
                ]); ?>

            <?= $form->field($model, 'id_language')->hiddenInput(); ?>
        </div>
    </div>
    <div class="form-buttons">
        <?= $form->cancelButton(Url::getPreviousIndexUrl()); ?>
        <?= $form->submitButton(_tF('save')); ?>
    </div>
<?php ActiveForm::end(); ?>