<?php
/*
* Copyright 2023 DactylGroup s.r.o.
*
* Licensed under the EUPL, Version 1.2 or – as soon they
will be approved by the European Commission - subsequent
versions of the EUPL (the "Licence");
* You may not use this work except in compliance with the
Licence.
* You may obtain a copy of the Licence at:
*
* https://joinup.ec.europa.eu/software/page/eupl5
*
* Unless required by applicable law or agreed to in
writing, software distributed under the Licence is
distributed on an "AS IS" basis,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
express or implied.
* See the Licence for the specific language governing
permissions and limitations under the Licence.
*/
?>


<?php

// Uses
use admin\widgets\ActiveField;
use dactylcore\core\helpers\Html;
use dactylcore\core\widgets\admin\activeform\ActiveForm;
use dactylcore\core\widgets\admin\activeform\ActiveFormAsset;
use dactylcore\system\admin\web\ConfigAsset;
use dactylcore\core\widgets\admin\select2\Select2;

// Sources
/* @var $this \dactylcore\core\web\AdminView */
/* @var $form \dactylcore\core\widgets\admin\activeform\ActiveForm */
/* @var $configItem \dactylcore\system\common\models\AbstractConfig */

// Settings
$pageId = 'config';
$formId = $pageId . '-form';
$this->showLanguageSelect = true;

// Breadcrumbs
$this->breadcrumbs[] = [
    'label' => _tF('settings', 'config'),
    'url' => currentUrl(),
];

$this->breadcrumbs[] = [
    'label' => _tF($permission, 'access-manager'),
    'url' => currentUrl(),
];

// Buttons
$this->buttons[] = Html::button(
    _tF('update'),
    Yii::$app->controller->module->id .
    '_' .
    Yii::$app->controller->id .
    '_' .
    Yii::$app->controller->action->id .
    '_' .
    Yii::$app->request->queryParams['permission'],
    [
        'type' => 'submit',
        'form' => $formId,
        'class' => 'btn btn-primary',
    ]
);
?>

<?= $this->beginContainer(false, ['id' => $pageId . '-update']); ?>
<?php $form = ActiveForm::begin([
    'id' => $formId,
]);
$form->fieldClass = ActiveField::class;
?>
<?php foreach ($groupedConfigItems as $name => $configItems) : ?>
    <div class="portlet">
        <div class="header">
            <h3><?= _t($name, 'config'); ?></h3>
        </div>
        <div class="body">
            <div class="row">
                <div class="col-md-12">
                    <?php foreach ($configItems as $configItem) : ?>
                        <?php
                        $isJson = isJson($configItem->value);
                        $options = $configItem->getActiveField()['options'] ?? [];
                        $function = $configItem->getActiveField()['function'];
                        $params = $configItem->getActiveField()['params'];
                        $isPassword = (isset($params['options']['type']) && $params['options']['type'] === 'password');
                        $hint = _tF($configItem['key'] . '_HINT', 'config')
                        == $configItem['key'] . '_HINT' ? false :
                            _tF(
                                $configItem['key'] . '_HINT',
                                'config'
                            );

                        if ($isPassword) {
                            $params['options']['value'] = '';
                        }

                        if ($isJson) {
                            $configItem->value = json_decode($configItem->value, true);
                        }

                        echo $form
                            ->field($configItem, '[' . $configItem['id'] . ']value', $options)
                            ->$function(...array_values($params))
                            ->hint($hint);

                        ?>
                    <?php endforeach; ?>
                </div>
            </div>
        </div>
    </div>
<?php endforeach; ?>


<?php

ActiveFormAsset::register($this);
ConfigAsset::register($this);

?>

<?php ActiveForm::end(); ?>
<?= $this->endContainer(); ?>


<?php if (Yii::$app->request->queryParams['permission'] == 'email' &&
    hasAccessTo('dc-system_config_smtp-test_email')) : ?>
    <?= $this->render('@dc-system/admin/views/config/partial/smtp-test') ?>
<?php endif; ?>