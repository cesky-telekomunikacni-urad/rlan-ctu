<?php
/*
* Copyright 2023 DactylGroup s.r.o.
*
* Licensed under the EUPL, Version 1.2 or – as soon they
will be approved by the European Commission - subsequent
versions of the EUPL (the "Licence");
* You may not use this work except in compliance with the
Licence.
* You may obtain a copy of the Licence at:
*
* https://joinup.ec.europa.eu/software/page/eupl5
*
* Unless required by applicable law or agreed to in
writing, software distributed under the Licence is
distributed on an "AS IS" basis,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
express or implied.
* See the Licence for the specific language governing
permissions and limitations under the Licence.
*/
?>


<?php
// Uses
use dactylcore\page\frontend\models\Page;
use frontend\widgets\dactylkit\DactylKit;

// Sources
/* @var $this \dactylcore\core\web\AdminView */
/* @var $page \dactylcore\page\common\models\Page */

// Register page JS
$this->registerJs($page->js);

// Register page CSS
$this->registerCss($page->css);

$this->title = $page->title;

// Breadcrumbs
$this->breadcrumbs[] = [
    'label' => $this->title,
];

\frontend\assets\CommonPageAsset::register($this);

?>

<section id="page" class="<?= Page::TYPES[$page->type]; ?>">
    <?= DactylKit::breadcrumb() ?>
    <h3 class="dk--page--title"><?= $page->title; ?></h3>
    <div id="page-elements" class="dk--page--content">
        <?= $page->getContent(); ?>
    </div>
    <?= $this->render('partial/tags', [
        'page' => $page,
    ]); ?>
</section>

<?php $this->registerJS(
    <<<JS

JS
);
?>