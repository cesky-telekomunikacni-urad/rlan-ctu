<?php
/*
* Copyright 2023 DactylGroup s.r.o.
*
* Licensed under the EUPL, Version 1.2 or – as soon they
will be approved by the European Commission - subsequent
versions of the EUPL (the "Licence");
* You may not use this work except in compliance with the
Licence.
* You may obtain a copy of the Licence at:
*
* https://joinup.ec.europa.eu/software/page/eupl5
*
* Unless required by applicable law or agreed to in
writing, software distributed under the Licence is
distributed on an "AS IS" basis,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
express or implied.
* See the Licence for the specific language governing
permissions and limitations under the Licence.
*/
?>


<?php

// Uses
use dactylcore\core\widgets\frontend\activeform\ActiveForm;
use frontend\widgets\dactylkit\DactylKit;
use yii\helpers\Html;
use dactylcore\core\web\FrontendView;
use dactylcore\user\frontend\assets\UserFormAsset;
use dactylcore\user\frontend\models\PasswordResetRequestForm;

// Assets
UserFormAsset::register($this);

// Sources
/* @var $this FrontendView */
/* @var $form ActiveForm */
/* @var $model PasswordResetRequestForm */
?>

<div class="row">
    <div class="col-sm-12">
        <span toggle=".curr-password" class="toggle-password">
            <?= DactylKit::icon(DactylKit::ICON_EYE); ?>
        </span>
        <?= $form->field($model, 'password')->passwordInput([
            'autofocus' => true,
            'spellcheck' => 'false',
            'encoded' => false,
            'class' => 'curr-password',
        ]) ?>
    </div>
</div>
<div class="row">
    <div class="col-sm-12">
        <span toggle=".conf-password" class="toggle-password">
            <?= DactylKit::icon(DactylKit::ICON_EYE); ?>
        </span>
        <?= $form->field($model, 'confirm_password')->passwordInput([
            'autofocus' => true,
            'spellcheck' => 'false',
            'encoded' => false,
            'class' => 'conf-password',
        ]) ?>
    </div>
</div>

<script>getAllFlashAlertMessages();</script>