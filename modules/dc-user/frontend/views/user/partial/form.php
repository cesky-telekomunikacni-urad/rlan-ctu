<?php
/*
* Copyright 2023 DactylGroup s.r.o.
*
* Licensed under the EUPL, Version 1.2 or – as soon they
will be approved by the European Commission - subsequent
versions of the EUPL (the "Licence");
* You may not use this work except in compliance with the
Licence.
* You may obtain a copy of the Licence at:
*
* https://joinup.ec.europa.eu/software/page/eupl5
*
* Unless required by applicable law or agreed to in
writing, software distributed under the Licence is
distributed on an "AS IS" basis,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
express or implied.
* See the Licence for the specific language governing
permissions and limitations under the Licence.
*/
?>


<?php

use frontend\widgets\ActiveForm;
use modules\user\common\models\RegisterForm;
use modules\user\common\models\User;

/**
 * @var RegisterForm $model
 * @var bool $company
 * @var ActiveForm $form
 */

if ($model->getScenario() == RegisterForm::SCENARIO_REGISTER_USER) {
    $type = 'user';
} elseif ($model->getScenario() == RegisterForm::SCENARIO_REGISTER_COMPANY) {
    $type = 'company';
} else {
    $type = false;
}

?>

<div class="register_inner_form <?= ($company ? 'company_form' : 'user_form') ?>"
    <?php if ($type) : ?>
        style="<?= (($company && $type == 'user') || (!$company && $type == 'company')) ? 'display:none;' : '' ?>"
    <?php endif; ?>>

    <?php if ($model->isNewRecord): ?>
        <h5 class="register_subtitle">
            <?= _tF($company ? 'account_create_company_title' : 'account_create_individual_title', 'user'); ?>
        </h5>
    <?php else: ?>
        <h5 class="register_subtitle">
            <?= _tF($company ? 'account_update_company_title' : 'account_update_user_title', 'user'); ?>
        </h5>
    <?php endif; ?>

    <?php if ($company) : ?>
        <div class="row">
            <div class="col-sm-12">
                <?= $form->field($model, 'companyName')->textInput() ?>
            </div>
        </div>
    <?php else: ?>
        <div class="row">
            <div class="col-sm-6">
                <?= $form->field($model, 'first_name')->textInput() ?>
            </div>
            <div class="col-sm-6">
                <?= $form->field($model, 'last_name')->textInput() ?>
            </div>
        </div>
    <?php endif; ?>

    <?php if ($company) : ?>
        <div class="row">
            <div class="col-sm-6">
                <?= $form->field($model, 'vatNumber')->textInput(['data-mask' => '00000000']) ?>
            </div>
            <div class="col-sm-6">
                <?= $form->field($model, 'taxNumber')->textInput(['data-mask' => 'SS0000000099']) ?>
            </div>
        </div>
    <?php endif; ?>

</div>
