<?php
/*
* Copyright 2023 DactylGroup s.r.o.
*
* Licensed under the EUPL, Version 1.2 or – as soon they
will be approved by the European Commission - subsequent
versions of the EUPL (the "Licence");
* You may not use this work except in compliance with the
Licence.
* You may obtain a copy of the Licence at:
*
* https://joinup.ec.europa.eu/software/page/eupl5
*
* Unless required by applicable law or agreed to in
writing, software distributed under the Licence is
distributed on an "AS IS" basis,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
express or implied.
* See the Licence for the specific language governing
permissions and limitations under the Licence.
*/
?>


<?php

use common\models\StationImport;
use frontend\components\FrontendView;
use yii\helpers\Html;

/**
 * @var FrontendView $this
 * @var array $result
 */

/**
 * @param array $errors
 *
 * @return string
 */
function formatErrorMessages(array $errors): string
{
    $return = '';

    if (isset($errors['error']) && !empty($errors['error'])) {
        $return .= '<ul>';
        $baseErrs = (is_array(reset($errors['error']))) ? call_user_func_array('array_merge', $errors['error']) :
            $errors['error'];
        foreach (array_unique($baseErrs) as $error) {
            $return .= "<li>$error</li>";
        }
        $return .= '</ul>';
    }

    return $return;
}

?>

<?php if (empty($result)): ?>
    <?= _tF('no_stations_imported', 'user-import'); ?>
<?php else: ?>
    <ul>
        <?php foreach ($result as $row => $stationResult): ?>

            <?php $isDelete = isset($stationResult['delete']) && $stationResult['delete']; ?>

            <?php if ($stationResult['status'] == StationImport::RESULT_SUCCESS): ?>
                <li class="success">
                    <b><?= _tF('import_line', 'user-import', ['row' => $row]); ?></b>

                    <?php if ($isDelete): ?>
                        <?= _tF('success_import_and_delete', 'user-import', [
                            'id' => $stationResult['station'],
                        ]); ?>
                    <?php else: ?>
                        <?= _tF('success_import', 'user-import', [
                            'link' => Html::a(_tF('station_detail', 'user-import') . " {$stationResult['station']}",
                                url(['/station/station', 'id' => $stationResult['station']]),
                                ['target' => '_blank', 'data-pjax' => '0']),
                        ]); ?>
                    <?php endif; ?>

                </li>
            <?php elseif ($stationResult['status'] == StationImport::RESULT_WARNING): ?>
                <li class="warning">
                    <b><?= _tF('import_line', 'user-import', ['row' => $row]); ?></b>
                    <?= _tF('warning_import', 'user-import'); ?>
                </li>
            <?php elseif ($stationResult['status'] >= StationImport::RESULT_LIMIT): ?>
                <li>
                    <b><?= _tF('import_line', 'user-import', ['row' => $row]); ?></b>
                    <?= _tF('warning_user_limit_matched', 'user-import'); ?>
                </li>
            <?php else: ?>
                <li class="error">
                    <b><?= _tF('import_line', 'user-import', ['row' => $row]); ?></b>
                    <?= _tF(!$isDelete ? 'error_import' : 'error_import_on_delete', 'user-import', [
                        'error' => formatErrorMessages($stationResult),
                    ]); ?>
                </li>
            <?php endif; ?>
        <?php endforeach; ?>
    </ul>
<?php endif; ?>
