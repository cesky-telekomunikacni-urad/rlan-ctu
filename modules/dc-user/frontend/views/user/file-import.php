<?php
/*
* Copyright 2023 DactylGroup s.r.o.
*
* Licensed under the EUPL, Version 1.2 or – as soon they
will be approved by the European Commission - subsequent
versions of the EUPL (the "Licence");
* You may not use this work except in compliance with the
Licence.
* You may obtain a copy of the Licence at:
*
* https://joinup.ec.europa.eu/software/page/eupl5
*
* Unless required by applicable law or agreed to in
writing, software distributed under the Licence is
distributed on an "AS IS" basis,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
express or implied.
* See the Licence for the specific language governing
permissions and limitations under the Licence.
*/
?>


<?php
// Uses
use frontend\assets\UserAsset;
use frontend\widgets\ActiveForm;
use frontend\widgets\dactylkit\DactylKit;
use modules\user\frontend\models\ImportFileUploadForm;

// Sources
/* @var $this \frontend\components\FrontendView */
/* @var $formModel ImportFileUploadForm */
/* @var $result array */

// Settings
$this->title = _tF('import', 'user-import');
$title = $this->title;

UserAsset::register($this);
?>

<?php $form = ActiveForm::begin([
    'id' => 'import-form',
    'enableAjaxValidation' => false,
    'options' => [
        'class' => 'import-form',
        'data-pjax' => true,
        'data-pjax-replace-state' => false,
        'data-pjax-push-state' => false,
        'enctype' => 'multipart/form-data',
        'data-confirm-alert-text' => _tF('confirm_alert_text', 'user-import'),
    ],
]); ?>

<div class="livebox-user">
    <div class="mapOverlay" style="display: none">
        <div class="lds-grid">
            <div></div>
            <div></div>
            <div></div>
            <div></div>
            <div></div>
            <div></div>
            <div></div>
            <div></div>
            <div></div>
        </div>
    </div>
    <div class="livebox-user__header">
    </div>
    <div class="livebox-user__content import-forms">

        <h4 class="livebox-user__content__title"><?= $title ?></h4>
        <div class="livebox-user__content__forms">
            <div class="form-container">
                <div class="row">
                    <div class="col-sm-12">
                        <p> <?= _tF('import_help_text', 'user-import'); ?> </p>
                    </div>
                </div>
                <div class="row">
                    <div class="col-sm-12">
                        <?= $form->field($formModel, 'importFile')
                                 ->fileInput([])
                                 ->label($formModel->getAttributeLabel('importFile'),
                                     ['class' => 'form-control']) ?>
                    </div>
                </div>
                <div class="livebox-user__content__button">
                    <?= DactylKit::button(_tF('import_submit_button', 'user-import'),
                        DactylKit::BUTTON_TYPE_PRIMARY,
                        '', '', '', [
                            'type' => 'submit',
                        ]) ?>
                </div>
            </div>
        </div>
    </div>
    <div class="livebox-user__content__results import-forms">
        <div class="row">
            <div class="col-sm-12">
                <?php if (!empty($result)): ?>
                    <?= DactylKit::alert('', $this->render('partial/import-result', ['result' => $result]),
                        DactylKit::ALERT_TYPE_INFO, false, ''); ?>
                <?php endif; ?>
            </div>
        </div>
    </div>
</div>

<?php ActiveForm::end(); ?>

<?php $this->registerJs(
    <<<JS

$(document).on('pjax:start', function(e, jqXHR, settings) {
    var livebox = LiveBox.getInstance($('.livebox-user').closest('.livebox').data('lid'));
        livebox.set('afterClose', function () {
            jqXHR.abort();
            mainApp.loadItems();
            mainApp.map.runMap('map');
        });
})

$(document).on('submit', '#import-form', function (e) {
    if($('#importfileuploadform-importfile').val()!=='' && !confirm($(this).data('confirm-alert-text'))){
        e.preventDefault();
        return false;
    }
    
    $(".livebox-user .mapOverlay").show();
  });
JS

); ?>
