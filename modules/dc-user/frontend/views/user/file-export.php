<?php
/*
* Copyright 2023 DactylGroup s.r.o.
*
* Licensed under the EUPL, Version 1.2 or – as soon they
will be approved by the European Commission - subsequent
versions of the EUPL (the "Licence");
* You may not use this work except in compliance with the
Licence.
* You may obtain a copy of the Licence at:
*
* https://joinup.ec.europa.eu/software/page/eupl5
*
* Unless required by applicable law or agreed to in
writing, software distributed under the Licence is
distributed on an "AS IS" basis,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
express or implied.
* See the Licence for the specific language governing
permissions and limitations under the Licence.
*/
?>


<?php
// Uses
use frontend\assets\UserAsset;
use frontend\widgets\ActiveForm;
use frontend\widgets\dactylkit\DactylKit;
use modules\user\frontend\models\ImportFileUploadForm;

// Sources
/* @var $this \frontend\components\FrontendView */
/* @var $formModel ImportFileUploadForm */
/* @var $result string */
/* @var $resultType string */
/* @var $tempFileName string */

// Settings
$this->title = _tF('export', 'user-export');
$title = $this->title;

UserAsset::register($this);

$form = ActiveForm::begin([
    'id' => 'export-form',
    'enableAjaxValidation' => false,
    'options' => [
        'class' => 'export-form',
        'data-pjax' => true,
        'data-pjax-replace-state' => false,
        'data-pjax-push-state' => false,
    ],
]);

?>

<div class="livebox-user">
    <div class="mapOverlay" style="display: none">
        <div class="lds-grid">
            <div></div>
            <div></div>
            <div></div>
            <div></div>
            <div></div>
            <div></div>
            <div></div>
            <div></div>
            <div></div>
        </div>
    </div>
    <div class="livebox-user__header">
    </div>
    <div class="livebox-user__content export-forms">

        <h4 class="livebox-user__content__title"><?= $title ?></h4>
        <div class="livebox-user__content__forms">
            <div class="form-container">
                <div class="row">
                    <div class="col-sm-12">
                        <p> <?= _tF('export_help_text', 'user-export'); ?> </p>
                    </div>
                </div>
                <div class="livebox-user__content__button">
                    <?= DactylKit::button(_tF('export_submit_button', 'user-export'),
                        DactylKit::BUTTON_TYPE_PRIMARY,
                        '', '', '', [
                            'type' => 'submit',
                            'id' => 'export-submit-button',
                            'data-temp-name' => url([
                                "stations-download-export",
                                'user_id' => loggedUser()->id,
                                'temp_name' => $tempFileName,
                            ], true),
                        ]) ?>
                </div>
            </div>
        </div>
        <?php if (!empty($result)): ?>
            <div class="livebox-user__content__results export-forms">
                <?= DactylKit::alert('', $result,
                    ($resultType == "success") ? DactylKit::ALERT_TYPE_SUCCESS : DactylKit::ALERT_TYPE_WARNING,
                    false, ''); ?>
            </div>
        <?php endif; ?>
    </div>
</div>

<?php ActiveForm::end(); ?>

<?php $this->registerJs(
    <<<JS
$(document).on('pjax:start', function (e) {
    $(".livebox-user .mapOverlay").show();    
});

$(document).on('pjax:end', function(e, jqXHR, settings) {
    $(".livebox-user .mapOverlay").hide();
    
    // Open non Pjax file download
    var downloadLink = $('#export-submit-button').data('temp-name')
    if (e.relatedTarget.id == "export-form" && downloadLink) {
        location.href = ( downloadLink );
    }
})
JS

); ?>
