<?php
/*
* Copyright 2023 DactylGroup s.r.o.
*
* Licensed under the EUPL, Version 1.2 or – as soon they
will be approved by the European Commission - subsequent
versions of the EUPL (the "Licence");
* You may not use this work except in compliance with the
Licence.
* You may obtain a copy of the Licence at:
*
* https://joinup.ec.europa.eu/software/page/eupl5
*
* Unless required by applicable law or agreed to in
writing, software distributed under the Licence is
distributed on an "AS IS" basis,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
express or implied.
* See the Licence for the specific language governing
permissions and limitations under the Licence.
*/
?>


<?php

use frontend\assets\UserAsset;
use frontend\widgets\ActiveForm;
use frontend\widgets\dactylkit\DactylKit;
use modules\user\common\models\RegisterForm;

/**
 * @var $model RegisterForm
 * @var $isAjax bool
 */

$title = _tF('register_submit_button', 'user');
$this->title = $title;

UserAsset::register($this);

?>

<?php $form = ActiveForm::begin([
    'id' => "register-form",
    'enableAjaxValidation' => false,
    'options' => [
        'class' => "register-form pjax",
        'data-pjax' => true,
        'data-pjax-replace-state' => false,
        'data-pjax-push-state' => false,
    ],
]); ?>

    <div class="livebox-user">
        <div class="livebox-user__header">
            <div class="livebox-user__header__buttons">
                <?= DactylKit::link(_tF('sign in', 'header'),
                    url(['/dc-user/user/login']),
                    DactylKit::LINK_TYPE_GHOST,
                    DactylKit::LINK_SIZE_MINI, '', '', [
                        'class' => 'dk--btn dk--btn--ghost dk--btn--mini',
                    ]) ?>
            </div>
        </div>
        <div class="livebox-user__content register-forms">
            <h4 class="livebox-user__content__title"><?= $title ?></h4>
            <h5 class="livebox-user__content__subtitle"><?= _tF('account_create_subtitle', 'user') ?></h5>

            <div class="livebox-user__content__chips">
                <?= DactylKit::chip(_tF('register-individual-type', 'user'), '', DactylKit::CHIP_SIZE_TALL, '', '', [
                    'class' => 'chip-left' . (($model->type == 'user') ? ' selected' : ''),
                    'data-form' => 'user_form',
                ]); ?>
                <?= DactylKit::chip(_tF('register-company-type', 'user'), '', DactylKit::CHIP_SIZE_TALL, '', '', [
                    'class' => ($model->type == 'company') ? 'selected' : '',
                    'data-form' => 'company_form',
                ]); ?>
            </div>
            <div class="livebox-user__content__forms" <?= (!$model->type) ? 'style="display: none"' : '' ?>>
                <?= Yii::$app->controller->renderPartial('@modules/dc-user/frontend/views/user/register', [
                    'isCompany' => false,
                    'model' => $model,
                    'form' => $form,
                ]) ?>
            </div>
        </div>
    </div>

<?php ActiveForm::end(); ?>
