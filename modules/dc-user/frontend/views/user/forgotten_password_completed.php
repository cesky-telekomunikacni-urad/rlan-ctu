<?php
/*
* Copyright 2023 DactylGroup s.r.o.
*
* Licensed under the EUPL, Version 1.2 or – as soon they
will be approved by the European Commission - subsequent
versions of the EUPL (the "Licence");
* You may not use this work except in compliance with the
Licence.
* You may obtain a copy of the Licence at:
*
* https://joinup.ec.europa.eu/software/page/eupl5
*
* Unless required by applicable law or agreed to in
writing, software distributed under the Licence is
distributed on an "AS IS" basis,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
express or implied.
* See the Licence for the specific language governing
permissions and limitations under the Licence.
*/
?>


<?php
// Uses
use dactylcore\core\widgets\common\pjax\Pjax;
use dactylcore\core\widgets\frontend\activeform\ActiveForm;
use dactylcore\user\frontend\assets\UserFormAsset;
use frontend\assets\UserAsset;
use dactylcore\core\web\FrontendView;
use dactylcore\user\frontend\models\PasswordResetRequestForm;
use frontend\widgets\dactylkit\DactylKit;
use modules\user\common\models\User;

// Sources
/* @var $this FrontendView */
/* @var $form ActiveForm */
/* @var $model PasswordResetRequestForm */
/* @var $user User */

// Assets
UserFormAsset::register($this);
UserAsset::register($this);

// Settings
$this->title = _tF('forgotten_password_completed_title', 'user');
?>

<?php Pjax::begin([
        'options' => [
            'id' => 'user-form-pjax',
        ],
        'enablePushState' => false,
        'enableReplaceState' => false,
    ]
); ?>

    <div class="livebox-user">
        <div class="livebox-user__header">
            <div class="livebox-user__header__buttons">
                <?= DactylKit::link(_tF('create account', 'header'),
                    url(['/dc-user/user/register']),
                    DactylKit::LINK_TYPE_GHOST,
                    DactylKit::LINK_SIZE_MINI, '', '', [
                        'class' => 'dk--btn dk--btn--ghost dk--btn--mini',
                    ]) ?>
            </div>
        </div>
        <div class="livebox-user__content">
            <h4 class="livebox-user__content__title"><?= $this->title ?></h4>

            <div class="livebox-password-sent-text">
                <p><?= _tF('forgotten_password_completed_heading{email}', 'user', ['email' => $user->email]); ?></p>
                <p class="subtext"> <?= _t('forgotten_password_completed_subheading', 'user') ?><br></p>
            </div>

            <div class="livebox-user__content__buttons">
                <?= DactylKit::link(_tF('send-email-again', 'user'),
                    url(['/dc-user/user/resend-forgotten-password-email', 'id' => $user->id]),
                    DactylKit::LINK_TYPE_SECONDARY,
                    DactylKit::LINK_SIZE_MINI, '', '', [
                        'class' => 'dk--btn dk--btn--secondary dk--btn--mini',
                    ]) ?>
                <?= DactylKit::link(_tF('back-to-login-button', 'user'),
                    url(['/dc-user/user/login']),
                    DactylKit::LINK_TYPE_PRIMARY,
                    DactylKit::LINK_SIZE_MINI, '', '', [
                        'class' => 'dk--btn dk--btn--primary dk--btn--mini',
                    ]) ?>
            </div>
        </div>
    </div>
    <script>getAllFlashAlertMessages();</script>

<?php Pjax::end() ?>