<?php
/*
* Copyright 2023 DactylGroup s.r.o.
*
* Licensed under the EUPL, Version 1.2 or – as soon they
will be approved by the European Commission - subsequent
versions of the EUPL (the "Licence");
* You may not use this work except in compliance with the
Licence.
* You may obtain a copy of the Licence at:
*
* https://joinup.ec.europa.eu/software/page/eupl5
*
* Unless required by applicable law or agreed to in
writing, software distributed under the Licence is
distributed on an "AS IS" basis,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
express or implied.
* See the Licence for the specific language governing
permissions and limitations under the Licence.
*/
?>


<?php

use frontend\widgets\ActiveForm;
use frontend\widgets\dactylkit\DactylKit;
use modules\user\common\models\RegisterForm;

/**
 * @var $this \frontend\components\FrontendView
 * @var $model RegisterForm
 * @var $isCompany bool
 * @var $form ActiveForm
 */

$deleteOptions = [
    'data' => [
      'password-text' => _tF('Confirm with your password', 'register'),
      'confirm-text' => _tF('confirm', 'register'),
      'cancel-text' => _tF('cancel', 'register'),
      'delete-link' => url('/dc-user/user/delete'),
    ],
    'id' => 'btn-delete-profile',
    'class' => 'delete-button dk--btn dk--btn--secondary',
];

if (!$model->isNewRecord) {
  \frontend\assets\UserRemoveAsset::register($this);
}

?>

<div class="form-container">

    <?php if ($model->isNewRecord) : ?>

        <?= $this->render('partial/form', [
            'form' => $form,
            'model' => $model,
            'company' => false,
        ]) ?>

        <?= $this->render('partial/form', [
            'form' => $form,
            'model' => $model,
            'company' => true,
        ]) ?>
    <?php else : ?>
        <?= $this->render('partial/form', [
            'form' => $form,
            'model' => $model,
            'company' => ($model->getScenario() == RegisterForm::SCENARIO_UPDATE_COMPANY),
        ]) ?>
    <?php endif; ?>

    <div class="row">
        <div class="col-sm-12">
            <?= $form->field($model, 'street')->textInput() ?>
        </div>
    </div>

    <div class="row">
        <div class="col-sm-6">
            <?= $form->field($model, 'city')->textInput() ?>
        </div>
        <div class="col-sm-6">
            <?= $form->field($model, 'zipCode')->textInput(['data-mask' => '000 00']) ?>
        </div>
    </div>

    <div class="row">
        <div class="col-sm-12">
            <?php $emailField = $form->field($model, 'email')->textInput() ?>

            <?php if ($model->scenario == RegisterForm::SCENARIO_UPDATE_COMPANY ||
                $model->scenario == RegisterForm::SCENARIO_UPDATE_USER) : ?>
                <?php $emailField->textInput(['readonly' => true, 'disabled' => 'disabled']) ?>
            <?php endif; ?>

            <?= $emailField; ?>
        </div>
    </div>
    <?php if ($model->isNewRecord) : ?>
        <div class="row">
            <div class="col-sm-12">
                <span toggle=".first-password" class="toggle-password">
                    <?= DactylKit::icon(DactylKit::ICON_EYE); ?>
                </span>
                <?= $form->field($model, 'password',
                    ['errorOptions' => ['encode' => false, 'class' => 'help-block']])
                         ->passwordInput(['class' => 'first-password']); ?>
            </div>
        </div>
    <?php else : ?>
        <div class="row">
            <div class="col-sm-12">
                <span toggle=".new-password" class="toggle-password">
                    <?= DactylKit::icon(DactylKit::ICON_EYE); ?>
                </span>
                <?= $form->field($model, 'new_password',
                    ['errorOptions' => ['encode' => false, 'class' => 'help-block']])
                         ->passwordInput(['class' => 'new-password']); ?>
            </div>
        </div>
        <div class="row">
            <div class="col-sm-12">
                <span toggle=".conf-password" class="toggle-password">
                    <?= DactylKit::icon(DactylKit::ICON_EYE); ?>
                </span>
                <?= $form->field($model, 'confirm_new_password',
                    ['errorOptions' => ['encode' => false, 'class' => 'help-block']])
                         ->passwordInput(['class' => 'conf-password']); ?>
            </div>
        </div>
        <div class="row">
            <div class="col-sm-12">
                <span toggle=".old-password" class="toggle-password">
                    <?= DactylKit::icon(DactylKit::ICON_EYE); ?>
                </span>
                <?= $form->field($model, 'old_password',
                    ['errorOptions' => ['encode' => false, 'class' => 'help-block'],])
                         ->passwordInput(['class' => 'old-password']); ?>
            </div>
        </div>
    <?php endif; ?>

    <?php if ($model->isNewRecord) : ?>
        <?= $form->field($model, 'type')->hiddenInput(['id' => 'hidden-type'])->label(false) ?>
        <?= $form->field($model, 'reCaptcha')->widget(\himiklab\yii2\recaptcha\ReCaptcha::class) ?>
        <?= $form->field($model, 'conditionsConfirmed',
            ['template' => '{input}{label}', 'options' => ['class' => 'conditions form-group']])
                 ->checkbox(['class' => 'conditionsCheckbox'])->label(_t('conditionsConfirmed', 'register')); ?>


        <div class="livebox-user__content__button">
            <?= DactylKit::button(_tF('register_submit_button', 'user'),
                DactylKit::BUTTON_TYPE_PRIMARY,
                '', '', '', [
                    'type' => 'submit',
                ]) ?>
        </div>

    <?php else : ?>
        <div class="livebox-user__content__buttons">
            <div data-pjax="0">
                <?= DactylKit::link(
                    _t('delete profile', 'register'),
                    'javascript',
                    DactylKit::LINK_TYPE_SECONDARY,
                    DactylKit::LINK_SIZE_NORMAL,
                    '', '', $deleteOptions,
                    ); ?>
            </div>
            <?= DactylKit::button(_tF('update_submit_button', 'user'),
                DactylKit::BUTTON_TYPE_PRIMARY,
                '', '', '', [
                    'type' => 'submit',
                ]) ?>
        </div>
    <?php endif; ?>

</div>