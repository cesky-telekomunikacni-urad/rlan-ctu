<?php
/*
* Copyright 2023 DactylGroup s.r.o.
*
* Licensed under the EUPL, Version 1.2 or – as soon they
will be approved by the European Commission - subsequent
versions of the EUPL (the "Licence");
* You may not use this work except in compliance with the
Licence.
* You may obtain a copy of the Licence at:
*
* https://joinup.ec.europa.eu/software/page/eupl5
*
* Unless required by applicable law or agreed to in
writing, software distributed under the Licence is
distributed on an "AS IS" basis,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
express or implied.
* See the Licence for the specific language governing
permissions and limitations under the Licence.
*/
?>


<?php


$title = _t('forgotten_password', 'register');
$this->title = $title;

use frontend\assets\UserAsset;
use frontend\widgets\ActiveForm;
use frontend\widgets\dactylkit\DactylKit;

UserAsset::register($this)

?>


<?php $form = ActiveForm::begin([
    'id' => 'request-password-form',
    'enableAjaxValidation' => false,
    'options' => [
        'class' => 'white-form request-password-form',
        'data-pjax' => true,
        'data-pjax-replace-state' => false,
        'data-pjax-push-state' => false,
    ],
]); ?>

    <div class="livebox-user">
        <div class="livebox-user__header">
            <div class="livebox-user__header__buttons">
                <?= DactylKit::link(_tF('create account', 'header'),
                    url(['/dc-user/user/register']),
                    DactylKit::LINK_TYPE_GHOST,
                    DactylKit::LINK_SIZE_MINI, '', '', [
                        'class' => 'dk--btn dk--btn--ghost dk--btn--mini',
                    ]) ?>
            </div>
        </div>
        <div class="livebox-user__content">

            <h4 class="livebox-user__content__title"><?= $title ?></h4>


            <span class="livebox-user__content__subtitle"><?= _t('set_mail_to_reset', 'register') ?></span>

            <div class="row">
                <div class="col-sm-12">
                    <?= $form->field($model, 'email')->textInput() ?>
                </div>
            </div>

            <div class="livebox-user__content__buttons">
                <?= DactylKit::link(_tF('back_to_login_button', 'register'),
                    url(['/dc-user/user/login']),
                    DactylKit::LINK_TYPE_SECONDARY,
                    DactylKit::LINK_SIZE_MINI, '', '', [
                        'class' => 'dk--btn dk--btn--secondary dk--btn--mini',
                    ]) ?>
                <?= DactylKit::button(_tF('request_pass_submit_button', 'register'),
                    DactylKit::BUTTON_TYPE_PRIMARY,
                    DactylKit::BUTTON_SIZE_MINI, '', '', [
                        'type' => 'submit',
                    ]) ?>
            </div>
        </div>
    </div>
    <script>getAllFlashAlertMessages();</script>

<?php ActiveForm::end(); ?>