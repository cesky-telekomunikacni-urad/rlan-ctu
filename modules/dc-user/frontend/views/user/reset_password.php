<?php
/*
* Copyright 2023 DactylGroup s.r.o.
*
* Licensed under the EUPL, Version 1.2 or – as soon they
will be approved by the European Commission - subsequent
versions of the EUPL (the "Licence");
* You may not use this work except in compliance with the
Licence.
* You may obtain a copy of the Licence at:
*
* https://joinup.ec.europa.eu/software/page/eupl5
*
* Unless required by applicable law or agreed to in
writing, software distributed under the Licence is
distributed on an "AS IS" basis,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
express or implied.
* See the Licence for the specific language governing
permissions and limitations under the Licence.
*/
?>


<?php

use dactylcore\user\frontend\assets\UserFormAsset;
use frontend\assets\UserAsset;
use frontend\widgets\ActiveForm;
use frontend\widgets\dactylkit\DactylKit;

/* @var $this yii\web\View */
/* @var $form ActiveForm */
UserFormAsset::register($this);
UserAsset::register($this);

// Settings
$this->title = _tF('reset_password_title', 'user');

?>

<?php $form = ActiveForm::begin([
    'id' => 'reset-password-form',
    'enableAjaxValidation' => false,
    'options' => [
        'class' => 'white-form reset-password-form',
        'data-pjax' => true,
        'data-pjax-replace-state' => false,
        'data-pjax-push-state' => false,
    ],
]); ?>

<div class="livebox-user">
    <div class="livebox-user__header">
    </div>
    <div class="livebox-user__content">
        <h4><?= _tF('reset_password_heading', 'user'); ?></h4>

        <div class="livebox-reset-form-subtitle"> <?= _t('reset_password_subheading', 'user') ?></div>

        <div class="livebox-user__content__forms">
            <?= $this->render('partial/reset-password-form', [
                'model' => $model,
                'form' => $form,
            ]) ?>
        </div>

        <div class="livebox-user__content__buttons aright">
            <?= DactylKit::button(_tF('reset_password_button', 'user'),
                DactylKit::BUTTON_TYPE_PRIMARY,
                DactylKit::BUTTON_SIZE_MINI, '', '', [
                    'type' => 'submit',
                ]) ?>
        </div>
    </div>
</div>
<script>getAllFlashAlertMessages();</script>

<?php ActiveForm::end(); ?>
<!-- END FORGOT PASSWORD FORM -->