<?php
/*
* Copyright 2023 DactylGroup s.r.o.
*
* Licensed under the EUPL, Version 1.2 or – as soon they
will be approved by the European Commission - subsequent
versions of the EUPL (the "Licence");
* You may not use this work except in compliance with the
Licence.
* You may obtain a copy of the Licence at:
*
* https://joinup.ec.europa.eu/software/page/eupl5
*
* Unless required by applicable law or agreed to in
writing, software distributed under the Licence is
distributed on an "AS IS" basis,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
express or implied.
* See the Licence for the specific language governing
permissions and limitations under the Licence.
*/
?>


<?php
// Uses
use frontend\assets\map\UserMapAsset;
use frontend\assets\UserAsset;
use frontend\widgets\dactylkit\DactylKit;

// Sources
/* @var $this \dactylcore\core\web\AdminView */
/* @var $model \dactylcore\user\common\models\User */
/* @var $byAdmin bool */

// Settings
$pageId = 'user';
$this->title = _tF('profile', 'user');

// Breadcrumbs
$this->breadcrumbs[] = [
    'url' => url(['/dc-user/user/index']),
    'label' => $this->title,
];
$this->breadcrumbs[] = [
    'label' => $model->getName(),
];

$enableEmailVerification = Yii::$app->getModule('dc-user')->enableEmailVerification;

$isByAdmin = false;
if (isset($byAdmin)) {
    $isByAdmin = $byAdmin;
}

// Table translated labels
$ofLabel = _t('of', 'stations-table');
$emptyStations = _tF('empty_table', 'stations-table');

UserAsset::register($this);
UserMapAsset::register($this);
?>

<div class="dk--column-holder row">
    <section class="dk--container col-lg-6 left-container">
        <div class="dk--container__content user-profile">
            <?= DactylKit::breadcrumb(); ?>
            <h3><?= $model->getName() ?> <?= $model->getInitialsIcon() ?></h3>
            <div class="user-profile__about">
                <h5><?= _tF('about', 'user') ?></h5>
                <div class="row">
                    <div class="col-lg-3 col-md-12 text-nowrap">
                        <?= _tF('user_id', 'user') ?>
                    </div>
                    <div class="col-lg-9 col-md-12 user-profile__about__value">
                        <span class="lock-icon" <?= DactylKit::tooltip(_tf('lock_tooltip', 'user')) ?>>
                            <?= DactylKit::icon(DactylKit::ICON_LIGHT_LOCK) ?>
                        </span>
                            <?= $model->id ?>
                    </div>
                </div>
                <div class="row">
                    <div class="col-lg-3 col-md-12 text-nowrap">
                        <?= _tF('user_type', 'user') ?>
                    </div>
                    <div class="col-lg-9 col-md-12 user-profile__about__value">
                        <span class="lock-icon" <?= DactylKit::tooltip(_tf('lock_tooltip', 'user')) ?>>
                            <?= DactylKit::icon(DactylKit::ICON_LIGHT_LOCK) ?>
                        </span>
                        <?php if ($model->type): ?>
                            <?= _tF("{$model->type}-type-profile", 'user') ?>
                        <?php else: ?>
                            ~
                        <?php endif; ?>
                    </div>
                </div>
                <div class="row">
                    <div class="col-lg-3 col-md-12">
                        <?= _tF('address', 'user') ?>
                    </div>
                    <div class="col-lg-9 col-md-12 user-profile__about__value">
                        <span class="lock-icon" <?= DactylKit::tooltip(_tf('lock_tooltip', 'user')) ?>>
                            <?= DactylKit::icon(DactylKit::ICON_LIGHT_LOCK) ?>
                        </span>
                        <?php if ($model->street || $model->zipCode || $model->city): ?>
                            <?= "{$model->street}, {$model->zipCode} {$model->city}" ?>
                        <?php else: ?>
                            ~
                        <?php endif; ?>
                    </div>
                </div>
                <div class="row">
                    <div class="col-lg-3 col-md-12">
                        <?= _tF('email', 'user') ?>
                    </div>
                    <div class="col-lg-9 col-md-12 user-profile__about__value">
                        <span class="lock-icon" <?= DactylKit::tooltip(_tf('lock_tooltip', 'user')) ?>>
                            <?= DactylKit::icon(DactylKit::ICON_LIGHT_LOCK) ?>
                        </span>
                        <?= "{$model->email}" ?>
                    </div>
                </div>
                <?php if ($model->isInAlliance()): ?>
                    <div class="row">
                        <div class="col-lg-3 col-md-12">
                            <?= _tF('in_alliance', 'user-alliance') ?>
                        </div>
                        <div class="col-lg-9 col-md-12 user-profile__about__value">
                        <span class="lock-icon" <?= DactylKit::tooltip(_tf('lock_tooltip', 'user')) ?>>
                            <?= DactylKit::icon(DactylKit::ICON_LIGHT_LOCK) ?>
                        </span>
                            <?= "{$model->alliance->name}" ?>
                        </div>
                    </div>
                <?php endif; ?>

                <?= DactylKit::link(
                    _tF('settings', 'user'),
                    ($isByAdmin) ? url(['/dc-user/user/update', 'id' => $model->id]) : url(['/dc-user/user/update']),
                    DactylKit::LINK_TYPE_SECONDARY,
                    DactylKit::LINK_SIZE_NORMAL, DactylKit::ICON_SETTINGS, '', [
                    'class' => 'dk--btn dk--btn--secondary',
                    'data' => [
                        'livebox' => 1,
                    ],
                ]) ?>

                <?= DactylKit::link(
                    _tF('alliance', 'user-alliance'),
                    ($isByAdmin) ? url(['/user-alliance/index', 'idUser' => $model->id]) : url(['/user-alliance/index']),
                    DactylKit::LINK_TYPE_SECONDARY,
                    DactylKit::LINK_SIZE_NORMAL, DactylKit::ICON_GROUPS, '', [
                    'class' => 'dk--btn dk--btn--secondary alliance-btn',
                    'data' => [
                        'livebox' => 1,
                    ],
                ]) ?>

                <?= DactylKit::link(
                    _tF('import', 'user'),
                    url(['/dc-user/user/file-import']),
                    DactylKit::LINK_TYPE_SECONDARY,
                    DactylKit::LINK_SIZE_NORMAL, DactylKit::ICON_IMPORT, '', [
                    'class' => 'dk--btn dk--btn--secondary',
                    'data' => [
                        'livebox' => 1,
                    ],
                ]) ?>

                <?=
                DactylKit::link(
                    _tF('station_export', 'user-export'),
                    url(['/dc-user/user/stations-export']),
                    DactylKit::LINK_TYPE_SECONDARY,
                    DactylKit::LINK_SIZE_NORMAL, DactylKit::ICON_EXPORT, '', [
                    'class' => 'dk--btn dk--btn--secondary',
                    'data' => [
                        'livebox' => 1,
                    ],
                ]) ?>
            </div>
          <style>
              thead input {display: none !important;}
          </style>
            <div class="user-profile__stations">
                <h4><?= _tF('stations60ghz', 'station') ?></h4>

                <div class="stations-list" id="vueappprofile">
                  <div class="multicheck-buttons" v-if="selectedIds.length" style="margin-top: 12px">
                    <div class="prolong-labels">
                      <span id="oneStation"><?= _tF('prolong 1 station', 'station')?></span>
                      <span id="twoFourStations"><?= _tF('prolong {number} station two four', 'station')?></span>
                      <span id="fiveStations"><?= _tF('prolong {number} station five', 'station')?></span>
                    </div>
                    <?= DactylKit::button( _tF('{{selectedIdsTitle}}', 'station'),
                      DactylKit::BUTTON_TYPE_PRIMARY,
                        '',
                         '@frontend/web/source_assets/img/icon/ic-refresh-20.svg',
                        '',
                        ['v-on:click' => 'prolongSelectedStations']
                    ) ?>
                  </div>
                    <div data-link="<?= url(['station/detail', 'id' => 0]); ?>"></div>
                    <vue-good-table
                            mode="remote"
                            @on-page-change="onPageChange"
                            @on-sort-change="onSortChange"
                            @on-column-filter="onColumnFilter"
                            @on-per-page-change="onPerPageChange"
                            @on-row-mouseenter="onRowMouseEnter"
                            @on-row-mouseleave="onRowMouseLeave"
                            :total-rows="totalRecords"
                            :is-loading.sync="isLoading"
                            :pagination-options="{
                                    enabled: true,
                                    perPage: 100,
                                    dropdownAllowAll: false,
                                    ofLabel: '<?= $ofLabel ?>',
                                    }"
                            :rows="rows"
                            :columns="columns"
                            @on-search="onSearch"
                            :search-options="{enabled: true, externalQuery: searchTerm}"
                            styleClass="vgt-table"
                            @on-selected-rows-change="selectionChanged"
                            :select-options="{
    enabled: true,
    selectOnCheckboxOnly: true,
    selectionText: 'rows selected',
    clearSelectionText: '',
    disableSelectInfo: true,
    selectAllByGroup: false,


  }"

                    >


                        <div slot="emptystate"><?= $emptyStations ?></div>

                        <template slot="table-column" slot-scope="props">
                            <span v-if="props.column.label == 'name'">
                                <?= _tF('table_name', 'stations-table') ?>
                                <img class="sort-triangle-icon"
                                     src="/frontend/web/source_assets/img/icon/ic-caret-down-20.svg"/>
                            </span>
                            <span v-else-if="props.column.label == 'id'">
                                <?= _tF('table_id', 'stations-table') ?>
                                <img class="sort-triangle-icon"
                                     src="/frontend/web/source_assets/img/icon/ic-caret-down-20.svg"/>
                            </span>
                            <span v-else-if="props.column.label == 'type'">
                                <?= _tF('table_type', 'stations-table') ?>
                                <img class="sort-triangle-icon"
                                     src="/frontend/web/source_assets/img/icon/ic-caret-down-20.svg"/>
                            </span>
                            <span v-else-if="props.column.label == 'status'">
                                <?= _tF('table_status', 'stations-table') ?>
                                <img class="sort-triangle-icon"
                                     src="/frontend/web/source_assets/img/icon/ic-caret-down-20.svg"/>
                            </span>
                            <span v-else>
                                {{props.column.label}}
                                <img class="sort-triangle-icon"
                                     src="frontend/web/source_assets/img/icon/ic-caret-down-20.svg"/>
                            </span>
                        </template>

                        <template slot="table-row" slot-scope="props">
                            <span v-if="props.column.field == 'name'" class="col-with-link top-span name-link">
                                <a :href="getLink(props.row.id_m)" target="_blank"><span v-html="props.row.name"></span>
                                    <img v-if="props.row.type == 'fs' && props.row.pair_position == 'a'"
                                         :src="'/frontend/web/source_assets/img/icon/ic-position-a.svg'"
                                          <?= DactylKit::tooltip(_tF('tooltip_a_station', 'station')) ?>
                                    />
                                    <img v-if="props.row.type == 'fs' && props.row.pair_position == 'b'"
                                         :src="'/frontend/web/source_assets/img/icon/ic-position-b.svg'"
                                          <?= DactylKit::tooltip(_tF('tooltip_b_station', 'station')) ?>
                                    />
                                    <span v-html="props.row.hardware_identifier_code" class="hardware-identifier"></span>
                                </a>
                                <span class="icon-background">
                                    <span class="icon" <?= DactylKit::tooltip(_tF('show station on map', 'station'))?> @click="flyTo(props.row.lng, props.row.lat)"></span>
                                </span>
                            </span>
                            <span v-else-if="props.column.field == 'status'"
                                  class="status-column top-span col-with-link">
                                <a :href="getLink(props.row.id_m)" target="_blank">
                                    <span class="statusColumn" :class="props.row.status">{{props.row.statusT}}</span>

                                    <span class="dropdown status-column__dropdown">
                                        <span class="btn btn-default dropdown-toggle status-icon"
                                              :class="'state-'+props.row.state"
                                              data-toggle="dropdown" aria-haspopup="true" aria-expanded="true"
                                              v-html="props.row.stateIcon">
                                        </span>
                                        <span class="dropdown-menu status-column__dropdown__menu">
                                            <span class="status-column__dropdown__menu__item">
                                                <?= _tF('valid_until', 'stations-table') ?>:
                                                <span class="status-column__dropdown__menu__item__bold" :class="'date-state-'+props.row.valid_to_class">
                                                    {{props.row.valid_to}}
                                                </span>
                                            </span>
                                            <br>
                                            <span class="status-column__dropdown__menu__item">
                                                <?= _tF('protected_until', 'stations-table') ?>:
                                                <span class="status-column__dropdown__menu__item__bold" :class="'date-state-'+props.row.protected_to_class">
                                                    {{props.row.protected_to}}
                                                </span>
                                            </span>
                                        </span>
                                    </span>
                                </a>
                              <span v-if="props.row.can_edit" class="dropdown status-column__dropdown second_icon">
                                <span class="btn btn-default dropdown-toggle status-icon"
                                      :class="'state-'+props.row.state"
                                      data-toggle="dropdown" aria-haspopup="true" aria-expanded="true"
                                >
                                  <img src="/img/icon/ic-more-20.svg" style="padding: 5px">
                                </span>
                                <span class="dropdown-menu status-column__dropdown__menu">
                                  <ul class="grid-menu">
                                      <li v-if="props.row.prolong"><a :href="getProlongLink(props.row.id, true)" data-livebox="1"><?= _tF('prolong', 'station'); ?></a></li>
                                      <li><a :href="getLink(props.row.id, true)" data-livebox="1"><?= _tF('edit', 'station'); ?></a></li>
                                      <li
                                              v-if="props.row.can_edit && props.row.status == '<?= \common\models\Station::STATUS_FINISHED ?>'"
                                      >
                                          <a @click="unpublishStation(props.row.id)">
                                              <?= _tF('unpublish', 'station'); ?>
                                          </a>
                                      </li>
                                      <li v-if="props.row.can_edit"><a @click="deleteStation(props.row.id)"><?= _tF('delete', 'station'); ?></a></li>
                                    </ul>
                                  </span>
                       </span>
                            </span>
                            <span v-else-if="props.column.field == 'type'" class="col-with-link top-span">
                                <a :href="getLink(props.row.id_m)" target="_blank">{{props.row.typeT}}</a>
                            </span>
                            <span v-else-if="props.column.field == 'id'" class="col-with-link top-span">
                                <a :href="getLink(props.row.id_m)" target="_blank">
                                    {{props.formattedRow[props.column.field]}}
                                </a>
                            </span>
                            <span v-else class="top-span">
                              {{props.formattedRow[props.column.field]}}
                            </span>
                        </template>

                        <template slot="loadingContent">
                            <div class="stations-table-loading"><?= _Tf('table_loading', 'stations-table') ?></div>
                        </template>
                    </vue-good-table>
                </div>

                <?php  /*  ?= DactylKit::link(
                    _tF('show_all_stations', 'stations-table'),
                    url(['/']),
                    DactylKit::LINK_TYPE_PRIMARY,
                    DactylKit::LINK_SIZE_NORMAL,
                    '', DactylKit::ICON_CHEVRON_RIGHT, [
                        'class' => 'user-profile__stations__under-table-btn dk--btn dk--btn--primary dk--btn--normal',
                    ]
                ) */ ?>
            </div>
        </div>
    </section>
    <section class="dk--container dk--container__gray col-lg-6 right-container">
        <div class="dk--container__content"
             style="width: 100%;     height: calc(100vh - 110px); padding: 0; max-width: 100%; position: relative">
            <div class="mapOverlay overlayIndex">
                <div class="lds-grid">
                    <div></div>
                    <div></div>
                    <div></div>
                    <div></div>
                    <div></div>
                    <div></div>
                    <div></div>
                    <div></div>
                    <div></div>
                </div>
            </div>
            <div id="map" class="half-width map-container"
                 data-stationlink="<?= url(['/station/station', 'id' => 0]) ?>"
                 data-stationlinkprolong="<?=  url(['/station/prolong-registration', 'id' => 0]) ?>"
                 data-confirmation-delete-text="<?=  _tF('Do you really want to delete station', 'station') ?>"
                 data-confirmation-unpublish-text="<?=  _tF('Do you really want to unpublish station?', 'station') ?>"
                 data-yes="<?=  _tF('yes delete', 'station') ?>"
                 data-no="<?=  _tF('no delete', 'station') ?>">
            </div>
        </div>
    </section>
</div>


<?php
$this->registerJsVar('langIso', getLangShortCode());

if ($isByAdmin) {
    $this->registerJsVar('idUserForData', $model->id);
}
?>

