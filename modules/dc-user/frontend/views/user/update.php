<?php
/*
* Copyright 2023 DactylGroup s.r.o.
*
* Licensed under the EUPL, Version 1.2 or – as soon they
will be approved by the European Commission - subsequent
versions of the EUPL (the "Licence");
* You may not use this work except in compliance with the
Licence.
* You may obtain a copy of the Licence at:
*
* https://joinup.ec.europa.eu/software/page/eupl5
*
* Unless required by applicable law or agreed to in
writing, software distributed under the Licence is
distributed on an "AS IS" basis,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
express or implied.
* See the Licence for the specific language governing
permissions and limitations under the Licence.
*/
?>


<?php
// Uses
use frontend\assets\UserAsset;
use frontend\widgets\ActiveForm;
use modules\user\common\models\RegisterForm;

// Sources
/* @var $isCompany bool */
/* @var $this \frontend\components\FrontendView */
/* @var $formModel RegisterForm */

// Settings
$this->title = _tF('profile', 'register');
$title = $this->title;

UserAsset::register($this);
?>


<?php $form = ActiveForm::begin([
    'id' => 'update-form',
    'enableAjaxValidation' => false,
    'options' => [
        'class' => 'register-form',
        'data-pjax' => true,
        'data-pjax-replace-state' => false,
        'data-pjax-push-state' => false,

    ],
]); ?>

<div class="livebox-user">
    <div class="livebox-user__header">
    </div>
    <div class="livebox-user__content register-forms">

        <h4 class="livebox-user__content__title"><?= $title ?></h4>
        <div class="livebox-user__content__forms">
            <?= Yii::$app->controller->renderPartial('@modules/dc-user/frontend/views/user/register', [
                'isCompany' => ($formModel->getScenario() == $formModel::SCENARIO_UPDATE_COMPANY),
                'model' => $formModel,
                'form' => $form,
            ]) ?>
        </div>
    </div>
</div>

<?php ActiveForm::end(); ?>
