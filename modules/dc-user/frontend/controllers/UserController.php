<?php
/*
* Copyright 2023 DactylGroup s.r.o.
*
* Licensed under the EUPL, Version 1.2 or – as soon they
will be approved by the European Commission - subsequent
versions of the EUPL (the "Licence");
* You may not use this work except in compliance with the
Licence.
* You may obtain a copy of the Licence at:
*
* https://joinup.ec.europa.eu/software/page/eupl5
*
* Unless required by applicable law or agreed to in
writing, software distributed under the Licence is
distributed on an "AS IS" basis,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
express or implied.
* See the Licence for the specific language governing
permissions and limitations under the Licence.
*/
?>


<?php

namespace modules\user\frontend\controllers;

use common\models\StationExport;
use console\tasks\ApiTestingInfoMailJob;
use dactylcore\user\common\models\GdprAgreement;
use dactylcore\user\common\models\UserRole;
use dactylcore\user\frontend\models\LoginForm;
use modules\user\common\models\PasswordResetForm;
use modules\user\frontend\models\RegisterForm;
use modules\user\common\models\User;
use modules\user\frontend\models\ImportFileUploadForm;
use modules\user\frontend\models\PasswordResetRequestForm;
use Yii;
use yii\db\Exception;
use yii\helpers\ArrayHelper;
use yii\web\BadRequestHttpException;
use yii\web\Response;
use yii\web\UnauthorizedHttpException;
use yii\web\UploadedFile;

class UserController extends \dactylcore\user\frontend\controllers\UserController
{
    public function behaviors()
    {
        return ArrayHelper::merge(parent::behaviors(), [
            'access' => [
                'class' => \yii\filters\AccessControl::class,
                'rules' => [
                    [
                        'allow' => true,
                        'actions' => [
                            'select-user-type',
                            'register-company',
                            'index',
                            'user-index',
                            'delete',
                            'file-import',
                            'stations-export',
                            'stations-download-export',
                        ],
                    ],
                    [
                        'allow' => true,
                        'actions' => [
                            'user-dropdown-data-ids',
                        ],
                        'roles' => ['@'],
                    ],
                ],
            ],
        ]);
    }

    public function actionIndex()
    {
        $user = loggedUser();

        if (!$user) {
            return $this->redirect(url(['login']));
        }

        $this->view->showSubHeader = true;
        $this->view->subHeaderContent = $this->renderPartial('@frontend/views/site/partial/sub_header',
            ['showMyOnly' => false]);

        return $this->render('index', [
            'model' => $user,
        ]);
    }

    public function actionUserIndex($id)
    {
        $user = \dactylcore\user\common\models\User::findOne($id);

        if (!$user || !hasAccessTo('app-frontend_station_update-all')) {
            return $this->redirect(['/dc-user/user/index']);
        }

        $this->view->showSubHeader = true;
        $this->view->subHeaderContent = $this->renderPartial('@frontend/views/site/partial/sub_header', [
            'showMyOnly' => false,
            'byAdmin' => true,
        ]);

        return $this->render('index', [
            'model' => $user,
            'byAdmin' => true,
        ]);
    }

    public function actionLogin()
    {
        $model = new LoginForm();

        if (!Yii::$app->request->isAjax) {
            $this->view->showSubHeader = true;
            $this->view->subHeaderContent = $this->renderPartial('@frontend/views/site/partial/sub_header',
                ['showMyOnly' => true]);
            return $this->render('@frontend/views/site/index', [
                'liveboxUrl' => url(['/dc-user/user/login']),
            ]);
        }

        if ($model->load(Yii::$app->request->post())) {
            if ($model->login()) {
                setSuccessFlash(_tF('login_success_flash', 'user'));
                return $this->goHome();
            }
        }

        $model->password = "";

        return $this->render('login', [
            'model' => $model,
        ]);
    }

    public function actionForgottenPassword()
    {
        $model = new PasswordResetRequestForm();

        if (!Yii::$app->request->isAjax) {
            $this->view->showSubHeader = true;
            $this->view->subHeaderContent = $this->renderPartial('@frontend/views/site/partial/sub_header',
                ['showMyOnly' => true]);
            return $this->render('@frontend/views/site/index', [
                'liveboxUrl' => url(['/dc-user/user/forgotten-password']),
            ]);
        }

        if ($model->load(\Yii::$app->request->post())) {
            $errors = [];
            if ($model->validate() && $model->sendEmail($errors)) {
                $user = User::findOne(['id' => $model->getIdUser()]);

                if (!$user) {
                    throw new UnauthorizedHttpException(_tF('user_does_not_exist', 'reset_password_email'));
                }

                setSuccessFlash(_tF('forgotten_request_sent_success_flash', 'user'));
                return $this->render('forgotten_password_completed', [
                    'user' => $user,
                ]);
            }
        }

        return $this->render('forgotten_password', [
            'model' => $model,
        ]);
    }

    public function actionResendForgottenPasswordEmail(int $id)
    {
        $user = User::findOne(['id' => $id]);
        if (!$user) {
            throw new UnauthorizedHttpException(_tF('user_does_not_exist', 'reset_password_email'));
        }

        $model = new PasswordResetRequestForm();
        $model->email = $user->email;
        $errors = [];
        if (!$model->sendEmail($errors)) {
            setErrorFlash($this::getError($errors));
        } else {
            setSuccessFlash(_tF('forgotten_request_sent_success_flash', 'user'));
        }

        $user = User::findOne(['id' => $model->getIdUser()]);

        if (!$user) {
            throw new UnauthorizedHttpException(_tF('user_does_not_exist', 'reset_password_email'));
        }

        return $this->render('forgotten_password_completed', [
            'user' => $user,
        ]);
    }

    /**
     * Reset user's password.
     *
     * @param string $token verification token
     *
     * @return string|\yii\web\Response
     * @throws UnauthorizedHttpException
     */
    public function actionResetPassword(string $token)
    {
        $model = new PasswordResetForm();
        $model->token = $token;

        if (!$model->validateToken()) {
            throw new UnauthorizedHttpException(_t('Password reset token is not valid.'));
        }

        if (!Yii::$app->request->isAjax) {
            $this->view->showSubHeader = true;
            $this->view->subHeaderContent = $this->renderPartial('@frontend/views/site/partial/sub_header',
                ['showMyOnly' => true]);
            return $this->render('@frontend/views/site/index', [
                'liveboxUrl' => url(['/dc-user/user/reset-password', 'token' => $token]),
            ]);
        }

        if ($model->load(\Yii::$app->request->post()) && $model->validate()) {
            $errors = [];
            if ($model->resetPassword($errors)) {
                setSuccessFlash(_tF('password_reset_success_flash', 'reset_password_email'));
                return $this->redirect(['/dc-user/user/login']);
            } else {
                setErrorFlash(static::getError($errors));
            }
        }

        return $this->render('reset_password', [
            'model' => $model,
        ]);
    }

    public function actionRegister()
    {
        if (!Yii::$app->request->isAjax && !Yii::$app->request->isPost) {
            $this->view->showSubHeader = true;
            $this->view->subHeaderContent = $this->renderPartial('@frontend/views/site/partial/sub_header',
                ['showMyOnly' => true]);
            return $this->render('@frontend/views/site/index', [
                'liveboxUrl' => url(['/dc-user/user/register']),
            ]);
        }

        $registerForm = new RegisterForm();
        if (Yii::$app->request->post('RegisterForm') && isset(Yii::$app->request->post('RegisterForm')['type'])) {
            // Need to set scenario by type
            if (Yii::$app->request->post('RegisterForm')['type'] == User::USER_TYPE_COMPANY) {
                $registerForm->setScenario(RegisterForm::SCENARIO_REGISTER_COMPANY);
            } else {
                $registerForm->setScenario(RegisterForm::SCENARIO_REGISTER_USER);
            }

            if ($registerForm->load(Yii::$app->request->post()) && $registerForm->validate()) {
                // frontend registration for clients only
                $registerForm->role = UserRole::ROLE_CLIEND_ID;
                $user = new User();

                if ($user->register($registerForm)) {
                    if ($registerForm->conditionsConfirmed) {
                        GdprAgreement::createNew('registration', $registerForm->email,
                            \Yii::$app->request->getUserIP()
                        );
                    }
                    setSuccessFlash(_tF('successfully registered', 'user'));
                    return $this->redirect(['/dc-user/user/login']);
                }
            }
        }

        $registerForm->password = '';
        $registerForm->confirm_password = '';
        $registerForm->new_password = '';
        $registerForm->confirm_new_password = '';


        return $this->render('select-user-type', [
            'model' => $registerForm,
        ]);
    }

    public function actionUpdate($id = 0)
    {
        // Return to profile, if is not Ajax
        if (!Yii::$app->request->isAjax) {
            return $this->redirect(['/dc-user/user/index']);
        }

        $user = $id ? User::findOne(['id' => $id]) : Yii::$app->user->identity;

        if (!$user) {
            throw new Exception(_tF('user not found', 'error'));
        }


        $registerForm = new RegisterForm();
        $registerForm->setScenario($user->isCompany() ? RegisterForm::SCENARIO_UPDATE_COMPANY :
            RegisterForm::SCENARIO_UPDATE_USER);
        $registerForm->setAttributes($user->attributes);
        $registerForm->id_user = $user->id;

        if ($registerForm->load(Yii::$app->request->post()) && $registerForm->validate()) {
            $registerForm->email = $user->email;

            if (!Yii::$app->security->validatePassword($registerForm->old_password, $user->password_hash)) {
                $registerForm->addError('old_password', _tF('old password incorrect', 'user'));
            }

            // manual checking
            if ($registerForm->new_password && $registerForm->new_password != $registerForm->confirm_new_password) {
                $registerForm->addError('confirm_new_password', _tF('passwords_do_not_match', 'user'));
            }

            // We need save type
            $registerForm->type = $user->type;
            if (!$registerForm->errors && $user->register($registerForm)) {
                setSuccessFlash(_tF('flash_successfully_updated', 'user'));
                return $this->redirect(url(['index']));
            }
        }

        $registerForm->password = '';
        $registerForm->confirm_password = '';
        $registerForm->new_password = '';
        $registerForm->confirm_new_password = '';
        $registerForm->old_password = '';

        return $this->render('update', [
            'user' => $user,
            'formModel' => $registerForm,
            'isCompany' => $user->isCompany(),
        ]);
    }

    public function actionDelete()
    {
        if (isPost()) {
            /**
             * @var $model User
             */
            $model = Yii::$app->user->identity;

            $postData = Yii::$app->request->post();

            if (isset($postData['password'])) {
                $userPasswordHash = Yii::$app->user->identity->password_hash;
                if (Yii::$app->security->validatePassword($postData['password'], $userPasswordHash)) {
                    if ($model->delete()) {
                        Yii::$app->user->logout(false);
                        setSuccessFlash(_tF('flash_successfully_deleted', 'register'));
                        return $this->goHome();
                    }
                }
                setErrorFlash(_tF('flash_wrong_password', 'register'));
                return false;
            }
        }

        setErrorFlash(_tF('flash_error_deleting', 'register'));
        return false;
    }

    /**
     * Gets IDs of users formatted for Select2 dropdown.
     *
     * @param string|null $term searched term
     * @param int $page page number
     *
     * @return array
     */
    public function actionUserDropdownDataIds($term = null, $page = 1): array
    {
        \Yii::$app->response->format = Response::FORMAT_JSON;
        $limit = 20;
        $data = User::getDropdownDataIds($term, $page, $limit);

        return [
            'results' => $data,
            'pagination' => [
                'more' => count($data) == $limit,
            ],
        ];
    }

    /**
     * Import of the user XLS file with stations
     *
     * @param int $id - User ID
     *
     * @return string|Response  - redirect to profile or render livebox view
     * @throws Exception
     */
    public function actionFileImport($id = 0)
    {
        // Return to profile, if is not Ajax
        if (!Yii::$app->request->isAjax) {
            return $this->redirect(['/dc-user/user/index']);
        }

        $user = $id ? User::findOne(['id' => $id]) : Yii::$app->user->identity;

        if (!$user) {
            throw new Exception(_tF('user not found', 'error'));
        }

        $model = new ImportFileUploadForm();

        if (Yii::$app->request->isPost) {
            $model->importFile = UploadedFile::getInstance($model, 'importFile');
            if ($model->validate()) {
                try {
                    $result = $model->upload();
                    return $this->render('file-import', ['formModel' => $model, 'result' => $result]);
                } catch (\Exception $e) {
                    Yii::error($e, "Import-Exception");
                    return $this->redirect(['/dc-user/user/index']);
                }
            }
        }

        return $this->render('file-import', ['formModel' => $model, 'result' => null]);
    }

    /**
     * Export of all user stations to the XLS File
     *
     * @return string|Response
     * @throws Exception
     */
    public function actionStationsExport()
    {
        // Return to profile, if is not Ajax
        if (!Yii::$app->request->isAjax) {
            return $this->redirect(['/dc-user/user/index']);
        }

        // Only logged user can export his stations
        $user = Yii::$app->user->identity;
        if (!$user) {
            throw new Exception(_tF('user not found', 'error'));
        }

        if (Yii::$app->request->isPost) {
            try {
                // To allow aborting pjax request.
                // One session can be used by one request, so even if request is aborted, browser/server
                // is still waiting for unoccupied cache...
                session_write_close();
                $exporter = new StationExport();

                try {
                    $filePath = $exporter->userStationsExport($user->id);
                    $resultMsg = _tF('export_success_alert', 'user-export');
                    $result = "success";
                } catch (\PhpOffice\PhpSpreadsheet\Exception $exception) {
                    $resultMsg = _tF('export_fail_alert', 'user-export');
                    $result = "error";
                }

                return $this->render('file-export', ['tempFileName' => $filePath, 'result' => $resultMsg, 'resultType' => $result]);
            } catch (\Exception $e) {
                Yii::error($e, "Export-Exception");
                return $this->redirect(['/dc-user/user/index']);
            }
        }

        return $this->render('file-export', ['tempFileName' => null, 'result' => null]);
    }

    /**
     * Explicit function to download export file. Pjax cannot download file, so this is called without pjax.
     *
     * @param $user_id
     * @param $temp_name
     *
     * @return \yii\console\Response|Response
     * @throws BadRequestHttpException
     * @throws Exception
     */
    public function actionStationsDownloadExport($user_id, $temp_name)
    {
        $user = Yii::$app->user->identity;

        if (!$user || $user_id != $user->getId()) {
            throw new Exception(_tF('user not found', 'error'));
        }

        if (!file_exists($temp_name)) {
            throw new BadRequestHttpException(_tF('export_file_not_exists', 'user-export'));
        }

        // To delete file after download
        register_shutdown_function('unlink', $temp_name);

        // Rename file before download
        $fileName = _t('stations', 'user-export') . '_' . date('Y-m-d_His');
        return Yii::$app->response->sendFile($temp_name, "{$fileName}.xlsx");
    }

    /**
     * Verify user's email.
     *
     * @param string $token verification token
     *
     * @return \yii\web\Response
     */
    public function actionConfirmEmail(string $token)
    {
        /**
         * @var $user User
         */
        $user = \Yii::$app->user->identityClass::findOne(['email_verification_token' => $token]);

        if ($user) {
            $user->email_verification_token = null;
            $user->logEvent = User::LOG_EVENT_CONFIRM;

            if ($user->save()) {
                // Only if this id API testing, we want to inform user about API
                if (env("API_TESTING_ENVIRONMENT")) {
                    ApiTestingInfoMailJob::pushJob([
                        'userId' => $user->id,
                        'title' => _tF('email_register_title', 'api-testing'),
                        'content' => _tF('email_register_body', 'api-testing'),
                        'subject' => _tF('email_register_subject', 'api-testing'),
                    ]);

                    // inform admin about new api user
                    $user->notifyAdminAboutMeAsNewApiUser();
                }
                setSuccessFlash(_tF('email_verification_success_flash', 'email_verification_email'));
            } else {
                setErrorFlash(_tF('email_verification_save_error_flash', 'email_verification_email'));
            }
        } else {
            setErrorFlash(_tF('email_verification_error_flash', 'email_verification_email'));
        }

        return $this->redirect(['/']);
    }
}