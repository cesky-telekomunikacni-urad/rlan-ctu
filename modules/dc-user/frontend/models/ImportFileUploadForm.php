<?php
/*
* Copyright 2023 DactylGroup s.r.o.
*
* Licensed under the EUPL, Version 1.2 or – as soon they
will be approved by the European Commission - subsequent
versions of the EUPL (the "Licence");
* You may not use this work except in compliance with the
Licence.
* You may obtain a copy of the Licence at:
*
* https://joinup.ec.europa.eu/software/page/eupl5
*
* Unless required by applicable law or agreed to in
writing, software distributed under the Licence is
distributed on an "AS IS" basis,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
express or implied.
* See the Licence for the specific language governing
permissions and limitations under the Licence.
*/
?>


<?php

namespace modules\user\frontend\models;

use common\models\StationImport;
use dactylcore\core\base\Model;
use dactylcore\core\widgets\common\bootstrap\Html;
use frontend\widgets\dactylkit\DactylKit;
use yii\helpers\ArrayHelper;
use yii\web\UploadedFile;

/**
 * Class ImportFileUploadForm
 */
class ImportFileUploadForm extends Model
{
    /**
     * @var UploadedFile
     */
    public $importFile;

    public function rules()
    {
        return [
            [['importFile'], 'required', 'message' => _tF('importFile', 'user-import')],
            [['importFile'], 'file', 'skipOnEmpty' => false, 'extensions' => 'xlsx, xls'],
        ];
    }

    public function attributeLabels()
    {
        return ArrayHelper::merge([
            'importFile' => _tF('importFile', 'user-import'),
        ], parent::attributeLabels());
    }

    public function upload()
    {
        // To allow aborting pjax request.
        // One session can be used by one request, so even if request is aborted, browser/server
        // is still waiting for unoccupied cache...
        session_write_close();
        $importer = new StationImport();

        $trans = \Yii::$app->db->beginTransaction();
        try {
            $result = $importer->userImport($this->importFile->tempName);

            $trans->commit();
            return $result;
        } catch (\Throwable $e) {
            $trans->rollBack();
            \Yii::error($e->getMessage());
            throw new $e;
        }
    }
}
