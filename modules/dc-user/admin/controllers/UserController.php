<?php
/*
* Copyright 2023 DactylGroup s.r.o.
*
* Licensed under the EUPL, Version 1.2 or – as soon they
will be approved by the European Commission - subsequent
versions of the EUPL (the "Licence");
* You may not use this work except in compliance with the
Licence.
* You may obtain a copy of the Licence at:
*
* https://joinup.ec.europa.eu/software/page/eupl5
*
* Unless required by applicable law or agreed to in
writing, software distributed under the Licence is
distributed on an "AS IS" basis,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
express or implied.
* See the Licence for the specific language governing
permissions and limitations under the Licence.
*/
?>


<?php

namespace modules\user\admin\controllers;

use console\tasks\ExportAllUsersJob;
use dactylcore\core\db\ActiveRecord;
use dactylcore\log\common\components\LogArchiveComponent;
use dactylcore\user\common\models\RegisterForm;
use modules\user\common\models\search\UserSearch;
use yii\db\QueryInterface;
use yii\helpers\ArrayHelper;
use yii\web\NotFoundHttpException;
use yii\web\Response;
use dactylcore\user\admin\models\User;

class UserController extends \dactylcore\user\admin\controllers\UserController
{
    public function behaviors()
    {
        return ArrayHelper::merge(parent::behaviors(), [
            'access' => [
                'rules' => [
                    [
                        'allow' => true,
                        'actions' => [
                            'export-csv',
                            'export-xls',
                            'user-dropdown-data',
                            'user-dropdown-data2',
                        ],
                        'roles' => ['@'],
                    ],
                ],
            ],
        ]);
    }

    public function actionCreate($company = false)
    {
        /**
         * @var $form RegisterForm
         */
        $form = $this->getForm();

        $form->setScenario($company ? RegisterForm::SCENARIO_CREATE_COMPANY : RegisterForm::SCENARIO_CREATE_USER);


        if (\Yii::$app->request->isPost) {
            $form->type = $company ? User::USER_TYPE_COMPANY : User::USER_TYPE_USER;
            $form->logEvent = ActiveRecord::OPERATION_DEFAULT_CREATE;

            if ($this->save($form)) {

                return $this->redirectToPreviousIndex();
            }
        }

        return $this->render('create', [
            'model' => $form,
            'company' => $company,
        ]);
    }

    public function actionUpdate($id)
    {
        $form = $this->getForm($id);
        $user = $this->getModel($id);

        $form->setScenario($user->isCompany() ? RegisterForm::SCENARIO_UPDATE_COMPANY :
            RegisterForm::SCENARIO_UPDATE_USER);

        if (\Yii::$app->request->isPost) {

            if ($this->save($form)) {
                return $this->redirectToPreviousIndex();
            }
        }

        return $this->render('update', [
            'company' => $user->isCompany(),
            'model' => $form,
        ]);
    }

    protected function getForm($id = null)
    {
        if (is_null($id)) {
            $form = new RegisterForm();
            $form->setScenario(RegisterForm::SCENARIO_REGISTER);
            return $form;
        }

        $user = $this->getModel($id);

        if ($user) {
            $form = new RegisterForm();
            $form->setScenario($user->isCompany() ? RegisterForm::SCENARIO_UPDATE_COMPANY :
                RegisterForm::SCENARIO_UPDATE_USER);
            $form->setAttributes($user->attributes);
            $form->id_user = $user->id;
            return $form;
        } else {
            throw new NotFoundHttpException(_t('The requested page does not exist.'));
        }
    }

    /**
     * Gets folders formatted for Select2 dropdown.
     *
     * @param string|null $term searched term
     * @param int $page page number
     * @param int $limit result items limit
     *
     * @return array folders
     */
    public function actionUserDropdownData($term = null, $page = 1)
    {
        \Yii::$app->response->format = Response::FORMAT_JSON;
        $limit = 20;
        $data = User::getDropdownData($term, $page, $limit);
        return [
            'results' => $data,
            'pagination' => [
                'more' => count($data) == $limit,
            ],
        ];
    }

    /**
     * Gets folders formatted for Select2 dropdown.
     *
     * @param string|null $term searched term
     * @param int $page page number
     * @param int $limit result items limit
     *
     * @return array folders
     */
    public function actionUserDropdownData2($term = null, $page = 1)
    {
        \Yii::$app->response->format = Response::FORMAT_JSON;
        $limit = 20;
        $data = User::getDropdownData2($term, $page, $limit);
        return [
            'results' => $data,
            'pagination' => [
                'more' => count($data) == $limit,
            ],
        ];
    }

    /**
     * @return bool
     */
    public function actionExportXls()
    {
        $searchModel = new UserSearch();
        $dataProvider = $searchModel->search(\Yii::$app->request->queryParams);

        return $this->export(LogArchiveComponent::FILE_TYPE_XLSX, $dataProvider->query);
    }

    /**
     * @return bool
     */
    public function actionExportCsv()
    {
        $searchModel = new UserSearch();
        $dataProvider = $searchModel->search(\Yii::$app->request->queryParams);

        return $this->export(LogArchiveComponent::FILE_TYPE_CSV, $dataProvider->query);
    }

    /**
     * @param string $fileType
     * @param QueryInterface $query
     * @return bool
     */
    protected function export(string $fileType, QueryInterface $query): bool
    {
        if (!in_array($fileType, LogArchiveComponent::FILE_TYPES)) {
            setErrorFlash(_tF('export_wrong_type', 'log'));
            return false;
        }

        ExportAllUsersJob::pushJob([
            'email' => loggedUser()->email,
            'domain' => domain(),
            'fileType' => $fileType,
            'query' => $query,
        ]);
        setInfoFlash(_tF('export_request_info', 'log'));

        return true;
    }
}
