<?php
/*
* Copyright 2023 DactylGroup s.r.o.
*
* Licensed under the EUPL, Version 1.2 or – as soon they
will be approved by the European Commission - subsequent
versions of the EUPL (the "Licence");
* You may not use this work except in compliance with the
Licence.
* You may obtain a copy of the Licence at:
*
* https://joinup.ec.europa.eu/software/page/eupl5
*
* Unless required by applicable law or agreed to in
writing, software distributed under the Licence is
distributed on an "AS IS" basis,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
express or implied.
* See the Licence for the specific language governing
permissions and limitations under the Licence.
*/
?>


<?php
// Uses
use dactylcore\core\helpers\Url;
use dactylcore\core\widgets\admin\activeform\ActiveForm;
use dactylcore\core\widgets\admin\select2\Select2;
use dactylcore\core\widgets\common\radiolist\RadioList;
use dactylcore\user\common\models\UserRole;
use modules\user\common\models\RegisterForm;
use modules\user\common\models\User;

// Sources
/* @var $this dactylcore\core\web\AdminView */
/* @var $form ActiveForm */
/* @var $model RegisterForm */
?>

<?php $form = ActiveForm::begin([
    'id' => $formId,
]); ?>
    <div class="portlet">
        <div class="body">
            <?php if ($model->role != UserRole::ROLE_SUPERADMIN_ID) : ?>
                <?= $form->field($model, 'role')->widget(Select2::class, [
                    'data' => UserRole::getAvailableRoles(),
                ]); ?>
            <?php endif; ?>

            <?= $form->field($model, 'type')->widget(Select2::class, [
                'data' => User::getUserTypes(),
            ]) ?>

            <?= $form->field($model, 'email'); ?>

            <?php if ($company): ?>
                <div class="row">
                    <div class="col-sm-12">
                        <?= $form->field($model, 'companyName')->textInput(['placeholder' => _tF('companyName', 'register_placeholder')]) ?>
                    </div>
                </div>
            <?php else: ?>

                <div class="row">
                    <div class="col-lg-6">
                        <?= $form->field($model, 'first_name')->textInput(); ?>
                    </div>
                    <div class="col-lg-6">
                        <?= $form->field($model, 'last_name')->textInput(); ?>
                    </div>
                </div>
            <?php endif; ?>
            <?php if ($company): ?>
                <div class="row">
                    <div class="col-sm-6">
                        <?= $form->field($model, 'vatNumber')->textInput(['placeholder' => _tF('vatNumber', 'register_placeholder')]) ?>
                    </div>
                    <div class="col-sm-6">
                        <?= $form->field($model, 'taxNumber')->textInput(['placeholder' => _tF('taxNumber', 'register_placeholder')]) ?>
                    </div>
                </div>
            <?php endif; ?>
            <div class="row">
                <div class="col-sm-6">
                    <?= $form->field($model, 'city')->textInput(['placeholder' => _tF('city', 'register_placeholder')]) ?>
                </div>
                <div class="col-sm-6">
                    <?= $form->field($model, 'zipCode')->textInput(['placeholder' => _tF('zipCode', 'register_placeholder')]) ?>
                </div>
            </div>

            <div class="row">
                <div class="col-sm-12">
                    <?= $form->field($model, 'street')->textInput(['placeholder' => _tF('street', 'register_placeholder')]) ?>
                </div>
            </div>
            <?php if (!$model->isNewRecord) : ?>
                <div class="row">
                    <div class="col-lg-12">
                        <?= $form->field($model, 'old_password')->passwordInput(); ?>

                    </div>
                </div>
            <?php endif; ?>

            <div class="row">
                <div class="col-lg-6">
                    <?php if ($model->isNewRecord) : ?>
                        <?= $form->field($model, 'password')->passwordInput(); ?>
                    <?php else : ?>
                        <?= $form->field($model, 'new_password')->passwordInput(); ?>
                    <?php endif; ?>
                </div>
                <div class="col-lg-6">
                    <?php if ($model->isNewRecord) : ?>
                        <?= $form->field($model, 'confirm_password')->passwordInput(); ?>
                    <?php else : ?>
                        <?= $form->field($model, 'confirm_new_password')->passwordInput(); ?>
                    <?php endif; ?>
                </div>
            </div>
            <div class="row">
                <div class="col-lg-12">
                    <?= $form->field($model, 'can_use_api')->widget(RadioList::class, [
                        'type' => RadioList::TYPE_HORIZONTAL,
                    ]); ?>
                </div>
            </div>
        </div>
    </div>
    <div class="form-buttons">
        <?= $form->cancelButton(Url::getPreviousIndexUrl()); ?>
        <?= $form->activeSubmitButton($model); ?>
    </div>
<?php ActiveForm::end(); ?>