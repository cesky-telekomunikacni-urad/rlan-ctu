<?php
/*
* Copyright 2023 DactylGroup s.r.o.
*
* Licensed under the EUPL, Version 1.2 or – as soon they
will be approved by the European Commission - subsequent
versions of the EUPL (the "Licence");
* You may not use this work except in compliance with the
Licence.
* You may obtain a copy of the Licence at:
*
* https://joinup.ec.europa.eu/software/page/eupl5
*
* Unless required by applicable law or agreed to in
writing, software distributed under the Licence is
distributed on an "AS IS" basis,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
express or implied.
* See the Licence for the specific language governing
permissions and limitations under the Licence.
*/
?>


<?php
// Uses
use admin\assets\FloatHeaderAsset;
use dactylcore\core\helpers\Html;
use dactylcore\core\helpers\Url;
use dactylcore\core\widgets\admin\gridview\GridView;
use dactylcore\core\widgets\common\bootstrap\Html as BootHtml;
use dactylcore\user\common\models\UserRole;

// Sources
/* @var $this \dactylcore\core\web\AdminView */
/* @var $model \dactylcore\user\common\models\User */
/* @var $searchModel \dactylcore\user\common\models\search\UserSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

// Settings
$pageId = 'user';
$this->title = _tF('users', 'user');

// Breadcrumbs
$this->breadcrumbs[] = [
    'label' => _tF('users', 'user'),
    'url' => currentUrl(),
];

// Buttons
$this->buttons[] = Html::a(_tF('export_all', 'user'),
    array_merge([$pageId . '/export-xls'], Yii::$app->request->queryParams),
    'dc-user_user_export', [
        'class' => 'btn btn-secondary export-btn',
        'data-method' => 'post',
        'data-ajax' => '1',
        'data-pjax' => '0',
        'data-confirm' => _tF('export_all_confirm', 'log'),
        'data-base-url' => Url::to([$pageId . '/export-xls']),
    ]
);
$this->buttons[] = Html::a(_tF('export_all_csv', 'station'),
    array_merge([$pageId . '/export-csv'], Yii::$app->request->queryParams),
    'dc-user_user_export', [
        'class' => 'btn btn-secondary export-btn',
        'data-method' => 'post',
        'data-ajax' => '1',
        'data-pjax' => '0',
        'data-confirm' => _tF('export_all_confirm', 'log'),
        'data-base-url' => Url::to([$pageId . '/export-csv']),
    ]
);

$this->buttons[] = Html::a(_tF('create_user', 'user'), [$pageId . '/create'], 'dc-user_user_create', [
    'class' => 'btn btn-primary',
]);
$this->buttons[] = Html::a(_tF('create_company', 'user'), [$pageId . '/create', 'company' => 1], 'dc-user_user_create',
    [
        'class' => 'btn btn-primary',
    ]);

$enableEmailVerification = Yii::$app->getModule('dc-user')->enableEmailVerification;

?>

<?= $this->beginContainer(true, ['id' => $pageId . '-index']); ?>
    <div class="portlet">
        <div class="body">
            <?= GridView::widget([
                'id' => $pageId . '-grid',
                'dataProvider' => $dataProvider,
                'filterModel' => $searchModel,
                'responsive' => true,
                'responsiveWrap' => true,
                'floatHeader' => true,
                'floatHeaderOptions' => [
                    'scrollingTop' => '0',
                    'position' => 'absolute',
                    'top' => 56,
                ],
                'doubleScroll' => true,
                'columns' => [
                    [
                        'attribute' => 'id',
                        'headerOptions' => [
                            'style' => 'min-width: 100px;',
                        ],
                        'contentOptions' => [
                            'style' => 'min-width: 100px;',
                        ],
                    ],
                    [
                        'attribute' => 'name',
                        'value' => function ($model) {
                            return $model->getNameAndCompanyName();
                        },
                        'headerOptions' => [
                            'style' => 'min-width: 300px;',
                        ],
                        'contentOptions' => [
                            'style' => 'min-width: 300px;',
                        ],
                    ],
                    [
                        'attribute' => 'email',
                        'headerOptions' => [
                            'style' => 'width: 33.33%;',
                        ],
                        'contentOptions' => [
                            'style' => 'width: 33.33%;',
                        ],
                    ],
                    [
                        'attribute' => 'type',
                        'filter' => ['company' => _t('company', 'user'), 'user' => _t('user', 'user')],
                        'filterType' => GridView::FILTER_SELECT2,
                        'filterWidgetOptions' => [
                            'pluginOptions' => [
                                'allowClear' => true,
                                'placeholder' => '',
                            ],
                        ],
                        'value' => function ($model) {
                            $company = $model->isCompany();
                            $type = $company ? dactylcore\core\widgets\common\bootstrap\Html::TYPE_PRIMARY :
                                dactylcore\core\widgets\common\bootstrap\Html::TYPE_INFO;
                            return BootHtml::badge($company ? _t('company', 'user') : _t('user', 'user'), $type);
                        },
                    ],
                    [
                        'attribute' => 'role',
                        'value' => 'rolename',
                        'filter' => UserRole::getAvailableRoles(),
                        'filterType' => GridView::FILTER_SELECT2,
                        'filterWidgetOptions' => [
                            'pluginOptions' => [
                                'allowClear' => true,
                                'placeholder' => '',
                            ],
                        ],
                        'headerOptions' => [
                            'style' => 'width: 33.33%;',
                        ],
                        'contentOptions' => [
                            'style' => 'width: 33.33%;',
                        ],
                    ],
                    [
                        'visible' => $enableEmailVerification,
                        'attribute' => 'email_verification_token',
                        'format' => 'raw',
                        'value' => function ($model) {
                            $verified = $model->getEmailVerified();

                            return BootHtml::badge($verified ? _t('yes') : _t('no'), $verified);
                        },
                        'filter' => [_t('yes'), _t('no')],
                        'filterType' => GridView::FILTER_SELECT2,
                        'filterWidgetOptions' => [
                            'pluginOptions' => [
                                'allowClear' => true,
                                'placeholder' => '',
                            ],
                        ],
                        'headerOptions' => [
                            'style' => 'width: 33.33%;',
                        ],
                        'contentOptions' => [
                            'style' => 'width: 33.33%;',
                        ],
                    ],
                    [
                        'label' => _tF('stations', 'station'),
                        'value' => function ($model) {
                            $c = $model->getStations()->count();

                            $t = explode('|', _t('0_stations|1_station|2-4_stations|5+_stations', 'station'));
                            if ($c == 0) {
                                $t = $t[0] ?? '';
                            } elseif ($c == 1) {
                                $t = $t[1] ?? '';
                            } elseif ($c < 5) {
                                $t = $t[2] ?? '';
                            } else {
                                $t = $t[3] ?? '';
                            }
                            $t = explode('_', $t)[1] ?? '';

                            $link = Html::a(
                                "{$c} {$t}",
                                ['/station/index', 'StationSearch' => ['id_user' => $model->id]],
                                'app-admin_station_index',
                                [
                                    'data-pjax' => 0,
                                ]
                            );


                            return $c > 0 ? $link : '';
                        },
                        'headerOptions' => [
                            'style' => 'min-width: 300px;',
                        ],
                        'contentOptions' => [
                            'style' => 'min-width: 300px;',
                        ],
                    ],
                    [
                        'attribute' => 'can_use_api',
                        'format' => 'raw',
                        'value' => function ($model) {
                            return BootHtml::badge($model->can_use_api ? _t('yes') : _t('no'), $model->can_use_api);
                        },
                        'filter' => [1 => _t('yes'), 0 => _t('no')],
                        'filterType' => GridView::FILTER_SELECT2,
                        'filterWidgetOptions' => [
                            'pluginOptions' => [
                                'allowClear' => true,
                                'placeholder' => '',
                            ],
                        ],
                        'headerOptions' => [
                            'style' => 'width: 33.33%;',
                        ],
                        'contentOptions' => [
                            'style' => 'width: 33.33%;',
                        ],
                    ],
                    [
                        'class' => 'dactylcore\core\widgets\common\gridview\ActionColumn',
                        'template' => '{renew} {update} {delete}',
                        'contentOptions' => [
                            'style' => 'min-width: 165px;',
                        ],
                        'buttons' => [
                            'renew' => function ($url, $model, $_, $grid) use ($enableEmailVerification) {
                                if ($model->getEmailVerified() || !$enableEmailVerification) {
                                    return '';
                                }

                                $options = [
                                    'data' => [
                                        'toggle' => 'tooltip',
                                        'placement' => 'top',
                                        'ajax' => 1,
                                        'confirm' => _tF('resend_verification_confirm', 'user'),
                                    ],
                                    'title' => _tF('resend_verification', 'user'),
                                ];

                                return Html::a('<i class="material-icons">send</i> ',
                                    url(['/dc-user/user/send-verification', 'id' => $model->id]), 'dc-user_user_update',
                                    $options);
                            },
                            'delete' => function ($url, $model, $_, $grid) {
                                $options = [
                                    'data' => [
                                        'pjax' => 0,
                                        'ajax' => 1,
                                        'method' => 'post',
                                        'reload-pjax-containers' => [$grid->id],
                                        'confirm' => Yii::t('yii', 'Are you sure you want to delete this item?'),
                                        'title' => _tF('delete'),
                                        'toggle' => 'tooltip',
                                        'placement' => 'top',
                                    ],
                                ];

                                if ($model->role == UserRole::ROLE_SUPERADMIN_ID) {
                                    return '';
                                }

                                return Html::a('<i class="material-icons">delete</i> ',
                                    url(['/dc-user/user/delete', 'id' => $model->id]), 'dc-user_user_delete', $options);
                            },
                        ],
                    ],
                ],
            ]); ?>
        </div>
    </div>
<?= $this->endContainer(); ?>

<?php
$this->registerJs(<<<JS
    $(document).on('pjax:end',   function() {
        var buttons = $('.export-btn');
        buttons.each(function() {
            var button = $(this);
            button.attr('href', button.data('base-url') + window.location.search);
        });
    });
JS
);
