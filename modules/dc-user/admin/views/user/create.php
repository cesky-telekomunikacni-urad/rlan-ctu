<?php
/*
* Copyright 2023 DactylGroup s.r.o.
*
* Licensed under the EUPL, Version 1.2 or – as soon they
will be approved by the European Commission - subsequent
versions of the EUPL (the "Licence");
* You may not use this work except in compliance with the
Licence.
* You may obtain a copy of the Licence at:
*
* https://joinup.ec.europa.eu/software/page/eupl5
*
* Unless required by applicable law or agreed to in
writing, software distributed under the Licence is
distributed on an "AS IS" basis,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
express or implied.
* See the Licence for the specific language governing
permissions and limitations under the Licence.
*/
?>


<?php
// Uses
use dactylcore\core\helpers\Html;
use dactylcore\core\helpers\Url;

// Sources
/* @var $this dactylcore\core\web\AdminView */
/* @var $model dactylcore\user\common\models\RegisterForm */

// Settings
$pageId = 'user';
$formId = $pageId . '-form';
$this->title = _tF('create_user', 'user');

// Breadcrumbs
$this->breadcrumbs[] = [
    'label' => _tF('users', 'user'),
    'url' => ['index'],
];

$this->breadcrumbs[] = [
    'label' => $this->title,
    'url' => currentUrl(),
];

// Buttons
$this->buttons[] = Html::a(_tF('cancel'), Url::getPreviousIndexUrl(), 'dc-user_user_create', [
    'class' => 'btn btn-secondary',
]);

$this->buttons[] = Html::button(_tF('create'), 'dc-user_user_create', [
    'type' => 'submit',
    'form' => $formId,
    'class' => 'btn btn-primary',
]);
?>

<?= $this->beginContainer(false, ['id' => $pageId . '-create']); ?>
    <?= $this->render('partial/form', [
        'model' => $model,
        'pageId' => $pageId,
        'company' => $company,
        'formId' => $formId,
    ]); ?>
<?= $this->endContainer(); ?>