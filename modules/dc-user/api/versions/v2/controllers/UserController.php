<?php
/*
* Copyright 2023 DactylGroup s.r.o.
*
* Licensed under the EUPL, Version 1.2 or – as soon they
will be approved by the European Commission - subsequent
versions of the EUPL (the "Licence");
* You may not use this work except in compliance with the
Licence.
* You may obtain a copy of the Licence at:
*
* https://joinup.ec.europa.eu/software/page/eupl5
*
* Unless required by applicable law or agreed to in
writing, software distributed under the Licence is
distributed on an "AS IS" basis,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
express or implied.
* See the Licence for the specific language governing
permissions and limitations under the Licence.
*/
?>


<?php

namespace modules\user\api\versions\v2\controllers;

use admin\models\RandomStationGenerator;
use api\helpers\ApiHelper;
use dactylcore\core\helpers\Html;
use dactylcore\user\api\versions\v2\helpers\AbstractApiAuthManager;
use dactylcore\user\api\versions\v2\helpers\ApiAuthManager;
use dactylcore\user\api\versions\v2\models\UserRole;
use dactylcore\user\common\components\PasswordFormatValidator;
use dactylcore\user\common\models\GdprAgreement;
use modules\user\frontend\models\RegisterForm;
use Yii;
use yii\base\BaseObject;
use yii\db\Exception;
use yii\filters\AccessControl;
use yii\filters\VerbFilter;
use yii\helpers\ArrayHelper;
use yii\web\BadRequestHttpException;
use dactylcore\user\api\versions\v2\models\User;
use yii\web\ForbiddenHttpException;
use yii\web\ServerErrorHttpException;
use yii\web\UnauthorizedHttpException;

/**
 * {@inheritDoc}
 */
class UserController extends \dactylcore\user\api\versions\v2\controllers\UserController
{
    public $serializer = \api\versions\v1\Serializer::class;

    public function behaviors()
    {
        return ArrayHelper::merge([
            'access' => [
                'class' => AccessControl::class,
                'rules' => [
                    [
                        'actions' => [
                            'delete-account',
                        ],
                        'allow' => true,
                        'matchCallback' => function ()
                        {
                            return $this->checkAccessToken();
                        },
                    ],
                    [
                        'actions' => [
                            'register-as-individual',
                            'register-as-company',
                        ],
                        'allow' => true,
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::class,
                'actions' => [
                    'delete-account' => ['delete'],
                    'register-as-individual' => ['post'],
                    'register-as-company' => ['post'],
                ],
            ],
        ], parent::behaviors());
    }

    /**
     * @OA\Delete(path="/user/delete-account",
     *   summary="Deletes own account",
     *   tags={"User"},
     *
     *     @OA\Response(
     *         response=200,
     *         description="",
     *         @OA\JsonContent(
     *             @OA\Property(
     *                 property="status",
     *                 title="Response status code",
     *                 type="integer",
     *             ),
     *         ),
     *     ),
     *     security={
     *         {"access-token":{}}
     *     },
     * )
     *
     */
    public function actionDeleteAccount()
    {
        $model = User::findOne(loggedUser()->id);

        \Yii::$app->user->logout();

        if (!$model->delete()) {
            throw new ServerErrorHttpException(
                $model->hasErrors() ?
                    $model->getFirstErrorMessage()
                    :
                    'Failed to delete the object for unknown reason.');
        }

        return [];
    }

    /**
     * @OA\Post(
     *     path="/user/register-as-individual",
     *     summary="Register as individual",
     *     tags={"User"},
     *     @OA\RequestBody(
     *          @OA\MediaType(
     *              mediaType="application/json",
     *              @OA\Schema(
     *                  ref="#/components/schemas/registerAsIndividualSchema"
     *              )
     *          )
     *     ),
     *
     *     @OA\Response(response=200,description="data for user",@OA\JsonContent(
     *           @OA\Property(property="status", title="Response status code", type="integer"),
     *           @OA\Property(property="data", title="Response data", ref="#/components/schemas/userStatusData"),
     *       ),
     *     ),
     *     @OA\Response(response=422, ref="#/components/responses/DataValidationFailed"),
     * )
     *
     * @return User
     * @throws BadRequestHttpException
     * @throws Exception
     * @throws \ReflectionException
     * @throws \yii\base\InvalidConfigException
     */
    public function actionRegisterAsIndividual()
    {
        return $this->registerAs(User::USER_TYPE_USER);
    }

    /**
     * @OA\Post(
     *     path="/user/register-as-company",
     *     summary="Register as company",
     *     tags={"User"},
     *     @OA\RequestBody(
     *          @OA\MediaType(
     *              mediaType="application/json",
     *              @OA\Schema(
     *                  ref="#/components/schemas/registerAsCompanySchema"
     *              )
     *          )
     *     ),
     *
     *     @OA\Response(response=200,description="data for user",@OA\JsonContent(
     *           @OA\Property(property="status", title="Response status code", type="integer"),
     *           @OA\Property(property="data", title="Response data", ref="#/components/schemas/userStatusData"),
     *       ),
     *     ),
     *     @OA\Response(response=422, ref="#/components/responses/DataValidationFailed"),
     * )
     *
     * @return User
     * @throws BadRequestHttpException
     * @throws Exception
     * @throws \ReflectionException
     * @throws \yii\base\InvalidConfigException
     */
    public function actionRegisterAsCompany()
    {
        return $this->registerAs(User::USER_TYPE_COMPANY);
    }

    protected function registerAs($type)
    {
        $post = Yii::$app->request->post();

        // Force user type
        $post['type'] = $type;

        // Prepare register form
        $registerForm = new RegisterForm();
        $registerForm->enableCaptchaValidation = false;
        $registerForm->setScenario("register_{$type}");
        $registerForm->setAttributes($post);
        $registerForm->role = UserRole::ROLE_CLIEND_ID;
        $registerForm->type = $post['type'];

        // Validate register form
        if ($registerForm->validate()) {
            $authManager = new ApiAuthManager();

            // Register user
            if (!static::$user = $authManager->register($post)) {
                throw $authManager->getFirstException();
            }

            GdprAgreement::createNew('registration', static::$user->email,
                \Yii::$app->request->getUserIP()
            );

            return static::$user;
        }

        return $registerForm;
    }

    /** @OA\Post(path="/user/login-with-facebook", tags={"Removed endpoints"}, deprecated= true, @OA\Response(response=200, description="",),) */
    /** @OA\Post(path="/user/login-with-google-plus", tags={"Removed endpoints"}, deprecated= true, @OA\Response(response=200, description="",),) */
    /** @OA\Post(path="/user/login-with-apple", tags={"Removed endpoints"}, deprecated= true, @OA\Response(response=200, description="",),) */
    /** @OA\Post(path="/user/device", tags={"Removed endpoints"}, deprecated= true, @OA\Response(response=200, description="",),) */
    /** @OA\Post(path="/user/register", tags={"Removed endpoints"}, deprecated= true, @OA\Response(response=200, description="",),) */
    /** @OA\Get(path="/user/send-email-verification", tags={"Removed endpoints"}, deprecated= true, @OA\Response(response=200, description="",),) */
    /** @OA\Post(path="/user/resend-verification-email", tags={"Removed endpoints"}, deprecated= true, @OA\Response(response=200, description="",),) */
    /** @OA\Post(path="/user/update", tags={"Removed endpoints"}, deprecated= true, @OA\Response(response=200, description="",),) */
    /** @OA\Post(path="/user/email-exists", tags={"Removed endpoints"}, deprecated= true, @OA\Response(response=200, description="",),) */


    /**
     * We need check access according to explicit allowance
     *
     * @return bool
     * @throws UnauthorizedHttpException
     * @throws \yii\web\HttpException
     */
    public function checkAccessToken()
    {
        if (parent::checkAccessToken()) {
            if (!static::$user->canUseApi()) {
                throw new UnauthorizedHttpException(\Yii::t('yii', 'You are not allowed to perform this action.'));
            }
            return true;
        }
        return false;
    }

    /**
     *
     * @OA\Post(
     *     path="/user/change-password",
     *     summary="Change user's password",
     *     tags={"User"},
     *     security={
     *         {"access-token":{}}
     *     },
     *     @OA\RequestBody(
     *          @OA\MediaType(
     *              mediaType="application/json",
     *              @OA\Schema(
     *                  type="object",
     *                  description="",
     *                  @OA\Property(property="password_old", type="string"),
     *                  @OA\Property(property="password_new", type="string"),
     *              )
     *          )
     *     ),
     *     @OA\Response(
     *       response=200,
     *       description="data for user",
     *       @OA\JsonContent(
     *           @OA\Property(property="status", title="Response status code", type="integer"),
     *           @OA\Property(property="data", title="Response data", ref="#/components/schemas/userStatusData"),
     *       ),
     *     ),
     * )
     */
    public function actionChangePassword()
    {
        $post = $this->module->request->post();

        if (!static::checkRequiredPostData(['password_old', 'password_new'], $error)) {
            throw new BadRequestHttpException(static::getError($error));
        }

        if (empty(static::$user->password_hash)) {
            throw new ForbiddenHttpException('User is registered with social network app');
        }

        if (!static::$user->validatePassword($post['password_old'])) {
            throw new ForbiddenHttpException('Password is not correct.');
        }

        $regForm = new \dactylcore\user\common\models\RegisterForm();
        $regForm->new_password = $post['password_new'];
        if (!$regForm->validate(['new_password'])) {
            throw new BadRequestHttpException(ApiHelper::simplifyModelErrors($regForm));
        }

        $transaction = \Yii::$app->db->beginTransaction();

        static::$user->resetAccessTokens();
        static::$user->accessToken = static::$user->generateAccessToken();

        static::$user->setPassword($post['password_new']);

        if (static::$user->save()) {
            $transaction->commit();
        } else {
            throw new BadRequestHttpException(static::$user->getFirstErrorMessage());
        }

        return static::$user;
    }

    /**
     *
     * @OA\Post(
     *     path="/user/logout",
     *     summary="Log user out",
     *     tags={"User"},
     *
     *     @OA\Response(
     *       response=200,
     *       description="OK",
     *       @OA\JsonContent(
     *           @OA\Property(property="status", title="Response status code", type="integer"),
     *           @OA\Property(property="data", title="Response data", type="object"),
     *       ),
     *     ),
     *     @OA\Response(response=401, ref="#/components/responses/Unauthorized"),
     *     security={
     *         {"access-token":{}}
     *     },
     * )
     *
     */

    /**
     *
     * @OA\Post(
     *     path="/user/login",
     *     summary="Log user in",
     *     tags={"User"},
     *     @OA\RequestBody(
     *          @OA\MediaType(
     *              mediaType="application/json",
     *              @OA\Schema(
     *                  ref="#/components/schemas/loginSchema"
     *              )
     *          )
     *     ),
     *     @OA\Response(
     *       response=200,
     *       description="data for user",
     *       @OA\JsonContent(
     *           @OA\Property(property="status", title="Response status code", type="integer"),
     *           @OA\Property(property="data", title="Response data", ref="#/components/schemas/userStatusData"),
     *       ),
     *     ),
     *     @OA\Response(
     *       response=400,
     *       description="missing parameters",
     *       @OA\JsonContent(
     *           @OA\Property(property="status", title="Response status code", type="integer"),
     *           @OA\Property(property="error", title="Error message", type="string"),
     *       ),
     *     ),
     *     @OA\Response(
     *       response=401,
     *       description="bad data",
     *       @OA\JsonContent(
     *           @OA\Property(property="status", title="Response status code", type="integer"),
     *           @OA\Property(property="error", title="Error message", type="string"),
     *       ),
     *     ),
     * )
     *
     */

    /**
     *
     * @OA\Patch(
     *     path="/user/update",
     *     summary="Update user info",
     *     tags={"User"},
     *     security={
     *         {"access-token":{}}
     *     },
     *     @OA\RequestBody(
     *          @OA\MediaType(
     *              mediaType="application/json",
     *              @OA\Schema(
     *                  ref="#/components/schemas/userUpdateSchema"
     *              )
     *          )
     *     ),
     *     @OA\Response(
     *       response=200,
     *       description="data for user",
     *       @OA\JsonContent(
     *           @OA\Property(property="status", title="Response status code", type="integer"),
     *           @OA\Property(property="data", title="Response data", ref="#/components/schemas/userStatusData"),     *
     *           ),
     *     ),
     * )
     *
     * @return User
     * @throws BadRequestHttpException
     * @throws Exception
     * @throws \ReflectionException
     * @throws \yii\base\InvalidConfigException
     */
    public function actionUpdateUser()
    {
        $data = $this->retypeToClassAttrs($this->getRequestParams());

        static::$user->setScenario(static::$user::SCENARIO_REST_UPDATE);

        $transaction = \Yii::$app->db->beginTransaction();

        static::$user->setAttributes($data);

        if (static::$user->save()) {

            $transaction->commit();
            return static::$user;
        } else {
            throw new BadRequestHttpException(static::$user->getFirstErrorMessage());
        }
    }

    /**
     *
     * @OA\Get(
     *     path="/user/status",
     *     summary="Get user status",
     *     tags={"User"},
     *     @OA\Response(
     *       response=200,
     *       description="data for user",
     *       @OA\JsonContent(
     *           @OA\Property(property="status", title="Response status code", type="integer"),
     *           @OA\Property(property="data", title="Response data", ref="#/components/schemas/userStatusData"),
     *       ),
     *     ),
     *     security={
     *         {"access-token":{}}
     *     },
     * )
     *
     */

    /**
     *
     * @OA\Post(
     *     path="/user/request-password-reset",
     *     summary="Sends email to reset password",
     *     tags={"User"},
     *     @OA\RequestBody(
     *          @OA\MediaType(
     *              mediaType="application/json",
     *              @OA\Schema(
     *                  type="object",
     *                  description="user's email",
     *                  required={"email"},
     *                  @OA\Property(property="email", type="string"),
     *              )
     *          )
     *     ),
     *     @OA\Response(
     *       response=200,
     *       description="OK",
     *       @OA\JsonContent(
     *           @OA\Property(property="status", title="Response status code", type="integer"),
     *           @OA\Property(property="data", title="Response data", type="object"),
     *       ),
     *     ),
     *
     *     @OA\Response(
     *       response=400,
     *       description="Password reset error",
     *       @OA\JsonContent(
     *           @OA\Property(property="status", title="Response status code", type="integer", example="400"),
     *           @OA\Property(property="error", title="Error message", type="string", example="Password reset error"),
     *       ),
     *     ),
     * )
     *
     * @return array
     * @throws BadRequestHttpException
     */
    public function actionRequestPasswordReset()
    {
        $post = $this->module->request->post();

        if (!static::checkRequiredPostData(['email'], $error)) {
            throw new BadRequestHttpException(static::getError($error));
        }

        $email = Html::encode($post['email']);
        $errors = [];

        $user = \Yii::$app->user->identityClass::findOne([
            'status' => User::STATUS_ACTIVE,
            'email' => $email,
        ]);
        /* @var $user User */

        if ($user) {
            $pass = Yii::$app->security->generateRandomString(2) . 'a-A1' . Yii::$app->security->generateRandomString(2);
            $user->setPassword($pass);

            if ($user->save()) {
                $sent = \Yii::$app->mailer
                    ->compose('@common/mail/password_reset_token',
                        ['password' => $pass])
                    ->setFrom(extractMailFromMailname(c('SMTP_FROM')))
                    ->setTo($email)
                    ->setSubject(
                        _tF('subject', 'reset_password_email')
                    )
                    ->send();

                if ($sent) {
                    return [];
                }
            }
        }

        throw new BadRequestHttpException('Password reset error');
    }
}

