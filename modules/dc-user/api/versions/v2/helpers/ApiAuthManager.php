<?php
/*
* Copyright 2023 DactylGroup s.r.o.
*
* Licensed under the EUPL, Version 1.2 or – as soon they
will be approved by the European Commission - subsequent
versions of the EUPL (the "Licence");
* You may not use this work except in compliance with the
Licence.
* You may obtain a copy of the Licence at:
*
* https://joinup.ec.europa.eu/software/page/eupl5
*
* Unless required by applicable law or agreed to in
writing, software distributed under the Licence is
distributed on an "AS IS" basis,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
express or implied.
* See the Licence for the specific language governing
permissions and limitations under the Licence.
*/
?>


<?php

namespace modules\user\api\versions\v2\helpers;

use dactylcore\user\api\versions\v2\helpers\AbstractApiAuthManager;
use dactylcore\user\common\models\User;
use yii\web\BadRequestHttpException;
use yii\web\IdentityInterface;

class ApiAuthManager extends AbstractApiAuthManager
{
    /**
     * @inheritDoc
     */
    public function register(array $userData, array $device = []): ?IdentityInterface
    {
        /**
         * @var IdentityInterface $user
         */
        $user = new $this->identityClass();
        /* @var $user \dactylcore\user\api\versions\v2\models\User */

        $user->setScenario($user::SCENARIO_REST_REGISTER);
        $user->generateAuthKey();
        $user->role = $user::ROLE_USER;
        $user->type = $userData['type'] ?? null;
        $user->setAttributes($userData);

        $transaction = \Yii::$app->db->beginTransaction();

        try {
            if ($user->save()) {
                $accessToken = $user->generateAccessToken();
                $user->accessToken = $accessToken;

                \Yii::$app->user->setIdentity($user);

                if ($this->saveDevice($device, $user->id)) {
                    $transaction->commit();
                    return $user;
                }
            } else {
                $this->addException(new BadRequestHttpException($user->getFirstErrorMessage()));
                $transaction->rollBack();
            }
        } catch (\Exception $e) {
            $this->addException($e);
            $transaction->rollBack();
        }

        return null;
    }
}