<?php
/*
* Copyright 2023 DactylGroup s.r.o.
*
* Licensed under the EUPL, Version 1.2 or – as soon they
will be approved by the European Commission - subsequent
versions of the EUPL (the "Licence");
* You may not use this work except in compliance with the
Licence.
* You may obtain a copy of the Licence at:
*
* https://joinup.ec.europa.eu/software/page/eupl5
*
* Unless required by applicable law or agreed to in
writing, software distributed under the Licence is
distributed on an "AS IS" basis,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
express or implied.
* See the Licence for the specific language governing
permissions and limitations under the Licence.
*/
?>


<?php

namespace modules\user\api\versions\v2\models;

use yii\db\ActiveQuery;
use yii\helpers\ArrayHelper;
use api\versions\v1\models\UserAlliance;


/**
 * @OA\Schema(
 *     schema="userBaseAttrs",
 *     description="Data",
 *     type="object",

 *     @OA\Property(property="street", type="string"),
 *     @OA\Property(property="city", type="string"),
 *     @OA\Property(property="zipCode", type="string"),
 *     @OA\Property(property="email", type="string"),
 * )
 */


/**
 * @OA\Schema(
 *     schema="registerBaseSchema",
 *     description="Data for register",
 *     type="object",
 *     required={"email", "password", "conditionsConfirmed"},
 *
 *     allOf = {
 *      @OA\Schema(ref = "#/components/schemas/userBaseAttrs"),
 *     },
 *     @OA\Property(property="password", type="string"),
 *     @OA\Property(property="conditionsConfirmed", type="boolean"),
 * )
 */


/**
 * @OA\Schema(
 *     schema="registerAsIndividualSchema",
 *     description="Data for register User",
 *     type="object",
 *     required={"first_name", "last_name"},
 *
 *     @OA\Property(property="first_name", type="string"),
 *     @OA\Property(property="last_name", type="string"),
 *     allOf = {
 *      @OA\Schema(ref = "#/components/schemas/registerBaseSchema"),
 *     },
 * )
 */

/**
 * @OA\Schema(
 *     schema="registerAsCompanySchema",
 *     description="Data for register Company",
 *     type="object",
 *     required={"companyName"},
 *
 *     @OA\Property(property="companyName", type="string"),
 *     @OA\Property(property="vatNumber", type="string"),
 *     @OA\Property(property="taxNumber", type="string"),
 *     allOf = {
 *      @OA\Schema(ref = "#/components/schemas/registerBaseSchema"),
 *     },
 * )
 */

/**
 * @OA\Schema(
 *     schema="userStatusData",
 *     description="user data",
 *     type="object",
 *
 *     @OA\Property(property="id", type="integer"),
 *     @OA\Property(property="type", type="string", enum={"user", "company"}),
 *     allOf = {
 *      @OA\Schema(ref = "#/components/schemas/userBaseAttrs"),
 *     },
 *     @OA\Property(property="companyName", type="string"),
 *     @OA\Property(property="vatNumber", type="string"),
 *     @OA\Property(property="taxNumber", type="string"),
 *     @OA\Property(property="first_name", type="string"),
 *     @OA\Property(property="last_name", type="string"),
 *     @OA\Property(property="access_token", ref="#/components/schemas/accessTokenData"),
 * )
 */

/**
 * @OA\Schema(
 *     schema="loginSchema",
 *     description="data for login User",
 *     type="object",
 *     required={"email", "password"},
 *     @OA\Property(property="email", type="string"),
 *     @OA\Property(property="password", type="string"),
 * )
 */

/** @OA\Schema(
 *     schema="userUpdateSchema",
 *     type="object",
 *     description="data for update User",
 *
 *     @OA\Property(property="companyName", type="string"),
 *     @OA\Property(property="vatNumber", type="string"),
 *     @OA\Property(property="taxNumber", type="string"),
 *     @OA\Property(property="first_name", type="string"),
 *     @OA\Property(property="last_name", type="string"),
 *     @OA\Property(property="email", type="string"),
 *     @OA\Property(property="street", type="string"),
 *     @OA\Property(property="city", type="string"),
 *     @OA\Property(property="zipCode", type="string"),
 * )
 */

/**
 * {@inheritDoc}
 */
class User extends \dactylcore\user\api\versions\v2\models\AbstractUser
{
    public function fields()
    {
        $route = \Yii::$app->controller->id . '/' . \Yii::$app->controller->action->id;

        $fields = array_merge(parent::fields(), [
            'type',
            'street',
            'city',
            'zipCode',
            'companyName',
            'vatNumber',
            'taxNumber',
        ]);

        return $fields;
    }

    public function scenarios()
    {
        $attrs = [
            'email',
            'role',
            'password',
            'avatar',
            'city',
            'zipCode',
            'street',
            'type',
        ];

        if ($this->type == $this::USER_TYPE_USER) {
            $attrs = ArrayHelper::merge($attrs, static::getApiRegNameAttrs());
        } else {
            $attrs = ArrayHelper::merge($attrs, [
                'companyName',
                'vatNumber',
                'taxNumber',
            ]);
        }

        return array_merge(parent::scenarios(), [
            self::SCENARIO_REST_REGISTER => $attrs,
            self::SCENARIO_REST_UPDATE => $attrs,
        ]);
    }

    /**
     * @return ActiveQuery
     * @throws \yii\base\InvalidConfigException
     */
    public function getAlliance()
    {
        return $this->hasOne(UserAlliance::class, ['id' => 'id_user_alliance'])
                    ->viaTable('user_alliance_connector', ['id_user' => 'id'])
                    ->innerJoin('user_alliance_connector',
                        'user_alliance_connector.id_user_alliance = user_alliance.id')
                    ->andWhere([
                        'OR',
                        ['user_alliance_connector.status' => UserAlliance::STATUS_MEMBER],
                        ['user_alliance_connector.status' => UserAlliance::STATUS_ADMIN],
                    ])
                    ->andWhere(['user_alliance_connector.id_user' => $this->id]);
    }
}
