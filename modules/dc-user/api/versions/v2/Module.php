<?php
/*
* Copyright 2023 DactylGroup s.r.o.
*
* Licensed under the EUPL, Version 1.2 or – as soon they
will be approved by the European Commission - subsequent
versions of the EUPL (the "Licence");
* You may not use this work except in compliance with the
Licence.
* You may obtain a copy of the Licence at:
*
* https://joinup.ec.europa.eu/software/page/eupl5
*
* Unless required by applicable law or agreed to in
writing, software distributed under the Licence is
distributed on an "AS IS" basis,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
express or implied.
* See the Licence for the specific language governing
permissions and limitations under the Licence.
*/
?>


<?php

namespace modules\user\api\versions\v2;

use dactylcore\core\web\RestUrlRule;

class Module extends \dactylcore\user\api\versions\v2\Module
{
    protected function addUrlRules()
    {
        parent::addUrlRules();

        $rules = [
            'user' => [
                'class' => RestUrlRule::class,
                'extraPatterns' => [
                    'DELETE delete-account'=>'delete-account',
                    'POST register-as-individual'=>'register-as-individual',
                    'POST register-as-company'=>'register-as-company'
                ],
            ],
        ];

        $this->addUrlManagerRules($rules);
    }
}
