<?php
/*
* Copyright 2023 DactylGroup s.r.o.
*
* Licensed under the EUPL, Version 1.2 or – as soon they
will be approved by the European Commission - subsequent
versions of the EUPL (the "Licence");
* You may not use this work except in compliance with the
Licence.
* You may obtain a copy of the Licence at:
*
* https://joinup.ec.europa.eu/software/page/eupl5
*
* Unless required by applicable law or agreed to in
writing, software distributed under the Licence is
distributed on an "AS IS" basis,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
express or implied.
* See the Licence for the specific language governing
permissions and limitations under the Licence.
*/
?>


<?php

namespace modules\user\api\versions\v2\tests\api;

use Codeception\Util\HttpCode;
use modules\user\api\versions\v2\tests\api\ApiTester;
use modules\user\common\models\User;

class UserCest
{
    protected $time;
    protected static $accessToken = null;
    protected static $data1
        = [
            'first_name' => 'Jan',
            'last_name' => 'Novak',
            'street' => 'Mala 18',
            'city' => 'Brno',
            'zipCode' => '63500',
            'email' => 'tester2@dactylgroup.com',
            'password' => 'MirkoAneta7777?',
            'conditionsConfirmed' => true,
        ];
    protected static $data2
        = [
            'companyName' => 'Moja firma',
            'street' => 'Mala 18',
            'city' => 'Brno',
            'zipCode' => '63500',
            'email' => 'tester3@dactylgroup.com',
            'password' => 'MirkoAneta7777?',
            'conditionsConfirmed' => true,
        ];

    /**
     * @param ApiTester $I
     */
    public function _before($I)
    {
        $this->time = time();
    }

    /**
     * @param ApiTester $I
     */
    public function registerAsIndividual($I)
    {
        $I->sendPOST('/user/register-as-individual', self::$data1);
        $I->seeResponseContainsJson([
            'status' => '200',
        ]);
    }

    /**
     * @param ApiTester $I
     */
    public function registerAsCompany($I)
    {
        $I->sendPOST('/user/register-as-company', self::$data2);
        $I->seeResponseContainsJson([
            'status' => '200',
        ]);
    }

    /**
     * @param ApiTester $I
     */
    public function login($I)
    {
        $I->sendPOST('/user/login', [
            'email' => self::$data1['email'],
            'password' => self::$data1['password'],
        ]);
        $I->seeResponseContainsJson([
            'status' => '403',
        ]);

        \Yii::$app->db->createCommand()
                      ->update('user', [
                          'email_verification_token' => null,
                          'can_use_api' => true,
                      ], ['email' => self::$data1['email']])
                      ->execute();

        $I->sendPOST('/user/login', [
            'email' => self::$data1['email'],
            'password' => self::$data1['password'],
        ]);
        $I->seeResponseContainsJson([
            'status' => '200',
        ]);
    }

    /**
     * @param ApiTester $I
     */
    public function changePassword($I)
    {
        $this->haveAccessTokenInHeader($I);

        $I->sendPOST('/user/change-password', [
            'password_old' => self::$data1['password'],
            'password_new' => self::$data1['password'] . '_new',
        ]);
        $I->seeResponseContainsJson([
            'status' => '200',
        ]);

        $I->sendPOST('/user/login', [
            'email' => self::$data1['email'],
            'password' => self::$data1['password'] . '_new',
        ]);
        $I->seeResponseContainsJson([
            'status' => '200',
        ]);
    }

    /**
     * @param ApiTester $I
     */
    public function deleteAccount($I)
    {
        $this->haveAccessTokenInHeader($I, self::$data2);

        $I->sendDELETE('/user/delete-account');
        $I->seeResponseContainsJson([
            'status' => '200',
        ]);
    }

    // #############################################

    /**
     * @param ApiTester $I
     */
    protected function haveAccessTokenInHeader($I, $userLoginData = null)
    {
        if ($userLoginData) {
            self::$accessToken = null;
        }

        if (!self::$accessToken) {
            if (!$userLoginData) {
                $userLoginData = self::$data1;
            }

            \Yii::$app->db->createCommand()
                          ->update('user', [
                              'email_verification_token' => null,
                              'can_use_api' => true,
                          ], ['email' => $userLoginData['email']])
                          ->execute();
            $I->sendPOST('/user/login', [
                'email' => $userLoginData['email'],
                'password' => $userLoginData['password'],
            ]);
            $response = json_decode($I->grabResponse());
            self::$accessToken = $response->data->access_token->token;
        }

        $I->haveHttpHeader('access-token', self::$accessToken);
    }

    /**
     * @param ApiTester $I
     */
    protected function ensureTestUser($I)
    {
        if (!User::findOne(['email' => self::$data1['email']])) {
            $I->haveRecord(User::class, merge(self::$data1, [
                'password_hash' => '$2y$13$Fz6j5xAFigUg93.CDZiFLuhvkJ0Ar73pxXsJhv7cMf7cB4dZuKMY2',
                'auth_key' => 'nwifFVoE1OEzkYLJdo3fhwp2ETEM6Wxa',
                'role' => 3,
                'created_at' => time(),
                'updated_at' => time(),
            ]));
        }
    }

    protected function getNameAttributes(string $firstName, string $lastName): array
    {
        $parent = parent::getNameAttributes($firstName, $lastName);

        return array_merge($parent, [
            'nickname' => 'testuser77',
        ]);
    }
}