<?php
/*
* Copyright 2023 DactylGroup s.r.o.
*
* Licensed under the EUPL, Version 1.2 or – as soon they
will be approved by the European Commission - subsequent
versions of the EUPL (the "Licence");
* You may not use this work except in compliance with the
Licence.
* You may obtain a copy of the Licence at:
*
* https://joinup.ec.europa.eu/software/page/eupl5
*
* Unless required by applicable law or agreed to in
writing, software distributed under the Licence is
distributed on an "AS IS" basis,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
express or implied.
* See the Licence for the specific language governing
permissions and limitations under the Licence.
*/
?>


<?php

namespace modules\user\common\models;

use dactylcore\core\widgets\common\bootstrap\Html;
use dactylcore\user\common\components\PasswordFormatValidator;
use dactylcore\user\common\models\User;
use himiklab\yii2\recaptcha\ReCaptchaValidator;
use yii\helpers\ArrayHelper;

class RegisterForm extends \dactylcore\user\common\models\AbstractRegisterForm
{
    const STATE_SUCCESSFUL = 0;
    const STATE_SAVE_ERROR = 1;
    const STATE_EMAIL_ERROR = 2;
    //
    public $companyName;
    public $vatNumber;
    public $taxNumber;
    // common extra fields (regarding the dc-user)
    public $city;
    public $type;
    public $zipCode;
    public $street;
    public $additionalInfo;
    public $role = User::ROLE_USER;
    public $reCaptcha;
    public $conditionsConfirmed;
    public $old_password;
    public $can_use_api;
    //
    const SCENARIO_REGISTER_USER = 'register_user';
    const SCENARIO_CREATE_USER = 'create_user';
    const SCENARIO_REGISTER_COMPANY = 'register_company';
    const SCENARIO_CREATE_COMPANY = 'create_company';
    const SCENARIO_UPDATE_COMPANY = 'update_company';
    const SCENARIO_UPDATE_USER = 'update_user';
    //
    public $enableCaptchaValidation = true;

    public function scenarios()
    {
        $common = [
            'city',
            'zipCode',
            'street',
            'additionalInfo',
            'email',
            'first_name',
            'last_name',
            'avatar',
            'role',
            'current_password',
            'new_password',
            'confirm_new_password',
            'conditionsConfirmed',
            'type',
            'can_use_api'
        ];
        $scenarios = parent::scenarios();
        $scenarios[self::SCENARIO_REGISTER_USER] = merge($common,
            ['email', 'first_name', 'last_name', 'role', 'password', 'reCaptcha']);
        $scenarios[self::SCENARIO_UPDATE_COMPANY] = merge($common,
            ['companyName', 'vatNumber', 'taxNumber', 'old_password']);
        $scenarios[self::SCENARIO_UPDATE_USER] = merge($common, ['old_password']);

        return $scenarios;
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {

        // We should choose one of variants. Not both!
        //   Using 'SCENARIOS' and theirs arrays of attributes, or
        //   'ON' clausules in RULES

        $rules = array_merge(parent::rules(), [
            [['email'], 'required'],
            [['type'], 'required'],

            [
                ['companyName'],
                'required',
                'on' => [self::SCENARIO_REGISTER_COMPANY, self::SCENARIO_UPDATE_COMPANY, self::SCENARIO_CREATE_COMPANY],
            ],
            [
                ['password'],
                'required',
                'on' => [
                    self::SCENARIO_REGISTER_USER,
                    self::SCENARIO_REGISTER_COMPANY,
                    self::SCENARIO_CREATE_USER,
                    self::SCENARIO_CREATE_COMPANY,
                ],
            ],
            [
                ['first_name', 'last_name'],
                'required',
                'on' => [
                    self::SCENARIO_REGISTER_USER,
                    self::SCENARIO_UPDATE_USER,
                    self::SCENARIO_CREATE_USER,
                    'default',
                ],
            ],

            [['city', 'zipCode', 'street', 'vatNumber', 'taxNumber'], 'string', 'max' => 255],
            [
                ['vatNumber'],
                'csIcoValidate',
            ],
            ['taxNumber', 'filter', 'filter' => 'mb_strtoupper'],
            [
                ['taxNumber'],
                'dicValidate',
            ],
            ['additionalInfo', 'string'],
            [
                ['password'],
                'required',
                'on' => [self::SCENARIO_REGISTER_COMPANY, self::SCENARIO_REGISTER_USER],
            ],
            // Removed for admins to change e-mails
            // [['old_password'], 'required', 'on' => [self::SCENARIO_UPDATE_USER, self::SCENARIO_UPDATE_COMPANY]],
            [
                ['conditionsConfirmed'],
                'required',
                'requiredValue' => 1,
                'message' => _tF('unchecked_conditions', 'register'),
                'on' => [self::SCENARIO_REGISTER_COMPANY, self::SCENARIO_REGISTER_USER],
            ],
            [['can_use_api'], 'boolean'],
            [['can_use_api'], 'default', 'value' => false],
        ]);

        if ($this->enableCaptchaValidation) {
            $rules[] = [
                // verifyCode needs to be entered correctly
                'reCaptcha',
                ReCaptchaValidator::className(),
                'uncheckedMessage' => _tF('register_captcha_error', 'register'),
                'except' => ['createWithoutVerification'],
                'on' => [self::SCENARIO_REGISTER_COMPANY, self::SCENARIO_REGISTER_USER],
            ];
        }

        return $rules;
    }

    public function attributeLabels()
    {
        return ArrayHelper::merge([
            'companyName' => _tF('companyName', 'register'),
            'city' => _tF('city', 'register'),
            'zipCode' => _tF('zipCode', 'register'),
            'street' => _tF('street', 'register'),
            'taxNumber' => _tF('taxNumber', 'register'),
            'vatNumber' => _tF('vatNumber', 'register'),
            'additionalInfo' => _tF('additionalInfo', 'register'),
            'conditionsConfirmed' => _t('conditionsConfirmed', 'register'),
            'old_password' => _tF('old_password', 'user'),
            'can_use_api' => _t('can_use_api', 'api-testing'),
            'reCaptcha' => '',
        ], parent::attributeLabels());
    }

    public function attributeHints()
    {
        return ArrayHelper::merge([
            'additional_info' => _tF('additionalInfo', 'register_hints'),
            'old_password' => _tF('old_password', 'register_hints'),
        ], parent::attributeHints());
    }

    /**
     * Validator only for CZ ICO
     */
    public function csIcoValidate($attribute, $params)
    {
        // pattern
        if (!preg_match('#^\d{8}$#', $this->$attribute)) {
            $this->addError($attribute, _tF('ico_error', 'user'));
            return false;
        }

        // controll SUM
        $ic = $this->$attribute;
        $a = 0;
        for ($i = 0; $i < 7; $i++) {
            $a += $ic[$i] * (8 - $i);
        }

        $a = $a % 11;
        if ($a === 0) {
            $c = 1;
        } elseif ($a === 1) {
            $c = 0;
        } else {
            $c = 11 - $a;
        }

        if ((int)$ic[7] !== $c) {
            $this->addError($attribute, _tF('ico_sum_error', 'user'));
            return false;
        }
        return true;
    }

    /**
     * Validator only for CZ ICO
     */
    public function dicValidate($attribute, $params)
    {
        // pattern
        if (!preg_match('#^[A-Z]{2}\d{8,10}$#', $this->$attribute)) {
            $this->addError($attribute, _tF('dic_error', 'user'));
            return false;
        }

        return true;
    }
}