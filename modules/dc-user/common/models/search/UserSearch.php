<?php
/*
* Copyright 2023 DactylGroup s.r.o.
*
* Licensed under the EUPL, Version 1.2 or – as soon they
will be approved by the European Commission - subsequent
versions of the EUPL (the "Licence");
* You may not use this work except in compliance with the
Licence.
* You may obtain a copy of the Licence at:
*
* https://joinup.ec.europa.eu/software/page/eupl5
*
* Unless required by applicable law or agreed to in
writing, software distributed under the Licence is
distributed on an "AS IS" basis,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
express or implied.
* See the Licence for the specific language governing
permissions and limitations under the Licence.
*/
?>


<?php

namespace modules\user\common\models\search;

use dactylcore\core\data\ActiveDataProvider;
use dactylcore\core\db\ActiveQuery;
use dactylcore\user\common\models\search\AbstractUserSearch;
use dactylcore\user\common\models\UserRole;
use modules\user\common\models\User;
use yii\base\Model;

/**
 * User represents the model behind the search form about `common\models\core\User`.
 */
class UserSearch extends AbstractUserSearch
{
    public $date_from;

    public $date_to;
    public $name;

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id', 'role', 'status', 'updated_at', 'email_verification_token'], 'integer'],
            ['created_at', 'string'],
            [['auth_key', 'password_hash', 'password_reset_token', 'email', 'date_from', 'date_to', 'type', 'name'], 'safe'],
            [['updatedAt', 'createdAt'], 'string'],
            [['can_use_api'], 'boolean'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        /**
         * @var $query ActiveQuery
         */
        $query = User::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'sort' => ['defaultOrder' => ['id' => SORT_DESC]],
        ]);

        if (\Yii::$app->request->get('UserSearch') !== null && !($this->load($params) && $this->validate())) {
            return $dataProvider;
        }

        if (isAdmin()) {
            $query->andWhere(['!=', 'role', UserRole::ROLE_SUPERADMIN_ID]);
        } elseif (!hasAdminRights()) {
            $query->andWhere(['and', ['!=', 'role', UserRole::ROLE_SUPERADMIN_ID], ['!=', 'role', UserRole::ROLE_ADMIN_ID]]);
        }

        $query->andFilterWhere([
            'user.id' => $this->id,
            'user.role' => $this->role,
            'user.status' => $this->status,
            'user.updated_at' => $this->updated_at,
            'user.type' => $this->type,
            'user.can_use_api' => $this->can_use_api,
        ]);


        $query->andFilterWhere(['like', 'email', $this->email]);
        $query->andFilterWhere(['or',
            ['like', 'companyName', $this->name],
            ['like', 'first_name', $this->name],
            ['like', 'last_name', $this->name]
        ]);

        $this->filterDateRange($query, 'createdAt', 'created_at');
        $this->filterDateRange($query, 'updatedAt', 'updated_at');

        if ($this->email_verification_token != null) {
            $query->andWhere([$this->email_verification_token ? 'IS NOT' : 'IS', 'email_verification_token', null]);
        }

        return $dataProvider;
    }
}
