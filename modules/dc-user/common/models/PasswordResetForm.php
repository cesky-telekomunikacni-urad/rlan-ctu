<?php
/*
* Copyright 2023 DactylGroup s.r.o.
*
* Licensed under the EUPL, Version 1.2 or – as soon they
will be approved by the European Commission - subsequent
versions of the EUPL (the "Licence");
* You may not use this work except in compliance with the
Licence.
* You may obtain a copy of the Licence at:
*
* https://joinup.ec.europa.eu/software/page/eupl5
*
* Unless required by applicable law or agreed to in
writing, software distributed under the Licence is
distributed on an "AS IS" basis,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
express or implied.
* See the Licence for the specific language governing
permissions and limitations under the Licence.
*/
?>


<?php


namespace modules\user\common\models;


use dactylcore\user\common\models\AbstractPasswordResetForm;

class PasswordResetForm extends AbstractPasswordResetForm
{
    public $confirm_password;

    public function rules()
    {
        return array_merge(parent::rules(), [
            [['confirm_password'], 'required'],
            ['confirm_password', 'compare', 'compareAttribute' => 'password', 'operator' => '==', 'message' => _tF('passwords_do_not_match', 'user')],
        ]);
    }

    public function attributeLabels()
    {
        return array_merge(parent::attributeLabels(), [
            'confirm_password' => _tF('confirm_password', 'user'),
        ]);
    }
}