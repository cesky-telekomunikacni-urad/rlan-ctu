<?php
/*
* Copyright 2023 DactylGroup s.r.o.
*
* Licensed under the EUPL, Version 1.2 or – as soon they
will be approved by the European Commission - subsequent
versions of the EUPL (the "Licence");
* You may not use this work except in compliance with the
Licence.
* You may obtain a copy of the Licence at:
*
* https://joinup.ec.europa.eu/software/page/eupl5
*
* Unless required by applicable law or agreed to in
writing, software distributed under the Licence is
distributed on an "AS IS" basis,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
express or implied.
* See the Licence for the specific language governing
permissions and limitations under the Licence.
*/
?>


<?php

namespace modules\user\common\models;

use admin\models\RandomStationGenerator;
use common\models\Station;
use common\models\UserAlliance;
use console\tasks\ApiTestingInfoMailJob;
use dactylcore\core\data\ActiveDataProvider;
use dactylcore\core\helpers\Html;
use dactylcore\log\console\tasks\RemoveExportFileJob;
use dactylcore\user\common\models\UserRole;
use HConfig;
use yii\db\ActiveQuery;
use yii\db\QueryInterface;
use yii\helpers\ArrayHelper;
use yii2tech\spreadsheet\Spreadsheet;

/**
 * @property string $companyName
 * @property string $vatNumber
 * @property string $taxNumber
 * @property string $city
 * @property string $zipCode
 * @property string $street
 * @property string $additionalInfo
 * @property string $type
 * @property boolean $can_use_api
 * @property UserAlliance $alliance
 */
class User extends \dactylcore\user\common\models\AbstractUser
{
    public const USER_TYPE_COMPANY = 'company';
    public const USER_TYPE_USER = 'user';

    public static function getUserTypes()
    {
        return [
            self::USER_TYPE_COMPANY => _tF(self::USER_TYPE_COMPANY, 'user'),
            self::USER_TYPE_USER => _tF(self::USER_TYPE_USER, 'user'),
        ];
    }

    public function rules()
    {
        return ArrayHelper::merge([
            [['city', 'zipCode', 'street', 'vatNumber', 'taxNumber', 'type', 'companyName'], 'string', 'max' => 255],
            ['can_use_api', 'boolean',],
            ['additionalInfo', 'safe',],
        ], parent::rules());
    }

    public static function passwordValidatorOptions()
    {
        return array_merge(parent::passwordValidatorOptions(), [
            'minLength' => 8,
            'includeUppercase' => true,
            'includeLowercase' => true,
            'includeDigits' => true,
            'includeSpecials' => true,
        ]);
    }

    public static function getExcludedAttributes(): array
    {
        return ArrayHelper::merge(parent::getExcludedAttributes(), ['facebook_id', 'google_plus_id', 'avatar', 'type']);
    }

    public function attributeLabels()
    {
        return ArrayHelper::merge([
            'companyName' => _tF('companyName', 'register'),
            'city' => _tF('city', 'register'),
            'zipCode' => _tF('zipCode', 'register'),
            'street' => _tF('street', 'register'),
            'taxNumber' => _tF('taxNumber', 'register'),
            'vatNumber' => _tF('vatNumber', 'register'),
            'additionalInfo' => _tF('additionalInfo', 'register'),
            'conditionsConfirmed' => _t('conditionsConfirmed', 'register'),
            'can_use_api' => _t('can_use_api', 'api-testing'),
            'reCaptcha' => '',
        ], parent::attributeLabels());
    }

    /**
     * {@inheritDoc}
     */
    public function getName()
    {
        if ($this->type == self::USER_TYPE_COMPANY) {
            return $this->companyName;
        } else {
            $name = parent::getName();
            if (!trim($name)) {
                $name = _tF('user # {id}', 'station', ['id' => $this->id]);
            }

            return $name;
        }
    }

    public function getNameWithMoreInfo()
    {
        return trim(implode(" ", [
            $this->first_name,
            $this->last_name,
            $this->companyName,
            "(ID {$this->id})",
            $this->email,
        ]));
    }

    public function getNameAndCompanyName()
    {
        return trim(implode(" ", [
            $this->first_name,
            $this->last_name,
            ' / ',
            $this->companyName,
        ]), ' /');
    }

    public function isCompany(): bool
    {
        return $this->type == self::USER_TYPE_COMPANY;
    }

    public function beforeDelete()
    {
        // Can not delete superadmin
        if ($this->role == UserRole::ROLE_SUPERADMIN_ID) {
            return false;
        }

        // Cannot delete other roles based on permissions
        if (!isset(UserRole::getAvailableRoles()[$this->role])) {
            return false;
        }

        return true;
    }

    /**
     * @return ActiveQuery
     */
    public function getStations()
    {
        $a = $this->hasMany(Station::class, ['id_user' => 'id']);
        return $a;
    }

    public function delete()
    {
        $stations = $this->getStations()->all();

        return self::deleteModels($stations) && parent::delete();
    }

    /**
     * Value used as label for dropdown widget.
     */
    // TODO edit avatar
    public function getDropdownLabel()
    {
        return $this->getName();
    }

    public static function getDropdownData($term = null, $page = 1, $limit = 20)
    {
        $offset = ($page - 1) * $limit;

        $query = static::find()
                       ->select([
                           'id',
                           'text' => 'TRIM(CONCAT(IFNULL(first_name,""), " ", IFNULL(last_name,"")))',
                           'email',
                       ])
                       ->filterHaving(['or', ['LIKE', 'text', $term], ['LIKE', 'id', $term]])
                       ->offset($offset)
                       ->orderBy(['first_name' => SORT_ASC]);

        if ($limit !== null) {
            $query->limit($limit);
        }

        $models = $query->asArray()->all();
        foreach ($models as &$model) {
            if (!trim($model['text'])) {
                $model['text'] = "# " . $model['id'];
            }
        }

        return $models;
    }

    public static function getDropdownData2($term = null, $page = 1, $limit = 20)
    {
        $offset = ($page - 1) * $limit;

        $query = static::find()
                       ->select([
                           'id',
                           'text' => 'TRIM(  CONCAT(  IFNULL(first_name,""), " ", IFNULL(last_name,""), " ", IFNULL(companyName,""), " (ID ", IFNULL(id,"-"), ") ", IFNULL(email,"")  )  )',
                           'email',
                       ])
                       ->filterHaving(['or', ['LIKE', 'text', $term], ['LIKE', 'id', $term]])
                       ->offset($offset)
                       ->orderBy(['first_name' => SORT_ASC]);

        if ($limit !== null) {
            $query->limit($limit);
        }

        $models = $query->asArray()->all();
        foreach ($models as &$model) {
            if (!trim($model['text'])) {
                $model['text'] = "# " . $model['id'];
            }
        }

        return $models;
    }

    public static function getDropdownDataIds($term = null, $page = 1, $limit = 20)
    {
        $offset = ($page - 1) * $limit;

        $query = static::find()
                       ->select([
                           'id',
                           'text' => 'LPAD(`id`, 6, "0")',
                       ])
                       ->filterHaving(['LIKE', 'id', $term])
                       ->offset($offset)
                       ->orderBy(['id' => SORT_ASC]);

        if ($limit !== null) {
            $query->limit($limit);
        }

        return $query->asArray()->all();
    }

    /**
     * Function returns chars of initials
     *
     * @return string
     */
    public function getInitials(): string
    {
        $nameWords = [];
        if ($this->isCompany()) {
            $nameWords = array_slice(explode(' ', $this->companyName), 0, 2);
        } else {
            if (isset($this->first_name) && isset($this->last_name)) {
                $nameWords[0] = $this->first_name;
                $nameWords[1] = $this->last_name;
            } else {
                $nameWords = array_slice(explode(' ', $this->first_name), 0, 2);
            }
        }
        // Remove empty strings from name array. Explode adding empty strings
        $nameWords = array_filter($nameWords);

        if (empty($nameWords)) {
            // Return e-mail, if names are empty
            return mb_substr($this->email, 0, 2);
        } elseif (count($nameWords) < 2) {
            // Return two chars if name is only one word
            return mb_substr($nameWords[0], 0, 2);
        } else {
            $initials = "";
            foreach ($nameWords as $name) {
                $initials .= mb_substr($name, 0, 1);
            }
            return $initials;
        }
    }

    /**
     * Returns HTML of initials icon
     *
     * @param string $size
     *
     * @return string
     */
    public function getInitialsIcon(string $size = ""): string
    {
        $cssClass = ($size ? "initials-icon initials-{$size}" : 'initials-icon');
        return Html::tag('div', mb_strtoupper($this->getInitials()), ['class' => $cssClass]);
    }

    public static function getColumnsForExport()
    {
        return [
            [
                'attribute' => 'id',
                'label' => 'ID',
            ],
            [
                'attribute' => 'name',
                'label' => 'Jmeno / Jmeno firmy',
                'value' => function (User $model)
                {
                    return $model->getNameAndCompanyName();
                },
            ],
            [
                'attribute' => 'email',
                'label' => 'Email',
            ],
            [
                'attribute' => 'type',
                'label' => 'Typ (osobni/firemni)',
                'value' => function (User $model)
                {
                    return $model->isCompany() ? 'firemni' : 'osobni';
                },
            ],
            [
                'attribute' => 'role',
                'label' => 'Role',
                'value' => function (User $model)
                {
                    return $model->getRoleName();
                },
            ],
            [
                'attribute' => 'vatNumber',
                'label' => 'ICO',
            ],
            [
                'attribute' => 'taxNumber',
                'label' => 'DIC',
            ],
            [
                'attribute' => 'street',
                'label' => 'Ulice',
            ],
            [
                'attribute' => 'city',
                'label' => 'Mesto',
            ],
            [
                'attribute' => 'zipCode',
                'label' => 'PSC',
            ],
            [
                'attribute' => 'additionalInfo',
                'label' => 'Dodatocne info',
            ],
            [
                'attribute' => 'created_at',
                'label' => 'Vytvoren',
                'value' => function (User $model)
                {
                    return $model->getCreatedAt();
                },
            ],
        ];
    }

    /**
     * Export all records as XLS
     *
     * @param string $domain
     * @param string $fileType
     * @param QueryInterface $query
     *
     * @return string
     */
    public static function exportAll(string $domain, string $fileType, QueryInterface $query): string
    {
        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'pagination' => [
                'pageSize' => 500,
            ],
        ]);

        // Prepare output file and writer
        $fileName = "users_" . date('Y-m-d_His') . "_" . \Yii::$app->security->generateRandomString(6) . '.' . mb_strtolower($fileType);
        $filePath = \Yii::getAlias('@uploads') . '/exports/' . $fileName;
        if (file_exists($filePath)) {
            unlink($filePath);
        }

        $spreadsheet = new Spreadsheet([
            'title' => "users_" . date('Y-m-d_His'),
            'dataProvider' => $dataProvider,
            'writerType' => ucfirst(mb_strtolower($fileType)),
            'columns' => static::getColumnsForExport(),
        ]);
        $spreadsheet->save($filePath);

        // Remove file after time
        \Yii::$app->queue->delay(env('EXPORTS_TTL_MIN', 30) * 60)->push(new RemoveExportFileJob([
            'filePath' => $filePath,
        ]));

        return $domain . "/uploads/exports/{$fileName}";
    }

    /**
     * @return ActiveQuery
     * @throws \yii\base\InvalidConfigException
     */
    public function getAlliance()
    {
        return $this->hasOne(UserAlliance::class, ['id' => 'id_user_alliance'])
                    ->viaTable('user_alliance_connector', ['id_user' => 'id'])
                    ->innerJoin('user_alliance_connector',
                        'user_alliance_connector.id_user_alliance = user_alliance.id')
                    ->andWhere([
                        'OR',
                        ['user_alliance_connector.status' => UserAlliance::STATUS_MEMBER],
                        ['user_alliance_connector.status' => UserAlliance::STATUS_ADMIN],
                    ])
                    ->andWhere(['user_alliance_connector.id_user' => $this->id]);
    }

    /**
     * @return bool
     * @throws \yii\base\InvalidConfigException
     */
    public function isInAlliance()
    {
        return $this->getAlliance()->exists();
    }

    /**
     * To check if this user can use API
     *
     * @return bool
     */
    public function canUseApi(): bool
    {
        return (bool)$this->can_use_api;
    }

    /**
     * @param bool $insert
     * @param array $changedAttributes
     */
    public function afterSave($insert, $changedAttributes)
    {
        parent::afterSave($insert, $changedAttributes);

        // If user get allowance to use api -> send info mail
        if (isset($changedAttributes['can_use_api']) &&
            $changedAttributes['can_use_api'] != $this->can_use_api &&
            $this->can_use_api
        ) {
            ApiTestingInfoMailJob::pushJob([
                'userId' => $this->id,
                'title' => _tF('email_allow_title', 'api-testing'),
                'content' => _tF('email_allow_body', 'api-testing'),
                'subject' => _tF('email_allow_subject', 'api-testing'),
            ]);

            // And generate Random stations if is testing environment
            if (env("API_TESTING_ENVIRONMENT") &&
                Station::find()->andWhere(['id_user' => $this->id])->count('id') == 0
            ) {
                // Generate 10 stations for starters
                RandomStationGenerator::generateRandomStations($this->id);
            }
        }
    }

    public function notifyAdminAboutMeAsNewApiUser()
    {
        $mail = \Yii::$app->mailer
            ->compose('@common/mail/notifyAdminAboutNewApiUser', [
                'title' => _tF('email_notifyAdminAboutNewApiUser_title', 'api-testing'),
                'content' => _tF('email_notifyAdminAboutNewApiUser_content_{userEmail}_{userId}', 'api-testing', [
                    'userEmail' => $this->email,
                    'userId' => $this->id,
                ]),
                'user' => $this,
            ])
            ->setFrom(extractMailFromMailname(HConfig::getValue('SMTP_FROM')))
            ->setTo(HConfig::getValue('SMTP_USERNAME'))
            ->setSubject(_tF('email_notifyAdminAboutNewApiUser_subject', 'api-testing'));

        return $mail->send();
    }
}