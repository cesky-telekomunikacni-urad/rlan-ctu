<?php
/*
* Copyright 2023 DactylGroup s.r.o.
*
* Licensed under the EUPL, Version 1.2 or – as soon they
will be approved by the European Commission - subsequent
versions of the EUPL (the "Licence");
* You may not use this work except in compliance with the
Licence.
* You may obtain a copy of the Licence at:
*
* https://joinup.ec.europa.eu/software/page/eupl5
*
* Unless required by applicable law or agreed to in
writing, software distributed under the Licence is
distributed on an "AS IS" basis,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
express or implied.
* See the Licence for the specific language governing
permissions and limitations under the Licence.
*/
?>


<?php

namespace modules\user\common\models;

use dactylcore\user\common\models\AbstractUserRole;

/**
 * @inheritDoc
 */
class UserRole extends AbstractUserRole
{
    public const ROLE_TECHNICIAN_ID = 4;


    public function hasAccessTo($permission)
    {
        // If this function is called from the "tests-app", there are no permissions for superadmin.
        // This is only dirty fix, just for this project and its tests, because test app do not have
        // any perms in the config.
        if (isSuperAdmin()) {
            return true;
        }

        $permissions = $this->getPermissions();

        if (is_string($permission)) {
            return isset($permissions[$permission]);
        }

        if (is_array($permission)) {
            $hasAccessTo = false;

            foreach ($permission as $p) {
                if (isset($permissions[$p])) {
                    $hasAccessTo = true;
                }
            }

            return $hasAccessTo;
        }

        return false;
    }
}