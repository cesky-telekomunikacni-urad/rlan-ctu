<?php
/*
* Copyright 2023 DactylGroup s.r.o.
*
* Licensed under the EUPL, Version 1.2 or – as soon they
will be approved by the European Commission - subsequent
versions of the EUPL (the "Licence");
* You may not use this work except in compliance with the
Licence.
* You may obtain a copy of the Licence at:
*
* https://joinup.ec.europa.eu/software/page/eupl5
*
* Unless required by applicable law or agreed to in
writing, software distributed under the Licence is
distributed on an "AS IS" basis,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
express or implied.
* See the Licence for the specific language governing
permissions and limitations under the Licence.
*/
?>


<?php

use dactylcore\core\db\Migration;

class m191028_000001_translation_update2 extends Migration
{
    public function safeUp()
    {
        // Article category
        $translations = [
            'cs' => [
                'activeInterference' => 'NS je rušena',
                'activeInterferenceHelp' => 'Nově instalovaná stanice může být rušena touto stanicí',
                'activeIsolationIncreaseRequested' => 'NS je rušena o [dB]',
                'cannotActive' => 'Nově instalovaná stanice nemůže být rušena touto stanicí',

                'passiveInterference' => 'NS ruší',
                'passiveInterferenceHelp' => 'Nově instalovaná stanice může rušit tuto stanici',
                'passiveInterferenceHelpPositiveColumn' => 'Nově instalovaná stanice neruší tuto stanici',
                'passiveInterferenceHelpNegativeColumn' => 'Nově instalovaná stanice ruší tuto stanicí',
                'passiveIsolationIncreaseRequested' => 'NS ruší o [dB]',
                'cannotPassive' => 'Nově instalovaná stanice nemůže rušit tuto stanici',

                'hasConflicts' => 'konflikt',
                'hasConflictsHelp' => 'Může dojít ke konfliktu mezi stanicemi.',
                'hasConflictsHelpNegative' => 'Dochází ke konfliktu mezi stanicemi. Novou stanici nelze umístit.',
                'hasConflictsHelpPositive' => 'Nedochází ke konfliktu mezi stanicemi. Novou stanici lze umístit.',

                'activeInterferenceHelpPositiveColumn' => 'Nově instalovaná stanice není rušena touto stanici',
                'activeInterferenceHelpNegativeColumn' => 'Nově instalovaná stanice je rušena touto stanicí',

                'cannotConflict' => 'Nemůže dojít ke konfliktu',

                'yes' => 'ano',
                'no' => 'ne',

                'declaration' => 'prohlášení o izolaci',
                'declare' => 'prohlásit za izolované a publikovat',
                'successfully declared' => 'Úspěšně jste deklaroval(a) nerušení a stanici publikoval(a)'
            ],
            'en' => [
                'activeInterference' => 'NS is interfered',
                'activeInterferenceHelp' => 'NS can be interfered by this stations',
                'activeIsolationIncreaseRequested' => 'interfered by [dB]',
                'cannotActive' => 'NS cannot be interfered by this station',

                'passiveInterference' => 'NS interferes',
                'passiveInterferenceHelp' => 'NS can interfere this station',
                'passiveInterferenceHelpPositiveColumn' => 'NS does not interfere this station',
                'passiveInterferenceHelpNegativeColumn' => 'NS does interfere this station',
                'passiveIsolationIncreaseRequested' => 'interfered [dB]',
                'cannotPassive' => 'NS cannot interfere this station',

                'hasConflicts' => 'conflict',
                'hasConflictsHelp' => 'There might be a conflict',
                'hasConflictsHelpNegative' => 'There is a conflict between this and new station. You cannot place the new station',
                'hasConflictsHelpPositive' => 'There is not any conflict between this and new station. You can place the new station',
                'cannotConflict' => 'There cannot appear any conflict',

                'activeInterferenceHelpPositiveColumn' => 'New station is not interfered by this station.',
                'activeInterferenceHelpNegativeColumn' => 'New station is interfered by this station.',

                'yes' => 'yes',
                'no' => 'no',

                'declaration' => 'declaration about isolation',
                'declare' => 'declare as isolated and publish',
                'successfully declared' => 'You successfully declared station as undisturbed and you have published this station',
            ],
        ];

        $this->saveTranslation(static::CS_LANG_ID, 'frontend.station', $translations['cs']);
        $this->saveTranslation(static::EN_LANG_ID, 'frontend.station', $translations['en']);

        $this->saveConfig('DECLARATION_TEXT', '', 'content', 'stations', 1, 1);

        $translations = [
            'cs' => [
                'DECLARATION_TEXT' => 'Text prohlášení o izolaci',
                'content' => 'Obsah',
                'stations' => 'Stanice',
            ],
            'en' => [
                'DECLARATION_TEXT' => 'Text of declaration about isolation',
            ]
        ];

        $this->saveTranslation(static::CS_LANG_ID, 'admin.config', $translations['cs']);
        $this->saveTranslation(static::EN_LANG_ID, 'admin.config', $translations['en']);

        $translations = [
            'cs' => [
                'app-frontend_station_update-all' => 'Upravovat všechny stanice',

                'app-admin_phi' => 'PHI',
                'app-admin_phi_index' => 'Seznam',
                'app-admin_phi_create' => 'Vytvořit',
                'app-admin_phi_update' => 'Upravit',
                'app-admin_phi_delete' => 'Smazat',

                'app-admin_phi-wigig' => 'PHI Wigig',
                'app-admin_phi-wigig_index' => 'Seznam',
                'app-admin_phi-wigig_create' => 'Vytvořit',
                'app-admin_phi-wigig_update' => 'Upravit',
                'app-admin_phi-wigig_delete' => 'Smazat',

                'app-admin_station' => 'Stanice',
                'app-admin_station_index' => 'Seznam',
                'app-admin_station_create' => 'Vytvořit',
                'app-admin_station_update' => 'Upravit',
                'app-admin_station_delete' => 'Smazat',

            ],
            'en' => [
                'app-frontend_station_update-all' => 'Modify all stations',

                'app-admin_phi' => 'PHI',
                'app-admin_phi_index' => 'List',
                'app-admin_phi_create' => 'Create',
                'app-admin_phi_update' => 'Update',
                'app-admin_phi_delete' => 'Delete',

                'app-admin_phi-wigig' => 'PHI Wigig',
                'app-admin_phi-wigig_index' => 'List',
                'app-admin_phi-wigig_create' => 'Create',
                'app-admin_phi-wigig_update' => 'Update',
                'app-admin_phi-wigig_delete' => 'Delete',

                'app-admin_station' => 'Station',
                'app-admin_station_index' => 'List',
                'app-admin_station_create' => 'Create',
                'app-admin_station_update' => 'Update',
                'app-admin_station_delete' => 'Delete',
            ]
        ];

        $this->saveTranslation(static::CS_LANG_ID, 'admin.access-manager', $translations['cs']);
        $this->saveTranslation(static::EN_LANG_ID, 'admin.access-manager', $translations['en']);


    }
}



