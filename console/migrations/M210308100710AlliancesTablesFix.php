<?php
/*
* Copyright 2023 DactylGroup s.r.o.
*
* Licensed under the EUPL, Version 1.2 or – as soon they
will be approved by the European Commission - subsequent
versions of the EUPL (the "Licence");
* You may not use this work except in compliance with the
Licence.
* You may obtain a copy of the Licence at:
*
* https://joinup.ec.europa.eu/software/page/eupl5
*
* Unless required by applicable law or agreed to in
writing, software distributed under the Licence is
distributed on an "AS IS" basis,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
express or implied.
* See the Licence for the specific language governing
permissions and limitations under the Licence.
*/
?>


<?php

use dactylcore\core\db\Migration;
use yii\helpers\Inflector;

class M210308100710AlliancesTablesFix extends Migration
{
    public function safeUp(): bool
    {
        $this->i18nFronte();
        $this->i18nLogs();
        $this->i18nComm();

        return true;
    }

    protected function i18nFronte(): void
    {
        $this->saveTranslation(
            self::CS_LANG_ID,
            "frontend.user-alliance",
            [
                'cannot_degrade_user' => 'Nelze degradovat tohoto uživatele',
                'success_degrade_user' => 'Uživatel úspěšně degradován',
                'degrade_member' => 'degradovat na člena',
                'change_alliance_description' => 'upravit popis sdružení',
                'update_description' => 'upravit',
                'create_description' => 'přidat popis',
                'save_description' => 'uložit',
                'cannot_change_description' => 'Nelze uložit nový popis',
                'success_change_description' => 'Nový popis úspěšně uložen',
            ]
        );
        $this->saveTranslation(
            self::EN_LANG_ID,
            "frontend.user-alliance",
            [
                'cannot_degrade_user' => 'Cannot degrade this user',
                'success_degrade_user' => 'User successfully degraded',
                'degrade_member' => 'degrade to member',
                'change_alliance_description' => 'change alliance description',
                'update_description' => 'change',
                'create_description' => 'add description',
                'save_description' => 'save',
                'cannot_change_description' => 'Cannot save new description',
                'success_change_description' => 'Description successfully saved',
            ]
        );
    }

    protected function i18nComm(): void
    {
        $this->saveTranslation(
            self::CS_LANG_ID,
            'common.user-alliance',
            [
                'notification_invitation_text' => 'Byl jste přizván do sdružení. Pozvánku přijmete tlačítkem níže.<br><br><i>Pokud není tato zpráva určena pro Vás, můžete ji ignorovat.</i>',
            ]
        );
        $this->saveTranslation(
            self::EN_LANG_ID,
            'common.user-alliance',
            [
                'notification_invitation_text' => 'You have been invited to join the alliance. You can accept the invitation by clicking the button below.<br><br>If this message is not intended for you, you can ignore it.',
            ]
        );
    }


    protected function i18nLogs(): void
    {
        $this->saveTranslation(
            self::CS_LANG_ID,
            'admin.log',
            [
                'member {email} degraded in {name} by {admin}' => 'Uživatel "{email}" degradován ve sdružení "{name}" uživatelem {admin}.',
            ]
        );
        $this->saveTranslation(
            self::EN_LANG_ID,
            'admin.log',
            [
                'member {email} degraded in {name} by {admin}' => 'User "{email}" degraded in alliance "{name}" by {admin}.',
            ]
        );
    }
}
