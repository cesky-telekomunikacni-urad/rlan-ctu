<?php
/*
* Copyright 2023 DactylGroup s.r.o.
*
* Licensed under the EUPL, Version 1.2 or – as soon they
will be approved by the European Commission - subsequent
versions of the EUPL (the "Licence");
* You may not use this work except in compliance with the
Licence.
* You may obtain a copy of the Licence at:
*
* https://joinup.ec.europa.eu/software/page/eupl5
*
* Unless required by applicable law or agreed to in
writing, software distributed under the Licence is
distributed on an "AS IS" basis,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
express or implied.
* See the Licence for the specific language governing
permissions and limitations under the Licence.
*/
?>


<?php

use dactylcore\core\db\Migration;


/**
 * Class m200106_000001_number
 */
class m200106_000002_number extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->lang();
    }

    public function lang()
    {
        $translations = [
            'cs' => [
                'must be a number separated by point' => 'musí být číslo oddělené desetinnou tečkou',
                'max distance length crossed' => 'Povolená délka spoje je max. 10 km',
            ],
            'en' => [
            ]
        ];


        $this->saveTranslation(static::CS_LANG_ID, 'frontend.station', $translations['cs']);
        $this->saveTranslation(static::EN_LANG_ID, 'frontend.station', $translations['en']);


        $translations = [
            'cs' => [
                'frequency' => 'Střední kmitočet musít být v intervalu 57 000 MHz až 66 000 MHz',
            ],
            'en' => [
            ]
        ];


        $this->saveTranslation(static::CS_LANG_ID, 'frontend.station_hint', $translations['cs']);
        $this->saveTranslation(static::EN_LANG_ID, 'frontend.station_hint', $translations['en']);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m200106_000001_number cannot be reverted.\n";

        return false;
    }

}
