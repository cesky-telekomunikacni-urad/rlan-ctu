<?php
/*
* Copyright 2023 DactylGroup s.r.o.
*
* Licensed under the EUPL, Version 1.2 or – as soon they
will be approved by the European Commission - subsequent
versions of the EUPL (the "Licence");
* You may not use this work except in compliance with the
Licence.
* You may obtain a copy of the Licence at:
*
* https://joinup.ec.europa.eu/software/page/eupl5
*
* Unless required by applicable law or agreed to in
writing, software distributed under the Licence is
distributed on an "AS IS" basis,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
express or implied.
* See the Licence for the specific language governing
permissions and limitations under the Licence.
*/
?>


<?php

use dactylcore\core\db\Migration;


class M210127124800ExclusionZones58GHz extends Migration
{
    public function safeUp()
    {
        $this->config();
        $this->trans();
    }

    protected function config()
    {
        $this->saveConfig('EXCLUDED_ZONES_58_GHZ', '', 'stations', 'excluded-zones', 0,1);
        $this->saveConfig('EXCLUDED_ZONE_58_GHZ_DIAMETER', '13', 'stations', 'excluded-zones', 0,1);
    }

    protected function trans()
    {
        $translations = [
            'cs' => [
                'excluded-zones' => 'Ochranné zóny',
                'EXCLUDED_ZONES_58_GHZ' => 'Ochranné zóny pro pásmo 5.8GHz',
                'EXCLUDED_ZONE_58_GHZ_DIAMETER' => 'Poloměr ochranné zóny pro pásmo 5.8GHz v km',
                'EXCLUDED_ZONES_58_GHZ_HINT' => 'Zadejte GPS souřadnice středů ochranných zón ve tvaru LAT,LNG (xy.abc,xy.abc) - každá položka na samostatný řádek.',
            ],
            'en' => [
                'excluded-zones' => 'Exclusion zones',
                'EXCLUDED_ZONES_58_GHZ' => 'Exclusion zones for 5.8GHz',
                'EXCLUDED_ZONE_58_GHZ_DIAMETER' => 'Diameter of exclusion zone for 5.8GHz in km',
                'EXCLUDED_ZONES_58_GHZ_HINT' => 'Enter GPS coordinates of exclusion zones centres in format LAT,LNG (xy.abc,xy.abc) - each entry on separate line.',
            ],
        ];
        $this->saveTranslation(static::CS_LANG_ID, 'admin.config', $translations['cs']);
        $this->saveTranslation(static::EN_LANG_ID, 'admin.config', $translations['en']);

        $translations = [
            'cs' => [
                'This AP is in conflict, please move it.' => 'Váš přístupový bod je v konfliktu s jednou nebo více mýtných brán nebo se nachází ve výluční zóně, přesuňte ho prosím.',
            ],
            'en' => [
                'This AP is in conflict, please move it.' => 'Your access point is in conflict with one or more toll gates or is located in an exclusion zone, reposition in please.',
            ],
        ];
        $this->saveTranslation(static::CS_LANG_ID, 'common.station', $translations['cs']);
        $this->saveTranslation(static::EN_LANG_ID, 'common.station', $translations['en']);

    }
}
