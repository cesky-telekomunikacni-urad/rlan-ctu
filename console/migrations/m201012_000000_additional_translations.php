<?php
/*
* Copyright 2023 DactylGroup s.r.o.
*
* Licensed under the EUPL, Version 1.2 or – as soon they
will be approved by the European Commission - subsequent
versions of the EUPL (the "Licence");
* You may not use this work except in compliance with the
Licence.
* You may obtain a copy of the Licence at:
*
* https://joinup.ec.europa.eu/software/page/eupl5
*
* Unless required by applicable law or agreed to in
writing, software distributed under the Licence is
distributed on an "AS IS" basis,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
express or implied.
* See the Licence for the specific language governing
permissions and limitations under the Licence.
*/
?>


<?php

use dactylcore\core\db\Migration;


/**
 * Class m200115_000000_flash
 */
class m201012_000000_additional_translations extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->lang();
    }

    public function lang()
    {
        $translations = [
            'cs' => [
                'toll gate' => 'Mýtná brána',
                'back' => 'zpět',
                'frequency_label' => 'střední kmitočet',
                'contact the owner if station conflicts' => 'Kontaktujte majitele stanice, pokud jsou Vaše stanice v konfliktu.',
                'eirp has to be in range -20 to 40' => 'E.I.R.P pro PtMP musí být v intervalu -20 dBm až +40 dBm',
                'eirp has to be in range -20 to 55' => 'E.I.R.P pro PtP musí být v intervalu -20 dBm až +55 dBm',
                'Do you know antenna gain and power or EIRP' => 'Znáte zisk antény a výkon nebo jen EIRP?',
            ],
            'en' => [
                'back' => 'back',
                'frequency_label' => 'center frequency',
            ]
        ];


        $this->saveTranslation(static::CS_LANG_ID, 'frontend.station', $translations['cs']);
        $this->saveTranslation(static::EN_LANG_ID, 'frontend.station', $translations['en']);

        $translations = [
            'cs' => [
                'conditionsConfirmed' => 'Souhlasím s <a href="https://www.ctu.cz/podminky-uziti" target="_blank">podmínkami použití</a>',
            ],
            'en' => [
                'conditionsConfirmed' => 'I agree with <a href="https://www.ctu.eu/terms-use" target="_blank">terms of use</a>',
            ]
        ];


        $this->saveTranslation(static::CS_LANG_ID, 'common.register', $translations['cs']);
        $this->saveTranslation(static::EN_LANG_ID, 'common.register', $translations['en']);

        $translations = [
            'cs' => [
                'power' => 'Někdy označováno: RF injection power into antenna, transmission power, conducted power.',
                'eirp' => 'E.I.R.P. [dBm] = zisk antény [dBi] + výkon [dBm]',
            ],
            'en' => [
                'power' => 'RF injection power into antenna, transmission power, conducted power',
            ]
        ];


        $this->saveTranslation(static::CS_LANG_ID, 'frontend.station_hint', $translations['cs']);
        $this->saveTranslation(static::EN_LANG_ID, 'frontend.station_hint', $translations['en']);

        $translations = [
            'cs' => [
                'conversations summary' => 'Zobrazeno {begin} - {end} z {totalCount} konverzací',
            ],
            'en' => [
                'conversations summary' => 'Showing {begin} - {end} of {totalCount} conversations',
            ],
        ];

        $this->saveTranslation(static::CS_LANG_ID, 'frontend.chat', $translations['cs']);
        $this->saveTranslation(static::EN_LANG_ID, 'frontend.chat', $translations['en']);
    }
}
