<?php
/*
* Copyright 2023 DactylGroup s.r.o.
*
* Licensed under the EUPL, Version 1.2 or – as soon they
will be approved by the European Commission - subsequent
versions of the EUPL (the "Licence");
* You may not use this work except in compliance with the
Licence.
* You may obtain a copy of the Licence at:
*
* https://joinup.ec.europa.eu/software/page/eupl5
*
* Unless required by applicable law or agreed to in
writing, software distributed under the Licence is
distributed on an "AS IS" basis,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
express or implied.
* See the Licence for the specific language governing
permissions and limitations under the Licence.
*/
?>


<?php

use dactylcore\core\db\Migration;


class M221209150244MacDupli2 extends Migration
{
    public function safeUp()
    {
        $this->i18n();
    }

    protected function i18n()
    {
        $this->saveTranslationCsEn('common.station', [
            'unique_mac_error_with_id_and_user' => [
                'MAC adresa "{value}" již byla použita u uživatele s ID={idUser}',
                'MAC address "{value}" was already used by user with ID={idUser}',
            ],
        ]);
    }
}
