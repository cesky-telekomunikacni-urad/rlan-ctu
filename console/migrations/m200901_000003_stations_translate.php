<?php
/*
* Copyright 2023 DactylGroup s.r.o.
*
* Licensed under the EUPL, Version 1.2 or – as soon they
will be approved by the European Commission - subsequent
versions of the EUPL (the "Licence");
* You may not use this work except in compliance with the
Licence.
* You may obtain a copy of the Licence at:
*
* https://joinup.ec.europa.eu/software/page/eupl5
*
* Unless required by applicable law or agreed to in
writing, software distributed under the Licence is
distributed on an "AS IS" basis,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
express or implied.
* See the Licence for the specific language governing
permissions and limitations under the Licence.
*/
?>


<?php

use dactylcore\core\db\Migration;


/**
 * Class m190830_113326_i18n
 */
class m200901_000003_stations_translate extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->lang();
    }

    protected function lang()
    {
        $translations = [
            'cs' => [
                'popup_direction' => 'Směr',
                'distance_error' => 'Vzdálenost mezi FS stanicemi nesmí být větší než {distance} km',
                'show list' => 'zobrazit seznam stanic',
                'status_headline' => 'stav',
                'type_headline' => 'typ',
                'stations60ghz' => '60GHz stanice',
                'switch map and list view' => 'Přepne mezi zobrazením se seznamem stanic a zobrazením mapy na celou obrazovku.'
            ],
            'en' => [
                'popup_direction' => 'Direction',
                'distance_error' => 'Distance between FS stations cannot be bigger than {distance} km',
                'show list' => 'Show station list',
                'status_headline' => 'status',
                'type_headline' => 'type',
                'stations60ghz' => '60GHz stations',
                'switch map and list view' => 'Switch between view with station list and view with fullscreen map.'
            ],
        ];

        $this->saveTranslation(static::CS_LANG_ID, 'common.station', $translations['cs']);
        $this->saveTranslation(static::EN_LANG_ID, 'common.station', $translations['en']);

    }
}
