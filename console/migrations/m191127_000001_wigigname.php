<?php
/*
* Copyright 2023 DactylGroup s.r.o.
*
* Licensed under the EUPL, Version 1.2 or – as soon they
will be approved by the European Commission - subsequent
versions of the EUPL (the "Licence");
* You may not use this work except in compliance with the
Licence.
* You may obtain a copy of the Licence at:
*
* https://joinup.ec.europa.eu/software/page/eupl5
*
* Unless required by applicable law or agreed to in
writing, software distributed under the Licence is
distributed on an "AS IS" basis,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
express or implied.
* See the Licence for the specific language governing
permissions and limitations under the Licence.
*/
?>


<?php

use dactylcore\core\db\Migration;


/**
 * Class m190830_113326_i18n
 */
class m191127_000001_wigigname extends Migration
{
    public function safeUp()
    {
        $this->lang();
    }


    protected function lang()
    {
        $translations = [
            'cs' => [
                'new wigig' => 'NOVÁ stanice WiGig PtP/PtMP',
                'new wigig help' => 'MGWS, IEEE802.11ad, beamforming; (s mitigací)',
                'new fs' => 'NOVÁ stanice FS PtP',
                'new fs help' => 'FS – Fixed Service; pouze pevné směrové spoje; (bez mitigace)',
                'eirp_manual' => 'znám jen EIRP',
                'eirp_auto' => 'automatický výpočet',
            ],
            'en' => [
                'new wigig' => 'NEW station WiGig PtP/PtMP',
                'new wigig help' => 'MGWS, IEEE802.11ad, beamforming; (with mitigation)',
                'new fs' => 'NEW station FS PtP',
                'new fs help' => 'FS – Fixed Service; solid connections only; (without mitigation)',
                'eirp_manual' => 'I know EIRP only',
                'eirp_auto' => 'automatic calculation',
            ]
        ];

        $this->saveTranslation(static::CS_LANG_ID, 'frontend.station', $translations['cs']);
        $this->saveTranslation(static::EN_LANG_ID, 'frontend.station', $translations['en']);
    }
}
