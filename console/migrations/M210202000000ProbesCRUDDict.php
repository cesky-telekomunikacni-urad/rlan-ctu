<?php
/*
* Copyright 2023 DactylGroup s.r.o.
*
* Licensed under the EUPL, Version 1.2 or – as soon they
will be approved by the European Commission - subsequent
versions of the EUPL (the "Licence");
* You may not use this work except in compliance with the
Licence.
* You may obtain a copy of the Licence at:
*
* https://joinup.ec.europa.eu/software/page/eupl5
*
* Unless required by applicable law or agreed to in
writing, software distributed under the Licence is
distributed on an "AS IS" basis,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
express or implied.
* See the Licence for the specific language governing
permissions and limitations under the Licence.
*/
?>


<?php

use dactylcore\core\db\Migration;


class M210202000000ProbesCRUDDict extends Migration
{
    public function safeUp()
    {
        $this->lang();
    }

    public function lang() {

        $translations = [
            'cs' => [
                'create_probe' => 'Přidat sondu',
                'create_successful_message' => 'Sonda úspěšně vytvořena!',
                'create_error_message' => 'Sondu nebylo možné vytvořit!',
                'flash_successfully_deleted' => 'Sonda smazána',
                'probe scans' => 'Uložená měření',
            ],
            'en' => [
                'create_probe' => 'Create probe',
                'create_successful_message' => 'Probe successfully created!',
                'create_error_message' => 'Probe could not be created!}',
                'flash_successfully_deleted' => 'Probe deleted',
                'probe scans' => 'Probe scans',
            ]
        ];

        $this->saveTranslation(self::CS_LANG_ID, 'admin.probe', $translations['cs']);
        $this->saveTranslation(self::EN_LANG_ID, 'admin.probe', $translations['en']);

    }
}
