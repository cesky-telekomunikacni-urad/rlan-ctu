<?php
/*
* Copyright 2023 DactylGroup s.r.o.
*
* Licensed under the EUPL, Version 1.2 or – as soon they
will be approved by the European Commission - subsequent
versions of the EUPL (the "Licence");
* You may not use this work except in compliance with the
Licence.
* You may obtain a copy of the Licence at:
*
* https://joinup.ec.europa.eu/software/page/eupl5
*
* Unless required by applicable law or agreed to in
writing, software distributed under the Licence is
distributed on an "AS IS" basis,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
express or implied.
* See the Licence for the specific language governing
permissions and limitations under the Licence.
*/
?>


<?php

use dactylcore\core\db\Migration;


class m200820_000000_wigig_angle extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->direction();
    }

    protected function direction()
    {
        $this->addColumn('station_wigig', 'angle_point_lat', $this->decimal(16, 14)->null());
        $this->addColumn('station_wigig', 'angle_point_lng', $this->decimal(16, 14)->null());
    }
}
