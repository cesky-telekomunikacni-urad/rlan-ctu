<?php
/*
* Copyright 2023 DactylGroup s.r.o.
*
* Licensed under the EUPL, Version 1.2 or – as soon they
will be approved by the European Commission - subsequent
versions of the EUPL (the "Licence");
* You may not use this work except in compliance with the
Licence.
* You may obtain a copy of the Licence at:
*
* https://joinup.ec.europa.eu/software/page/eupl5
*
* Unless required by applicable law or agreed to in
writing, software distributed under the Licence is
distributed on an "AS IS" basis,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
express or implied.
* See the Licence for the specific language governing
permissions and limitations under the Licence.
*/
?>


<?php

use dactylcore\core\db\Migration;


class M210113091201ExportStationsInRadius extends Migration
{
    public function safeUp()
    {
        $this->lang();
    }

    protected function lang()
    {
        $translations = [
            'cs' => [
                'export_stations_in_radius' => 'Export stanic v okruhu',
                'export_form_button_label' => 'Exportovat do CSV',
                'radius' => 'poloměr [km]',
                'export_success_message' => 'Úspěšně vyexportováno do',
                'successfully_exported' => 'Úspěšně vyexportováno',
            ],
            'en' => [
                'export_stations_in_radius' => "Export stations in radius",
                'export_form_button_label' => 'Export as CSV',
                'radius' => 'radius [km]',
                'export_success_message' => 'successfully exported to',
                'successfully_exported' => 'successfully exported',
            ],
        ];

        $this->saveTranslation(static::CS_LANG_ID, 'common.station', $translations['cs']);
        $this->saveTranslation(static::EN_LANG_ID, 'common.station', $translations['en']);
    }
}
