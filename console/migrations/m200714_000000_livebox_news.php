<?php
/*
* Copyright 2023 DactylGroup s.r.o.
*
* Licensed under the EUPL, Version 1.2 or – as soon they
will be approved by the European Commission - subsequent
versions of the EUPL (the "Licence");
* You may not use this work except in compliance with the
Licence.
* You may obtain a copy of the Licence at:
*
* https://joinup.ec.europa.eu/software/page/eupl5
*
* Unless required by applicable law or agreed to in
writing, software distributed under the Licence is
distributed on an "AS IS" basis,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
express or implied.
* See the Licence for the specific language governing
permissions and limitations under the Licence.
*/
?>


<?php

use dactylcore\core\db\Migration;


class m200714_000000_livebox_news extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->config();
        $this->lang();
    }

    public function config()
    {

        $this->saveConfig('HELP_YT_VIDEO', '', 'content', 'help-box', 1,1);
        $this->saveConfig('HELP_MANUAL_PDF', '', 'content', 'help-box', 1, 1);

    }

    public function lang()
    {
        $translations = [
            'cs' => [
                'help' => 'Nápověda',
                'instructions' => 'Kompletní návod',
                'user guide' => 'Návod pro uživatele',
                'no file provided' => 'Soubor bohužel není dostupný',
            ],
            'en' => [
                'help' => 'Help',
                'instructions' => 'Instructions',
                'user guide' => 'User guide',
                'no file provided' => 'File is unfortunately unreachable',
            ],
        ];

        $this->saveTranslation(static::CS_LANG_ID, 'frontend.help-box', $translations['cs']);
        $this->saveTranslation(static::EN_LANG_ID, 'frontend.help-box', $translations['en']);

        $translations = [
            'cs' => [
                'HELP_YT_VIDEO' => 'Videonávod (link na Youtube)',
                'HELP_YT_VIDEO_HINT' => 'Zadejte URL videa pro sdílení ve tvaru youtu.be/... Tato URL je dostupná z dialogového okna zobrazeného po kliknutí na "Sdílet" pod videem.',
                'HELP_MANUAL_PDF' => 'Návod pro uživatele',
                'help-box' => 'Nápověda',
            ],
            'en' => [
                'HELP_YT_VIDEO' => 'Video tutorial Youtube link',
                'HELP_YT_VIDEO_HINT' => 'Enter video URL in youtu.be/... format.',
                'HELP_MANUAL_PDF' => 'User guide',
                'help-box' => 'Help',
            ],
        ];

        $this->saveTranslation(static::CS_LANG_ID, 'admin.config', $translations['cs']);
        $this->saveTranslation(static::EN_LANG_ID, 'admin.config', $translations['en']);


    }

}
