<?php
/*
* Copyright 2023 DactylGroup s.r.o.
*
* Licensed under the EUPL, Version 1.2 or – as soon they
will be approved by the European Commission - subsequent
versions of the EUPL (the "Licence");
* You may not use this work except in compliance with the
Licence.
* You may obtain a copy of the Licence at:
*
* https://joinup.ec.europa.eu/software/page/eupl5
*
* Unless required by applicable law or agreed to in
writing, software distributed under the Licence is
distributed on an "AS IS" basis,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
express or implied.
* See the Licence for the specific language governing
permissions and limitations under the Licence.
*/
?>


<?php

use dactylcore\core\db\Migration;


/**
 * Class m190830_113326_i18n
 */
class m191202_000001_hints extends Migration
{
    public function safeUp()
    {
        $this->lang();
    }


    protected function lang()
    {
        $translations = [
            'cs' => [
                // wigig
                'eirp' => 'E.I.R.P. [dBm) = zisk [dBi] + výkon [dBm]',
                'next' => 'Pokračovat na další krok.',
                'next my' => 'Pokračovat na další krok. Změny nebudou uloženy.',
                'channel_width' => 'Zabraná šířka pásma, channel bandwidth. Např. 2160 MHz.',
                'save and recount' => 'Koordinační kalkulačka odhadne rádiovou bilanci a navrhne další kroky.',
                'power' => 'RF injection power into antenna, transmission power, conducted power.',
                'antenna_volume' => 'Při zisku antény nad 25 dBi je stanice považována za WiGig PtP. Pro < 25 dBi je považována za WiGig PtMP.',
                'publish' => 'Status „Čeká“ se změní na „Aktivní“, stanice je zobrazena na mapě a je zaznamenáno datum první registrace.',
                'choose angle' => 'Kliknutím na růžici aktivujete nastavení azimutu, kam směřuje hlavní svazek. V případě beamformingu vezměte střední úhel.',

                // fs
                'qam' => 'Modulační schéma.',
                'save and continue' => 'Koordinační kalkulačka odhadne rádiovou bilanci a navrhne další kroky.',
                'table of affected stations' => 'Koordinační kalkulačka prověří, zda není potenciální konflikt s blízkými stanicemi. Posoudí, zda Nová stanice může potenciálně rušit, nebo zda může být potenciálně rušena.',

                'status' => '„Aktivní“: stanice je úspěšně zkoordinována a je zobrazena na mapě. „Čeká“: stanice čeká na úkon potřebný k dokončení koordinace; není zobrazena na mapě. „Koncept“: zadávané údaje o stanici nejsou potvrzeny. „Expirováno“: záznam o stanici vypršel; stanice se již nezobrazuje na mapě.',
                'registered_at' => 'Datum první registrace označuje den prvního záznamu.',
                'valid_to' => 'Platnost záznamu je 1 rok od první registrace nebo od Obnovení registrace.',
                'protected_to' => 'Ochranná lhůta je datum, po němž bude záznam automaticky vymazán, pokud nedojde k Obnovení registrace.',
                'compare_table' => 'Koordinační kalkulačka prověří, zda není potenciální konflikt s blízkými stanicemi. Posoudí, zda Nová stanice může potenciálně rušit, nebo zda může být potenciálně rušena.',
                'comparison table help' => 'Koordinační kalkulačka prověřila potenciální konflikty s blízkými stanicemi.',

            ],
            'en' => [
            ]
        ];

        $this->saveTranslation(static::CS_LANG_ID, 'frontend.station_hint', $translations['cs']);
        $this->saveTranslation(static::EN_LANG_ID, 'frontend.station_hint', $translations['en']);
        $translations = [
            'cs' => [
                'station_name' => 'název',
            ],
            'en' => [
                'station_name' => 'name',
            ]
        ];

        $this->saveTranslation(static::CS_LANG_ID, 'frontend.station', $translations['cs']);
        $this->saveTranslation(static::EN_LANG_ID, 'frontend.station', $translations['en']);
    }
}