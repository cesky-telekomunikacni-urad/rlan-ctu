<?php
/*
* Copyright 2023 DactylGroup s.r.o.
*
* Licensed under the EUPL, Version 1.2 or – as soon they
will be approved by the European Commission - subsequent
versions of the EUPL (the "Licence");
* You may not use this work except in compliance with the
Licence.
* You may obtain a copy of the Licence at:
*
* https://joinup.ec.europa.eu/software/page/eupl5
*
* Unless required by applicable law or agreed to in
writing, software distributed under the Licence is
distributed on an "AS IS" basis,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
express or implied.
* See the Licence for the specific language governing
permissions and limitations under the Licence.
*/
?>


<?php

use dactylcore\core\db\Migration;


class M201120000001i18nTooltips extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->lang();
    }


    public function lang()
    {
        $translations = [
            'cs' => [
                'passive interference' => 'NS ruší:',
                'active interference' => 'NS je rušena:',
                'no' => 'ne',

                'passive not interference' => 'Nově instalovaná stanice neruší tuto stanici',
                'passive not interference with {reserve}' => 'Nově instalovaná stanice neruší tuto stanici s rezervou {reserve} dB',
                'passive interference by {amount}' => 'Nově instalovaná stanice ruší tuto stanicí o {amount} dB',

                'active not interference' => 'Nově instalovaná stanice není rušena touto stanicí',
                'active not interference with {reserve}' => 'Nově instalovaná stanice není rušena touto stanicí s rezervou {reserve} dB',
                'active interference by {amount}' => 'Nově instalovaná stanice je rušena touto stanicí o {amount} dB',

                'show station on map' => 'Zobrazit stanici na mapě'
            ],
            'en' => [
                'passive interference' => 'NS interferes:',
                'active interference' => 'NS is interfered:',
            ],
        ];

        $this->saveTranslation(static::CS_LANG_ID, 'frontend.station', $translations['cs']);
        $this->saveTranslation(static::EN_LANG_ID, 'frontend.station', $translations['en']);

        $this->saveConfig('MAPBOX_PROD', 'pk.eyJ1IjoidGVsZWtvbXVuaWthY2lyIiwiYSI6ImNraGV4ZXJucTBqd3Qycm52OGVvODg4bnMifQ.syvzO5tvvxoC7rdxZkTHGg', 'stations', 'stations', 0, 1);
        $this->saveConfig('MAPBOX_DEV', 'pk.eyJ1IjoidGVsZWtvbXVuaWthY2lyIiwiYSI6ImNraDM2NnpvZTEwcDIzMG8zaGpiZTk4OGkifQ.gl9El1mh9L7fHS31syhDfw', 'stations', 'stations', 0, 1);

        $translations = [
            'cs' => [
                'MAPBOX_PROD' => 'Mapbox token pro produkci',
            ],
            'en' => [
                'MAPBOX_DEV' => 'Mapbox token pro vyvoj',
            ]
        ];

        $this->saveTranslation(static::CS_LANG_ID, 'admin.config', $translations['cs']);
        $this->saveTranslation(static::EN_LANG_ID, 'admin.config', $translations['en']);

        $this->saveConfig('REGISTRATION_PERIOD_WARNING', 1, 'dc-system', 'stations', 0, 0);
        $this->saveConfig('PROTECTION_PERIOD_WARNING', 1, 'dc-system', 'stations', 0, 0);
    }
}
