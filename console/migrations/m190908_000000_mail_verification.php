<?php
/*
* Copyright 2023 DactylGroup s.r.o.
*
* Licensed under the EUPL, Version 1.2 or – as soon they
will be approved by the European Commission - subsequent
versions of the EUPL (the "Licence");
* You may not use this work except in compliance with the
Licence.
* You may obtain a copy of the Licence at:
*
* https://joinup.ec.europa.eu/software/page/eupl5
*
* Unless required by applicable law or agreed to in
writing, software distributed under the Licence is
distributed on an "AS IS" basis,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
express or implied.
* See the Licence for the specific language governing
permissions and limitations under the Licence.
*/
?>


<?php

use dactylcore\core\db\Migration;


/**
 * Class m190830_113326_i18n
 */
class m190908_000000_mail_verification extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->lang();
    }

    protected function lang()
    {
        $translations = [
            'cs' => [
                'subject' => 'ČTÚ - Potvrzení emailu',
            ],
            'en' => [
                'subject' => 'CTO - Email confirmation',
            ],
        ];

        $this->saveTranslation(static::CS_LANG_ID, 'common.email_verification_email', $translations['cs']);
        $this->saveTranslation(static::EN_LANG_ID, 'common.email_verification_email', $translations['en']);

        $translations = [
            'cs' => [
                'footer' => 'Český telekomunikační úřad',
            ],
            'en' => [
                'footer' => 'Czech Telecommunication Office',
            ],
        ];

        $this->saveTranslation(static::CS_LANG_ID, 'common.email', $translations['cs']);
        $this->saveTranslation(static::EN_LANG_ID, 'common.email', $translations['en']);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m190908_000000_mail_verification cannot be reverted.\n";

        return false;
    }

}
