<?php
/*
* Copyright 2023 DactylGroup s.r.o.
*
* Licensed under the EUPL, Version 1.2 or – as soon they
will be approved by the European Commission - subsequent
versions of the EUPL (the "Licence");
* You may not use this work except in compliance with the
Licence.
* You may obtain a copy of the Licence at:
*
* https://joinup.ec.europa.eu/software/page/eupl5
*
* Unless required by applicable law or agreed to in
writing, software distributed under the Licence is
distributed on an "AS IS" basis,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
express or implied.
* See the Licence for the specific language governing
permissions and limitations under the Licence.
*/
?>


<?php

use dactylcore\core\db\Migration;


class M220725131307UserApi extends Migration
{
    public function safeUp()
    {
        $this->struct();
        $this->conf();
        $this->i18n();
    }

    private function struct()
    {
        $this->alterColumn('user', 'street', $this->string(255)->null());
        $this->alterColumn('user', 'city', $this->string(255)->null());
        $this->alterColumn('user', 'zipCode', $this->string(255)->null());
    }

    private function i18n()
    {
        $this->saveTranslationCsEn('common.user-alliance', [
            'cannot_create_alliance_because_already_present' => [
                'Již jste v uživatelském sdružení, není možné vytvořit další.',
                'You are already in the user association, it is not possible to create another one.',
            ],
            'Only alliance member can update this alliance' => [
                'Tuto alianci může aktualizovat pouze člen aliance',
                'Only alliance member can update this alliance',
            ],
            'Only alliance admin can delete this alliance' => [
                'Tuto alianci může smazat pouze správce aliance',
                'Only alliance admin can delete this alliance',
            ],
            'Only alliance admin can update this alliance' => [
                'Tuto alianci může upravit pouze správce aliance',
                'Only alliance admin can update this alliance',
            ],
        ]);

        $this->saveTranslationCsEn('api.default', [
            'Data Validation Failed' => [
                'Ověření dat se nezdařilo',
                'Data Validation Failed',
            ],
        ]);

        $translations = [
            'cs' => [
                'password_must_contain' => 'Heslo musí obsahovat:',
                'lower_letters' => 'malá písmena',
                'capital_letters' => 'velká písmena',
                'digits' => 'číslice',
                'special_chars_{chars}' => 'speciální znaky {chars}',
                'length_{minLength}' => 'Minimální počet znaků: {minLength}',
            ],
            'en' => [
                'password_must_contain' => 'Password must contain:',
                'lower_letters' => 'lower letters',
                'capital_letters' => 'captital letters',
                'digits' => 'digits',
                'special_chars_{chars}' => 'special characters {chars}',
                'length_{minLength}' => 'Minimal length: {minLength}',
            ],
        ];
        $this->saveTranslation(static::CS_LANG_ID, 'api.password-format', $translations['cs']);
        $this->saveTranslation(static::EN_LANG_ID, 'api.password-format', $translations['en']);
    }

    private function conf()
    {
        $this->saveConfig('API_REGISTER_NAME_ATTRS', 1);
    }
}
