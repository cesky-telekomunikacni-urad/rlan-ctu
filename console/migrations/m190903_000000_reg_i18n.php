<?php
/*
* Copyright 2023 DactylGroup s.r.o.
*
* Licensed under the EUPL, Version 1.2 or – as soon they
will be approved by the European Commission - subsequent
versions of the EUPL (the "Licence");
* You may not use this work except in compliance with the
Licence.
* You may obtain a copy of the Licence at:
*
* https://joinup.ec.europa.eu/software/page/eupl5
*
* Unless required by applicable law or agreed to in
writing, software distributed under the Licence is
distributed on an "AS IS" basis,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
express or implied.
* See the Licence for the specific language governing
permissions and limitations under the Licence.
*/
?>


<?php

use dactylcore\core\db\Migration;


/**
 * Class m190830_113326_i18n
 */
class m190903_000000_reg_i18N extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $translations = [
            'cs' => [
                'companyName' => 'název firmy',
                'city' => 'město',
                'zipCode' =>  'PSČ',
                'street' => 'ulice',
                'taxNumber' => 'DIČ',
                'vatNumber' => 'IČ',
                'additionalInfo' => 'doplňující info',
                'conditionsConfirmed' => 'Souhlasím s <a href="https://www.ctu.cz/podminky-uziti" target="_blank">podmínkami použití</a>',
                'register_submit_button' => 'registrovat',
                'register user title' => 'Registrace nového uživatele',
                'register company title' => 'Registrace nového uživatele',
                'register user info' => 'Kontaktní údaje o fyzické osobě',
                'register company info' => 'kontaktní údaje o právnické osobě',
                'login title' => 'Přihlášení',
                'login_sub_heading' => '(pro registrované uživatele)',
                'credentials' => 'Přihlašovací údaje:',
                'reset_password_button' => 'Obnovení hesla',
                'login_submit_button' => 'Přihlásit se',
                'register_big_button' => 'registrace',
                'forgotten_password' => 'Obnovení hesla',
                'set_mail_to_reset' => 'Zadejte svoji e-mailovou adresu',
                'back_to_login_button' => 'Zpět na přihlášení',
                'request_pass_submit_button' => 'Odeslat',
                'select user type' => 'Registrace nového uživatele',
                'select user type subtitle' => 'Dotaz na právní subjektivitu',
                'company-type' => 'právnická osoba',
                'user-type' => 'fyzická osoba',
                'register_captcha_error' => 'Je nutné ověřit vaši lidskost',
                'unchecked_conditions' => 'Je nutné souhlasit s podmínkami použití',
                'save' => 'Uložit',
                'profile' => 'Profil uživatele',
                'Are you sure you want to delete your account?' => 'Jste si jist, že si chcete smazat profil? Tato operace je nevratná',
                'delete profile' => 'Vymazat profil',
                'flash_successfully_deleted' => 'Uživatel smazán'
            ],
            'en' => [
                'companyName' => 'company name',
                'city' => 'city',
                'zipCode' =>  'ZIP',
                'street' => 'street',
                'taxNumber' => 'TAX ID',
                'vatNumber' => 'VAT ID',
                'additionalInfo' => 'additional info',
                'register_submit_button' => 'register',
                'conditionsConfirmed' => 'I agree with <a href="https://www.ctu.eu/terms-use" target="_blank">terms of use</a>',
                'register user title' => 'New user registration',
                'register company title' => 'New company registration',
                'register user info' => 'Information about user',
                'register company info' => 'Company information',
                'login title' => 'Login',
                'login_sub_heading' => '(for registered users)',
                'credentials' => 'Credentials:',
                'reset_password_button' => 'Password recovery',
                'login_submit_button' => 'Log in',
                'register_big_button' => 'Registration',
                'forgotten_password' =>  'Password recovery',
                'set_mail_to_reset' => 'Fill your email address',
                'back_to_login_button' => 'Back to login',
                'request_pass_submit_button' => 'Submit',
                'select user type' => 'New user registration',
                'select user type subtitle' => 'Select user type',
                'company-type' => 'company',
                'user-type' => 'user',
                'unchecked_conditions' => 'You have to agree with term and conditions',
                'save' => 'Save',
                'profile' => 'Profile',
                'Are you sure you want to delete your account?' => 'Are you sure?',
                'delete profile' => 'Delete your profile',
                'flash_successfully_deleted' => 'Profile deleted'

            ],
        ];

        $this->saveTranslation(static::CS_LANG_ID, 'common.register', $translations['cs']);
        $this->saveTranslation(static::EN_LANG_ID, 'common.register', $translations['en']);

        $translationsHints = [
            'cs' => [
                'email' => 'bude použit jako přihlašovací jméno',
                'additionalInfo' => 'doplňující informace o registraci',
            ],
            'en' => [
                'email' => 'will be used as a login',
                'additionalInfo' => 'additional information about your registration',
            ],
        ];

        $this->saveTranslation(static::CS_LANG_ID, 'frontend.register_hint', $translationsHints['cs']);
        $this->saveTranslation(static::EN_LANG_ID, 'frontend.register_hint', $translationsHints['en']);

        $translationsHints = [
            'cs' => [
                'first_name' => 'Jan',
                'last_name' => 'Novák',
                'companyName' => '',
                'vatNumber' => '',
                'taxNumber' => '',
                'city' => 'Brno',
                'zipCode' => '12300',
                'street' => 'Slovákova 35',
                'email_placeholder' => 'jan.novak@example.com',
            ],
            'en' => [
                'first_name' => 'John',
                'last_name' => 'Doe',
                'companyName' => '',
                'vatNumber' => '',
                'taxNumber' => '',
                'city' => 'Brno',
                'zipCode' => '12300',
                'street' => 'Main street 35',
                'email_placeholder' => 'john.doe@example.com',

            ],
        ];

        $this->saveTranslation(static::CS_LANG_ID, 'common.register_placeholder', $translationsHints['cs']);
        $this->saveTranslation(static::EN_LANG_ID, 'common.register_placeholder', $translationsHints['en']);

        $translationsHints = [
            'cs' => [
                'select-user-type' => 'typ-registrace',
                'register-company' => 'registrace-firmy',
            ],
            'en' => [
                'select-user-type' => 'registration-type',
                'register-company' => 'company-registration',
            ],
        ];

        $this->saveTranslation(static::CS_LANG_ID, 'frontend.url_rules', $translationsHints['cs']);
        $this->saveTranslation(static::EN_LANG_ID, 'frontend.url_rules', $translationsHints['en']);
        $translationsHints = [
            'cs' => [
                'create_company' => 'Vytvořit firmu',
                'user' => 'Uživatel',
                'company' => 'Firma',
                'type' => 'Typ',
            ],
            'en' => [
                'create_company' => 'Create a company',
                'user' => 'User',
                'company' => 'Company',
                'type' => 'Type',
            ],
        ];

        $this->saveTranslation(static::CS_LANG_ID, 'admin.user', $translationsHints['cs']);
        $this->saveTranslation(static::EN_LANG_ID, 'admin.user', $translationsHints['en']);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m190830_113326_i18n cannot be reverted.\n";

        return false;
    }

}
