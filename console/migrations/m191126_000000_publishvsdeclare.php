<?php
/*
* Copyright 2023 DactylGroup s.r.o.
*
* Licensed under the EUPL, Version 1.2 or – as soon they
will be approved by the European Commission - subsequent
versions of the EUPL (the "Licence");
* You may not use this work except in compliance with the
Licence.
* You may obtain a copy of the Licence at:
*
* https://joinup.ec.europa.eu/software/page/eupl5
*
* Unless required by applicable law or agreed to in
writing, software distributed under the Licence is
distributed on an "AS IS" basis,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
express or implied.
* See the Licence for the specific language governing
permissions and limitations under the Licence.
*/
?>


<?php

use dactylcore\core\db\Migration;


/**
 * Class m190830_113326_i18n
 */
class m191126_000000_publishvsdeclare extends Migration
{
    public function safeUp()
    {
        $this->struct();
        $this->lang();
    }

    protected function struct()
    {
        $this->addColumn('station', 'published_by', $this->string(25)->defaultValue(''));
    }

    protected function lang()
    {
        $translations = [
            'cs' => [
                'published_by' => 'Způsob publikování',
                'publication' => 'publikace',
                'declaration' => 'deklarace',

                'publication_help' => 'uživatel stanici publikoval bez konfliktu',
                'declaration_help' => 'uživatel deklaroval, že nalezený konflikt je řešen jiným technickým způsobem',
            ],
            'en' => [
                'published_by' => 'Way of publication',
                'publication' => 'publication',
                'declaration' => 'declaration',

                'publication_help' => 'user published station without any conflict',
                'declaration_help' => 'user declared that found conflicts are solved by other technical solutions',
            ]
        ];

        $this->saveTranslation(static::CS_LANG_ID, 'common.station', $translations['cs']);
        $this->saveTranslation(static::EN_LANG_ID, 'common.station', $translations['en']);
    }
}
