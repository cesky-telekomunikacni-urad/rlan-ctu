<?php
/*
* Copyright 2023 DactylGroup s.r.o.
*
* Licensed under the EUPL, Version 1.2 or – as soon they
will be approved by the European Commission - subsequent
versions of the EUPL (the "Licence");
* You may not use this work except in compliance with the
Licence.
* You may obtain a copy of the Licence at:
*
* https://joinup.ec.europa.eu/software/page/eupl5
*
* Unless required by applicable law or agreed to in
writing, software distributed under the Licence is
distributed on an "AS IS" basis,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
express or implied.
* See the Licence for the specific language governing
permissions and limitations under the Licence.
*/
?>


<?php

use dactylcore\core\db\Migration;


class m200728_000000_chat_lang extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->lang();
        $this->langURL();
    }

    public function lang()
    {
        $translations = [
            'cs' => [
                'conversations' => 'Konverzace',
                'conversations summary' => 'Zobrazeno {begin} - {end} z {totalCount} konverzací',
                'message read' => 'Zpráva přečtena příjemcem',
                'send message' => 'Odeslat zprávu',
                'something went wrong' => 'Něco se rozbilo, zkuste znova',
                'no messages in this thread' => 'Tato konverzace neobsahuje zatím žádné správy.'
            ],
            'en' => [
                'conversations' => 'Conversations',
                'conversations summary' => 'Showing {begin} - {end} of {totalCount} conversations',
                'message read' => 'Message read by recipient',
                'send message' => 'Send message',
                'something went wrong' => 'Something went wrong, try again',
                'no messages in this thread' => 'This thread contains no messages yet.'
            ],
        ];

        $this->saveTranslation(static::CS_LANG_ID, 'frontend.chat', $translations['cs']);
        $this->saveTranslation(static::EN_LANG_ID, 'frontend.chat', $translations['en']);
    }

    public function langURL()
    {
        $translations = [
            'cs' => [
                'conversations' => 'konverzace',
                'conversations/thread' => 'konverzace/diskuze',
                'conversations/generic-question' => 'conversations/otazka',
                'conversations/disturbance-question' => 'conversations/konflikt',
            ],
            'en' => [
                'conversations' => 'conversations',
                'conversations/thread' => 'conversations/thread',
                'conversations/generic-question' => 'conversations/question',
                'conversations/disturbance-question' => 'conversations/conflict',
            ],
        ];

        $this->saveTranslation(static::CS_LANG_ID, 'frontend.url', $translations['cs']);
        $this->saveTranslation(static::EN_LANG_ID, 'frontend.url', $translations['en']);
    }
}
