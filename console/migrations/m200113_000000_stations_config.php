<?php
/*
* Copyright 2023 DactylGroup s.r.o.
*
* Licensed under the EUPL, Version 1.2 or – as soon they
will be approved by the European Commission - subsequent
versions of the EUPL (the "Licence");
* You may not use this work except in compliance with the
Licence.
* You may obtain a copy of the Licence at:
*
* https://joinup.ec.europa.eu/software/page/eupl5
*
* Unless required by applicable law or agreed to in
writing, software distributed under the Licence is
distributed on an "AS IS" basis,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
express or implied.
* See the Licence for the specific language governing
permissions and limitations under the Licence.
*/
?>


<?php

use dactylcore\core\db\Migration;


/**
 * Class m200113_000000_crud_help
 */
class m200113_000000_stations_config extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->config();
    }

    public function config()
    {
        $this->saveConfig('REGISTRATION_PERIOD', 12, 'stations', 'periods', 0, 1);
        $this->saveConfig('REGISTRATION_PERIOD_WARNING', 2, 'stations', 'periods', 0, 1);
        $this->saveConfig('PROTECTION_PERIOD', 18, 'stations', 'periods', 0, 1);
        $this->saveConfig('PROTECTION_PERIOD_WARNING', 2, 'stations', 'periods', 0, 1);

        $this->saveConfig('FRONT_TO_BACK_RATIO_HIGH', 25, 'stations', 'stations', 0, 1);
        $this->saveConfig('FRONT_TO_BACK_RATIO_LOW', 15, 'stations', 'stations', 0, 1);
        $this->saveConfig('PTMP_THRESHOLD', 25, 'stations', 'stations', 0, 1);

        $this->saveConfig('MAX_WAITING_STATIONS', 5, 'stations', 'stations', 0, 1);


        $translations = [
            'cs' => [
                'stations' => 'Stanice',
                'periods' => 'Časové lhůty',
            ],
            'en' => [
            ]
        ];


        $this->saveTranslation(static::CS_LANG_ID, 'admin.config', $translations['cs']);
        $this->saveTranslation(static::EN_LANG_ID, 'admin.config', $translations['en']);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m200113_000000_crud_help cannot be reverted.\n";

        return false;
    }

}
