<?php
/*
* Copyright 2023 DactylGroup s.r.o.
*
* Licensed under the EUPL, Version 1.2 or – as soon they
will be approved by the European Commission - subsequent
versions of the EUPL (the "Licence");
* You may not use this work except in compliance with the
Licence.
* You may obtain a copy of the Licence at:
*
* https://joinup.ec.europa.eu/software/page/eupl5
*
* Unless required by applicable law or agreed to in
writing, software distributed under the Licence is
distributed on an "AS IS" basis,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
express or implied.
* See the Licence for the specific language governing
permissions and limitations under the Licence.
*/
?>


<?php

use dactylcore\core\db\Migration;


/**
 * Class m190830_113326_i18n
 */
class m191210_000001_results extends Migration
{
    public function safeUp()
    {
        $this->lang();
    }


    protected function lang()
    {


        $translations = [
            'cs' => [
                'results_station_name' => 'Pozice FS stanice',
                'station a' => 'Stanice A',
                'station b' => 'Stanice B',
                'show info' => 'Zobrazit hodnoty',
                'hide info' => 'Skrýt hodnoty',
                'show all info' => 'Zobrazit všechny hodnoty',
                'hide all info' => 'Skrýt všechny hodnoty',
                'typeShortCut' => 'Typ',
                'my station' => 'moje stanice',
            ],
            'en' => [
                'results_station_name' => 'FS station position',
                'station a' => 'Station A',
                'station b' => 'Station B',
                'show info' => 'Show values',
                'hide info' => 'Hide values',
                'show all info' => 'Show all values',
                'hide all info' => 'Hide all values',
                'typeShortCut' => 'Type',
                'my station' => 'my station',
            ]
        ];

        $this->saveTranslation(static::CS_LANG_ID, 'frontend.station', $translations['cs']);
        $this->saveTranslation(static::EN_LANG_ID, 'frontend.station', $translations['en']);
    }
}