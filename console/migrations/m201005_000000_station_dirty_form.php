<?php
/*
* Copyright 2023 DactylGroup s.r.o.
*
* Licensed under the EUPL, Version 1.2 or – as soon they
will be approved by the European Commission - subsequent
versions of the EUPL (the "Licence");
* You may not use this work except in compliance with the
Licence.
* You may obtain a copy of the Licence at:
*
* https://joinup.ec.europa.eu/software/page/eupl5
*
* Unless required by applicable law or agreed to in
writing, software distributed under the Licence is
distributed on an "AS IS" basis,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
express or implied.
* See the Licence for the specific language governing
permissions and limitations under the Licence.
*/
?>


<?php

use dactylcore\core\db\Migration;


class m201005_000000_station_dirty_form extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->lang();
    }

    protected function lang()
    {
        $translations = [
            'cs' => [
                'continue' => 'Pokračovat',
                'cancel' => 'Zrušit',
                'This will un-publish your station' => 'Tato akce zneplatní Vaši stanici, pokračovat?',
            ],
            'en' => [
                'continue' => 'Continue',
                'cancel' => 'Cancel',
                'This will un-publish your station' => 'This action will un-publish Your station, continue?',
            ],
        ];

        $this->saveTranslation(static::CS_LANG_ID, 'frontend.dirty-form', $translations['cs']);
        $this->saveTranslation(static::EN_LANG_ID, 'frontend.dirty-form', $translations['en']);
    }
}
