<?php
/*
* Copyright 2023 DactylGroup s.r.o.
*
* Licensed under the EUPL, Version 1.2 or – as soon they
will be approved by the European Commission - subsequent
versions of the EUPL (the "Licence");
* You may not use this work except in compliance with the
Licence.
* You may obtain a copy of the Licence at:
*
* https://joinup.ec.europa.eu/software/page/eupl5
*
* Unless required by applicable law or agreed to in
writing, software distributed under the Licence is
distributed on an "AS IS" basis,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
express or implied.
* See the Licence for the specific language governing
permissions and limitations under the Licence.
*/
?>


<?php

use dactylcore\core\db\Migration;


class m200715_000001_station_registration extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->alterColumn('station', 'lng', $this->decimal(16,14)->null());
        $this->alterColumn('station', 'lat', $this->decimal(16,14)->null());

        $this->lang();
        $this->wigigLang();
        $this->fsLang();
        $this->stationLang();
        $this->stationHint();
        $this->urlLang();
    }

    public function lang()
    {
        $translations = [
            'cs' => [
                'create' => 'Vytvořit',
                '60ghz' => '60 GHz Stanice',
                'add fs' => 'Přidat novou FS PtP stanici',
                'import fs' => 'Importovat FS PtP stanici',
                'add wigig' => 'Přidat novou WiGig PtP alebo PtMP stanici',
                'import wigig' => 'Importovat WiGig PtP alebo PtMP stanici',
                'access point' => 'Přístupový bod',
                'add ap' => 'Přidat nový přístupový bod',
                'import ap' => 'Importovat přístupový bod',
                'toll gate' => 'Mýtna brána',
                'add tg' => 'Přidat novou mýtnou bránu',
                'import tg' => 'Importovat mýtnou bránu',
                '52ghz' => '5.2 GHz Stanice',
                'add 52' => 'Přidat novou 5.2 GHz stanici',
                'import 52' => 'Importovat 5.2 GHz stanici',
                'other' => 'Jiné',
                'add probe' => 'Přidat novou sondu',
                'concept' => 'Koncept',
                'next step' => 'Pokračovať',
                'name' => 'Jméno',
                'location' => 'Poloha',
                'parameters' => 'Parametry',
                'summary' => 'Shrnutí',
                'continue' => 'Pokračovat',
                'save and continue' => 'Uložit a pokračovat',
                'publish' => 'Publikovat',
                'back' => 'Zpět',
                'edit' => 'Upravit',
                'information' => 'Informace',
                'pending' => 'Čeká',
                'active' => 'Aktivní',
                'expired' => 'Expirováno',
                'flash_successfully_created' => 'Stanice úspěšně vytvořena',
                'flash_successfully_updated' => 'Stanice úspěšně aktualizovaná',
                'unpublished limit reached tooltip' => 'Dosáhli jste limitu nepublikovaných stanic. Pro přidání další stanice publikujete nebo odstraňte čekající stanice.'
            ],
            'en' => [
                'create' => 'Create',
                '60ghz' => '60 GHz Station',
                'add fs' => 'Add new FS PtP Station',
                'import fs' => 'Import FS PtP Station',
                'add wigig' => 'Add new WiGig PtP or PtMP Station',
                'import wigig' => 'Import WiGig PtP or PtMP Station',
                'access point' => 'Access Point',
                'add ap' => 'Add new Access Point',
                'import ap' => 'Import Access Points',
                'toll gate' => 'Toll Gate',
                'add tg' => 'Add new Toll Gate',
                'import tg' => 'Import Toll Gate',
                '52ghz' => '5.2 GHz Station',
                'add 52' => 'Add new 5.2 GHz Station',
                'import 52' => 'Import 5.2 GHz Station',
                'other' => 'Other',
                'add probe' => 'Add new probe',
                'concept' => 'Concept',
                'next step' => 'Continue',
                'name' => 'Name',
                'location' => 'Location',
                'parameters' => 'Parameters',
                'summary' => 'Summary',
                'continue' => 'Continue',
                'save and continue' => 'Save and continue',
                'publish' => 'publish',
                'back' => 'Back',
                'edit' => 'Edit',
                'information' => 'Information',
                'pending' => 'Pending',
                'active' => 'Active',
                'expired' => 'Expired',
                'save and exit' => 'Save and exit',
                'flash_successfully_created' => 'Station successfully created',
                'flash_successfully_updated' => 'Station successfully updated',
                'unpublished limit reached tooltip' => 'You reached limit of unpublished stations. To add more stations you need to publish or remove waiting stations',
            ],
        ];

        $this->saveTranslation(static::CS_LANG_ID, 'frontend.station', $translations['cs']);
        $this->saveTranslation(static::EN_LANG_ID, 'frontend.station', $translations['en']);
    }

    public function wigigLang()
    {
        $translations = [
            'cs' => [
                'untitled station' => 'Nepojmenovaná stanice',
                'What is station name' => 'Jaké je jméno stanice ?',
                'Invalid format of name' => 'Můžete použít jenom alfanumerické znaky a @, #, $, &, -. +, /.',
                'Where is the station located' => 'Kde je stanice umístněna ?',
                'Use map to select location or enter GPS coordinates' => 'Použijte mapu pro volbu umístnění nebo zadejte GPS souřadnice.',
                'Use map to select direction' => 'Použijte mapu pro zadání hlavního směru záření.',
                'Do you know antenna gain and power or EIRP' => 'Znáte zisk a sílu anteny nebo EIRP?',
                'automatic calculation' => 'Automatický přepočet',
                'EIRP' => 'EIRP',
                'What is the channel bandwidth' => 'Jaká je šířka pásma?',
                'Which of these identifiers do you know' => 'Který z nasledujících identifikátorů znáte?',
                'i know mac' => 'MAC adresa',
                'i know serial number' => 'Sériové číslo',
                'Where is the station directed' => 'Kam stanice směruje?',
                'select direction' => 'Zvolit směr vyzařování',
            ],
            'en' => [
                'untitled station' => 'Untitled station',
                'What is station name' => 'What is the name of the Station?',
                'Invalid format of name' => 'You can only use alphanumeric characters and @, #, $, &, -, +, /.',
                'Where is the station located' => 'Where is the station located?',
                'Use map to select location or enter GPS coordinates' => 'Use the map to select the location or enter the GPS coordinates.',
                'Use map to select direction' => 'Use the map to select the direction.',
                'Do you know antenna gain and power or EIRP' => 'Do you know antenna gain and power or EIRP?',
                'automatic calculation' => 'Automatic calculation',
                'EIRP' => 'EIRP',
                'What is the channel bandwidth' => 'What is the channel bandwidth?',
                'Which of these identifiers do you know' => 'Which of these identifiers do you know?',
                'i know mac' => 'MAC address',
                'i know serial number' => 'Serial number',
                'Where is the station directed' => 'Where is the station directed?',
                'select direction' => 'Select direction',
            ],
        ];

        $this->saveTranslation(static::CS_LANG_ID, 'frontend.station', $translations['cs']);
        $this->saveTranslation(static::EN_LANG_ID, 'frontend.station', $translations['en']);
    }

    public function fsLang()
    {
        $translations = [
            'cs' => [
                'Where are stations located' => 'Kde jsou stanice umístěny?',
                'Where is the station A located' => 'Kde je stanice A umístěna?',
                'add station B' => 'Přidat stanici B',
                'Where is the station B located' => 'Kde je stanice B umístěna?',
                'Station A' => 'Stanice A',
                'Station B' => 'Stanice B',
                'What is antenna gain and power' => 'Jaký je zisk a síla antény?',
                'What is the medium frequency' => 'Jaká je hodnota středního kmitočtu?',
                'What is the desired signal to interference ratio?' => 'Jaký je požadovaný poměr signálu k rušení?',
                'frequency_label' => 'Středný kmitočet',
                'ratio_signal_interference_label' => 'Modulace a požadavek C/I',
                'power_label' => 'Přivedený výkon',
                'eirp_label' => 'EIRP',
                'lat_a' => 'Zeměpisná šířka A',
                'lng_a' => 'Zeměpisná délka A',
                'lat_b' => 'Zeměpisná šířka B',
                'lng_b' => 'Zeměpisná délka B',
            ],
            'en' => [
                'Where are stations located' => 'Where are stations located?',
                'Where is the station A located' => 'Where is the station A located?',
                'add station B' => 'Add station B',
                'Where is the station B located' => 'Where is the station B located?',
                'Station A' => 'Station A',
                'Station B' => 'Station B',
                'What is antenna gain and power' => 'What is the antenna gain and power?',
                'What is the medium frequency' => 'What is the medium frequency?',
                'What is the desired signal to interference ratio?' => 'What is the desired signal to interference ratio?',
                'frequency_label' => 'center frequency',
                'ratio_signal_interference_label' => 'Signal / interference ratio',
                'power_label' => 'Power',
                'eirp_label' => 'EIRP',

                'lat_a' => 'latitude A',
                'lng_a' => 'longitude A',
                'lat_b' => 'latitude B',
                'lng_b' => 'longitude B',

            ],
        ];

        $this->saveTranslation(static::CS_LANG_ID, 'frontend.station', $translations['cs']);
        $this->saveTranslation(static::EN_LANG_ID, 'frontend.station', $translations['en']);
    }

    public function stationLang()
    {
        $translations = [
            'cs' => [
                'lng' => 'Zeměpisná délka',
                'lat' => 'Zeměpisná šířka',
                'direction' => 'Hlavní směr vyzařovaní',
                'latitude has to be in czechia' => 'Zeměpisná šířka se musí nacházet na územi Česka',
                'longitude has to be in czechia' => 'Zeměpisná délka se musí nacházet na územi Česka',
                'antenna power cant be negative' => 'Výkon antény nesmí být záporný.',
                'antenna gain has to be in range 30 to 60' => 'Zadaný zisk antény musí být v intervalu 30-60 dBi.',
                'Invalid MAC format' => 'MAC adresa musí být v správnem formátu, napr. 01:23:45:67:89:AB',
                'station type fs' => 'FS PtP',
                'station type wigig ptp' => 'WiGig PtP',
                'station type wigig ptmp' => 'WiGig PtMP',
                'antenna gain' => 'Zisk antény',
                'bandwidth' => 'Šířka pásma',
                'affected stations' => 'Ovlivněné stanice',
                'Coordinator calculator table heading' => 'Koordinační kalkulačka pro kontrolu potenciálních konfliktů s okolitými stanicem.',
                'kind' => 'Druh',
                'isolation declaration' => 'Prohlášení o izolaci',
                'declare' => 'Prohlásit izolaci',
                'coordination calculator summary' => 'Zobrazeno {begin} - {end} z {totalCount} položek',
            ],
            'en' => [
                'lng' => 'Longitude',
                'lat' => 'Latitude',
                'direction' => 'Direction of radiation',
                'latitude has to be in czechia' => 'Latitude has to be in Czechia',
                'longitude has to be in czechia' => 'Longitude has to be in Czechia',
                'antenna power cant be negative' => 'The applied power cannot be negative.',
                'antenna gain has to be in range 30 to 60' => 'The gain of the antenna must be between 0 dBi and 60 dBi.',
                'Invalid MAC format' => 'The MAC address must have the correct form, e.g. 01:23:45:67:89:AB',
                'station type fs' => 'FS PtP',
                'station type wigig ptp' => 'WiGig PtP',
                'station type wigig ptmp' => 'WiGig PtMP',
                'antenna gain' => 'Antenna gain',
                'bandwidth' => 'Bandwidth',
                'affected stations' => 'Affected station',
                'Coordinator calculator table heading' => 'The coordination calculator checked for potential conflicts with nearby stations.',
                'kind' => 'Kind',
                'isolation declaration' => 'Isolation declaration',
                'declare' => 'Declare isolation',
                'coordination calculator summary' => 'Showing {begin} - {end} of {totalCount} items'
            ],
        ];

        $this->saveTranslation(static::CS_LANG_ID, 'frontend.station', $translations['cs']);
        $this->saveTranslation(static::EN_LANG_ID, 'frontend.station', $translations['en']);
    }

    public function stationHint()
    {
        $translations = [
            'cs' => [
                'antenna_volume' => 'Nad 25 dBi = WiGig PtP, pod 25 dBi = WiGiG PtMP',
                'power' => 'RF síla do antény, přenosový výkon, přivedený výkon',
                'eirp' => 'EIRP [dBm] = zisk antény [dBi] + síla [dBm]',
                'ratio_signal_interference' => 'Modulace a požadavek C/I.',
            ],
            'en' => [
                'antenna_volume' => 'Above 25 dBi = WiGig PtP, below 25 dBi = WiGig PtMP',
                'power' => 'RF injection power into antenna, transmission power, conducted power',
                'eirp' => 'EIRP [dBm] = antenna gain [dBi] + power [dBm]',
                'ratio_signal_interference' => 'Ratio signal / interference',
            ],
        ];

        $this->saveTranslation(static::CS_LANG_ID, 'frontend.station_hint', $translations['cs']);
        $this->saveTranslation(static::EN_LANG_ID, 'frontend.station_hint', $translations['en']);
    }

    public function urlLang()
    {
        $translations = [
            'cs' => [
                'station/create_fs' => 'stanice/vytvorit_fs',
                'station/create_wigig' => 'stanice/vytvorit_wigig',
                'station/edit' => 'stanice/upravit',
                'station/publish' => 'stanice/publikovat',
                'station/declaration' => 'stanice/deklarace',
                'station/declare' => 'stanice/deklarovat',
            ],
            'en' => [
                'station/create_fs' => 'station/create_fs',
                'station/create_wigig' => 'station/create_wigig',
                'station/edit' => 'station/edit',
                'station/publish' => 'station/publish',
                'station/declaration' => 'station/declaration',
                'station/declare' => 'station/declare',
            ],
        ];

        $this->saveTranslation(static::CS_LANG_ID, 'frontend.url', $translations['cs']);
        $this->saveTranslation(static::EN_LANG_ID, 'frontend.url', $translations['en']);
    }
}
