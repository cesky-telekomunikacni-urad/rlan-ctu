<?php
/*
* Copyright 2023 DactylGroup s.r.o.
*
* Licensed under the EUPL, Version 1.2 or – as soon they
will be approved by the European Commission - subsequent
versions of the EUPL (the "Licence");
* You may not use this work except in compliance with the
Licence.
* You may obtain a copy of the Licence at:
*
* https://joinup.ec.europa.eu/software/page/eupl5
*
* Unless required by applicable law or agreed to in
writing, software distributed under the Licence is
distributed on an "AS IS" basis,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
express or implied.
* See the Licence for the specific language governing
permissions and limitations under the Licence.
*/
?>


<?php

use common\models\Station;
use dactylcore\core\db\Migration;
use frontend\controllers\StationController;


class m200820_000000_wigig_angles_data extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $geotools = new \League\Geotools\Geotools();
        $stations = Station::find()->where(['type' => 'wigig'])->with('stationWigig');
        foreach ($stations->each() as $station) {
            /**
             * @var $station Station
             * @var $wigig \common\models\StationWigig
             */
            $wigig = $station->getStationTypeObject();
            $coordA = new \League\Geotools\Coordinate\Coordinate([$station->lat, $station->lng]);
            $destinationPoint = $geotools->vertex()->setFrom($coordA)->destination($wigig->direction, 50000);
            $wigig->setScenario(StationController::STEP_2_LOCATION);
            $wigig->angle_point_lat = $destinationPoint->getLatitude();
            $wigig->angle_point_lng = $destinationPoint->getLongitude();
            $wigig->save();
        }
    }

}
