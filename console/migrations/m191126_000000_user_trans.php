<?php
/*
* Copyright 2023 DactylGroup s.r.o.
*
* Licensed under the EUPL, Version 1.2 or – as soon they
will be approved by the European Commission - subsequent
versions of the EUPL (the "Licence");
* You may not use this work except in compliance with the
Licence.
* You may obtain a copy of the Licence at:
*
* https://joinup.ec.europa.eu/software/page/eupl5
*
* Unless required by applicable law or agreed to in
writing, software distributed under the Licence is
distributed on an "AS IS" basis,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
express or implied.
* See the Licence for the specific language governing
permissions and limitations under the Licence.
*/
?>


<?php

use dactylcore\core\db\Migration;


/**
 * Class m190830_113326_i18n
 */
class m191126_000000_user_trans extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->lang();
    }


    protected function lang()
    {

        $translations = [
            'cs' => [
                'successfully registered' => 'Registrace úspěšná. Zkontrolujte Vaši emailovou schránku (včetně SPAMu) a potvrďte váš účet',
                'user_email_not_verified' => 'Uživatelský účet nebyl potvrzen emailem. Zkontrolujte Vaši emailovou schránku (včetně SPAMu) a potvrďte váš účet.',
            ],
            'en' => [
                'successfully registered' => 'Registration was successful. Please check your email mailbox (including SPAM) and confirm your account.',
                'user_email_not_verified' => 'User account was not confirmed by email. Please check your email mailbox (including SPAM) and confirm your account.',
            ],
        ];

        $this->saveTranslation(static::CS_LANG_ID, 'common.user', $translations['cs']);
        $this->saveTranslation(static::EN_LANG_ID, 'common.user', $translations['en']);

    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m190830_113326_i18n cannot be reverted.\n";

        return false;
    }

}
