<?php
/*
* Copyright 2023 DactylGroup s.r.o.
*
* Licensed under the EUPL, Version 1.2 or – as soon they
will be approved by the European Commission - subsequent
versions of the EUPL (the "Licence");
* You may not use this work except in compliance with the
Licence.
* You may obtain a copy of the Licence at:
*
* https://joinup.ec.europa.eu/software/page/eupl5
*
* Unless required by applicable law or agreed to in
writing, software distributed under the Licence is
distributed on an "AS IS" basis,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
express or implied.
* See the Licence for the specific language governing
permissions and limitations under the Licence.
*/
?>


<?php

use dactylcore\core\db\Migration;


class m200730_000000_station_registration_lang extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->urlLang();
        $this->direction();
    }

    public function urlLang()
    {
        $translations = [
            'cs' => [
                'station/create_fs' => 'stanice/vytvorit-fs',
                'station/create_wigig' => 'stanice/vytvorit-wigig',
            ],
            'en' => [
                'station/create_fs' => 'station/create-fs',
                'station/create_wigig' => 'station/create-wigig',
            ],
        ];

        $this->saveTranslation(static::CS_LANG_ID, 'frontend.url', $translations['cs']);
        $this->saveTranslation(static::EN_LANG_ID, 'frontend.url', $translations['en']);
    }

    protected function direction()
    {
        $this->alterColumn('station_wigig', 'direction', $this->integer(3)->defaultValue(0));
    }
}
