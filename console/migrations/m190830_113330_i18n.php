<?php
/*
* Copyright 2023 DactylGroup s.r.o.
*
* Licensed under the EUPL, Version 1.2 or – as soon they
will be approved by the European Commission - subsequent
versions of the EUPL (the "Licence");
* You may not use this work except in compliance with the
Licence.
* You may obtain a copy of the Licence at:
*
* https://joinup.ec.europa.eu/software/page/eupl5
*
* Unless required by applicable law or agreed to in
writing, software distributed under the Licence is
distributed on an "AS IS" basis,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
express or implied.
* See the Licence for the specific language governing
permissions and limitations under the Licence.
*/
?>


<?php

use dactylcore\core\db\Migration;


/**
 * Class m190830_113326_i18n
 */
class m190830_113330_i18N extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $translations = [
            'cs' => [
                'company_name' => 'Český telekomunikační úřad',
                'header_text' => 'Portál k evidenci stanic v pásmu 60 GHz ',
                'header_title' => 'Portál k evidenci stanic v pásmu 60 GHz ',
            ],
            'en' => [
                'company_name' => 'Czech Telecommunication Office',
                'header_text' => 'Portal for evidence of devices in zone 60 GHz ',
                'header_title' => 'Portal for evidence of devices in zone 60 GHz ',],
        ];

        $this->saveTranslation(static::CS_LANG_ID, 'frontend.header', $translations['cs']);
        $this->saveTranslation(static::EN_LANG_ID, 'frontend.header', $translations['en']);
        $translations = [
            'cs' => [
                'copyright' => '© Český telekomunikační úřad',
            ],
            'en' => [
                'copyright' => '© Czech Telecommunication Office',
            ],
        ];

        $this->saveTranslation(static::CS_LANG_ID, 'frontend.footer', $translations['cs']);
        $this->saveTranslation(static::EN_LANG_ID, 'frontend.footer', $translations['en']);

        $translations = [
            'cs' => [
                'public-header' => 'Horní menu veřejné',
                'protected-header' => 'Horní menu chráněné',
            ],
            'en' => [
                'public-header' => 'Top public menu',
                'protected-header' => 'Top protected menu',
            ],
        ];

        $this->saveTranslation(static::CS_LANG_ID, 'common.menu', $translations['cs']);
        $this->saveTranslation(static::EN_LANG_ID, 'common.menu', $translations['en']);

        $translations = [
            'cs' => [
                'title' => 'Došlo k chybě',
                'forbidden' => 'Nemáte opravnění pro tuto akci',
                'unknown error' => 'Došlo k neznámé chybě',
                'page not found' => 'Stránka nenalezena',
                'user not found' => 'Uživatel nenalezen',
            ],
            'en' => [
                'title' => 'Something went wrong',
                'forbidden' => 'You do not have permission for this operation',
                'unknown error' => 'There is an unknown error',
                'page not found' => 'Page not found',
                'user not found' => 'User not found',
            ],
        ];

        $this->saveTranslation(static::CS_LANG_ID, 'frontend.error', $translations['cs']);
        $this->saveTranslation(static::EN_LANG_ID, 'frontend.error', $translations['en']);

        // system default
        $translations = [
            'cs' => [
                'per_page_label' => 'položek',
            ],
            'en' => [
                'per_page_label' => 'items'
            ],
        ];

        $this->saveTranslation(static::CS_LANG_ID, 'frontend.gridview', $translations['cs']);
        $this->saveTranslation(static::EN_LANG_ID, 'frontend.gridview', $translations['en']);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m190830_113326_i18n cannot be reverted.\n";

        return false;
    }

}
