<?php
/*
* Copyright 2023 DactylGroup s.r.o.
*
* Licensed under the EUPL, Version 1.2 or – as soon they
will be approved by the European Commission - subsequent
versions of the EUPL (the "Licence");
* You may not use this work except in compliance with the
Licence.
* You may obtain a copy of the Licence at:
*
* https://joinup.ec.europa.eu/software/page/eupl5
*
* Unless required by applicable law or agreed to in
writing, software distributed under the Licence is
distributed on an "AS IS" basis,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
express or implied.
* See the Licence for the specific language governing
permissions and limitations under the Licence.
*/
?>


<?php

use dactylcore\core\db\Migration;


class M210226180514UserImport extends Migration
{
    public function safeUp()
    {
        $translations = [
            'cs' => [
                'gps_error' => 'nevalidně zadané GPS souřadnice',
                'not_unique_error' => 'na těchto souřadnicích je více než jedna stanice. Upravte prosím manuálně.',
                'unknown_action' => 'zadána neznámá akce: "{action}"',
                'load_error' => 'nepodařilo se načíst data',
                'pair_action_error' => 'párová stanice má chybné pole akce',
                'pair_gps_error' => 'párová stanice má chybné GPS souřadnice',
                'pair_type_error' => 'párová stanice má chybný typ',
                'pair_station_errors' => 'chyby jsou u párové stanice',
                'unknown_type' => 'neznámý typ: "{type}"',
                'importFile' => 'vyberte soubor',
                'import' => 'hromadný import stanic',
                'import_help_text' => 'Nejdříve vyberte soubor z počítače a po spuštění prosím vyčkejte na výsledek procesu importu.',
                'import_submit_button' => 'spustit import stanic',
                'no_stations_imported' => 'nebyly importované žádné stanice',
                'import_line' => 'řádek {row}: ',
                'success_import' => 'stanice úspěšně importovaná - {link}',
                'warning_import' => 'akce nezadána - přeskočeno',
                'warning_user_limit_matched' => 'Bylo dosaženo maximálního limitu řádků. Tento a nasledující řádky byly ignorovány.',
                'error_import' => 'stanici se nepodařilo importovat kvůli následujícím chybám:<br>{error}',
                'station_detail' => 'detail stanice',
                'update_unmatched_types' => 'Neodpovida typ stanice -> "{excel-type}" != "{station-type}"',
            ],
            'en' => [
                'gps_error' => 'filled GPS are not valid',
                'not_unique_error' => 'on these GPS are more than one station. Please edit her manually.',
                'unknown_action' => 'unknown action: "{action}"',
                'load_error' => 'cannot load data',
                'pair_action_error' => 'pair station has wrong action',
                'pair_gps_error' => 'pair station has wrong GPS',
                'pair_type_error' => 'pair station has wrong type',
                'pair_station_errors' => 'errors are by the pair station',
                'unknown_type' => 'unknown type: "{type}"',
                'importFile' => 'select a file',
                'import' => 'bulk station import',
                'import_help_text' => 'Select a local file first. After start of import process, please wait for the results.',
                'import_submit_button' => 'run stations import',
                'no_stations_imported' => 'no stations imported',
                'import_line' => 'on row {row}: ',
                'success_import' => 'station successfully imported - {link}',
                'warning_import' => 'action not filled - ignored',
                'warning_user_limit_matched' => 'Maximum row limit reached. This and the following lines were ignored.',
                'error_import' => 'station cannot be imported because due to following errors:<br>{error}',
                'station_detail' => 'station detail',
                'update_unmatched_types' => 'Types do not match -> "{excel-type}" != "{station-type}"',
            ]
        ];

        $this->saveTranslation(static::CS_LANG_ID, 'frontend.user-import', $translations['cs']);
        $this->saveTranslation(static::EN_LANG_ID, 'frontend.user-import', $translations['en']);
    }
}
