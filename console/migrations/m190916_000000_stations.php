<?php
/*
* Copyright 2023 DactylGroup s.r.o.
*
* Licensed under the EUPL, Version 1.2 or – as soon they
will be approved by the European Commission - subsequent
versions of the EUPL (the "Licence");
* You may not use this work except in compliance with the
Licence.
* You may obtain a copy of the Licence at:
*
* https://joinup.ec.europa.eu/software/page/eupl5
*
* Unless required by applicable law or agreed to in
writing, software distributed under the Licence is
distributed on an "AS IS" basis,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
express or implied.
* See the Licence for the specific language governing
permissions and limitations under the Licence.
*/
?>


<?php

use dactylcore\core\db\Migration;


/**
 * Class m190830_113326_i18n
 */
class m190916_000000_stations extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->struct();
        $this->lang();
        $this->gases();
    }

    public function struct()
    {
        $this->createTable("station", [
            'id' => $this->primaryKey(11),
            'id_user' => $this->integer(12)->null(),
            'id_station_pair' => $this->integer(11)->null(),
            'id_master' => $this->integer(11)->null(),
            'pair_position' => $this->string(255)->null(), // A or B

            'type' => $this->string(255)->null(),
            'lng' => $this->decimal(16, 14)->notNull(),
            'lat' => $this->decimal(16, 14)->notNull(),
            'antenna_volume' => $this->decimal(10, 2)->null(),
            'channel_width' => $this->decimal(10, 2)->null(),
            'power' => $this->decimal(10, 2)->null(),

            'registered_at' => $this->integer(11)->null(),
            'valid_to' => $this->integer(11)->null(),
            'protected_to' => $this->integer(11)->null(),
            'status' => $this->string(255)->notNull(),

            'deleted' => $this->integer(1)->notNull()->defaultValue(0),
            'created_at' => $this->integer(11)->null(),
            'updated_at' => $this->integer(11)->null(),

        ], static::$TABLE_OPTIONS);


        $this->createTable("station_fs", [
            'id_station_fs' => $this->primaryKey(11),
            'id_station' => $this->integer(11)->notNull(),

            'ratio_signal_interference' => $this->integer(3)->null(),
            'frequency' => $this->decimal(10, 2)->null(),
            'specific_run_down' => $this->decimal(10, 6)->null(),


            'deleted' => $this->integer(1)->notNull()->defaultValue(0),
            'created_at' => $this->integer(11)->null(),
            'updated_at' => $this->integer(11)->null(),
        ], static::$TABLE_OPTIONS);

        $this->createTable("station_wigig", [
            'id_station_wigig' => $this->primaryKey(11),
            'id_station' => $this->integer(11)->notNull(),

            'direction' => $this->integer(3)->notNull(),
            'eirp' => $this->decimal(10, 2)->null(),
            'front_to_back_ratio' => $this->decimal(10, 2)->null(),
            'is_ptmp' => $this->tinyInteger(1)->defaultValue(1),


            'deleted' => $this->integer(1)->notNull()->defaultValue(0),
            'created_at' => $this->integer(11)->null(),
            'updated_at' => $this->integer(11)->null(),
        ], static::$TABLE_OPTIONS);

        $this->addForeignKey('fk_station_user', 'station', 'id_user', 'user', 'id', 'no action', 'no action');
        $this->addForeignKey('fk_station_station_fs', 'station_fs', 'id_station', 'station', 'id', 'no action', 'no action');
        $this->addForeignKey('fk_station_station', 'station', 'id_station_pair', 'station', 'id', 'no action', 'no action'); // pair A to B
        $this->addForeignKey('fk_station_station_wigig', 'station_wigig', 'id_station', 'station', 'id', 'no action', 'no action');
    }


    protected function lang()
    {
        $translations = [
            'cs' => [
                'id' => 'ID',
                'type' => 'Typ',
                'lng' => 'GPS Délka [°]',
                'lat' => 'GPS šířka [°]',
                'antenna_volume' => 'Zisk antény [dBi]',
                'channel_width' => 'Šířka kanálu [MHz]',
                'power' => 'Přivedený výkon [dBm]',
                'registered_at' => 'Registrováno',
                'valid_to' => 'Platné do',
                'protected_to' => 'Chráněno',
                'status' => 'Stav',
                'fs' => 'EN 302 217',
                'wigig' => 'WiGig',
                // fs
                'ratio_signal_interference' => 'Modulace a požadavek C/I [dB]',
                'frequency' => 'Použitý střední kmitočet [MHz]',
                'specific_run_down' => 'Specifický útlum [dB]',
                // wigig
                'direction' => 'Hlavní směr vyzařování [°]',
                'eirp' => 'E.I.R.P. [dBm]',
                'front_to_back_ratio' => 'Front to back ratio [dB]',
                'mhz' => 'MHz',
                'dbi' => 'dBi',
                'dbm' => 'dBm',
                'new wigig' => 'Nová stanice WiGig/MGWS',
                'new fs' => 'Nová stanice EN 302 217',
                'renew registration' => 'Obnovit registraci',
                'import' => 'Import',
                'export' => 'Export',
                'back to coordinates' => 'Zpět na polohu',
                'back to parameters' => 'Zpět na parametry',
                'save and continue' => 'Uložit a pokračovat',
                'back to stations' => 'Zpět na stanice',
                'type_fs' => 'FS PtP',
                'type_wigig_ptmp' => 'WiGig PtMP',
                'type_wigig_ptp' => 'WiGig PtP',

                'status_draft' => 'koncept',
                'status_waiting' => 'čeká',
                'status_finished' => 'aktivní',
                'status_expired' => 'expirováno',

                'successfully published' => 'úspěšně publikováno',
                'has conflicts' => 'nelze publikovat, stanice má konflikty',
                'next' => 'dále',
                'save' => 'uložit',
                'publish station' => 'publikovat stanici',
                'are you sure you want to save?' => 'opravdu chcete data upravit a uložit? Budete muset následně projít procesem publikace stanice',

                'flash_successfully_deleted' => 'stanice úspěšně smazána',
                'error while deleting' => 'chyba během mazání',

                'stations' => 'stanice',
                'typeName' => 'typ',
                'stations_admin_name' => 'stanice',
                'is_ptmp' => 'Je PTMP',

                'save and recount' => 'uložit a přepočítat',
                'successfully prolonged' => 'úspěšně prodlouženo',
                'Are you sure you want to prolong this station?' => 'Opravdu chcete prodloužit platnost této stanice?',
            ],
            'en' => [
                'id' => 'ID',
                'type' => 'Type',
                'lng' => 'GPS Lng [°]',
                'lat' => 'GPS Lat [°]',
                'antenna_volume' => 'Antenna volume [dBi]',
                'channel_width' => 'Channel width [MHz]',
                'power' => 'Power [dBm]',
                'registered_at' => 'Registered at',
                'valid_to' => 'Valid to',
                'protected_to' => 'protected to',
                'status' => 'status',
                'fs' => 'EN 302 217',
                'wigig' => 'WiGig',
                // fs
                'ratio_signal_interference' => 'modulation and C/I request [dB]',
                'frequency' => 'Frequency [MHz]',
                'specific_run_down' => 'Specific rundown [dB]',
                // wigig
                'direction' => 'Direction [°]',
                'eirp' => 'E.I.R.P. [dBm]',
                'front_to_back_ratio' => 'Front to back ratio [dB]',
                'mhz' => 'MHz',
                'dbi' => 'dBi',
                'dbm' => 'dBm',
                'new wigig' => 'New WiGig/MGWS',
                'new fs' => 'new EN 302 217',
                'renew registration' => 'renew registration',
                'import' => 'Import',
                'export' => 'Export',
                'back to coordinates' => 'back to coordinates',
                'back to parameters' => 'back to parameters',
                'save and continue' => 'save and continue',
                'back to stations' => 'back to stations',
                'type_fs' => 'FS PtP',
                'type_wigig_ptmp' => 'WiGig PtMP',
                'type_wigig_ptp' => 'WiGig PtP',

                'status_draft' => 'draft',
                'status_waiting' => 'waiting',
                'status_finished' => 'active',
                'status_expired' => 'expired',

                'successfully published' => 'successfully published',
                'has conflicts' => 'cannot publish, the station has conflicts',
                'next' => 'next',
                'save' => 'save',
                'publish station' => 'publish station',
                'are you sure you want to save?' => 'are you sure you want to save data and modify the station? You might have to proceed the process of publication again',

                'flash_successfully_deleted' => 'station successfully deleted',
                'error while deleting' => 'error while deleting',

                'stations' => 'stations',
                'typeName' => 'type',
                'stations_admin_name' => 'stations',
                'is_ptmp' => 'is PTMP',

                'save and recount' => 'save and recount',
                'successfully prolonged' => 'successfully prolonged',
                'Are you sure you want to prolong this station?' => 'Are you sure you want to prolong this station?',
            ],
        ];

        $this->saveTranslation(static::CS_LANG_ID, 'common.station', $translations['cs']);
        $this->saveTranslation(static::EN_LANG_ID, 'common.station', $translations['en']);

        // this is rewritten in m200113_000000_stations_config
        $this->saveConfig('REGISTRATION_PERIOD', 12, 'dc-system', 'stations', 0, 0);
        $this->saveConfig('REGISTRATION_PERIOD_WARNING', 2, 'dc-system', 'stations', 0, 0);
        $this->saveConfig('PROTECTION_PERIOD', 18, 'dc-system', 'stations', 0, 0);
        $this->saveConfig('PROTECTION_PERIOD_WARNING', 2, 'dc-system', 'stations', 0, 0);
        $this->saveConfig('FRONT_TO_BACK_RATIO_HIGH', 25, 'dc-system', 'stations', 0, 0);
        $this->saveConfig('FRONT_TO_BACK_RATIO_LOW', 15, 'dc-system', 'stations', 0, 0);
        $this->saveConfig('PTMP_THRESHOLD', 25, 'dc-system', 'stations', 0, 0);

        $translations = [
            'cs' => [
                'stations' => 'Stanice',
                'REGISTRATION_PERIOD' => 'Délka registrace (počet měsíců)',
                'REGISTRATION_PERIOD_WARNING' => 'Varování před koncem registrace (počet měsíců)',

                'PROTECTION_PERIOD' => 'Délka ochranné lhůty (počet měsíců)',
                'PROTECTION_PERIOD_WARNING' => 'Varování před koncem ochranné lhůty (počet měsíců)',

                'FRONT_TO_BACK_RATIO_HIGH' => 'Front to back ratio - vysoká hodnota',
                'FRONT_TO_BACK_RATIO_LOW' => 'Front to back ratio - nízká hodnota',

                'PTMP_THRESHOLD' => 'Limit pro stanovení PtMP',
            ],
            'en' => [
                'stations' => 'Stanice',
                'MAIN_PAGE_ID_LOGGED' => 'Registration period (months)',
                'MAIN_PAGE_ID_LOGGED_WARNING' => 'Warning before registration expiration (months)',
                'PROTECTION_PERIOD' => 'Registration protection period (months)',
                'PROTECTION_PERIOD_WARNING' => 'Warning before registration protection expiration (months)',


                'FRONT_TO_BACK_RATIO_HIGH' => 'Front to back ratio - high value',
                'FRONT_TO_BACK_RATIO_LOW' => 'Front to back ratio - low value',

                'PTMP_THRESHOLD' => 'Threshold for PtMP',
            ]
        ];

        $this->saveTranslation(static::CS_LANG_ID, 'admin.config', $translations['cs']);
        $this->saveTranslation(static::EN_LANG_ID, 'admin.config', $translations['en']);

    }

    public function gases()
    {
        $data = file_get_contents('gases.json', true);
        $dataEncoded = json_decode($data, true);

        $this->createTable("gases", [
            'frequency' => $this->decimal(10, 6)->notNull(),
            'lattgas' => $this->decimal(10, 6)->notNull(),
        ], static::$TABLE_OPTIONS);


        $this->batchInsert('gases', ['frequency', 'lattgas'], $dataEncoded);

    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m190908_000000_mail_verification cannot be reverted.\n";

        return false;
    }

}
