<?php
/*
* Copyright 2023 DactylGroup s.r.o.
*
* Licensed under the EUPL, Version 1.2 or – as soon they
will be approved by the European Commission - subsequent
versions of the EUPL (the "Licence");
* You may not use this work except in compliance with the
Licence.
* You may obtain a copy of the Licence at:
*
* https://joinup.ec.europa.eu/software/page/eupl5
*
* Unless required by applicable law or agreed to in
writing, software distributed under the Licence is
distributed on an "AS IS" basis,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
express or implied.
* See the Licence for the specific language governing
permissions and limitations under the Licence.
*/
?>


<?php

use dactylcore\core\db\Migration;


class M210128151253PublicationInGrid extends Migration
{
    public function safeUp()
    {
        $this->trans();
    }

    protected function trans()
    {
        $translations = [
            'cs' => [
                'isolation' => 'izolace',
                'published by' => 'publikace',
                'standard_publish' => 'stanice publikována standardním způsobem',
                'declared_publish' => 'stanice publikována prohlášením o izolaci',
            ],
            'en' => [
                'isolation' => 'isolation',
                'published by' => 'published by',
                'standard_publish' => 'the station was published in a standard way',
                'declared_publish' => 'the station was published with a declaration of isolation',
            ],
        ];
        $this->saveTranslation(static::CS_LANG_ID, 'frontend.station', $translations['cs']);
        $this->saveTranslation(static::EN_LANG_ID, 'frontend.station', $translations['en']);
    }
}
