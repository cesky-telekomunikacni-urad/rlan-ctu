<?php
/*
* Copyright 2023 DactylGroup s.r.o.
*
* Licensed under the EUPL, Version 1.2 or – as soon they
will be approved by the European Commission - subsequent
versions of the EUPL (the "Licence");
* You may not use this work except in compliance with the
Licence.
* You may obtain a copy of the Licence at:
*
* https://joinup.ec.europa.eu/software/page/eupl5
*
* Unless required by applicable law or agreed to in
writing, software distributed under the Licence is
distributed on an "AS IS" basis,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
express or implied.
* See the Licence for the specific language governing
permissions and limitations under the Licence.
*/
?>


<?php

use dactylcore\core\db\Migration;


class M220802132245ChangeStationType extends Migration
{
    public function safeUp()
    {
        $this->i18n();
        $this->i18nUrls();
    }

    public function i18n()
    {
        $translations = [
            'cs' => [
                'Do you really want to change type of this station?' => 'Skutečně chcete změnit typ této stanice?',
                'change type to 52' => 'změnit typ na 5.2 GHz',
                'station does not exists' => 'Zvolená stanice neexistuje',
                'type successfully changed' => 'Typ stanice byl úspěšně změněn.',
                'type cannot be changed' => 'Nelze změnit typ stanice.',
            ],
            'en' => [
                'confirm' => 'are you sure?',
                'Do you really want to change type of this station?' => 'Do you really want to change type of this station?',
                'change type to 52' => 'change type to 5.2 GHz',
                'station does not exists' => 'selected station does not exists',
                'type successfully changed' => 'Type of the station was successfully changed.',
                'type cannot be changed' => 'Cannot change type of the station.',
            ]
        ];

        $this->saveTranslation(static::CS_LANG_ID, 'frontend.station', $translations['cs']);
        $this->saveTranslation(static::EN_LANG_ID, 'frontend.station', $translations['en']);
    }

    public function i18nUrls() {
        $translations = [
            'cs' => [
                'station/change-type-58-to-52' => 'stanice/zmenit-typ',
                'station/delete' => 'stanice/smazat',
            ],
            'en' => [
                'station/change-type-58-to-52' => 'station/change-type',
                'station/delete' => 'station/delete',
            ],
        ];

        $this->saveTranslation(static::CS_LANG_ID, 'frontend.url', $translations['cs']);
        $this->saveTranslation(static::EN_LANG_ID, 'frontend.url', $translations['en']);
    }
}
