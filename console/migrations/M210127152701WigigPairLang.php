<?php
/*
* Copyright 2023 DactylGroup s.r.o.
*
* Licensed under the EUPL, Version 1.2 or – as soon they
will be approved by the European Commission - subsequent
versions of the EUPL (the "Licence");
* You may not use this work except in compliance with the
Licence.
* You may obtain a copy of the Licence at:
*
* https://joinup.ec.europa.eu/software/page/eupl5
*
* Unless required by applicable law or agreed to in
writing, software distributed under the Licence is
distributed on an "AS IS" basis,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
express or implied.
* See the Licence for the specific language governing
permissions and limitations under the Licence.
*/
?>


<?php

use dactylcore\core\db\Migration;


class M210127152701WigigPairLang extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->lang();
    }

    public function lang() {

        $language = [
            'cs' => [
                'id_station_pair' => 'párová stanice',
                'pair_station' => 'spárovaná stanice',
                'partner_is_not_suitable' => 'Párová stanice už bohužel nesplňuje podmínky pro spárování.',
                'mac_or_serial' => 'Ser. číslo/MAC',
                'pair_station_link_alert' => 'Spárovaná stanice má zatím nastavené původní parametry. Ke změně dojde po publikaci této stanice.',
                'republication_of_partner_flash' => 'Spárovaná stanice byla upravena. Prosím zopakujte její publikaci.',
                'This step will remove pairing!' => 'Tento krok odstraní spárování!',
                'Do you really want to delete station' => 'Opravdu chcete stanici odstranit?',
                'wigig-ptp-ptmp-pair-error' => 'Tato stanice je typu PtMP a nemůže být v páru se stanicí typu PtP.',
            ],
            'en' => [
                'id_station_pair' => 'pair station',
                'pair_station' => 'pair station',
                'partner_is_not_suitable' => 'Pair station is unfortunately no longer suitable for pairing.',
                'mac_or_serial' => 'Serial/MAC',
                'pair_station_link_alert' => 'Paired station still has old parameters. It will be changed after publication.',
                'republication_of_partner_flash' => 'Paired station was updated. Please run publication process again.',
                'This step will remove pairing!' => 'This step will remove pairing!',
                'Do you really want to delete station' => 'Do you really want to delete station?',
                'wigig-ptp-ptmp-pair-error' => 'This station is a PtMP and cannot be paired with a PtP station.',
            ]
        ];

        $this->saveTranslation(self::CS_LANG_ID, 'common.station', $language['cs']);
        $this->saveTranslation(self::EN_LANG_ID, 'common.station', $language['en']);


    }
}
