<?php
/*
* Copyright 2023 DactylGroup s.r.o.
*
* Licensed under the EUPL, Version 1.2 or – as soon they
will be approved by the European Commission - subsequent
versions of the EUPL (the "Licence");
* You may not use this work except in compliance with the
Licence.
* You may obtain a copy of the Licence at:
*
* https://joinup.ec.europa.eu/software/page/eupl5
*
* Unless required by applicable law or agreed to in
writing, software distributed under the Licence is
distributed on an "AS IS" basis,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
express or implied.
* See the Licence for the specific language governing
permissions and limitations under the Licence.
*/
?>


<?php

use dactylcore\core\db\Migration;


class m200827_000000_security_headers_csp extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {

        $cspDirectivesArray = [
            'connect-src' => [
                "https://*.mapbox.com",
                "https://events.mapbox.com",
            ],
            'script-src' => [
                "'unsafe-eval'",
                "https://api.mapbox.com/",
                "https://npmcdn.com/@turf/turf@5.1.6/turf.min.js",
                "https://cdn.jsdelivr.net/npm/axios/",
                "https://polyfill.io/v3/",
                "https://cdnjs.cloudflare.com/",
            ],
            'style-src' => [
                "https://api.mapbox.com",
                "https://api.tiles.mapbox.com",
            ],
            'frame-src' => [
                "https://www.youtube.com/",
                "https://www.youtu.be",
            ]
        ];

        $this->saveCSPDirective('CSP_HEADERS_CONNECT-SRC', $cspDirectivesArray['connect-src']);
        $this->saveCSPDirective('CSP_HEADERS_SCRIPT-SRC', $cspDirectivesArray['script-src']);
        $this->saveCSPDirective('CSP_HEADERS_STYLE-SRC', $cspDirectivesArray['style-src']);
        $this->saveCSPDirective('CSP_HEADERS_FRAME-SRC', $cspDirectivesArray['frame-src']);


    }
}
