<?php
/*
* Copyright 2023 DactylGroup s.r.o.
*
* Licensed under the EUPL, Version 1.2 or – as soon they
will be approved by the European Commission - subsequent
versions of the EUPL (the "Licence");
* You may not use this work except in compliance with the
Licence.
* You may obtain a copy of the Licence at:
*
* https://joinup.ec.europa.eu/software/page/eupl5
*
* Unless required by applicable law or agreed to in
writing, software distributed under the Licence is
distributed on an "AS IS" basis,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
express or implied.
* See the Licence for the specific language governing
permissions and limitations under the Licence.
*/
?>


<?php

use dactylcore\core\db\Migration;

class m191002_000002_translation_update extends Migration
{
    public function safeUp()
    {
        // Article category
        $translations = [
            'cs' => [
                'station_position' => 'Pozice stanice',
                'stations_position' => 'Pozice stanic',
                'station_a' => 'Stanice A',
                'station_b' => 'Stanice B',
                'station_parameters' => 'Parametry stanice',
                'stations_parameters' => 'Parametry stanic',
                'type' => 'Typ',
                'parametr_list' => 'Výpis parametrů',
                'table_stations_in_range' => 'Tabulka dotčených stanic',
                'distance' => 'Vzdálenost [km]',
                'reset_form' => 'Obnovit',
                'station_type' => 'Typ stanice',
                'inConflict' => 'Konflikt',
                'passiveIsolationIncreaseRequested' => 'zvyšte izolaci o [dB]',
                'mine only' => 'moje stanice',
                'show all' => 'všechny stanice',
                'choose angle' => 'vyberte úhel',
                'status_badge' => 'stav: '
            ],
            'en' => [
                'station_position' => 'Station position',
                'stations_position' => 'Stations position',
                'station_a' => 'Station A',
                'station_b' => 'Station B',
                'station_parameters' => 'Station parameters',
                'stations_parameters' => 'Stations parameters',
                'type' => 'Type',
                'parametr_list' => 'Parametr list',
                'table_stations_in_range' => 'Table of stations in range',
                'distance' => 'Distance [km]',
                'reset_form' => 'Reset',
                'station_type' => 'Station type',
                'inConflict' => 'conflict',
                'passiveIsolationIncreaseRequested' => 'improve isolation by [dB]',
                'mine only' => 'my only',
                'show all' => 'show all',
                'choose angle' => 'choose angle',
                'status_badge' => 'status: '
            ],
        ];

        $this->saveTranslation(static::CS_LANG_ID, 'frontend.station', $translations['cs']);
        $this->saveTranslation(static::EN_LANG_ID, 'frontend.station', $translations['en']);
    }
}
