<?php
/*
* Copyright 2023 DactylGroup s.r.o.
*
* Licensed under the EUPL, Version 1.2 or – as soon they
will be approved by the European Commission - subsequent
versions of the EUPL (the "Licence");
* You may not use this work except in compliance with the
Licence.
* You may obtain a copy of the Licence at:
*
* https://joinup.ec.europa.eu/software/page/eupl5
*
* Unless required by applicable law or agreed to in
writing, software distributed under the Licence is
distributed on an "AS IS" basis,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
express or implied.
* See the Licence for the specific language governing
permissions and limitations under the Licence.
*/
?>


<?php

use dactylcore\core\db\Migration;
use yii\helpers\Inflector;

class M210216134958AlliancesTables extends Migration
{
    /**
     * @var string Set table name for CRUD entity you want to create.
     */
    protected string $tableName = 'user_alliance';

    protected string $tableNamePluralized = ''; // business_offer_categories

    protected string $sectionName = ''; // business-offer-category

    protected function initVariables(): void
    {
        if (!$this->sectionName) {
            $this->sectionName = str_replace('_', '-', $this->tableName);
        }

        if (!$this->tableNamePluralized) {
            $this->tableNamePluralized = Inflector::pluralize($this->tableName);
        }
    }

    public function safeUp(): bool
    {
        $this->initVariables();
        $this->structure();
        $this->permissions();
        $this->i18nPermissions();
        $this->i18nCommon();
        $this->i18nFronte();
        $this->i18nAdmin();
        $this->i18nLogs();
        $this->i18nUrls();

        return true;
    }

    protected function structure(): void
    {
        $this->createTable($this->tableName, [
            'id' => $this->primaryKey(11)->notNull(),

            'name' => $this->string(255)->notNull(),
            'description' => $this->text()->null()->defaultValue(null),

            'deleted' => $this->tinyInteger(1)->notNull()->defaultValue(0),
            'created_at' => $this->integer(11)->notNull(),
            'updated_at' => $this->integer(11)->notNull(),
        ]);

        // Connector table
        $this->createTable("user_alliance_connector", [
            "id" => $this->primaryKey(11)->notNull(),

            "id_{$this->tableName}" => $this->integer(11)->notNull(),
            'id_user' => $this->integer(11)->notNull(),

            'invitation_token' => $this->string(255)->null()->defaultValue(null),
            // Invited, member, admin, excluded
            'status' => $this->string(32)->notNull(),

            'deleted' => $this->tinyInteger(1)->notNull()->defaultValue(0),
            'created_at' => $this->integer(11)->notNull(),
            'invited_at' => $this->integer(11)->notNull(),
            'updated_at' => $this->integer(11)->notNull(),
        ]);

        $this->addForeignKey(
            "fk_{$this->tableName}_user_connector",
            "user_alliance_connector",
            "id_{$this->tableName}",
            $this->tableName,
            'id',
            'no action',
            'no action'
        );

        $this->addForeignKey(
            "fk_user_alliance_connector_user",
            "user_alliance_connector",
            'id_user',
            'user',
            'id',
            'no action',
            'no action'
        );
    }

    protected function permissions(): void
    {
        $this->saveUserRolePermissions([
            "app-admin_{$this->sectionName}",
            "app-admin_{$this->sectionName}_index",
            "app-admin_{$this->sectionName}_create",
            "app-admin_{$this->sectionName}_update",
            "app-admin_{$this->sectionName}_delete",
        ]);
    }

    protected function i18nPermissions(): void
    {
        $this->saveTranslation(
            1,
            'admin.access-manager',
            [
                "app-admin_{$this->sectionName}" => 'Uživatelská sdružení',
                "app-admin_{$this->sectionName}_index" => 'Seznam',
                "app-admin_{$this->sectionName}_create" => 'Vytvořit',
                "app-admin_{$this->sectionName}_update" => 'Upravit',
                "app-admin_{$this->sectionName}_delete" => 'Odstranit',
            ]
        );
        $this->saveTranslation(
            2,
            'admin.access-manager',
            [
                "app-admin_{$this->sectionName}" => 'User alliances',
                "app-admin_{$this->sectionName}_index" => 'List',
                "app-admin_{$this->sectionName}_create" => 'Create',
                "app-admin_{$this->sectionName}_update" => 'Update',
                "app-admin_{$this->sectionName}_delete" => 'Delete',
            ]
        );
    }

    protected function i18nAdmin(): void
    {
        $this->saveTranslation(
            1,
            "admin.{$this->sectionName}",
            [
                $this->tableNamePluralized => 'Uživatelská sdružení',
                "create_{$this->tableName}" => 'Vytvořit sdružení',
                "update_{$this->tableName}" => 'Upravit sdružení',
                'flash_successfully_created' => 'Sdružení bylo úspěšně vytvořeno.',
                'flash_successfully_updated' => 'Sdružení bylo úspěšně upraveno.',
                'flash_successfully_deleted' => 'Sdružení bylo úspěšně odstraněno.',
            ]
        );
        $this->saveTranslation(
            2,
            "admin.{$this->sectionName}",
            [
                $this->tableNamePluralized => 'User alliances',
                "create_{$this->tableName}" => 'Create alliance',
                "update_{$this->tableName}" => 'Update alliance',
                'flash_successfully_created' => 'Alliance was successfully created.',
                'flash_successfully_updated' => 'Alliance was successfully updated.',
                'flash_successfully_deleted' => 'Alliance was successfully deleted.',
            ]
        );
    }

    protected function i18nCommon(): void
    {
        $this->saveTranslation(
            1,
            "common.{$this->sectionName}",
            [
                'name' => 'název sdružení',
                'description' => 'popis',
                'invitation_token' => 'token pozvánky',
                'status' => 'členský status',

                'deleted' => 'odstraněno',
                'created_at' => 'vytvořeno',
                'updated_at' => 'upraveno',
                'invited_at' => 'člen pozván',

                // mails
                'notification_invitation_subject' => 'Pozvánka do sdružení: {name}',
                'notification_excluded_subject' => 'Oznámení o vyloučení ze sdružení: {name}',
                'notification_deleted_subject' => 'Oznámení o smazání sdružení: {name}',
                'notification_invitation_text' => 'Byl jste přizván do sdružení. Pozvánku můžete přijmout tlačítkem níže, nebo ignorovat.',
                'notification_excluded_text' => 'Byl jste vyloučen ze sdružení. Pokud vyloučení nesouhlasí, kontaktujte jeho správce.',
                'notification_deleted_text' => 'Sdružení v němž jste členem bylo smazáno. Můžete nyní vstoupit do jiného sdružení nebo založit své vlastní.',
                'invitation_type' => 'Pozvání do',
                'exclusion_type' => 'Oznámení o vyloučení ze',
                'deletion_type' => 'Oznámení o zrušení',
                'accept_invitation' => 'Přijmout pozvánku',
            ]
        );
        $this->saveTranslation(
            2,
            "common.{$this->sectionName}",
            [
                'name' => 'alliance name',
                'description' => 'description',
                'invitation_token' => 'invitation token',
                'status' => 'member status',

                'deleted' => 'deleted',
                'created_at' => 'created',
                'updated_at' => 'updated',
                'invited_at' => 'invited',

                // Mails
                'notification_invitation_subject' => 'Invitation to the alliance: {name}',
                'notification_excluded_subject' => 'Notice of exclusion from alliance: {name}',
                'notification_deleted_subject' => 'Notice of deletion of alliance: {name}',
                'notification_invitation_text' => 'You were invited to the alliance. You can accept this invitation by clicking button below, or ignore this message.',
                'notification_excluded_text' => 'You were excluded from the alliance. If you do not agree, you have to contact the administrator.',
                'notification_deleted_text' => 'The alliance you are a member of has been deleted. You can now join another alliance or create your own.',
                'invitation_type' => 'Invitation into',
                'exclusion_type' => 'Notice of exclusion from',
                'deletion_type' => 'Notice of deletion of',
                'accept_invitation' => 'Accept invitation',
            ]
        );
    }

    protected function i18nFronte(): void
    {
        $this->saveTranslation(
            1,
            "frontend.{$this->sectionName}",
            [
                'other_user_warning' => 'POZOR! Pracujete na profilu jiného uživatele!',
                'alliance_setup' => 'správa sdružení',
                'alliance' => 'sdružení',
                'without_text' => 'Uživatelská sdružení slouží k propojení uživatelů a jejich stanic. Členům jednoho sdružení je umožněno vzájemně spravovat stanice (prodlužovat platnost, mazat, upravovat, atp.).</p><p>Pokud chcete do sdružení vstoupit, je nutné buďto založit nové sdružení, anebo přijmout e-mailovou pozvánku od správce již založeného. Pokud jste se správcem v kontaktu, předejte mu prosím vaše ID pro identifikaci: "<u>{user_id}</u>"',
                'create_alliance' => 'založit sdružení',
                'accepting_invitation' => 'přijatá pozvánka',
                'excluding_member' => 'vyloučení člena',
                'promoting_member' => 'povýšení člena',

                'change_alliance_name' => 'upravit název sdružení',
                'save_new_name' => 'uložit nový název',
                'promote_member' => 'povýšit na správce',
                'exclude_member' => 'vyloučit člena',
                'flash_successfully_created' => 'Sdružení úspěšně vytvořeno',
                'cannot_create_alliance' => 'Nelze založit sdružení',
                'invitation_sent' => 'Pozvánka úspěšně odeslána',
                'cannot_send_invitation' => 'Nelze odeslat pozvánku',
                'verification_unknown_invitation' => 'Pozvánka neexistuje, nebo již byla použita',
                'verification_success_accept' => 'Pozvánka potvrzena a přijata',
                'verification_error_accept' => 'Pozvánku nelze přijmout',
                'verification_user_already_member' => 'Pozvánku nelze přijmout, uživatel již je členem jiného sdružení',
                'verification_unknown_user' => 'Pozvaného uživatele se nepodařilo nalézt',
                'success_change_name' => 'Nový název uložen',
                'cannot_change_name' => 'Nelze uložit nový název',
                'success_promote_user' => 'Uživatel úspěšně povýšen',
                'cannot_promote_user' => 'Nelze povýšit tohoto uživatele',
                'success_exclude_user' => 'Uživatel úspěšně vyloučen',
                'cannot_exclude_user' => 'Nelze vyloučit tototo uživatele',
                'success_delete_allaince' => 'Sdružení úspěšně smazáno',
                'cannot_delete_alliance' => 'Sdružení nelze odstranit',
                'success_resign_user' => 'Úspěšně jste vystoupil ze sdružení',
                'cannot_resign_user' => 'Nepodařilo se vystoupit ze sdružení',
                'create_user_alliance' => 'Založit sdružení',
                'admin_role' => 'Správce',
                'member_role' => 'Člen',
                'choose_user' => 'Zvolte uživatele',
                'invite_user' => 'Pozvat uživatele',
                'delete_alliance' => 'Smazat sdružení',
                'resign_alliance' => 'Vystoupit ze sdružení',
                'in_alliance' => 've sdružení',
                'want_delete_alliance' => 'Opravdu chcete smazat sdružení?',
                'want_resign_alliance' => 'Opravdu chcete vystoupit ze sdružení?',

                'id' => 'id',
                'user_name' => 'uživatel',
                'role' => 'role',
                'action' => 'akce',
                'yes_remove' => 'Ano, smazat',
                'no_remove' => 'Ne, ponechat',
                'yes_resign' => 'Ano, vystoupit',
                'no_resign' => 'Ne, zůstat',
                'really' => 'Jste si jistí?',
            ]
        );
        $this->saveTranslation(
            2,
            "frontend.{$this->sectionName}",
            [
                'other_user_warning' => "CAUTION! You are working on another user's profile!",
                'alliance_setup' => 'alliance administration',
                'without_text' => "User alliances are used to connect users and their stations. Members of one alliance are allowed to manage stations of each other (extend validity, delete, edit, etc.).</p><p>If you want to join the association, you must either create a new association or accept an e-mail invitation from the already established administrator. If you're in contact with an administrator, please provide your ID for identification: \"<u>{user_id}</u>\"",
                'create_alliance' => 'create alliance',
                'alliance' => 'alliance',
                'cannot_create_alliance' => 'cannot create alliance',

                'change_alliance_name' => 'change the name',
                'save_new_name' => 'save new name',
                'promote_member' => 'promote to administrator',
                'exclude_member' => 'exclude the member',
                'accepting_invitation' => 'accepted invitation',
                'excluding_member' => 'member excluded',
                'promoting_member' => 'member promoted',
                'flash_successfully_created' => 'Alliance successfully created',
                'invitation_sent' => 'Invitation successfully sent',
                'cannot_send_invitation' => 'Cannot send invitation',
                'verification_unknown_invitation' => 'Invitation does not exist or has already been used',
                'verification_success_accept' => 'Invitation confirmed and accepted',
                'verification_error_accept' => 'Invitation not accepted',
                'verification_user_already_member' => 'Invitation cannot be accepted, user is already a member of another alliance',
                'verification_unknown_user' => 'The invited user could not be found',
                'success_change_name' => 'New name saved',
                'cannot_change_name' => 'Cannot save new name',
                'success_promote_user' => 'User successfully promoted',
                'cannot_promote_user' => 'Cannot promote this user',
                'success_exclude_user' => 'User successfully excluded',
                'cannot_exclude_user' => 'Cannot exclude this user',
                'success_delete_allaince' => 'Alliance successfully deleted',
                'cannot_delete_alliance' => 'Cannot delete alliance',
                'success_resign_user' => 'You have successfully leave the alliance',
                'cannot_resign_user' => 'Failed to leave alliance',
                'create_user_alliance' => 'Create alliance',
                'admin_role' => 'Admin',
                'member_role' => 'Member',
                'choose_user' => 'Choose user',
                'invite_user' => 'Invite user',
                'delete_alliance' => 'Delete alliance',
                'resign_alliance' => 'Leave alliance',
                'in_alliance' => 'in association',
                'want_delete_alliance' => 'Do you really want to delete the alliance?',
                'want_resign_alliance' => 'Do you really want to leave the alliance?',

                'id' => 'id',
                'user_name' => 'username',
                'role' => 'role',
                'action' => 'action',
                'yes_remove' => 'Yes delete',
                'no_remove' => 'No leave it',
                'yes_resign' => 'Yes leave',
                'no_resign' => 'No stay',
                'really' => 'Are you sure?',
            ]
        );
    }

    protected function i18nLogs(): void
    {
        $modelName = Inflector::id2camel($this->tableName, '_');

        $this->saveTranslation(
            1,
            'admin.log',
            [
                "{$modelName}" => 'Uživatelské sdružení',
                "{$modelName} add {name}" => 'Přidáno sdružení {name}',
                "{$modelName} edit {name}" => 'Upraveno sdružení {name}',
                "{$modelName} remove {name}" => 'Smazáno sdružení {name}',

                'invitation into alliance {name} was accepted by {email}' => 'Pozvánka do sdružení "{name}" byla přijata uživatelem {email}.',
                'member {email} excluded from {name} by {admin}' => 'Uživatel "{email}" vyloučen ze sdružení "{name}" uživatelem {admin}.',
                'member {email} promoted in {name} by {admin}' => 'Uživatel "{email}" povýšen ve sdružení "{name}" uživatelem {admin}.',
                'member {email} resign from {name}' => 'Uživatel {email} vystoupil ze sdružení "{name}"',
                '{type} alliance {name} was sent to email address {email}' => '{type} sdružení "{name}" bylo odesláno na e-mail: {email}',
            ]
        );
        $this->saveTranslation(
            2,
            'admin.log',
            [
                "{$modelName}" => 'User alliance',
                "{$modelName} add {name}" => 'Alliance {name} were added.',
                "{$modelName} edit {name}" => 'Alliance {name} were updated.',
                "{$modelName} remove {name}" => 'Alliance {name} were deleted.',

                'invitation into alliance {name} was accepted by {email}' => 'Invitation into alliance "{name}" was accepted by {email}.',
                'member {email} excluded from {name} by {admin}' => 'User "{email}" excluded from alliance "{name}" by {admin}.',
                'member {email} promoted in {name} by {admin}' => 'User "{email}" promoted in alliance "{name}" by {admin}.',
                'member {email} resign from {name}' => 'User {email} withdrew from the alliance "{name}"',
                '{type} alliance {name} was sent to email address {email}' => '{type} alliance "{name}" was sent to email: {email}',
            ]
        );
    }

    public function i18nUrls() {
        $translations = [
            'cs' => [
                'profile_url' => 'profil',
            ],
            'en' => [
                'profile_url' => 'profile',
            ],
        ];

        $this->saveTranslation(static::CS_LANG_ID, 'frontend.url', $translations['cs']);
        $this->saveTranslation(static::EN_LANG_ID, 'frontend.url', $translations['en']);
    }
}
