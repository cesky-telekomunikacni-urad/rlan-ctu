<?php
/*
* Copyright 2023 DactylGroup s.r.o.
*
* Licensed under the EUPL, Version 1.2 or – as soon they
will be approved by the European Commission - subsequent
versions of the EUPL (the "Licence");
* You may not use this work except in compliance with the
Licence.
* You may obtain a copy of the Licence at:
*
* https://joinup.ec.europa.eu/software/page/eupl5
*
* Unless required by applicable law or agreed to in
writing, software distributed under the Licence is
distributed on an "AS IS" basis,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
express or implied.
* See the Licence for the specific language governing
permissions and limitations under the Licence.
*/
?>


<?php

use dactylcore\core\db\Migration;


class m200823_000001_registration_lang_update extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->translate();
    }

    public function translate()
    {
        $translations = [
            'cs' => [
                'register' => 'Registrace',
            ],
            'en' => [
                'register' => 'Registration',
            ],
        ];

        $this->saveTranslation(static::CS_LANG_ID, 'common.menu_item', $translations['cs']);
        $this->saveTranslation(static::EN_LANG_ID, 'common.menu_item', $translations['en']);

        $translations = [
            'cs' => [
                'angle has to be in range 0 to 360' => 'úhel musí být v intervalu 0 - 360°',
            ],
            'en' => [
                'angle has to be in range 0 to 360' => 'the angle has to be in range 0 - 360°',
            ],
        ];

        $this->saveTranslation(static::CS_LANG_ID, 'common.station', $translations['cs']);
        $this->saveTranslation(static::EN_LANG_ID, 'common.station', $translations['en']);
    }
}
