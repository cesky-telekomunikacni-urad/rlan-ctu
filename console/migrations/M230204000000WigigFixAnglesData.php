<?php
/*
* Copyright 2023 DactylGroup s.r.o.
*
* Licensed under the EUPL, Version 1.2 or – as soon they
will be approved by the European Commission - subsequent
versions of the EUPL (the "Licence");
* You may not use this work except in compliance with the
Licence.
* You may obtain a copy of the Licence at:
*
* https://joinup.ec.europa.eu/software/page/eupl5
*
* Unless required by applicable law or agreed to in
writing, software distributed under the Licence is
distributed on an "AS IS" basis,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
express or implied.
* See the Licence for the specific language governing
permissions and limitations under the Licence.
*/
?>


<?php

use common\models\Station;
use dactylcore\core\db\Migration;
use frontend\controllers\StationController;
use League\Geotools\Coordinate\Coordinate;
use yii\helpers\Console;


class M230204000000WigigFixAnglesData extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $geotools = new \League\Geotools\Geotools();
        $stationsQuery = Station::find()
                                ->select(['lat', 'lng', 'id'])
                                ->where(['type' => 'wigig'])
                                ->asArray()
                                ->with('stationWigig');

        $totalCount = $stationsQuery->count();

        $c = 0;
        foreach ($stationsQuery->each() as $station) {
            $station = (object)$station;

            /**
             * @var $station Station
             * @var $wigig \common\models\StationWigig
             */

            $wigig = (object)$station->stationWigig[0];
            $coordA = new Coordinate([$station->lat, $station->lng]);
            $destinationPoint = $geotools->vertex()->setFrom($coordA)->destination($wigig->direction, 50000);

            $latDiff = abs(round((float)$destinationPoint->getLatitude() - (float)$wigig->angle_point_lat, 5));
            $lngDiff = abs(round((float)$destinationPoint->getLongitude() - (float)$wigig->angle_point_lng, 5));

            $limit = 0.001;
            if ($latDiff > $limit && $lngDiff > $limit) {

                $wigig->angle_point_lat = $destinationPoint->getLatitude();
                $wigig->angle_point_lng = $destinationPoint->getLongitude();

                // Uncomment to debug
                $q
                    = " UPDATE station_wigig SET angle_point_lat={$wigig->angle_point_lat}, angle_point_lng={$wigig->angle_point_lng} WHERE id_station_wigig={$wigig->id_station_wigig}";
                $updated = Yii::$app->db->createCommand($q)->execute();
                Console::output("#{$totalCount}/{$c} .. SID={$station->id} DIR={$wigig->direction} LATDIFF={$latDiff} LNGDIFF={$lngDiff}" .
                    ' ... fixing, result=' .
                    booleanToString($updated));

                $c++;
            }
        }

        Console::output("Summary: {$c} stations fixed");
    }
}
