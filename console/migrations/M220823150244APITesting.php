<?php
/*
* Copyright 2023 DactylGroup s.r.o.
*
* Licensed under the EUPL, Version 1.2 or – as soon they
will be approved by the European Commission - subsequent
versions of the EUPL (the "Licence");
* You may not use this work except in compliance with the
Licence.
* You may obtain a copy of the Licence at:
*
* https://joinup.ec.europa.eu/software/page/eupl5
*
* Unless required by applicable law or agreed to in
writing, software distributed under the Licence is
distributed on an "AS IS" basis,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
express or implied.
* See the Licence for the specific language governing
permissions and limitations under the Licence.
*/
?>


<?php

use dactylcore\core\db\Migration;


class M220823150244APITesting extends Migration
{
    public function safeUp()
    {
        $this->struct();
        $this->config();
        $this->i18n();
    }

    protected function struct()
    {
        $this->addColumn('user', 'can_use_api', $this->boolean()->defaultValue(false)->null());
        // We need update existing urs (for testing purposes)
        $this->update('user', ['can_use_api' => 1], ['in', 'id', [1, 2, 3]]);
    }

    protected function config()
    {
        $this->saveConfig('TESTING_API_MAIN_SITE', '', 'content', 'api-testing', 0, 1);
    }

    protected function i18n()
    {
        $this->saveTranslationCsEn('common.api-testing', [
            'can_use_api' => [
                'API povoleno?',
                'Can use API?',
            ],
            'email_register_subject' => [
                'Registrace do testovacího prostředí API dokončena',
                'Registration to the API testing environment done',
            ],
            'email_register_title' => [
                'Jak se připojit k testování API projektu RLAN',
                'How to join RLAN project API testing',
            ],
            'email_register_body' => [
                '<p>Vítejte v testovacím prostředí API projektu RLAN. Toto prostředí slouží jen a pouze pro otestování funkcionalit API projektu RLAN před vstupem do produkčního prostředí. Cílem je umožnit otestování všech funkcí API při vývoji Vašich nástrojů bez obav o narušení dat produkčního charakteru.</p>
                 <p>Před možností započít testování API je nutné dostat povolení přístupu od správce projektu. Proto prosím kontaktujte správce na adrese <a href="mailto:60ghz@ctu.cz">60ghz@ctu.cz</a> s žádostí o povolení vstupu do API testovacího prostředí.</p>
                 <p>Jakmile Vám správce přístup udělí, budete opět informováni e-mailem. Do té doby je možné nahlížet do dokumentace API na odkaze uvedeném níže.</p>
                 <p>Děkujeme že využíváte naše služby a také za Vaši spolupráci i trpělivost.</p>',
                '<p>Welcome to the API testing environment of the RLAN project. This environment has one and one purpose only, it\'s for testing the API functionality of the RLAN project before entering the production environment. The goal is to enable the testing of all API functions during the development of your tools without worrying about the disruption of production data.</p>
                  <p>Before you can start using the API, you must get access permission from the project admin. Therefore, please contact the administrator at <a href="mailto:60ghz@ctu.cz">60ghz@ctu.cz</a> with a request for permission to enter the API test environment.</p>
                  <p>As soon as the administrator grants you access, you will be informed again by email. Until then, the API documentation can be viewed at the link below.</p>
                  <p>Thank you for using our services and also for your cooperation and patience.</p>',
            ],
            'email_allow_subject' => [
                'Povolení do k použití testovací API uděleno',
                'Allowance to use testing API was granted',
            ],
            'email_allow_title' => [
                'Informace a návody k řádnému otestování API projektu RLAN',
                'Informations and instructions to test API of RLAN project',
            ],
            'email_allow_body' => [
                '<p>Po udělení povolení použití testovací verze API je možné přistoupit k testování. Pro povolení vstupu do API produkčního RLAN systému je vyžadováno otestování minimálně následujících akcí:
<ul>
<li>Registrace stanice</li>
<li>Úprava stanice</li>
<li>Publikace stanice</li>
<li>Úprava poblikované stanice</li>
<li>Publikace čekajících ůprav stanice</li>
<li>Odpublikování stanice</li>
<li>...</li>
</ul>
</p>
<p>Návod na použití API vychází z dokumentace API, jež je dostupná na odkaze uvedeném níže. Doporučujeme tedy řídit se dokumentací a zároveň prosíme, vyvarujte se pro testovací účely použití reálných dat.</p>
<p>Po dokončení testování je nezbytné zažádat administrátory projektu o udělení přístupu do API produkčního prostředí. Proto prosíme kontaktujte správce na adrese  <a href="mailto:60ghz@ctu.cz">60ghz@ctu.cz</a> s žádostí o povolení vstupu do API produkčního prostředí.</p>
<p>Samozřejmě je adresa platná i pro případ, zda máte jakékoli dotazy čí připomínky k testovacímu provozu RLAN API.</p>',
                '<p>After granting permission to use the test version of the API, it is possible to proceed with testing. To allow access to the API of the production RLAN system, testing of at least the following actions is required:
<ul>
<li>Station Registration</li>
<li>Edit station</li>
<li>Station publication</li>
<li>Editing a clicked station</li>
<li>Publication of pending station edits</li>
<li>Unpublishing the station</li>
<li>...</li>
</ul>
</p>
<p>Instructions for using the API are based on the API documentation, which is available at the link below. We therefore recommend following the documentation and please avoid using real data for testing purposes.</p>
<p>After testing is complete, it is necessary to request the project administrators to grant access to the API of the production environment. Therefore, please contact the administrator at <a href="mailto:60ghz@ctu.cz">60ghz@ctu.cz</a> with a request for permission to enter the API production environment.</p>
<p>Of course, the address is also valid in case you have any questions or comments about the RLAN API test operation.</p>',
            ],
            'api_docu' => [
                'API dokumentace',
                'API documentation',
            ],
        ]);

        $this->saveTranslationCsEn('frontend.api-testing', [
            'api_testing_index_title' => [
                'Testování API',
                'API testing',
            ],
            'api_testing_index_content' => [
                'Toto je projekt, kde je možné testovat API komunikační rozhraní <a href="api/v1/docs/">podle dokumentace</a>. Jde o nutnou prerekvizitu pro otevření použití API rozhraní na produkční verzi projektu.',
                'This is a project where it is possible to test the communication interface API <a href="api/v1/docs/">according to the documentation</a>. This is a necessary prerequisite for opening the API interface on the production version of the project.',
            ],
        ]);

        $this->saveTranslationCsEn('admin.config', [
            'api-testing' => [
                'Testování API',
                'API testing',
            ],
            'TESTING_API_MAIN_SITE' => [
                'Stránka zobrazena na FE části.',
                'Page showed on FE part.',
            ],
        ]);
    }
}
