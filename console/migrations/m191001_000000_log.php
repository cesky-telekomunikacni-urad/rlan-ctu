<?php
/*
* Copyright 2023 DactylGroup s.r.o.
*
* Licensed under the EUPL, Version 1.2 or – as soon they
will be approved by the European Commission - subsequent
versions of the EUPL (the "Licence");
* You may not use this work except in compliance with the
Licence.
* You may obtain a copy of the Licence at:
*
* https://joinup.ec.europa.eu/software/page/eupl5
*
* Unless required by applicable law or agreed to in
writing, software distributed under the Licence is
distributed on an "AS IS" basis,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
express or implied.
* See the Licence for the specific language governing
permissions and limitations under the Licence.
*/
?>


<?php

use common\models\StationWigig;
use dactylcore\core\db\Migration;


/**
 * Class m190830_113326_i18n
 */
class m191001_000000_log extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->lang();
    }

    public function lang()
    {
        $translations = [
            'cs' => [
                'Station add {name}' => 'Stanice <b>{name}</b> přidána',
                'Station edit {name}' => 'Stanice <b>{name}</b> upravena',
                'Station delete {name}' => 'Stanice <b>{name}</b> smazána',
                'waiting-finished {name}' => 'Stanice <b>{name}</b> publikována',
                'user {userName} declared station {stationName}' => 'Uživatel <b>{userName}</b> deklaroval nerušení stanice <b>{stationName}</b> a publikoval ji',
                'waiting-finished' => 'Publikace',
                'declare' => 'Deklarace',
                'Station' => 'Stanice',
            ],
            'en' => [
                'Station add {name}' => 'Station <b>{name}</b> added',
                'Station edit {name}' => 'Station <b>{name}</b> modified',
                'Station delete {name}' => 'Station <b>{name}</b> deleted',
                'waiting-finished {name}' => 'Station <b>{name}</b> published',
                'waiting-finished' => 'Publication',
                'Station' => 'Station',
                'declare' => 'Deklarace',
                'user {userName} declared station {stationName}' => 'User <b>{userName}</b>  declared zero disruption and published station <b>{stationName}</b>',
            ]
        ];


        $this->saveTranslation(static::CS_LANG_ID, 'common.log', $translations['cs']);
        $this->saveTranslation(static::EN_LANG_ID, 'common.log', $translations['en']);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m190908_000000_mail_verification cannot be reverted.\n";

        return false;
    }

}
