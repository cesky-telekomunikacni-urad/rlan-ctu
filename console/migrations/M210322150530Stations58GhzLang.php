<?php
/*
* Copyright 2023 DactylGroup s.r.o.
*
* Licensed under the EUPL, Version 1.2 or – as soon they
will be approved by the European Commission - subsequent
versions of the EUPL (the "Licence");
* You may not use this work except in compliance with the
Licence.
* You may obtain a copy of the Licence at:
*
* https://joinup.ec.europa.eu/software/page/eupl5
*
* Unless required by applicable law or agreed to in
writing, software distributed under the Licence is
distributed on an "AS IS" basis,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
express or implied.
* See the Licence for the specific language governing
permissions and limitations under the Licence.
*/
?>


<?php

use dactylcore\core\db\Migration;


class M210322150530Stations58GhzLang extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->lang();
    }


    public function lang() {

        $translations = [
            'cs' => [
                'type_58_ap' => 'WiFi AP 5.8',
                'type_52' => 'WiFi AP 5.2',
            ],
            'en' => [
                'type_58_ap' => 'WiFi AP 5.8',
                'type_52' => 'WiFi AP 5.2',
            ],
        ];

        $this->saveTranslation(self::CS_LANG_ID, 'common.station', $translations['cs']);
        $this->saveTranslation(self::EN_LANG_ID, 'common.station', $translations['en']);

        $translations = [
            'cs' => [
                'type_58_ap' => 'WiFi AP 5.8 GHz',
                'type_52' => 'WiFi AP 5.2 GHz',
            ],
            'en' => [
                'type_58_ap' => 'WiFi AP 5.8 GHz',
                'type_52' => 'WiFi AP 5.2 GHz',
            ],
        ];

        $this->saveTranslation(self::CS_LANG_ID, 'frontend.station', $translations['cs']);
        $this->saveTranslation(self::EN_LANG_ID, 'frontend.station', $translations['en']);
    }
}
