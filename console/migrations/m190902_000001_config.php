<?php
/*
* Copyright 2023 DactylGroup s.r.o.
*
* Licensed under the EUPL, Version 1.2 or – as soon they
will be approved by the European Commission - subsequent
versions of the EUPL (the "Licence");
* You may not use this work except in compliance with the
Licence.
* You may obtain a copy of the Licence at:
*
* https://joinup.ec.europa.eu/software/page/eupl5
*
* Unless required by applicable law or agreed to in
writing, software distributed under the Licence is
distributed on an "AS IS" basis,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
express or implied.
* See the Licence for the specific language governing
permissions and limitations under the Licence.
*/
?>


<?php

use dactylcore\core\db\Migration;


/**
 * Class m190902_000001_config
 */
class m190902_000001_config extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {

        $this->saveConfig('MAIN_PAGE_ID_LOGGED', 0, 'content', 'main-page', 1, 1);
        $this->saveConfig('MAIN_PAGE_ID_NOT_LOGGED', 0, 'content', 'main-page', 1, 1);

        $translations = [
            'cs' => [
                'MAIN_PAGE_ID_LOGGED' => 'Hlavní stránka přihlášeného uživatele',
                'MAIN_PAGE_ID_NOT_LOGGED' => 'Hlavní stránka nepřihlášeného uživatele',
                'main-page' => 'Hlavní stránky',
            ],
            'en' => [
                'MAIN_PAGE_ID_LOGGED' => 'Main page for logged user',
                'MAIN_PAGE_ID_NOT_LOGGED' => 'Main page for not logged user',
            ]
        ];

        $this->saveTranslation(static::CS_LANG_ID, 'admin.config', $translations['cs']);
        $this->saveTranslation(static::EN_LANG_ID, 'admin.config', $translations['en']);


        $translations = [
            'cs' => [
                'main page not defined' => 'Hlavní stránka není definovaná',
            ],
            'en' => [
                'main page not defined' => 'Main page isn not defined',
            ]
        ];

        $this->saveTranslation(static::CS_LANG_ID, 'frontend.static', $translations['cs']);
        $this->saveTranslation(static::EN_LANG_ID, 'frontend.static', $translations['en']);

        $config = [
            'cs' => [
                'dc-system_config_update_content' => 'Obsah',
                'dc-system_config_update_stations' => 'Stanice',
            ],
            'en' => [
            ],
        ];

        $this->saveTranslation(static::CS_LANG_ID, 'admin.access-manager', $config['cs']);
        $this->saveTranslation(static::EN_LANG_ID, 'admin.access-manager', $config['en']);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m190902_000001_config cannot be reverted.\n";

        return false;
    }

}
