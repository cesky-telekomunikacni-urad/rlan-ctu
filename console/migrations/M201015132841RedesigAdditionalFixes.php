<?php
/*
* Copyright 2023 DactylGroup s.r.o.
*
* Licensed under the EUPL, Version 1.2 or – as soon they
will be approved by the European Commission - subsequent
versions of the EUPL (the "Licence");
* You may not use this work except in compliance with the
Licence.
* You may obtain a copy of the Licence at:
*
* https://joinup.ec.europa.eu/software/page/eupl5
*
* Unless required by applicable law or agreed to in
writing, software distributed under the Licence is
distributed on an "AS IS" basis,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
express or implied.
* See the Licence for the specific language governing
permissions and limitations under the Licence.
*/
?>


<?php

use dactylcore\core\db\Migration;


/**
 * Class m190830_113326_i18n
 */
class M201015132841RedesigAdditionalFixes extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->lang();
    }

    protected function lang()
    {
        $translations = [
            'cs' => [
                'You cannot create another station' => 'Již nelze vytvářet další stanice',
                'You have only one station left to create' => 'Zbývá poslední stanice již můžete vytvořit',
                'You have {stations} stations left to create' => 'Zbývá {stations} stanic, jež můžete vytvořit',
                'You have {up4stations} stations left to create' => 'Zbývají {up4stations} stanice, jež můžete vytvořit',

                'You can only have {stations} station in unpublished state.' => 'Můžete mít nanejvýš {stations} nezveřejněnou stanici.',
                'You can only have {up4stations} stations in unpublished state.' => 'Můžete mít nanejvýš {up4stations} nezveřejněné stanice.',
                'You can only have {stations} stations in unpublished state.' => 'Můžete mít nanejvýš {stations} nezveřejněných stanic.',
            ],
            'en' => [
                'You cannot create another station' => 'You cannot create another station',
                'You have only one station left to create' => 'You have only one station left to create',
                'You have {stations} stations left to create' => 'You have {stations} stations left to create',
                'You have {up4stations} stations left to create' => 'You have {up4stations} stations left to create',

                'You can only have {stations} station in unpublished state.' => 'You can only have {stations} station in unpublished state.',
                'You can only have {up4stations} stations in unpublished state.' => 'You can only have {up4stations} stations in unpublished state.',
                'You can only have {stations} stations in unpublished state.' => 'You can only have {stations} stations in unpublished state.',
            ],
        ];

        $this->saveTranslation(static::CS_LANG_ID, 'frontend.station-registration', $translations['cs']);
        $this->saveTranslation(static::EN_LANG_ID, 'frontend.station-registration', $translations['en']);

        $translations = [
            'cs' => [
                'add_wigig_sublabel' => 'MGWS, IEEE802, 11ad, beamforming (s&nbspmitigací)',
                'add_fs_sublabel' => 'Fixed Services, pouze pevné směrové spoje (bez mitigace)',
            ],
            'en' => [
                'add_wigig_sublabel' => 'MGWS, IEEE802, 11ad, beamforming (with&nbspmitigation)',
                'add_fs_sublabel' => 'Fixed Services, only fixed directional connections (without mitigation)',
            ],
        ];

        $this->saveTranslation(static::CS_LANG_ID, 'frontend.station', $translations['cs']);
        $this->saveTranslation(static::EN_LANG_ID, 'frontend.station', $translations['en']);


        $translations = [
            'cs' => [
                'explanations' => 'Vysvětlivky',
                'add_pair' => 'Pár stanic FS - stanice A a stanice B',
                'add_wigig' => 'Stanice WiGiG',
                'add_wigig_sublabel' => 'stanice WiGiG PtP nebo PtMP',
                'add_fs' => 'Stanice FS',
                'add_fs_sublabel' => 'stanice EN 302 217',
            ],
            'en' => [
                'explanations' => 'explanations',
                'add_pair' => 'Pair of FS stations - stations A and station B',
                'add_wigig' => 'WiGiG Station',
                'add_wigig_sublabel' => 'WiGiG PtP or PtMP station',
                'add_fs' => 'FS Station',
                'add_fs_sublabel' => 'EN 302 217 station',
            ],
        ];

        $this->saveTranslation(static::CS_LANG_ID, 'frontend.help-box', $translations['cs']);
        $this->saveTranslation(static::EN_LANG_ID, 'frontend.help-box', $translations['en']);
    }
}
