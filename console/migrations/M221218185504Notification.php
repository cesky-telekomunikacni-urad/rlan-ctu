<?php
/*
* Copyright 2023 DactylGroup s.r.o.
*
* Licensed under the EUPL, Version 1.2 or – as soon they
will be approved by the European Commission - subsequent
versions of the EUPL (the "Licence");
* You may not use this work except in compliance with the
Licence.
* You may obtain a copy of the Licence at:
*
* https://joinup.ec.europa.eu/software/page/eupl5
*
* Unless required by applicable law or agreed to in
writing, software distributed under the Licence is
distributed on an "AS IS" basis,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
express or implied.
* See the Licence for the specific language governing
permissions and limitations under the Licence.
*/
?>


<?php

use dactylcore\core\db\Migration;
use yii\helpers\Inflector;

class M221218185504Notification extends Migration
{
    /**
     * @var string Set table name for CRUD entity you want to create.
     */
    protected string $tableName = 'notification';

    public function safeUp(): bool
    {
        $this->structure();
        $this->permissions();
        $this->i18n();

        return true;
    }

    protected function structure(): void
    {
        $this->createTable($this->tableName, [
            'id' => $this->primaryKey(11)->notNull(),

            'text' => $this->string(1000)->notNull(),
            'displayed_from' => $this->integer()->null(),
            'displayed_to' => $this->integer()->null(),
            'enabled' => $this->tinyInteger(1)->defaultValue(0),

            'deleted' => $this->tinyInteger(1)->notNull()->defaultValue(0),
            'created_at' => $this->integer(11)->notNull(),
            'updated_at' => $this->integer(11)->notNull(),
        ]);
    }

    protected function permissions(): void
    {
        $this->saveUserRolePermissions([
            "{$this->moduleName}_{$this->sectionName}",
            "{$this->moduleName}_{$this->sectionName}_index",
            "{$this->moduleName}_{$this->sectionName}_create",
            "{$this->moduleName}_{$this->sectionName}_update",
            "{$this->moduleName}_{$this->sectionName}_delete",
        ]);
    }

    protected function i18n(): void
    {
        $nameSingularCS = 'Notifikace'; // e.g. Záznam
        $namePluralCS = 'Notifikace'; // e.g. Záznamy
        $nameSingularEN = 'Notification'; // e.g. Record
        $namePluralEN = 'Notifications'; // e.g. Records

        // Access manager permissions
        $this->saveTranslation2([self::CS_LANG_ID, self::EN_LANG_ID], 'admin.access-manager', [
            "{$this->moduleName}_{$this->sectionName}" => [$namePluralCS, $namePluralEN],
            "{$this->moduleName}_{$this->sectionName}_index" => ['Seznam', 'List'],
            "{$this->moduleName}_{$this->sectionName}_create" => ['Vytvořit', 'Create'],
            "{$this->moduleName}_{$this->sectionName}_update" => ['Upravit', 'Update'],
            "{$this->moduleName}_{$this->sectionName}_delete" => ['Odstranit', 'Delete'],
        ]);

        // Admin crud
        $this->saveTranslation2([self::CS_LANG_ID, self::EN_LANG_ID], "admin.{$this->sectionName}", [
            $this->tableNamePluralized => [$namePluralCS, $namePluralEN],
            "create_{$this->tableName}" => ["Nová " . lcfirst($nameSingularCS), "New " . lcfirst($nameSingularEN)],
            "update_{$this->tableName}" => ['Upravit', 'Update'],
            'flash_successfully_created' => [
                "{$nameSingularCS} úspešne vytvorena.",
                "{$nameSingularEN} successfully created.",
            ],
            'flash_successfully_updated' => [
                "{$nameSingularCS} úspešne upravena.",
                "{$nameSingularEN} successfully updated.",
            ],
            'flash_successfully_deleted' => [
                "{$nameSingularCS} úspešne smazána.",
                "{$nameSingularEN} successfully deleted.",
            ],
        ]);

        // Model attributes labels
        $this->saveTranslation2([self::CS_LANG_ID, self::EN_LANG_ID], "common.{$this->sectionName}", [
            'text' => ['Text', 'Text'],
            'displayed_from' => ['Zobrazovat od', 'Display from'],
            'displayed_to' => ['Zobrazovat do', 'Display to'],
            'enabled' => ['Zapnuto', 'Enabled'],
            'disabled' => ['Vypnuto', 'Disabled'],
        ]);

        // Admin logs
        $this->saveTranslation2([self::CS_LANG_ID, self::EN_LANG_ID], 'admin.log', [
            "{$this->modelName}" => [$nameSingularCS, $nameSingularEN],
            "{$this->modelName} add {name}" => [
                "{$nameSingularCS} <b>{name}</b> přidána",
                "{$nameSingularEN} <b>{name}</b> added",
            ],
            "{$this->modelName} edit {name}" => [
                "{$nameSingularCS} <b>{name}</b> editována",
                "{$nameSingularEN} <b>{name}</b> edited",
            ],
            "{$this->modelName} remove {name}" => [
                "{$nameSingularCS} <b>{name}</b> smazána",
                "{$nameSingularEN} <b>{name}</b> deleted",
            ],
        ]);
    }
}