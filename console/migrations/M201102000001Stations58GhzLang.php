<?php
/*
* Copyright 2023 DactylGroup s.r.o.
*
* Licensed under the EUPL, Version 1.2 or – as soon they
will be approved by the European Commission - subsequent
versions of the EUPL (the "Licence");
* You may not use this work except in compliance with the
Licence.
* You may obtain a copy of the Licence at:
*
* https://joinup.ec.europa.eu/software/page/eupl5
*
* Unless required by applicable law or agreed to in
writing, software distributed under the Licence is
distributed on an "AS IS" basis,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
express or implied.
* See the Licence for the specific language governing
permissions and limitations under the Licence.
*/
?>


<?php

use dactylcore\core\db\Migration;


class M201102000001Stations58GhzLang extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->lang();
    }


    public function lang() {

        $translations = [
            'cs' => [
                'Invalid station type' => 'Neplatný typ stanice',
                'type_58' => '5.8 GHz',
                'type_58_ap' => 'Přístupový bod',
                'type_58_tg' => 'Mýtná brána',
                'type_52' => '5.2 GHz',
                'add_tg_sublabel' => '',
                'add_ap_sublabel' => '',
                'What is ssid of access point' => 'Jaké je SSID přístupového bodu?',
                'ssid' => 'SSID',
                'ssid hint' => 'Název přístupového bodu',
                'What is MAC address of access point' => 'Jaká je MAC adresa přístupového bodu?',
                'untitled access point' => 'Nepojmenovaný přístupový bod',
                'untitled toll gate' => 'Nepojmenovaná mýtná brána',
                'What is toll gate name' => 'Jaký je název mýtné brány?',
                'access points' => 'Přístupové body',
                'toll gates' => 'Mýtné brány',
                'diameter' => 'Poloměr',
                '58ghz' => '5.8 GHz Stanice',
                'Cannot place here' => 'Stanici zde nelze umístnit',
                'This AP is in conflict, please move it.' => 'Váš přístupový bod je v konfliktu s jednou nebo více mýtních brán, přesuňte ho prosím.'
            ],
            'en' => [
                'add_tg_sublabel' => '',
                'add_ap_sublabel' => '',
                'Invalid station type' => 'Invalid station type',
                'type_58' => '5.8 GHz',
                'type_58_ap' => 'Access point',
                'type_58_tg' => 'Toll gate',
                'type_52' => '5.2 GHz',
                'What is ssid of access point' => 'What is the SSID of the Access Point?',
                'ssid' => 'SSID',
                'ssid hint' => 'Name of the access point',
                'What is MAC address of access point' => 'What is the MAC address of the Access Point?',
                'untitled access point' => 'Untitled access point',
                'untitled toll gate' => 'Untitled toll gate',
                'What is toll gate name' => 'What is the toll gate name?',
                'access points' => 'Access points',
                'toll gates' => 'Toll gates',
                'diameter' => 'Diameter',
                '58ghz' => '5.8 GHz Stations',
                'Cannot place here' => 'Cannot place the station here',
                'This AP is in conflict, please move it.' => 'Your access point is in conflict with one or more toll gates, reposition in please.'
            ],
        ];

        $this->saveTranslation(self::CS_LANG_ID, 'common.station', $translations['cs']);
        $this->saveTranslation(self::EN_LANG_ID, 'common.station', $translations['en']);

        $translations = [
            'cs' => [
                'station/create-ap' => 'stanice/vytvorit-ap',
                'station/create-tg' => 'stanice/vytvorit-branu',
                'station/create-52-ghz' => 'stanice/vytvorit-52-ghz',
            ],
            'en' => [
                'station/create-ap' => 'station/create-ap',
                'station/create-tg' => 'station/create-gate',
                'station/create-52-ghz' => 'station/create-52-ghz',
            ],
        ];

        $this->saveTranslation(self::CS_LANG_ID, 'frontend.url', $translations['cs']);
        $this->saveTranslation(self::EN_LANG_ID, 'frontend.url', $translations['en']);

        $translations = [
            'cs' => [
                'app-frontend_station_add_toll_gate' => 'Přidat mýtní bránu',
            ],
            'en' => [
                'app-frontend_station_add_toll_gate' => 'Add toll gate',
            ],
        ];

        $this->saveTranslation(self::CS_LANG_ID, 'admin.access-manager', $translations['cs']);
        $this->saveTranslation(self::EN_LANG_ID, 'admin.access-manager', $translations['en']);

    }
}
