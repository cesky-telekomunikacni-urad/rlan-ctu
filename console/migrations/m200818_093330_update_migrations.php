<?php
/*
* Copyright 2023 DactylGroup s.r.o.
*
* Licensed under the EUPL, Version 1.2 or – as soon they
will be approved by the European Commission - subsequent
versions of the EUPL (the "Licence");
* You may not use this work except in compliance with the
Licence.
* You may obtain a copy of the Licence at:
*
* https://joinup.ec.europa.eu/software/page/eupl5
*
* Unless required by applicable law or agreed to in
writing, software distributed under the Licence is
distributed on an "AS IS" basis,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
express or implied.
* See the Licence for the specific language governing
permissions and limitations under the Licence.
*/
?>


<?php

use dactylcore\core\db\Migration;


class m200818_093330_update_migrations extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->lang();
    }

    public function lang()
    {
        $translations = [
            'cs' => [
                'password_hint' => 'Heslo musí obsahovat alespoň 8 znaků, velká a malá písmena, číslice a jeden ze speciálních znaků: !@,#=%^&(_?-\'+<.>):*;\\$"/'
            ],
            'en' => [
                'password_hint' => 'Password must contain at least 8 characters, capital and lowercase letters, digits and one of special characters: !@,#=%^&(_?-\'+<.>):*;\\$"/'
            ]
        ];

        $this->saveTranslation(static::CS_LANG_ID, 'frontend.user', $translations['cs']);
        $this->saveTranslation(static::EN_LANG_ID, 'frontend.user', $translations['en']);


        $translations = [
            'cs' => [
                'password_placeholder' => 'Heslo',
                'email_placeholder' => 'E-mail',
            ],
            'en' => [
                'password_placeholder' => 'Password',
                'email_placeholder' => 'Email',
            ],
        ];

        $this->saveTranslation(static::CS_LANG_ID, 'common.register_placeholder', $translations['cs']);
        $this->saveTranslation(static::EN_LANG_ID, 'common.register_placeholder', $translations['en']);

        $translations = [
            'cs' => [
                'reset_password_button' => 'Nemohu se přihlásit',
            ],
            'en' => [
                'reset_password_button' => 'I cannot log in',
            ],
        ];

        $this->saveTranslation(static::CS_LANG_ID, 'common.register', $translations['cs']);
        $this->saveTranslation(static::EN_LANG_ID, 'common.register', $translations['en']);
    }
}
