<?php
/*
* Copyright 2023 DactylGroup s.r.o.
*
* Licensed under the EUPL, Version 1.2 or – as soon they
will be approved by the European Commission - subsequent
versions of the EUPL (the "Licence");
* You may not use this work except in compliance with the
Licence.
* You may obtain a copy of the Licence at:
*
* https://joinup.ec.europa.eu/software/page/eupl5
*
* Unless required by applicable law or agreed to in
writing, software distributed under the Licence is
distributed on an "AS IS" basis,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
express or implied.
* See the Licence for the specific language governing
permissions and limitations under the Licence.
*/
?>


<?php

use dactylcore\core\db\Migration;


/**
 * Class m190830_113326_i18n
 */
class m200925_000000_expire extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->lang();
    }

    protected function lang()
    {
        $translations = [
            'cs' => [
                'Station is about to expire' => 'Vaše stanice brzy expiruje',
                'Station has expired' => 'Vaše stanice již expirovala',
                'You have to renew this station' => 'Vaše stanice již není aktivní. Můžete se pokusit i znovuaktivovat.',
                'You can renew station validity and protection period.' => 'Pokud chcete nadále provozovat tuto stanici, pak jí prodlužte datum platnosti',
                'declare and prolong' => 'Deklarovat a prodloužit',
                'delete' => 'smazat',
                'cancel delete' => 'Ne, ponechat',
                'ok delete' => 'Ano, smazat',
                'confirm' => 'Opravdu?',
                'prolong' => 'prodloužit',

                'prolong 1 station' => 'prodloužit 1 stanici',
                'prolong {number} station two four' => 'prodloužit {number} stanice',
                'prolong {number} station five' => 'prodloužit {number} stanic',

                'stations successfully prolonged' => 'Stanice úspěšně prodlouženy',
                'stations failed to be prolonged' => 'Při prodloužení stanic došlo k chybě. Pokuste se prosím neprodloužené stanice upravit manuálně.',
            ],
            'en' => [
                'Station is about to expire' => 'Station is about to expire',
                'Station has expired' => 'Station has expired',
                'You have to renew this station' => 'Your station is not active anymore. You can try to renew it.',
                'You can renew station validity and protection period.' => 'You can renew station validity and protection period.',
                'declare and prolong' => 'Declare as isolated and prolong',
                'delete' => 'delete',
                'prolong' => 'prolong',

                'prolong 1 station' => 'prolong 1 station',
                'prolong {number} station two four' => 'prolong {number} stations',
                'prolong {number} station five' => 'prolong {number} stations',


                'stations successfully prolonged' => 'Stations successfully prolonged',
                'stations failed to be prolonged' => 'Stations failed to be prolonged. Try check the unprolonged stations manually please.',
            ],
        ];

        $this->saveTranslation(static::CS_LANG_ID, 'common.station', $translations['cs']);
        $this->saveTranslation(static::EN_LANG_ID, 'common.station', $translations['en']);
    }
}
