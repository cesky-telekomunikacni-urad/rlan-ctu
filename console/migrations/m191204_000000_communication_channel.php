<?php
/*
* Copyright 2023 DactylGroup s.r.o.
*
* Licensed under the EUPL, Version 1.2 or – as soon they
will be approved by the European Commission - subsequent
versions of the EUPL (the "Licence");
* You may not use this work except in compliance with the
Licence.
* You may obtain a copy of the Licence at:
*
* https://joinup.ec.europa.eu/software/page/eupl5
*
* Unless required by applicable law or agreed to in
writing, software distributed under the Licence is
distributed on an "AS IS" basis,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
express or implied.
* See the Licence for the specific language governing
permissions and limitations under the Licence.
*/
?>


<?php

use dactylcore\core\db\Migration;


/**
 * Class m190830_113326_i18n
 */
class m191204_000000_communication_channel extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->struct();
        $this->lang();
    }

    public function struct()
    {
        $this->createTable("thread", [
            'id' => $this->primaryKey(11),

            'id_recipient' => $this->integer(12)->null(),
            'id_sender' => $this->integer(11)->null(),

            // a station we are talking about (new station)
            'id_station' => $this->integer(11)->notNull(),
            // a station which the "id_station" is in conflict with
            'id_station_disturber' => $this->integer(11)->null(),

            'show_sender_info' => $this->boolean()->defaultValue(false),
            'show_recipient_info' => $this->boolean()->defaultValue(false),
            'reason' => $this->string(255)->notNull()->defaultValue('generic'), // A or B

            'subject' => $this->string(255)->null(),

            'status_sender' => $this->string(255)->notNull()->defaultValue('new'),
            'status_recipient' => $this->string(255)->notNull()->defaultValue('new'),

            'deleted' => $this->integer(1)->notNull()->defaultValue(0),
            'created_at' => $this->integer(11)->null(),
            'updated_at' => $this->integer(11)->null(),

        ], static::$TABLE_OPTIONS);


        $this->createTable("message", [
            'id' => $this->primaryKey(11),

            'id_author' => $this->integer(12)->null(),
            'id_thread' => $this->integer(12)->null(),
            'content' => $this->longText()->null(),
            'status' => $this->string(255)->notNull()->defaultValue('new'),

            'deleted' => $this->integer(1)->notNull()->defaultValue(0),
            'created_at' => $this->integer(11)->null(),
            'updated_at' => $this->integer(11)->null(),
        ], static::$TABLE_OPTIONS);

        $this->addForeignKey('fk_id_recipient', 'thread', 'id_recipient', 'user', 'id', 'no action', 'no action');
        $this->addForeignKey('fk_id_sender', 'thread', 'id_sender', 'user', 'id', 'no action', 'no action');

        $this->addForeignKey('fk_id_station', 'thread', 'id_station', 'station', 'id', 'no action', 'no action');
        $this->addForeignKey('fk_id_station_disturber', 'thread', 'id_station_disturber', 'station', 'id', 'no action', 'no action');

        $this->addForeignKey('fk_id_author', 'message', 'id_author', 'user', 'id', 'no action', 'no action');
        $this->addForeignKey('fk_id_thread', 'message', 'id_thread', 'thread', 'id', 'no action', 'no action');
    }


    protected function lang()
    {
        $translations = [
            'cs' => [
                'id_recipient' => 'Adresát',
                'id_sender' => 'Odesílatel',
                'id_station' => 'Předmětná stanice',
                'id_station_disturber' => 'Rušící stanice',
                'reason' => 'Důvod',
                'subject' => 'Předmět',
                'status' => 'Stav',

                'id_author' => 'Autor',
                'id_thread' => 'Konvezace',
                'content' => 'Zpráva',
                'station' => 'stanice',
                'conflict station' => 'konflikt s',

                'status_new' => 'nová',
                'status_read' => 'přečteno',
                'status_archived' => 'archivováno',
                'user' => 'diskutující',
                'anonymous user {id}' => 'anonymní uživatel {id}',

                'send message' => 'Odeslat',
                'message has been send' => 'zpráva odeslána',
                'error while sending message' => 'chyba odesílaní zprávy',
                'message on station {stationName}' => 'zpráva u stanice {stationName}',
                'message on disturbing {stationName} by {stationAggressorName}' => 'zpráva u konfliktu stanice {stationName} se stanicí {stationAggressorName}',
                'contact owner' => 'kontaktujte majitele',
                'show_sender_info' => 'zobrazit osobní údaj - moji e-mailovou adresu',
                'show_recipient_info' => 'zobrazit osobní údaj - moji e-mailovou adresu',
                'contact owner help' => 'kontaktovat majitele stanice',
                'contact owner disturbance' => 'kontaktovat provozovatele Stanice, kde je indikován konflikt.',
                'open discussion disturbance' => 'otevřít diskuzi s majitelem konfliktní stanice',
                'owner has been contacted' => 'majitel stanice byl kontaktován',
                'portalName' => '60ghz.ctu.cz',
                'disturbing station {stationLink}' => 'konfliktní stanice {stationLink}',
                'discussed station {stationLink}' => 'diskutovaná stanice {stationLink}',
                'response' => 'odpovědět',
                'new message from {userName}' => 'nová zpráva od {userName}',
                'station doesnt exist' => 'stanice už neexistuje',
                'user doesnt exist' => 'uživatel už neexistuje',
                'default contact message from {victimStation} owner to {aggressorStation} owner. login to {url}' => 'Vážený/á uživateli/ko Portálu 60 GHz,<br>
moje nově instalovaná stanice {victimStation} je v technologickém konfliktu s Vaší stanicí {aggressorStation}. Chtěl/a bych Vás touto cestou požádat o změnu parametrů, např. snížení požadavku na C/I u Vámi provozované stanice. Změnu můžete provést přihlášením na {url}.
<br>
Děkuji
',
            ],
            'en' => [

            ],
        ];
        $this->saveTranslation(static::CS_LANG_ID, 'common.messaging', $translations['cs']);
        $this->saveTranslation(static::EN_LANG_ID, 'common.messaging', $translations['en']);
    }


    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m190908_000000_mail_verification cannot be reverted.\n";

        return false;
    }

}
