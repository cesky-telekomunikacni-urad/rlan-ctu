<?php
/*
* Copyright 2023 DactylGroup s.r.o.
*
* Licensed under the EUPL, Version 1.2 or – as soon they
will be approved by the European Commission - subsequent
versions of the EUPL (the "Licence");
* You may not use this work except in compliance with the
Licence.
* You may obtain a copy of the Licence at:
*
* https://joinup.ec.europa.eu/software/page/eupl5
*
* Unless required by applicable law or agreed to in
writing, software distributed under the Licence is
distributed on an "AS IS" basis,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
express or implied.
* See the Licence for the specific language governing
permissions and limitations under the Licence.
*/
?>


<?php

use dactylcore\core\db\Migration;


class M201102000000Stations52Ghz extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->struct();
    }

    public function struct()
    {
        $this->createTable("station_52", [
            'id_station_52' => $this->primaryKey(11),
            'id_station' => $this->integer(11)->notNull(),

            'deleted' => $this->integer(1)->notNull()->defaultValue(0),
            'created_at' => $this->integer(11)->null(),
            'updated_at' => $this->integer(11)->null(),
        ], static::$TABLE_OPTIONS);

        $this->addForeignKey('fk_station_station_52', 'station_52', 'id_station', 'station', 'id', 'no action', 'no action');
    }

}
