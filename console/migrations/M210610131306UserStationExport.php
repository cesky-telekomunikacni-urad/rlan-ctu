<?php
/*
* Copyright 2023 DactylGroup s.r.o.
*
* Licensed under the EUPL, Version 1.2 or – as soon they
will be approved by the European Commission - subsequent
versions of the EUPL (the "Licence");
* You may not use this work except in compliance with the
Licence.
* You may obtain a copy of the Licence at:
*
* https://joinup.ec.europa.eu/software/page/eupl5
*
* Unless required by applicable law or agreed to in
writing, software distributed under the Licence is
distributed on an "AS IS" basis,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
express or implied.
* See the Licence for the specific language governing
permissions and limitations under the Licence.
*/
?>


<?php

use dactylcore\core\db\Migration;


class M210610131306UserStationExport extends Migration
{
    public function safeUp() {
        $this->i18nUrls();
        $this->translate();
    }

    public function translate()
    {
        $translations = [
            'cs' => [
                'export' => 'export všech stanic',
                'station_export' => 'export',
                'export_help_text' => 'Vyexportuje seznam Vašich stanic do souboru formátu XLSX, který lze obratem importovat (slouží pro drobné úpravy stanic).<br><br>Po spuštění procesu bude generování exportu nějakou dobu trvat (až 5 minut). Prosíme tedy o trpělivost.',
                'export_submit_button' => 'Spustit export stanic',
                'non_existing_user_error' => 'Nebyl zadán uživatel, nebo neexistuje.',
                'export_file_not_exists' => 'Požadovaný export neexistuje. Zkuste jej vygenerovat prosím znovu.',
                'export_success_alert' => 'export úspěšně dokončen',
                'export_fail_alert' => 'Export se nepodařilo vytvořit. Zkuste to prosím znovu, nebo kontaktujte podporu.',
                'stations' => 'stanice',
            ],
            'en' => [
                'export' => 'export of all stations',
                'station_export' => 'export',
                'export_help_text' => 'This will export the list of all your stations in XLSX file format, which can be imported again.<br><br>After start of the process export will take some time (max 5 minutes). So please be patient.',
                'export_submit_button' => 'Start the station export',
                'non_existing_user_error' => 'User not filled or not existed.',
                'export_file_not_exists' => 'Required export file does not exists. Try again later.',
                'export_success_alert' => 'export successfully ended',
                'export_fail_alert' => 'Export file cannot be created. Try again later or contact the support.',
                'stations' => 'stations',
            ],
        ];

        $this->saveTranslation(static::CS_LANG_ID, 'frontend.user-export', $translations['cs']);
        $this->saveTranslation(static::EN_LANG_ID, 'frontend.user-export', $translations['en']);
    }

    public function i18nUrls() {
        $translations = [
            'cs' => [
                'stations_export_url' => 'profil/export-stanic',
                'stations_import_url' => 'profil/import-stanic',
            ],
            'en' => [
                'stations_export_url' => 'profile/stations-export',
                'stations_import_url' => 'profile/stations-import',
            ],
        ];

        $this->saveTranslation(static::CS_LANG_ID, 'frontend.url', $translations['cs']);
        $this->saveTranslation(static::EN_LANG_ID, 'frontend.url', $translations['en']);
    }
}
