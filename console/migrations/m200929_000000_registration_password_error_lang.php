<?php
/*
* Copyright 2023 DactylGroup s.r.o.
*
* Licensed under the EUPL, Version 1.2 or – as soon they
will be approved by the European Commission - subsequent
versions of the EUPL (the "Licence");
* You may not use this work except in compliance with the
Licence.
* You may obtain a copy of the Licence at:
*
* https://joinup.ec.europa.eu/software/page/eupl5
*
* Unless required by applicable law or agreed to in
writing, software distributed under the Licence is
distributed on an "AS IS" basis,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
express or implied.
* See the Licence for the specific language governing
permissions and limitations under the Licence.
*/
?>


<?php

use dactylcore\core\db\Migration;


class m200929_000000_registration_password_error_lang extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->lang();
    }

    protected function lang()
    {
        $translations = [
            'cs' => [
                'password_must_contain' => 'Heslo musí obsahovat:',
                'lower_letters' => 'malá písmena',
                'capital_letters' => 'velká písmena',
                'digits' => 'číslice',
                'special_chars_{chars}' => 'speciální znaky {chars}',
                'length_{minLength}' => 'Minimální počet znaků: {minLength}',
            ],
            'en' => [
                'password_must_contain' => 'Password must contain:',
                'lower_letters' => 'lower letters',
                'capital_letters' => 'captital letters',
                'digits' => 'digits',
                'special_chars_{chars}' => 'special characters {chars}',
                'length_{minLength}' => 'Minimal length: {minLength}',
            ],
        ];

        $this->saveTranslation(static::CS_LANG_ID, 'frontend.password-format', $translations['cs']);
        $this->saveTranslation(static::EN_LANG_ID, 'frontend.password-format', $translations['en']);
    }
}
