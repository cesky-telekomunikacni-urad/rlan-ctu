<?php
/*
* Copyright 2023 DactylGroup s.r.o.
*
* Licensed under the EUPL, Version 1.2 or – as soon they
will be approved by the European Commission - subsequent
versions of the EUPL (the "Licence");
* You may not use this work except in compliance with the
Licence.
* You may obtain a copy of the Licence at:
*
* https://joinup.ec.europa.eu/software/page/eupl5
*
* Unless required by applicable law or agreed to in
writing, software distributed under the Licence is
distributed on an "AS IS" basis,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
express or implied.
* See the Licence for the specific language governing
permissions and limitations under the Licence.
*/
?>


<?php

use dactylcore\core\db\Migration;


class M201026000001StationLangFix extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->lang();
    }

    public function lang()
    {
        $translations = [
            'cs' => [
                'add wigig' => 'Přidat novou WiGig PtP nebo PtMP stanici',
                'import wigig' => 'Importovat WiGig PtP nebo PtMP stanici',
                'next step' => 'Pokračovat',
                'antenna gain has to be in range 0 to 60' => 'Zadaný zisk antény musí být v intervalu 0-60 dBi.',
                'all' => 'všechny',
                'my' => 'moje',
                'show' => 'zobraz',
            ],
            'en' => [
                'antenna gain has to be in range 30 to 60' => 'The gain of the antenna must be between 30 dBi and 60 dBi.',
                'antenna gain has to be in range 0 to 60' => 'Antenna gain has to be in range 0 to 60.',
                'all' => 'all.',
                'my' => 'my',
                'show' => 'show',
            ],
        ];

        $this->saveTranslation(static::CS_LANG_ID, 'frontend.station', $translations['cs']);
        $this->saveTranslation(static::EN_LANG_ID, 'frontend.station', $translations['en']);
    }

}
