<?php
/*
* Copyright 2023 DactylGroup s.r.o.
*
* Licensed under the EUPL, Version 1.2 or – as soon they
will be approved by the European Commission - subsequent
versions of the EUPL (the "Licence");
* You may not use this work except in compliance with the
Licence.
* You may obtain a copy of the Licence at:
*
* https://joinup.ec.europa.eu/software/page/eupl5
*
* Unless required by applicable law or agreed to in
writing, software distributed under the Licence is
distributed on an "AS IS" basis,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
express or implied.
* See the Licence for the specific language governing
permissions and limitations under the Licence.
*/
?>


<?php

use dactylcore\core\db\Migration;


/**
 * Class m190830_113326_i18n
 */
class m191128_000002_data_restriction extends Migration
{
    public function safeUp()
    {
        $this->lang();
    }


    protected function lang()
    {
        $translations = [
            'cs' => [
                'frequency has to be in range 57GHz to 66Ghz' => 'Střední kmitočet musít být v intervalu 57 000 MHz až 66 000 MHz',

                'antenna power has to be in range 30 to 60' => 'Zisk antény musí být v intervalu 30 dBi až 60 dBi',
                'antenna power has to be in range 0 to 60' => 'Zisk antény musí být v intervalu 0 dBi až 60 dBi',

                'eirp has to be in range -20 to 40' => 'E.I.R.P pro PTMP musí být v intervalu -20 dBm až 40 dBm',
                'eirp has to be in range -20 to 55' => 'E.I.R.P pro PTP musí být v intervalu -20 dBm až 55 dBm',

                'channel_width has to be in range 50 to 2200' => 'Šířka kanálu musí být v intervalu 50 MHz až 2200 MHz',
            ],
            'en' => [
            ]
        ];

        $this->saveTranslation(static::CS_LANG_ID, 'frontend.station', $translations['cs']);
        $this->saveTranslation(static::EN_LANG_ID, 'frontend.station', $translations['en']);
    }
}