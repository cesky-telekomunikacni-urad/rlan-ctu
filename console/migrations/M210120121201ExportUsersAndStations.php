<?php
/*
* Copyright 2023 DactylGroup s.r.o.
*
* Licensed under the EUPL, Version 1.2 or – as soon they
will be approved by the European Commission - subsequent
versions of the EUPL (the "Licence");
* You may not use this work except in compliance with the
Licence.
* You may obtain a copy of the Licence at:
*
* https://joinup.ec.europa.eu/software/page/eupl5
*
* Unless required by applicable law or agreed to in
writing, software distributed under the Licence is
distributed on an "AS IS" basis,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
express or implied.
* See the Licence for the specific language governing
permissions and limitations under the Licence.
*/
?>


<?php

use dactylcore\core\db\Migration;


class M210120121201ExportUsersAndStations extends Migration
{
    public function safeUp()
    {
        $this->perms();
        $this->trans();
    }

    protected function perms()
    {
        $this->saveUserRolePermissions(['dc-user_user_export']);
        $this->saveUserRolePermissions(['app-admin_station_export']);
    }

    protected function trans()
    {
        $translations = [
            'cs' => [
                'export_all' => 'Export do XLS',
            ],
            'en' => [
                'export_all' => 'Export into XLS',
            ],
        ];
        $this->saveTranslation(static::CS_LANG_ID, 'admin.user', $translations['cs']);
        $this->saveTranslation(static::EN_LANG_ID, 'admin.user', $translations['en']);

        $translations = [
            'cs' => [
                'export_all' => 'Export do XLS',
                'export_request_info' => 'Export bude připraven za pár minut. Budete informováni e-mailem.',
                'export_finished_email_subject' => 'Export všech stanic je připraven ke stažení',
                'export_finished_email_body__{downloadLink}' => 'Export všech stanic stáhnete zde:<br><br>{downloadLink}',
            ],
            'en' => [
                'export_all' => 'Export into XLS',
                'export_request_info' => 'Export will be ready in a few minutes. You will be informed to email.',
                'export_finished_email_subject' => 'All stations export is ready to download',
                'export_finished_email_body__{downloadLink}' => 'All stations export is ready here:<br><br>{downloadLink}',
            ],
        ];
        $this->saveTranslation(static::CS_LANG_ID, 'common.station', $translations['cs']);
        $this->saveTranslation(static::EN_LANG_ID, 'common.station', $translations['en']);

        $config = [
            'cs' => [
                'dc-user_user_export' => 'Export',
                'app-admin_station_export' => 'Export',
            ],
            'en' => [
                'dc-user_user_export' => 'Export',
                'app-admin_station_export' => 'Export',
            ],
        ];
        $this->saveTranslation(static::CS_LANG_ID, 'admin.access-manager', $config['cs']);
        $this->saveTranslation(static::EN_LANG_ID, 'admin.access-manager', $config['en']);
    }
}
