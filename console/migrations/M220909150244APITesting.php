<?php
/*
* Copyright 2023 DactylGroup s.r.o.
*
* Licensed under the EUPL, Version 1.2 or – as soon they
will be approved by the European Commission - subsequent
versions of the EUPL (the "Licence");
* You may not use this work except in compliance with the
Licence.
* You may obtain a copy of the Licence at:
*
* https://joinup.ec.europa.eu/software/page/eupl5
*
* Unless required by applicable law or agreed to in
writing, software distributed under the Licence is
distributed on an "AS IS" basis,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
express or implied.
* See the Licence for the specific language governing
permissions and limitations under the Licence.
*/
?>


<?php

use dactylcore\core\db\Migration;


class M220909150244APITesting extends Migration
{
    public function safeUp()
    {
        $this->i18n();
    }

    protected function i18n()
    {
        $this->saveTranslationCsEn('common.api-testing', [
            'email_register_body' => [
                '<p>Vítejte v testovacím prostředí API projektu RLAN. Toto prostředí slouží jen a pouze pro otestování funkcionalit API projektu RLAN před vstupem do produkčního prostředí. Cílem je umožnit otestování všech funkcí API při vývoji Vašich nástrojů bez obav o narušení dat produkčního charakteru.</p>
                 <p>Před možností započít testování API je nutné dostat povolení přístupu od správce projektu, který tak učiní do pár dní.</p>
                 <p>Jakmile Vám správce přístup udělí, budete opět informováni e-mailem. Do té doby je možné nahlížet do dokumentace API na odkaze uvedeném níže.</p>
                 <p>Děkujeme že využíváte naše služby a také za Vaši spolupráci i trpělivost.</p>',

                '<p>Welcome to the API testing environment of the RLAN project. This environment has one and one purpose only, it\'s for testing the API functionality of the RLAN project before entering the production environment. The goal is to enable the testing of all API functions during the development of your tools without worrying about the disruption of production data.</p>
                  <p>Before you can start using the API, you must get access permission from the project admin who will allow it in a few days.</p>
                  <p>As soon as the administrator grants you access, you will be informed again by email. Until then, the API documentation can be viewed at the link below.</p>
                  <p>Thank you for using our services and also for your cooperation and patience.</p>',
            ],
            'email_notifyAdminAboutNewApiUser_subject' => [
                'Nová registrace do testovacího prostředí API',
                'New registration to the API testing environment',
            ],
            'email_notifyAdminAboutNewApiUser_title' => [
                'Nová registrace do testovacího prostředí API',
                'New registration to the API testing environment',
            ],
            'email_notifyAdminAboutNewApiUser_content_{userEmail}_{userId}' => [
                '<p>Nový užívatel<br><strong>ID: {userId}<br>Email: {userEmail}</strong><br><br>vás žádá o povolení přístupu k testovacímu API.</p>',
                '<p>New user<br><strong>ID: {userId}<br>Email: {userEmail}</strong><br><br>asks you to allow access to the test API.</p>',
            ],
            'go_to_user_edit' => [
                'Jdi na úpravu uživatele',
                'Go to user edit',
            ],
        ]);
    }
}
