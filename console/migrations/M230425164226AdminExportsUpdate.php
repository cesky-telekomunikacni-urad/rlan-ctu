<?php
/*
* Copyright 2023 DactylGroup s.r.o.
*
* Licensed under the EUPL, Version 1.2 or – as soon they
will be approved by the European Commission - subsequent
versions of the EUPL (the "Licence");
* You may not use this work except in compliance with the
Licence.
* You may obtain a copy of the Licence at:
*
* https://joinup.ec.europa.eu/software/page/eupl5
*
* Unless required by applicable law or agreed to in
writing, software distributed under the Licence is
distributed on an "AS IS" basis,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
express or implied.
* See the Licence for the specific language governing
permissions and limitations under the Licence.
*/
?>


<?php

use dactylcore\core\db\Migration;

class M230425164226AdminExportsUpdate extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $translations = [
            'cs' => [
                'export_all_csv' => 'Export do CSV',
                'move_to_new_owner' => 'Převést stanice pod jiného majitele',
                'station_move_new_owner_info' => 'Vyberte nového majitele pro vyfiltrované stanice ({count} stanic/e):',
                'change_owner_to_selected' => 'Převést stanice pod jiného majitele',
                'flash_successfully_moved' => 'Stanice byly úspěšně převedeny.',
                'confirm_station_move' => 'Opravdu chcete převést vybrané stanice pod uvedeného majitele?',
                'yes_move' => 'Přesunout',
            ],
            'en' => [
                'export_all_csv' => 'Export into CSV',
                'move_to_new_owner' => 'Move stations to new owner',
                'station_move_new_owner_info' => 'Select new owner for filtered stations ({count} stanic/e):',
                'change_owner_to_selected' => 'Move stations to new owner',
                'flash_successfully_moved' => 'Stations were successfully moved.',
                'confirm_station_move' => 'Do you really want to move selected stations to the specified owner?',
                'yes_move' => 'Move',
            ],
        ];
        $this->saveTranslation(static::CS_LANG_ID, 'common.station', $translations['cs']);
        $this->saveTranslation(static::EN_LANG_ID, 'common.station', $translations['en']);

        $translations = [
            'cs' => [
                'export_finished_email_body' => 'Uživatelé byly exportováni a jsou dostupní zde:<br><br>{downloadLink}<br><br><i>Platnost odkazu je {minutes} minut.</i>',
                'export_finished_email_subject' => 'Export všech uživatelů je připraven ke stažení',
            ],
            'en' => [
                'export_finished_email_body' => 'Users were exported and are available here:<br><br>{downloadLink}<br><br><i>Link is valid for {minutes} minutes.</i>',
                'export_finished_email_subject' => 'Export of all users is ready to download',
            ],
        ];
        $this->saveTranslation(static::CS_LANG_ID, 'common.user', $translations['cs']);
        $this->saveTranslation(static::EN_LANG_ID, 'common.user', $translations['en']);

        $translations = [
            'cs' => [
                'source' => 'zdroj',
                'frontend' => 'Web',
                'type_of_event_source' => 'Typ předmětu události',
                'event_source_name' => 'Předmět události',
            ],
            'en' => [
                'source' => 'source',
                'frontend' => 'Web',
                'type_of_event_source' => 'Event subject type',
                'event_source_name' => 'Event subject',
            ],
        ];
        $this->saveTranslation(static::CS_LANG_ID, 'admin.log', $translations['cs']);
        $this->saveTranslation(static::EN_LANG_ID, 'admin.log', $translations['en']);
    }
}
