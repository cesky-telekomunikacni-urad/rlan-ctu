<?php
/*
* Copyright 2023 DactylGroup s.r.o.
*
* Licensed under the EUPL, Version 1.2 or – as soon they
will be approved by the European Commission - subsequent
versions of the EUPL (the "Licence");
* You may not use this work except in compliance with the
Licence.
* You may obtain a copy of the Licence at:
*
* https://joinup.ec.europa.eu/software/page/eupl5
*
* Unless required by applicable law or agreed to in
writing, software distributed under the Licence is
distributed on an "AS IS" basis,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
express or implied.
* See the Licence for the specific language governing
permissions and limitations under the Licence.
*/
?>


<?php

use dactylcore\core\db\Migration;


class m200723_000002_station_detail_lang extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->lang();
    }


    public function lang()
    {
        $translations = [
            'cs' => [
                'edit your station parameters' => 'Otevře obrazovku úpravy stanice.',
                'contact the owner if station conflicts' => 'Kontaktujte majitele stanice pokud jsou Vaše stanice v konfliktu.',
                'contact owner' => 'Kontaktovat majitele',
                'deadlines' => 'Platnost',
                'valid until' => 'Platné do',
                'valid until tooltip' => 'Datum do kdy je stanice platná.',
                'protected until' => 'Chráněné do',
                'protected until tooltip' => 'Datum do kdy je stanice chráněná.',
                'conflict' => 'Konflikt:',
                'passive interference' => 'Pasivní rušení:',
                'active interference' => 'Aktivní rušení:',
                'interferes' => 'Ano ({val} dB)',
                'not interferes' => 'Ne ({val} dB)',
                'details' => 'Podrobnosti',
                'ID' => 'ID',
                'owner' => 'Majitel',
                'modified' => 'Upraveno',
                'created' => 'Vytvořeno',
                'Coordinator calculator table hint' => 'Koordinační kalkulačka pro kontrolu potenciálních konfliktů s okolními stanicemi.',
                'should reload' => 'tato stanice byla upravena, načtěte znovu',
            ],
            'en' => [
                'edit your station parameters' => 'Opens station editation screen.',
                'contact the owner if station conflicts' => 'Contact the owner if your stations conflict.',
                'contact owner' => 'Contact owner',
                'deadlines' => 'Deadlines',
                'valid until' => 'Valid until',
                'valid until tooltip' => 'Station info is valid until this date.',
                'protected until' => 'Protected until',
                'protected until tooltip' => 'Station is protected until this date.',
                'conflict' => 'Conflict:',
                'passive interference' => 'Passive interference:',
                'active interference' => 'Active interference:',
                'interferes' => 'Yes ({val} dB)',
                'not interferes' => 'No ({val} dB)',
                'details' => 'Details',
                'ID' => 'ID',
                'owner' => 'Owner',
                'modified' => 'Modified',
                'created' => 'Created',
                'Coordinator calculator table hint' => 'The coordination calculator checked for potential conflicts with nearby stations.',
                'should reload' => 'station has been changed, reload',
            ],
        ];

        $this->saveTranslation(static::CS_LANG_ID, 'frontend.station', $translations['cs']);
        $this->saveTranslation(static::EN_LANG_ID, 'frontend.station', $translations['en']);

    }

}
