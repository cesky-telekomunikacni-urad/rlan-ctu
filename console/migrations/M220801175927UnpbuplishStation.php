<?php
/*
* Copyright 2023 DactylGroup s.r.o.
*
* Licensed under the EUPL, Version 1.2 or – as soon they
will be approved by the European Commission - subsequent
versions of the EUPL (the "Licence");
* You may not use this work except in compliance with the
Licence.
* You may obtain a copy of the Licence at:
*
* https://joinup.ec.europa.eu/software/page/eupl5
*
* Unless required by applicable law or agreed to in
writing, software distributed under the Licence is
distributed on an "AS IS" basis,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
express or implied.
* See the Licence for the specific language governing
permissions and limitations under the Licence.
*/
?>


<?php

use dactylcore\core\db\Migration;


class M220801175927UnpbuplishStation extends Migration
{
    public function safeUp()
    {
        $this->i18n();
        $this->i18nUrls();
    }

    public function i18n()
    {
        $translations = [
            'cs' => [
                'unpublished' => 'odpublikováno',
                'status_unpublished' => 'odpublikováno',
                'unpublish' => 'odpublikovat',
                'flash_successfully_unpublished' => 'stanice úspěšně odpublikována',
                'error_while_unpublishing' => 'nepodařilo se odpublikovat stanici',
                'Do you really want to unpublish station?' => 'Opravdu chcete tuto stanici odpublikovat?',

            ],
            'en' => [
                'unpublished' => 'unpublished',
                'status_unpublished' => 'unpublished',
                'unpublish' => 'unpublish',
                'flash_successfully_unpublished' => 'station successfully unpublished',
                'error_while_unpublishing' => 'station cannot be unpublished',
                'Do you really want to unpublish station?' => 'Do you really want to unpublish this station?',
            ]
        ];

        $this->saveTranslation(static::CS_LANG_ID, 'common.station', $translations['cs']);
        $this->saveTranslation(static::EN_LANG_ID, 'common.station', $translations['en']);

        // Just typo repair
        $translations = [
            'cs' => [
                'Do you really want to delete station' => 'Opravdu chcete smazat tuto stanici?',
            ],
            'en' => [
                'Do you really want to delete station' => 'Do you really want to delete this station?',
            ],
        ];

        $this->saveTranslation(static::CS_LANG_ID, 'frontend.station', $translations['cs']);
        $this->saveTranslation(static::EN_LANG_ID, 'frontend.station', $translations['en']);
    }


    public function i18nUrls()
    {
        $translations = [
            'cs' => [
                'station/unpublish' => 'stanice/odpublikovat',
            ],
            'en' => [
                'station/unpublish' => 'station/unpublish',
            ],
        ];

        $this->saveTranslation(static::CS_LANG_ID, 'frontend.url', $translations['cs']);
        $this->saveTranslation(static::EN_LANG_ID, 'frontend.url', $translations['en']);
    }
}
