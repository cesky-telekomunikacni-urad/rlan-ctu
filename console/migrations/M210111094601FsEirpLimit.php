<?php
/*
* Copyright 2023 DactylGroup s.r.o.
*
* Licensed under the EUPL, Version 1.2 or – as soon they
will be approved by the European Commission - subsequent
versions of the EUPL (the "Licence");
* You may not use this work except in compliance with the
Licence.
* You may obtain a copy of the Licence at:
*
* https://joinup.ec.europa.eu/software/page/eupl5
*
* Unless required by applicable law or agreed to in
writing, software distributed under the Licence is
distributed on an "AS IS" basis,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
express or implied.
* See the Licence for the specific language governing
permissions and limitations under the Licence.
*/
?>


<?php

use dactylcore\core\db\Migration;


class M210111094601FsEirpLimit extends Migration
{
    public function safeUp()
    {
        $translations = [
            'cs' => [
                'eirp_max_55' => 'součet zisku a výkonu antény, musí být v intervalu 0-55.',
                'eirp too much for ptp' => 'Suma zisku antény a výkonu udává nepovolenou hodnotu E.I.R.P, která pro PtP musí být <-20, 55 >',
                'eirp too much for ptmp' => 'Suma zisku antény a výkonu udává nepovolenou hodnotu E.I.R.P, která pro PtMP musí být <-20 , 40 >',
            ],
            'en' => [
                'eirp_max_55' => 'the sum of the gain and power of the antenna must be in the range 0-55.',
                'eirp too much for ptp' => 'Sum of antenna volume and power exceeds the allowed value for E.I.R.P  which has to be <-20 , 55 > for PtP',
                'eirp too much for ptmp' => 'Sum of antenna volume and power exceeds the allowed value for E.I.R.P which has to be <-20 , 40 > for PtMP',
            ],
        ];

        $this->saveTranslation(static::CS_LANG_ID, 'frontend.station', $translations['cs']);
        $this->saveTranslation(static::EN_LANG_ID, 'frontend.station', $translations['en']);
    }
}