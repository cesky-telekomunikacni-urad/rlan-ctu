<?php
/*
* Copyright 2023 DactylGroup s.r.o.
*
* Licensed under the EUPL, Version 1.2 or – as soon they
will be approved by the European Commission - subsequent
versions of the EUPL (the "Licence");
* You may not use this work except in compliance with the
Licence.
* You may obtain a copy of the Licence at:
*
* https://joinup.ec.europa.eu/software/page/eupl5
*
* Unless required by applicable law or agreed to in
writing, software distributed under the Licence is
distributed on an "AS IS" basis,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
express or implied.
* See the Licence for the specific language governing
permissions and limitations under the Licence.
*/
?>


<?php

use dactylcore\core\db\Migration;


/**
 * Class m200130_000000_id_user
 */
class m200710_000000_layout_dict extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->lang();
    }

    public function lang()
    {
        $translations = [
            'cs' => [
                'create account' => 'Vytvořit účet',
                'create' => 'Vytvořit'
            ],
            'en' => [
                'create account' => 'Create an account',
                'create' => 'Create'
            ],
        ];

        $this->saveTranslation(static::CS_LANG_ID, 'frontend.header', $translations['cs']);
        $this->saveTranslation(static::EN_LANG_ID, 'frontend.header', $translations['en']);

        $translations = [
            'cs' => [
                'copyright' => '© 2020 Český telekomunikační úřad'
            ],
            'en' => [
                'copyright' => '© 2020 Czech Telecommunication Office'
            ]
        ];

        $this->saveTranslation(static::CS_LANG_ID, 'frontend.footer', $translations['cs']);
        $this->saveTranslation(static::EN_LANG_ID, 'frontend.footer', $translations['en']);


        $translations = [
            'cs' => [
                '60ghz' => '60 GHz',
                'access points' => 'Přístupové Body',
                'toll gates' => 'Mýtne brány',
                '52ghz' => '5.2 GHz',
            ],
            'en' => [
                '60ghz' => '60 GHz',
                'access points' => 'Access Points',
                'toll gates' => 'Toll Gates',
                '52ghz' => '5.2 GHz',
            ]
        ];

        $this->saveTranslation(static::CS_LANG_ID, 'frontend.menu', $translations['cs']);
        $this->saveTranslation(static::EN_LANG_ID, 'frontend.menu', $translations['en']);

    }

}
