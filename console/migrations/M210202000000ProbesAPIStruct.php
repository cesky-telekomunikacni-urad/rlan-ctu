<?php
/*
* Copyright 2023 DactylGroup s.r.o.
*
* Licensed under the EUPL, Version 1.2 or – as soon they
will be approved by the European Commission - subsequent
versions of the EUPL (the "Licence");
* You may not use this work except in compliance with the
Licence.
* You may obtain a copy of the Licence at:
*
* https://joinup.ec.europa.eu/software/page/eupl5
*
* Unless required by applicable law or agreed to in
writing, software distributed under the Licence is
distributed on an "AS IS" basis,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
express or implied.
* See the Licence for the specific language governing
permissions and limitations under the Licence.
*/
?>


<?php

use dactylcore\core\db\Migration;


class M210202000000ProbesAPIStruct extends Migration
{
    public function safeUp()
    {
        $this->tables();
        $this->lang();
    }

    protected function tables()
    {
        $this->createTable('probe', [
            'id' => $this->primaryKey(11),

            'name' => $this->string(255)->notNull(),
            'description' => $this->text()->notNull(),
            'access_token' => $this->string(255)->notNull()->unique(),
            'status' => $this->integer(11)->notNull(),

            'deleted' => $this->integer(1)->notNull()->defaultValue(0),

            'created_at' => $this->integer(11)->null(),
            'updated_at' => $this->integer(11)->null(),
        ]);

        $this->createTable('probe_scan', [
            'id' => $this->primaryKey(11),

            'id_probe' => $this->integer(11)->notNull(),

            'address' => $this->string(512)->notNull(),
            'time' => $this->string(19)->notNull(),

            'deleted' => $this->integer(1)->notNull()->defaultValue(0),

            'created_at' => $this->integer(11)->null(),
            'updated_at' => $this->integer(11)->null(),
        ]);
        $this->addForeignKey('fk_probe_scan_probe', 'probe_scan', 'id_probe', 'probe', 'id', 'no action', 'no action');

        $this->createTable('probe_scan_wlan', [
            'id' => $this->primaryKey(11),

            'id_probe_scan' => $this->integer(11)->notNull(),

            'bssid' => $this->string(512)->notNull(),
            'rssi' => $this->decimal()->notNull(),
            'ssid' => $this->string(512)->notNull(),
            'capabilities' => $this->string(512)->notNull(),
            'frequency' => $this->integer(11)->notNull(),

            'deleted' => $this->integer(1)->notNull()->defaultValue(0),

            'created_at' => $this->integer(11)->null(),
            'updated_at' => $this->integer(11)->null(),
        ]);
        $this->addForeignKey('fk_probe_scan_wlan_snac', 'probe_scan_wlan', 'id_probe_scan', 'probe_scan', 'id', 'no action', 'no action');

    }

    public function lang() {

        $translations = [
            'cs' => [
                'id' => 'id',
                'name' => 'název',
                'description' => 'popis',
                'access_token' => 'přístupový token',
                'probe' => 'sonda',
                'probes' => 'sondy',
                'status' => 'stav',
                'active' => 'aktivní',
                'inactive' => 'neaktivní',
                'created_at' => 'vytvořeno',
                'updated_at' => 'upraveno',
            ],
            'en' => [
                'id' => 'id',
                'name' => 'name',
                'description' => 'description',
                'access_token' => 'access token',
                'probe' => 'probe',
                'probes' => 'probes',
                'status' => 'status',
                'active' => 'active',
                'inactive' => 'inactive',
                'created_at' => 'created at',
                'updated_at' => 'updated at',
            ]
        ];

        $this->saveTranslation(self::CS_LANG_ID, 'common.probe', $translations['cs']);
        $this->saveTranslation(self::EN_LANG_ID, 'common.probe', $translations['en']);

        $translations = [
            'cs' => [
                'id_probe_scan' => 'Sken sondy',
                'bssid' => 'BSSID',
                'time' => 'čas měření',
                'address' => 'místo měření',
                'rssi' => 'RSSI',
                'ssid' => 'SSID',
                'capabilities' => 'Schopnosti',
                'frequency' => 'Frekvence',
                'list_probe_scan_wlan_heading' => 'Seznam nalezených stanic při skenování',
            ],
            'en' => [
                'id_probe_scan' => 'Probe scan',
                'bssid' => 'BSSID',
                'time' => 'time',
                'address' => 'address',
                'rssi' => 'RSSI',
                'ssid' => 'SSID',
                'capabilities' => 'Capabilities',
                'frequency' => 'Frequency',
                'list_probe_scan_wlan_heading' => 'List of found WLANs during scan',
            ],
        ];

        $this->saveTranslation(self::CS_LANG_ID, 'common.probe', $translations['cs']);
        $this->saveTranslation(self::EN_LANG_ID, 'common.probe', $translations['en']);

    }
}
