<?php
/*
* Copyright 2023 DactylGroup s.r.o.
*
* Licensed under the EUPL, Version 1.2 or – as soon they
will be approved by the European Commission - subsequent
versions of the EUPL (the "Licence");
* You may not use this work except in compliance with the
Licence.
* You may obtain a copy of the Licence at:
*
* https://joinup.ec.europa.eu/software/page/eupl5
*
* Unless required by applicable law or agreed to in
writing, software distributed under the Licence is
distributed on an "AS IS" basis,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
express or implied.
* See the Licence for the specific language governing
permissions and limitations under the Licence.
*/
?>


<?php

use dactylcore\core\db\Migration;
use dactylcore\user\common\models\UserRole;

class M210119101201StationsGrid extends Migration
{
    public function safeUp()
    {
        $this->lang();
        $this->newRole();
    }

    protected function lang()
    {
        $translations = [
            'cs' => [
                '0_stations|1_station|2-4_stations|5+_stations' => '0_stanic|1_stanice|2-4_stanice|5+_stanic',
            ],
            'en' => [
                '0_stations|1_station|2-4_stations|5+_stations' => '0_stations|1_station|2-4_stations|5+_stations',
                'stations_admin_name' => 'stations',
            ],
        ];

        $this->saveTranslation(static::CS_LANG_ID, 'common.station', $translations['cs']);
        $this->saveTranslation(static::EN_LANG_ID, 'common.station', $translations['en']);
    }

    protected function newRole()
    {
        $this->insert('user_role', [
            'id' => UserRole::ROLE_TECHNICIAN_ID,
            'permissions' => '{}',
            'created_at' => time(),
            'updated_at' => time(),
        ]);

        $this->insert('lang_user_role', [
            'id_language' => static::CS_LANG_ID,
            'id_user_role' => UserRole::ROLE_TECHNICIAN_ID,
            'name' => 'technik',
        ]);

        $this->insert('lang_user_role', [
            'id_language' => static::EN_LANG_ID,
            'id_user_role' => UserRole::ROLE_TECHNICIAN_ID,
            'name' => 'technician',
        ]);

        $this->saveUserRolePermissions([
            'app-admin_station',
            'app-admin_station_index',
            'dc-user_user',
            'dc-user_user_index',
        ], UserRole::ROLE_TECHNICIAN_ID);
    }
}
