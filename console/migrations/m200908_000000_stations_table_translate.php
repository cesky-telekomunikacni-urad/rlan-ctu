<?php
/*
* Copyright 2023 DactylGroup s.r.o.
*
* Licensed under the EUPL, Version 1.2 or – as soon they
will be approved by the European Commission - subsequent
versions of the EUPL (the "Licence");
* You may not use this work except in compliance with the
Licence.
* You may obtain a copy of the Licence at:
*
* https://joinup.ec.europa.eu/software/page/eupl5
*
* Unless required by applicable law or agreed to in
writing, software distributed under the Licence is
distributed on an "AS IS" basis,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
express or implied.
* See the Licence for the specific language governing
permissions and limitations under the Licence.
*/
?>


<?php

use dactylcore\core\db\Migration;


/**
 * Class m190830_113326_i18n
 */
class m200908_000000_stations_table_translate extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->lang();
    }

    protected function lang()
    {
        $translations = [
            'cs' => [
                'table_id' => 'ID',
                'table_name' => 'Název',
                'table_type' => 'Typ',
                'table_status' => 'Status',
                'type_wigig' => 'WiGig PtP',
                'type_fs' => 'FS PtP',
                'table_loading' => 'Nahrávám data...',
                'of' => 'ze',
                'empty_table' => 'nenalezeny žádné stanice',
                'show_all_stations' => 'zobrazit všechny stanice',
                'protected_until' => 'chráněno do',
                'valid_until' => 'platné do',
                'date_format' => '%e. %B, %Y',
            ],
            'en' => [
                'table_id' => 'ID',
                'table_name' => 'Name',
                'table_type' => 'Type',
                'table_status' => 'Status',
                'type_wigig' => 'WiGig PtP',
                'type_fs' => 'FS PtP',
                'table_loading' => 'Loading data...',
                'of' => 'of',
                'empty_table' => 'no stations found',
                'show_all_stations' => 'show all Stations',
                'protected_until' => 'protected until',
                'valid_until' => 'valid until',
                'date_format' => '%B %e, %Y',
            ],
        ];

        $this->saveTranslation(static::CS_LANG_ID, 'common.stations-table', $translations['cs']);
        $this->saveTranslation(static::EN_LANG_ID, 'common.stations-table', $translations['en']);

    }
}
