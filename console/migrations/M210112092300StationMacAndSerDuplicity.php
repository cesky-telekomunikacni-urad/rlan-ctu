<?php
/*
* Copyright 2023 DactylGroup s.r.o.
*
* Licensed under the EUPL, Version 1.2 or – as soon they
will be approved by the European Commission - subsequent
versions of the EUPL (the "Licence");
* You may not use this work except in compliance with the
Licence.
* You may obtain a copy of the Licence at:
*
* https://joinup.ec.europa.eu/software/page/eupl5
*
* Unless required by applicable law or agreed to in
writing, software distributed under the Licence is
distributed on an "AS IS" basis,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
express or implied.
* See the Licence for the specific language governing
permissions and limitations under the Licence.
*/
?>


<?php

use dactylcore\core\db\Migration;

class M210112092300StationMacAndSerDuplicity extends Migration
{
    public function safeUp()
    {
        $this->lang();
    }

    protected function lang()
    {
        $translations = [
            'cs' => [
                'mac_duplicity_warning' => 'Varování: Tato MAC adresa je již přiřazena i jiné stanici',
                'serial_duplicity_warning' => 'Varování: Toto sériové číslo je již přiřazeno i jiné stanici',
            ],
            'en' => [
                'mac_duplicity_warning' => "Warning: This MAC address is already assigned also to other station",
                'serial_duplicity_warning' => 'Warning: This serial number is already assigned also to another station',
            ],
        ];

        $this->saveTranslation(static::CS_LANG_ID, 'common.station', $translations['cs']);
        $this->saveTranslation(static::EN_LANG_ID, 'common.station', $translations['en']);
    }
}