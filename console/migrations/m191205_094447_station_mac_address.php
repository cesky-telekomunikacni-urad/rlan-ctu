<?php
/*
* Copyright 2023 DactylGroup s.r.o.
*
* Licensed under the EUPL, Version 1.2 or – as soon they
will be approved by the European Commission - subsequent
versions of the EUPL (the "Licence");
* You may not use this work except in compliance with the
Licence.
* You may obtain a copy of the Licence at:
*
* https://joinup.ec.europa.eu/software/page/eupl5
*
* Unless required by applicable law or agreed to in
writing, software distributed under the Licence is
distributed on an "AS IS" basis,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
express or implied.
* See the Licence for the specific language governing
permissions and limitations under the Licence.
*/
?>


<?php

use dactylcore\core\db\Migration;

/**
 * Class m191205_094447_station_mac_address
 */
class m191205_094447_station_mac_address extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->struct();
        $this->lang();
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m191205_094447_station_mac_address cannot be reverted.\n";

        return false;
    }

    protected function struct()
    {
        $this->addColumn('station', 'hardware_identifier', $this->tinyInteger(1)->defaultValue(0));
        $this->addColumn('station', 'mac_address', $this->string(32)->null());
        $this->addColumn('station', 'serial_number', $this->string(32)->null());
    }

    protected function lang()
    {
        $translations = [
            'cs' => [
                'i_know_mac_address' => 'Znám MAC adresu',
                'i_know_serial_number' => 'Znám výrobní číslo',
                'mac_address' => 'MAC adresa',
                'serial_number' => 'výrobní číslo',
            ],
            'en' => [
                'i_know_mac_address' => 'I know MAC address',
                'i_know_serial_number' => 'I know serial number',
                'mac_address' => 'MAC address',
                'serial_number' => 'serial number',
            ],
        ];

        $this->saveTranslation(static::CS_LANG_ID, 'common.station', $translations['cs']);
        $this->saveTranslation(static::EN_LANG_ID, 'common.station', $translations['en']);
    }
}
