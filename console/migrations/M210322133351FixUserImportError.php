<?php
/*
* Copyright 2023 DactylGroup s.r.o.
*
* Licensed under the EUPL, Version 1.2 or – as soon they
will be approved by the European Commission - subsequent
versions of the EUPL (the "Licence");
* You may not use this work except in compliance with the
Licence.
* You may obtain a copy of the Licence at:
*
* https://joinup.ec.europa.eu/software/page/eupl5
*
* Unless required by applicable law or agreed to in
writing, software distributed under the Licence is
distributed on an "AS IS" basis,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
express or implied.
* See the Licence for the specific language governing
permissions and limitations under the Licence.
*/
?>


<?php

use dactylcore\core\db\Migration;


class M210322133351FixUserImportError extends Migration
{
    public function safeUp()
    {
        $translations = [
            'cs' => [
                'warning_import' => 'akce &lt;nova&gt; nebo &lt;upravena&gt; nezadána - přeskočeno',
                'not_existed_error' => 'na těchto souřadnicích neexistuje žádná stanice. Nejdříve ji prosím vytvořte.',

            ],
            'en' => [
                'warning_import' => 'action &lt;nova&gt; nebo &lt;upravena&gt; not filled - ignored',
                'not_existed_error' => 'on these GPS do not exists any station yet. Please create it first.',
            ]
        ];

        $this->saveTranslation(static::CS_LANG_ID, 'frontend.user-import', $translations['cs']);
        $this->saveTranslation(static::EN_LANG_ID, 'frontend.user-import', $translations['en']);
    }
}
