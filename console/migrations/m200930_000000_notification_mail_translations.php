<?php
/*
* Copyright 2023 DactylGroup s.r.o.
*
* Licensed under the EUPL, Version 1.2 or – as soon they
will be approved by the European Commission - subsequent
versions of the EUPL (the "Licence");
* You may not use this work except in compliance with the
Licence.
* You may obtain a copy of the Licence at:
*
* https://joinup.ec.europa.eu/software/page/eupl5
*
* Unless required by applicable law or agreed to in
writing, software distributed under the Licence is
distributed on an "AS IS" basis,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
express or implied.
* See the Licence for the specific language governing
permissions and limitations under the Licence.
*/
?>


<?php

use dactylcore\core\db\Migration;


/**
 * Class m200115_000000_flash
 */
class m200930_000000_notification_mail_translations extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->lang();
    }

    public function lang()
    {
        $translations = [
            'cs' => [
                'notification_expired_headline' => 'expirace stanic',
                'notification_registration_period_headline' => 'blíží se konec platnosti stanic',
                'notification_protection_period_headline' => 'blíží se konec ochranné lhůty stanic',
                'mail_notification' => 'odeslána mailová notifikace',
                'your stations have expired:' => 'Vaše následující stanice expirovaly:',
                'your stations will end validity in {warning} months:' => 'Vaše následující stanice ukončí platnost za {warning} měsíce:',
                'your stations will end protection in {warning} months:' => 'konec ochranné lhůty Vašich následujících stanic nastane za {warning} měsíce:',
            ],
            'en' => [
            ]
        ];


        $this->saveTranslation(static::CS_LANG_ID, 'common.station-notification', $translations['cs']);
        $this->saveTranslation(static::EN_LANG_ID, 'common.station-notification', $translations['en']);

        $logTranslations = [
            'cs' => [
                'warning for stations [{names}] was sent to email address {email}' => 'notifikace o platnosti stanic: [{names}] byla odeslána na email {email}'
            ],
            'en' => [
                'warning for stations [{names}] was sent to email address {email}' => 'warning for stations: [{names}] was sent to email address {email}'
            ]
        ];

        $this->saveTranslation(static::CS_LANG_ID, 'common.log', $logTranslations['cs']);
        $this->saveTranslation(static::EN_LANG_ID, 'common.log', $logTranslations['en']);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m200130_000000_notification_mail_translations cannot be reverted.\n";

        return false;
    }

}
