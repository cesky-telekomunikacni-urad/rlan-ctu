<?php
/*
* Copyright 2023 DactylGroup s.r.o.
*
* Licensed under the EUPL, Version 1.2 or – as soon they
will be approved by the European Commission - subsequent
versions of the EUPL (the "Licence");
* You may not use this work except in compliance with the
Licence.
* You may obtain a copy of the Licence at:
*
* https://joinup.ec.europa.eu/software/page/eupl5
*
* Unless required by applicable law or agreed to in
writing, software distributed under the Licence is
distributed on an "AS IS" basis,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
express or implied.
* See the Licence for the specific language governing
permissions and limitations under the Licence.
*/
?>


<?php

use dactylcore\core\db\Migration;


class m200806_095407_user_forms extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->lang();
    }

    public function lang()
    {
        $translations = [
            'cs' => [
                'email' => 'E-mail',
                'login_success_flash' => 'Přihlášení úspěšné. Vítejte!',
                'forgotten_request_sent_success_flash' => 'Instrukce na e-mail odeslány',
                'password_reset_success_flash' => 'Nové heslo nastaveno',
                'register-individual-type' => 'osobní',
                'register-company-type' => 'firemní',
                'register_submit_button' => 'vytvořit účet',
                'account_create_subtitle' => 'Začněte výběrem typu účtu',
                'account_create_individual_title' => 'Nyní doplňte informace o Vás',
                'account_create_company_title' => 'Nyní doplňte informace o Vaší firmě',
                'ico_error' => 'Není uveden správný tvar IČ',
                'ico_sum_error' => 'Kontrolní součet IČ neodpovídá. Prosím zkontrolujte zadané IČ.'
            ],
            'en' => [
                'email' => 'Email',
                'login_success_flash' => 'Log in successful. Welcome!',
                'forgotten_request_sent_success_flash' => 'Email with instructions was sent',
                'password_reset_success_flash' => 'New password saved',
                'register-individual-type' => 'individual',
                'register-company-type' => 'company',
                'register_submit_button' => 'create an account',
                'account_create_subtitle' => 'Begin by selecting your role',
                'account_create_individual_title' => 'Now, enter information about you',
                'account_create_company_title' => 'Now, enter information about your company',
                'ico_error' => 'Wrong VAT number format',
                'ico_sum_error' => 'Wrong Sum control of VAT. Please check VAT.'
            ],
        ];

        $this->saveTranslation(static::CS_LANG_ID, 'frontend.user', $translations['cs']);
        $this->saveTranslation(static::EN_LANG_ID, 'frontend.user', $translations['en']);
    }
}
