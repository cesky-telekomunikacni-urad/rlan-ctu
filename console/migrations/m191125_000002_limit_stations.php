<?php
/*
* Copyright 2023 DactylGroup s.r.o.
*
* Licensed under the EUPL, Version 1.2 or – as soon they
will be approved by the European Commission - subsequent
versions of the EUPL (the "Licence");
* You may not use this work except in compliance with the
Licence.
* You may obtain a copy of the Licence at:
*
* https://joinup.ec.europa.eu/software/page/eupl5
*
* Unless required by applicable law or agreed to in
writing, software distributed under the Licence is
distributed on an "AS IS" basis,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
express or implied.
* See the Licence for the specific language governing
permissions and limitations under the Licence.
*/
?>


<?php

use dactylcore\core\db\Migration;


/**
 * Class m190830_113326_i18n
 */
class m191125_000002_limit_stations extends Migration
{
    public function safeUp()
    {
        $this->saveConfig('MAX_WAITING_STATIONS', 5, 'dc-system', 'stations', 0, 0);

        $translations = [
            'cs' => [
                'MAX_WAITING_STATIONS' => 'Maximální počet nepublikovaných stanic',
            ],
            'en' => [
                'MAX_WAITING_STATIONS' => 'Maximum amount of unpublished stations',
            ]
        ];

        $this->saveTranslation(static::CS_LANG_ID, 'admin.config', $translations['cs']);
        $this->saveTranslation(static::EN_LANG_ID, 'admin.config', $translations['en']);

        $translations = [
            'cs' => [
                'max waiting stations' => 'Dosažen maximální počet nepublikovných stanic',
                '{curent} / {limit}' => '{curent} / {limit}',
                'max unpublished stations limit' => 'Limit počtu nepublikovaných stanic ve stavu Čeká',
            ],
            'en' => [
                'max waiting stations' => 'You have reached max limit for unpublished stations',
                '{curent} / {limit}' => '{curent} / {limit}',
                'max unpublished stations limit' => 'Max unpublished stations limit in status Waiting',
            ]
        ];

        $this->saveTranslation(static::CS_LANG_ID, 'frontend.station', $translations['cs']);
        $this->saveTranslation(static::EN_LANG_ID, 'frontend.station', $translations['en']);

    }
}
