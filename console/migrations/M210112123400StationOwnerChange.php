<?php
/*
* Copyright 2023 DactylGroup s.r.o.
*
* Licensed under the EUPL, Version 1.2 or – as soon they
will be approved by the European Commission - subsequent
versions of the EUPL (the "Licence");
* You may not use this work except in compliance with the
Licence.
* You may obtain a copy of the Licence at:
*
* https://joinup.ec.europa.eu/software/page/eupl5
*
* Unless required by applicable law or agreed to in
writing, software distributed under the Licence is
distributed on an "AS IS" basis,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
express or implied.
* See the Licence for the specific language governing
permissions and limitations under the Licence.
*/
?>


<?php

use dactylcore\core\db\Migration;


class M210112123400StationOwnerChange extends Migration
{
    public function safeUp()
    {
        $this->lang();
    }

    protected function lang()
    {
        $translations = [
            'cs' => [
                'id_user' => 'Majitel',
                'change_owner_tooltip' => 'Změnit majitele',
                'owner_change' => 'Změna majitele',
                'station_name' => 'Název',
                'station' => 'Stanice',
                'old_owner' => 'Původní majitel',
                'new_owner' => 'Nový majitel',
                'flash_successfully_updated' => 'Stanice byla úspěšně upravena',
            ],
            'en' => [
                'id_user' => "Owner",
                'change_owner_tooltip' => 'Change owner',
                'owner_change' => 'Owner change',
                'station_name' => 'Name',
                'station' => 'Station',
                'old_owner' => 'Old owner',
                'new_owner' => 'New owner',
                'flash_successfully_updated' => 'Station was successfully updated',
            ],
        ];

        $this->saveTranslation(static::CS_LANG_ID, 'common.station', $translations['cs']);
        $this->saveTranslation(static::EN_LANG_ID, 'common.station', $translations['en']);
    }
}