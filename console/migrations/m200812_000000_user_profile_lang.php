<?php
/*
* Copyright 2023 DactylGroup s.r.o.
*
* Licensed under the EUPL, Version 1.2 or – as soon they
will be approved by the European Commission - subsequent
versions of the EUPL (the "Licence");
* You may not use this work except in compliance with the
Licence.
* You may obtain a copy of the Licence at:
*
* https://joinup.ec.europa.eu/software/page/eupl5
*
* Unless required by applicable law or agreed to in
writing, software distributed under the Licence is
distributed on an "AS IS" basis,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
express or implied.
* See the Licence for the specific language governing
permissions and limitations under the Licence.
*/
?>


<?php

use dactylcore\core\db\Migration;


class m200812_000000_user_profile_lang extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->translate();
        $this->urls();
    }

    public function urls() {
        $translations = [
            'cs' => [
                'profile_url' => 'profil',
            ],
            'en' => [
                'profile_url' => 'profile',
            ],
        ];

        $this->saveTranslation(static::CS_LANG_ID, 'frontend.url_rules', $translations['cs']);
        $this->saveTranslation(static::EN_LANG_ID, 'frontend.url_rules', $translations['en']);
    }

    public function translate()
    {
        $translations = [
            'cs' => [
                'profile' => 'profil',
                'company-type-profile' => 'právnická osoba',
                'user-type-profile' => 'fyzická osoba',
                'user_type' => 'typ uživatele',
                'lock_tooltip' => 'neveřejná informace',
                'address' => 'adresa',
                'email' => 'e-mail',
                'settings' => 'nastavení',
                'update_submit_button' => 'uložit nastavení',
                'account_update_company_title' => 'upravte údaje Vaší firmy',
                'account_update_user_title' => 'upravte Vaše údaje',
            ],
            'en' => [
                'profile' => 'profile',
                'company-type-profile' => 'Legal entity',
                'user-type-profile' => 'individual',
                'user_type' => 'user type',
                'lock_tooltip' => 'non-public information',
                'address' => 'address',
                'email' => 'email',
                'settings' => 'settings',
                'update_submit_button' => 'save settings',
                'account_update_company_title' => 'update information about your company',
                'account_update_user_title' => 'update information about you',
            ],
        ];

        $this->saveTranslation(static::CS_LANG_ID, 'frontend.user', $translations['cs']);
        $this->saveTranslation(static::EN_LANG_ID, 'frontend.user', $translations['en']);
    }
}
