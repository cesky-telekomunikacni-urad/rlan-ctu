<?php
/*
* Copyright 2023 DactylGroup s.r.o.
*
* Licensed under the EUPL, Version 1.2 or – as soon they
will be approved by the European Commission - subsequent
versions of the EUPL (the "Licence");
* You may not use this work except in compliance with the
Licence.
* You may obtain a copy of the Licence at:
*
* https://joinup.ec.europa.eu/software/page/eupl5
*
* Unless required by applicable law or agreed to in
writing, software distributed under the Licence is
distributed on an "AS IS" basis,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
express or implied.
* See the Licence for the specific language governing
permissions and limitations under the Licence.
*/
?>


<?php

use dactylcore\core\db\Migration;


class M220513081306SortByExpiration extends Migration
{
    public function safeUp()
    {
        $this->i18n();
    }

    public function i18n()
    {
        $this->saveTranslationCsEn('frontend.station', [
            'sort_by_expiration' => [
                'seřadit od expirujících',
                'sort by expiring',
            ],
        ]);
    }
}
