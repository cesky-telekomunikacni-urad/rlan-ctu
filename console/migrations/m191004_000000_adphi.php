<?php
/*
* Copyright 2023 DactylGroup s.r.o.
*
* Licensed under the EUPL, Version 1.2 or – as soon they
will be approved by the European Commission - subsequent
versions of the EUPL (the "Licence");
* You may not use this work except in compliance with the
Licence.
* You may obtain a copy of the Licence at:
*
* https://joinup.ec.europa.eu/software/page/eupl5
*
* Unless required by applicable law or agreed to in
writing, software distributed under the Licence is
distributed on an "AS IS" basis,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
express or implied.
* See the Licence for the specific language governing
permissions and limitations under the Licence.
*/
?>


<?php

use dactylcore\core\db\Migration;


/**
 * Class m190830_113326_i18n
 */
class m191004_000000_adphi extends Migration
{
    public function safeUp()
    {
        $this->createTable("phi", [
            'id' => $this->primaryKey(11),

            'phi12' => $this->decimal(10, 2)->null(),
            'phi34' => $this->decimal(10, 2)->null(),
            'g_phi' => $this->decimal(10, 2)->null(),
            'ad_phi' => $this->decimal(10, 2)->null(),
            'g_min' => $this->decimal(10, 2)->null(),
            'g_max' => $this->decimal(10, 2)->null(),
            'note' => $this->string(255)->null(),


            'deleted' => $this->integer(1)->notNull()->defaultValue(0),
            'created_at' => $this->integer(11)->null(),
            'updated_at' => $this->integer(11)->null(),

        ], static::$TABLE_OPTIONS);

        $translations = [
            'cs' => [
                'phi12' => 'PHI I,II',
                'phi34' => 'PHI III,IV',
                'g_phi' => 'G(PHI)',
                'ad_phi' => 'AD(PHI)',
                'g_min' => 'G Min',
                'g_max' => 'G Max',
                'note' => 'Poznámka',
            ],
            'en' => [
                'phi12' => 'PHI I,II',
                'phi34' => 'PHI III,IV',
                'g_phi' => 'G(PHI)',
                'ad_phi' => 'AD(PHI)',
                'g_min' => 'G Min',
                'g_max' => 'G Max',
                'note' => 'Note',
            ]
        ];

        $this->saveTranslation(static::CS_LANG_ID, 'admin.phi', $translations['cs']);
        $this->saveTranslation(static::EN_LANG_ID, 'admin.phi', $translations['en']);


        $data = file_get_contents('phi.json', true);
        $dataEncoded = json_decode($data, true);

        $this->batchInsert('phi', ['phi34', 'phi12', 'g_phi', 'note', 'ad_phi', 'g_min', 'g_max'], $dataEncoded);

    }
}
