<?php
/*
* Copyright 2023 DactylGroup s.r.o.
*
* Licensed under the EUPL, Version 1.2 or – as soon they
will be approved by the European Commission - subsequent
versions of the EUPL (the "Licence");
* You may not use this work except in compliance with the
Licence.
* You may obtain a copy of the Licence at:
*
* https://joinup.ec.europa.eu/software/page/eupl5
*
* Unless required by applicable law or agreed to in
writing, software distributed under the Licence is
distributed on an "AS IS" basis,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
express or implied.
* See the Licence for the specific language governing
permissions and limitations under the Licence.
*/
?>


<?php

use dactylcore\core\db\Migration;


/**
 * Class m190830_113326_i18n
 */
class m190904_000000_user_fields extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->struc();
        $this->lang();
    }

    public function struc()
    {
        $this->addColumn('user', 'type', $this->string(255)->null());
        $this->addColumn('user', 'companyName', $this->string(255)->null());
        $this->addColumn('user', 'vatNumber', $this->string(255)->null());
        $this->addColumn('user', 'taxNumber', $this->string(255)->null());
        $this->addColumn('user', 'street', $this->string(255)->notNull());
        $this->addColumn('user', 'city', $this->string(255)->notNull());
        $this->addColumn('user', 'zipCode', $this->string(255)->notNull());
        $this->addColumn('user', 'additionalInfo', $this->text()->null());
    }

    protected function lang()
    {
        $translations = [
            'cs' => [
                'forgotten password sent' => 'Obnova hesla proběhla úspěšně'
            ],
            'en' => [
                'forgotten password sent' => 'Password recovery was successful'
            ],
        ];

        $this->saveTranslation(static::CS_LANG_ID, 'frontend.register', $translations['cs']);
        $this->saveTranslation(static::EN_LANG_ID, 'frontend.register', $translations['en']);

        $translations = [
            'cs' => [
                'logout' => 'Odhlášení',
                'login' => 'Přihlášení'
            ],
            'en' => [
                'logout' => 'Logout',
                'login' => 'Login'
            ],
        ];

        $this->saveTranslation(static::CS_LANG_ID, 'admin.menu_item', $translations['cs']);
        $this->saveTranslation(static::EN_LANG_ID, 'admin.menu_item', $translations['en']);
        $translations = [
            'cs' => [
                'email_verification_token' => 'Ověřeno',
            ],
            'en' => [
                'email_verification_token' => 'Verified',
            ],
        ];

        $this->saveTranslation(static::CS_LANG_ID, 'frontend.user', $translations['cs']);
        $this->saveTranslation(static::EN_LANG_ID, 'frontend.user', $translations['en']);

    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m190830_113326_i18n cannot be reverted.\n";

        return false;
    }

}
