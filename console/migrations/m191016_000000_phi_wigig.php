<?php
/*
* Copyright 2023 DactylGroup s.r.o.
*
* Licensed under the EUPL, Version 1.2 or – as soon they
will be approved by the European Commission - subsequent
versions of the EUPL (the "Licence");
* You may not use this work except in compliance with the
Licence.
* You may obtain a copy of the Licence at:
*
* https://joinup.ec.europa.eu/software/page/eupl5
*
* Unless required by applicable law or agreed to in
writing, software distributed under the Licence is
distributed on an "AS IS" basis,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
express or implied.
* See the Licence for the specific language governing
permissions and limitations under the Licence.
*/
?>


<?php

use dactylcore\core\db\Migration;


/**
 * Class m190830_113326_i18n
 */
class m191016_000000_phi_wigig extends Migration
{
    public function safeUp()
    {
        $this->createTable("phi_wigig", [
            'id' => $this->primaryKey(11),

            'phi' => $this->decimal(10, 2)->null(),
            'p' => $this->decimal(10, 2)->null(),
            'ad' => $this->decimal(10, 2)->null(),
            'ptmp' => $this->tinyInteger(1)->defaultValue(1)->null(),


            'deleted' => $this->integer(1)->notNull()->defaultValue(0),
            'created_at' => $this->integer(11)->null(),
            'updated_at' => $this->integer(11)->null(),

        ], static::$TABLE_OPTIONS);

        $translations = [
            'cs' => [
                'phi_wigig' => 'PHI Wigig',
                'phi' => 'PHI',
                'p' => 'P wigig',
                'ad' => 'AD(PHI)',
                'ptmp' => 'PTMP',
            ],
            'en' => [
                'phi' => 'PHI',
                'p' => 'P wigig',
                'ad' => 'AD(PHI)',
                'ptmp' => 'PTMP',
            ]
        ];

        $this->saveTranslation(static::CS_LANG_ID, 'admin.phi_wigig', $translations['cs']);
        $this->saveTranslation(static::EN_LANG_ID, 'admin.phi_wigig', $translations['en']);


        $data = file_get_contents('wigig_phi.json', true);
        $dataEncoded = json_decode($data, true);

        $this->batchInsert('phi_wigig', ['phi', 'p', 'ad', 'ptmp',], $dataEncoded);

    }
}
