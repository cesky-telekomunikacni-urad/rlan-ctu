<?php
/*
* Copyright 2023 DactylGroup s.r.o.
*
* Licensed under the EUPL, Version 1.2 or – as soon they
will be approved by the European Commission - subsequent
versions of the EUPL (the "Licence");
* You may not use this work except in compliance with the
Licence.
* You may obtain a copy of the Licence at:
*
* https://joinup.ec.europa.eu/software/page/eupl5
*
* Unless required by applicable law or agreed to in
writing, software distributed under the Licence is
distributed on an "AS IS" basis,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
express or implied.
* See the Licence for the specific language governing
permissions and limitations under the Licence.
*/
?>


<?php

use dactylcore\core\db\Migration;


class M220808131307StationApi extends Migration
{
    public function safeUp()
    {
        $this->struct();
        $this->i18n();
    }

    private function i18n()
    {
        $this->saveTranslationCsEn('api.station', [
            'has_conflicts_warning' => [
                'Stanice byla uložena jako koncept avšak ji nebude možné publikovat protože má konflikty, které lze vyřešit možnými řešeními popsanými v této odpovědi.',
                'Station was saved as concept but it will not be possible to publish because it has conflicts which can be solved by possible solutions described in this response.',
            ],
            'has_conflicts_error' => [
                'Stanici nebude možné publikovat protože má konflikty, které lze vyřešit možnými řešeními popsanými v této odpovědi.',
                'Station cannot be published because it has conflicts which can be solved by possible solutions described in this response.',
            ],
            'has_conflicts_on_update_when_published' => [
                'Změny byly uloženy stranou jako čekající, protože stanice je již publikována. Stanice má také konflikty, které lze vyřešit možnými řešeními popsanými v této odpovědi.',
                'Changes were saved aside as waiting because station is published. Station has also conflicts which can be solved by possible solutions described in this response.',
            ],
            'solution1_how' => [
                'Upravte parametry stanice tak, aby ke konfliktu již nedocházelo (můžete použít chatovou komunikaci s vlastníkem konfliktní stanice přes Thread endpointy)',
                'Adjust station parameters to state that conflict no more occurs (you can use chat communication with conflict station owner via Thread endpoints)',
            ],
            'solution2_how' => [
                'V endpointe /publish odešlete parametr „solveConflictByDeclaration“ nastavený na hodnotu true',
                'In /publish endpoint, send parameter "solveConflictByDeclaration" set to true',
            ],
        ]);
    }

    private function struct()
    {
        $this->addColumn('station', 'waiting_api_changes_json', $this->text()->null());
    }
}
