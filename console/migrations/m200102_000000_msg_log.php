<?php
/*
* Copyright 2023 DactylGroup s.r.o.
*
* Licensed under the EUPL, Version 1.2 or – as soon they
will be approved by the European Commission - subsequent
versions of the EUPL (the "Licence");
* You may not use this work except in compliance with the
Licence.
* You may obtain a copy of the Licence at:
*
* https://joinup.ec.europa.eu/software/page/eupl5
*
* Unless required by applicable law or agreed to in
writing, software distributed under the Licence is
distributed on an "AS IS" basis,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
express or implied.
* See the Licence for the specific language governing
permissions and limitations under the Licence.
*/
?>


<?php

use common\models\StationWigig;
use dactylcore\core\db\Migration;


/**
 * Class m190830_113326_i18n
 */
class m200102_000000_msg_log extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->lang();
    }

    public function lang()
    {
        $translations = [
            'cs' => [
                'Message add {name}' => 'Zpráva <b>{name}</b> zaslána',
                'Message edit {name}' => 'Zpráva <b>{name}</b> upravena',
                'Message remove {name}' => 'Zpráva <b>{name}</b> smazána',
                'Message' => 'Zpráva',

                'Thread add {name}' => 'Vlákno <b>{name}</b> vytvořeno',
                'Thread edit {name}' => 'Vlákno <b>{name}</b> upraveno',
                'Thread remove {name}' => 'Vlákno <b>{name}</b> smazáno',
                'Thread' => 'Vlákno',
            ],
            'en' => [
            ]
        ];


        $this->saveTranslation(static::CS_LANG_ID, 'common.log', $translations['cs']);
        $this->saveTranslation(static::EN_LANG_ID, 'common.log', $translations['en']);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m190908_000000_mail_verification cannot be reverted.\n";

        return false;
    }

}
