<?php
/*
* Copyright 2023 DactylGroup s.r.o.
*
* Licensed under the EUPL, Version 1.2 or – as soon they
will be approved by the European Commission - subsequent
versions of the EUPL (the "Licence");
* You may not use this work except in compliance with the
Licence.
* You may obtain a copy of the Licence at:
*
* https://joinup.ec.europa.eu/software/page/eupl5
*
* Unless required by applicable law or agreed to in
writing, software distributed under the Licence is
distributed on an "AS IS" basis,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
express or implied.
* See the Licence for the specific language governing
permissions and limitations under the Licence.
*/
?>


<?php

use dactylcore\core\db\Migration;


/**
 * Class m190830_113326_i18n
 */
class m191212_000001_mac_address_err_translation extends Migration
{
    public function safeUp()
    {
        $this->lang();
    }


    protected function lang()
    {


        $translations = [
            'cs' => [
                'Invalid format of MAC address' => 'Nesprávný formát MAC adresy',
                'Invalid format of serial number' => 'Nesprávný formát výrobního čísla: povoleno max. 12 znaků, kombinace číslic a písmen anglické abecedy',
                'mac_address_hint' => 'MAC adresa ve tvaru 11:aa:22:bb:33:cc nebo 11aa22bb33cc',
                'serial_number_hint' => 'Výrobní číslo - max. 12 znaků, kombinace číslic a písmen anglické abecedy'
            ],
            'en' => [
                'Invalid format of MAC address' => 'Invalid format of MAC address',
                'Invalid format of serial number' => 'Invalid format of serial number',
                'mac_address_hint' => 'MAC address in format 11:aa:22:bb:33:cc nebo 11aa22bb33cc',
                'serial_number_hint' => 'Serial number - max 12 characters (letters or digits)'
            ]
        ];

        $this->saveTranslation(static::CS_LANG_ID, 'common.station', $translations['cs']);
        $this->saveTranslation(static::EN_LANG_ID, 'common.station', $translations['en']);
    }
}