<?php
/*
* Copyright 2023 DactylGroup s.r.o.
*
* Licensed under the EUPL, Version 1.2 or – as soon they
will be approved by the European Commission - subsequent
versions of the EUPL (the "Licence");
* You may not use this work except in compliance with the
Licence.
* You may obtain a copy of the Licence at:
*
* https://joinup.ec.europa.eu/software/page/eupl5
*
* Unless required by applicable law or agreed to in
writing, software distributed under the Licence is
distributed on an "AS IS" basis,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
express or implied.
* See the Licence for the specific language governing
permissions and limitations under the Licence.
*/
?>


<?php

use dactylcore\core\db\Migration;


/**
 * Class m190830_113326_i18n
 */
class m191218_000000_fixes extends Migration
{
    public function safeUp()
    {
        $this->lang();
    }


    protected function lang()
    {
        $translations = [
            'cs' => [
                'old_password' => 'původní heslo',
                'old password incorrect' => 'nevalidní původní heslo',
            ],
            'en' => [
            ]
        ];

        $this->saveTranslation(static::CS_LANG_ID, 'common.user', $translations['cs']);
        $this->saveTranslation(static::EN_LANG_ID, 'common.user', $translations['en']);
        $translations = [
            'cs' => [
                'old_password' => 'zadejte původní heslo pro změnu údajů',
            ],
            'en' => [
            ]
        ];

        $this->saveTranslation(static::CS_LANG_ID, 'frontend.register_hints', $translations['cs']);
        $this->saveTranslation(static::EN_LANG_ID, 'frontend.register_hints', $translations['en']);


    }
}

