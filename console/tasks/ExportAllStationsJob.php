<?php
/*
* Copyright 2023 DactylGroup s.r.o.
*
* Licensed under the EUPL, Version 1.2 or – as soon they
will be approved by the European Commission - subsequent
versions of the EUPL (the "Licence");
* You may not use this work except in compliance with the
Licence.
* You may obtain a copy of the Licence at:
*
* https://joinup.ec.europa.eu/software/page/eupl5
*
* Unless required by applicable law or agreed to in
writing, software distributed under the Licence is
distributed on an "AS IS" basis,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
express or implied.
* See the Licence for the specific language governing
permissions and limitations under the Licence.
*/
?>


<?php

namespace console\tasks;

use common\models\Station;
use dactylcore\jobs\console\tasks\BaseJob;
use yii\db\QueryInterface;

class ExportAllStationsJob extends BaseJob
{
    const QUEUE_MAX_EXECUTION_TIME = 60 * 10;

    /**
     * @var string
     */
    public string $email;

    /**
     * @var string
     */
    public string $domain;

    /**
     * @var string
     */
    public string $fileType;

    /**
     * @var QueryInterface $query
     */
    public QueryInterface $query;

    public function execute($queue)
    {
        parent::execute($queue);

        ini_set('memory_limit', '1024M');

        $exportedFilelink = Station::exportAll($this->domain, $this->fileType, $this->query);
        if ($exportedFilelink) {
            \Yii::$app->mailer->htmlLayout = '@core/mail/layouts/html';
            $mail = \Yii::$app->mailer
                ->compose('@common/mail/exported-stations', [
                     'exportedFilelink' => $exportedFilelink,
                ])
                ->setFrom(YII_ENV_DEV ? 'no-reply@localhost' : extractMailFromMailname(c('SMTP_FROM')))
                ->setTo($this->email)
                ->setSubject(_tF('export_finished_email_subject', 'station'));

            if ($mail->send()) {
                return true;
            }
        }
        return false;
    }
}