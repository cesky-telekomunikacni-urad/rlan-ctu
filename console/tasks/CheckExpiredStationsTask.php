<?php
/*
* Copyright 2023 DactylGroup s.r.o.
*
* Licensed under the EUPL, Version 1.2 or – as soon they
will be approved by the European Commission - subsequent
versions of the EUPL (the "Licence");
* You may not use this work except in compliance with the
Licence.
* You may obtain a copy of the Licence at:
*
* https://joinup.ec.europa.eu/software/page/eupl5
*
* Unless required by applicable law or agreed to in
writing, software distributed under the Licence is
distributed on an "AS IS" basis,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
express or implied.
* See the Licence for the specific language governing
permissions and limitations under the Licence.
*/
?>


<?php


namespace console\tasks;


use common\models\Message;
use common\models\Station;
use dactylcore\jobs\console\tasks\BaseTask;
use dactylcore\log\LogManager;
use yii\base\ErrorException;

class CheckExpiredStationsTask extends BaseTask
{
    public $schedule = '0 1 * * *';
    public $description = 'Checks stations validity dates and signs expired.';

    /**
     * @return mixed|void
     * @throws ErrorException
     */
    public function run()
    {
        if (!$this->setExpired()) {
            throw new ErrorException('Setting stations expired failed.');
        }
        LogManager::getInstance()->saveOperations(false);
    }

    /**
     * @return bool
     * @throws \Exception
     */
    public function setExpired(): bool
    {
        $success = true;
        $today = (new \DateTime('NOW'))->setTime(0, 0, 0);

        $stations = Station::find()
            ->select(['id', 'id_user'])
            ->andWhere(['<', 'protected_to', \Yii::$app->formatter->asTimestamp($today)])
            ->andWhere(['status' => Station::STATUS_FINISHED])
            ->asArray()
            ->all();

        $users = [];
        foreach ($stations as $station) {
            $users[($station['id_user'] ?? 0)][] = $station['id'];
        }

        // For all users try notification. If it fails, continue to another user
        foreach ($users as $user => $stations) {
            $transaction = \Yii::$app->db->beginTransaction();

            // All stations of one user
            try {
                // Try expire all stations of this user
                $stationModels = Station::find()
                    ->andWhere(['IN', 'id', $stations])
                    ->indexBy('id');

                /** @var Station $station */
                foreach ($stationModels->each() as $station) {
                    // Change status to expired
                    $station->status = Station::STATUS_EXPIRED;
                    $success &= $station->save(false);

                    if (!$success) {
                        throw new ErrorException($station->getFirstErrorMessage());
                    }
                }

                // Try send one mail to user, containing all stations
                try {
                    StationNotificationMailJob::pushJob([
                        'stationsIds' => $stations,
                        'userId' => $user,
                        'notificationType' => StationNotificationMailJob::NOTIFICATION_EXPIRED,
                    ]);
                } catch (\Exception $e) {
                    \Yii::error($e->getMessage());
                }

                // If all stations were saved, commit transaction
                if ($success) {
                    $transaction->commit();
                }
            } catch (\Exception $e) {
                $transaction->rollBack();
                \Yii::error($e->getMessage());
                continue;
            }

            \Yii::info("Expirovaly nasledujici stanice uzivatele {$user}: [" . implode(", ", $stations) . "]\n");
        }

        return $success;
    }
}