<?php
/*
* Copyright 2023 DactylGroup s.r.o.
*
* Licensed under the EUPL, Version 1.2 or – as soon they
will be approved by the European Commission - subsequent
versions of the EUPL (the "Licence");
* You may not use this work except in compliance with the
Licence.
* You may obtain a copy of the Licence at:
*
* https://joinup.ec.europa.eu/software/page/eupl5
*
* Unless required by applicable law or agreed to in
writing, software distributed under the Licence is
distributed on an "AS IS" basis,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
express or implied.
* See the Licence for the specific language governing
permissions and limitations under the Licence.
*/
?>


<?php

namespace console\tasks;

use common\models\Message;
use dactylcore\jobs\console\tasks\BaseJob;

class MessageNotificationJob extends BaseJob
{

    /**
     * @var int
     */
    public $messageId;

    /**
     * @var int
     */
    public $sendingUser;

    /**
     * @var string
     */
    public $subject;

    public function execute($queue)
    {
        parent::execute($queue);

        $message = Message::findOne($this->messageId);
        if (!$message) {
            return true;
        }
        $thread = $message->thread;

        // set theme mail layout
        \Yii::$app->mailer->htmlLayout = '@core/mail/layouts/html';

        $oppositeUser = $thread->getOppositeUser($message->id_author);
        $address = YII_DEBUG ? "60ghz@ctu.cz" : $oppositeUser->email;
        if ($oppositeUser) {
            // create verification email
            $mail = \Yii::$app->mailer->compose('@common/mail/message', [
                'userName' => $thread->getOppositeUsersName($oppositeUser->id),
                'messageContent' => $message->content,
            ])
                ->setFrom(extractMailFromMailname(c('SMTP_FROM')))
                ->setTo($address)
                ->setSubject($this->subject);

            if (!$mail->send()) {
                return false;
            }
        } else {
            \Yii::error("unable to send message for message {$this->messageId} because there is no opposite user");
        }

        return true;
    }

    /**
     * @param $messageId
     * @param $subject
     * @param $sendingUser
     * @return bool
     */
    public static function pushEmailJob($messageId, $subject, $sendingUser)
    {
        return (bool)\Yii::$app->queue->push(new MessageNotificationJob([
            'messageId' => $messageId,
            'subject' => $subject,
            'sendingUser' => $sendingUser,
        ]));
    }
}