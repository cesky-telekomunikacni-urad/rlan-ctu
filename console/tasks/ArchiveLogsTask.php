<?php
/*
* Copyright 2023 DactylGroup s.r.o.
*
* Licensed under the EUPL, Version 1.2 or – as soon they
will be approved by the European Commission - subsequent
versions of the EUPL (the "Licence");
* You may not use this work except in compliance with the
Licence.
* You may obtain a copy of the Licence at:
*
* https://joinup.ec.europa.eu/software/page/eupl5
*
* Unless required by applicable law or agreed to in
writing, software distributed under the Licence is
distributed on an "AS IS" basis,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
express or implied.
* See the Licence for the specific language governing
permissions and limitations under the Licence.
*/
?>


<?php


namespace console\tasks;

use dactylcore\jobs\console\tasks\BaseTask;
use dactylcore\log\common\components\LogArchiveComponent;
use dactylcore\log\LogManager;
use yii\base\ErrorException;

class ArchiveLogsTask extends BaseTask
{
    // Every month on the first day at 2:00 AM
    public $schedule = '0 2 1 * *';
    public $description = 'Archive logs from old months.';

    /**
     * @return void
     * @throws ErrorException
     */
    public function run(): void
    {
        if (!$this->archiveOldLogs()) {
            throw new ErrorException('Archivation of old log failed.');
        }
        LogManager::getInstance()->saveOperations(false);
    }

    /**
     * @return bool
     * @throws \Exception
     */
    public function archiveOldLogs(): bool
    {
        try {
            return LogArchiveComponent::archiveMonthlyLog();
        } catch (\Exception $e) {
            \Yii::error("Archivation of old log failed: {$e->getMessage()}");
            return false;
        }
    }
}