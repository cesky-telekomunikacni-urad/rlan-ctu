<?php
/*
* Copyright 2023 DactylGroup s.r.o.
*
* Licensed under the EUPL, Version 1.2 or – as soon they
will be approved by the European Commission - subsequent
versions of the EUPL (the "Licence");
* You may not use this work except in compliance with the
Licence.
* You may obtain a copy of the Licence at:
*
* https://joinup.ec.europa.eu/software/page/eupl5
*
* Unless required by applicable law or agreed to in
writing, software distributed under the Licence is
distributed on an "AS IS" basis,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
express or implied.
* See the Licence for the specific language governing
permissions and limitations under the Licence.
*/
?>


<?php


namespace console\tasks;


use common\models\Station;
use dactylcore\core\db\ActiveRecord;
use dactylcore\jobs\console\tasks\BaseJob;
use dactylcore\language\common\components\LanguageManager;
use dactylcore\log\LogManager;
use dactylcore\log\LogOperation;
use dactylcore\user\common\models\User;
use HConfig;
use yii\base\ErrorException;

class UserAllianceMailJob extends BaseJob
{
    /**
     * @var string $allianceName
     */
    public string $allianceName;
    /**
     * @var string|null $inviteToken
     */
    public string $inviteToken = '';
    /**
     * @var int $userId
     */
    public int $userId;
    /**
     * @var int $notificationType
     */
    public int $notificationType;
    const NOTIFICATION_INVITATION = 1;
    const NOTIFICATION_EXCLUDED = 2;
    const NOTIFICATION_DELETED = 3;

    public function execute($queue)
    {
        parent::execute($queue);

        return $this->sendMailNotification();
    }

    /**
     * @return bool
     */
    public function sendMailNotification(): bool
    {
        /**
         * @var LanguageManager $langManager
         */
        $langManager = \Yii::$app->getModule('dc-language')->getLanguageManager();
        $langManager->setLanguageByShortCode('cs');

        /**
         * @var $user User
         */
        $user = User::find()
                    ->select(['id', 'email'])
                    ->andWhere(['id' => $this->userId])
                    ->one();
        if (!$user) {
            return true;
        }

        // set theme mail layout
        \Yii::$app->mailer->htmlLayout = '@core/mail/layouts/html';
        $subject = $this->composeMessageHeadline();

        try {
            $address = YII_DEBUG ? "60ghz@ctu.cz" : $user->email;
            $mail = \Yii::$app->mailer->compose('@common/mail/user_alliance_notification', [
                'notificationHeadline' => $subject,
                'notificationContent' => $this->composeMessageContent(),
                'allianceName' => $this->allianceName,
                'inviteToken' => $this->inviteToken,
            ])
                                      ->setFrom(extractMailFromMailname(HConfig::getValue('SMTP_FROM')))
                                      ->setTo($address)
                                      ->setSubject($subject);

            $success = $mail->send();

            if ($success) {
                $logManager = LogManager::getInstance();
                $log = (new LogOperation());
                $log->operationType = ActiveRecord::OPERATION_TYPE_EVENT;
                $log->dataChanged = true;
                $log->modelId = $user->getName();
                $log->operations = [
                    'mail_notification' => [
                        '{type} alliance {name} was sent to email address {email}',
                        [
                            'name' => $this->allianceName,
                            'email' => $user->email,
                            'type' => $this->getNotificationTranslation($this->notificationType),
                        ],
                    ],
                ];
                $logManager->registerOperation($log);
            } else {
                throw new ErrorException('Unable to send mail notification');
            }
        } catch (\Exception $e) {
            \Yii::error($e->getMessage());
        }

        // Save logs
        LogManager::getInstance()->saveOperations(false);
        return true;
    }

    /**
     * @return string
     */
    protected function composeMessageHeadline(): string
    {
        switch ($this->notificationType) {
            case static::NOTIFICATION_INVITATION:
                return _tF('notification_invitation_subject', 'user-alliance', ['name' => $this->allianceName]);
            case static::NOTIFICATION_EXCLUDED:
                return _tF('notification_excluded_subject', 'user-alliance', ['name' => $this->allianceName]);
            case static::NOTIFICATION_DELETED:
                return _tF('notification_deleted_subject', 'user-alliance', ['name' => $this->allianceName]);
            default:
                return '';
        }
    }

    /**
     * @param string $stationName
     *
     * @return string
     */
    protected function composeMessageContent(): string
    {
        switch ($this->notificationType) {
            case static::NOTIFICATION_INVITATION:
                return _tF('notification_invitation_text', 'user-alliance');
            case static::NOTIFICATION_EXCLUDED:
                return _tF('notification_excluded_text', 'user-alliance');
            case static::NOTIFICATION_DELETED:
                return _tF('notification_deleted_text', 'user-alliance');
            default:
                return '';
        }
    }

    protected function getNotificationTranslation($notifType)
    {
        switch ($this->notificationType) {
            case static::NOTIFICATION_INVITATION:
                return _tF('invitation_type', 'user-alliance');
            case static::NOTIFICATION_EXCLUDED:
                return _tF('exclusion_type', 'user-alliance');
            case static::NOTIFICATION_DELETED:
                return _tF('deletion_type', 'user-alliance');
            default:
                return '';
        }
    }
}