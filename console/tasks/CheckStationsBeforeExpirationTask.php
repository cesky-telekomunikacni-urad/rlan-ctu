<?php
/*
* Copyright 2023 DactylGroup s.r.o.
*
* Licensed under the EUPL, Version 1.2 or – as soon they
will be approved by the European Commission - subsequent
versions of the EUPL (the "Licence");
* You may not use this work except in compliance with the
Licence.
* You may obtain a copy of the Licence at:
*
* https://joinup.ec.europa.eu/software/page/eupl5
*
* Unless required by applicable law or agreed to in
writing, software distributed under the Licence is
distributed on an "AS IS" basis,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
express or implied.
* See the Licence for the specific language governing
permissions and limitations under the Licence.
*/
?>


<?php


namespace console\tasks;


use common\models\Station;
use dactylcore\jobs\console\tasks\BaseTask;
use dactylcore\log\LogManager;
use yii\helpers\ArrayHelper;

class CheckStationsBeforeExpirationTask extends BaseTask
{
    public $schedule = '0 12 * * *';
    public $description = 'Sends notification email before station expiration';
    protected $_stations = [];

    public function run()
    {
        $this->notifyStations('valid_to', 'REGISTRATION_PERIOD_WARNING');
        $this->notifyStations('protected_to', 'PROTECTION_PERIOD_WARNING');
        $this->sendMailNotifications();
    }

    /**
     * @param string $columnToExpire
     * @param string $confWarningKey
     */
    public function notifyStations(string $columnToExpire, string $confWarningKey)
    {
        $periodWarning = c($confWarningKey);
        $stations = static::findStationsByTimestamp($columnToExpire, $periodWarning);

        $notificationType = $columnToExpire == 'valid_to'
            ? StationNotificationMailJob::NOTIFICATION_REGISTRATION_WARNING
            : StationNotificationMailJob::NOTIFICATION_PROTECTION_WARNING;

        foreach ($stations as $station) {
            $this->_stations[$notificationType][$station['id_user']][] = $station['id'];
        }
    }

    public function sendMailNotifications()
    {
        foreach ($this->_stations as $notificationType => $users) {
            foreach ($users as $userId => $stations) {
                try {
                    StationNotificationMailJob::pushJob([
                        'stationsIds' => $stations,
                        'userId' => $userId,
                        'notificationType' => $notificationType,
                    ]);
                } catch (\Exception $e) {
                    \Yii::error($e->getMessage());
                }
            }
        }
    }

    /**
     * @param string $timestampColumn
     * @param $periodWarning
     *
     * @return array|\yii\db\ActiveRecord[]
     */
    public static function findStationsByTimestamp(string $timestampColumn, $periodWarning)
    {
        $date = date('Y-m-d', strtotime("+{$periodWarning} months"));

        return Station::find()
                      ->select(['id', 'id_user'])
                        // Do not check DRAFT, WAITING and EXPIRED
                      ->andWhere(['status' => Station::STATUS_FINISHED])
                      ->filterDateRange("{$date} - {$date}", $timestampColumn, 'Y-m-d')
                      ->asArray()
                      ->all();
    }
}