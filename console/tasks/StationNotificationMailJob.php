<?php
/*
* Copyright 2023 DactylGroup s.r.o.
*
* Licensed under the EUPL, Version 1.2 or – as soon they
will be approved by the European Commission - subsequent
versions of the EUPL (the "Licence");
* You may not use this work except in compliance with the
Licence.
* You may obtain a copy of the Licence at:
*
* https://joinup.ec.europa.eu/software/page/eupl5
*
* Unless required by applicable law or agreed to in
writing, software distributed under the Licence is
distributed on an "AS IS" basis,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
express or implied.
* See the Licence for the specific language governing
permissions and limitations under the Licence.
*/
?>


<?php


namespace console\tasks;


use common\models\Station;
use dactylcore\core\db\ActiveRecord;
use dactylcore\jobs\console\tasks\BaseJob;
use dactylcore\language\common\components\LanguageManager;
use dactylcore\log\LogManager;
use dactylcore\log\LogOperation;
use dactylcore\user\common\models\User;
use HConfig;
use yii\base\ErrorException;

class StationNotificationMailJob extends BaseJob
{
    /**
     * @var array $stationsIds
     */
    public array $stationsIds;
    /**
     * @var int $userId
     */
    public int $userId;
    /**
     * @var int $notificationType
     */
    public int $notificationType;
    const NOTIFICATION_EXPIRED = 1;
    const NOTIFICATION_REGISTRATION_WARNING = 2;
    const NOTIFICATION_PROTECTION_WARNING = 3;

    public function execute($queue)
    {
        parent::execute($queue);

        return $this->sendMailNotification();
    }

    /**
     * @return bool
     */
    public function sendMailNotification(): bool
    {
        /**
         * @var LanguageManager $langManager
         */
        $langManager = \Yii::$app->getModule('dc-language')->getLanguageManager();
        $langManager->setLanguageByShortCode('cs');

        /**
         * @var $user User
         */
        $user = User::find()
            ->select(['id', 'email'])
            ->andWhere(['id' => $this->userId])
            ->one();
        if (!$user) {
            return true;
        }

        $stations = Station::find()
            ->select(['id', 'name', 'type'])
            ->andWhere(['IN', 'id', $this->stationsIds])
            ->asArray()
            ->all();

        // set theme mail layout
        \Yii::$app->mailer->htmlLayout = '@core/mail/layouts/html';
        $subject = $this->composeMessageHeadline();

        try {
            $address = YII_DEBUG ? "60ghz@ctu.cz" : $user->email;
            $mail = \Yii::$app->mailer->compose('@common/mail/station_notification', [
                'notificationHeadline' => $subject,
                'notificationContent' => $this->composeMessageContent(),
                'stations' => $stations,
            ])
                ->setFrom(extractMailFromMailname(HConfig::getValue('SMTP_FROM')))
                ->setTo($address)
                ->setSubject($subject);

            $success = $mail->send();

            if ($success) {
                $logManager = LogManager::getInstance();
                $log = (new LogOperation());
                $log->operationType = ActiveRecord::OPERATION_TYPE_EVENT;
                $log->dataChanged = true;
                $log->modelId = $user->getName();
                $log->operations = [
                    'mail_notification' => [
                        'warning for stations [{names}] was sent to email address {email}',
                        [
                            'names' => implode(", ", $this->stationsIds),
                            'email' => $user->email,
                        ],
                    ],
                ];
                $logManager->registerOperation($log);
            } else {
                throw new ErrorException('Unable to send mail notification');
            }
        } catch (\Exception $e) {
            \Yii::error($e->getMessage());
        }

        // Save logs
        LogManager::getInstance()->saveOperations(false);
        return true;
    }

    /**
     * @return string
     */
    protected function composeMessageHeadline(): string
    {
        switch ($this->notificationType) {
            case static::NOTIFICATION_EXPIRED:
                return _tF('notification_expired_headline', 'station-notification');
            case static::NOTIFICATION_REGISTRATION_WARNING:
                return _tF('notification_registration_period_headline', 'station-notification');
            case static::NOTIFICATION_PROTECTION_WARNING:
                return _tF('notification_protection_period_headline', 'station-notification');
            default:
                return '';
        }
    }

    /**
     * @param string $stationName
     *
     * @return string
     */
    protected function composeMessageContent(): string
    {
        $string = "your stations ";

        switch ($this->notificationType) {
            case static::NOTIFICATION_EXPIRED:
                $warning = 0;
                $string .= "have expired:";
                break;
            case static::NOTIFICATION_REGISTRATION_WARNING:
                $warning = HConfig::getValue('REGISTRATION_PERIOD_WARNING');
                $string .= "will end validity in {warning} months:";
                break;
            case static::NOTIFICATION_PROTECTION_WARNING:
                $warning = HConfig::getValue('PROTECTION_PERIOD_WARNING');
                $string .= "will end protection in {warning} months:";
                break;
            default:
                \Yii::info("Undefined notification type: {$this->notificationType}");
                return '';
        }

        return _tF($string, 'station-notification', ['warning' => $warning]);
    }
}