<?php
/*
* Copyright 2023 DactylGroup s.r.o.
*
* Licensed under the EUPL, Version 1.2 or – as soon they
will be approved by the European Commission - subsequent
versions of the EUPL (the "Licence");
* You may not use this work except in compliance with the
Licence.
* You may obtain a copy of the Licence at:
*
* https://joinup.ec.europa.eu/software/page/eupl5
*
* Unless required by applicable law or agreed to in
writing, software distributed under the Licence is
distributed on an "AS IS" basis,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
express or implied.
* See the Licence for the specific language governing
permissions and limitations under the Licence.
*/
?>


<?php


namespace console\tasks;


use common\models\Station;
use dactylcore\core\db\ActiveRecord;
use dactylcore\jobs\console\tasks\BaseJob;
use dactylcore\language\common\components\LanguageManager;
use dactylcore\log\LogManager;
use dactylcore\log\LogOperation;
use dactylcore\user\common\models\User;
use HConfig;
use yii\base\ErrorException;

class ApiTestingInfoMailJob extends BaseJob
{
    /**
     * @var int $userId
     */
    public int $userId;

    /**
     * @var string $title
     */
    public string $title;

    /**
     * @var string $content
     */
    public string $content;

    /**
     * @var string $subject
     */
    public string $subject;

    public function execute($queue)
    {
        parent::execute($queue);

        return $this->sendMailInfo();
    }

    /**
     * @return bool
     */
    public function sendMailInfo(): bool
    {
        /**
         * @var $user User
         */
        $user = User::find()
            ->select(['id', 'email'])
            ->andWhere(['id' => $this->userId])
            ->one();
        if (!$user) {
            return true;
        }

        // set theme mail layout
        \Yii::$app->mailer->htmlLayout = '@core/mail/layouts/html';

        try {
            $address = env('API_TESTING_MAIL_ADDRESS', $user->email);
            $mail = \Yii::$app->mailer->compose('@common/mail/api_testing_info', [
                'headline' => $this->subject,
                'title' => $this->title,
                'content' => $this->content,
            ])
                ->setFrom(extractMailFromMailname(HConfig::getValue('SMTP_FROM')))
                ->setTo($address)
                ->setSubject($this->subject);

            $success = $mail->send();

            if ($success) {
                $logManager = LogManager::getInstance();
                $log = (new LogOperation());
                $log->operationType = ActiveRecord::OPERATION_TYPE_EVENT;
                $log->dataChanged = true;
                $log->modelId = $user->getName();
                $log->operations = [
                    'mail_notification' => [
                        'info about API was sent to email address {email}',
                        [
                            'email' => $user->email,
                        ],
                    ],
                ];
                $logManager->registerOperation($log);
            } else {
                throw new ErrorException('Unable to send mail notification');
            }
        } catch (\Exception $e) {
            \Yii::error($e);
            \Yii::error($e->getMessage());
        }

        // Save logs
        LogManager::getInstance()->saveOperations(false);
        return true;
    }
}