<?php
/*
* Copyright 2023 DactylGroup s.r.o.
*
* Licensed under the EUPL, Version 1.2 or – as soon they
will be approved by the European Commission - subsequent
versions of the EUPL (the "Licence");
* You may not use this work except in compliance with the
Licence.
* You may obtain a copy of the Licence at:
*
* https://joinup.ec.europa.eu/software/page/eupl5
*
* Unless required by applicable law or agreed to in
writing, software distributed under the Licence is
distributed on an "AS IS" basis,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
express or implied.
* See the Licence for the specific language governing
permissions and limitations under the Licence.
*/
?>


<?php

$consoleConfig = [
    'params' => require __DIR__ . '/params.php',
    'bootstrap' => [
        'dc-menu',
        'dc-page',
        'dc-log',
    ],

    'modules' => [
        'dc-menu' => [
            'class' => dactylcore\menu\console\Module::class,
        ],
        'dc-page' => [
            'class' => dactylcore\page\console\Module::class,
        ],
        'dc-log' => [
            'class' => dactylcore\log\console\Module::class,
        ],
    ],

    'controllerMap' => [
        'log-archive' => [
            'class' => dactylcore\log\console\controllers\LogArchiveController::class,
        ],
    ],
];

return $consoleConfig;