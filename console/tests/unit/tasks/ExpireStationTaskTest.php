<?php
/*
* Copyright 2023 DactylGroup s.r.o.
*
* Licensed under the EUPL, Version 1.2 or – as soon they
will be approved by the European Commission - subsequent
versions of the EUPL (the "Licence");
* You may not use this work except in compliance with the
Licence.
* You may obtain a copy of the Licence at:
*
* https://joinup.ec.europa.eu/software/page/eupl5
*
* Unless required by applicable law or agreed to in
writing, software distributed under the Licence is
distributed on an "AS IS" basis,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
express or implied.
* See the Licence for the specific language governing
permissions and limitations under the Licence.
*/
?>


<?php

use Codeception\Test\Unit;
use common\models\Station;
use common\models\StationFs;
use common\models\StationFsPair;
use common\models\StationWigig;
use console\tasks\CheckExpiredStationsTask;
use console\tasks\CheckStationsBeforeExpirationTask;
use console\tasks\StationNotificationMailJob;
use dactylcore\core\db\Query;
use dactylcore\core\helpers\Date;
use dactylcore\log\common\models\Log;
use dactylcore\log\LogManager;
use frontend\controllers\StationController;
use modules\user\common\models\User;


class ExpireStationTaskTest extends Unit
{
    public $idsToRestore = [];
    public static $macPool = [
        '81:EC:07:D2:62:10',
        '81:EC:07:D2:62:11',
        '81:EC:07:D2:62:12',
        '81:EC:07:D2:62:13',
        '81:EC:07:D2:62:14',
        '81:EC:07:D2:62:15',
        '81:EC:07:D2:62:16',
        '81:EC:07:D2:62:17',
        '81:EC:07:D2:62:18',
        '81:EC:07:D2:62:19',
        '81:EC:07:D2:62:20',
        '81:EC:07:D2:62:21',
        '81:EC:07:D2:62:22',
        '81:EC:07:D2:62:23',
        '81:EC:07:D2:62:24',
        '81:EC:07:D2:62:25',
        '81:EC:07:D2:62:26',
        '81:EC:07:D2:62:27',
        '81:EC:07:D2:62:28',
        '81:EC:07:D2:62:29',
        '81:EC:07:D2:62:30',
        '81:EC:07:D2:62:31',
        '81:EC:07:D2:62:32',
        '81:EC:07:D2:62:33',
        '81:EC:07:D2:62:34',
        '81:EC:07:D2:62:35',
        '81:EC:07:D2:62:36',
        '81:EC:07:D2:62:37',
        '81:EC:07:D2:62:38',
        '81:EC:07:D2:62:39',
        '81:EC:07:D2:62:40',
        '81:EC:07:D2:62:41',
        '81:EC:07:D2:62:42',
        '81:EC:07:D2:62:43',
        '81:EC:07:D2:62:44',
        '81:EC:07:D2:62:45',
        '81:EC:07:D2:62:46',
        '81:EC:07:D2:62:47',
        '81:EC:07:D2:62:48',
        '81:EC:07:D2:62:49',
        '81:EC:07:D2:62:50',
    ];

    public function _before()
    {
        $this->idsToRestore = array_keys(Station::find()->select('id')->indexBy('id')->all());
        if ($this->idsToRestore) {
            Yii::$app->db->createCommand('UPDATE station SET deleted=1 WHERE id IN (' .
                implode(',', $this->idsToRestore) .
                ')')->execute();
        }
    }

    public function _after()
    {
        if ($this->idsToRestore) {
            Yii::$app->db->createCommand('UPDATE station SET deleted=0 WHERE id IN (' .
                implode(',', $this->idsToRestore) .
                ')')->execute();
        }
    }

    public function testExpiredStatus()
    {
        /**
         * CHECK UNEXPIRED STATIONS
         */

        $date = new Date();
        $date->modify("-1 day");

        $wigExpiredStation = $this->buildWigig(null, $date);
        $wigDraftStation = $this->buildWigig(null, $date);
        $wigWaitStation = $this->buildWigig(null, $date);

        // Stations with non finished status
        $wigExpiredStation->moveToStatus(Station::STATUS_EXPIRED);
        $this->assertTrue($wigExpiredStation->save());
        $wigDraftStation->moveToStatus(Station::STATUS_DRAFT);
        $this->assertTrue($wigDraftStation->save());
        $wigWaitStation->moveToWaiting();
        $this->assertTrue($wigWaitStation->save());


        // Stations with protected date in future, so do not expire them
        $date->modify("+1 day"); // today
        $pairFinishedStation0 = $this->buildFs(null, $date);

        $date->modify("+1 day"); // tomorrow
        $pairFinishedStation1 = $this->buildFs(null, $date);

        $date->modify("+1 day");
        $pairFinishedStation2 = $this->buildFs(null, $date);

        $date->modify("+2 day");
        $pairFinishedStation4 = $this->buildFs(null, $date);

        // Try to push mails, but none should be pushed
        $expireTask = new CheckExpiredStationsTask();
        $this->assertTrue($expireTask->setExpired(), 'setExpired task cannot be done!');


        // Check notification task in Queue
        $nowTime = new DateTime("NOW");
        $nowTime->modify("-2 minutes");

        $queueJobs = (new Query())
            ->from('queue')
            ->andWhere(['>', 'pushed_at', \Yii::$app->formatter->asTimestamp($nowTime)])
            ->all();

        foreach ($queueJobs as $job) {
            $object = unserialize($job['job']);

            if ($object instanceof StationNotificationMailJob) {
                if ((isset($object->userId) && $object->userId == 1) &&
                    (isset($object->notificationType) &&
                        $object->notificationType == StationNotificationMailJob::NOTIFICATION_EXPIRED)) {
                    // StationsID must be set
                    $this->assertTrue(isset($object->stationsIds));

                    // Our created stations cannot be in the notification mail!!
                    $this->assertEmpty(array_intersect(
                        $object->stationsIds,
                        [
                            $wigExpiredStation->id,
                            $wigDraftStation->id,
                            $wigWaitStation->id,
                            $pairFinishedStation0->stationA->id,
                            $pairFinishedStation0->stationB->id,
                            $pairFinishedStation1->stationA->id,
                            $pairFinishedStation1->stationB->id,
                            $pairFinishedStation2->stationA->id,
                            $pairFinishedStation2->stationB->id,
                            $pairFinishedStation4->stationA->id,
                            $pairFinishedStation4->stationB->id,
                        ]
                    ));
                }
            }
        }

        // Statuses must be unchanged (maby using refresh() would look better..)
        $this->assertEquals(Station::STATUS_EXPIRED, Station::findOne($wigExpiredStation->id)->status);
        $this->assertEquals(Station::STATUS_WAITING, Station::findOne($wigWaitStation->id)->status);
        $this->assertEquals(Station::STATUS_DRAFT, Station::findOne($wigDraftStation->id)->status);
        $this->assertEquals(Station::STATUS_FINISHED, Station::findOne($pairFinishedStation0->stationA->id)->status);
        $this->assertEquals(Station::STATUS_FINISHED, Station::findOne($pairFinishedStation0->stationB->id)->status);
        $this->assertEquals(Station::STATUS_FINISHED, Station::findOne($pairFinishedStation1->stationA->id)->status);
        $this->assertEquals(Station::STATUS_FINISHED, Station::findOne($pairFinishedStation1->stationB->id)->status);
        $this->assertEquals(Station::STATUS_FINISHED, Station::findOne($pairFinishedStation2->stationA->id)->status);
        $this->assertEquals(Station::STATUS_FINISHED, Station::findOne($pairFinishedStation2->stationB->id)->status);
        $this->assertEquals(Station::STATUS_FINISHED, Station::findOne($pairFinishedStation4->stationA->id)->status);
        $this->assertEquals(Station::STATUS_FINISHED, Station::findOne($pairFinishedStation4->stationB->id)->status);

        $this->deleteWigig($wigExpiredStation);
        $this->deleteWigig($wigDraftStation);
        $this->deleteWigig($wigWaitStation);
        $this->deleteFs($pairFinishedStation0);
        $this->deleteFs($pairFinishedStation1);
        $this->deleteFs($pairFinishedStation2);
        $this->deleteFs($pairFinishedStation4);


        /**
         * CHECK the expired stations
         */

        $date = new Date();

        // Protected_to in the past
        $date->modify("-1 day");
        $wigFinished = $this->buildWigig(null, $date);
        $date->modify("-1 day");
        $pairFinished = $this->buildFs(null, $date);
        $date->modify("-10 day");
        $wigFinishedOld = $this->buildWigig(null, $date);
        // clear operations
        LogManager::getInstance()->saveOperations(false);

        // Try to push mails
        $this->assertTrue($expireTask->setExpired(), 'setExpired task cannot be done!');
        LogManager::getInstance()->saveOperations(false);
        // There must be email about expiration
        $nowTime = new DateTime("NOW");
        $nowTime->modify("-2 minutes");

        $queueJobs = (new Query())
            ->from('queue')
            ->andWhere(['>', 'pushed_at', \Yii::$app->formatter->asTimestamp($nowTime)])
            ->all();

        // Try to find our pushed mail JOB
        $notifCnt = 0;
        $notifIds = [];
        $this->assertGreaterThan(0, count($queueJobs));
        foreach ($queueJobs as $job) {
            $object = unserialize($job['job']);
            $this->assertInstanceOf(StationNotificationMailJob::class, $object);
            if ($object instanceof StationNotificationMailJob) {
                if ((isset($object->userId) && $object->userId == 1) &&
                    (isset($object->notificationType) &&
                        $object->notificationType == StationNotificationMailJob::NOTIFICATION_EXPIRED)) {
                    // StationsID must be set
                    $this->assertTrue(isset($object->stationsIds));

                    // Our created stations cannot be in the notification mail!!
                    if ($intersect = array_intersect($object->stationsIds, [
                        $wigFinished->id,
                        $wigFinishedOld->id,
                        $pairFinished->stationA->id,
                        $pairFinished->stationB->id,
                    ])) {
                        $notifCnt++;
                        array_push($notifIds, $job['id']);
                        // All stations must be in that mail
                        $this->assertEquals(4, count($intersect), 'Count of stations in notification is wrong!');
                    }
                }
            }
        }
        // There must be only one notification with ours Stations!!
        $this->assertEquals(1, $notifCnt, 'Wrong count of pushed notifications!!');

        // Delete those jobs (notifications)
        foreach ($notifIds as $notifId) {
            (new Query)
                ->createCommand()
                ->delete('queue', ['id' => $notifId])
                ->execute();
        }

        // Finished stations must be now expired
        $this->assertEquals(Station::STATUS_EXPIRED, Station::findOne($wigFinished->id)->status);
        $this->assertEquals(Station::STATUS_EXPIRED, Station::findOne($wigFinishedOld->id)->status);
        $this->assertEquals(Station::STATUS_EXPIRED, Station::findOne($pairFinished->stationA->id)->status);
        $this->assertEquals(Station::STATUS_EXPIRED, Station::findOne($pairFinished->stationB->id)->status);

        $this->deleteWigig($wigFinished);
        $this->deleteWigig($wigFinishedOld);
        $this->deleteFs($pairFinished);
    }

    public function testValidToNotification()
    {
        /**
         * CHECK VALID TO -> WITHOUT notification
         */

        $dateValid = new Date();

        $periodWarning = c('REGISTRATION_PERIOD_WARNING');
        $dateValid->modify("+{$periodWarning} month");

        $dateValid->modify("-1 day");
        $wigMinus1 = $this->buildWigig($dateValid);
        $dateValid->modify("-1 day");
        $wigMinus2 = $this->buildWigig($dateValid);
        $dateValid->modify("-3 day");
        $wigMinus5 = $this->buildWigig($dateValid);

        // Today
        $dateValid->modify("+5 day");

        $dateValid->modify("+1 day");
        $pairPlus1 = $this->buildFs($dateValid);
        $dateValid->modify("+2 day");
        $pairPlus2 = $this->buildFs($dateValid);

        // Try to push mails, but none should be pushed
        $notifyTask = new CheckStationsBeforeExpirationTask();
        $this->assertNull($notifyTask->run(), 'NotifyTask task cannot be done!');

        // There must not be any email about validation
        $nowTime = new DateTime("NOW");
        $nowTime->modify("-2 minutes");

        // Read jobs
        $queueJobs = (new Query())
            ->from('queue')
            ->andWhere(['>', 'pushed_at', \Yii::$app->formatter->asTimestamp($nowTime)])
            ->all();

        // Check jobs
        foreach ($queueJobs as $job) {
            $object = unserialize($job['job']);

            if ($object instanceof StationNotificationMailJob) {
                if ((isset($object->userId) && $object->userId == 1) &&
                    (isset($object->notificationType) &&
                        $object->notificationType == StationNotificationMailJob::NOTIFICATION_REGISTRATION_WARNING)) {
                    // StationsID must be set
                    $this->assertTrue(isset($object->stationsIds));

                    // Our created stations cannot be in the notification mail!!
                    $this->assertEmpty(array_intersect(
                        $object->stationsIds,
                        [
                            $wigMinus1->id,
                            $wigMinus2->id,
                            $wigMinus5->id,
                            $pairPlus1->stationA->id,
                            $pairPlus1->stationB->id,
                            $pairPlus2->stationA->id,
                            $pairPlus2->stationB->id,
                        ]
                    ));
                }
            }
        }

        $this->deleteWigig($wigMinus1);
        $this->deleteWigig($wigMinus2);
        $this->deleteWigig($wigMinus5);
        $this->deleteFs($pairPlus1);
        $this->deleteFs($pairPlus2);


        /**
         * CHECK VALID TO -> WITH notification
         */

        $dateValid = new Date();
        $dateValid->modify("+{$periodWarning} month");

        $wig0 = $this->buildWigig($dateValid);
        $pair0 = $this->buildFs($dateValid);

        // Try to push mails
        $notifyTask = new CheckStationsBeforeExpirationTask();
        $this->assertNull($notifyTask->run(), 'NotifyTask task cannot be done!');

        // There must be one email about validation
        $nowTime = new DateTime("NOW");
        $nowTime->modify("-2 minutes");

        $queueJobs = (new Query())
            ->from('queue')
            ->andWhere(['>', 'pushed_at', \Yii::$app->formatter->asTimestamp($nowTime)])
            ->all();

        // Check pushed jobs
        $notifCnt = 0;
        $notifIds = [];
        foreach ($queueJobs as $job) {
            $object = unserialize($job['job']);

            if ($object instanceof StationNotificationMailJob) {
                if ((isset($object->userId) && $object->userId == 1) &&
                    (isset($object->notificationType) &&
                        $object->notificationType == StationNotificationMailJob::NOTIFICATION_REGISTRATION_WARNING)) {
                    // StationsID must be set
                    $this->assertTrue(isset($object->stationsIds));

                    // Our created stations cannot be in the notification mail!!
                    if ($intersect = array_intersect($object->stationsIds, [
                        $wig0->id,
                        $pair0->stationA->id,
                        $pair0->stationB->id,
                    ])) {
                        $notifCnt++;
                        array_push($notifIds, $job['id']);
                        $this->assertEquals(3, count($intersect), 'Count of stations in notification is wrong!');
                    }
                }
            }
        }

        // There must be only one notification with ours Stations!!
        $this->assertEquals(1, $notifCnt, 'Wrong count of pushed notifications!!');

        // Delete those jobs (notifications)
        foreach ($notifIds as $notifId) {
            (new Query)
                ->createCommand()
                ->delete('queue', ['id' => $notifId])
                ->execute();
        }

        $this->deleteWigig($wig0);
        $this->deleteFs($pair0);
    }

    public function testProtectedToNotification()
    {
        /**
         * CHECK PROTECTED TO -> WITHOUT notification
         */

        $dateProtected = new Date();

        $periodWarning = c('PROTECTION_PERIOD_WARNING');
        $dateProtected->modify("+{$periodWarning} month");

        $dateProtected->modify("-1 day");
        $wigMinus1 = $this->buildWigig(null, $dateProtected);
        $dateProtected->modify("-1 day");
        $wigMinus2 = $this->buildWigig(null, $dateProtected);
        $dateProtected->modify("-3 day");
        $wigMinus5 = $this->buildWigig(null, $dateProtected);

        // Today
        $dateProtected->modify("+5 day");

        $dateProtected->modify("+1 day");
        $pairPlus1 = $this->buildFs(null, $dateProtected);
        $dateProtected->modify("+2 day");
        $pairPlus2 = $this->buildFs(null, $dateProtected);

        // Try to push mails, but none should be pushed
        $notifyTask = new CheckStationsBeforeExpirationTask();
        $this->assertNull($notifyTask->run(), 'NotifyTask task cannot be done!');

        // There must not be any email about validation
        $nowTime = new DateTime("NOW");
        $nowTime->modify("-2 minutes");

        $queueJobs = (new Query())
            ->from('queue')
            ->andWhere(['>', 'pushed_at', \Yii::$app->formatter->asTimestamp($nowTime)])
            ->all();

        // Check jobs
        foreach ($queueJobs as $job) {
            $object = unserialize($job['job']);

            if ($object instanceof StationNotificationMailJob) {
                if ((isset($object->userId) && $object->userId == 1) &&
                    (isset($object->notificationType) &&
                        $object->notificationType == StationNotificationMailJob::NOTIFICATION_PROTECTION_WARNING)) {
                    // StationsID must be set
                    $this->assertTrue(isset($object->stationsIds));

                    // Our created stations cannot be in the notification mail!!
                    $this->assertEmpty(array_intersect(
                        $object->stationsIds,
                        [
                            $wigMinus1->id,
                            $wigMinus2->id,
                            $wigMinus5->id,
                            $pairPlus1->stationA->id,
                            $pairPlus1->stationB->id,
                            $pairPlus2->stationA->id,
                            $pairPlus2->stationB->id,
                        ]
                    ));
                }
            }
        }

        $this->deleteWigig($wigMinus1);
        $this->deleteWigig($wigMinus2);
        $this->deleteWigig($wigMinus5);
        $this->deleteFs($pairPlus1);
        $this->deleteFs($pairPlus2);


        /**
         * CHECK PROTECTED TO -> WITH notification
         */

        $dateProtected = new Date();
        $dateProtected->modify("+{$periodWarning} month");

        $wig0 = $this->buildWigig(null, $dateProtected);
        $pair0 = $this->buildFs(null, $dateProtected);

        // Try to push mails
        $notifyTask = new CheckStationsBeforeExpirationTask();
        $this->assertNull($notifyTask->run(), 'NotifyTask task cannot be done!');

        // There must be one email about validation
        $nowTime = new DateTime("NOW");
        $nowTime->modify("-2 minutes");

        $queueJobs = (new Query())
            ->from('queue')
            ->andWhere(['>', 'pushed_at', \Yii::$app->formatter->asTimestamp($nowTime)])
            ->all();

        // Check all notifications
        $notifCnt = 0;
        $notifIds = [];
        foreach ($queueJobs as $job) {
            $object = unserialize($job['job']);

            if ($object instanceof StationNotificationMailJob) {
                if ((isset($object->userId) && $object->userId == 1) &&
                    (isset($object->notificationType) &&
                        $object->notificationType == StationNotificationMailJob::NOTIFICATION_PROTECTION_WARNING)) {
                    // StationsID must be set
                    $this->assertTrue(isset($object->stationsIds));

                    // Our created stations cannot be in the notification mail!!
                    if ($intersect = array_intersect($object->stationsIds, [
                        $wig0->id,
                        $pair0->stationA->id,
                        $pair0->stationB->id,
                    ])) {
                        $notifCnt++;
                        array_push($notifIds, $job['id']);
                        $this->assertEquals(3, count($intersect), 'Count of stations in notification is wrong!');
                    }
                }
            }
        }
        // There must be only one notification with ours Stations!!
        $this->assertEquals(1, $notifCnt, 'Wrong count of pushed notifications!!');

        // Delete those jobs (notifications)
        foreach ($notifIds as $notifId) {
            (new Query)
                ->createCommand()
                ->delete('queue', ['id' => $notifId])
                ->execute();
        }

        $this->deleteWigig($wig0);
        $this->deleteFs($pair0);
    }

    /**
     * @throws Exception
     */
    public function testNotificationSending()
    {
        $date = new Date();
        $date->modify("-1 day");

        // Create finished stations with expired protected time
        $wigMail = $this->buildWigig(null, $date);
        $pairMail = $this->buildFs(null, $date);
        // Have to save operations in logs. Without it, mail sending will crash.

        // For test we are using superadmin account
        $userId = 1;

        // Set fake Notification JOB
        $mailJob = new StationNotificationMailJob();
        $mailJob->stationsIds = [$wigMail->id, $pairMail->stationA->id, $pairMail->stationB->id];
        $mailJob->userId = $userId;
        $mailJob->notificationType = StationNotificationMailJob::NOTIFICATION_EXPIRED;

        // Send the mail
        $this->assertTrue($mailJob->sendMailNotification());

        // Cannot check "send()" response itself. But we can check log of the operation.
        $nowTime = new DateTime("NOW");
        $nowTime->modify("-1 minutes");

        // Get last logs
        $lastLogs = Log::find()->andWhere(['>', 'created_at', \Yii::$app->formatter->asTimestamp($nowTime)])->all();

        $ourLog = null;
        foreach ($lastLogs as $lastLog) {
            $operations = json_decode($lastLog->operations);
            if (isset($operations->mail_notification) &&
                isset($operations->mail_notification[1]) &&
                (isset($operations->mail_notification[1]->email) && $operations->mail_notification[1]->names)) {

                if (User::findOne($userId)->email == $operations->mail_notification[1]->email &&
                    count(array_intersect(      // Must contain our stations
                        mb_split(", ", $operations->mail_notification[1]->names),
                        [$wigMail->id, $pairMail->stationA->id, $pairMail->stationB->id]))) {

                    // If it contains what we need, it's our wanted log
                    $ourLog = $lastLog;
                }
            }
        }

        // Check if our log really exists
        $this->assertNotNull($ourLog);

        $ourLog->hardDelete();
        $this->deleteWigig($wigMail);
        $this->deleteFs($pairMail);

        /**
         * CHECK SENDING WITH ERROR -> WITHOUT LOG
         */

        $lastUser = User::find()->select('id')->orderBy(['id' => SORT_DESC])->asArray()->one();
        $lastLogId = Log::find()->select('id')->orderBy(['id' => SORT_DESC])->asArray()->one();

        // Send mail with fake parameters
        $mailJob = new StationNotificationMailJob();
        $mailJob->stationsIds = [1, 2, 3];
        $mailJob->userId = $lastUser['id'] + 1; // Unexisting user
        $mailJob->notificationType = StationNotificationMailJob::NOTIFICATION_EXPIRED;
        $this->assertTrue($mailJob->sendMailNotification());    // Still returning True

        // But no new log was saved
        $newerLastLogId = Log::find()->select('id')->orderBy(['id' => SORT_DESC])->asArray()->one();
        $this->assertEquals($lastLogId['id'], $newerLastLogId['id']);
    }

    /*****************************************************************
     * Protected help functions to create and delete stations
     *****************************************************************/

    protected function buildWigig(Date $validTo = null, Date $protectedTo = null, string $mac = null)
    {
        $data = json_decode(file_get_contents(__DIR__ . '/../../fixtures/stationWigig.json'), true);

        $data['station']['mac_address'] = array_shift(static::$macPool);
        
        $station = new Station();
        $today = $date = new Date();
        $station->setAttributes($data['station']);

        if (!$validTo) {
            $validTo = $today->modify("+12 months");
        }

        if (!$protectedTo) {
            $protectedTo = $today->modify("+18 months");
        }

        $station->valid_to = $validTo->getTimestamp();
        $station->protected_to = $protectedTo->getTimestamp();

        $station->status = Station::STATUS_FINISHED;
        $this->assertTrue($station->save());

        $wigig = new StationWigig();
        $wigig->setAttributes($data['wigig']);

        $wigig->id_station = $station->id;
        $wigig->save();

        $station->moveToStatus(Station::STATUS_FINISHED);
        return $station;
    }

    protected function deleteWigig(Station $station)
    {
        $wigig = $station->getStationTypeObject();
        $wigig->hardDelete();
        $station->hardDelete();
    }

    protected function buildFs(Date $validTo = null, Date $protectedTo = null, string $macA = null, string $macB = null)
    {
        $data = json_decode(file_get_contents(__DIR__ . '/../../fixtures/stationFs.json'), true);

        $data['a']['mac_address'] = array_shift(static::$macPool);
        $data['b']['mac_address'] = array_shift(static::$macPool);

        $pair = new StationFsPair();
        $pair->initNew();
        $pair->setData([Station::FORM_NAME => [Station::POSITION_A => $data['a'], Station::POSITION_B => $data['b'],]]);
        $today = $date = new Date();

        if (!$validTo) {
            $validTo = $today->modify("+12 months");
        }

        if (!$protectedTo) {
            $protectedTo = $today->modify("+18 months");
        }

        $pair->stationA->valid_to = $validTo->getTimestamp();
        $pair->stationB->valid_to = $validTo->getTimestamp();

        $pair->stationA->protected_to = $protectedTo->getTimestamp();
        $pair->stationB->protected_to = $protectedTo->getTimestamp();


        $pair->stationB->pointerToPairStation = $pair->stationA;
        $pair->stationA->pointerToPairStation = $pair->stationB;
        $pair->save();

        $pair->setFsData([
            StationFs::FORM_NAME => [
                Station::POSITION_A => $data['typeA'],
                Station::POSITION_B => $data['typeB'],
            ],
        ], StationController::STEP_1_NAME);
        $pair->setFsData([
            StationFs::FORM_NAME => [
                Station::POSITION_A => $data['typeA'],
                Station::POSITION_B => $data['typeB'],
            ],
        ], StationController::STEP_3_PARAMS);

        $pair->FsSave();
        $pair->stationA->moveToStatus(Station::STATUS_FINISHED);
        $pair->stationB->moveToStatus(Station::STATUS_FINISHED);
        return $pair;
    }

    protected function deleteFs($pair)
    {
        $pair->stationA->typeStation->hardDelete();
        $pair->stationB->typeStation->hardDelete();
        Yii::$app->db->createCommand('SET FOREIGN_KEY_CHECKS=0')->execute();

        $pair->stationB->hardDelete();
        $pair->stationA->hardDelete();

        Yii::$app->db->createCommand('SET FOREIGN_KEY_CHECKS=1')->execute();
    }
}