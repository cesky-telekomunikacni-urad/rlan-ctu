<?php
/*
* Copyright 2023 DactylGroup s.r.o.
*
* Licensed under the EUPL, Version 1.2 or – as soon they
will be approved by the European Commission - subsequent
versions of the EUPL (the "Licence");
* You may not use this work except in compliance with the
Licence.
* You may obtain a copy of the Licence at:
*
* https://joinup.ec.europa.eu/software/page/eupl5
*
* Unless required by applicable law or agreed to in
writing, software distributed under the Licence is
distributed on an "AS IS" basis,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
express or implied.
* See the Licence for the specific language governing
permissions and limitations under the Licence.
*/
?>


<?php

namespace frontend\controllers;

use dactylcore\core\web\FrontendController;
use dactylcore\system\common\models\File;


class LiveBoxController extends FrontendController
{
    public function actionHelp()
    {
        $videoSlug = null;

        $configUrl = c('HELP_YT_VIDEO');
        if ($configUrl) {
            $videoSlug = $this->youtubeIdFromUrl($configUrl);
        }

        $manualFile = File::findOne(c('HELP_MANUAL_PDF'));

        return $this->renderPartial('help', [
            'videoSlug' => $videoSlug,
            'manualFile' => $manualFile,
        ]);
    }

    /**
     * get youtube video ID from URL
     *
     * @param string $url
     *
     * @return string Youtube video id or FALSE if none found.
     * @authro hakre
     */
    protected function youtubeIdFromUrl($url) : string
    {
        $pattern
            = '%^# Match any youtube URL
        (?:https?://)?  # Optional scheme. Either http or https
        (?:www\.)?      # Optional www subdomain
        (?:             # Group host alternatives
          youtu\.be/    # Either youtu.be,
        | youtube\.com  # or youtube.com
          (?:           # Group path alternatives
            /embed/     # Either /embed/
          | /v/         # or /v/
          | /watch\?v=  # or /watch\?v=
          )             # End path alternatives.
        )               # End host alternatives.
        ([\w-]{10,12})  # Allow 10-12 for 11 char youtube id.
        $%x';
        $result = preg_match($pattern, $url, $matches);
        if (false !== $result) {
            return $matches[1];
        }
        return '';
    }
}
