<?php
/*
* Copyright 2023 DactylGroup s.r.o.
*
* Licensed under the EUPL, Version 1.2 or – as soon they
will be approved by the European Commission - subsequent
versions of the EUPL (the "Licence");
* You may not use this work except in compliance with the
Licence.
* You may obtain a copy of the Licence at:
*
* https://joinup.ec.europa.eu/software/page/eupl5
*
* Unless required by applicable law or agreed to in
writing, software distributed under the Licence is
distributed on an "AS IS" basis,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
express or implied.
* See the Licence for the specific language governing
permissions and limitations under the Licence.
*/
?>


<?php


namespace frontend\controllers;


use common\models\Message;
use common\models\search\ThreadSearch;
use common\models\Station;
use common\models\Thread;
use console\tasks\MessageNotificationJob;
use dactylcore\core\web\FrontendController;
use Yii;
use yii\bootstrap\Html;
use yii\filters\AccessControl;

class MessagingController extends FrontendController
{


    public function behaviors()
    {
        $protectedOperations = [
            'generic-question',
            'disturbance-question',
            'index',
        ];
        return \yii\helpers\ArrayHelper::merge(parent::behaviors(), [
            'access' => [
                'class' => AccessControl::class,
                'rules' => [
                    [
                        'allow' => true,
                        'actions' => $protectedOperations,
                        'roles' => ['@'],
                    ],
                    [
                        'allow' => true,
                        'actions' => ['thread'],
                        'matchCallback' => function ($rule, $action) use ($protectedOperations) {
                            $id = Station::deFormatId(\Yii::$app->request->get('id'));
                            $userId = \Yii::$app->getUser()->id;

                            $thread = Thread::find()->where([
                                'and',
                                ['id' => $id],
                                ['or',
                                    ['id_recipient' => $userId],
                                    ['id_sender' => $userId,]
                                ]
                            ]);
                            $c = $thread->count();
                            return $c > 0;
                        },
                    ],

                    'default' => [
                        'allow' => false,
                    ],
                ],
            ],
        ]);
    }

    // generic call
    public function actionGenericQuestion($id)
    {
        if (!Yii::$app->request->isAjax) {
            $urlOptions = ['messaging/generic-question', 'id' => $id];

            $this->view->showSubHeader = true;
            $this->view->subHeaderContent = $this->renderPartial('../site/partial/sub_header', ['showMyOnly' => true]);
            return $this->render('../site/index', [
                'liveboxUrl' => url($urlOptions),
            ]);
        }

        $thread = $this->getThreadForStation($id);

        if (!$thread->isNewRecord) {
            return $this->actionThread($thread->id);
        }
        elseif ($thread->validate() && $thread->save()) {
            $model = $this->getMessageModel($thread);

            return $this->render('thread', [
                'thread' => $thread,
                'messageModel' => $model,
                'userId' => \Yii::$app->getUser()->id,
                'ajaxUrl' => url(['messaging/thread', 'id' => $thread->id, 'isSubmit' => true]),
            ]);
        }

        setErrorFlash(_tF('something went wrong', 'chat'));
        return $this->actionIndex();
    }


    public function actionIndex($threadId = null)
    {
        $filter = \Yii::$app->request->queryParams;

        $this->layout = 'single-column';

        $searchModel = new ThreadSearch();
        $dataProvider = $searchModel->searchUsersThreads($filter, \Yii::$app->getUser()->id);

        if ($threadId != null) {
            $liveboxUrl = url(['messaging/thread', 'id' => $threadId]);
            return $this->render
            ('threads', [
                'searchModel' => $searchModel,
                'dataProvider' => $dataProvider,
                'liveboxUrl' => $liveboxUrl,
            ]);
        }

        return $this->render
        ('threads', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * @param $stationId - Station id
     * @param null $stationAggressorId
     * @return Thread
     */
    public function getThreadForStation($stationId, $stationAggressorId = null): Thread
    {
        $reason = Thread::REASON_GENERIC;
        $userId = \Yii::$app->getUser()->id;
        $station = Station::findOne(['id' => $stationId]);

        $recipientId = $station->id_user;

        $params = ['id_station' => $stationId, 'id_sender' => $userId];
        if ($stationAggressorId) {
            $stationAggressor = Station::findOne(['id' => $stationAggressorId]);
            $recipientId = $stationAggressor->id_user;

            $reason = Thread::REASON_CONFLICT;
            $params['id_station_disturber'] = $stationAggressorId;
        }
        $thread = Thread::findOne($params);

        if (!$thread) {
            $thread = new Thread();
            $thread->id_station = $stationId;
            $thread->id_station_disturber = $stationAggressorId;
            $thread->id_sender = $userId;
            $thread->id_recipient = $recipientId;
            $thread->status_sender = Thread::STATUS_READ;
            $thread->reason = $reason;
            $thread->status_recipient = Thread::STATUS_NEW;
            if ($stationAggressorId) {
                $stationAggressor = Station::findOne(['id' => $stationAggressorId]);
                $thread->subject = _tF('message on disturbing {stationName} by {stationAggressorName}', 'messaging',
                    [
                        'stationName' => $station->getTypeName(),
                        'stationAggressorName' => $stationAggressor->getTypeName(),
                    ]
                );
            }
        }

        return $thread;
    }

    public function getThread($threadId)
    {
        $userId = \Yii::$app->getUser()->id;
        return Thread::findOne(['id' => $threadId, ['or', 'id_sender' => $userId, 'id_recipient' => $userId,]]);
    }

    public function getMessageModel(Thread $thread)
    {
        $message = new Message();
        $message->id_thread = $thread->id;
        $message->id_author = \Yii::$app->getUser()->id;

        return $message;
    }

    public function actionThread($id, $isSubmit = false)
    {

        $thread = $this->getThread($id);
        $thread->setAsRead();

        $msgModel = $this->getMessageModel($thread);

        if (!\Yii::$app->request->isAjax) {
            return $this->actionIndex($id);
        }

        $allRead = false;

        if (Thread::findUnreadThreads() == 0) {
            $allRead = true;
        }

        if (\Yii::$app->request->isPost || $isSubmit) {
            $data = \Yii::$app->request->post();

            if (isset($data['Thread']['show_sender_info'])) {
                $data['Thread']['show_sender_info'] = $data['Thread']['show_sender_info'] == "true";
            }
            if (isset($data['Thread']['show_recipient_info'])) {
                $data['Thread']['show_recipient_info'] = $data['Thread']['show_recipient_info'] == "true";
            }

            $msgModel->load($data);
            $thread->load($data);

            if (!$thread->subject) {
                $thread->subject = _tF('message on station {stationName}', 'messaging',
                    ['stationName' => $thread->station->getTypeName()]);
            }
            if ($msgModel->validate() &&
                $msgModel->save() &&
                $thread->setAsNew($thread->getOppositeRole())
                &&
                MessageNotificationJob::pushEmailJob($msgModel->id, "RE: {$thread->subject}", \Yii::$app->getUser()->id)
            ) {
                // response
                setSuccessFlash(_tF('message has been send', 'messaging'));
            } else {
                setErrorFlash(_tF('error while sending message', 'messaging'));
            }

            $thread = $this->getThread($id);

            return $this->renderPartial('partial/thread_messages', [
                'thread' => $thread,
                'messageModel' => $msgModel,
                'userId' => \Yii::$app->getUser()->id,
            ]);
        }

        return $this->render('thread', [
            'thread' => $thread,
            'messageModel' => $msgModel,
            'userId' => \Yii::$app->getUser()->id,
            'ajaxUrl' => url(['messaging/thread', 'id' => $id, 'isSubmit' => true]),
            'allRead' => $allRead,
        ]);
    }

    protected function generateDefaultMessage($victimStation, $aggressorStation)
    {
        $agUrl = url(['station/station', 'id' => $aggressorStation->id, 'step' => 3], true,);
        $vicUrl = url(['station/station', 'id' => $victimStation->id, 'step' => 3], true,);
        $url = url(['/'], true);
        $aggressorStation = Html::a($aggressorStation->getName(), $agUrl, ['data-mce-href' => $agUrl]);
        $victimStation = Html::a($victimStation->getName(), $vicUrl, ['data-mce-href' => $vicUrl]);
        $urlElement = Html::a(_tF('portalName', 'messaging'), $url, ['data-mce-href' => $url]);
        return _tF('default contact message from {victimStation} owner to {aggressorStation} owner. login to {url}', 'messaging',
            ['victimStation' => $victimStation, 'aggressorStation' => $aggressorStation, 'url' => $urlElement]);
    }

    public function actionDisturbanceQuestion($idAggressor, $idVictim)
    {
        if (!Yii::$app->request->isAjax) {
            $urlOptions = ['messaging/disturbance-question', 'idAggressor' => $idAggressor, 'idVictim' => $idVictim];

            $this->view->showSubHeader = true;
            $this->view->subHeaderContent = $this->renderPartial('../site/partial/sub_header', ['showMyOnly' => true]);
            return $this->render('../site/index', [
                'liveboxUrl' => url($urlOptions),
            ]);
        }

        $victimStation = Station::findOne(['id' => $idVictim]);
        $aggressorStation = Station::findOne(['id' => $idAggressor]);
        $thread = $this->getThreadForStation($idVictim, $idAggressor);

        if (!$thread->isNewRecord) {
            return $this->actionThread($thread->id);
        }
        elseif ($thread->validate() && $thread->save()) {
            $model = $this->getMessageModel($thread);
            $defaultMsg = $this->generateDefaultMessage($victimStation, $aggressorStation);

            $model->content = $defaultMsg;

            return $this->render('thread', [
                'thread' => $thread,
                'messageModel' => $model,
                'userId' => \Yii::$app->getUser()->id,
                'ajaxUrl' => url(['messaging/thread', 'id' => $thread->id, 'isSubmit' => true]),
            ]);
        }

        setErrorFlash(_tF('something went wrong', 'chat'));
        return $this->actionIndex();
    }
}