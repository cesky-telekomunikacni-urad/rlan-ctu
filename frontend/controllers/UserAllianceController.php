<?php
/*
* Copyright 2023 DactylGroup s.r.o.
*
* Licensed under the EUPL, Version 1.2 or – as soon they
will be approved by the European Commission - subsequent
versions of the EUPL (the "Licence");
* You may not use this work except in compliance with the
Licence.
* You may obtain a copy of the Licence at:
*
* https://joinup.ec.europa.eu/software/page/eupl5
*
* Unless required by applicable law or agreed to in
writing, software distributed under the Licence is
distributed on an "AS IS" basis,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
express or implied.
* See the Licence for the specific language governing
permissions and limitations under the Licence.
*/
?>


<?php

namespace frontend\controllers;

use common\models\UserAlliance;
use dactylcore\core\db\ActiveRecord;
use dactylcore\log\LogManager;
use dactylcore\log\LogOperation;
use modules\user\common\models\User;
use Yii;
use yii\db\Exception;
use dactylcore\core\web\FrontendController;
use yii\filters\AccessControl;
use yii\helpers\ArrayHelper;

/**
 * UserAllianceController implements the CRUD actions for UserAlliance model. */
class UserAllianceController extends FrontendController
{
    public function behaviors()
    {
        return ArrayHelper::merge(parent::behaviors(), [
            'access' => [
                'class' => AccessControl::class,
                'rules' => [
                    [
                        'allow' => true,
                        'actions' =>
                            [
                                'index',
                                'create',
                                'invite-user',
                                'confirm-invitation',
                                'change-alliance-name',
                                'change-alliance-description',
                                'promote-member',
                                'degrade-member',
                                'exclude-member',
                                'delete',
                                'resign',
                            ],
                        'roles' => ['@'],
                    ],
                    [
                        'allow' => true,
                        'actions' =>
                            [
                                'confirm-invitation',
                            ],
                        'roles' => ['?'],
                    ],
                ],
            ],
        ]);
    }

    public function actionIndex($idUser = 0)
    {
        // Return to profile, if is not Ajax
        if (!Yii::$app->request->isAjax) {
            return $this->redirect(['/dc-user/user/index']);
        }

        $user = ($idUser) ? User::findOne($idUser) : Yii::$app->user->identity;

        if (!$user) {
            return $this->redirect(['/dc-user/user/index']);
        }

        $allianceModel = $user->alliance;

        if ($allianceModel) {
            return $this->render('index', [
                'userId' => $user->getId(),
                'model' => $allianceModel,
                'isAdmin' => $allianceModel->isUserAdmin($user->getId()),
                'byAdmin' => ($idUser !== 0),
                'dataProvider' => $allianceModel->getAllStations(),
            ]);
        }

        return $this->render('no-alliance-index', [
            'userId' => $user->getId(),
            'byAdmin' => ($idUser !== 0),
        ]);
    }

    public function actionCreate($idUser = 0)
    {
        $byAdmin = ($idUser !== 0);
        $errorView = ($byAdmin) ? ['/dc-user/user/user-index', 'id' => $idUser] : ['/dc-user/user/index'];

        // Return to profile, if is not Ajax
        if (!Yii::$app->request->isAjax && !Yii::$app->request->isPjax) {
            return $this->redirect($errorView);
        }

        $user = ($idUser) ? User::findOne($idUser) : Yii::$app->user->identity;
        if (!$user || $user->isInAlliance()) {
            setErrorFlash(_tF('cannot_create_alliance', 'user-alliance'));
            return $this->redirect($errorView);
        }

        // New Alliance for this user
        $allianceModel = new UserAlliance();

        if ($allianceModel->load(Yii::$app->request->post())) {
            // Set creator as admin
            if ($allianceModel->validate() &&
                $allianceModel->save() &&
                $allianceModel->addUserAsAdmin($user->getId())) {
                setSuccessFlash(_tF('flash_successfully_created', 'user-alliance'));

                return $this->render('index', [
                    'userId' => $user->getId(),
                    'model' => $allianceModel,
                    'isAdmin' => $allianceModel->isUserAdmin($user->getId()),
                    'byAdmin' => $byAdmin,
                    'dataProvider' => $allianceModel->getAllStations(),
                ]);
            } else {
                $allianceModel->addError('name', _tF('cannot_create_alliance', 'user-alliance'));
            }
        }

        return $this->render('create', [
            'model' => $allianceModel,
            'byAdmin' => $byAdmin,
            'userId' => $user->getId(),
        ]);
    }

    /**
     * Send invitation to alliance to one user
     *
     * @param $inviteId
     *
     * @return bool|\yii\web\Response
     */
    public function actionInviteUser($inviteId, $idUser = 0)
    {
        $errorView = ($idUser !== 0) ? ['/dc-user/user/user-index', 'id' => $idUser] : ['/dc-user/user/index'];

        if (!Yii::$app->request->isAjax && !Yii::$app->request->isPjax) {
            return $this->redirect($errorView);
        }

        $user = ($idUser) ? User::findOne($idUser) : Yii::$app->user->identity;
        if (!$user || !$user->alliance) {
            setErrorFlash(_tF('cannot_send_invitation', 'user-alliance'));
            return $this->redirect($errorView);
        }

        if ($user->alliance->sendInvitationTo($inviteId)) {
            setSuccessFlash(_tF('invitation_sent', 'user-alliance'));
            return true;
        } else {
            setErrorFlash(_tF('cannot_send_invitation', 'user-alliance'));
        }
        return false;
    }

    /**
     * Verify user's email.
     *
     * @param string $code verification token
     *
     * @return \yii\web\Response
     */
    public function actionConfirmInvitation(string $code)
    {
        $userId = UserAlliance::getUserIdByInvitation($code);
        if ($userId === null) {
            setErrorFlash(_tF('verification_unknown_invitation', 'user-alliance'));
            return $this->redirect(['/dc-user/user/index']);
        }

        $user = User::findOne($userId);
        if ($user) {
            if (!$user->isInAlliance()) {
                if (UserAlliance::acceptInvitation($code)) {
                    //Log action
                    $logManager = LogManager::getInstance();
                    $log = (new LogOperation());
                    $log->operationType = ActiveRecord::OPERATION_TYPE_EVENT;
                    $log->dataChanged = true;
                    $log->modelId = $user->getName();
                    $log->operations = [
                        'accepting_invitation' => [
                            'invitation into alliance {name} was accepted by {email}',
                            [
                                'name' => $user->alliance->name,
                                'email' => $user->email,
                            ],
                        ],
                    ];
                    $logManager->registerOperation($log);

                    setSuccessFlash(_tF('verification_success_accept', 'user-alliance'));
                } else {
                    setErrorFlash(_tF('verification_error_accept', 'user-alliance'));
                }
            } else {
                setErrorFlash(_tF('verification_user_already_member', 'user-alliance'));
            }
        } else {
            setErrorFlash(_tF('verification_unknown_user', 'user-alliance'));
        }

        return $this->redirect(['/dc-user/user/index']);
    }

    /**
     * @param $idAlliance
     * @param $newName
     *
     * @return false|string
     */
    public function actionChangeAllianceName($newName, $idUser = 0)
    {
        $byAdmin = ($idUser !== 0);
        $errorView = ($byAdmin) ? ['/dc-user/user/user-index', 'id' => $idUser] : ['/dc-user/user/index'];

        // Return to profile, if is not Ajax
        if (!Yii::$app->request->isAjax) {
            return $this->redirect($errorView);
        }

        $user = ($idUser) ? User::findOne($idUser) : Yii::$app->user->identity;
        if (!$user) {
            setErrorFlash(_tF('cannot_change_name', 'user-alliance'));
            return $this->redirect($errorView);
        }

        if ($user->alliance) {
            $user->alliance->name = $newName;
            if ($user->alliance->validate() && $user->alliance->save()) {
                setSuccessFlash(_tF('success_change_name', 'user-alliance'));
                return $this->render('index', [
                    'userId' => $user->getId(),
                    'model' => $user->alliance,
                    'isAdmin' => $user->alliance->isUserAdmin($user->getId()),
                    'byAdmin' => $byAdmin,
                    'dataProvider' => $user->alliance->getAllStations(),
                ]);
            }
        }

        setErrorFlash(_tF('cannot_change_name', 'user-alliance'));
        return false;
    }

    /**
     * New description from post
     *
     * @param $idAlliance
     *
     * @return false|string
     */
    public function actionChangeAllianceDescription($idUser = 0)
    {
        $byAdmin = ($idUser !== 0);
        $errorView = ($byAdmin) ? ['/dc-user/user/user-index', 'id' => $idUser] : ['/dc-user/user/index'];

        // Return to profile, if is not Ajax
        if (!Yii::$app->request->isAjax) {
            return $this->redirect($errorView);
        }

        $user = ($idUser) ? User::findOne($idUser) : Yii::$app->user->identity;
        $newDescription = Yii::$app->request->post('newDescription');
        if (!$user || $newDescription === null) {
            setErrorFlash(_tF('cannot_change_description', 'user-alliance'));
            return $this->redirect($errorView);
        }

        if ($user->alliance) {
            $user->alliance->description = $newDescription;
            if ($user->alliance->validate() && $user->alliance->save()) {
                setSuccessFlash(_tF('success_change_description', 'user-alliance'));
                return $this->render('index', [
                    'userId' => $user->getId(),
                    'model' => $user->alliance,
                    'isAdmin' => $user->alliance->isUserAdmin($user->getId()),
                    'byAdmin' => $byAdmin,
                    'dataProvider' => $user->alliance->getAllStations(),
                ]);
            }
        }

        setErrorFlash(_tF('cannot_change_description', 'user-alliance'));
        return false;
    }

    /**
     * From Member to Admin
     *
     * @param $idAlliance
     * @param $idUser
     *
     * @return false|string|\yii\web\Response
     * @throws Exception
     */
    public function actionPromoteMember($idUser, $idAdmin = 0)
    {
        $byAdmin = ($idAdmin !== 0);
        $errorView = ($byAdmin) ? ['/dc-user/user/user-index', 'id' => $idAdmin] : ['/dc-user/user/index'];

        // Return to profile, if is not Ajax
        if (!Yii::$app->request->isAjax) {
            return $this->redirect($errorView);
        }

        $user = ($idAdmin) ? User::findOne($idAdmin) : Yii::$app->user->identity;
        if (!$user) {
            setErrorFlash(_tF('cannot_promote_user', 'user-alliance'));
            return $this->redirect($errorView);
        }

        if ($user->alliance && $user->alliance->isUserAdmin($user->getId())
            && $user->alliance->isUserMember($idUser) && $user->alliance->promoteMember($idUser)) {
            setSuccessFlash(_tF('success_promote_user', 'user-alliance'));
        } else {
            setErrorFlash(_tF('cannot_promote_user', 'user-alliance'));
        }

        return $this->render('index', [
            'userId' => $user->getId(),
            'model' => $user->alliance,
            'isAdmin' => $user->alliance->isUserAdmin($user->getId()),
            'byAdmin' => $byAdmin,
            'dataProvider' => $user->alliance->getAllStations(),
        ]);
    }

    /**
     * From Admin to Member
     *
     * @param $idUser
     *
     * @return false|string|\yii\web\Response
     * @throws Exception
     */
    public function actionDegradeMember($idUser, $idAdmin = 0)
    {
        $byAdmin = ($idAdmin !== 0);
        $errorView = ($byAdmin) ? ['/dc-user/user/user-index', 'id' => $idAdmin] : ['/dc-user/user/index'];

        // Return to profile, if is not Ajax
        if (!Yii::$app->request->isAjax) {
            return $this->redirect($errorView);
        }

        $user = ($idAdmin) ? User::findOne($idAdmin) : Yii::$app->user->identity;
        if (!$user) {
            setErrorFlash(_tF('cannot_degrade_user', 'user-alliance'));
            return $this->redirect($errorView);
        }

        if ($user->alliance && $user->alliance->isUserAdmin($user->getId())
            && $user->alliance->isUserAdmin($idUser) && $user->alliance->degradeMember($idUser)) {

            //Log action
            $logManager = LogManager::getInstance();
            $log = (new LogOperation());
            $log->operationType = ActiveRecord::OPERATION_TYPE_EVENT;
            $log->dataChanged = true;
            $log->modelId = $user->getName();
            $log->operations = [
                'promoting_member' => [
                    'member {email} degraded in {name} by {admin}',
                    [
                        'name' => $user->alliance->name,
                        'email' => $idUser,
                        'admin' => $user->email,
                    ],
                ],
            ];
            $logManager->registerOperation($log);
            $logManager->saveOperations(null);

            setSuccessFlash(_tF('success_degrade_user', 'user-alliance'));
        } else {
            setErrorFlash(_tF('cannot_degrade_user', 'user-alliance'));
        }

        return $this->render('index', [
            'userId' => $user->getId(),
            'model' => $user->alliance,
            'isAdmin' => $user->alliance->isUserAdmin($user->getId()),
            'byAdmin' => $byAdmin,
            'dataProvider' => $user->alliance->getAllStations(),
        ]);
    }

    /**
     * @param $idUser
     *
     * @return false|string|\yii\web\Response
     */
    public function actionExcludeMember($idUser, $idAdmin = 0)
    {
        $byAdmin = ($idAdmin !== 0);
        $errorView = ($byAdmin) ? ['/dc-user/user/user-index', 'id' => $idAdmin] : ['/dc-user/user/index'];

        // Return to profile, if is not Ajax
        if (!Yii::$app->request->isAjax) {
            return $this->redirect($errorView);
        }

        $user = ($idAdmin) ? User::findOne($idAdmin) : Yii::$app->user->identity;
        if (!$user) {
            setErrorFlash(_tF('cannot_exclude_user', 'user-alliance'));
            return $this->redirect($errorView);
        }

        if ($user->alliance && $user->alliance->isUserAdmin($user->getId())
            && $user->alliance->isUserMember($idUser) && $user->alliance->excludeMember($idUser)) {
            //Log action
            $logManager = LogManager::getInstance();
            $log = (new LogOperation());
            $log->operationType = ActiveRecord::OPERATION_TYPE_EVENT;
            $log->dataChanged = true;
            $log->modelId = $user->getName();
            $log->operations = [
                'excluding_member' => [
                    'member {email} excluded from {name} by {admin}',
                    [
                        'name' => $user->alliance->name,
                        'email' => $idUser,
                        'admin' => $user->email,
                    ],
                ],
            ];
            $logManager->registerOperation($log);
            $logManager->saveOperations(null);

            setSuccessFlash(_tF('success_exclude_user', 'user-alliance'));
        } else {
            setErrorFlash(_tF('cannot_exclude_user', 'user-alliance'));
        }

        return $this->render('index', [
            'userId' => $user->getId(),
            'model' => $user->alliance,
            'isAdmin' => $user->alliance->isUserAdmin($user->getId()),
            'byAdmin' => $byAdmin,
            'dataProvider' => $user->alliance->getAllStations(),
        ]);
    }

    /**
     * @return false|\yii\web\Response
     * @throws Exception
     */
    public function actionDelete($idUser = 0)
    {
        $byAdmin = ($idUser !== 0);
        $errorView = ($byAdmin) ? ['/dc-user/user/user-index', 'id' => $idUser] : ['/dc-user/user/index'];

        $user = ($idUser) ? User::findOne($idUser) : Yii::$app->user->identity;
        if (!$user) {
            setErrorFlash(_tF('cannot_delete_alliance', 'user-alliance'));
            return $this->redirect($errorView);
        }

        /** @var UserAlliance $allianceModel */
        $allianceModel = $user->alliance;
        if ($allianceModel && $allianceModel->isUserAdmin($user->id) && $allianceModel->delete()) {
            setSuccessFlash(_tF('success_delete_allaince', 'user-alliance'));
        } else {
            setErrorFlash(_tF('cannot_delete_alliance', 'user-alliance'));
        }

        return $this->redirect($errorView);
    }

    public function actionResign($idUser = 0)
    {
        $byAdmin = ($idUser !== 0);
        $errorView = ($byAdmin) ? ['/dc-user/user/user-index', 'id' => $idUser] : ['/dc-user/user/index'];

        $user = ($idUser) ? User::findOne($idUser) : Yii::$app->user->identity;
        if (!$user) {
            setErrorFlash(_tF('cannot_resign_user', 'user-alliance'));
            return $this->redirect($errorView);
        }

        /** @var UserAlliance $allianceModel */
        $allianceModel = $user->alliance;
        if ($allianceModel && $allianceModel->resignMember($user->id)) {

            //Log action
            $logManager = LogManager::getInstance();
            $log = (new LogOperation());
            $log->operationType = ActiveRecord::OPERATION_TYPE_EVENT;
            $log->dataChanged = true;
            $log->modelId = $user->getName();
            $log->operations = [
                'excluding_member' => [
                    'member {email} resign from {name}',
                    [
                        'name' => $user->alliance->name,
                        'email' => $user->email,
                    ],
                ],
            ];
            $logManager->registerOperation($log);
            $logManager->saveOperations(null);

            setSuccessFlash(_tF('success_resign_user', 'user-alliance'));
        } else {
            setErrorFlash(_tF('cannot_resign_user', 'user-alliance'));
        }

        return $this->redirect(['/dc-user/user/index']);
    }
}
