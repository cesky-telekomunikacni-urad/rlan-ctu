<?php
/*
* Copyright 2023 DactylGroup s.r.o.
*
* Licensed under the EUPL, Version 1.2 or – as soon they
will be approved by the European Commission - subsequent
versions of the EUPL (the "Licence");
* You may not use this work except in compliance with the
Licence.
* You may obtain a copy of the Licence at:
*
* https://joinup.ec.europa.eu/software/page/eupl5
*
* Unless required by applicable law or agreed to in
writing, software distributed under the Licence is
distributed on an "AS IS" basis,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
express or implied.
* See the Licence for the specific language governing
permissions and limitations under the Licence.
*/
?>


<?php

namespace frontend\controllers;

use common\models\search\StationSearch;
use common\models\Station;
use common\models\Station52;
use common\models\Station58;
use common\models\StationFs;
use common\models\StationWigig;
use common\models\Thread;
use common\models\UserAlliance;
use dactylcore\core\db\ActiveRecord;
use dactylcore\log\LogManager;
use dactylcore\log\LogOperation;
use DateTime;
use frontend\widgets\FrontendController;
use modules\user\common\models\User;
use Yii;
use yii\data\ArrayDataProvider;
use yii\filters\AccessControl;
use yii\web\BadRequestHttpException;
use yii\web\ForbiddenHttpException;
use yii\web\NotFoundHttpException;
use yii\web\Response;

class StationController extends FrontendController
{
    public const STEP_1_NAME = 1;
    public const STEP_2_LOCATION = 2;
    public const STEP_3_PARAMS = 3;
    public const STEP_4_SUMMARY = 4;
    public const SCENARIO_DELETE = 99;
    public const SCENARIO_UNPUBLISH = 'unpublish';
    public const TYPE_WIGIG = 'wigig';
    public const TYPE_FS = 'fs';
    public const PARTNER_SESSION_KEY = 'partnerCheck';
    public const TYPE_52 = '52ghz';
    public const TYPE_58_AP = 'ap_58ghz';
    public const TYPE_58_TG = 'tg_58ghz';
    public static $viewsFs
        = [
            self::STEP_1_NAME => 'step1_name',
            self::STEP_2_LOCATION => 'step2_location',
            self::STEP_3_PARAMS => 'step3_parameters',
            self::STEP_4_SUMMARY => 'step4_summary',
        ];
    public static $viewsWigig
        = [
            self::STEP_1_NAME => 'step1_name',
            self::STEP_2_LOCATION => 'step2_location',
            self::STEP_3_PARAMS => 'step3_parameters',
            self::STEP_4_SUMMARY => 'step4_summary',
        ];
    public static $views58
        = [
            self::STEP_1_NAME => 'step1_info',
            self::STEP_2_LOCATION => 'step2_location',
            self::STEP_3_PARAMS => 'step3_parameters',
            self::STEP_4_SUMMARY => 'step4_summary',
        ];
    public static $views52
        = [
            self::STEP_1_NAME => 'step1_name',
            self::STEP_2_LOCATION => 'step2_location',
            self::STEP_3_PARAMS => 'step3_parameters',
            self::STEP_4_SUMMARY => 'step4_summary',
        ];
    protected $myStationsOnly = false;
    protected $type = self::TYPE_WIGIG;
    protected $canEdit;
    protected $model;
    protected $modelStation;
    protected $modelTypeStation;

    public function behaviors()
    {
        $protectedOperations = [
//            'update',
            'delete',
            'publish',
            'prolong-registration',
            'declaration',
            'declare',
            'contact-to-conflict-users',
        ];
        return \yii\helpers\ArrayHelper::merge(parent::behaviors(), [
            'access' => [
                'class' => AccessControl::class,
                'rules' => [
                    [
                        'allow' => false,
                        'actions' => $protectedOperations,
                        'matchCallback' => function ($rule, $action) use ($protectedOperations)
                        {
                            $id = Station::deFormatId(Yii::$app->request->get('id'));
                            return in_array($action->id, $protectedOperations) && !$this->canEdit($id);
                        },
                    ],
                    [
                        'allow' => true,
                        'actions' =>
                            [
                                'create-fs',
                                'create-wigig',
                                'wigig-pair-dropdown-data',
                            ],
                        'roles' => ['@'],
                    ],
                    [
                        'allow' => false,
                        'actions' =>
                            [
                                'create-fs',
                                'create-wigig',
                                'wigig-pair-dropdown-data',
                            ],
                        'roles' => ['?'],
                    ],

                    'default' => [
                        'allow' => true,
                    ],
                ],
            ],
        ]);
    }

    public function actionMyStations()
    {
        $this->myStationsOnly = true;
        return $this->actionStations();
    }

    protected function checkUnpublishedLimit()
    {
        return (StationSearch::countUnpublishedStations(Yii::$app->getUser()->id) >= c('MAX_WAITING_STATIONS'));
    }

    public function actionCreateFs()
    {
        if ($this->checkUnpublishedLimit()) {
            throw new ForbiddenHttpException();
        }
        return $this->actionStationFs(self::STEP_1_NAME);
    }

    public function actionCreateWigig()
    {
        if ($this->checkUnpublishedLimit()) {
            throw new ForbiddenHttpException();
        }
        return $this->actionStationWigig(self::STEP_1_NAME);
    }

    public function actionCreateAccessPoint()
    {
        if ($this->checkUnpublishedLimit()) {
            throw new ForbiddenHttpException();
        }
        return $this->actionStationAccessPoint(self::STEP_1_NAME);
    }

    public function actionCreateTollGate()
    {
        if ($this->checkUnpublishedLimit()) {
            throw new ForbiddenHttpException();
        }
        return $this->actionStationTollGate(self::STEP_1_NAME);
    }

    public function actionCreateStation52Ghz()
    {
        if ($this->checkUnpublishedLimit()) {
            throw new ForbiddenHttpException();
        }
        return $this->actionStation52(self::STEP_1_NAME);
    }

    protected function getStationTypeAction($modelStation, $step, $checkAjax = true)
    {

        switch ($modelStation->type) {
            case self::TYPE_FS:
                return $this->actionStationFs($step, $modelStation->id, $checkAjax);
            case self::TYPE_WIGIG:
                return $this->actionStationWigig($step, $modelStation->id, $checkAjax);
            case self::TYPE_58_AP:
                return $this->actionStationAccessPoint($step, $modelStation->id, $checkAjax);
            case self::TYPE_58_TG:
                return $this->actionStationTollGate($step, $modelStation->id, $checkAjax);
            case self::TYPE_52:
                return $this->actionStation52($step, $modelStation->id, $checkAjax);
            default:
                throw new Exception(_tF('Invalid station type', 'station'));
        }
    }

    public function actionEdit($id = null)
    {
        if (!userIsLogged()) {
            return $this->redirect(['dc-user/user/login']);
        }

        $modelStation = $this->getModel($id);

        if ($modelStation == null) {
            throw new NotFoundHttpException();
        }
        if (!$this->canEdit($id)) {
            throw new ForbiddenHttpException();
        }
        if ($modelStation->status == Station::STATUS_UNPUBLISHED) {
            $modelStation->moveToStatus(Station::STATUS_DRAFT);
        }

        $step = $modelStation->loadStationProgress();

        return $this->getStationTypeAction($modelStation, $step);
    }

    public function actionStepBack($id)
    {
        $modelStation = $this->getModel($id);

        if ($modelStation == null) {
            throw new NotFoundHttpException();
        }

        $step = $modelStation->loadStationProgress();
        if ($step > 1) {
            $step -= 1;
            $modelStation->storeStationProgress($step);
        }

        return $this->getStationTypeAction($modelStation, $step, false);
    }

    public function actionStation($id = null, $editFinished = false)
    {
        // TODO subheader content ?
        $this->view->showSubHeader = true;
        $this->view->subHeaderContent = $this->renderPartial('detail/partial/sub_header');

        $id = Station::deFormatId($id);
        $modelStation = $this->getModel($id);
        $this->type = $modelStation->type;

        if ($this->canSee($id)) {

            if ($modelStation->status == Station::STATUS_WAITING ||
                $modelStation->status == Station::STATUS_DRAFT ||
                $modelStation->status == Station::STATUS_UNPUBLISHED ||
                $editFinished) {
                return $this->actionEdit($id);
            }

            $bData = [];
            $id = Station::deFormatId($id);

            $dataModel = null;
            /**
             * @var $modelStation Station
             */
            $modelStation = null;

            switch ($this->type) {
                case self::TYPE_FS:
                    $dataModel = StationSearch::getModelsFs($id);
                    $modelStation = $dataModel->stationA;
                    break;
                case self::TYPE_WIGIG:
                case self::TYPE_58_AP:
                case self::TYPE_58_TG:
                case self::TYPE_52:
                    $dataModel = $this->getModel($id);
                    $modelStation = $dataModel;
                    break;
                default:
                    break;
            }

            $searchModel = new StationSearch();
            $filter = Yii::$app->request->queryParams;

            switch ($modelStation->type) {
                case self::TYPE_FS:
                case self::TYPE_WIGIG:
                    $data = $searchModel->searchCloseStations($modelStation);
                    break;
                case self::TYPE_58_TG:
                case self::TYPE_58_AP:
                    $data = $searchModel->searchTollGateVicinity($modelStation);
                    break;
                default:
                    $data = [];
            }


            if ($modelStation->isFs()) {
                $bModel = $modelStation->getPairStation();
                $bData = $searchModel->searchCloseStations($bModel);
            }

            // sorting by conflict ones first
            uasort($data, function (Station $a, Station $b) use ($bData, $modelStation)
            {

                /*
                 * 1 => up
                 * 0 => keep
                 * -1 => down
                 */
                $aHasConflict = ($a->hasConflicts || (isset($bData[$a->id]) && $bData[$a->id]->hasConflicts));
                $bHasConflict = ($b->hasConflicts || (isset($bData[$b->id]) && $bData[$b->id]->hasConflicts));

                $aShowInterference = $a->showInterference($modelStation->type, $a->type, 'general');
                $bShowInterference = $b->showInterference($modelStation->type, $b->type, 'general');

                if ($bHasConflict && !$aHasConflict) {
                    return 1;
                } elseif (!$bHasConflict && $aHasConflict) {
                    return -1;
                } elseif ($bHasConflict && $aHasConflict) {
                    return 0;
                } else {
                    if ($bShowInterference && !$aShowInterference) {
                        return 1;
                    } elseif (!$bShowInterference && $aShowInterference) {
                        return -1;
                    } else {
                        return 0;
                    }
                }
            });

            if (array_key_exists('StationSearch', $filter) &&
                array_key_exists('hasConflicts', $filter['StationSearch'])) {
                if ($filter['StationSearch']['hasConflicts'] == 'yes') {
                    $data = array_filter($data, function ($model) use ($bData)
                    {
                        return $model->hasConflicts || (isset($bData[$model->id]) && $bData[$model->id]->hasConflicts);
                    });
                } elseif ($filter['StationSearch']['hasConflicts'] == 'no') {
                    // no
                    $data = array_filter($data, function ($model) use ($bData)
                    {
                        return !$model->hasConflicts &&
                            !(isset($bData[$model->id]) && $bData[$model->id]->hasConflicts);
                    });
                }
            }

            $dataProvider = new ArrayDataProvider([
                'allModels' => $data,
                'pagination' => ['pageSize' => 100],
                'sort' => [
                    'attributes' => ['id'],
                ],
            ]);

            $startedConversationsWith = array_keys(Thread::find()
                                                         ->where(['id_station' => $modelStation->id])
                                                         ->indexBy('id_station_disturber')
                                                         ->all());

            switch ($modelStation->type) {
                case self::TYPE_FS:
                    return $this->render('detail/fs', [
                        'modelStation' => $modelStation,
                        'modelStationType' => $dataModel,
                        'canEdit' => $this->canEdit($id),
                        'dataProvider' => $dataProvider,
                        'searchModel' => $searchModel,
                        'startedConversationsWith' => $startedConversationsWith,
                        'bData' => $bData,
                    ]);
                case self::TYPE_WIGIG:

                    $modelType = $this->getModelType($modelStation->type, $id);

                    return $this->render('detail/wigig', [
                        'modelStation' => $modelStation,
                        'modelType' => $modelType,
                        'canEdit' => $this->canEdit($id),
                        'dataProvider' => $dataProvider,
                        'searchModel' => $searchModel,
                        'startedConversationsWith' => $startedConversationsWith,
                        'bData' => $bData,
                    ]);
                case self::TYPE_58_AP:
                case self::TYPE_58_TG:
                    $modelType = $this->getModelType($modelStation->type, $id);

                    return $this->render('detail/58ghz', [
                        'modelStation' => $modelStation,
                        'modelType' => $modelType,
                        'canEdit' => $this->canEdit($id),
                        'dataProvider' => $dataProvider,
                        'searchModel' => $searchModel,
                        'startedConversationsWith' => $startedConversationsWith,
                        'bData' => $bData,
                    ]);
                case self::TYPE_52:
                    $modelType = $this->getModelType($modelStation->type, $id);

                    return $this->render('detail/52ghz', [
                        'modelStation' => $modelStation,
                        'modelType' => $modelType,
                        'canEdit' => $this->canEdit($id),
                        'dataProvider' => $dataProvider,
                        'searchModel' => $searchModel,
                        'startedConversationsWith' => $startedConversationsWith,
                        'bData' => $bData,
                    ]);
                default:
                    break;
            }
        } else {
            throw new ForbiddenHttpException();
        }
    }

    public function comparison($id, $type)
    {
        $bData = [];
        $id = Station::deFormatId($id);

        $dataModel = $type == self::TYPE_FS ? StationSearch::getModelsFs($id) : $this->getModel($id);

        /**
         * @var $modelStation Station
         */
        $modelStation = $type == self::TYPE_FS ? $dataModel->stationA : $dataModel;

        if ($modelStation->isDraft()) {
            $modelStation->moveToWaiting();
        }

        // check if the stations is really ready for this step
        if ($type == self::TYPE_FS) {
            if (!$dataModel->stationA->isReadyToBePublished()) {
                return $this->actionStationFs(self::STEP_3_PARAMS, $id);
            }
        } else {
            if (!$dataModel->isReadyToBePublished()) {
                return $this->actionStationWigig(self::STEP_3_PARAMS, $id);
            }
        }

        $searchModel = new StationSearch();
        $filter = Yii::$app->request->queryParams;
        $data = $searchModel->searchCloseStations($modelStation, $filter);

        if ($modelStation->isFs()) {
            $bModel = $modelStation->getPairStation();
            $bData = $searchModel->searchCloseStations($bModel);
        }

        // sorting by conflict ones first
        uasort($data, function (Station $a, Station $b) use ($bData, $modelStation)
        {

            /*
             * 1 => up
             * 0 => keep
             * -1 => down
             */
            $aHasConflict = ($a->hasConflicts || (isset($bData[$a->id]) && $bData[$a->id]->hasConflicts));
            $bHasConflict = ($b->hasConflicts || (isset($bData[$b->id]) && $bData[$b->id]->hasConflicts));

            $aShowInterference = $a->showInterference($modelStation->type, $a->type, 'general');
            $bShowInterference = $b->showInterference($modelStation->type, $b->type, 'general');

            if ($bHasConflict && !$aHasConflict) {
                return 1;
            } elseif (!$bHasConflict && $aHasConflict) {
                return -1;
            } elseif ($bHasConflict && $aHasConflict) {
                return 0;
            } else {
                if ($bShowInterference && !$aShowInterference) {
                    return 1;
                } elseif (!$bShowInterference && $aShowInterference) {
                    return -1;
                } else {
                    return 0;
                }
            }
        });

        if (array_key_exists('StationSearch', $filter) && array_key_exists('hasConflicts', $filter['StationSearch'])) {
            if ($filter['StationSearch']['hasConflicts'] == 'yes') {
                $data = array_filter($data, function ($model) use ($bData)
                {
                    return $model->hasConflicts || (isset($bData[$model->id]) && $bData[$model->id]->hasConflicts);
                });
            } elseif ($filter['StationSearch']['hasConflicts'] == 'no') {
                // no
                $data = array_filter($data, function ($model) use ($bData)
                {
                    $out = !$model->hasConflicts && !(isset($bData[$model->id]) && $bData[$model->id]->hasConflicts);
                    return $out;
                });
            }
        }

        $dataProvider = new ArrayDataProvider([
            'allModels' => $data,
            'pagination' => ['pageSize' => 100],
            'sort' => [
                'attributes' => ['id'],
            ],
        ]);

        $startedConversationsWith = array_keys(Thread::find()
                                                     ->where(['id_station' => $modelStation->id])
                                                     ->indexBy('id_station_disturber')
                                                     ->all());

        return $this->renderAjax($this->selectCreateView(self::STEP_4_SUMMARY), [
            'bData' => $bData,
            'canEdit' => $this->canEdit,
            'model' => $dataModel,
            'userId' => Yii::$app->getUser()->id,
            'modelStation' => $modelStation,
            'hasChanged' => $dataModel->hasChanged() ? 'true' : 'false',
            'type' => $this->type,
            'searchModel' => $searchModel,
            'anyInConflict' => $searchModel->anyInConflict,
            'dataProvider' => $dataProvider,
            'startedConversationsWith' => $startedConversationsWith,
            'steps' => $this->stationRegistrationSteps(),
        ]);
    }

    public function comparison58ghz($id, $type)
    {
        $bData = [];
        $id = Station::deFormatId($id);

        $dataModel = $this->getModel($id);
        $modelStation = $dataModel;

        if ($modelStation->isDraft()) {
            $modelStation->moveToWaiting();
        }

        if ($type == self::TYPE_58_AP) {
            if (!$dataModel->isReadyToBePublished()) {
                return $this->actionStationAccessPoint(self::STEP_3_PARAMS, $id);
            }
        } else {
            if (!$dataModel->isReadyToBePublished()) {
                return $this->actionStationTollGate(self::STEP_3_PARAMS, $id);
            }
        }

        $searchModel = new StationSearch();
        $filter = Yii::$app->request->queryParams;
        $data = $searchModel->searchCloseStations($modelStation, $filter);

        // sorting by conflict ones first
        uasort($data, function (Station $a, Station $b) use ($bData, $modelStation)
        {

            /*
             * 1 => up
             * 0 => keep
             * -1 => down
             */
            $aHasConflict = ($a->hasConflicts || (isset($bData[$a->id]) && $bData[$a->id]->hasConflicts));
            $bHasConflict = ($b->hasConflicts || (isset($bData[$b->id]) && $bData[$b->id]->hasConflicts));

            $aShowInterference = $a->showInterference($modelStation->type, $a->type, 'general');
            $bShowInterference = $b->showInterference($modelStation->type, $b->type, 'general');

            if ($bHasConflict && !$aHasConflict) {
                return 1;
            } elseif (!$bHasConflict && $aHasConflict) {
                return -1;
            } elseif ($bHasConflict && $aHasConflict) {
                return 0;
            } else {
                if ($bShowInterference && !$aShowInterference) {
                    return 1;
                } elseif (!$bShowInterference && $aShowInterference) {
                    return -1;
                } else {
                    return 0;
                }
            }
        });
    }

    public function actionStationFs($step, $id = null, $checkAjax = true)
    {
        if ($checkAjax && !Yii::$app->request->isAjax) {
            $urlOptions = !$id ? ['station/create-fs'] : ['station/edit', 'id' => $id];

            $this->view->showSubHeader = true;
            $this->view->subHeaderContent = $this->renderPartial('../site/partial/sub_header', ['showMyOnly' => true]);
            return $this->render('../site/index', [
                'liveboxUrl' => url($urlOptions),
            ]);
        }

        $this->type = self::TYPE_FS;

        if ($step == self::STEP_4_SUMMARY) {
            return $this->comparison($id, self::TYPE_FS);
        }

        $modelStation = StationSearch::getModelsFs($id);
        $modelStation->setStep($step);

        if ($checkAjax && \Yii::$app->request->isPost &&
            ($modelStation->isNewRecord || $this->canEdit($id) || $id === null) &&
            userIsLogged()) {
            $data = Yii::$app->request->post();

            $modelStation->setData($data);
            if ($this->isAjaxActiveFormValidation()) {
                $out = [];
                $a = $this->performAjaxActiveFormValidation($modelStation->stationA);
                foreach ($a as $key => $val) {
                    $keyData = explode('-', $key);
                    $keyToUse = [$keyData[0], 'a', $keyData[1]];
                    $out[implode('-', $keyToUse)] = $val;
                }
                $b = $this->performAjaxActiveFormValidation($modelStation->stationB);
                foreach ($b as $key => $val) {
                    $keyData = explode('-', $key);
                    $keyToUse = [$keyData[0], 'b', $keyData[1]];
                    $out[implode('-', $keyToUse)] = $val;
                }

                return $out;
            }


            $modelStation->setUser(Yii::$app->getUser()->id);

            if ($modelStation->setData($data)) {
                if ($modelStation->save()) {
                    if ($modelStation->setFsData($data, $step)) {
                        if ($modelStation->FsSave()) {

                            //move to the next step if model saved
                            $step += 1;
                            $modelStation->stationA->storeStationProgress($step);

                            if ($step == self::STEP_4_SUMMARY) {
                                return $this->comparison($modelStation->stationA->id, $this->type);
                            }

                            $modelStation->setNextScenarios();
                        }
                    } else {
                        // setting at least data from form to be remembered to the next request. Otherwise the form will be rendered with empty values
                        $modelStation->setFsData($data, $step);

                        if (!$modelStation->hasTypeErrors()) {
                            setErrorFlash(_tF('flash_error'));
                        }
                    }
                }
            } else {
                // setting at least data from form to be remembered to the next request. Otherwise the form will be rendered with empty values
                $modelStation->setFsData($data, $step);

                if (!$modelStation->hasStationErrors()) {
                    setErrorFlash(_tF('flash_error'));
                }
            }
        }


        return $this->render($this->selectCreateView($step), [
            'userId' => \Yii::$app->getUser()->id,
            'canEdit' => $this->canEdit,
            'hasChanged' => $modelStation->hasChanged() ? 'true' : 'false',
            'modelStationA' => $modelStation->stationA,
            'modelStationB' => $modelStation->stationB,
            'modelStationAType' => $modelStation->stationA->typeStation
                ??
                $modelStation->stationA->getStationTypeObject(),
            'modelStationBType' => $modelStation->stationB->typeStation
                ??
                $modelStation->stationB->getStationTypeObject(),
            'steps' => $this->stationRegistrationSteps(),
        ]);
    }

    public function actionStationWigig($step, $id = null, $checkAjax = true)
    {
        if ($checkAjax && !Yii::$app->request->isAjax) {
            $urlOptions = !$id ? ['station/create-wigig'] : ['station/edit', 'id' => $id];

            $this->view->showSubHeader = true;
            $this->view->subHeaderContent = $this->renderPartial('../site/partial/sub_header', ['showMyOnly' => true]);

            return $this->render('../site/index', [
                'liveboxUrl' => url($urlOptions),
            ]);
        }

        $this->type = self::TYPE_WIGIG;

        if ($step == self::STEP_4_SUMMARY) {
            return $this->comparison($id, self::TYPE_WIGIG);
        }

        $modelStation = $this->getModel($id);
        $modelStation->type = $this->type;
        $modelStation->setChecksum();
        $modelStation->setScenario($step);
        /* try to fetch modelTypeStation, if it does not exist yet, create one */
        try {
            $modelType = $this->getModelType($modelStation->type, $id);
        } catch (NotFoundHttpException $exception) {
            $modelType = new StationWigig();
        }
        $modelType->setScenario($step);
        $modelType->setChecksum();


        if ($checkAjax && \Yii::$app->request->isPost &&
            ($modelStation->isNewRecord || $this->canEdit($id) || $id === null) &&
            userIsLogged()) {

            $data = Yii::$app->request->post();
            $modelStation->load($data);
            if ($this->isAjaxActiveFormValidation()) {
                $modelType->load($data);
                return array_merge($this->performAjaxActiveFormValidation($modelStation),
                    ($this->performAjaxActiveFormValidation($modelType)));
            }

            $modelStation->type = $this->type;
            $modelStation->setUserId(Yii::$app->getUser()->id);

            if ($modelStation->load($data)) {
                $isNewRecord = $modelStation->isNewRecord;
                if ($isNewRecord) {
                    $modelStation->initRegistrations();
                }

                $transaction = Yii::$app->db->beginTransaction();

                if ($modelStation->save()) {
                    $modelType->id_station = $modelStation->id;

                    if ($modelType->load($data)) {
                        $modelType->setPtmp($modelStation->antenna_volume);

                        if ($modelType->getScenario() == StationController::STEP_2_LOCATION) {
                            $modelType->setAngleDirections();
                        }
                        if ($modelType->getScenario() == StationController::STEP_3_PARAMS) {
                            if ($modelType->eirp_method == StationWigig::EIRP_METHOD_MANUAL) {
                                $modelStation->power = null;
                                $modelStation->antenna_volume = null;
                                $modelStation->save();
                            } elseif ($modelStation->antenna_volume != null && $modelStation->power != null) {
                                $modelType->eirp = $modelStation->power + $modelStation->antenna_volume;
                            } else {
                                $modelType->eirp = null;
                            }
                        }
                        if ($modelType->save()) {

                            $transaction->commit();

                            $step += 1;
                            $modelStation->storeStationProgress($step);

                            if ($step == self::STEP_4_SUMMARY) {
                                return $this->comparison($modelStation->id, $modelStation->type);
                            }
                        }
                    } else {
                        if (!$modelType->hasErrors() && $step > self::STEP_2_LOCATION) {
                            setErrorFlash(_tF('flash_error'));
                            $transaction->rollBack();
                        } else {
                            $modelType->save();
                            $modelStation->typeStation = $modelType;
                            $transaction->commit();

                            if ($modelStation->name == '') {
                                $modelStation->name = $modelStation->getTypeName();
                                $modelStation->skipLogging = true;
                                $modelStation->save();
                            }

                            $step += 1;
                            $modelStation->storeStationProgress($step);

                            $modelStation->setNextScenario();
                            $modelType->setNextScenario();
                        }
                    }
                }

                // show PtMP with PtP pair error on antenna_volume and eirp input
                if ($modelType->hasErrors('is_ptmp')) {
                    $modelStation->addError('antenna_volume', $modelType->getFirstError('is_ptmp'));
                    $modelType->addError('eirp', $modelType->getFirstError('is_ptmp'));
                }
            } else {
                if (!$modelStation->hasErrors()) {
                    setErrorFlash(_tF('flash_error'));
                }
            }

            $modelType->load($data);
        }

        return $this->render($this->selectCreateView($step), [
            'userId' => \Yii::$app->getUser()->id,
            'canEdit' => $this->canEdit,
            'hasChanged' => ($modelStation->hasChanged() || $modelType->hasChanged()) ? 'true' : 'false',
            'modelStation' => $modelStation,
            'modelStationType' => $modelType,
            'steps' => $this->stationRegistrationSteps(),
        ]);
    }

    public function actionStationAccessPoint($step, $id = null, $checkAjax = true)
    {

        if (!hasAccessTo('app-frontend_station_add_access_point')) {
            throw new ForbiddenHttpException();
        }

        if ($checkAjax && !Yii::$app->request->isAjax) {
            $urlOptions = !$id ? ['station/create-access-point'] : ['station/edit', 'id' => $id];

            $this->view->showSubHeader = true;
            $this->view->subHeaderContent = $this->renderPartial('../site/partial/sub_header', ['showMyOnly' => true]);

            return $this->render('../site/index', [
                'liveboxUrl' => url($urlOptions),
            ]);
        }

        $this->type = self::TYPE_58_AP;

        $modelStation = $this->getModel($id);
        $modelStation->type = $this->type;
        $modelStation->hardware_identifier = Station::MAC_ADDRESS;
        $modelStation->setChecksum();
        $modelStation->setScenario($step);

        /* try to fetch modelTypeStation, if it does not exist yet, create one */
        try {
            $modelType = $this->getModelType($modelStation->type, $id);
        } catch (NotFoundHttpException $exception) {
            $modelType = new Station58();
        }
        $modelType->is_ap = 1;
        $modelType->setChecksum();

        if ($checkAjax && \Yii::$app->request->isPost &&
            ($modelStation->isNewRecord || $this->canEdit($id) || $id === null) &&
            userIsLogged()) {
            $data = Yii::$app->request->post();
            $modelStation->type = $this->type;
            $modelStation->id_user = Yii::$app->getUser()->id;

            if ($modelStation->load($data)) {
                $isNewRecord = $modelStation->isNewRecord;
                if ($isNewRecord) {
                    $modelStation->initRegistrations();
                }

                $transaction = Yii::$app->db->beginTransaction();
                if ($modelStation->save()) {
                    $modelType->id_station = $modelStation->id;

                    if ($modelType->save()) {
                        $transaction->commit();

                        if (!$modelType->hasErrors()) {
                            if ($modelStation->isDraft() &&
                                $modelStation->getScenario() == StationController::STEP_3_PARAMS) {
                                $modelStation->moveToWaiting();
                            }
                            $step += 1;
                            $modelStation->storeStationProgress($step);

                            $modelStation->setNextScenario();
                        }
                    } else {
                        $transaction->rollBack();
                    }
                }
            } else {
                if (!$modelStation->hasErrors()) {
                    setErrorFlash(_tF('flash_error'));
                }
            }
        }

        $searchModel = null;
        $closeStations = [];
        $anyInConflict = false;

        if ($step == self::STEP_4_SUMMARY) {
            $searchModel = new StationSearch();
            $closeStations = $searchModel->searchTollGateVicinity($modelStation, $modelStation->type);
            $anyInConflict = $searchModel->anyInConflict;
        }

        return $this->renderAjax($this->selectCreateView($step), [
            'userId' => \Yii::$app->getUser()->id,
            'canEdit' => $this->canEdit,
            'hasChanged' => ($modelStation->hasChanged() || $modelType->hasChanged()) ? 'true' : 'false',
            'modelStation' => $modelStation,
            'modelStationType' => $modelType,
            'steps' => $this->stationRegistrationSteps(),
            'anyInConflict' => $anyInConflict,
            'closeStations' => $closeStations,
        ]);
    }

    public function actionStationTollGate($step, $id = null, $checkAjax = true)
    {

        if (!hasAccessTo('app-frontend_station_add_toll_gate')) {
            throw new ForbiddenHttpException();
        }

        if ($checkAjax && !Yii::$app->request->isAjax) {
            $urlOptions = !$id ? ['station/create-toll-gate'] : ['station/edit', 'id' => $id];
            return $this->render('../site/index', [
                'liveboxUrl' => url($urlOptions),
            ]);
        }

        $this->type = self::TYPE_58_TG;

        $modelStation = $this->getModel($id);
        $modelStation->type = $this->type;
        $modelStation->hardware_identifier = Station::MAC_ADDRESS;
        $modelStation->setChecksum();
        $modelStation->setScenario($step);

        /* try to fetch modelTypeStation, if it does not exist yet, create one */
        try {
            $modelType = $this->getModelType($modelStation->type, $id);
        } catch (NotFoundHttpException $exception) {
            $modelType = new Station58();
        }
        $modelType->is_ap = 0;
        $modelType->setChecksum();

        if ($checkAjax && \Yii::$app->request->isPost &&
            ($modelStation->isNewRecord || $this->canEdit($id) || $id === null) &&
            userIsLogged()) {
            $data = Yii::$app->request->post();
            $modelStation->type = $this->type;
            $modelStation->id_user = Yii::$app->getUser()->id;

            if ($modelStation->load($data)) {
                $isNewRecord = $modelStation->isNewRecord;
                if ($isNewRecord) {
                    $modelStation->initRegistrations();
                }

                $transaction = Yii::$app->db->beginTransaction();

                if ($modelStation->save()) {
                    $modelType->id_station = $modelStation->id;

                    if ($modelType->save()) {
                        $transaction->commit();

                        if (!$modelType->hasErrors()) {
                            if ($modelStation->isDraft() &&
                                $modelStation->getScenario() == StationController::STEP_3_PARAMS) {
                                $modelStation->moveToWaiting();
                            }
                            $step += 1;
                            $modelStation->storeStationProgress($step);

                            $modelStation->setNextScenario();
                        }
                    } else {
                        $transaction->rollBack();
                    }
                }
            } else {
                if (!$modelStation->hasErrors()) {
                    setErrorFlash(_tF('flash_error'));
                }
            }
        }

        return $this->renderAjax($this->selectCreateView($step), [
            'userId' => \Yii::$app->getUser()->id,
            'canEdit' => $this->canEdit,
            'hasChanged' => ($modelStation->hasChanged() || $modelType->hasChanged()) ? 'true' : 'false',
            'modelStation' => $modelStation,
            'modelStationType' => $modelType,
            'steps' => $this->stationRegistrationSteps(),
        ]);
    }

    public function actionStation52($step, $id = null, $checkAjax = true)
    {
        if (!hasAccessTo('app-frontend_station_add_52ghz')) {
            throw new ForbiddenHttpException();
        }

        if ($checkAjax && !Yii::$app->request->isAjax) {
            $urlOptions = !$id ? ['station/create-station-52-ghz'] : ['station/edit', 'id' => $id];

            $this->view->showSubHeader = true;
            $this->view->subHeaderContent = $this->renderPartial('../site/partial/sub_header', ['showMyOnly' => true]);

            return $this->render('../site/index', [
                'liveboxUrl' => url($urlOptions),
            ]);
        }

        $this->type = self::TYPE_52;

        $modelStation = $this->getModel($id);
        $modelStation->type = $this->type;
        $modelStation->hardware_identifier = Station::MAC_ADDRESS;
        $modelStation->setChecksum();
        $modelStation->setScenario($step);

        /* try to fetch modelTypeStation, if it does not exist yet, create one */
        try {
            $modelType = $this->getModelType($modelStation->type, $id);
        } catch (NotFoundHttpException $exception) {
            $modelType = new Station52();
        }
        $modelType->setChecksum();

        if ($checkAjax && \Yii::$app->request->isPost &&
            ($modelStation->isNewRecord || $this->canEdit($id) || $id === null) &&
            userIsLogged()) {
            $data = Yii::$app->request->post();
            $modelStation->type = $this->type;
            $modelStation->id_user = Yii::$app->getUser()->id;

            if ($modelStation->load($data)) {
                $isNewRecord = $modelStation->isNewRecord;
                if ($isNewRecord) {
                    $modelStation->initRegistrations();
                }

                $transaction = Yii::$app->db->beginTransaction();

                if ($modelStation->save()) {
                    $modelType->id_station = $modelStation->id;

                    if ($modelType->save()) {
                        $transaction->commit();

                        if (!$modelType->hasErrors()) {
                            if ($modelStation->isDraft() &&
                                $modelStation->getScenario() == StationController::STEP_3_PARAMS) {
                                $modelStation->moveToWaiting();
                            }
                            $step += 1;
                            $modelStation->storeStationProgress($step);

                            $modelStation->setNextScenario();
                        }
                    } else {
                        $transaction->rollBack();
                    }
                }
            } else {
                if (!$modelStation->hasErrors()) {
                    setErrorFlash(_tF('flash_error'));
                }
            }
        }

        $searchModel = null;
        $closeStations = [];
        $anyInConflict = false;

        /*if ($step == self::STEP_4_SUMMARY) {
            $searchModel = new StationSearch();
            $closeStations = $searchModel->searchTollGateVicinity($modelStation, $modelStation->type);
            $anyInConflict = $searchModel->anyInConflict;
        }*/

        return $this->renderAjax($this->selectCreateView($step), [
            'userId' => \Yii::$app->getUser()->id,
            'canEdit' => $this->canEdit,
            'hasChanged' => ($modelStation->hasChanged() || $modelType->hasChanged()) ? 'true' : 'false',
            'modelStation' => $modelStation,
            'modelStationType' => $modelType,
            'steps' => $this->stationRegistrationSteps(),
            'anyInConflict' => $anyInConflict,
            'closeStations' => $closeStations,
        ]);
    }

    public function actionUnpublish($id)
    {
        $station = Station::findOne($id);
        if ($station && $station->moveToUnpublished()) {
            setSuccessFlash(_tF('flash_successfully_unpublished', 'station'));
        } else {
            setErrorFlash(_tF('error_while_unpublishing', 'station'));
        }

        return $this->redirect(['site/index']);
    }

    public function actionDelete($id)
    {
        if (Station::deleteStation($id)) {
            setSuccessFlash(_tF('flash_successfully_deleted', 'station'));
        } else {
            setErrorFlash(_tF('error while deleting', 'station'));
        }

        return $this->redirect(['site/index']);
    }

    protected function selectCreateView($step): string
    {
        $path = ['station'];

        switch ($this->type) {
            case self::TYPE_FS:
                $path[] = 'fs';
                $views = self::$viewsFs;
                break;
            case self::TYPE_WIGIG:
                $path[] = 'wigig';
                $views = self::$viewsWigig;
                break;
            case self::TYPE_58_AP:
                $path[] = '58ghz';
                $path[] = 'access_point';
                $views = self::$views58;
                break;
            case self::TYPE_58_TG:
                $path[] = '58ghz';
                $path[] = 'toll_gate';
                $views = self::$views58;
                break;
            case self::TYPE_52:
                $path[] = '52ghz';
                $views = self::$views52;
                break;
            default:
                throw new Exception(_tF('Invalid station type', 'station'));
        }

        $path[] = $views[$step];
        return implode(DIRECTORY_SEPARATOR, $path);
    }

    protected function getModel(int $id = null): Station
    {
        if ($this->model) {
            return $this->model;
        }

        if (!$id) {
            $station = (new Station());
            $station->status = Station::STATUS_DRAFT;
            $station->hardware_identifier = Station::MAC_ADDRESS;
            return $station;
        }
        $model = Station::find()->andWhere(['id' => $id])->one();
        if ($model !== null) {
            $this->model = $model;
            return $model;
        } else {
            throw new NotFoundHttpException(_t('The requested page does not exist.'));
        }
    }

    protected function getModelType($type, int $id = null)
    {
        switch ($type) {
            case self::TYPE_FS:
                $className = StationFs::class;
                break;
            case self::TYPE_WIGIG:
                $className = StationWigig::class;
                break;
            case self::TYPE_58_TG:
            case self::TYPE_58_AP:
                $className = Station58::class;
                break;
            case self::TYPE_52:
                $className = Station52::class;
                break;
            default:
                throw new Exception(_tF('Invalid station type', 'station'));
        }

        if (!$id) {
            return new $className;
        }

        $model = $className::find()->andWhere(['id_station' => $id])->one();
        if ($model !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException(_t('The requested page does not exist.'));
        }
    }

    public function actionPublish($id)
    {
        $id = Station::deFormatId($id);
        if (\Yii::$app->request->isPost && $this->canEdit($id) && userIsLogged()) {
            $model = $this->getModel($id);
            $model->published_by = Station::PUBLISHED_BY_PUBLICATION;

            if ($model->type == self::TYPE_58_TG || $model->type == self::TYPE_58_AP || $model->type == self::TYPE_52) {
                $model->setScenario(self::STEP_4_SUMMARY);
            }

            $isMacUniqueResponse = ['answer' => true];
            if (trim($model->mac_address) != '') {
                $isMacUniqueResponse = $this->actionIsMacAddressUnique($model->mac_address, $model->id);
            }

            if ($isMacUniqueResponse['answer']) {
                if (!$model->hasConflictsWithOthers()) {
                    // Get session control of partner
                    $check = $this->loadPartnerCheck();
                    if ($check !== $model->id_station_pair && !$model->saveMyPartner()) {
                        $model->addError('id_station_pair', _tF('partner_is_not_suitable', 'station'));
                        $model->setScenario(self::STEP_2_LOCATION);
                        $model->id_station_pair = null;
                        $model->storeStationProgress(self::STEP_2_LOCATION);
                        return $this->actionStationWigig(self::STEP_2_LOCATION, $id);
                    }

                    if ($model->isReadyToBePublished() && $model->moveToFinished()) {
                        setSuccessFlash(_tF('successfully published', 'station'));

                        if (!$model->isFs()) { // wigig only
                            if ($model->id_station_pair !== null && $check !== $model->id_station_pair) {
                                $this->storePartnerCheck($model->id);
                                setInfoFlash(_tF('republication_of_partner_flash', 'station'));
                                $model->stationPair->storeStationProgress(self::STEP_4_SUMMARY);
                                return $this->redirect(['station/edit', 'id' => $model->id_station_pair]);
                            } else {
                                \Yii::$app->session->remove(self::PARTNER_SESSION_KEY);
                            }
                        }
                    } else {
                        setWarningFlash(_tF('not ready to be published', 'station'));
                        // Send user to edit site, if station cannot be prolonged
                        return $this->redirect(['station/edit', 'id' => $id]);
                    }
                } else {
                    setWarningFlash(_tF('has conflicts', 'station'));
                }
            } else {
                setErrorFlash($isMacUniqueResponse['message']);
            }
        }

        return $this->redirect(['station/station', 'id' => $id]);
    }

    public function actionProlongRegistration($id)
    {
        if ($this->canEdit($id)) {
            $model = $this->getModel($id);
            switch ($model->type) {
                case StationController::TYPE_WIGIG:
                    return $this->actionStationWigig(self::STEP_4_SUMMARY, $id);
                case StationController::TYPE_52:
                    return $this->actionStation52(self::STEP_4_SUMMARY, $id);
                case StationController::TYPE_58_TG:
                    return $this->actionStationTollGate(self::STEP_4_SUMMARY, $id);
                case StationController::TYPE_58_AP:
                    return $this->actionStationAccessPoint(self::STEP_4_SUMMARY, $id);
                case StationController::TYPE_FS:
                default:
                    return $this->actionStationFs(self::STEP_4_SUMMARY, $id);
            }
        } else {
            throw new BadRequestHttpException('neplatny request');
        }
    }

    public function actionDeclaration($id)
    {
        if (!Yii::$app->request->isAjax) {
            $urlOptions = ['station/declaration', 'id' => $id];

            $this->view->showSubHeader = true;
            $this->view->subHeaderContent = $this->renderPartial('../site/partial/sub_header', ['showMyOnly' => true]);

            return $this->render('../site/index', [
                'liveboxUrl' => url($urlOptions),
            ]);
        }

        $id = Station::deFormatId($id);
        $model = $this->getModel($id);
        if ($model->isWaiting() || $model->isForProlongation()) {
            $steps = $this->stationRegistrationSteps();
            return $this->render('station/declaration', ['modelStation' => $model, 'steps' => $steps]);
        } else {
            throw new BadRequestHttpException('neplatny request');
        }
    }

    public function actionContactToConflictUsers($id)
    {

        $id = Station::deFormatId($id);
        $model = $this->getModel($id);

        $searchModel = new StationSearch();
        $data = $searchModel->searchCloseStations($model);
        $conflictStations = [];
        if ($searchModel->conflictIds) {
            $conflictStations = Station::find()->where(['IN', 'id', $searchModel->conflictIds])->all();
        }
        $userIds = array_map(function (Station $model)
        {
            return $model->id_user;
        }, $conflictStations);
        $userIds = array_unique($userIds);

        $users = User::find()->where(['IN', 'id', $userIds])->all();

        return $this->render('station/partial/contact-users', ['users' => $users]);
    }

    public function actionDeclare($id)
    {
        $id = Station::deFormatId($id);
        if (\Yii::$app->request->isPost && $this->canEdit($id) && userIsLogged()) {
            $model = $this->getModel($id);

            // do  not log in afterSave, because it would save as publication
            $model->skipLogging = true;
            $model->published_by = Station::PUBLISHED_BY_DECLARATION;

            // Get session control of partner
            $check = $this->loadPartnerCheck();
            if ($check !== $model->id_station_pair && !$model->saveMyPartner()) {
                $model->addError('id_station_pair', _tF('partner_is_not_suitable', 'station'));
                $model->setScenario(self::STEP_2_LOCATION);
                $model->id_station_pair = null;
                $model->storeStationProgress(self::STEP_2_LOCATION);
                return $this->actionStationWigig(self::STEP_2_LOCATION, $id);
            }

            if (($model->isWaiting() || $model->isForProlongation()) && $model->moveToFinished()) {
                $logManager = LogManager::getInstance();
                $logOperation = (new LogOperation())->setData(false, [], $model);
                $logOperation->operationType = ActiveRecord::OPERATION_TYPE_UPDATE;
                $logOperation->operations = [
                    'declare' =>
                        [
                            'user {userName} declared station {stationName}',
                            ['userName' => $model->user->getName(), 'stationName' => $model->getTypeName(),],
                        ],
                ];

                $logManager->registerOperation($logOperation);
                $logManager->saveOperations(null);
                setSuccessFlash(_tF('successfully declared', 'station'));

                if (!$model->isFs()) { // wigig only
                    if ($model->id_station_pair !== null && $check !== $model->id_station_pair) {
                        $this->storePartnerCheck($model->id);
                        setInfoFlash(_tF('republication_of_partner_flash', 'station'));
                        $model->stationPair->storeStationProgress(self::STEP_4_SUMMARY);
                        return $this->redirect(['station/edit', 'id' => $model->id_station_pair]);
                    } else {
                        \Yii::$app->session->remove(self::PARTNER_SESSION_KEY);
                    }
                }

                return $this->redirect(['station', 'id' => $id]);
            } else {
                setWarningFlash(_tF('has conflicts', 'station'));
            }
        }

        return $this->redirect(['station', 'id' => $id]);
    }

    protected function canEdit($id)
    {
        $model = $this->getModel($id);
        if ($model->id_user == null) {
            return false;
        }

        // Check alliance too
        return $model->id_user == Yii::$app->getUser()->id ||
            hasAccessTo('app-frontend_station_update-all') ||
            UserAlliance::areInOneAlliance($model->id_user, Yii::$app->getUser()->id);
    }

    protected function canSee($id)
    {
        $userId = Yii::$app->getUser()->id;
        $model = $this->getModel($id);

        if ($model->id_user == $userId ||
            $model->isPublished() ||
            hasAccessTo('app-frontend_station_update-all') ||
            UserAlliance::areInOneAlliance($model->id_user, $userId)) {
            return true;
        } else {
            $threadAllowers = Thread::find()->where([
                'or',
                ['id_recipient' => $userId, 'id_station' => $id],
                ['id_sender' => $userId, 'id_station' => $id],
            ])->count();
            return $threadAllowers > 0;
        }
    }

    public function actionRegistrationStations()
    {
        return $this->renderAjax('registration/stations_selector');
    }

    public function actionIsMacAddressUnique($value, $id = null)
    {
        $model = new Station();
        if ($id) {
            $id = Station::deFormatId($id);
            $model = $this->getModel($id);
        }

        Yii::$app->response->format = Response::FORMAT_JSON;

        $answer = $model->isMacAddressUnique($value, $duplicitingStationId);
        $message = null;
        if (!$answer) {
            $s = Station::findOne($duplicitingStationId);
            $message = _tF(($s->id_user == loggedUser()->id) ? 'unique_mac_error_with_id' :
                'unique_mac_error_with_id_and_user',
                'station', [
                    'value' => $value,
                    'id' => $duplicitingStationId,
                    'idUser' => $s->id_user,
                ]);
        }

        return [
            'answer' => $answer,
            'message' => $message,
        ];
    }

    public function actionIsSerialNumberUnique($value, $id = null)
    {
        $model = new Station();
        if ($id) {
            $id = Station::deFormatId($id);
            $model = $this->getModel($id);
        }

        Yii::$app->response->format = Response::FORMAT_JSON;

        return [
            'answer' => $model->isSerialNumberUnique($value),
        ];
    }

    protected function stationRegistrationSteps()
    {
        switch ($this->type) {
            case self::TYPE_FS:
            case self::TYPE_WIGIG:
            case self::TYPE_58_AP:
            case self::TYPE_58_TG:
            case self::TYPE_52:
                return [
                    1 => _tF('name', 'station'),
                    2 => _tF('location', 'station'),
                    3 => _tF('parameters', 'station'),
                    4 => _tF('summary', 'station'),
                ];
            default:
                throw new BadRequestHttpException(_tF('Invalid station type', 'station'));
        }
    }

    public static function actionWigigPairDropdownData($idStation, $lat, $lng, $term = null, $page = 1, $limit = 20)
    {
        \Yii::$app->response->format = Response::FORMAT_JSON;
        $limit = 20;

        $data = [];

        if ($lat && $lng && $idStation) {
            $data = StationSearch::searchWigigsToPair($idStation, $lat, $lng, $term, $page, $limit);
        }

        return [
            'results' => $data,
            'pagination' => [
                'more' => count($data) == $limit,
            ],
        ];
    }

    /**
     * Tries to load session data containing ID of partner to check.
     */
    protected function loadPartnerCheck()
    {
        if ($data = Yii::$app->session->get(self::PARTNER_SESSION_KEY)) {
            $unSerialized = unserialize($data);
            if ($unSerialized['timeout'] >= (new DateTime())) {
                return $unSerialized['data'];
            }
        }
        return -1;
    }

    /**
     * Stores session data containing number of last visited step in station creation process.
     *
     * @param $idPartner - id of partner Station
     *
     */
    protected function storePartnerCheck($idPartner)
    {
        $expirationTime = (new DateTime())->modify("+1 hour");

        $toStore = ['timeout' => $expirationTime, 'data' => $idPartner];
        $sessionKey = self::PARTNER_SESSION_KEY;

        Yii::$app->session->set($sessionKey, serialize($toStore));
    }

    public function actionChangeType58To52($id)
    {
        $station = Station::findOne($id);
        if (is_null($station)) {
            setErrorFlash(_tF('station does not exists', 'station'));
        }
        if ($station->changeType58to52()) {
            setSuccessFlash(_tF('type successfully changed', 'station'));
        } else {
            setErrorFlash(_tF('type cannot be changed', 'station'));
        }

        return $this->redirect(['station/station', 'id' => $id]);
    }
}