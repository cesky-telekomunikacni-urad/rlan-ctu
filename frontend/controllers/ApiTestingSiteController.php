<?php
/*
* Copyright 2023 DactylGroup s.r.o.
*
* Licensed under the EUPL, Version 1.2 or – as soon they
will be approved by the European Commission - subsequent
versions of the EUPL (the "Licence");
* You may not use this work except in compliance with the
Licence.
* You may obtain a copy of the Licence at:
*
* https://joinup.ec.europa.eu/software/page/eupl5
*
* Unless required by applicable law or agreed to in
writing, software distributed under the Licence is
distributed on an "AS IS" basis,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
express or implied.
* See the Licence for the specific language governing
permissions and limitations under the Licence.
*/
?>


<?php

namespace frontend\controllers;

use dactylcore\page\frontend\models\Page;
use Yii;
use yii\web\Controller;

/**
 * Site controller
 */
class ApiTestingSiteController extends Controller
{
    /**
     * Displays homepage.
     *
     * @return string
     */
    public function actionIndex()
    {
        Yii::$app->getSession()->removeAll();

        $page = Page::findOne(c('TESTING_API_MAIN_SITE'));
        $this->layout = '@frontend/views/layouts/api-testing';

        // Show implicit api testing page
        if (is_null($page)) {
            return $this->render('index', []);
        }

        // Show created page through admin site
        $title = ($page->meta_title !== null && $page->meta_title !== '') ? $page->meta_title : $page->title;
        $description = ($page->meta_description !== null && $page->meta_description !== '') ? $page->meta_description : $page->annotation;

        // Meta tags
        $this->view->title = $title . ' | ' . Yii::$app->name;
        $this->view->registerMetaTag(['name' => 'description', 'content' => $description]);
        // OG
        $this->view->registerMetaTag([
            'property' => 'og:type',
            'content' => 'website',
        ]);
        $this->view->registerMetaTag(['property' => 'og:url', 'content' => absoluteCurrentUrl()]);
        $this->view->registerMetaTag(['property' => 'og:locale', 'content' => str_replace('-', '_', getLangIsoCode())]);
        $this->view->registerMetaTag(['property' => 'og:title', 'content' => $title]);
        $this->view->registerMetaTag(['property' => 'og:description', 'content' => $description]);

        // OG Image
        $image = $page->metaImageFile;
        if (!$image) {
            $image = $page->image;
        }

        if ($image) {
            $info = json_decode($image->info, true);
            $this->view->registerMetaTag(['property' => 'og:image', 'content' => absoluteUrl($image->getFileUrl())]);
            $this->view->registerMetaTag(['property' => 'og:image:width', 'content' => $info['width']]);
            $this->view->registerMetaTag(['property' => 'og:image:height', 'content' => $info['height']]);
        }

        return $this->render('@dc-page/frontend/views/page/view', [
            'page' => $page,
        ]);
    }

    /**
     * Displays error page.
     *
     * @return string
     */
    public function actionError()
    {
        $exception = \Yii::$app->errorHandler->exception;
        if (isset($exception->statusCode) && $exception->statusCode == 404) {
            $message = _tF('page not found', 'error');
        } elseif (isset($exception->statusCode) && $exception->statusCode == 403) {
            $message = _tF('forbidden', 'error');
        } else {
            $message = _tF('unknown error', 'error');
        }
        return $this->render('error', ['message' => $message]);
    }
}
