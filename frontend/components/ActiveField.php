<?php
/*
* Copyright 2023 DactylGroup s.r.o.
*
* Licensed under the EUPL, Version 1.2 or – as soon they
will be approved by the European Commission - subsequent
versions of the EUPL (the "Licence");
* You may not use this work except in compliance with the
Licence.
* You may obtain a copy of the Licence at:
*
* https://joinup.ec.europa.eu/software/page/eupl5
*
* Unless required by applicable law or agreed to in
writing, software distributed under the Licence is
distributed on an "AS IS" basis,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
express or implied.
* See the Licence for the specific language governing
permissions and limitations under the Licence.
*/
?>


<?php


namespace frontend\components;


use dactylcore\core\helpers\Html;

class ActiveField extends \dactylcore\core\widgets\common\activeform\ActiveField
{
    public function hint($content, $options = [])
    {
        if ($content === null && method_exists($this->model, 'getAttributeHint')) {
            $content = $this->model->getAttributeHint($this->attribute);
        }

        if ($content === false || $content === '' || $content === null) {
            $this->parts['{hint}'] = '';

            return $this;
        }

        $this->template = "<div class='control-label-hint-group'>{label}{hint}</div>\n{input}\n{error}";
        $this->parts['{hint}'] = Html::tag('span', Html::tag('i', 'help_outline', [
            'class' => 'material-icons',
        ]), [
            'class' => 'control-hint',
            'data-container' => 'body',
            'data-toggle' => 'popover',
            'data-placement' => 'top',
            'data-html' => 'true',
            'data-content' => $content,
        ]);

        return $this;
    }
}