<?php
/*
* Copyright 2023 DactylGroup s.r.o.
*
* Licensed under the EUPL, Version 1.2 or – as soon they
will be approved by the European Commission - subsequent
versions of the EUPL (the "Licence");
* You may not use this work except in compliance with the
Licence.
* You may obtain a copy of the Licence at:
*
* https://joinup.ec.europa.eu/software/page/eupl5
*
* Unless required by applicable law or agreed to in
writing, software distributed under the Licence is
distributed on an "AS IS" basis,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
express or implied.
* See the Licence for the specific language governing
permissions and limitations under the Licence.
*/
?>


<?php
namespace frontend\components;

use common\models\Thread;
use dactylcore\core\widgets\common\bootstrap\Html;
use frontend\widgets\dactylkit\DactylKit;
use yii\helpers\ArrayHelper;

class Menu extends \dactylcore\menu\frontend\widgets\menu\Menu
{

    public function renderLink($label, $url, $newWindow, $linkOptions = [])
    {
        $options = ArrayHelper::merge($this->linkOptions, $linkOptions);

        if ($newWindow) {
            $options['target'] = '_blank';
        }

        if (isset($options['class'])) {
            if ($options['class'] == 'dk--nav-bar--conversations') {
                if (Thread::findUnreadThreads() != 0) {
                    $options['class'] = $options['class'] . ' dk--nav-bar--conversations__unread';
                }
            }
            if ($options['class'] == 'dk--nav-bar--create') {
                $icon = DactylKit::icon(DactylKit::ICON_PLUS);
                $label = <<<HTML
                    <div class="dk--btn__icon">$icon</div>
                    <span class="dk--btn__label">$label</span>
HTML;
                $options['class'] = $options['class'] . ' dk--btn dk--btn--primary dk--btn--mini dk--nav-bar__hide-md';
                $options['data-pjax'] = 1;
                $options['data-livebox'] = 1;
                $options['data-livebox-customparams'] = '{"close":true}';
            }
        }

        return Html::a($label, $url, $options);

    }

    /**
     * had to modify this to do not interfere with styles in SCSS files (mobile menu)
     */
    public function registerClientCss()
    {
        $this->view->registerCss(
            <<<CSS
        @media (max-width: {$this->responsiveBreakpoint}px) {
            html.{$this->id}-disabled-scroll {
                overflow: hidden;
            }
            
            #{$this->id}-hamburger {
                display: block;
                z-index: 102;
                position: relative;
                top: 0;
                right: 0;
            }
            
            #{$this->id} nav {
                z-index: 101;
                position: fixed;
                top: 0;
                right: 0;
                height: 100%;
                min-width: 304px;
                max-width: 304px;
                padding: 80px 24px 24px 24px;
                overflow: hidden;
                background: #fff;
                transform: translateX(100%);
                transition: transform 300ms cubic-bezier(.4,0,.2,1);
            }
            
            #{$this->id} nav > ul {
                height: calc(100% - 80px - 24px);
                padding-top: 0;
                margin-right: -24px;
                overflow-x: hidden;
                overflow-y: scroll;
            }
            
            #{$this->id} nav > ul:before {
                content: '';
                display: block;
                position: absolute;
                top: auto;
                left: 24px;
                right: 24px;
                border-top: 1px solid #f2f2f2;
            }
            
            #{$this->id}.dc-menu-responsive-active:before {
                visibility: visible;
                opacity: 1;
            }
            
            #{$this->id}.dc-menu-responsive-active nav {
                transform: translateX(0%);
            }
            
            #{$this->id} nav ul {
                flex-flow: column;
                visibility: visible;
                opacity: 1;
                background: transparent;
                transition: opacity 0ms, visibility 0ms;
            }
            
            #{$this->id} nav > ul > li {
               padding-left: 0;
               border-bottom: none;
            }
            
            #{$this->id} nav ul li.dropdown > span {
                display: block;
                cursor: default;
            }
            
            #{$this->id} nav ul li.dropdown > ul {
                padding: 0;
            }
            
            #{$this->id} nav ul li a, #{$this->id} nav ul li span {
                display: block;
                line-height: 24px;
                font-size: 14px;
                padding: 16px 8px;
                border-bottom: 1px solid #f2f2f2;
            }
            
            #{$this->id} nav > ul > li > ul > li > a, #{$this->id} nav > ul > li > ul > li > span {
                line-height: 18px;
                font-size: 12px;
            }
            
            #{$this->id} nav > ul > li > ul > li > ul > li > a, #{$this->id} nav > ul > li > ul > li > ul > li > span {
                padding: 8px 0 8px 8px;
                line-height: 18px;
                font-size: 12px;
                color: #757575;
            }
            
            #{$this->id} nav ul li a:after, #{$this->id} nav ul li span:after {
                display: none!important;
            }
  
            #{$this->id} nav ul li.active > a, #{$this->id} nav ul li:hover > a {
                
            }
            
            #{$this->id} nav > ul > li.dropdown > ul > li.dropdown {
                padding: 0;
                margin: 0;
                border-top: 0;
                border-bottom: 0;
            }
            
            #{$this->id} nav ul ul {
                position: static;
            }
  
        }
CSS
        );
    }
}