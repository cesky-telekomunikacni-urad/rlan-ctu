<?php
/*
* Copyright 2023 DactylGroup s.r.o.
*
* Licensed under the EUPL, Version 1.2 or – as soon they
will be approved by the European Commission - subsequent
versions of the EUPL (the "Licence");
* You may not use this work except in compliance with the
Licence.
* You may obtain a copy of the Licence at:
*
* https://joinup.ec.europa.eu/software/page/eupl5
*
* Unless required by applicable law or agreed to in
writing, software distributed under the Licence is
distributed on an "AS IS" basis,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
express or implied.
* See the Licence for the specific language governing
permissions and limitations under the Licence.
*/
?>


<?php

use common\models\search\StationSearch;
use frontend\controllers\StationController;
use frontend\widgets\dactylkit\DactylKit;

\dactylcore\core\widgets\common\pjax\PjaxAsset::register($this);

$unpublishedStations = StationSearch::countUnpublishedStations(Yii::$app->getUser()->id);
$maxUnpublished = c('MAX_WAITING_STATIONS');

$disableStationRegistration = ($unpublishedStations >= $maxUnpublished);
$showWarningRegistration = ($unpublishedStations >= ($maxUnpublished / 4) * 3);
$leftStations = ($maxUnpublished - $unpublishedStations);
if ($leftStations < 0) {
    $leftStations = 0;
}

switch ($leftStations) {
    case 0:
        $leftStationTranslation = _tF('You cannot create another station', 'station-registration');
        break;
    case 1:
        $leftStationTranslation = _tF('You have only one station left to create', 'station-registration');
        break;
    case 2:
    case 3:
    case 4:
        $leftStationTranslation = _t('You have {up4stations} stations left to create', 'station-registration', ['up4stations' => $leftStations]);
        break;
    default:
        $leftStationTranslation = _t('You have {stations} stations left to create', 'station-registration', ['stations' => $leftStations]);
}

switch ($maxUnpublished) {
    case 1:
        $maxUnpublishedTranslated = ['You can only have {stations} station in unpublished state.', ['stations' => $maxUnpublished]];
        break;
    case 2:
    case 3:
    case 4:
        $maxUnpublishedTranslated = ['You can only have {up4stations} stations in unpublished state.', ['up4stations' => $maxUnpublished]];
        break;
    default:
        $maxUnpublishedTranslated = ['You can only have {stations} stations in unpublished state.', ['stations' => $maxUnpublished]];

}

$stations58Items = [];

if (hasAccessTo('app-frontend_station_add_access_point')) {
    $stations58Items[] = [
        'label' => _tF('add ap', 'station'),
        'icon-class' => 'dk--station__ap',
        'sub-label' => _tF('add_ap_sublabel', 'station'),
        'url' => url('station/create-access-point'),
    ];
}

if (hasAccessTo('app-frontend_station_add_toll_gate')) {
    $stations58Items[] = [
        'label' => _tF('add tg', 'station'),
        'icon-class' => 'dk--station__tg',
        'sub-label' => _tF('add_tg_sublabel', 'station'),
        'url' => url('station/create-toll-gate'),
    ];
}

$station52Items = hasAccessTo('app-frontend_station_add_52ghz') ? [
        [
            'label' => _tF('add 52', 'station'),
            'icon-class' => 'dk--station__52',
            'url' => url(['station/create-station-52-ghz']),
            'sub-label' => '',
        ],
    ] :
    [];

$stations = [
    [
        'type' => _tF('60ghz', 'station'),
        'items' => [
            [
                'label' => _tF('add wigig', 'station'),
                'icon-class' => 'dk--station__wigig',
                'url' => url(['station/create-wigig']),
                'sub-label' => _tF('add_wigig_sublabel', 'station'),
            ],
            [
                'label' => _tF('add fs', 'station'),
                'icon-class' => 'dk--station__fs',
                'url' => url(['station/create-fs']),
                'sub-label' => _tF('add_fs_sublabel', 'station'),
            ],
//            [
//                'label' => _tF('import fs', 'station'),
//                'icon-class' => 'dk--station__fs',
//                'url' => '#',
//            ],
//            [
//                'label' => _tF('import wigig', 'station'),
//                'icon-class' => 'dk--station__wigig',
//                'url' => '#',
//            ],
        ],
    ],
    [
        'type' => _tF('58ghz', 'station'),
        'items' => $stations58Items,
    ],
    [
        'type' => _tF('52ghz', 'station'),
        'items' => $station52Items,
    ],
]

?>

<?php \yii\widgets\Pjax::begin([
    'options' => [
        'id' => 'station-form-xx'
    ],
    // to handle just the links, not the forms
    'formSelector' => false,
]); ?>
  <div class="livebox--station-registration">
    <div class="livebox--header">
    </div>
    <div class="livebox--station-registration--content livebox--station-registration__signpost--content">
      <h4><?= _tF('create', 'station') ?></h4>
        <?php if ($showWarningRegistration): ?>
          <div class="livebox--station-registration--content--warning">
              <?= DactylKit::alert($leftStationTranslation,
                  _tF($maxUnpublishedTranslated[0], 'station-registration', $maxUnpublishedTranslated[1]),
                  DactylKit::ALERT_TYPE_WARNING);
              ?>
          </div>
        <?php endif; ?>
        <?php foreach ($stations as $stationType): ?>
        <?php if (count($stationType['items']) != 0) : ?>
          <div class="dk--station--type">
            <span class="dk--station--type__heading"><?= $stationType['type'] ?></span>
            <div class="dk--station--type__list">
                <?php foreach ($stationType['items'] as $item): ?>
                    <?php if ($disableStationRegistration) : ?>
                    <a class="dk--station--type__list-item dk--station--type__list-item__disabled"
                        <?= DactylKit::tooltip(_tF('unpublished limit reached tooltip', 'station')) ?>>
                      <div class="dk--station--type__list-item__icon <?= $item['icon-class'] ?>">
                          <?= DactylKit::icon(isset($item['icon']) ? $item['icon'] : DactylKit::ICON_PLUS) ?>
                      </div>
                      <span class="dk--station--type__list-item__label">
                          <?= $item['label'] ?>
                          <?php if (isset($item['sub-label'])): ?>
                            <span class="dk--station--type__list-item__sub-label"><?= $item['sub-label'] ?></span>
                          <?php endif; ?>
                      </span>
                    </a>
                    <?php else: ?>
                    <a href="<?= $item['url'] ?>" class="dk--station--type__list-item">
                      <div class="dk--station--type__list-item__icon <?= $item['icon-class'] ?>">
                          <?= DactylKit::icon(isset($item['icon']) ? $item['icon'] : DactylKit::ICON_PLUS) ?>
                      </div>
                      <span class="dk--station--type__list-item__label">
                          <?= $item['label'] ?>
                          <?php if (isset($item['sub-label'])): ?>
                            <span class="dk--station--type__list-item__sub-label"><?= $item['sub-label'] ?></span>
                          <?php endif; ?>
                      </span>
                    </a>
                    <?php endif; ?>
                <?php endforeach; ?>
            </div>
          </div>
        <?php endif; ?>
        <?php endforeach; ?>
    </div>
  </div>
<?php \yii\widgets\Pjax::end() ?>