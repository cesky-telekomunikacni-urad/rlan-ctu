<?php
/*
* Copyright 2023 DactylGroup s.r.o.
*
* Licensed under the EUPL, Version 1.2 or – as soon they
will be approved by the European Commission - subsequent
versions of the EUPL (the "Licence");
* You may not use this work except in compliance with the
Licence.
* You may obtain a copy of the Licence at:
*
* https://joinup.ec.europa.eu/software/page/eupl5
*
* Unless required by applicable law or agreed to in
writing, software distributed under the Licence is
distributed on an "AS IS" basis,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
express or implied.
* See the Licence for the specific language governing
permissions and limitations under the Licence.
*/
?>


<?php

//uses
use common\models\Station;
use frontend\assets\LayoutAsset;
use frontend\assets\map\DetailAccessPointAsset;
use frontend\assets\map\DetailTollGateAsset;
use frontend\assets\StationAsset;
use frontend\widgets\dactylkit\DactylKit;
use kartik\dialog\Dialog;

/**
 * @var $modelStation \common\models\Station
 * @var $modelType \common\models\Station58
 * @var $canEdit bool
 * @var $dataProvider
 * @var $searchModel
 * @var $startedConversationsWith array
 * @var $bData array
 */

$dmsLat = $modelStation->DDtoDMS(true);
$dmsLng = $modelStation->DDtoDMS(false);

LayoutAsset::register($this);
StationAsset::register($this);
$modelType->is_ap ? DetailAccessPointAsset::register($this) : DetailTollGateAsset::register($this);

Dialog::widget([]);

// Site title
$this->title = _tF('stations', 'station') . " {$modelStation->getFormattedId()}";

// Breadcrumbs
//$this->breadcrumbs[] = $modelType->is_ap
//    ? [
//      'label' => _tF('access points', 'station'),
//      'url' => url(['station/access_points']),
//    ]
//    : [
//        'label' => _tF('toll gates', 'station'),
//        'url' => url(['station/toll_gates']),
//    ];

$this->breadcrumbs[] = [
    'label' => $modelStation->name,
];

$badge = null;

switch ($modelStation->status) {
    case (Station::STATUS_EXPIRED):
    {
        $badge = DactylKit::badge(_tF('expired', 'station'), DactylKit::BADGE_WARNING);
        break;
    }
    case (Station::STATUS_FINISHED):
    {
        $badge = DactylKit::badge(_tF('active', 'station'), DactylKit::BADGE_SUCCESS);
        break;
    }
}

?>

    <div class="dk--column-holder row">
        <section id="vueapp-detail-58" class="dk--container col-md-6 dk--station">
            <div class="dk--container__content">
                <?= DactylKit::breadcrumb() ?>
                <h4><?= $modelStation->name ?></h4>
                <?php if (userIsLogged()): ?>
                    <?= $this->render('partial/user_section', [
                        'canEdit' => $canEdit,
                        'model' => $modelStation,
                    ]) ?>
                    <hr class="dk--hr">
                <?php endif; ?>

                <span class="dk--table--ghost__heading"><?= _tF('information', 'station') ?></span>
                <table class="dk--table dk--table--ghost">
                    <tbody>
                    <tr>
                        <td><?= _tF('kind', 'station') ?></td>
                        <td><?= $modelStation->getTypeShortCut() ?></td>
                    </tr>
                    <tr>
                        <td><?= _tF('status', 'station') ?></td>
                        <td><?= $badge ?></td>
                    </tr>
                    </tbody>
                </table>
                <hr class="dk--hr">

                <?php if ($modelStation->canEdit() && $modelStation->mac_address): ?>
                    <span class="dk--table--ghost__heading"><?= _tF('parameters', 'station') ?></span>
                    <div class="dk--table--scroll-holder">
                        <table class="dk--table dk--table--ghost">
                            <tbody>
                            <tr>
                                <td><?= $modelStation->getAttributeLabel('mac_address') ?></td>
                                <td><?= mb_strtoupper($modelStation->mac_address) ?></td>
                            </tr>
                            </tbody>
                        </table>
                    </div>
                    <hr class="dk--hr">
                <?php endif; ?>

                <span class="dk--table--ghost__heading"><?= _tF('location', 'station') ?></span>
                <table class="dk--table dk--table--ghost">
                    <tbody>
                    <tr>
                        <td><?= _tF('GPS', 'station') ?></td>
                        <td>
                      <span <?= DactylKit::tooltip("{$dmsLat['deg']}° {$dmsLat['min']}' " .
                          round($dmsLat['sec']) .
                          "''") ?>>
                        <?= $modelStation->gpsFormatted(true) ?>
                      </span>,
                            <span <?= DactylKit::tooltip("{$dmsLng['deg']}° {$dmsLat['min']}' " .
                                round($dmsLng['sec']) .
                                "''") ?>>
                        <?= $modelStation->gpsFormatted(false) ?>
                      </span>
                        </td>
                    </tr>
                    <?php if (!$modelType->is_ap): ?>
                        <tr>
                            <td><?= _tF('diameter', 'station') ?></td>
                            <td><?= c('TOLL_GATE_RADIUS') ?> m</td>
                        </tr>
                    <?php endif; ?>
                    </tbody>
                </table>
                <hr class="dk--hr">
                <?= $this->render('partial/table/deadlines', ['model' => $modelStation]) ?>
                <hr class="dk--hr">
                <?= $this->render('partial/table/details', ['model' => $modelStation]) ?>
            </div>
        </section>
        <section class="dk--container dk--container__gray col-md-6 right-container" style="padding: 0;">
            <div class="dk--container__content"
                 style="width: 100%;     height: calc(100vh - 110px); padding: 0; max-width: 100%; position: relative">
                <div class="mapOverlay overlayIndex overlayDetail58">
                    <div class="lds-grid">
                        <div></div>
                        <div></div>
                        <div></div>
                        <div></div>
                        <div></div>
                        <div></div>
                        <div></div>
                        <div></div>
                        <div></div>
                    </div>
                </div>
                <div id="map-detail-58" class="half-width"
                     style="width: 100%; height: 100%; position: fixed; top: 110px; "
                     data-radius="<?= c('TOLL_GATE_RADIUS') ?>"
                     data-stationlink="<?= url(['station/station', 'id' => 0]) ?>">
                </div>
            </div>
        </section>
    </div>
