<?php
/*
* Copyright 2023 DactylGroup s.r.o.
*
* Licensed under the EUPL, Version 1.2 or – as soon they
will be approved by the European Commission - subsequent
versions of the EUPL (the "Licence");
* You may not use this work except in compliance with the
Licence.
* You may obtain a copy of the Licence at:
*
* https://joinup.ec.europa.eu/software/page/eupl5
*
* Unless required by applicable law or agreed to in
writing, software distributed under the Licence is
distributed on an "AS IS" basis,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
express or implied.
* See the Licence for the specific language governing
permissions and limitations under the Licence.
*/
?>


<?php

//uses
use dactylcore\core\widgets\common\pjax\Pjax;
use frontend\assets\LayoutAsset;
use frontend\assets\map\DetailFsAsset;
use frontend\assets\StationAsset;
use frontend\widgets\dactylkit\DactylKit;

/**
 * @var $modelStation \common\models\Station
 * @var $modelStationType \common\models\StationFsPair
 * @var $canEdit bool
 * @var $dataProvider
 * @var $searchModel
 * @var $startedConversationsWith array
 * @var $bData array
 */

LayoutAsset::register($this);
StationAsset::register($this);
DetailFsAsset::register($this);

$modelStationA = $modelStationType->stationA;
$modelStationB = $modelStationType->stationB;

// Site title
$this->title = _tF('stations', 'station') . " {$modelStation->getFormattedId()}";

//// Breadcrumbs
//$this->breadcrumbs[] = [
//    'label' => _tF('stations', 'station'),
//    'url' => url(['/']),
//];

$this->breadcrumbs[] = [
    'label' => $modelStationA->name,
];


?>

<div class="dk--column-holder row">
    <section id="vueappfs-detail" class="dk--container col-md-6 dk--station">
        <div class="dk--container__content">
            <?= DactylKit::breadcrumb() ?>
            <h4><?= $modelStationA->name ?></h4>
            <?php if (userIsLogged()): ?>
                <?= $this->render('partial/user_section', [
                    'canEdit' => $canEdit,
                    'model' => $modelStationA,
                ]) ?>
                <hr class="dk--hr">
            <?php endif; ?>
            <?= $this->render('partial/table/information', ['model' => $modelStationA]) ?>
            <hr class="dk--hr">
            <?= $this->render('partial/table/fs/parameters', [
                'modelStationA' => $modelStationA,
                'modelStationB' => $modelStationB,
            ]) ?>
            <hr class="dk--hr">
            <?= $this->render('partial/table/fs/location', [
                'modelStationA' => $modelStationA,
                'modelStationB' => $modelStationB,
            ]) ?>
            <hr class="dk--hr">
            <?= $this->render('partial/table/deadlines', ['model' => $modelStationA]) ?>
            <hr class="dk--hr">
            <?= $this->render('partial/table/details', ['model' => $modelStationA]) ?>
            <hr class="dk--hr">
            <h6 class="coordination-calc__heading"><?= _tF('affected stations', 'station') ?></h6>
            <span class="livebox--station-registration--content__subheading"><?= _tF('Coordinator calculator table hint',
                    'station') ?></span>
            <?php Pjax::begin([
                'options' => [
                    'id' => 'coordination-calc',
                ],
                'timeout' => 0,
                'linkSelector' => "#coordination-calc .pagination a",
                'enablePushState' => false,
                'enableReplaceState' => false,
            ]) ?>
            <?= $this->render('../station/step5_summary_table', [
                'model' => $modelStation,
                'dataProvider' => $dataProvider,
                'searchModel' => $searchModel,
                'startedConversationsWith' => $startedConversationsWith,
                'bData' => $bData,
                'showFilter' => true,
                'canEdit' => $canEdit,
            ]) ?>
            <?php Pjax::end() ?>
        </div>
    </section>
  <section class="dk--container dk--container__gray col-md-6 right-container" style="padding: 0;">
    <div class="dk--container__content"
         style="width: 100%;     height: calc(100vh - 110px); padding: 0; max-width: 100%; position: relative">
      <div class="mapOverlay overlayIndex overlayDetailFs">
        <div class="lds-grid">
          <div></div>
          <div></div>
          <div></div>
          <div></div>
          <div></div>
          <div></div>
          <div></div>
          <div></div>
          <div></div>
        </div>
      </div>
      <div id="map-detail-fs" class="half-width" style="width: 100%; height: calc(100% - 110px); position: fixed; top: 110px; "
           data-stationlink="<?= url(['station/station', 'id' => 0]) ?>">
      </div>
    </div>
  </section>
</div>
