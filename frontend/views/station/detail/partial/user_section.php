<?php
/*
* Copyright 2023 DactylGroup s.r.o.
*
* Licensed under the EUPL, Version 1.2 or – as soon they
will be approved by the European Commission - subsequent
versions of the EUPL (the "Licence");
* You may not use this work except in compliance with the
Licence.
* You may obtain a copy of the Licence at:
*
* https://joinup.ec.europa.eu/software/page/eupl5
*
* Unless required by applicable law or agreed to in
writing, software distributed under the Licence is
distributed on an "AS IS" basis,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
express or implied.
* See the Licence for the specific language governing
permissions and limitations under the Licence.
*/
?>


<?php

//uses
use common\models\Station;
use frontend\controllers\StationController;
use frontend\widgets\dactylkit\DactylKit;

/**
 * @var $canEdit bool
 * @var $model Station
 */

$deleteConfirmText = _t('Do you really want to delete station', 'station');
if (!$model->isFs() && $model->id_station_pair !== null) {
    $deleteConfirmText .= '<br><br>' . _t('This step will remove pairing!', 'station');
}

$unpublishConfirmText = _t('Do you really want to unpublish station?', 'station');
$changeTypeConfirmText = _tF('Do you really want to change type of this station?', 'station');


?>
<div class="dk--station--user-section">
    <?php
    if ($canEdit): ?>
        <?= DactylKit::link(
            _tF('edit', 'station'),
            url(['station/edit', 'id' => $model->id]),
            DactylKit::LINK_TYPE_TERTIARY,
            DactylKit::LINK_SIZE_NORMAL,
            '@frontend/web/source_assets/img/icon/ic-edit-24.svg',
            '', [
            'class' => 'dk--btn dk--btn--secondary',
            'data' => [
                'pjax' => 1,
                'livebox' => 1,
                'livebox-customparams' => '{"close":false}',
            ],
        ]) ?>
        <?php if ($model->isPublished()): ?>
        <?= DactylKit::link(
            _tF('unpublish', 'station'),
            url(['station/unpublish', 'id' => $model->id]),
            DactylKit::LINK_TYPE_TERTIARY,
            DactylKit::LINK_SIZE_NORMAL,
            '@frontend/web/source_assets/img/icon/ic-unverified-24.svg',
            '', [
            'class' => 'dk--btn dk--btn--secondary',
            'data' => [
                'ajax' => true,
                'confirm' => $unpublishConfirmText,
                'method' => 'post',
            ],
        ]) ?>
        <?php endif; ?>
        <?php if ($model->type == StationController::TYPE_58_AP): ?>
            <?= DactylKit::link(
                _tF("change type to 52", 'station'),
                url(['station/change-type-58-to-52', 'id' => $model->id]),
                DactylKit::LINK_TYPE_TERTIARY,
                DactylKit::LINK_SIZE_NORMAL,
                '@frontend/web/source_assets/img/icon/ic-renew-24.svg',
                '', [
                'class' => 'dk--btn dk--btn--secondary',
                'data' => [
                    'ajax' => true,
                    'confirm' => $changeTypeConfirmText,
                    'method' => 'post',
                ],
            ]) ?>
        <?php endif; ?>
        <?= DactylKit::link(
            _tF('delete', 'station'),
            url(['station/delete', 'id' => $model->id]),
            DactylKit::LINK_TYPE_TERTIARY,
            DactylKit::LINK_SIZE_NORMAL,
            '@frontend/web/source_assets/img/icon/ic-close-24.svg',
            '', [
            'class' => 'dk--btn dk--btn--secondary',
            'data' => [
                'ajax' => true,
                'confirm' => $deleteConfirmText,
                'method' => 'post',
            ],
        ]) ?>

        <div class="shouldReload-wrapper">
            <div id="shouldReload">
                <?= DactylKit::link(
                    _tF('should reload', 'station'),
                    url(['station/station', 'id' => $model->id]),
                    DactylKit::LINK_TYPE_SECONDARY,
                    DactylKit::LINK_SIZE_NORMAL,
                    '@frontend/web/source_assets/img/icon/ic-renew-24.svg',
                    '', [
                    'class' => 'dk--btn dk--btn--primary',
                ]) ?>
            </div>
        </div>
        <span class="dk--station--user-section__description"><?= _tF('edit your station parameters',
                'station') ?></span>

    <?php else: ?>
        <?= DactylKit::link(
            _tF('contact owner', 'station'),
            url(['messaging/generic-question', 'id' => $model->id]),
            DactylKit::LINK_TYPE_TERTIARY,
            DactylKit::LINK_SIZE_NORMAL,
            '@frontend/web/source_assets/img/icon/ic-chat-16.svg',
            '', [
            'class' => 'dk--btn dk--btn--primary',
            'data' => [
                'pjax' => 1,
                'livebox' => 1,
                'livebox-customparams' => '{"close":false}',
            ],
        ]) ?>
        <span class="dk--station--user-section__description"><?= _tF('contact the owner if station conflicts',
                'station') ?></span>
    <?php endif; ?>
</div>

<?php
$this->registerJs("
krajeeDialog.defaults.confirm.title = '" . _tF('confirm', 'station') . "' ;
krajeeDialog.defaults.confirm.btnCancelLabel = '" . _tF('no delete', 'station') . "';
krajeeDialog.defaults.confirm.btnOKLabel = '" . _tF('yes delete', 'station') . "';
");
?>

