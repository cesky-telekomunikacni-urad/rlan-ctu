<?php
/*
* Copyright 2023 DactylGroup s.r.o.
*
* Licensed under the EUPL, Version 1.2 or – as soon they
will be approved by the European Commission - subsequent
versions of the EUPL (the "Licence");
* You may not use this work except in compliance with the
Licence.
* You may obtain a copy of the Licence at:
*
* https://joinup.ec.europa.eu/software/page/eupl5
*
* Unless required by applicable law or agreed to in
writing, software distributed under the Licence is
distributed on an "AS IS" basis,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
express or implied.
* See the Licence for the specific language governing
permissions and limitations under the Licence.
*/
?>


<?php

use common\models\Station;
use dactylcore\core\helpers\Date;
use frontend\widgets\dactylkit\DactylKit;

/**
 * @var $model Station
 */

if (!isset($couple)) {
    $couple = false;
}

?>


<span class="dk--table--ghost__heading"><?= _tF('deadlines', 'station') ?></span>
<table class="dk--table dk--table--ghost">
  <tbody>
  <tr class="dk--table--tr__icon">
    <td class="dk--table--td__icon"><?= _tF('valid until', 'station') ?>
      <div <?= DactylKit::tooltip(_tF('valid until tooltip',
          'station')) ?>><?= DactylKit::icon(DactylKit::ICON_INFO_20) ?></div>
    </td>
    <td><?= $model->getDateTimeAttributeFormatted('valid_to', _t('date_format', 'station-detail')) ?></td>
      <?php if($couple): ?>
          <td><?= $model->stationPair->getDateTimeAttributeFormatted('valid_to', _t('date_format', 'station-detail')) ?></td>
      <?php endif; ?>

  </tr>
  <tr class="dk--table--tr__icon">
    <td class="dk--table--td__icon"><?= _tF('protected until', 'station') ?>
      <div <?= DactylKit::tooltip(_tF('protected until tooltip',
          'station')) ?>><?= DactylKit::icon(DactylKit::ICON_INFO_20) ?></div>
    </td>
    <td><?= $model->getDateTimeAttributeFormatted('protected_to', _t('date_format', 'station-detail')) ?></td>
      <?php if($couple): ?>
          <td><?= $model->stationPair->getDateTimeAttributeFormatted('protected_to', _t('date_format', 'station-detail')) ?></td>
      <?php endif; ?>
  </tr>
  </tbody>
</table>

<?php
if ($model->isForProlongation()):?>


    <?= $model->getIsExpired() ? DactylKit::alert(
        _tF('Station has expired', 'station'),
        _tF('You have to renew this station', 'station'),
        DactylKit::ALERT_TYPE_CRITICAL,
        true,
        null,
        false,
        [],
        $buttonLabel = _tF('prolong station', 'station'),
        $buttonUrl = url(['station/prolong-registration', 'id' => $model->id]),
        $buttonOptions = ['class' => 'dk--btn dk--btn--primary dk--btn--danger', 'data-livebox' => 1]
    ) : DactylKit::alert(
        _tF('Station is about to expire', 'station'),
        _tF('You can renew station validity and protection period.', 'station'),
        DactylKit::ALERT_TYPE_WARNING,
        true,
        null,
        false,
        [],
        $buttonLabel = _tF('prolong station', 'station'),
        $buttonUrl = url(['station/prolong-registration', 'id' => $model->id]),
        $buttonOptions = ['class' => 'dk--btn dk--btn--primary dk--btn--warning', 'data-livebox' => 1]
    ) ?>

<?php
endif;
?>
