<?php
/*
* Copyright 2023 DactylGroup s.r.o.
*
* Licensed under the EUPL, Version 1.2 or – as soon they
will be approved by the European Commission - subsequent
versions of the EUPL (the "Licence");
* You may not use this work except in compliance with the
Licence.
* You may obtain a copy of the Licence at:
*
* https://joinup.ec.europa.eu/software/page/eupl5
*
* Unless required by applicable law or agreed to in
writing, software distributed under the Licence is
distributed on an "AS IS" basis,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
express or implied.
* See the Licence for the specific language governing
permissions and limitations under the Licence.
*/
?>


<?php

use common\models\Station;
use frontend\widgets\dactylkit\DactylKit;

/**
 * @var $modelStationA Station
 * @var $modelStationB Station
 */

$dmsLngA = $modelStationA->DDtoDMS(false);
$dmsLatA = $modelStationA->DDtoDMS(true);

$dmsLngB = $modelStationB->DDtoDMS(false);
$dmsLatB = $modelStationB->DDtoDMS(true);

?>

<span class="dk--table--ghost__heading"><?= _tF('location', 'station') ?></span>
<table class="dk--table dk--table--ghost">
  <thead>
  <tr>
    <th class="dk--table--ghost__hide-mobile"></th>
    <th><?= _tF('Station A', 'station') ?></th>
    <th><?= _tF('Station B', 'station') ?></th>
  </tr>
  </thead>
  <tbody>
  <tr>
    <td class="dk--table--ghost__hide-mobile"><?= _tF('GPS', 'station') ?></td>
    <td class="pr-5">
      <span <?= DactylKit::tooltip("{$dmsLngA['deg']}° {$dmsLngA['min']}' " . round($dmsLngA['sec']) . "''") ?>>
          <?= $modelStationA->gpsFormatted(false) ?>
      </span>,
      <span <?= DactylKit::tooltip("{$dmsLatA['deg']}° {$dmsLatA['min']}' " . round($dmsLatA['sec']) . "''") ?>>
          <?= $modelStationA->gpsFormatted(true) ?>
      </span>
    </td>
    <td class="pr-5">
      <span <?= DactylKit::tooltip("{$dmsLngB['deg']}° {$dmsLngB['min']}' " . round($dmsLngB['sec']) . "''") ?>>
          <?= $modelStationB->gpsFormatted(false) ?>
      </span>,
      <span <?= DactylKit::tooltip("{$dmsLatB['deg']}° {$dmsLatB['min']}' " . round($dmsLatB['sec']) . "''") ?>>
          <?= $modelStationB->gpsFormatted(true) ?>
      </span>
    </td>
  </tr>
  </tbody>
</table>
