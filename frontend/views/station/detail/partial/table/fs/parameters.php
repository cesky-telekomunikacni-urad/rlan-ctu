<?php
/*
* Copyright 2023 DactylGroup s.r.o.
*
* Licensed under the EUPL, Version 1.2 or – as soon they
will be approved by the European Commission - subsequent
versions of the EUPL (the "Licence");
* You may not use this work except in compliance with the
Licence.
* You may obtain a copy of the Licence at:
*
* https://joinup.ec.europa.eu/software/page/eupl5
*
* Unless required by applicable law or agreed to in
writing, software distributed under the Licence is
distributed on an "AS IS" basis,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
express or implied.
* See the Licence for the specific language governing
permissions and limitations under the Licence.
*/
?>


<?php

use common\models\Station;
use common\models\StationFs;
use frontend\widgets\dactylkit\DactylKit;

/**
 * @var $modelStationA Station
 * @var $modelStationB Station
 * @var $modelStationAType StationFs
 * @var $modelStationBType StationFs
 */

$modelStationAType = $modelStationA->getStationTypeObject();
$modelStationBType = $modelStationB->getStationTypeObject();

?>

<span class="dk--table--ghost__heading"><?= _tF('parameters', 'station') ?></span>
<div class="dk--table--scroll-holder">
    <table class="dk--table dk--table--ghost">
        <thead>
        <tr>
            <th></th>
            <th><?= _tF('Station A', 'station') ?></th>
            <th><?= _tF('Station B', 'station') ?></th>
        </tr>
        </thead>
        <tbody>
        <tr>
            <td class="dk--table--td__icon"><?= _tF('antenna gain', 'station') ?><div <?= DactylKit::tooltip(_tF('antenna_volume',
                    'station_hint')) ?> ><?= DactylKit::icon(DactylKit::ICON_INFO_20) ?></div></td>
            <td><?= Station::formatNumber($modelStationA->antenna_volume) ?? '-' ?> dBi</td>
            <td><?= Station::formatNumber($modelStationB->antenna_volume) ?? '-' ?> dBi</td>
        </tr>
        <tr>
            <td class="dk--table--td__icon"><?= _tF('power_label', 'station') ?><div <?= DactylKit::tooltip(_tF('power',
                    'station_hint')) ?> ><?= DactylKit::icon(DactylKit::ICON_INFO_20) ?></div></td>
            <td><?= Station::formatNumber($modelStationA->power) ?? '-' ?> dBm</td>
            <td><?= Station::formatNumber($modelStationB->power) ?? '-' ?> dBm</td>
        </tr>
        <tr>
            <td class="dk--table--td__icon"><?= _tF('bandwidth', 'station') ?><div <?= DactylKit::tooltip(_tF('channel_width',
                    'station_hint')) ?> ><?= DactylKit::icon(DactylKit::ICON_INFO_20) ?></div></td>
            <td><?= Station::formatNumber($modelStationA->channel_width) ?? '-' ?> MHz</td>
            <td><?= Station::formatNumber($modelStationB->channel_width) ?? '-' ?> MHz</td>
        </tr>
        <tr>
            <td class="dk--table--td__icon"><?= _tF('frequency_label', 'station') ?><div <?= DactylKit::tooltip(_tF('frequency',
                    'station_hint')) ?> ><?= DactylKit::icon(DactylKit::ICON_INFO_20) ?></div></td>
            <td><?= Station::formatNumber($modelStationAType->frequency) ?? '-' ?> MHz</td>
            <td><?= Station::formatNumber($modelStationBType->frequency) ?? '-' ?> MHz</td>
        </tr>
        <tr>
            <td class="dk--table--td__icon"> <?= _tF('ratio_signal_interference_label', 'station') ?>
                <div <?= DactylKit::tooltip(_tF('ratio_signal_interference',
                    'station_hint')) ?> ><?= DactylKit::icon(DactylKit::ICON_INFO_20) ?></div></td>
            <td><?= StationFs::$qamValues[$modelStationAType->ratio_signal_interference] ?></td>
            <td><?= StationFs::$qamValues[$modelStationBType->ratio_signal_interference] ?></td>
        </tr>

        <?php if ($modelStationA->canEdit()): ?>
          <tr>
              <?php if ($modelStationA->hardware_identifier == Station::SERIAL_NUMBER) : ?>
                <td><?= $modelStationA->getAttributeLabel('serial_number') ?></td>
                <td><?= $modelStationA->serial_number ?? '' ?></td>
                <td><?= $modelStationB->serial_number ?? '' ?></td>
              <?php else: ?>
                <td><?= $modelStationA->getAttributeLabel('mac_address') ?></td>
                <td><?= $modelStationA->mac_address ?? '' ?></td>
                <td><?= $modelStationB->mac_address ?? '' ?></td>
              <?php endif; ?>
          </tr>
        <?php endif; ?>
        </tbody>
    </table>
</div>
