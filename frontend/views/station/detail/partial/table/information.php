<?php
/*
* Copyright 2023 DactylGroup s.r.o.
*
* Licensed under the EUPL, Version 1.2 or – as soon they
will be approved by the European Commission - subsequent
versions of the EUPL (the "Licence");
* You may not use this work except in compliance with the
Licence.
* You may obtain a copy of the Licence at:
*
* https://joinup.ec.europa.eu/software/page/eupl5
*
* Unless required by applicable law or agreed to in
writing, software distributed under the Licence is
distributed on an "AS IS" basis,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
express or implied.
* See the Licence for the specific language governing
permissions and limitations under the Licence.
*/
?>


<?php

/**
 * @var $model \common\models\Station
 */

use common\models\Station;
use frontend\controllers\StationController;
use frontend\widgets\dactylkit\DactylKit;

$modelType = $model->getStationTypeObject();
switch ($model->status) {
    case (Station::STATUS_EXPIRED):
    {
        $badge = DactylKit::badge(_tF('expired', 'station'), DactylKit::BADGE_WARNING);
        break;
    }
    case (Station::STATUS_FINISHED):
    {
        $badge = DactylKit::badge(_tF('active', 'station'), DactylKit::BADGE_SUCCESS);
        break;
    }
}

?>

<span class="dk--table--ghost__heading"><?= _tF('information', 'station') ?></span>
<table class="dk--table dk--table--ghost">
  <tbody>
  <tr>
    <td><?= _tF('kind', 'station') ?></td>
    <td><?= $model->type == StationController::TYPE_FS ? _tF('station type fs', 'station') :
            ($modelType->is_ptmp ?
                _tF('station type wigig ptmp', 'station') : _tF('station type wigig ptp', 'station')) ?></td>
  </tr>
  <tr>
    <td><?= _tF('status', 'station') ?></td>
    <td><?= $badge ?></td>
  </tr>
  </tbody>
</table>

