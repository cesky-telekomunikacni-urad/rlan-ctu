<?php
/*
* Copyright 2023 DactylGroup s.r.o.
*
* Licensed under the EUPL, Version 1.2 or – as soon they
will be approved by the European Commission - subsequent
versions of the EUPL (the "Licence");
* You may not use this work except in compliance with the
Licence.
* You may obtain a copy of the Licence at:
*
* https://joinup.ec.europa.eu/software/page/eupl5
*
* Unless required by applicable law or agreed to in
writing, software distributed under the Licence is
distributed on an "AS IS" basis,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
express or implied.
* See the Licence for the specific language governing
permissions and limitations under the Licence.
*/
?>


<?php

use common\models\Station;
use dactylcore\core\helpers\Date;
use frontend\widgets\dactylkit\DactylKit;
use yii\helpers\Html;

/**
 * @var $model Station
 */

if (!isset($couple)) {
    $couple = false;
}

?>

<span class="dk--table--ghost__heading"><?= _tF('details', 'station') ?></span>
<table class="dk--table dk--table--ghost">
    <tbody>
    <tr>
        <td><?= _tF('ID', 'station') ?></td>
        <td><?= $model->getFormattedId() ?></td>
        <?php if ($couple): ?>
            <td><?= $model->stationPair->getFormattedId() ?></td>
        <?php endif; ?>
    </tr>
    <tr>
        <td><?= _tF('owner', 'station') ?></td>
        <td class="dk--table--td__icon-mr">
            <?php if (hasAccessTo('app-frontend_station_update-all') && loggedUser()->id !== $model->id_user): ?>
                <?= Html::a(
                    DactylKit::icon(DactylKit::ICON_ACCOUNT) .
                    "<span>{$model->getUserIdPadded()}</span>",
                    url(['dc-user/user/user-index', 'id' => $model->id_user]),
                ); ?>
            <?php else: ?>
                <?= DactylKit::icon(DactylKit::ICON_ACCOUNT) ?>
                <span><?= $model->getUserIdPadded() ?></span>
            <?php endif; ?>
        </td>
        <?php if ($couple): ?>
            <td class="dk--table--td__icon-mr">
                <?= DactylKit::icon(DactylKit::ICON_ACCOUNT) ?>
                <span><?= $model->stationPair->getUserIdPadded() ?></span></td>
        <?php endif; ?>

    </tr>
    <tr>
        <td><?= _tF('modified', 'station') ?></td>
        <td><?= $model->getDateTimeAttributeFormatted('updated_at', _t('date_format', 'station-detail')) ?></td>
        <?php if ($couple): ?>
            <td><?= $model->stationPair->getDateTimeAttributeFormatted('updated_at', _t('date_format', 'station-detail')) ?></td>
        <?php endif; ?>
    </tr>
    <tr>
        <td><?= _tF('created', 'station') ?></td>
        <td><?= $model->getDateTimeAttributeFormatted('created_at', _t('date_format', 'station-detail')) ?></td>
        <?php if ($couple): ?>
            <td><?= $model->stationPair->getDateTimeAttributeFormatted('created_at', _t('date_format', 'station-detail')) ?></td>
        <?php endif; ?>
    </tr>
    </tbody>
</table>
