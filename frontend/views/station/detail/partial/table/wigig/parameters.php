<?php
/*
* Copyright 2023 DactylGroup s.r.o.
*
* Licensed under the EUPL, Version 1.2 or – as soon they
will be approved by the European Commission - subsequent
versions of the EUPL (the "Licence");
* You may not use this work except in compliance with the
Licence.
* You may obtain a copy of the Licence at:
*
* https://joinup.ec.europa.eu/software/page/eupl5
*
* Unless required by applicable law or agreed to in
writing, software distributed under the Licence is
distributed on an "AS IS" basis,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
express or implied.
* See the Licence for the specific language governing
permissions and limitations under the Licence.
*/
?>


<?php

use common\models\Station;
use common\models\StationWigig;
use frontend\widgets\dactylkit\DactylKit;

/**
 * @var $model Station
 */

$modelWigig = $model->getStationTypeObject();
$modelLabels = $model->attributeLabels();

if (!isset($couple)) {
    $couple = false;
}

?>

<span class="dk--table--ghost__heading"><?= _tF('parameters', 'station') ?></span>
<div class="dk--table--scroll-holder">
  <table class="dk--table dk--table--ghost">
    <tbody>
    <?php if ($couple): ?>
      <thead>
      <tr>
          <th></th>
          <th></th>
          <th><?= _tF('pair_station', 'station') ?></th>
      </tr>
      </thead>
      <?php endif; ?>
      <tr class="dk--table--tr__icon">
      <td class="dk--table--td__icon"><?= _tF('antenna gain', 'station') ?>
        <span <?= DactylKit::tooltip(_tF('antenna_volume',
            'station_hint')) ?> ><?= DactylKit::icon(DactylKit::ICON_INFO_20) ?></span></td>
      <td><?= $model->antenna_volume != '' ? Station::formatNumber($model->antenna_volume) : '-' ?> dBi</td>
          <?php if ($couple): ?>
              <td><?= $model->stationPair->antenna_volume != '' ?
                      Station::formatNumber($model->stationPair->antenna_volume) : '-' ?> dBi
              </td>
          <?php endif; ?>
      </tr>
    <tr class="dk--table--tr__icon">
      <td class="dk--table--td__icon"><?= _tF('power_label', 'station') ?><span <?= DactylKit::tooltip(_tF('power',
              'station_hint')) ?> ><?= DactylKit::icon(DactylKit::ICON_INFO_20) ?></span></td>
      <td><?= $model->power != '' ? Station::formatNumber($model->power) : '-' ?> dBm</td>
        <?php if ($couple): ?>
            <td><?= $model->stationPair->power != '' ? Station::formatNumber($model->stationPair->power) : '-' ?> dBm</td>
        <?php endif; ?>
    </tr>
    <tr class="dk--table--tr__icon">
      <td class="dk--table--td__icon"><?= _tF('eirp_label', 'station') ?><span <?= DactylKit::tooltip(_tF('eirp',
              'station_hint')) ?> ><?= DactylKit::icon(DactylKit::ICON_INFO_20) ?></span></td>
      <td><?= $modelWigig->eirp != '' ? Station::formatNumber($modelWigig->eirp) : '-' ?> dBm</td>
        <?php if ($couple): ?>
            <td><?= $modelWigig->station->stationPair->getStationTypeObject()->eirp != '' ?
                    Station::formatNumber($modelWigig->station->stationPair->getStationTypeObject()->eirp) : '-' ?>
                dBm
            </td>
        <?php endif; ?>
    </tr>
    <tr class="dk--table--tr__icon">
      <td class="dk--table--td__icon"><?= _tF('bandwidth', 'station') ?>
        <span <?= DactylKit::tooltip(_tF('channel_width',
            'station_hint')) ?> ><?= DactylKit::icon(DactylKit::ICON_INFO_20) ?></span></td>
      <td><?= $model->channel_width ? Station::formatNumber($model->channel_width) : '-' ?> MHz</td>
        <?php if ($couple): ?>
            <td><?= $model->stationPair->channel_width ? Station::formatNumber($model->stationPair->channel_width) :
                    '-' ?> MHz
            </td>
        <?php endif; ?>
    </tr>
    <?php if ($model->canEdit()): ?>
      <tr>
          <td><?= _tF('mac_or_serial', 'station') ?></td>
          <?php if ($model->hardware_identifier == Station::SERIAL_NUMBER) : ?>
            <td><?= $model->serial_number ?? '' ?></td>
          <?php else: ?>
            <td><?= $model->mac_address ?? '' ?></td>
          <?php endif; ?>

          <?php if ($couple && $model->stationPair->canEdit()): ?>
              <?php if ($model->stationPair->hardware_identifier == Station::SERIAL_NUMBER) : ?>
                  <td><?= $model->stationPair->serial_number ?? '' ?></td>
              <?php else: ?>
                  <td><?= $model->stationPair->mac_address ?? '' ?></td>
              <?php endif; ?>
          <?php endif; ?>
      </tr>
    <?php endif; ?>
    </tbody>
  </table>
</div>