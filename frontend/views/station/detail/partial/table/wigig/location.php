<?php
/*
* Copyright 2023 DactylGroup s.r.o.
*
* Licensed under the EUPL, Version 1.2 or – as soon they
will be approved by the European Commission - subsequent
versions of the EUPL (the "Licence");
* You may not use this work except in compliance with the
Licence.
* You may obtain a copy of the Licence at:
*
* https://joinup.ec.europa.eu/software/page/eupl5
*
* Unless required by applicable law or agreed to in
writing, software distributed under the Licence is
distributed on an "AS IS" basis,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
express or implied.
* See the Licence for the specific language governing
permissions and limitations under the Licence.
*/
?>


<?php

use common\models\Station;
use frontend\widgets\dactylkit\DactylKit;

/**
 * @var $model Station
 */

$dmsLat = $model->DDtoDMS(true);
$dmsLng = $model->DDtoDMS(false);

if (!isset($couple)) {
    $couple = false;
}

if ($couple) {
    $dmsLatC = $model->stationPair->DDtoDMS(true);
    $dmsLngC = $model->stationPair->DDtoDMS(false);
}

?>

<span class="dk--table--ghost__heading"><?= _tF('location', 'station') ?></span>
<table class="dk--table dk--table--ghost">
  <tbody>
  <tr>
    <td><?= _tF('GPS', 'station') ?></td>
    <td>
          <span <?= DactylKit::tooltip("{$dmsLng['deg']}° {$dmsLng['min']}' " . round($dmsLng['sec']) . "''") ?>>
          <?= $model->gpsFormatted(false) ?>
      </span>,
      <span <?= DactylKit::tooltip("{$dmsLat['deg']}° {$dmsLat['min']}' " . round($dmsLat['sec']) . "''") ?>>
          <?= $model->gpsFormatted(true) ?>
      </span>
    </td>
      <?php if ($couple): ?>
          <td>
          <span <?= DactylKit::tooltip("{$dmsLngC['deg']}° {$dmsLngC['min']}' " . round($dmsLngC['sec']) . "''") ?>>
          <?= $model->stationPair->gpsFormatted(false) ?>
      </span>,
              <span <?= DactylKit::tooltip("{$dmsLatC['deg']}° {$dmsLatC['min']}' " . round($dmsLatC['sec']) . "''") ?>>
          <?= $model->stationPair->gpsFormatted(true) ?>
      </span>
          </td>
      <?php endif; ?>
  </tr>
  <tr>
      <td><?= _tF('direction', 'station') ?></td>
      <td>
        <span style="margin-left: 8px;">
          <?= $model->getStationTypeObject()->direction ?>°
        </span>
      </td>
      <?php if ($couple): ?>
          <td>
        <span style="margin-left: 8px;">
         <?= $model->stationPair->getStationTypeObject()->direction ?>°
        </span>
          </td>
      <?php endif; ?>
  </tr>
  </tbody>
</table>
