<?php
/*
* Copyright 2023 DactylGroup s.r.o.
*
* Licensed under the EUPL, Version 1.2 or – as soon they
will be approved by the European Commission - subsequent
versions of the EUPL (the "Licence");
* You may not use this work except in compliance with the
Licence.
* You may obtain a copy of the Licence at:
*
* https://joinup.ec.europa.eu/software/page/eupl5
*
* Unless required by applicable law or agreed to in
writing, software distributed under the Licence is
distributed on an "AS IS" basis,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
express or implied.
* See the Licence for the specific language governing
permissions and limitations under the Licence.
*/
?>


<?php

use common\models\Station;
use dactylcore\core\widgets\admin\gridview\GridView;
use dactylcore\core\widgets\common\bootstrap\Html as BootHtml;
use frontend\widgets\dactylkit\DactylKit;
use yii\helpers\Html;

/**
 * @var $dataProvider
 * @var $searchModel
 * @var $model Station
 * @var $startedConversationsWith array
 * @var $bData array
 * @var $showFilter bool
 * @var $canEdit bool
 */

\frontend\assets\CoordinationCalcAsset::register($this);

$time = time();
$newStationType = $model->type;
$randomString = Yii::$app->security->generateRandomString(8);
$firstColumn = [];
if ($model->isFs()) {
    $columns = [
        [
            'class' => kartik\grid\ExpandRowColumn::class,
            'expandIcon' => DactylKit::icon(DactylKit::ICON_CHEVRON_DOWN),
            'collapseIcon' => DactylKit::icon('@frontend/web/source_assets/img/icon/ic-chevron-up-20.svg'),
            'value' => function ($model, $key, $index, $column)
            {
                return GridView::ROW_COLLAPSED;
            },
            'headerOptions' => ['class' => 'expand-icon-header'],
            'contentOptions' => [
                'data-toggle' => 'tooltip',
                'data-placement' => 'top',
            ],
            'expandAllTitle' => _tF('show all info', 'station'),
            'expandTitle' => _tF('show info', 'station'),
            'collapseTitle' => _tF('hide info', 'station'),
            'collapseAllTitle' => _tF('hide all info', 'station'),

            'detail' => function ($model, $key, $index, $column) use ($bData, $newStationType)
            {

                return Yii::$app->controller->renderPartial('station/partial/fs/collision.php', [
                    'modelA' => $model,
                    'modelB' => $bData[$model->id],
                    'newStationType' => $newStationType,
                ]);
            },
        ],
        [
            'attribute' => 'id',
            'label' => _tF('id', 'station'),
            'value' => function (Station $model)
            {
                return $model->formatThisId();
            },

        ],
        [
            'attribute' => 'typeShortCut',
            'label' => _tF('typeShortCut', 'station'),
            'filter' => \dactylcore\core\widgets\admin\select2\Select2::widget([
                'model' => $searchModel,
                'options' => ['id' => "typeShortCutId{$randomString}"],
                'attribute' => 'typeShortCut',
                'pluginOptions' => [
                    'allowClear' => true,
                    'placeholder' => ' ',
                ],
                'data' => [
                    'fs' => _tF('type_fs', 'station'),
                    'wigig_ptp' => _tF('type_wigig_ptp', 'station'),
                    'wigig_ptmp' => _tF('type_wigig_ptmp', 'station'),
                ],
            ]),
        ],
        [
            'attribute' => 'name',
            'label' => _tF('name', 'station'),
            'format' => 'raw',
            'value' => function ($model)
            {
                /**
                 * @var $model Station
                 */
                if ($model->isFs()) {
                    $stationSearch = new \common\models\search\StationSearch();
                    $pair = $stationSearch->getModelsFs($model->id);
                    // determine pair position based on ID's -> station A has always smaller ID
                    $stationAId = $pair->stationA->id < $pair->stationB->id ? $pair->stationA->id : $pair->stationB->id;

                    $img = $stationAId == $model->id ?
                        DactylKit::icon('@frontend/web/source_assets/img/icon/ic-position-a.svg') :
                        DactylKit::icon('@frontend/web/source_assets/img/icon/ic-position-b.svg');

                    $linkText = $model->name . $img;

                    return Html::a($linkText, ['station/station', 'id' => $model->id],
                        ['target' => '_blank', 'data-pjax' => "0"]);
                } else {
                    return Html::a($model->name, ['station/station', 'id' => $model->id],
                        ['target' => '_blank', 'data-pjax' => "0"]);
                }
            },

        ],
        [
            'attribute' => 'published_by',
            'label' => _tF('published by'),
            'format' => 'html',
            'visible' => (isset($canEdit) && $canEdit),
            'value' => function (Station $model)
            {
                if ($model->isFs() && !$model->isMaster()) {
                    $published = $model->getPairStation()->published_by;
                } else {
                    $published = $model->published_by;
                }
                return $published == Station::PUBLISHED_BY_PUBLICATION ?
                    '<span class="tooltip-selector" title="' .
                    _tF('standard_publish', 'station') .
                    '" data-placement="top"><i class="material-icons">assignment_turned_in</i></span>' :
                    '<span class="tooltip-selector" title="' .
                    _tF('declared_publish', 'station') .
                    '" data-placement="top"><i class="material-icons">assignment_late</i></span>';
            },
            'filter' => \dactylcore\core\widgets\admin\select2\Select2::widget([
                'model' => $searchModel,
                'attribute' => 'published_by',
                'pluginOptions' => [
                    'allowClear' => true,
                    'placeholder' => '',
                ],
                'data' => [
                    Station::PUBLISHED_BY_DECLARATION => _tF('isolation', 'station'),
                    Station::PUBLISHED_BY_PUBLICATION => _tF('publication', 'station'),
                ],
            ]),
        ],
        [
            'attribute' => 'hasConflicts',
            'label' => _tF('hasConflicts', 'station'),
            'format' => 'html',
            'value' => function (Station $model) use ($bData, $newStationType)
            {
                if ($bData[$model->id]->showInterference($newStationType, $bData[$model->id]->type, 'general') &&
                    $model->showInterference($newStationType, $model->type, 'general')) {
                    $positive = false;
                    if ($model->hasConflicts || $bData[$model->id]->hasConflicts) {
                        $icon = 'ic-warning-20.svg';
                    } else {
                        $positive = true;
                        $icon = 'ic-check-circle-20.svg';
                    }
                    $helpMsg = _tF($positive ? 'hasConflictsHelpPositive' : 'hasConflictsHelpNegative', 'station');
                } else {
                    $icon = 'ic-circle-current-20.svg';
                    $helpMsg = _tF('cannotConflict', 'station');
                }

                return "<span class='tooltip-selector' title='{$helpMsg}' data-placement='top'><img src='/img/icon/{$icon}'  alt='{$helpMsg}'></span>";
            },
            'headerOptions' => [
                'title' => _tF('hasConflictsHelp', 'station'),
                'data-toggle' => 'tooltip',
                'data-placement' => 'top',
            ],
            'filter' => \dactylcore\core\widgets\admin\select2\Select2::widget([
                'model' => $searchModel,
                'options' => ['id' => "hasConflictsId{$randomString}"],
                'attribute' => 'hasConflicts',
                'pluginOptions' => [
                    'allowClear' => true,
                    'placeholder' => '',
                ],
                'data' => ['yes' => _tF('yes', 'station'), 'no' => _tF('no', 'station'),],
            ]),
        ],
        [
            'format' => 'raw',
            'value' => function (Station $thisModel) use ($model, $startedConversationsWith)
            {
                if ($model->isWaiting() &&
                    $thisModel->hasConflicts &&
                    $thisModel->id_user != $model->id_user &&
                    $model->isMyStation()) {
                    $title = in_array($thisModel->id, $startedConversationsWith) ?
                        _tF('open discussion disturbance', 'messaging') : _tF('contact owner disturbance', 'messaging');
                    $icon = in_array($thisModel->id, $startedConversationsWith) ? 'message' : 'send';
                    return \yii\bootstrap\Html::a($icon,
                        ['messaging/disturbance-question', 'idAggressor' => $thisModel->id, 'idVictim' => $model->id],
                        [
                            'class' => 'material-icons',
                            'data' => [
                                'livebox' => true,
                            ],
                            'title' => $title,
                            'data-toggle' => 'tooltip',
                            'data-placement' => 'top',
                        ]);
                } else {
                    if ( userIsLogged() && loggedUser()->getId() == $thisModel->id_user) {
                        $tooltip = DactylKit::tooltip(_tF('this station is yours', 'station'));
                        return "<i {$tooltip} class=\"material-icons \"> person</i>";
                    }
                    return '';
                }
            },
        ],
    ];
} else {
    // wigig
    $columns = [
        [
            'class' => kartik\grid\ExpandRowColumn::class,
            'expandIcon' => DactylKit::icon(DactylKit::ICON_CHEVRON_DOWN),
            'collapseIcon' => DactylKit::icon('@frontend/web/source_assets/img/icon/ic-chevron-up-20.svg'),
            'value' => function ($model, $key, $index, $column)
            {
                return GridView::ROW_COLLAPSED;
            },
            'headerOptions' => ['class' => 'expand-icon-header'],
            'contentOptions' => [
                'data-toggle' => 'tooltip',
                'data-placement' => 'top',
            ],
            'expandAllTitle' => _tF('show all info', 'station'),
            'expandTitle' => _tF('show info', 'station'),
            'collapseTitle' => _tF('hide info', 'station'),
            'collapseAllTitle' => _tF('hide all info', 'station'),

            'detail' => function ($model, $key, $index, $column) use ($bData, $newStationType)
            {

                return Yii::$app->controller->renderPartial('station/partial/wigig/collision.php', [
                    'model' => $model,
                    'newStationType' => $newStationType,
                ]);
            },
        ],
        [
            'attribute' => 'id',
            'label' => _tF('id', 'station'),
            'value' => function ($model)
            {
                return $model->formatThisId();
            },

        ],
        [
            'attribute' => 'typeShortCut',
            'label' => _tF('typeShortCut', 'station'),
            'filter' => \dactylcore\core\widgets\admin\select2\Select2::widget([
                'model' => $searchModel,
                'attribute' => 'typeShortCut',
                'options' => ['id' => "typeShortCutId{$randomString}"],
                'pluginOptions' => [
                    'allowClear' => true,
                    'placeholder' => '',
                ],
                'data' => [
                    'fs' => _tF('type_fs', 'station'),
                    'wigig_ptp' => _tF('type_wigig_ptp', 'station'),
                    'wigig_ptmp' => _tF('type_wigig_ptmp', 'station'),
                ],
            ]),
        ],
        [
            'attribute' => 'name',
            'label' => _tF('name', 'station'),
            'format' => 'raw',
            'value' => function ($model)
            {
                /**
                 * @var $model Station
                 */
                if ($model->isFs()) {
                    $stationSearch = new \common\models\search\StationSearch();
                    $pair = $stationSearch->getModelsFs($model->id);
                    // determine pair position based on ID's -> station A has always smaller ID
                    $stationAId = $pair->stationA->id < $pair->stationB->id ? $pair->stationA->id : $pair->stationB->id;

                    $img = $stationAId == $model->id ?
                        DactylKit::icon('@frontend/web/source_assets/img/icon/ic-position-a.svg') :
                        DactylKit::icon('@frontend/web/source_assets/img/icon/ic-position-b.svg');

                    $linkText = $model->name . $img;

                    return Html::a($linkText, ['station/station', 'id' => $model->id],
                        ['target' => '_blank', 'data-pjax' => "0"]);
                } else {
                    return Html::a($model->name, ['station/station', 'id' => $model->id],
                        ['target' => '_blank', 'data-pjax' => "0"]);
                }
            },

        ],
        [
            'attribute' => 'published_by',
            'label' => _tF('published by'),
            'format' => 'html',
            'visible' => (isset($canEdit) && $canEdit),
            'value' => function (Station $model)
            {
                if ($model->isFs() && !$model->isMaster()) {
                    $published = $model->getPairStation()->published_by;
                } else {
                    $published = $model->published_by;
                }
                return $published == Station::PUBLISHED_BY_PUBLICATION ?
                    '<span class="tooltip-selector" title="' .
                    _tF('standard_publish', 'station') .
                    '" data-placement="top"><i class="material-icons">assignment_turned_in</i></span>' :
                    '<span class="tooltip-selector" title="' .
                    _tF('declared_publish', 'station') .
                    '" data-placement="top"><i class="material-icons">assignment_late</i></span>';
            },
            'filter' => \dactylcore\core\widgets\admin\select2\Select2::widget([
                'model' => $searchModel,
                'attribute' => 'published_by',
                'pluginOptions' => [
                    'allowClear' => true,
                    'placeholder' => '',
                ],
                'data' => [
                    Station::PUBLISHED_BY_PUBLICATION => _tF('publication', 'station'),
                    Station::PUBLISHED_BY_DECLARATION => _tF('isolation', 'station'),
                ],
            ]),
        ],
        [
            'attribute' => 'hasConflicts',
            'label' => _tF('hasConflicts', 'station'),
            'format' => 'html',
            'value' => function (Station $model) use ($newStationType)
            {
                if ($model->showInterference($newStationType, $model->type, 'general')) {
                    $positive = false;
                    if ($model->hasConflicts) {
                        $icon = 'ic-warning-20.svg';
                    } else {
                        $positive = true;
                        $icon = 'ic-check-circle-20.svg';
                    }
                    $helpMsg = _tF($positive ? 'hasConflictsHelpPositive' : 'hasConflictsHelpNegative', 'station');
                } else {
                    $icon = 'ic-circle-current-20.svg';
                    $helpMsg = _tF('cannotConflict', 'station');
                }

                return "<span class='tooltip-selector' title='{$helpMsg}' data-placement='top'><img src='/img/icon/{$icon}'  alt='{$helpMsg}'></span>";
            },
            'headerOptions' => [
                'title' => _tF('hasConflictsHelp', 'station'),
                'data-toggle' => 'tooltip',
                'data-placement' => 'top',
            ],
            'filter' => \dactylcore\core\widgets\admin\select2\Select2::widget([
                'model' => $searchModel,
                'options' => ['id' => "hasConflictsId{$randomString}"],
                'attribute' => 'hasConflicts',
                'pluginOptions' => [
                    'allowClear' => true,
                    'placeholder' => '',
                ],
                'data' => ['yes' => _tF('yes', 'station'), 'no' => _tF('no', 'station'),],
            ]),
        ],
        [
            'format' => 'html',
            'value' => function (Station $thisModel) use ($model, $startedConversationsWith)
            {
                if ($model->isWaiting() &&
                    $thisModel->hasConflicts &&
                    $thisModel->id_user != $model->id_user &&
                    $model->isMyStation()) {
                    $title = in_array($thisModel->id, $startedConversationsWith) ?
                        _tF('open discussion disturbance', 'messaging') : _tF('contact owner disturbance', 'messaging');
                    $icon = in_array($thisModel->id, $startedConversationsWith) ? 'message' : 'send';
                    return \yii\bootstrap\Html::a($icon,
                        ['messaging/disturbance-question', 'idAggressor' => $thisModel->id, 'idVictim' => $model->id],
                        [
                            'class' => 'material-icons',
                            'data' => [
                                'livebox' => true,
                                'livebox-method' => 'post',
                            ],
                            'title' => $title,
                            'data-toggle' => 'tooltip',
                            'data-placement' => 'top',
                        ]);
                } else {
                    if ( userIsLogged() && loggedUser()->getId() == $thisModel->id_user) {
                        $tooltip = DactylKit::tooltip(_tF('this station is yours', 'station'));
                        return "<i {$tooltip} class=\"material-icons \"> person</i>";
                    }
                    return '';
                }
            },
        ],
    ];
}
?>

<div id="pjax-loader" class="stationList">
    <div id="loading">
        <div class="stations-table-loading"><?= _tF('table_loading', 'stations-table') ?></div>
    </div>
    <?= \dactylcore\core\widgets\common\gridview\GridView::widget([
        'dataProvider' => $dataProvider,
        'pjax' => false,
        'filterModel' => $showFilter ? $searchModel : null,
        'options' => [
            'class' => 'grid-view coordination-calc',
            'id' => "grid-{$randomString}",
        ],
        'layout' => '<div>{items}</div><div>{pager}{summary}</div>',
        'summary' => '<div class="summary">' . _tF('coordination calculator summary', 'station') . '</div>',
        'columns' => $columns,
        'pager' => [
            'nextPageLabel' => DactylKit::icon(DactylKit::ICON_CHEVRON_RIGHT),
            'prevPageLabel' => DactylKit::icon(DactylKit::ICON_CHEVRON_LEFT),
        ],
    ]) ?>
</div>