<?php
/*
* Copyright 2023 DactylGroup s.r.o.
*
* Licensed under the EUPL, Version 1.2 or – as soon they
will be approved by the European Commission - subsequent
versions of the EUPL (the "Licence");
* You may not use this work except in compliance with the
Licence.
* You may obtain a copy of the Licence at:
*
* https://joinup.ec.europa.eu/software/page/eupl5
*
* Unless required by applicable law or agreed to in
writing, software distributed under the Licence is
distributed on an "AS IS" basis,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
express or implied.
* See the Licence for the specific language governing
permissions and limitations under the Licence.
*/
?>


<?php

use dactylcore\core\helpers\Html;
use frontend\widgets\dactylkit\DactylKit; ?>

<table class="dk--table dk--table--ghost collision-table">
  <tbody>
  <tr>
    <td>
        <?php
        if ($model->showInterference($newStationType, $model->type, 'general')) {
            // positive = it is ok
            $positive = false;
            if ($model->hasConflicts) {
                $class = 'dk--conflict__yes';
                $text = _tF('yes', 'station');
            } else {
                $positive = true;
                $class = 'dk--conflict__no';
                $text = _tF('no', 'station');
            }

            $helpMsg = _tF($positive ? 'hasConflictsHelpPositive' : 'hasConflictsHelpNegative', 'station');
        } else {
            $helpMsg = _tF('cannotConflict', 'station');
            $class = 'dk--conflict__no';
            $text = _tF('no', 'station');
        }
        ?>
      <span class="dk--conflict__label "><?= _tF('conflict', 'station') ?></span>
      <span class="dk--conflict <?= $class ?>" <?= DactylKit::tooltip($helpMsg) ?> ><?= $text ?></span>
    </td>
    <td>
        <?php $class = '';
        if ($model->showInterference($newStationType, $model->type, 'passive')) {
            // positive = it is ok
            $helpMsg = '';
            if ($model->passiveIsolationIncreaseRequested == 0) {
                $val = _tF('no', 'station');
                $class = 'dk--conflict__no';
                $helpMsg = _tF('passive not interference', 'station');
            } else {
                $interferenceVal = round($model->passiveIsolationIncreaseRequested, 1);

                switch (true) {
                    case  (is_infinite($interferenceVal) || is_nan($interferenceVal)):
                    {
                        $interferenceVal = "~";
                        $class = 'dk--conflict__yes';
                        $val = _tF('interferes', 'station', ['val' => $interferenceVal]);

                        $helpMsg = _tF('passive interference by {amount}', 'station', ['amount' => $interferenceVal]);
                        break;
                    }
                    case $interferenceVal < -110:
                    {
                        $class = 'dk--conflict__no';
                        $val = _tF('no', 'station');
                        $helpMsg = _tF('passive not interference', 'station');

                        break;
                    }
                    case $interferenceVal <= 0:
                    {
                        $class = 'dk--conflict__no';
                        $val = _tF('not interferes', 'station', ['val' => $interferenceVal]);
                        $helpMsg = _tF('passive not interference with {reserve}', 'station', ['reserve' => $interferenceVal]);

                        break;
                    }
                    case $interferenceVal > 0:
                    {
                        $class = 'dk--conflict__yes';
                        $val = _tF('interferes', 'station', ['val' => $interferenceVal]);
                        $helpMsg = _tF('passive interference by {amount}', 'station', ['amount' => $interferenceVal]);

                        break;
                    }
                }
            }
        } else {
            $helpMsg = _tF('cannotPassive', 'station');
            $val = _tF('no', 'station');
            $class = 'dk--conflict__no';
        }

        $out = "<span class='{$class}' " . DactylKit::tooltip($helpMsg) . ">{$val}</span>";
        ?>
      <span class="dk--conflict__label "><?= _tF('passive interference', 'station') ?></span>
        <?= $out ?>
    </td>
    <td>
        <?php
        $class = '';
        $helpMsg = '';
        if ($model->showInterference($newStationType, $model->type, 'active')) {
            // positive = it is ok
            $positive = false;
            if ($model->activeIsolationIncreaseRequested == 0) {
                $val = _tF('no', 'station');
                $class = 'dk--conflict__no';
                $helpMsg = _tF('active not interference', 'station');
            } else {
                $interferenceVal = round($model->activeIsolationIncreaseRequested, 1);
                switch (true) {
                    case  (is_infinite($interferenceVal) || is_nan($interferenceVal)):
                    {
                        $interferenceVal = "~";
                        $class = 'dk--conflict__yes';
                        $val = _tF('interferes', 'station', ['val' => $interferenceVal]);
                        $helpMsg = _tF('active interference by {amount}', 'station', ['amount' => $interferenceVal]);

                        break;
                    }
                    case $interferenceVal < -110:
                    {
                        $class = 'dk--conflict__no';
                        $val = _tF('no', 'station');
                        $helpMsg = _tF('active not interference', 'station');

                        break;
                    }
                    case $interferenceVal <= 0:
                    {
                        $class = 'dk--conflict__no';
                        $val = _tF('not interferes', 'station', ['val' => $interferenceVal]);
                        $helpMsg = _tF('active not interference with {reserve}', 'station', ['reserve' => $interferenceVal]);

                        break;
                    }
                    case $interferenceVal > 0:
                    {
                        $class = 'dk--conflict__yes';
                        $val = _tF('interferes', 'station', ['val' => $interferenceVal]);
                        $helpMsg = _tF('active interference by {amount}', 'station', ['amount' => $interferenceVal]);

                        break;
                    }
                }
            }
        } else {
            $helpMsg = _tF('cannotActive', 'station');
            $val = _tF('no', 'station');
            $class = 'dk--conflict__no';
        }

        $out = "<span class='{$class}' " . DactylKit::tooltip($helpMsg) . ">{$val}</span>";
        ?>
      <span class="dk--conflict__label "><?= _tF('active interference', 'station') ?></span>
        <?= $out ?>
    </td>
  </tr>
  </tbody>
</table>
