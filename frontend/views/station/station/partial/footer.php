<?php
/*
* Copyright 2023 DactylGroup s.r.o.
*
* Licensed under the EUPL, Version 1.2 or – as soon they
will be approved by the European Commission - subsequent
versions of the EUPL (the "Licence");
* You may not use this work except in compliance with the
Licence.
* You may obtain a copy of the Licence at:
*
* https://joinup.ec.europa.eu/software/page/eupl5
*
* Unless required by applicable law or agreed to in
writing, software distributed under the Licence is
distributed on an "AS IS" basis,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
express or implied.
* See the Licence for the specific language governing
permissions and limitations under the Licence.
*/
?>


<?php

/**
 * @var $prevUrl string
 * @var $nextBtnLabel string
 * @var $dirtyNextBtnLabel string
 * @var $continueBtn bool
 */

use frontend\widgets\dactylkit\DactylKit;

?>

<div class="livebox--station-registration--footer">
    <?= $prevUrl != '' ?
        DactylKit::link('<span class="dk--btn__icon">' . DactylKit::icon(DactylKit::ICON_CHEVRON_LEFT) . '</span>
                    <span class="dk--btn__label">' . _tF('back', 'station') . '</span>',
            $prevUrl, DactylKit::LINK_TYPE_TERTIARY,
            DactylKit::LINK_SIZE_NORMAL, '', '', [
                'class' => 'dk--btn dk--btn--secondary',
                'data' => [
                    'pjax' => "1",
                    'pjax-push-state' => "0"
                ],
            ]) : '' ?>
    <?= $continueBtn ? DactylKit::button(_tF('continue', 'station'),
        DactylKit::BUTTON_TYPE_PRIMARY,
        '',
        '',
        DactylKit::ICON_CHEVRON_RIGHT,
        [
            'class' => 'loading-button next-step-button',
        ]
    ) : '' ?>
    <?= $nextBtnLabel != '' ? DactylKit::button($nextBtnLabel,
        DactylKit::BUTTON_TYPE_PRIMARY,
        '',
        '',
        DactylKit::ICON_CHEVRON_RIGHT,
        [
            'class' => $continueBtn ? 'loading-button next-step-save-button next-step-hidden-button' : 'loading-button next-step-save-button',
        ]
    ) : '' ?>
    <?= $dirtyNextBtnLabel != '' ? DactylKit::button($dirtyNextBtnLabel,
        DactylKit::BUTTON_TYPE_PRIMARY,
        '',
        '',
        DactylKit::ICON_CHEVRON_RIGHT,
        [
            'class' => 'loading-button next-step-dirty-button next-step-hidden-button',
            'data' => [
                'confirm-text' => _tF('continue', 'dirty-form'),
                'cancel-text' => _tF('cancel', 'dirty-form'),
                'message-text' => _tF('This will un-publish your station', 'dirty-form'),
            ],
        ]
    ) : '' ?>
</div>
<script>
    var options = [
        {
            selector: '.livebox--station-registration', // Mandatory, CSS selector
            vh: 100,  // Mandatory, height in vh units
        },
    ];
    var vhFix = new VHChromeFix(options);

    $("#station-form").submit(function() {
        $(".loading-button").find('.dk--btn__label').html('<div class="livebox-loading next-loading" data-fadespeed="150" data-color="#003782"><div class="loader"><svg class="circular" viewBox="25 25 50 50"><circle class="path" cx="50" cy="50" r="20" fill="none" stroke-width="2" stroke-miterlimit="10"></circle> </svg></div></div>');
    });
</script>
