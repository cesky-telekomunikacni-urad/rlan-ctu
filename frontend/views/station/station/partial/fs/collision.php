<?php
/*
* Copyright 2023 DactylGroup s.r.o.
*
* Licensed under the EUPL, Version 1.2 or – as soon they
will be approved by the European Commission - subsequent
versions of the EUPL (the "Licence");
* You may not use this work except in compliance with the
Licence.
* You may obtain a copy of the Licence at:
*
* https://joinup.ec.europa.eu/software/page/eupl5
*
* Unless required by applicable law or agreed to in
writing, software distributed under the Licence is
distributed on an "AS IS" basis,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
express or implied.
* See the Licence for the specific language governing
permissions and limitations under the Licence.
*/
?>


<?php

use common\models\Station;
use dactylcore\core\widgets\common\bootstrap\Html as BootHtml;
use frontend\widgets\dactylkit\DactylKit;
use yii\helpers\Html;

/**
 * @var $modelA Station
 * @var $modelB Station
 */
?>
<table class="dk--table dk--table--ghost collision-table">
  <tbody>
  <tr>
    <td><?= _tF('station a', 'station') ?></td>
    <td>
        <?php
        $class = '';
        $helpMsg = 'aa';

        if ($modelA->showInterference($newStationType, $modelA->type, 'general')) {
            // positive = it is ok
            $positive = false;
            if ($modelA->hasConflicts) {
                $class = 'dk--conflict__yes';
                $text = _tF('yes', 'station');
            } else {
                $positive = true;
                $class = 'dk--conflict__no';
                $text = _tF('no', 'station');
            }
            $helpMsg = _tF($positive ? 'hasConflictsHelpPositive' : 'hasConflictsHelpNegative', 'station');
        } else {
            $helpMsg = _tF('cannotConflict', 'station');
            $class = 'dk--conflict__no';
            $text = _tF('no', 'station');
        }

        ?>
      <span class="dk--conflict__label "> <?= _tF('conflict', 'station') ?></span>
      <span class="dk--conflict <?= $class ?>" <?= DactylKit::tooltip($helpMsg) ?> ><?= $text ?></span>
    </td>
    <td>
        <?php $class = '';
        $helpMsg = 'xx';

        if ($modelA->showInterference($newStationType, $modelA->type, 'passive')) {
            // positive = it is ok
            if ($modelA->passiveIsolationIncreaseRequested == 0) {
                $val = _tF('no', 'station');
                $class = 'dk--conflict__no';
                $helpMsg = _tF('passiveInterferenceHelpPositiveColumn', 'station');
            } else {
                $interferenceVal = round($modelA->passiveIsolationIncreaseRequested, 1);
                $positive = $interferenceVal < 0;
                switch (true) {
                    case  (is_infinite($interferenceVal) || is_nan($interferenceVal)):
                    {
                        $interferenceVal = "~";
                        $class = 'dk--conflict__yes';
                        $val = _tF('interferes', 'station', ['val' => $interferenceVal]);

                        $helpMsg = _tF('passive interference by {amount}', 'station', ['amount' => $interferenceVal]);
                        break;
                    }
                    case $interferenceVal < -110:
                    {
                        $class = 'dk--conflict__no';
                        $val = _tF('no', 'station');
                        $helpMsg = _tF('passive not interference', 'station');

                        break;
                    }
                    case $interferenceVal <= 0:
                    {
                        $class = 'dk--conflict__no';
                        $val = _tF('not interferes', 'station', ['val' => $interferenceVal]);
                        $helpMsg = _tF('passive not interference with {reserve}', 'station', ['reserve' => $interferenceVal]);

                        break;
                    }
                    case $interferenceVal > 0:
                    {
                        $class = 'dk--conflict__yes';
                        $val = _tF('interferes', 'station', ['val' => $interferenceVal]);
                        $helpMsg = _tF('passive interference by {amount}', 'station', ['amount' => $interferenceVal]);

                        break;
                    }
                }
            }
        } else {
            $helpMsg = _tF('cannotPassive', 'station');
            $val = _tF('no', 'station');
            $class = 'dk--conflict__no';
        }

        $out = Html::tag('span', $val,
            [
                'class' => $class,
                'title' => $helpMsg,
                'data-toggle' => 'tooltip',
                'data-placement' => 'top',
            ]); ?>
      <span class="dk--conflict__label "> <?= _tF('passive interference', 'station') ?></span>
        <?= $out ?>
    </td>
    <td>
        <?php
        $class = '';
        $helpMsg = 'cc';

        if ($modelA->showInterference($newStationType, $modelA->type, 'active')) {
            // positive = it is ok
            if ($modelA->activeIsolationIncreaseRequested == 0) {
                $val = _tF('no', 'station');
                $class = 'dk--conflict__no';
                $helpMsg = _tF('activeInterferenceHelpPositiveColumn', 'station');
            } else {
                $interferenceVal = round($modelA->activeIsolationIncreaseRequested, 1);
                switch (true) {
                    case  (is_infinite($interferenceVal) || is_nan($interferenceVal)):
                    {
                        $interferenceVal = "~";
                        $class = 'dk--conflict__yes';
                        $val = _tF('interferes', 'station', ['val' => $interferenceVal]);
                        $helpMsg = _tF('active interference by {amount}', 'station', ['amount' => $interferenceVal]);

                        break;
                    }
                    case $interferenceVal < -110:
                    {
                        $class = 'dk--conflict__no';
                        $val = _tF('no', 'station');
                        $helpMsg = _tF('active not interference', 'station');

                        break;
                    }
                    case $interferenceVal <= 0:
                    {
                        $class = 'dk--conflict__no';
                        $val = _tF('not interferes', 'station', ['val' => $interferenceVal]);
                        $helpMsg = _tF('active not interference with {reserve}', 'station', ['reserve' => $interferenceVal]);

                        break;
                    }
                    case $interferenceVal > 0:
                    {
                        $class = 'dk--conflict__yes';
                        $val = _tF('interferes', 'station', ['val' => $interferenceVal]);
                        $helpMsg = _tF('active interference by {amount}', 'station', ['amount' => $interferenceVal]);

                        break;
                    }
                }
            }
        } else {
            $helpMsg = _tF('cannotActive', 'station');
            $val = _tF('no', 'station');
            $class = 'dk--conflict__no';
        }

        $out = Html::tag('span', $val,
            [
                'class' => $class,
                'title' => $helpMsg,
                'data-toggle' => 'tooltip',
                'data-placement' => 'top',
            ]); ?>
      <span class="dk--conflict__label "><?= _tF('active interference', 'station') ?></span>
        <?= $out ?>
    </td>
  </tr>

  <tr>
    <td><?= _tF('station b', 'station') ?></td>
    <td>
        <?php
        $class = '';
        if ($modelB->showInterference($newStationType, $modelB->type, 'general')) {
            // positive = it is ok
            $positive = false;
            if ($modelB->hasConflicts) {
                $class = 'dk--conflict__yes';
                $text = _tF('yes', 'station');
            } else {
                $positive = true;
                $text = _tF('no', 'station');
                $class = 'dk--conflict__no';
            }

            $helpMsg = _tF($positive ? 'hasConflictsHelpPositive' : 'hasConflictsHelpNegative', 'station');
        } else {
            $helpMsg = _tF('cannotConflict', 'station');
            $class = 'dk--conflict__no';
            $text = _tF('no', 'station');
        }

        ?>
      <span class="dk--conflict__label "><?= _tF('conflict', 'station') ?></span>
      <span class="dk--conflict <?= $class ?>" <?= DactylKit::tooltip($helpMsg) ?> ><?= $text ?></span>
    </td>
    <td>
        <?php $class = '';
        $helpMsg = 'dd';

        if ($modelB->showInterference($newStationType, $modelB->type, 'passive')) {
            // positive = it is ok
            $positive = false;
            if ($modelB->passiveIsolationIncreaseRequested == 0) {
                $val = _tF('no', 'station');
                $class = 'dk--conflict__no';
                $helpMsg = _tF('passiveInterferenceHelpPositiveColumn', 'station');
            } else {
                $interferenceVal = round($modelB->passiveIsolationIncreaseRequested, 1);
                switch (true) {
                    case  (is_infinite($interferenceVal) || is_nan($interferenceVal)):
                    {
                        $interferenceVal = "~";
                        $class = 'dk--conflict__yes';
                        $val = _tF('interferes', 'station', ['val' => $interferenceVal]);

                        $helpMsg = _tF('passive interference by {amount}', 'station', ['amount' => $interferenceVal]);
                        break;
                    }
                    case $interferenceVal < -110:
                    {
                        $class = 'dk--conflict__no';
                        $val = _tF('no', 'station');
                        $helpMsg = _tF('passive not interference', 'station');

                        break;
                    }
                    case $interferenceVal <= 0:
                    {
                        $class = 'dk--conflict__no';
                        $val = _tF('not interferes', 'station', ['val' => $interferenceVal]);
                        $helpMsg = _tF('passive not interference with {reserve}', 'station', ['reserve' => $interferenceVal]);

                        break;
                    }
                    case $interferenceVal > 0:
                    {
                        $class = 'dk--conflict__yes';
                        $val = _tF('interferes', 'station', ['val' => $interferenceVal]);
                        $helpMsg = _tF('passive interference by {amount}', 'station', ['amount' => $interferenceVal]);

                        break;
                    }
                }
            }

        } else {
            $helpMsg = _tF('cannotPassive', 'station');
            $val = _tF('no', 'station');
            $class = 'dk--conflict__no';
        }

        $out = "<span class='{$class}' " . DactylKit::tooltip($helpMsg) . ">{$val}</span>";
        ?>
      <span class="dk--conflict__label "><?= _tF('passive interference', 'station') ?></span>
        <?= $out ?>
    </td>
    <td>
        <?php
        $class = '';
        $helpMsg = '';
        if ($modelB->showInterference($newStationType, $modelB->type, 'active')) {
            // positive = it is ok
            $positive = false;
            if ($modelB->activeIsolationIncreaseRequested == 0) {
                $val = _tF('no', 'station');
                $class = 'dk--conflict__no';
                $helpMsg = _tF('activeInterferenceHelpPositiveColumn', 'station');
            } else {
                $interferenceVal = round($modelB->activeIsolationIncreaseRequested, 1);
                switch (true) {
                    case  (is_infinite($interferenceVal) || is_nan($interferenceVal)):
                    {
                        $interferenceVal = "~";
                        $class = 'dk--conflict__yes';
                        $val = _tF('interferes', 'station', ['val' => $interferenceVal]);
                        $helpMsg = _tF('active interference by {amount}', 'station', ['amount' => $interferenceVal]);

                        break;
                    }
                    case $interferenceVal < -110:
                    {
                        $class = 'dk--conflict__no';
                        $val = _tF('no', 'station');
                        $helpMsg = _tF('active not interference', 'station');

                        break;
                    }
                    case $interferenceVal <= 0:
                    {
                        $class = 'dk--conflict__no';
                        $val = _tF('not interferes', 'station', ['val' => $interferenceVal]);
                        $helpMsg = _tF('active not interference with {reserve}', 'station', ['reserve' => $interferenceVal]);

                        break;
                    }
                    case $interferenceVal > 0:
                    {
                        $class = 'dk--conflict__yes';
                        $val = _tF('interferes', 'station', ['val' => $interferenceVal]);
                        $helpMsg = _tF('active interference by {amount}', 'station', ['amount' => $interferenceVal]);

                        break;
                    }
                }
            }
        } else {
            $helpMsg = _tF('cannotActive', 'station');
            $val = _tF('no', 'station');
            $class = 'dk--conflict__no';
        }
        $out = "<span class='{$class}' " . DactylKit::tooltip($helpMsg) . ">{$val}</span>";
        ?>

      <span class="dk--conflict__label "><?= _tF('active interference', 'station') ?></span>
        <?= $out ?>
    </td>
  </tr>
  </tbody>
</table>