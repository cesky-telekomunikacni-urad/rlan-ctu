<?php
/*
* Copyright 2023 DactylGroup s.r.o.
*
* Licensed under the EUPL, Version 1.2 or – as soon they
will be approved by the European Commission - subsequent
versions of the EUPL (the "Licence");
* You may not use this work except in compliance with the
Licence.
* You may obtain a copy of the Licence at:
*
* https://joinup.ec.europa.eu/software/page/eupl5
*
* Unless required by applicable law or agreed to in
writing, software distributed under the Licence is
distributed on an "AS IS" basis,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
express or implied.
* See the Licence for the specific language governing
permissions and limitations under the Licence.
*/
?>


<?php

/**
 * @var $model \common\models\Station
 * @var $saveExit bool
 */

use common\models\Station;
use frontend\controllers\StationController;
use frontend\widgets\dactylkit\DactylKit;

switch ($model->status) {
    case (Station::STATUS_WAITING):
    {
        $badge = DactylKit::badge(_tF('pending', 'station'), DactylKit::BADGE_INFORMATIONAL);
        break;
    }
    case (Station::STATUS_UNPUBLISHED):
    {
        $badge = DactylKit::badge(_tF('unpublished', 'station'), DactylKit::BADGE_INFORMATIONAL);
        break;
    }
    case (Station::STATUS_EXPIRED):
    {
        $badge = DactylKit::badge(_tF('expired', 'station'), DactylKit::BADGE_WARNING);
        break;
    }
    case (Station::STATUS_FINISHED):
    {
        $badge = DactylKit::badge(_tF('active', 'station'), DactylKit::BADGE_SUCCESS);
        break;
    }
    default:
    {
        $badge = DactylKit::badge(_tF('concept', 'station'));
    }
}



?>

<div class="livebox--header">
    <?= $model->getName() ?>
    <?= $badge ?>
    <?php
    /* TODO temporarily removed, lacks functionality, (to be added sometime later ?) */
    if (/*$saveExit*/
    false) {
        echo DactylKit::button(_tF('save and exit', 'station'), DactylKit::BUTTON_TYPE_GHOST, '', '', '', [
            'class' => 'exit',
        ]);
    }
    ?>
</div>
