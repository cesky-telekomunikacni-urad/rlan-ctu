<?php
/*
* Copyright 2023 DactylGroup s.r.o.
*
* Licensed under the EUPL, Version 1.2 or – as soon they
will be approved by the European Commission - subsequent
versions of the EUPL (the "Licence");
* You may not use this work except in compliance with the
Licence.
* You may obtain a copy of the Licence at:
*
* https://joinup.ec.europa.eu/software/page/eupl5
*
* Unless required by applicable law or agreed to in
writing, software distributed under the Licence is
distributed on an "AS IS" basis,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
express or implied.
* See the Licence for the specific language governing
permissions and limitations under the Licence.
*/
?>


<?php

use common\models\Station;
use dactylcore\core\widgets\common\bootstrap\Html as BootHtml;
use yii\helpers\Html;

/**
 * @var $modelA Station
 * @var $modelB Station
 */
?>
<table>
    <thead>
    <tr class="help-row">
        <th><?= _tF('results_station_name', 'station') ?></th>
        <th
                title="<?= _tF('hasConflictsHelp', 'station') ?>"
                data-toggle='tooltip'
                data-placement='top'
        ><?= _tF('hasConflicts', 'station') ?></th>
        <th
                title="<?= _tF('passiveInterferenceHelp', 'station') ?>"
                data-toggle='tooltip'
                data-placement='top'
        ><?= _tF('passiveInterference', 'station') ?></th>
        <th
                title="<?= _tF('passiveInterferenceHelp', 'station') ?>"
                data-toggle='tooltip'
                data-placement='top'
        ><?= _tF('passiveIsolationIncreaseRequested', 'station') ?></th>
        <th
                title="<?= _tF('activeInterferenceHelp', 'station') ?>"
                data-toggle='tooltip'
                data-placement='top'
        ><?= _tF('activeInterference', 'station') ?></th>
        <th
                title="<?= _tF('activeInterferenceHelp', 'station') ?>"
                data-toggle='tooltip'
                data-placement='top'
        ><?= _tF('activeIsolationIncreaseRequested', 'station') ?></th>
    </tr>
    </thead>
    <tbody>
    <tr>
        <td><?= _tF('station a', 'station') ?></td>
        <td>
            <?php
            $class = '';
            if ($modelA->showInterference($newStationType, $modelA->type, 'general')) {
                // positive = it is ok
                $positive = false;
                if ($modelA->hasConflicts) {
                    $val = BootHtml::badge(_tF('yes', 'station'), BootHtml::TYPE_WARNING);
                } else {
                    $positive = true;
                    $val = BootHtml::badge(_tF('no', 'station'), BootHtml::TYPE_SUCCESS);
                }

                $helpMsg = _tF($positive ? 'hasConflictsHelpPositive' : 'hasConflictsHelpNegative', 'station');
            } else {
                $helpMsg = _tF('cannotConflict', 'station');
                $val = BootHtml::badge(_tF('no', 'station'), BootHtml::TYPE_DEFAULT);
                $class = 'cannotInterfere';
            }

            $out = Html::tag('span', $val,
                [
                    'class' => $class,
                    'title' => $helpMsg,
                    'data-toggle' => 'tooltip',
                    'data-placement' => 'top',
                ]);
            ?>
            <?= $out ?>
        </td>
        <td>
            <?php $class = '';
            if ($modelA->showInterference($newStationType, $modelA->type, 'passive')) {
                // positive = it is ok
                $positive = false;
                if ($modelA->passiveInterference) {
                    $val = BootHtml::badge(_tF('yes', 'station'), BootHtml::TYPE_WARNING);
                } else {
                    $positive = true;
                    $val = BootHtml::badge(_tF('no', 'station'), BootHtml::TYPE_SUCCESS);
                }

                $helpMsg = _tF($positive ? 'passiveInterferenceHelpPositiveColumn' :
                    'passiveInterferenceHelpNegativeColumn', 'station');
            } else {
                $helpMsg = _tF('cannotPassive', 'station');
                $val = BootHtml::badge(_tF('no', 'station'), BootHtml::TYPE_DEFAULT);
                $class = 'cannotInterfere';
            }

            $out = Html::tag('span', $val,
                [
                    'class' => $class,
                    'title' => $helpMsg,
                    'data-toggle' => 'tooltip',
                    'data-placement' => 'top',
                ]); ?>
            <?= $out ?>
        </td>
        <td>
            <?php $class = '';
            if ($modelA->showInterference($newStationType, $modelA->type, 'passive')) {
                // positive = it is ok
                $positive = false;
                if ($modelA->passiveIsolationIncreaseRequested == 0) {
                    $val = '-';
                } else {
                    $val = round($modelA->passiveIsolationIncreaseRequested, 1);
                    $positive = $val < 0;
                }

                $helpMsg = _tF($positive ? 'passiveInterferenceHelpPositiveColumn' :
                    'passiveInterferenceHelpNegativeColumn', 'station');
            } else {
                $helpMsg = _tF('cannotPassive', 'station');
                $val = '-';
                $class = 'cannotInterfere';
            }

            $out = Html::tag('span', $val,
                [
                    'class' => $class,
                    'title' => $helpMsg,
                    'data-toggle' => 'tooltip',
                    'data-placement' => 'top',
                ]); ?>
            <?= $out ?>
        </td>
        <td>
            <?php $class = '';
            if ($modelA->showInterference($newStationType, $modelA->type, 'active')) {
                // positive = it is ok
                $positive = false;
                if ($modelA->activeInterference) {
                    $val = BootHtml::badge(_tF('yes', 'station'), BootHtml::TYPE_WARNING);
                } else {
                    $positive = true;
                    $val = BootHtml::badge(_tF('no', 'station'), BootHtml::TYPE_SUCCESS);
                }

                $helpMsg = _tF($positive ? 'activeInterferenceHelpPositiveColumn' :
                    'activeInterferenceHelpNegativeColumn', 'station');
            } else {
                $helpMsg = _tF('cannotActive', 'station');
                $val = BootHtml::badge(_tF('no', 'station'), BootHtml::TYPE_DEFAULT);
                $class = 'cannotInterfere';
            }

            $out = Html::tag('span', $val,
                [
                    'class' => $class,
                    'title' => $helpMsg,
                    'data-toggle' => 'tooltip',
                    'data-placement' => 'top',
                ]);
            ?>
            <?= $out; ?>

        </td>
        <td>
            <?php
            $class = '';
            if ($modelA->showInterference($newStationType, $modelA->type, 'active')) {
                // positive = it is ok
                $positive = false;
                if ($modelA->activeIsolationIncreaseRequested == 0) {
                    $val = '-';
                } else {
                    $val = round($modelA->activeIsolationIncreaseRequested, 1);
                    $positive = $val < 0;
                    if (is_infinite($val) || is_nan($val)) {
                        $val = "~";
                    }
                }

                $helpMsg = _tF($positive ? 'activeInterferenceHelpPositiveColumn' :
                    'activeInterferenceHelpNegativeColumn', 'station');
            } else {
                $helpMsg = _tF('cannotActive', 'station');
                $val = '-';
                $class = 'cannotInterfere';
            }

            $out = Html::tag('span', $val,
                [
                    'class' => $class,
                    'title' => $helpMsg,
                    'data-toggle' => 'tooltip',
                    'data-placement' => 'top',
                ]); ?>
            <?= $out ?>
        </td>
    </tr>

    <tr>
        <td><?= _tF('station b', 'station') ?></td>
        <td>
            <?php
            $class = '';
            if ($modelB->showInterference($newStationType, $modelB->type, 'general')) {
                // positive = it is ok
                $positive = false;
                if ($modelB->hasConflicts) {
                    $val = BootHtml::badge(_tF('yes', 'station'), BootHtml::TYPE_WARNING);
                } else {
                    $positive = true;
                    $val = BootHtml::badge(_tF('no', 'station'), BootHtml::TYPE_SUCCESS);
                }

                $helpMsg = _tF($positive ? 'hasConflictsHelpPositive' : 'hasConflictsHelpNegative', 'station');
            } else {
                $helpMsg = _tF('cannotConflict', 'station');
                $val = BootHtml::badge(_tF('no', 'station'), BootHtml::TYPE_DEFAULT);
                $class = 'cannotInterfere';
            }

            $out = Html::tag('span', $val,
                [
                    'class' => $class,
                    'title' => $helpMsg,
                    'data-toggle' => 'tooltip',
                    'data-placement' => 'top',
                ]);
            ?>
            <?= $out ?>
        </td>
        <td>
            <?php $class = '';
            if ($modelB->showInterference($newStationType, $modelB->type, 'passive')) {
                // positive = it is ok
                $positive = false;
                if ($modelB->passiveInterference) {
                    $val = BootHtml::badge(_tF('yes', 'station'), BootHtml::TYPE_WARNING);
                } else {
                    $positive = true;
                    $val = BootHtml::badge(_tF('no', 'station'), BootHtml::TYPE_SUCCESS);
                }

                $helpMsg = _tF($positive ? 'passiveInterferenceHelpPositiveColumn' :
                    'passiveInterferenceHelpNegativeColumn', 'station');
            } else {
                $helpMsg = _tF('cannotPassive', 'station');
                $val = BootHtml::badge(_tF('no', 'station'), BootHtml::TYPE_DEFAULT);
                $class = 'cannotInterfere';
            }

            $out = Html::tag('span', $val,
                [
                    'class' => $class,
                    'title' => $helpMsg,
                    'data-toggle' => 'tooltip',
                    'data-placement' => 'top',
                ]); ?>
            <?= $out ?>
        </td>
        <td>
            <?php $class = '';
            if ($modelB->showInterference($newStationType, $modelB->type, 'passive')) {
                // positive = it is ok
                $positive = false;
                if ($modelB->passiveIsolationIncreaseRequested == 0) {
                    $val = '-';
                } else {
                    $val = round($modelB->passiveIsolationIncreaseRequested, 1);
                    $positive = $val < 0;
                }

                $helpMsg = _tF($positive ? 'passiveInterferenceHelpPositiveColumn' :
                    'passiveInterferenceHelpNegativeColumn', 'station');
            } else {
                $helpMsg = _tF('cannotPassive', 'station');
                $val = '-';
                $class = 'cannotInterfere';
            }

            $out = Html::tag('span', $val,
                [
                    'class' => $class,
                    'title' => $helpMsg,
                    'data-toggle' => 'tooltip',
                    'data-placement' => 'top',
                ]); ?>
            <?= $out ?>
        </td>
        <td>
            <?php $class = '';
            if ($modelB->showInterference($newStationType, $modelB->type, 'active')) {
                // positive = it is ok
                $positive = false;
                if ($modelB->activeInterference) {
                    $val = BootHtml::badge(_tF('yes', 'station'), BootHtml::TYPE_WARNING);
                } else {
                    $positive = true;
                    $val = BootHtml::badge(_tF('no', 'station'), BootHtml::TYPE_SUCCESS);
                }

                $helpMsg = _tF($positive ? 'activeInterferenceHelpPositiveColumn' :
                    'activeInterferenceHelpNegativeColumn', 'station');
            } else {
                $helpMsg = _tF('cannotActive', 'station');
                $val = BootHtml::badge(_tF('no', 'station'), BootHtml::TYPE_DEFAULT);
                $class = 'cannotInterfere';
            }

            $out = Html::tag('span', $val,
                [
                    'class' => $class,
                    'title' => $helpMsg,
                    'data-toggle' => 'tooltip',
                    'data-placement' => 'top',
                ]);
            ?>
            <?= $out; ?>

        </td>
        <td>
            <?php
            $class = '';
            if ($modelB->showInterference($newStationType, $modelB->type, 'active')) {
                // positive = it is ok
                $positive = false;
                if ($modelB->activeIsolationIncreaseRequested == 0) {
                    $val = '-';
                } else {
                    $val = round($modelB->activeIsolationIncreaseRequested, 1);
                    $positive = $val < 0;
                    if (is_infinite($val) || is_nan($val)) {
                        $val = "~";
                    }
                }

                $helpMsg = _tF($positive ? 'activeInterferenceHelpPositiveColumn' :
                    'activeInterferenceHelpNegativeColumn', 'station');
            } else {
                $helpMsg = _tF('cannotActive', 'station');
                $val = '-';
                $class = 'cannotInterfere';
            }

            $out = Html::tag('span', $val,
                [
                    'class' => $class,
                    'title' => $helpMsg,
                    'data-toggle' => 'tooltip',
                    'data-placement' => 'top',
                ]); ?>
            <?= $out ?>
        </td>
    </tr>
    </tbody>
</table>