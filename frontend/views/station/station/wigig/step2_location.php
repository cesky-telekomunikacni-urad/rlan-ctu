<?php
/*
* Copyright 2023 DactylGroup s.r.o.
*
* Licensed under the EUPL, Version 1.2 or – as soon they
will be approved by the European Commission - subsequent
versions of the EUPL (the "Licence");
* You may not use this work except in compliance with the
Licence.
* You may obtain a copy of the Licence at:
*
* https://joinup.ec.europa.eu/software/page/eupl5
*
* Unless required by applicable law or agreed to in
writing, software distributed under the Licence is
distributed on an "AS IS" basis,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
express or implied.
* See the Licence for the specific language governing
permissions and limitations under the Licence.
*/
?>


<?php

/**
 * @var $steps array
 * @var $modelStation \common\models\Station
 * @var $modelStationType \common\models\StationWigig
 * @var $hasChanged string
 */

use dactylcore\core\widgets\common\select2\Select2;
use frontend\assets\map\AddWigigAsset;
use frontend\widgets\ActiveForm;
use frontend\widgets\dactylkit\DactylKit;
use yii\helpers\Html;


?>

<?php $form = ActiveForm::begin([
    'enableAjaxValidation' => true,
    'id' => 'station-form',
    'pjax' => true,
    'pjaxOptions' => [
        'enablePushState' => true,
        'enableReplaceState' => true,
    ],
    'options' => [
        'data-url' => url(['station/edit', 'id' => $modelStation->id]),
        'data-changed' => $hasChanged,
    ],
    'action' => url(['station/edit', 'id' => $modelStation->id]),
]);
AddWigigAsset::register($this);

?>
    <script>
        var urlToPush = $("form#station-form").data('url');
        var hasChanged = $("form#station-form").data('changed') + "";
        if (history.pushState && urlToPush) {
            history.pushState(null, null, urlToPush); // URL is now /inbox/N
        }

        needsToReload = needsToReload || (hasChanged === 'true');
    </script>
    <div class="livebox--station-registration">
        <?= $this->render('../partial/header', ['model' => $modelStation, 'saveExit' => false]) ?>
        <div class="livebox--station-registration--content map-block" id="vueapp-wigig">
            <?= DactylKit::progressBar(2, $steps) ?>
            <h6><?= _tF('Where is the station located', 'station') ?></h6>
            <span class="livebox--station-registration--content__subheading"><?= _tF('Use map to select location or enter GPS coordinates',
                    'station') ?></span>
            <div class="row no-gutters livebox--station-registration--content__location-inputs fs-address-inputs">
                <div class="col-6 col-md-4 first-input-column">
                    <?= $form->field($modelStation, 'lat')->textInput([
                        'id' => 'location-lat',
                        'maxlength' => true,
                        'v-model' => 'locationLat',
                        '@keyup' => 'changePosition',
                        'class' => 'dangerous',
                    ]) ?>
                </div>
                <div class="col-6 col-md-4 second-input-column">
                    <?= $form->field($modelStation, 'lng')->textInput([
                        'id' => 'location-lng',
                        'maxlength' => true,
                        'v-model' => 'locationLng',
                        '@keyup' => 'changePosition',
                        'class' => 'dangerous',
                    ]) ?>
                </div>

                <div class="col-lg-3 col-md-12 col-sm-12 offset-lg-1 px-0">
                    <?= $form->field($modelStationType, 'direction')->textInput([
                        'type' => 'number',
                        'step' => 0.000000000000001,
                        'min' => 0,
                        'max' => 360,
                        'v-model' => 'direction',
                        '@keyup' => 'changeDirection',
                        'class' => 'dangerous' . ($modelStation->id_station_pair !== null ? ' as-disabled ' : ''),
                        'readonly' => ($modelStation->id_station_pair !== null),
                    ])->hint(false) ?>
                </div>

                <div class="col-lg-4 col-md-12">
                    <?= $form->field($modelStation, 'id_station_pair')->widget(Select2::class, [
                        'hideSearch' => false,
                        'options' => [
                            'class' => 'dangerous ',
                        ],
                        'data' => ($modelStation->id_station_pair !== null && $modelStation->stationPair !== null) ?
                            [
                                $modelStation->id_station_pair => Html::tag('span', $modelStation->stationPair->name, [
                                    'data' => [
                                        'lat' => $modelStation->stationPair->lat,
                                        'lng' => $modelStation->stationPair->lng,
                                    ],
                                ]),
                            ] : [],
                        'pluginOptions' => [
                            'allowClear' => true,
                            'escapeMarkup' => new \yii\web\JsExpression(
                                <<<JS
                                function(markup) {
                                    return markup; // do not escape
                                }
JS
                            ),
                            'ajax' => [
                                'url' => url(['station/wigig-pair-dropdown-data']),
                                'data' => new \yii\web\JsExpression(
                                    <<<JS
                                    function (params) {
                                        return {
                                            idStation: {$modelStation->id},
                                            lat: $("#location-lat").val(),
                                            lng: $("#location-lng").val(),
                                            term: params.term,
                                            page: params.page || 1
                                        }
                                    }
JS
                                ),
                            ],
                        ],
                    ]) ?>
                </div>
            </div>
            <?= $this->render('../../../site/partial/switcher') ?>
        </div>
        <script>
            if (window['initWigig']) {
                initWigig();
            }
        </script>


        <div class="map-outer-box" style="width: 100%; height: 100%">
            <div id="map-wigig" style="width: 100%; height: 100%; min-height: 250px;"
                 data-stationlink="<?= url(['station/station', 'id' => 0]) ?>">
                <div class="overlayWigig mapOverlay">
                    <div class="lds-grid">
                        <div></div>
                        <div></div>
                        <div></div>
                        <div></div>
                        <div></div>
                        <div></div>
                        <div></div>
                        <div></div>
                        <div></div>
                    </div>
                </div>
            </div>
        </div>

        <?= $this->render('../partial/footer', [
            'prevUrl' => url(['station/step-back', 'id' => $modelStation->id]),
            'nextBtnLabel' => _tF('save and continue', 'station'),
            'dirtyNextBtnLabel' => _tF('save and recount', 'station'),
            'continueBtn' => true,
        ]) ?>
    </div>
<?php if ($modelStation->isPublished()): ?>
  <script>initDirtyForms(true);</script>
<?php else: ?>
  <script>initDirtyForms(false);</script>
<?php endif; ?>

<?php $this->registerJS(
    <<<JS
    $(document).on('select2:select', '#station-id_station_pair', function (e) {
        mainApp.setDirectionToPartner();
        $("input[name='StationWigig[direction]']").addClass('as-disabled').attr('readonly',true);
    });

    $(document).on('select2:unselect', '#station-id_station_pair', function (e) {
        $("input[name='StationWigig[direction]']").removeClass('as-disabled').attr('readonly',false);
        mainApp.map.showDirectionMarker();
    });
JS
); ?>

<?php ActiveForm::end() ?>