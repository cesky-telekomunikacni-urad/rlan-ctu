<?php
/*
* Copyright 2023 DactylGroup s.r.o.
*
* Licensed under the EUPL, Version 1.2 or – as soon they
will be approved by the European Commission - subsequent
versions of the EUPL (the "Licence");
* You may not use this work except in compliance with the
Licence.
* You may obtain a copy of the Licence at:
*
* https://joinup.ec.europa.eu/software/page/eupl5
*
* Unless required by applicable law or agreed to in
writing, software distributed under the Licence is
distributed on an "AS IS" basis,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
express or implied.
* See the Licence for the specific language governing
permissions and limitations under the Licence.
*/
?>


<?php

/**
 * @var $dataProvider
 * @var $searchModel
 * @var $model Station
 * @var $modelStation Station
 * @var $startedConversationsWith array
 * @var $bData array
 * @var $steps array
 * @var $hasChanged string
 * @var $anyInConflict boolean - is there any conflict with other stations
 */

use common\models\StationWigig;
use dactylcore\core\widgets\common\pjax\Pjax;
use frontend\assets\map\CalcWigigAsset;
use frontend\controllers\StationController;
use common\models\Station;
use frontend\widgets\ActiveForm;
use frontend\widgets\dactylkit\DactylKit;
use yii\helpers\Html;

$modelLabels = $model->attributeLabels();
/**
 * @var $modelWigig StationWigig
 */
$modelWigig = $model->getStationTypeObject();
$modelWigigLabels = $modelWigig->attributeLabels();

if ($modelStation->isForProlongation()) {
    if ($anyInConflict) {
        $nextBtnLabel = _tF('declare and prolong', 'station');
    } else {
        $nextBtnLabel = _tF('prolong station', 'station');
    }
} else {
    if ($modelStation->isPublished()) {
        $nextBtnLabel = '';
    } else {
        $nextBtnLabel = $anyInConflict ? _tF('isolation declaration', 'station') : _tF('publish', 'station');
    }
}

$action = $anyInConflict ? 'station/declaration' : 'station/publish';

/* decides if last step in progress-bar will be shown as completed or in progress */
$progressStep = $modelStation->status == Station::STATUS_FINISHED ? 5 : 4;
?>

<?php $form = ActiveForm::begin([
    'id' => 'station-form',
    'pjax' => true,
    'pjaxOptions' => [
        'enablePushState' => true,
        'enableReplaceState' => true,
    ],
    'options' => [
        'data-url' => url(['station/edit', 'id' => $modelStation->id]),
        'data-changed' => (string)$hasChanged,
    ],
    'action' => url([$action, 'id' => $modelStation->id]),
]);

$dmsLng = $model->DDtoDMS(false);
$dmsLat = $model->DDtoDMS(true);

CalcWigigAsset::register($this); ?>

<script>
    var urlToPush = $("form#station-form").data('url');
    var hasChanged = $("form#station-form").data('changed') + "";

    if (history.pushState && urlToPush) {
        history.pushState(null, null, urlToPush); // URL is now /inbox/N
    }
    needsToReload = needsToReload || (hasChanged === 'true');
</script>
<div class="livebox--station-registration">
    <?= $this->render('../partial/header', ['model' => $model, 'saveExit' => true]) ?>
  <div class="livebox--station-registration--content">
      <?= DactylKit::progressBar($progressStep, $steps) ?>
    <span class="dk--table--ghost__heading"><?= _tF('information', 'station') ?></span>
    <table class="dk--table dk--table--ghost">
      <tbody>
      <tr>
        <td><?= _tF('kind', 'station') ?></td>
        <td><?= $model->type == StationController::TYPE_FS ? _tF('station type fs', 'station') :
                ($modelWigig->is_ptmp ? _tF('station type wigig ptmp', 'station') :
                     _tF('station type wigig ptp', 'station')) ?></td>
      </tr>
      </tbody>
    </table>
    <hr id="vueapp-wigig-calc" class="dk--hr">
    <span class="dk--table--ghost__heading"><?= _tF('location', 'station') ?></span>
    <table class="dk--table dk--table--ghost">
      <tbody>
      <tr>
        <td><?= _tF('GPS', 'station') ?></td>
        <td>
          <span <?= DactylKit::tooltip("{$dmsLng['deg']}° {$dmsLng['min']}' " . round($dmsLng['sec']) . "''",
              DactylKit::TOOLTIP_TOP) ?> style="margin:0">
                <?= $model->lng ?>°,
          </span>
          <span <?= DactylKit::tooltip("{$dmsLat['deg']}° {$dmsLat['min']}' " . round($dmsLat['sec']) . "''",
              DactylKit::TOOLTIP_TOP) ?> style="margin:0">
                <?= $model->lat ?>°
          </span>
        </td>
      </tr>
      <tr>
        <td><?= _tF('direction', 'station') ?></td>
        <td><?= "{$modelWigig->direction}°" ?></td>
      </tr>
      <?php if ($model->id_station_pair && $model->stationPair !== null): ?>
          <tr>
              <td><?= _tF('pair_station', 'station') ?></td>
              <td>
                  <?= Html::a("{$model->stationPair->name} {$model->stationPair->getTypeName()}",
                      url(['station/station', 'id' => $model->id_station_pair]),
                      ['target' => '_blank', 'data-pjax' => "0"]) ?>
              </td>
          </tr>
          <tr>
              <td></td>
              <td>
                  <?= DactylKit::alert('', _tF('pair_station_link_alert', 'station'),DactylKit::ALERT_TYPE_INFO); ?>
              </td>
          </tr>
      <?php endif; ?>
      </tbody>
    </table>
    <div style="width: 100%; height: 400px">
      <div id="map-calc-wigig" style="width: 100%; height: 100%;  min-height: 250px;"
           data-stationlink="<?= url(['station/station', 'id' => 0]) ?>">
        <div class="mapOverlay overlayCalcWigig">
          <div class="lds-grid">
            <div></div>
            <div></div>
            <div></div>
            <div></div>
            <div></div>
            <div></div>
            <div></div>
            <div></div>
            <div></div>
          </div>
        </div>
      </div>
    </div>
    <hr class="dk--hr">
    <span class="dk--table--ghost__heading"><?= _tF('parameters', 'station') ?></span>
    <table class="dk--table dk--table--ghost">
      <tbody>
      <tr>
        <td><?= _tF('antenna gain', 'station') ?><span <?= DactylKit::tooltip(_tF('antenna_volume',
                'station_hint')) ?> ><?= DactylKit::icon(DactylKit::ICON_INFO_20) ?></span></td>
        <td><?= $model->antenna_volume != '' ? Station::formatNumber($model->antenna_volume) : '-' ?> dBi</td>
      </tr>
      <tr>
        <td><?= _tF('power_label', 'station') ?><span <?= DactylKit::tooltip(_tF('power',
                'station_hint')) ?> ><?= DactylKit::icon(DactylKit::ICON_INFO_20) ?></span></td>
        <td><?= $model->power != '' ? Station::formatNumber($model->power) : '-' ?> dBm</td>
      </tr>
      <tr>
        <td><?= _tF('eirp_label', 'station') ?><span <?= DactylKit::tooltip(_tF('eirp',
                'station_hint')) ?> ><?= DactylKit::icon(DactylKit::ICON_INFO_20) ?></span></td>
        <td><?= $modelWigig->eirp != '' ? Station::formatNumber($modelWigig->eirp) : '-' ?> dBm</td>
      </tr>
      <tr>
        <td><?= _tF('bandwidth', 'station') ?><span <?= DactylKit::tooltip(_tF('channel_width',
                'station_hint')) ?> ><?= DactylKit::icon(DactylKit::ICON_INFO_20) ?></span></td>
        <td><?= $model->channel_width ? Station::formatNumber($model->channel_width) : '-' ?> MHz</td>
      </tr>
      <?php if ($model->isMyStation()): ?>
        <tr>
            <?php if ($modelStation->hardware_identifier == Station::SERIAL_NUMBER) : ?>
              <td><?= $modelLabels['serial_number'] ?></td>
              <td><?= $modelStation->serial_number ?? '' ?></td>
            <?php else: ?>
              <td><?= $modelLabels['mac_address'] ?></td>
              <td><?= $modelStation->mac_address ?? '' ?></td>
            <?php endif; ?>
        </tr>
      <?php endif; ?>
      </tbody>
    </table>
    <hr class="dk--hr">
    <h5><?= _tF('affected stations', 'station') ?></h5>
    <span class="livebox--station-registration--content__subheading"><?= _tF('Coordinator calculator table heading',
            'station') ?></span>
    <div class="dk--table--scroll-holder">
        <?php Pjax::begin([
            'options' => [
                'id' => 'coordination-calc-summary',
            ],
            'timeout' => 0,
            'linkSelector' => "#coordination-calc-summary .pagination a",
            'enablePushState' => false,
            'enableReplaceState' => false,
        ]) ?>
        <?= $this->render('../step5_summary_table', [
            'startedConversationsWith' => $startedConversationsWith,
            'dataProvider' => $dataProvider,
            'searchModel' => $searchModel,
            'model' => $modelStation,
            'bData' => $bData,
            // the filter is disabled because setting the filter is submitting the form (and so far I have no fix yet)
            'showFilter' => false,
            'canEdit' => true,
        ]); ?>
        <?php Pjax::end() ?>
    </div>
  </div>
    <?= $this->render('../partial/footer', [
        'prevUrl' => url(['station/step-back', 'id' => $modelStation->id]),
        'nextBtnLabel' => $nextBtnLabel,
        'dirtyNextBtnLabel' => '',
        'continueBtn' => false,
    ]) ?>
</div>
<script>
    getAllFlashAlertMessages();
    if (window['initCalcWigig']) {
        initCalcWigig();
    }

    $("#coordination-calc-summary").on('pjax:start', function () {
        $("#coordination-calc-summary #loading").fadeIn();
    }).on('pjax:end', function () {
        $("#coordination-calc-summary #loading").fadeOut();
    });
</script>
<?php ActiveForm::end() ?>
