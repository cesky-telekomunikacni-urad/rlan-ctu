<?php
/*
* Copyright 2023 DactylGroup s.r.o.
*
* Licensed under the EUPL, Version 1.2 or – as soon they
will be approved by the European Commission - subsequent
versions of the EUPL (the "Licence");
* You may not use this work except in compliance with the
Licence.
* You may obtain a copy of the Licence at:
*
* https://joinup.ec.europa.eu/software/page/eupl5
*
* Unless required by applicable law or agreed to in
writing, software distributed under the Licence is
distributed on an "AS IS" basis,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
express or implied.
* See the Licence for the specific language governing
permissions and limitations under the Licence.
*/
?>


<?php

/**
 * @var $steps array
 * @var $modelStation \common\models\Station
 * @var $modelStationType \common\models\StationWigig
 * @var $hasChanged string
 */

use frontend\widgets\ActiveForm;
use frontend\widgets\dactylkit\DactylKit;
use common\models\Station;

$macAddrPlaceholder = $modelStation->getAttributeLabel('mac_address');
$serialNumberPlaceholder = $modelStation->getAttributeLabel('serial_number');

?>
<?php \yii\widgets\PjaxAsset::register($this) ?>

<?php $form = ActiveForm::begin([
    'id' => 'station-form',
    'pjax' => true,
    'pjaxOptions' => [
        'enablePushState' => true,
        'enableReplaceState' => true,
    ],
    'options' => [
        'data-url' => url(['station/edit', 'id' => $modelStation->id]),
        'data-changed' => $hasChanged,
        'data-id' => $modelStation->id,
    ],
    'action' => url(['station/edit', 'id' => $modelStation->id]),
]);
?>
    <script>
        var urlToPush = $("form#station-form").data('url');
        var hasChanged = $("form#station-form").data('changed') + "";
        if (history.pushState && urlToPush) {
            history.pushState(null, null, urlToPush); // URL is now /inbox/N
        }

        needsToReload = needsToReload || (hasChanged === 'true');
    </script>
    <div class="livebox--station-registration">
        <?= $this->render('../partial/header', ['model' => $modelStation, 'saveExit' => true]) ?>
        <div class="livebox--station-registration--content">
            <?= DactylKit::progressBar(3, $steps) ?>
            <h6><?= _tF('Do you know antenna gain and power or EIRP', 'station') ?></h6>
            <div class="row-no-gutters">
                <?= $form->field($modelStationType, 'is_ptmp', [
                    'showErrors' => false // hide errors because they are on antenna_volume and eirp input
                ])->hiddenInput([
                    'class' => ' dangerous ptmp_value',
                    'data-ptmp-treshold' => c('PTMP_THRESHOLD'),
                ])->label(false) ?>
                <?= $form->field($modelStationType, 'eirp_method', ['options' => ['class' => 'eirp_method_radio']])
                         ->radioList([
                             'auto' => _tF('eirp_auto', 'station'),
                             'manual' => _tF('eirp_manual', 'station'),
                         ],
                             [
                                 'item' => function ($index, $label, $name, $checked, $value)
                                 {
                                     $checkMarkup = $checked ? 'checked="checked"' : '';
                                     $return = '<div class="checkbox"><label>';
                                     $return .= "<input type='radio' name='{$name}' value='{$value}' $checkMarkup />";
                                     $return .= DactylKit::chip($label);
                                     $return .= '</label></div>';

                                     return $return;
                                 },
                             ])
                         ->label(false); ?>
            </div>
            <div class="row no-gutters justify-content-between align-items-start eirp-inputs">
                <?= $form->field($modelStation, 'antenna_volume',)->textInput([
                    'maxlength' => true,
                    'class' => 'dangerous',
                ]) ?>
                <span class="delimiter">+</span>
                <?= $form->field($modelStation, 'power')->textInput(['maxlength' => true, 'class' => 'dangerous']) ?>
                <span class="delimiter">=</span>
                <?= $form->field($modelStationType, 'eirp')->textInput([
                    'maxlength' => true,
                    'disabled' => true,
                    'class' => 'dangerous',
                ]) ?>
            </div>
            <h6><?= _tF('What is the channel bandwidth', 'station') ?></h6>
            <div class="col-md-6 px-0">
                <?= $form->field($modelStation, 'channel_width')->textInput([
                    'maxlength' => true,
                    'class' => 'dangerous',
                ]) ?>
            </div>
            <h6><?= _tF('Which of these identifiers do you know', 'station') ?></h6>
            <div class="row-no-gutters">
                <?= $form->field($modelStation, 'hardware_identifier',
                    ['options' => ['class' => 'hardware_identifier_input']])->radioList([
                    Station::MAC_ADDRESS => _tF('i know mac', 'station'),
                    Station::SERIAL_NUMBER => _tF('i know serial number', 'station'),
                ],
                    [
                        'item' => function ($index, $label, $name, $checked, $value)
                        {
                            $checkMarkup = $checked ? 'checked="checked"' : '';
                            $return = '<div class="checkbox"><label>';
                            $return .= "<input type='radio' name='{$name}' value='{$value}' $checkMarkup />";
                            $return .= DactylKit::chip($label);
                            $return .= '</label></div>';

                            return $return;
                        },
                        'class' => 'hardware_identifier_radio',
                    ])->label(false); ?>
            </div>
            <div class="col-md-6 px-0 footer-bumper">
                <?= $form->field($modelStation, "macAddress")->textInput()->hint(false) ?>
                <?= $form->field($modelStation, "serialNumber")->textInput()->hint(false) ?>
            </div>
        </div>
        <?= $this->render('../partial/footer', [
            'prevUrl' => url(['station/step-back', 'id' => $modelStation->id]),
            'nextBtnLabel' => _tF('save and continue', 'station'),
            'dirtyNextBtnLabel' => _tF('save and recount', 'station'),
            'continueBtn' => true,
        ]) ?>
    </div>
<?php if ($modelStation->isPublished()): ?>
    <script>initDirtyForms(true);</script>
<?php else: ?>
    <script>initDirtyForms(false);</script>
<?php endif; ?>

<?php ActiveForm::end() ?>