<?php
/*
* Copyright 2023 DactylGroup s.r.o.
*
* Licensed under the EUPL, Version 1.2 or – as soon they
will be approved by the European Commission - subsequent
versions of the EUPL (the "Licence");
* You may not use this work except in compliance with the
Licence.
* You may obtain a copy of the Licence at:
*
* https://joinup.ec.europa.eu/software/page/eupl5
*
* Unless required by applicable law or agreed to in
writing, software distributed under the Licence is
distributed on an "AS IS" basis,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
express or implied.
* See the Licence for the specific language governing
permissions and limitations under the Licence.
*/
?>


<?php

/**
 * @var $steps array
 * @var $modelStation \common\models\Station
 * @var $modelStationType \common\models\StationWigig
 * @var $hasChanged string
 */

use frontend\assets\map\AddStation52Asset;
use frontend\widgets\ActiveForm;
use frontend\widgets\dactylkit\DactylKit;

?>

<?php \yii\widgets\PjaxAsset::register($this) ?>

<?php $form = ActiveForm::begin([
    'id' => 'station-form',
    'pjax' => true,
    'pjaxOptions' => [
        'enablePushState' => true,
        'enableReplaceState' => true,
    ],
    'options' => [
        'data-url' => url(['station/edit', 'id' => $modelStation->id]),
        'data-changed' => $hasChanged,
        'data-id' => $modelStation->id,
    ],
    'action' => url(['station/edit', 'id' => $modelStation->id]),
]);

AddStation52Asset::register($this);
?>
    <script>
        var urlToPush = $("form#station-form").data('url');
        var hasChanged = $("form#station-form").data('changed') + "";
        if (history.pushState && urlToPush) {
            history.pushState(null, null, urlToPush); // URL is now /inbox/N
        }

        needsToReload = needsToReload || (hasChanged === 'true');
    </script>
    <div class="livebox--station-registration">
        <?= $this->render('../partial/header', ['model' => $modelStation, 'saveExit' => true]) ?>
        <div class="livebox--station-registration--content">
            <?= DactylKit::progressBar(3, $steps) ?>
            <h6><?= $modelStation->getAttributeLabel('macAddress') ?></h6>
            <?= $form->field($modelStation, "macAddress")->textInput()->hint(false) ?>
        </div>
        <?= $this->render('../partial/footer', [
            'prevUrl' => url(['station/step-back', 'id' => $modelStation->id]),
            'nextBtnLabel' => _tF('save and continue', 'station'),
            'dirtyNextBtnLabel' => _tF('save and recount', 'station'),
            'continueBtn' => true,
        ]) ?>
    </div>
<?php if ($modelStation->isPublished()): ?>
    <script>initDirtyForms(true);</script>
<?php else: ?>
    <script>initDirtyForms(false);</script>
<?php endif; ?>

<?php ActiveForm::end() ?>