<?php
/*
* Copyright 2023 DactylGroup s.r.o.
*
* Licensed under the EUPL, Version 1.2 or – as soon they
will be approved by the European Commission - subsequent
versions of the EUPL (the "Licence");
* You may not use this work except in compliance with the
Licence.
* You may obtain a copy of the Licence at:
*
* https://joinup.ec.europa.eu/software/page/eupl5
*
* Unless required by applicable law or agreed to in
writing, software distributed under the Licence is
distributed on an "AS IS" basis,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
express or implied.
* See the Licence for the specific language governing
permissions and limitations under the Licence.
*/
?>


<?php

use common\models\search\StationSearch;
use common\models\StationFs;
use common\models\StationFsPair;
use dactylcore\core\data\ActiveDataProvider;
use dactylcore\core\widgets\common\pjax\Pjax;
use frontend\assets\map\CalcFsAsset;
use frontend\controllers\StationController;
use common\models\Station;
use frontend\widgets\ActiveForm;
use frontend\widgets\dactylkit\DactylKit;

/**
 * @var $userId int
 * @var $model StationFsPair
 * @var $modelStation Station
 * @var $dataProvider ActiveDataProvider
 * @var $searchModel StationSearch
 * @var $anyInConflict boolean - is there any conflict with other stations
 * @var $canEdit boolean - can this user can edit/modify this station
 * @var $bData array
 * @var $startedConversationsWith array
 * @var $steps array
 * @var $hasChanged string
 */

$modelStationA = $model->stationA;
$modelStationB = $model->stationB;
$modelStationAType = $modelStationA->getStationTypeObject();
$modelStationBType = $modelStationB->getStationTypeObject();

$action = $anyInConflict ? 'station/declaration' : 'station/publish';
if ($modelStation->isForProlongation()) {
    if ($anyInConflict) {
        $nextBtnLabel = _tF('declare and prolong', 'station');
    } else {
        $nextBtnLabel = _tF('prolong station', 'station');
    }
} else {
    if ($modelStation->isPublished()) {
        $nextBtnLabel = '';
    } else {
        $nextBtnLabel = $anyInConflict ? _tF('isolation declaration', 'station') : _tF('publish', 'station');
    }
}

/* decides if last step in progress-bar will be shown as completed or in progress */
$progressStep = $modelStation->status == Station::STATUS_FINISHED ? 5 : 4;

// GPS other format
$dmsLngA = $modelStationA->DDtoDMS(false);
$dmsLatA = $modelStationA->DDtoDMS(true);

$dmsLngB = $modelStationB->DDtoDMS(false);
$dmsLatB = $modelStationB->DDtoDMS(true);

?>

<?php
$form = ActiveForm::begin([
    'id' => 'station-form',
    'pjax' => true,
    'pjaxOptions' => [
        'enablePushState' => false,
        'enableReplaceState' => false,
    ],
    'options' => [
        'data-url' => url(['station/edit', 'id' => $modelStationA->id]),
        'data-changed' => (string)$hasChanged,
    ],
    'action' => url([$action, 'id' => $modelStation->id]),
]);
CalcFsAsset::register($this);
?>
<script>
    var urlToPush = $("form#station-form").data('url');
    var hasChanged = $("form#station-form").data('changed') + "";

    if (history.pushState && urlToPush) {
        history.pushState(null, null, urlToPush); // URL is now /inbox/N
    }
    needsToReload = needsToReload || (hasChanged === 'true');
</script>
<div class="livebox--station-registration">
    <?= $this->render('../partial/header', ['model' => $modelStation, 'saveExit' => true]) ?>
  <div class="livebox--station-registration--content">
      <?= DactylKit::progressBar($progressStep, $steps) ?>
    <span class="dk--table--ghost__heading"><?= _tF('information', 'station') ?></span>
    <table class="dk--table dk--table--ghost">
      <tbody>
      <tr>
        <td><?= _tF('kind', 'station') ?></td>
        <td><?= $modelStation->type == StationController::TYPE_FS ? _tF('station type fs', 'station') :
                ($modelStation->antenna_volume >= 25 ? _tF('station type wigig ptp', 'station') :
                    _tF('station type wigig ptmp', 'station')) ?></td>
      </tr>
      </tbody>
    </table>
    <hr class="dk--hr">
    <span class="dk--table--ghost__heading"><?= _tF('location', 'station') ?></span>
    <div class="dk--table--scroll-holder">
      <table class="dk--table dk--table--ghost">
        <thead>
        <tr>
          <th></th>
          <th><?= _tF('Station A', 'station') ?></th>
          <th><?= _tF('Station B', 'station') ?></th>
        </tr>
        </thead>
        <tbody>
        <tr>
          <td style="padding-top: 17px"><?= _tF('GPS', 'station') ?></td>
          <td style="padding-top: 17px">
            <span <?= DactylKit::tooltip("{$dmsLngA['deg']}° {$dmsLngA['min']}' " .
                round($dmsLngA['sec']) .
                "''") ?> style="margin:0">
                <?= $modelStationA->lng ?>°,
            </span>
            <span <?= DactylKit::tooltip("{$dmsLatA['deg']}° {$dmsLatA['min']}' " .
                round($dmsLatA['sec']) .
                "''") ?> style="margin:0">
                <?= $modelStationA->lat ?>°
            </span>
          </td>
          <td style="padding-top: 17px">
            <span <?= DactylKit::tooltip("{$dmsLngB['deg']}° {$dmsLngB['min']}' " .
                round($dmsLngB['sec']) .
                "''") ?> style="margin:0">
                <?= $modelStationB->lng ?>°,
            </span>
            <span <?= DactylKit::tooltip("{$dmsLatB['deg']}° {$dmsLatB['min']}' " .
                round($dmsLatB['sec']) .
                "''") ?> style="margin:0">
                <?= $modelStationB->lat ?>°
            </span>
          </td>
        </tr>
        </tbody>
      </table>
    </div>
    <div style="width: 100%; height: 400px">
      <div id="map-calc-fs" style="width: 100%; height: 100%;  min-height: 250px;"
           data-stationlink="<?= url(['station/station', 'id' => 0]) ?>">
        <div class="mapOverlay overlayCalcFs">
          <div class="lds-grid">
            <div></div>
            <div></div>
            <div></div>
            <div></div>
            <div></div>
            <div></div>
            <div></div>
            <div></div>
            <div></div>
          </div>
        </div>
      </div>
    </div>
    <hr class="dk--hr">
    <span class="dk--table--ghost__heading"><?= _tF('parameters', 'station') ?></span>
    <div class="dk--table--scroll-holder">
      <table class="dk--table dk--table--ghost">
        <thead>
        <tr>
          <th></th>
          <th><?= _tF('Station A', 'station') ?></th>
          <th><?= _tF('Station B', 'station') ?></th>
        </tr>
        </thead>
        <tbody>
        <tr>
          <td><?= _tF('antenna gain', 'station') ?><span <?= DactylKit::tooltip(_tF('antenna_volume',
                  'station_hint')) ?> ><?= DactylKit::icon(DactylKit::ICON_INFO_20) ?></span></td>
          <td><?= Station::formatNumber($modelStationA->antenna_volume) ?? '-' ?> dBi</td>
          <td><?= Station::formatNumber($modelStationB->antenna_volume) ?? '-' ?> dBi</td>
        </tr>
        <tr>
          <td><?= _tF('power_label', 'station') ?><span <?= DactylKit::tooltip(_tF('power',
                  'station_hint')) ?> ><?= DactylKit::icon(DactylKit::ICON_INFO_20) ?></span></td>
          <td><?= Station::formatNumber($modelStationA->power) ?? '-' ?> dBm</td>
          <td><?= Station::formatNumber($modelStationB->power) ?? '-' ?> dBm</td>
        </tr>
        <tr>
          <td><?= _tF('bandwidth', 'station') ?><span <?= DactylKit::tooltip(_tF('channel_width',
                  'station_hint')) ?> ><?= DactylKit::icon(DactylKit::ICON_INFO_20) ?></span></td>
          <td><?= Station::formatNumber($modelStationA->channel_width) ?? '-' ?> MHz</td>
          <td><?= Station::formatNumber($modelStationB->channel_width) ?? '-' ?> MHz</td>
        </tr>
        <tr>
          <td><?= _tF('frequency_label', 'station') ?><span <?= DactylKit::tooltip(_tF('frequency',
                  'station_hint')) ?> ><?= DactylKit::icon(DactylKit::ICON_INFO_20) ?></span></td>
          <td><?= Station::formatNumber($modelStationAType->frequency) ?? '-' ?> MHz</td>
          <td><?= Station::formatNumber($modelStationBType->frequency) ?? '-' ?> MHz</td>
        </tr>
        <tr>
          <td><?= _tF('ratio_signal_interference_label', 'station') ?>
            <span <?= DactylKit::tooltip(_tF('ratio_signal_interference',
                'station_hint')) ?> ><?= DactylKit::icon(DactylKit::ICON_INFO_20) ?></span></td>
          <td><?= StationFs::$qamValues[$modelStationAType->ratio_signal_interference] ?></td>
          <td><?= StationFs::$qamValues[$modelStationBType->ratio_signal_interference] ?></td>
        </tr>
        <?php if ($modelStationA->canEdit()): ?>
          <tr>
              <?php if ($modelStationA->hardware_identifier == Station::SERIAL_NUMBER) : ?>
                <td><?= $modelStationA->getAttributeLabel('serial_number') ?></td>
                <td><?= $modelStationA->serial_number ?? '' ?></td>
                <td><?= $modelStationB->serial_number ?? '' ?></td>
              <?php else: ?>
                <td><?= $modelStationA->getAttributeLabel('mac_address') ?></td>
                <td><?= $modelStationA->mac_address ?? '' ?></td>
                <td><?= $modelStationB->mac_address ?? '' ?></td>
              <?php endif; ?>
            </td>
          </tr>
        <?php endif; ?>
        </tbody>
      </table>
    </div>
    <hr id="vueappfs-calc" class="dk--hr">
    <h5 class="mt-5"><?= _tF('affected stations', 'station') ?></h5>
    <span class="livebox--station-registration--content__subheading"><?= _tF('Coordinator calculator table heading',
            'station') ?></span>
    <div class="dk--table--scroll-holder">
        <?= $this->render('../step5_summary_table', [
            'startedConversationsWith' => $startedConversationsWith,
            'dataProvider' => $dataProvider,
            'searchModel' => $searchModel,
            'model' => $modelStation,
            'bData' => $bData,
            'showFilter' => false,
            'canEdit' => true,
        ]); ?>
    </div>
  </div>
    <?= $this->render('../partial/footer', [
        'prevUrl' => url(['station/step-back', 'id' => $modelStation->id]),
        'nextBtnLabel' => $nextBtnLabel,
        'dirtyNextBtnLabel' => '',
        'continueBtn' => false,
    ]) ?>
</div>
<script>
    if (window['initCalcFs']) {
        initCalcFs();
    }
    getAllFlashAlertMessages();

    $("#coordination-calc-summary").on('pjax:start', function () {
        $("#coordination-calc-summary #loading").fadeIn();
    }).on('pjax:end', function () {
        $("#coordination-calc-summary #loading").fadeOut();
    });
</script>
<?php ActiveForm::end() ?>

