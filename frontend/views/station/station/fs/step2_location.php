<?php
/*
* Copyright 2023 DactylGroup s.r.o.
*
* Licensed under the EUPL, Version 1.2 or – as soon they
will be approved by the European Commission - subsequent
versions of the EUPL (the "Licence");
* You may not use this work except in compliance with the
Licence.
* You may obtain a copy of the Licence at:
*
* https://joinup.ec.europa.eu/software/page/eupl5
*
* Unless required by applicable law or agreed to in
writing, software distributed under the Licence is
distributed on an "AS IS" basis,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
express or implied.
* See the Licence for the specific language governing
permissions and limitations under the Licence.
*/
?>


<?php

//Uses
use frontend\assets\map\AddFsAsset;
use common\models\Station;
use frontend\widgets\ActiveForm;
use frontend\widgets\dactylkit\DactylKit;

/**
 * @var $steps array
 * @var $modelStationA Station
 * @var $modelStationB Station
 * @var $modelStationAType \common\models\StationFs
 * @var $modelStationBType \common\models\StationFs
 * @var $hasChanged string
 */

$a = Station::POSITION_A;
$b = Station::POSITION_B;

?>

<?php $form = ActiveForm::begin([
    'enableAjaxValidation' => true,
    'id' => 'station-form',
    'pjax' => true,
    'pjaxOptions' => [
        'enablePushState' => true,
        'enableReplaceState' => true,
    ],
    'options' => [
        'data-url' => url(['station/edit', 'id' => $modelStationA->id]),
        'data-changed' => $hasChanged,
    ],
    'action' => url(['station/edit', 'id' => $modelStationA->id]),
]);
AddFsAsset::register($this);

?>

    <script>
        var urlToPush = $("form#station-form").data('url');
        var hasChanged = $("form#station-form").data('changed') + "";
        if (history.pushState && urlToPush) {
            history.pushState(null, null, urlToPush); // URL is now /inbox/N
        }

        needsToReload = needsToReload || (hasChanged === 'true');
    </script>
    <div class="livebox--station-registration">
        <?= $this->render('../partial/header', ['model' => $modelStationA, 'saveExit' => false]) ?>
        <div class="livebox--station-registration--content map-block" id="vueappfs">
            <?= DactylKit::progressBar(2, $steps) ?>
            <h6><?= _tF('Where are stations located', 'station') ?></h6>
            <span class="livebox--station-registration--content__subheading"><?= _tF('Use map to select location or enter GPS coordinates',
                    'station') ?></span>
            <div class="row livebox--station-registration--content__location-inputs fs-address-inputs">
                <div class="col-md-6">
                    <div class="row">
                        <div class="col-6 first-input-column">
                            <?= $form->field($modelStationA, "[{$a}]lat")->textInput([
                                'v-model' => 'locationLatA',
                                'maxlength' => true,
                                '@change' => 'changeA',
                                'class' => 'dangerous',
                            ])->label(_tF('lat_a', 'station')) ?>
                        </div>
                        <div class="col-6 second-input-column">
                            <?= $form->field($modelStationA, "[{$a}]lng")->textInput([
                                'maxlength' => true,
                                'v-model' => 'locationLngA',
                                '@change' => 'changeA',
                                'class' => 'dangerous',
                            ])->label(_tF('lng_a', 'station')) ?>
                        </div>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="row">
                        <div class="col-6 first-input-column">
                            <?= $form->field($modelStationB, "[{$b}]lat")->textInput([
                                'v-model' => 'locationLatB',
                                'maxlength' => true,
                                'class' => 'dangerous',
                                '@change' => 'changeB',
                            ])->label(_tF('lat_b', 'station')) ?>
                        </div>
                        <div class="col-6 second-input-column">
                            <?= $form->field($modelStationB, "[{$b}]lng")->textInput([
                                'maxlength' => true,
                                'v-model' => 'locationLngB',
                                'class' => 'dangerous',
                                '@change' => 'changeB',
                            ])->label(_tF('lng_b', 'station')) ?>
                        </div>
                    </div>
                </div>
            </div>

            <div class="map-distance-checker" v-bind:class="{ 'has-error': distanceWarning, 'hidden': distanceHide}">
                <div class="map-distance-checker__container">
                    <span class="map-distance-checker__container__icon">
                        <?= file_get_contents(Yii::getAlias('@frontend/web/source_assets/img/icon/ic-ruler-24.svg')) ?>
                    </span>
                    <span class="map-distance-checker__container__distance">{{distance}} km</span>
                </div>
            </div>
            <?= $this->render('../../../site/partial/switcher') ?>
        </div>

        <div class="map-outer-box" style="width: 100%; height: 100%">
            <div id="map-fs" style="width: 100%; height: 100%; min-height: 250px;"
                 data-stationlink="<?= url(['station/station', 'id' => 0]) ?>">
                <div class="mapOverlay overlayFs">
                    <div class="lds-grid">
                        <div></div>
                        <div></div>
                        <div></div>
                        <div></div>
                        <div></div>
                        <div></div>
                        <div></div>
                        <div></div>
                        <div></div>
                    </div>
                </div>
            </div>
        </div>

        <script>
            if (window['initFs']) {
                initFs();
            }
        </script>
        <?= $this->render('../partial/footer', [
            'prevUrl' => url(['station/step-back', 'id' => $modelStationA->id]),
            'nextBtnLabel' => _tF('save and continue', 'station'),
            'dirtyNextBtnLabel' => _tF('save and recount', 'station'),
            'continueBtn' => true,
        ]) ?>
    </div>
<?php if ($modelStationA->isPublished()): ?>
  <script>initDirtyForms(true);</script>
<?php else: ?>
  <script>initDirtyForms(false);</script>
<?php endif; ?>
<?php ActiveForm::end() ?>