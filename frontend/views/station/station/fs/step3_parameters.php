<?php
/*
* Copyright 2023 DactylGroup s.r.o.
*
* Licensed under the EUPL, Version 1.2 or – as soon they
will be approved by the European Commission - subsequent
versions of the EUPL (the "Licence");
* You may not use this work except in compliance with the
Licence.
* You may obtain a copy of the Licence at:
*
* https://joinup.ec.europa.eu/software/page/eupl5
*
* Unless required by applicable law or agreed to in
writing, software distributed under the Licence is
distributed on an "AS IS" basis,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
express or implied.
* See the Licence for the specific language governing
permissions and limitations under the Licence.
*/
?>


<?php

use common\models\StationFs;
use dactylcore\core\widgets\common\select2\Select2;
use common\models\Station;
use frontend\widgets\ActiveForm;
use frontend\widgets\dactylkit\DactylKit;

/**
 * @var $steps array
 * @var $modelStationA Station
 * @var $modelStationB Station
 * @var $modelStationAType StationFs
 * @var $modelStationBType StationFs
 * @var $hasChanged string
 */

$a = Station::POSITION_A;
$b = Station::POSITION_B;

?>

<?php $form = ActiveForm::begin([
    'id' => 'station-form',
    'pjax' => true,
    'pjaxOptions' => [
        'enablePushState' => true,
        'enableReplaceState' => true,
    ],
    'options' => [
        'data-url' => url(['station/edit', 'id' => $modelStationA->id]),
        'data-changed' => (string)$hasChanged,
        'data-id' => $modelStationA->id,
    ],
    'action' => url(['station/edit', 'id' => $modelStationA->id]),
]);
?>
    <script>
        var urlToPush = $("form#station-form").data('url');
        var hasChanged = $("form#station-form").data('changed') + "";

        if (history.pushState && urlToPush) {
            history.pushState(null, null, urlToPush); // URL is now /inbox/N
        }
        needsToReload = needsToReload || (hasChanged === 'true');
    </script>
    <div class="livebox--station-registration">
        <?= $this->render('../partial/header', ['model' => $modelStationA, 'saveExit' => true]) ?>
        <div class="livebox--station-registration--content">
            <?= DactylKit::progressBar(3, $steps) ?>
            <div class="row no-gutters footer-bumper">
                <div class="col-md-6 fs_step4">
                    <h5><?= _tF('Station A', 'station') ?></h5>
                    <h6><?= _tF('What is antenna gain and power', 'station') ?></h6>
                    <?= $form->field($modelStationA, "[{$a}]antenna_volume")->textInput(["maxlength" => true]) ?>
                    <?= $form->field($modelStationA, "[{$a}]power")->textInput([
                        "maxlength" => true,
                        'class' => 'dangerous',
                    ]); ?>
                    <h6><?= _tF('What is the channel bandwidth', 'station') ?></h6>
                    <?= $form->field($modelStationA, "[{$a}]channel_width")->textInput([
                        "maxlength" => true,
                        'class' => 'dangerous',
                    ]) ?>
                    <h6><?= _tF('What is the medium frequency', 'station') ?></h6>
                    <?= $form->field($modelStationAType, "[{$a}]frequency")->textInput([
                        "maxlength" => true,
                        'class' => 'dangerous',
                    ]) ?>
                    <h6><?= _tF('What is the desired signal to interference ratio?', 'station') ?></h6>
                    <?= $form->field($modelStationAType, "[{$a}]ratio_signal_interference")->widget(Select2::class, [
                        'data' => StationFs::$qamValues,
                    ])->hint(false); ?>
                    <h6><?= _tF('Which of these identifiers do you know', 'station') ?></h6>
                    <div class="row-no-gutters">
                        <?= $form->field($modelStationA, "[{$a}]hardware_identifier",
                            ['options' => ['class' => 'hardware_identifier_radio_a']])->radioList([
                            Station::MAC_ADDRESS => _tF('i know mac', 'station'),
                            Station::SERIAL_NUMBER => _tF('i know serial number', 'station'),
                        ],
                            [
                                'item' => function ($index, $label, $name, $checked, $value)
                                {
                                    $checkMarkup = $checked ? 'checked="checked"' : '';
                                    $return = '<div class="checkbox"><label>';
                                    $return .= "<input type='radio' name='{$name}' value='{$value}' $checkMarkup />";
                                    $return .= DactylKit::chip($label);
                                    $return .= '</label></div>';

                                    return $return;
                                },
                                'class' => 'hardware_identifier_radio',
                            ])->label(false); ?>
                    </div>
                    <?= $form->field($modelStationA, "[{$a}]macAddress")
                             ->textInput(['id' => 'modelStationA_mac-input'])
                             ->hint(false) ?>
                    <?= $form->field($modelStationA, "[{$a}]serialNumber")
                             ->textInput(['id' => 'modelStationA_sn-input'])
                             ->hint(false) ?>
                </div>
                <div class="col-md-6 fs_step4">
                    <h5 class="dk--h5-mt-mobile"><?= _tF('Station B', 'station') ?></h5>
                    <div class="dk--h6-spacer"></div>
                    <?= $form->field($modelStationB, "[{$b}]antenna_volume")->textInput(["maxlength" => true]) ?>
                    <?= $form->field($modelStationB, "[{$b}]power")->textInput([
                        "maxlength" => true,
                        'class' => 'dangerous',
                    ]); ?>
                    <div class="dk--h6-spacer"></div>
                    <?= $form->field($modelStationB, "[{$b}]channel_width")->textInput([
                        "maxlength" => true,
                        'class' => 'dangerous',
                    ]) ?>
                    <div class="dk--h6-spacer"></div>
                    <?= $form->field($modelStationBType, "[{$b}]frequency")->textInput([
                        "maxlength" => true,
                        'class' => 'dangerous',
                    ]) ?>
                    <div class="dk--h6-spacer"></div>
                    <?= $form->field($modelStationBType, "[{$b}]ratio_signal_interference")->widget(Select2::class, [
                        'data' => StationFs::$qamValues,
                    ])->hint(false); ?>
                    <div class="dk--h6-spacer"></div>
                    <div class="row-no-gutters">
                        <?= $form->field($modelStationB, "[{$b}]hardware_identifier",
                            ['options' => ['class' => 'hardware_identifier_radio_b']])->radioList([
                            Station::MAC_ADDRESS => _tF('i know mac', 'station'),
                            Station::SERIAL_NUMBER => _tF('i know serial number', 'station'),
                        ],
                            [
                                'item' => function ($index, $label, $name, $checked, $value)
                                {
                                    $checkMarkup = $checked ? 'checked="checked"' : '';
                                    $return = '<div class="checkbox"><label>';
                                    $return .= "<input type='radio' name='{$name}' value='{$value}' $checkMarkup />";
                                    $return .= DactylKit::chip($label);
                                    $return .= '</label></div>';

                                    return $return;
                                },
                                'class' => 'hardware_identifier_radio',
                            ])->label(false); ?>
                    </div>
                    <?= $form->field($modelStationB, "[{$b}]macAddress")
                             ->textInput(['id' => 'modelStationB_mac-input'])
                             ->hint(false) ?>
                    <?= $form->field($modelStationB, "[{$b}]serialNumber")
                             ->textInput(['id' => 'modelStationB_sn-input'])
                             ->hint(false) ?>
                </div>
            </div>
        </div>

        <?= $this->render('../partial/footer', [
            'prevUrl' => url(['station/step-back', 'id' => $modelStationA->id]),
            'nextBtnLabel' => _tF('save and continue', 'station'),
            'dirtyNextBtnLabel' => _tF('save and recount', 'station'),
            'continueBtn' => true,
        ]) ?>
    </div>
<?php if ($modelStationA->isPublished()): ?>
    <script>initDirtyForms(true);</script>
<?php else: ?>
    <script>initDirtyForms(false);</script>
<?php endif; ?>
<?php ActiveForm::end() ?>