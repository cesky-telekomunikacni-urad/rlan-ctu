<?php
/*
* Copyright 2023 DactylGroup s.r.o.
*
* Licensed under the EUPL, Version 1.2 or – as soon they
will be approved by the European Commission - subsequent
versions of the EUPL (the "Licence");
* You may not use this work except in compliance with the
Licence.
* You may obtain a copy of the Licence at:
*
* https://joinup.ec.europa.eu/software/page/eupl5
*
* Unless required by applicable law or agreed to in
writing, software distributed under the Licence is
distributed on an "AS IS" basis,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
express or implied.
* See the Licence for the specific language governing
permissions and limitations under the Licence.
*/
?>


<?php

use common\models\Station;
use frontend\widgets\ActiveForm;
use frontend\widgets\dactylkit\DactylKit;

/**
 * @var $steps array
 * @var $modelStationA Station
 * @var $modelStationB Station
 * @var $modelStationAType \common\models\StationFs
 * @var $modelStationBType \common\models\StationFs
 * @var $hasChanged string
 */

$a = Station::POSITION_A;
$b = Station::POSITION_B;

?>

<?php
$form = ActiveForm::begin([
    'id' => 'station-form',
    'pjax' => true,
    'pjaxOptions' => [
        'enablePushState' => true,
        'enableReplaceState' => true,
    ],
    'options' => [
        'data-url' => $modelStationA->id ? url(['station/edit', 'id' => $modelStationA->id]) : '',
        'data-changed' => $hasChanged,
    ],
    'action' => url($modelStationA->id ? ['station/edit', 'id' => $modelStationA->id] : ['station/create-fs'])
]) ?>
    <script>
        // TODO experimentalni, zatim nechame tady, pak muzeme presunout do bundlu
        var urlToPush = $("form#station-form").data('url');
        var hasChanged = $("form#station-form").data('changed') + "";
        if (history.pushState && urlToPush) {
            history.pushState(null, null, urlToPush); // URL is now /inbox/N
        }

        needsToReload = needsToReload || (hasChanged === 'true');
    </script>
    <div id="station-form-pjax">
        <div class="livebox--station-registration">
            <?= $this->render('../partial/header', ['model' => $modelStationA, 'saveExit' => false]) ?>
            <div class="livebox--station-registration--content">
                <?= DactylKit::progressBar(1, $steps) ?>
                <h6><?= _tF('What is station name', 'station') ?></h6>
                <?= $form->field($modelStationA, "[{$a}]name")->textInput([
                    'maxlength' => true,
                    'class' => 'get-mirror-name footer-bumper',
                ]) ?>
                <?= $form->field($modelStationB, "[{$b}]name")
                         ->hiddenInput(['class' => 'mirror-name'])
                         ->label(false)->error(false); ?>
            </div>
            <?= $this->render('../partial/footer', [
                'prevUrl' => '',
                'nextBtnLabel' => _tF('save and continue', 'station'),
                'dirtyNextBtnLabel' => _tF('save and recount', 'station'),
                'continueBtn' => true,
            ]) ?>
        </div>
    </div>

<?php if ($modelStationA->isPublished()): ?>
  <script>initDirtyForms(true);</script>
<?php else: ?>
  <script>initDirtyForms(false);</script>
<?php endif; ?>
<?php ActiveForm::end() ?>

