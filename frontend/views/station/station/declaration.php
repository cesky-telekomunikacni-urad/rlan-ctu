<?php
/*
* Copyright 2023 DactylGroup s.r.o.
*
* Licensed under the EUPL, Version 1.2 or – as soon they
will be approved by the European Commission - subsequent
versions of the EUPL (the "Licence");
* You may not use this work except in compliance with the
Licence.
* You may obtain a copy of the Licence at:
*
* https://joinup.ec.europa.eu/software/page/eupl5
*
* Unless required by applicable law or agreed to in
writing, software distributed under the Licence is
distributed on an "AS IS" basis,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
express or implied.
* See the Licence for the specific language governing
permissions and limitations under the Licence.
*/
?>


<?php

use common\models\Station;
use frontend\widgets\ActiveForm;
use frontend\widgets\dactylkit\DactylKit;
use yii\helpers\Html;

/**
 * @var $modelStation Station
 * @var $steps array
 */

?>

<?php $form = ActiveForm::begin([
    'id' => 'station-form',
    'pjax' => false,
    'pjaxOptions' => [
        'enablePushState' => false,
        'enableReplaceState' => false,
    ],
    'action' => url(['station/declare', 'id' => $modelStation->id]),
]) ?>
<div class="livebox--station-registration">
    <?= $this->render('partial/header', ['model' => $modelStation, 'saveExit' => true]) ?>
    <div class="livebox--station-registration--content">
        <?= DactylKit::progressBar(4, $steps) ?>
        <?= c('DECLARATION_TEXT') ?>
      <div class="footer-bumper"></div>
    </div>
    <?= $this->render('partial/footer', [
        'prevUrl' => url(['station/edit', 'id' => $modelStation->id]),
        'nextBtnLabel' => _tF('declare', 'station'),
        'dirtyNextBtnLabel' => '',
        'continueBtn' => false,
    ]) ?>
</div>
<?php ActiveForm::end() ?>
