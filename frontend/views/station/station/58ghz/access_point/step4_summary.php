<?php
/*
* Copyright 2023 DactylGroup s.r.o.
*
* Licensed under the EUPL, Version 1.2 or – as soon they
will be approved by the European Commission - subsequent
versions of the EUPL (the "Licence");
* You may not use this work except in compliance with the
Licence.
* You may obtain a copy of the Licence at:
*
* https://joinup.ec.europa.eu/software/page/eupl5
*
* Unless required by applicable law or agreed to in
writing, software distributed under the Licence is
distributed on an "AS IS" basis,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
express or implied.
* See the Licence for the specific language governing
permissions and limitations under the Licence.
*/
?>


<?php

/**
 * @var $dataProvider
 * @var $searchModel
 * @var $model Station
 * @var $modelStation Station
 * @var $startedConversationsWith array
 * @var $bData array
 * @var $steps array
 * @var $hasChanged string
 * @var $anyInConflict boolean - is there any conflict with other stations
 */

use frontend\assets\map\CalcAccessPointAsset;
use frontend\controllers\StationController;
use common\models\Station;
use frontend\widgets\ActiveForm;
use frontend\widgets\dactylkit\DactylKit;

$model = $modelStation;

$modelLabels = $model->attributeLabels();

if ($modelStation->isForProlongation()) {
    $nextBtnLabel = $anyInConflict ? '' : _tF('prolong station', 'station');
} else {
    if ($modelStation->status == Station::STATUS_FINISHED) {
        $nextBtnLabel = '';
    } else {
        $nextBtnLabel = $anyInConflict ? '' : _tF('publish', 'station');
    }
}

/* decides if last step in progress-bar will be shown as completed or in progress */
$progressStep = $modelStation->status == Station::STATUS_FINISHED ? 4 : 3;

$dmsLng = $modelStation->DDtoDMS(false);
$dmsLat = $modelStation->DDtoDMS(true);
?>

<?php $form = ActiveForm::begin([
    'id' => 'station-form',
    'pjax' => true,
    'pjaxOptions' => [
        'enablePushState' => true,
        'enableReplaceState' => true,
    ],
    'options' => [
        'data-url' => url(['station/edit', 'id' => $modelStation->id]),
        'data-changed' => (string)$hasChanged,
    ],
    'action' => url(['station/publish', 'id' => $modelStation->id]),
]);
CalcAccessPointAsset::register($this);
?>
<script>
    var urlToPush = $("form#station-form").data('url');
    var hasChanged = $("form#station-form").data('changed') + "";

    if (history.pushState && urlToPush) {
        history.pushState(null, null, urlToPush); // URL is now /inbox/N
    }
    needsToReload = needsToReload || (hasChanged === 'true');
</script>
<div class="livebox--station-registration">
    <?= $this->render('../../partial/header', ['model' => $model, 'saveExit' => true]) ?>
    <div class="livebox--station-registration--content">
        <?= DactylKit::progressBar($progressStep, $steps) ?>
        <span class="dk--table--ghost__heading"><?= _tF('information', 'station') ?></span>
        <table class="dk--table dk--table--ghost">
            <tbody>
            <tr>
                <td><?= _tF('SSID', 'station') ?></td>
                <td><?= $modelStation->name ?></td>
            </tr>
            <tr>
                <td><?= _tF('kind', 'station') ?></td>
                <td><?= $modelStation->getTypeShortCut() ?></td>
            </tr>
            <?php if ($modelStation->mac_address): ?>
                <tr>
                    <td><?= $modelLabels['mac_address'] ?></td>
                    <td><?= mb_strtoupper($modelStation->mac_address) ?></td>
                </tr>
            <?php endif; ?>
            </tbody>
        </table>
        <hr id="vueapp-calc-ap" class="dk--hr">
        <span class="dk--table--ghost__heading"><?= _tF('location', 'station') ?></span>
        <table class="dk--table dk--table--ghost">
            <tbody>
            <tr>
                <td>
                   <span <?= DactylKit::tooltip("{$dmsLng['deg']}° {$dmsLng['min']}' " . round($dmsLng['sec']) . "''",
                       DactylKit::TOOLTIP_TOP) ?> style="margin:0">
                        <?= $model->lng ?>°,
                  </span>
                    <span <?= DactylKit::tooltip("{$dmsLat['deg']}° {$dmsLat['min']}' " . round($dmsLat['sec']) . "''",
                        DactylKit::TOOLTIP_TOP) ?> style="margin:0">
                        <?= $model->lat ?>°
                  </span>
                </td>
            </tr>
            </tbody>
        </table>
        <?php if ($anyInConflict) : ?>

            <?= DactylKit::alert(
                _tF('Cannot place here', 'station'),
                _tF('This AP is in conflict, please move it.', 'station'),
                DactylKit::ALERT_TYPE_CRITICAL
            ) ?>

        <?php endif; ?>
        <div style="width: 100%; height: 400px">
            <div id="map-calc-ap" style="width: 100%; height: 100%;  min-height: 250px;"
                 data-stationlink="<?= url(['station/station', 'id' => 0]) ?>"
                 data-radius="<?= c('TOLL_GATE_RADIUS') ?>">
                <div class="mapOverlay overlayCalcAP">
                    <div class="lds-grid">
                        <div></div>
                        <div></div>
                        <div></div>
                        <div></div>
                        <div></div>
                        <div></div>
                        <div></div>
                        <div></div>
                        <div></div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <?= $this->render('../../partial/footer', [
        'prevUrl' => url(['station/step-back', 'id' => $modelStation->id]),
        'nextBtnLabel' => $nextBtnLabel,
        'continueBtn' => false,
        'dirtyNextBtnLabel' => '',
    ]) ?>
</div>
<script>
    getAllFlashAlertMessages();
    if (window['initCalcAccessPoint']) {
        initCalcAccessPoint();
    }
</script>
<?php ActiveForm::end() ?>
