<?php
/*
* Copyright 2023 DactylGroup s.r.o.
*
* Licensed under the EUPL, Version 1.2 or – as soon they
will be approved by the European Commission - subsequent
versions of the EUPL (the "Licence");
* You may not use this work except in compliance with the
Licence.
* You may obtain a copy of the Licence at:
*
* https://joinup.ec.europa.eu/software/page/eupl5
*
* Unless required by applicable law or agreed to in
writing, software distributed under the Licence is
distributed on an "AS IS" basis,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
express or implied.
* See the Licence for the specific language governing
permissions and limitations under the Licence.
*/
?>


<?php

/**
 * @var $steps array
 * @var $modelStation \common\models\Station
 * @var $modelStationType \common\models\StationWigig
 * @var $hasChanged string
 */

use frontend\assets\map\AddAccessPointAsset;
use frontend\widgets\ActiveForm;
use frontend\widgets\dactylkit\DactylKit;

?>

<?php $form = ActiveForm::begin([
    'id' => 'station-form',
    'pjax' => true,
    'pjaxOptions' => [
        'enablePushState' => true,
        'enableReplaceState' => true,
    ],
    'options' => [
        'data-url' => url(['station/edit', 'id' => $modelStation->id]),
        'data-changed' => $hasChanged,
    ],
    'action' => url(['station/edit', 'id' => $modelStation->id]),
]);
AddAccessPointAsset::register($this);
?>
    <script>
        var urlToPush = $("form#station-form").data('url');
        var hasChanged = $("form#station-form").data('changed') + "";
        if (history.pushState && urlToPush) {
            history.pushState(null, null, urlToPush); // URL is now /inbox/N
        }

        needsToReload = needsToReload || (hasChanged === 'true');
    </script>
    <div class="livebox--station-registration">
        <?= $this->render('../../partial/header', ['model' => $modelStation, 'saveExit' => false]) ?>
        <div class="livebox--station-registration--content" id="vueapp-ap">
            <?= DactylKit::progressBar(2, $steps) ?>
            <h6><?= _tF('Where is the station located', 'station') ?></h6>
            <span class="livebox--station-registration--content__subheading"><?= _tF('Use map to select location or enter GPS coordinates',
                    'station') ?></span>
            <div class="row no-gutters livebox--station-registration--content__location-inputs">
                <div class="col-md-6">
                    <?= $form->field($modelStation, 'lat')->textInput([
                        'id' => 'location-lat',
                        'maxlength' => true,
                        'v-model' => 'locationLat',
                        '@keyup' => 'changePosition',
                        'class' => 'dangerous',
                        ]) ?>
                </div>
                <div class="col-md-6">
                    <?= $form->field($modelStation, 'lng')->textInput([
                        'id' => 'location-lng',
                        'maxlength' => true,
                        'v-model' => 'locationLng',
                        '@keyup' => 'changePosition',
                        'class' => 'dangerous',
                        ]) ?>
                </div>
            </div>
        </div>

        <script>
            if (window['initAccessPoint']) {
                initAccessPoint();
            }
        </script>

        <div class="map-outer-box" style="width: 100%; height: 100%">
          <div id="map-ap" style="width: 100%; height: 100%; min-height: 250px;"
               data-stationlink="<?= url(['station/station', 'id' => 0]) ?>"
               data-radius="<?= c('TOLL_GATE_RADIUS') ?>">
            <div class="overlayAp mapOverlay">
              <div class="lds-grid">
                <div></div>
                <div></div>
                <div></div>
                <div></div>
                <div></div>
                <div></div>
                <div></div>
                <div></div>
                <div></div>
              </div>
            </div>
          </div>
        </div>

        <?= $this->render('../../partial/footer', [
            'prevUrl' => url(['station/step-back', 'id' => $modelStation->id]),
            'nextBtnLabel' => _tF('continue', 'station'),
            'continueBtn' => true,
            'dirtyNextBtnLabel' => _tF('save and continue', 'station'),
        ]) ?>
    </div>
<?php if ($modelStation->isPublished()): ?>
    <script>initDirtyForms(true);</script>
<?php else: ?>
    <script>initDirtyForms(false);</script>
<?php endif; ?>
<?php ActiveForm::end() ?>