<?php
/*
* Copyright 2023 DactylGroup s.r.o.
*
* Licensed under the EUPL, Version 1.2 or – as soon they
will be approved by the European Commission - subsequent
versions of the EUPL (the "Licence");
* You may not use this work except in compliance with the
Licence.
* You may obtain a copy of the Licence at:
*
* https://joinup.ec.europa.eu/software/page/eupl5
*
* Unless required by applicable law or agreed to in
writing, software distributed under the Licence is
distributed on an "AS IS" basis,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
express or implied.
* See the Licence for the specific language governing
permissions and limitations under the Licence.
*/
?>


<?php

/**
 * @var $steps array
 * @var $modelStation \common\models\Station
 * @var $modelStationType \common\models\Station58
 * @var $hasChanged string
 */

use common\models\Station;
use frontend\widgets\ActiveForm;
use frontend\widgets\dactylkit\DactylKit; ?>

<?php $form = ActiveForm::begin([
    'id' => 'station-form',
    'pjax' => true,
    'pjaxOptions' => [
        'enablePushState' => true,
        'enableReplaceState' => true,
    ],
    'options' => [
        'data-url' => $modelStation->id ? url(['station/edit', 'id' => $modelStation->id]) : '',
        'data-changed' => $hasChanged,
        'data-id' => $modelStation->id,
    ],
    'action' => url($modelStation->id ? ['station/edit', 'id' => $modelStation->id] : ['station/create-access-point']),
]) ?>
  <script>
      var urlToPush = $("form#station-form").data('url');
      var hasChanged = $("form#station-form").data('changed') + "";
      if (history.pushState && urlToPush) {
          history.pushState(null, null, urlToPush); // URL is now /inbox/N
      }

      needsToReload = needsToReload || (hasChanged === 'true');
  </script>
  <div id="station-form-pjax">
    <div class="livebox--station-registration">
        <?= $this->render('../../partial/header', ['model' => $modelStation, 'saveExit' => false]) ?>
      <div class="livebox--station-registration--content">
          <?= DactylKit::progressBar(1, $steps) ?>
        <h6><?= _tF('What is ssid of access point', 'station') ?></h6>
          <?= $form->field($modelStation, 'name')
                   ->textInput(['maxlength' => true])
                   ->label(_tF('ssid', 'station'))
                   ->hint(_tF('ssid hint', 'station')) ?>
      </div>
        <?= $this->render('../../partial/footer', [
            'prevUrl' => '',
            'nextBtnLabel' => _tF('continue', 'station'),
            'continueBtn' => true,
            'dirtyNextBtnLabel' => '',
        ]) ?>
    </div>
  </div>
<?php ActiveForm::end() ?>