<?php
/*
* Copyright 2023 DactylGroup s.r.o.
*
* Licensed under the EUPL, Version 1.2 or – as soon they
will be approved by the European Commission - subsequent
versions of the EUPL (the "Licence");
* You may not use this work except in compliance with the
Licence.
* You may obtain a copy of the Licence at:
*
* https://joinup.ec.europa.eu/software/page/eupl5
*
* Unless required by applicable law or agreed to in
writing, software distributed under the Licence is
distributed on an "AS IS" basis,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
express or implied.
* See the Licence for the specific language governing
permissions and limitations under the Licence.
*/
?>


<?php

use dactylcore\core\helpers\Date;
use common\models\Thread;
use frontend\widgets\dactylkit\DactylKit;

/**
 * @var $thread Thread
 */

?>


<div class="livebox--thread--conversation__scroll-wrapper">
    <?php
    for ($i = 0; $i < count($thread->messages); $i++): ?>
        <?php
        $message = $thread->messages[$i];
        $showTimestamp = true;
        if ($i != count($thread->messages) - 1) {
            $nextMessage = $thread->messages[$i + 1];
            $showTimestamp = $message->showTimestamp($nextMessage);
            $showIsRead = $message->showIsRead($nextMessage);
        } else {
            $showIsRead = $message->showIsRead(null);
        }

        ?>
        <div class="message-item-wrapper wrapper-<?= $message->classShowAs(); ?>">
            <div class="message-item message-<?= $message->classShowAs(); ?>">
                <div class="message-content"><?= $message->content; ?></div>
                <?php if ($showTimestamp) : ?>
                    <div class="message-info">
                        <span class="row no-gutters message-date">
                            <?= $message->getDateTimeFormatted(); ?>
                            <?php if ($showIsRead) : ?>
                                <span <?= DactylKit::tooltip(_tF('message read', 'chat')) ?>>
                                <?= DactylKit::icon('@frontend/web/source_assets/img/icon/ic-check-circle-20.svg') ?>
                            </span>
                            <?php endif; ?>
                        </span>
                    </div>
                <?php endif; ?>
            </div>
        </div>
    <?php endfor; ?>
</div>

