<?php
/*
* Copyright 2023 DactylGroup s.r.o.
*
* Licensed under the EUPL, Version 1.2 or – as soon they
will be approved by the European Commission - subsequent
versions of the EUPL (the "Licence");
* You may not use this work except in compliance with the
Licence.
* You may obtain a copy of the Licence at:
*
* https://joinup.ec.europa.eu/software/page/eupl5
*
* Unless required by applicable law or agreed to in
writing, software distributed under the Licence is
distributed on an "AS IS" basis,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
express or implied.
* See the Licence for the specific language governing
permissions and limitations under the Licence.
*/
?>


<?php

use common\models\Thread;
use frontend\widgets\dactylkit\DactylKit;

/**
 * @var $model Thread
 */

if (!$model->messages) {
  return;
}

?>

<a href="<?= url(['messaging/thread', 'id' => $model->id]) ?>"
   class="dk--thread__item <?= $model->isNewForMe() ? 'dk--thread__item__new' :
       '' ?>" data-livebox=1 data-pjax=1 data-livebox-customparams='{"close":true}'>
    <div class="dk--thread__item--info row justify-content-between no-gutters">
        <div class="row no-gutters">
            <?= $model->station->getFormattedId() ?>
            <?= DactylKit::icon('@frontend/web/source_assets/img/icon/ic-dot-divider-24.svg') ?>
            <?= $model->station->name ?>
        </div>
        <div class="dk--thread__item--info__date row no-gutters">
            <?= $model->messages ? $model->getLatestMessageTimeFormatted() : '' ?>
        </div>
    </div>
    <span class="dk--thread__item--message <?= $model->isNewForMe() ? 'dk--thread__item--message__new' :
        '' ?> <?= $model->messages ? '' : 'dk--thread__item--message__empty' ?>"><?= $model->messages ?
            $model->getLatestMessageContent() :
            _tF('no messages in this thread', 'chat') ?></span>
    <span class="dk--thread__item--user">
        <?= $model->getOppositeUserIdentification() ?>
        <?= DactylKit::icon(DactylKit::ICON_ACCOUNT) ?>
    </span>
</a>

