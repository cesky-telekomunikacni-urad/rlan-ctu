<?php
/*
* Copyright 2023 DactylGroup s.r.o.
*
* Licensed under the EUPL, Version 1.2 or – as soon they
will be approved by the European Commission - subsequent
versions of the EUPL (the "Licence");
* You may not use this work except in compliance with the
Licence.
* You may obtain a copy of the Licence at:
*
* https://joinup.ec.europa.eu/software/page/eupl5
*
* Unless required by applicable law or agreed to in
writing, software distributed under the Licence is
distributed on an "AS IS" basis,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
express or implied.
* See the Licence for the specific language governing
permissions and limitations under the Licence.
*/
?>


<?php
/**
 * @var $thread \common\models\Thread
 * @var $messageModel \common\models\Message
 * @var $ajaxUrl string
 * @var $allRead bool
 */

use artkost\yii2\trumbowyg\Trumbowyg;
use frontend\widgets\ActiveForm;
use frontend\assets\ThreadAsset;
use frontend\widgets\dactylkit\DactylKit;

ThreadAsset::register($this);

?>

<div class="livebox--thread">
    <div class="livebox--header livebox--thread--header">
        <div class="row no-gutters">
            <?= $thread->station->getFormattedId() ?>
            <?= DactylKit::icon('@frontend/web/source_assets/img/icon/ic-dot-divider-24.svg') ?>
            <?= $thread->station->name ?>
        </div>
        <span class="livebox--thread--header--user">
            <?= $thread->getOppositeUserIdentification() ?>
            <?= DactylKit::icon(DactylKit::ICON_ACCOUNT) ?>
        </span>
    </div>
    <div class="livebox--thread--conversation" id="conversation-messages-pjax">
        <?= $this->render('partial/thread_messages', [
            'thread' => $thread,
        ]) ?>
    </div>
    <div class="livebox--thread--response-form">
        <?php
        $iAm = $thread->iAm();
        $form = ActiveForm::begin([
            'id' => 'reply-form',
            'pjax' => true,
            'pjaxOptions' => [
                'enablePushState' => false,
                'enableReplaceState' => false,
            ],
            'action' => url(['messaging/thread', 'id' => $thread->id]),
        ]);
        ?>
        <div class="message" data-checkbox-name="show_<?= $iAm ?>_info">
            <?= $form->field($thread, "show_{$iAm}_info")->checkbox(); ?>
            <?= $form->field($messageModel, 'content')->tinyMce([
                'settings' => [
                    'plugins' => [
                        'colors',
                    ],
                    'btns' => [
                        ['strong', 'em', 'del', 'foreColor'],
                        ['unorderedList', 'orderedList'],
                        ['removeformat'],
                    ],
                ],
            ])->label(false); ?>
        </div>
        <span class="thread-conversation-submit" id="submit-message-pjax"
              data-ajaxUrl="<?= $ajaxUrl ?>" <?= DactylKit::tooltip(_tF('send message', 'chat')) ?>>
            <?= DactylKit::icon('@frontend/web/source_assets/img/icon/ic-send-24.svg') ?>
        </span>
        <?php ActiveForm::end(); ?>
    </div>
</div>
<script nonce="<?= Yii::$app->params['identCSP'] ?>">
    var options = [
        {
            selector: '.livebox--thread', // Mandatory, CSS selector
            vh: 100,  // Mandatory, height in vh units
        },
    ];
    var vhFix = new VHChromeFix(options);
    $('#submit-message-pjax').click(function () {
        event.preventDefault();
        var ajaxUrl = $('#submit-message-pjax').data('ajaxurl');
        var content = $('#message-content').val();
        $('#message-content').val('');
        $('.trumbowyg-editor').html("");
        var name = $('.message').data('checkbox-name');
        var checkboxName = 'Thread[' + name + ']';
        var checkboxContent = $('input[name="' + checkboxName + '"]')[1].checked;
        var formData = {
            'Message[content]': content,
        };
        formData[checkboxName] = checkboxContent;


        $.ajax({
            method: "POST",
            url: ajaxUrl,
            data: formData
        }).done(function (response) {
            $('#conversation-messages-pjax').html(response);
            $("#conversation-messages-pjax").scrollTop(100000);
            getAllFlashAlertMessages();
        });
    });

    $( document ).ready(function() {
        // Scroll to the bottom     (heh, bottom :D )
        $("#conversation-messages-pjax").scrollTop(100000);
    });

</script>
<?php if (isset($allRead) && $allRead) : ?>
    <script nonce="<?= Yii::$app->params['identCSP'] ?>">
        $('.dk--nav-bar--conversations').removeClass('dk--nav-bar--conversations__unread');
    </script>
<?php endif; ?>
