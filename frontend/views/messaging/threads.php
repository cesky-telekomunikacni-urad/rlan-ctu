<?php
/*
* Copyright 2023 DactylGroup s.r.o.
*
* Licensed under the EUPL, Version 1.2 or – as soon they
will be approved by the European Commission - subsequent
versions of the EUPL (the "Licence");
* You may not use this work except in compliance with the
Licence.
* You may obtain a copy of the Licence at:
*
* https://joinup.ec.europa.eu/software/page/eupl5
*
* Unless required by applicable law or agreed to in
writing, software distributed under the Licence is
distributed on an "AS IS" basis,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
express or implied.
* See the Licence for the specific language governing
permissions and limitations under the Licence.
*/
?>


<?php

use common\models\search\ThreadSearch;
use common\models\Station;
use common\models\Thread;
use dactylcore\core\data\ActiveDataProvider;
use dactylcore\core\widgets\admin\gridview\GridView;
use dactylcore\core\widgets\common\bootstrap\Html as BootHtml;
use dactylcore\core\widgets\common\listview\ListView;
use frontend\assets\ThreadsAsset;
use frontend\widgets\dactylkit\DactylKit;
use yii\bootstrap\Html;

ThreadsAsset::register($this);

/**
 * @var ActiveDataProvider $dataProvider
 * @var ThreadSearch $searchModel
 * @var string $liveboxUrl
 */

$this->title = _tF('conversations', 'chat');

// Breadcrumbs
$this->breadcrumbs[] = [
    'label' => $this->title,
];

?>

<?= DactylKit::breadcrumb() ?>
<span class="dk--page-title"><?= $this->title ?></span>
<?= ListView::widget([
    'id' => 'conversations-grid',
    'dataProvider' => $dataProvider,
    'itemView' => 'partial/threads_item',
    'layout' => '{items} {summary}',
    'summary' => '<span class="summary">' . _tF('conversations summary', 'chat') . '</span>',
]); ?>

<?php if (isset($liveboxUrl)) {
    $js = <<<JS
        $(document).ready(function() {
            
        });
            let stationBox = LiveBox.create();
            $.ajax("{$liveboxUrl}").done(function(response) {
                stationBox.contentText = response;
                stationBox.show();
            });
        JS;
    $this->registerJS($js);
} ?>


