<?php
/*
* Copyright 2023 DactylGroup s.r.o.
*
* Licensed under the EUPL, Version 1.2 or – as soon they
will be approved by the European Commission - subsequent
versions of the EUPL (the "Licence");
* You may not use this work except in compliance with the
Licence.
* You may obtain a copy of the Licence at:
*
* https://joinup.ec.europa.eu/software/page/eupl5
*
* Unless required by applicable law or agreed to in
writing, software distributed under the Licence is
distributed on an "AS IS" basis,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
express or implied.
* See the Licence for the specific language governing
permissions and limitations under the Licence.
*/
?>


<?php

use frontend\widgets\dactylkit\DactylKit;

?>
<span class="dk--table--ghost__heading">Heading ghost</span>
<table class="dk--table dk--table--ghost">
    <thead>
    <tr>
        <th></th>
        <th>Station A</th>
        <th>Station B</th>
    </tr>
    </thead>
    <tbody>
    <tr>
        <td>GPS</td>
        <td>50.47684609°, 13.35223108°</td>
        <td>50.47684609°, 13.35223108°</td>
    </tr>
    <tr>
        <td>Power</td>
        <td>30 dBi</td>
        <td>30 dBi</td>
    </tr>
    </tbody>
</table>
<span style="margin-top:32px" class="dk--table--ghost__heading">Deadlines</span>
<table style="width: 450px" class="dk--table dk--table--ghost">
    <tbody>
    <tr>
        <td>Valid until <?= DactylKit::icon(DactylKit::ICON_INFO_20) ?></td>
        <td>March 10, 2021</td>
        <td></td>
    </tr>
    <tr>
        <td>Protected until <?= DactylKit::icon(DactylKit::ICON_INFO_20) ?></td>
        <td>September 10, 2021</td>
        <td></td>
    </tr>
    </tbody>
</table>
