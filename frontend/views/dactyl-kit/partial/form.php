<?php
/*
* Copyright 2023 DactylGroup s.r.o.
*
* Licensed under the EUPL, Version 1.2 or – as soon they
will be approved by the European Commission - subsequent
versions of the EUPL (the "Licence");
* You may not use this work except in compliance with the
Licence.
* You may obtain a copy of the Licence at:
*
* https://joinup.ec.europa.eu/software/page/eupl5
*
* Unless required by applicable law or agreed to in
writing, software distributed under the Licence is
distributed on an "AS IS" basis,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
express or implied.
* See the Licence for the specific language governing
permissions and limitations under the Licence.
*/
?>


<?php

$fakeModel = new \yii\base\DynamicModel((['nameEmpty', 'nameErrorEmpty', 'nameFilled', 'nameError', 'radioDisplay', 'switch']));
$fakeModel->nameFilled = 'value';
$fakeModel->nameError = 'value';
$fakeModel->nameErrorEmpty = '';
$fakeModel->addRule(['nameError', 'nameErrorEmpty'], 'string', ['min' => 10]);
$fakeModel->addRule(['nameErrorEmpty'], 'required');
$fakeModel->validate();
?>

<p>Use <strong>\frontend\widgets\ActiveForm</strong> for forms.</p>

<?php $form = \frontend\widgets\ActiveForm::begin() ?>

<strong>Without label</strong>
<?= $form->field($fakeModel, 'nameEmpty')->textInput(['id' => 'input0'])->label(false) ?>

<strong>Empty</strong>
<?= $form->field($fakeModel, 'nameEmpty')->textInput(['id' => 'input1']) ?>

<strong>Empty with hint</strong>
<?= $form->field($fakeModel, 'nameEmpty')->textInput(['id' => 'input2'])->hint('Hint') ?>

<strong>Empty with hint disabled</strong>
<?= $form->field($fakeModel, 'nameEmpty')->textInput(['id' => 'input3', 'disabled' => 'disabled'])->hint('Hint') ?>

<strong>Filled</strong>
<?= $form->field($fakeModel, 'nameFilled')->textInput(['id' => 'input4']) ?>

<strong>Filled with hint</strong>
<?= $form->field($fakeModel, 'nameFilled')->textInput(['id' => 'input5'])->hint('Hint') ?>

<strong>Filled with hint disabled</strong>
<?= $form->field($fakeModel, 'nameFilled')->textInput(['id' => 'input6', 'disabled' => 'disabled'])->hint('Hint') ?>

<strong>Error on empty</strong>
<?= $form->field($fakeModel, 'nameErrorEmpty')->textInput(['id' => 'input7']) ?>

<strong>Error on filled</strong>
<?= $form->field($fakeModel, 'nameError')->textInput(['id' => 'input7']) ?>

<strong>Error with hint</strong>
<?= $form->field($fakeModel, 'nameError')->textInput(['id' => 'input8'])->hint('Hint') ?>

<strong>Error on disabled input</strong>
<?= $form->field($fakeModel, 'nameError')->textInput(['id' => 'input9', 'disabled' => 'disabled'])->hint('Hint') ?>

<strong>Checkbox</strong>
<?= $form->field($fakeModel, 'nameError')->checkbox(['id' => 'input10']) ?>

<strong>Radio</strong>
<?= $form->field($fakeModel, 'radioDisplay')->radioList([
        0 => 'First',
        1 => 'Second',
        2 => 'Third',
    ],
    [
        'item' => function ($index, $label, $name, $checked, $value) {
            $checkMarkup = $checked ? 'checked="checked"' : '';
            $return = '<div class="checkbox"><label>';
            $return .= "<input type='radio' name='{$name}' value='{$value}' $checkMarkup />";
            $return .= "<span class='input-icon'></span>";
            $return .= "<span class='checkbox-label'>{$label}</span>";
            $return .= '</label></div>';

            return $return;
        }
    ])->label(false); ?>

<strong>Switch</strong>
<?= $form->field($fakeModel, 'switch')->switchButton(['id' => 'sw10']) ?>

<?php $form::end(); ?>
