<?php
/*
* Copyright 2023 DactylGroup s.r.o.
*
* Licensed under the EUPL, Version 1.2 or – as soon they
will be approved by the European Commission - subsequent
versions of the EUPL (the "Licence");
* You may not use this work except in compliance with the
Licence.
* You may obtain a copy of the Licence at:
*
* https://joinup.ec.europa.eu/software/page/eupl5
*
* Unless required by applicable law or agreed to in
writing, software distributed under the Licence is
distributed on an "AS IS" basis,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
express or implied.
* See the Licence for the specific language governing
permissions and limitations under the Licence.
*/
?>


<?php

use frontend\widgets\dactylkit\DactylKit;

?>
<?= DactylKit::button('Primary', DactylKit::BUTTON_TYPE_PRIMARY); ?>
<?= DactylKit::button('Primary left', DactylKit::BUTTON_TYPE_PRIMARY, '', DactylKit::ICON_PLUS); ?>
<?= DactylKit::button('Primary right', DactylKit::BUTTON_TYPE_PRIMARY, '', '', DactylKit::ICON_CHEVRON_RIGHT); ?>
<?= DactylKit::button('Primary mini', DactylKit::BUTTON_TYPE_PRIMARY, DactylKit::BUTTON_SIZE_MINI); ?>
<?= DactylKit::button('Secondary', DactylKit::BUTTON_TYPE_SECONDARY); ?>
<?= DactylKit::button('Ghost', DactylKit::BUTTON_TYPE_GHOST); ?>
<?= DactylKit::button('', DactylKit::BUTTON_TYPE_PRIMARY, '', '@frontend/web/source_assets/img/icon/ic-apps-24.svg'); ?>
<?= DactylKit::button('', DactylKit::BUTTON_TYPE_PRIMARY, DactylKit::BUTTON_SIZE_MINI, '@frontend/web/source_assets/img/icon/ic-apps-24.svg'); ?>
<?= DactylKit::button('Floating', DactylKit::BUTTON_TYPE_FLOATING); ?>
<?= DactylKit::button('', DactylKit::BUTTON_TYPE_CIRCLE, '', '@frontend/web/source_assets/img/icon/ic-apps-24.svg'); ?>