<?php
/*
* Copyright 2023 DactylGroup s.r.o.
*
* Licensed under the EUPL, Version 1.2 or – as soon they
will be approved by the European Commission - subsequent
versions of the EUPL (the "Licence");
* You may not use this work except in compliance with the
Licence.
* You may obtain a copy of the Licence at:
*
* https://joinup.ec.europa.eu/software/page/eupl5
*
* Unless required by applicable law or agreed to in
writing, software distributed under the Licence is
distributed on an "AS IS" basis,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
express or implied.
* See the Licence for the specific language governing
permissions and limitations under the Licence.
*/
?>


<?php

use frontend\widgets\dactylkit\DactylKit;

\yii\widgets\Pjax::begin();

$allModels = [];
for ($i = 0; $i < 1000; $i++) {
    $allModels[$i] = [
        'id' => $i,
        'min' => rand(0,2048),
        'max' => rand(2048,4096),
    ];
}

$arrDataProvider = new \yii\data\ArrayDataProvider([
        'allModels' => $allModels,
        'sort' => [
            'attributes' => ['id', 'min', 'max']
        ],
        'pagination' => [
            'pageSize' => 2,
        ]
    ]
);
?>
<?= \dactylcore\core\widgets\common\gridview\GridView::widget([
    'dataProvider' => $arrDataProvider,
    'layout' => '<div>{items}</div><div>{pager}{summary}</div>',
    'summary' => '<div class="summary">Showing {begin} - {end} of {totalCount} items</div>',
    'columns' => [
        ['class' => 'yii\grid\CheckboxColumn',],
        'id',
        'min',
        'max'
    ],
    'pager' => [
        'nextPageLabel' => DactylKit::icon(DactylKit::ICON_CHEVRON_RIGHT),
        'prevPageLabel' => DactylKit::icon(DactylKit::ICON_CHEVRON_LEFT),
    ],
]) ?>
<?php \yii\widgets\Pjax::end() ?>