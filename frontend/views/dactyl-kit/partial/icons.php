<?php
/*
* Copyright 2023 DactylGroup s.r.o.
*
* Licensed under the EUPL, Version 1.2 or – as soon they
will be approved by the European Commission - subsequent
versions of the EUPL (the "Licence");
* You may not use this work except in compliance with the
Licence.
* You may obtain a copy of the Licence at:
*
* https://joinup.ec.europa.eu/software/page/eupl5
*
* Unless required by applicable law or agreed to in
writing, software distributed under the Licence is
distributed on an "AS IS" basis,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
express or implied.
* See the Licence for the specific language governing
permissions and limitations under the Licence.
*/
?>


<?php

use frontend\widgets\dactylkit\DactylKit;

?>
<?= DactylKit::icon(DactylKit::ICON_CHEVRON_RIGHT) ?>
<?= DactylKit::icon(DactylKit::ICON_CHEVRON_LEFT) ?>
<?= DactylKit::icon(DactylKit::ICON_CHEVRON_DOWN) ?>
<?= DactylKit::icon(DactylKit::ICON_SEARCH) ?>
<?= DactylKit::icon(DactylKit::ICON_PLUS) ?>
<?= DactylKit::icon(DactylKit::ICON_LOCK) ?>
<?= DactylKit::icon(DactylKit::ICON_SUCCESS) ?>
<?= DactylKit::icon(DactylKit::ICON_WARNING) ?>
<?= DactylKit::icon(DactylKit::ICON_ERROR) ?>
<?= DactylKit::icon(DactylKit::ICON_HELP) ?>
<?= DactylKit::icon(DactylKit::ICON_A) ?>
<?= DactylKit::icon(DactylKit::ICON_B) ?>
<?= DactylKit::icon(DactylKit::ICON_CLOSE) ?>
<?= DactylKit::icon(DactylKit::ICON_INFO) ?>
<?= DactylKit::icon(DactylKit::ICON_EYE) ?>
<?= DactylKit::icon(DactylKit::ICON_ACCOUNT) ?><?php
