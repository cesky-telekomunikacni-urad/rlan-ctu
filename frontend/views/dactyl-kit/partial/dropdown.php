<?php
/*
* Copyright 2023 DactylGroup s.r.o.
*
* Licensed under the EUPL, Version 1.2 or – as soon they
will be approved by the European Commission - subsequent
versions of the EUPL (the "Licence");
* You may not use this work except in compliance with the
Licence.
* You may obtain a copy of the Licence at:
*
* https://joinup.ec.europa.eu/software/page/eupl5
*
* Unless required by applicable law or agreed to in
writing, software distributed under the Licence is
distributed on an "AS IS" basis,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
express or implied.
* See the Licence for the specific language governing
permissions and limitations under the Licence.
*/
?>


<?php
$fakeModel = new \yii\base\DynamicModel((['typeEmpty', 'typeFilled', 'typeEmpty2', 'typeFilled2', 'typeFilled3', 'typeEmptyError', 'typeFilledError']));
$fakeModel->typeFilled = 0;
$fakeModel->typeFilled2 = 0;
$fakeModel->typeFilledError = 0;
$fakeModel->addRule(['typeFilledError'], 'compare', ['compareValue' => 10]);
$fakeModel->addRule(['typeEmptyError'], 'required');
$fakeModel->validate();
?>
<?php $form = \frontend\widgets\ActiveForm::begin() ?>

    <strong>Empty</strong>
<?= $form->field($fakeModel, 'typeEmpty')->widget(\dactylcore\core\widgets\common\select2\Select2::class, [
    'id' => 'dropdown1',
    'name' => 'dropdown1',
    'data' => [],
    'pluginOptions' => [
        'allowClear' => true,
        'placeholder' => '',
    ],
]) ?>

    <strong>Filled</strong>
<?= $form->field($fakeModel, 'typeFilled')->widget(\dactylcore\core\widgets\common\select2\Select2::class, [
    'id' => 'dropdown2',
    'name' => 'dropdown2',
    'data' => ['value 1', 'value 2', 'value 3'],
    'pluginOptions' => [
        'allowClear' => true,
        'placeholder' => '',
    ],
]) ?>

    <strong>Without label empty</strong>
<?= $form->field($fakeModel, 'typeEmpty2')->widget(\dactylcore\core\widgets\common\select2\Select2::class, [
    'id' => 'dropdown3',
    'name' => 'dropdown3',
    'data' => ['value 1', 'value 2', 'value 3'],
    'pluginOptions' => [
        'allowClear' => true,
        'placeholder' => '',
    ],
])->label(false) ?>

    <strong>Without label filled</strong>
<?= $form->field($fakeModel, 'typeFilled2')->widget(\dactylcore\core\widgets\common\select2\Select2::class, [
    'id' => 'dropdown4',
    'name' => 'dropdown4',
    'data' => ['value 1', 'value 2', 'value 3'],
    'pluginOptions' => [
        'allowClear' => true,
        'placeholder' => '',
    ],
])->label(false) ?>


    <strong>Error empty</strong>
<?= $form->field($fakeModel, 'typeEmptyError')->widget(\dactylcore\core\widgets\common\select2\Select2::class, [
    'id' => 'dropdown5',
    'name' => 'dropdown5',
    'data' => ['value 1', 'value 2', 'value 3'],
    'pluginOptions' => [
        'allowClear' => true,
        'placeholder' => '',
    ],
]) ?>

    <strong>Error filled</strong>
<?= $form->field($fakeModel, 'typeFilledError')->widget(\dactylcore\core\widgets\common\select2\Select2::class, [
    'id' => 'dropdown6',
    'name' => 'dropdown6',
    'data' => ['value 1', 'value 2', 'value 3'],
    'pluginOptions' => [
        'allowClear' => true,
        'placeholder' => '',
    ],
]) ?>

    <strong>With Hint</strong>
<?= $form->field($fakeModel, 'typeFilled3')->widget(\dactylcore\core\widgets\common\select2\Select2::class, [
    'id' => 'dropdown7',
    'name' => 'dropdown7',
    'data' => ['value 1', 'value 2', 'value 3'],
    'pluginOptions' => [
        'allowClear' => true,
        'placeholder' => '',
    ],
])->hint('helper') ?>
<?php $form::end(); ?>