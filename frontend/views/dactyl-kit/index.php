<?php
/*
* Copyright 2023 DactylGroup s.r.o.
*
* Licensed under the EUPL, Version 1.2 or – as soon they
will be approved by the European Commission - subsequent
versions of the EUPL (the "Licence");
* You may not use this work except in compliance with the
Licence.
* You may obtain a copy of the Licence at:
*
* https://joinup.ec.europa.eu/software/page/eupl5
*
* Unless required by applicable law or agreed to in
writing, software distributed under the Licence is
distributed on an "AS IS" basis,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
express or implied.
* See the Licence for the specific language governing
permissions and limitations under the Licence.
*/
?>


<?php

use dactylcore\core\web\FrontendView;
use frontend\widgets\dactylkit\DactylKit;

/* @var $this FrontendView */

\frontend\assets\DactylKitAsset::register($this);

// Breadcrumbs
$this->breadcrumbs[] = [
    'label' => "Dactyl Kit",
];
?>

<style>
    .dactyl-kit-demo .container {
        margin-bottom: 80px;
        border-top: 2px solid #eee;
    }

    #breadcrumb > .container {
        border: none;
    }

    h2 {
        padding-top: 15px;
        padding-bottom: 32px;
    }

    /* offset footer to not interfere with content (only CTU issue) */
    main {
        padding-bottom: 250px;
    }
</style>

<div class="dactyl-kit-demo">
    <?= $this->render('@frontend/widgets/dactylkit/components/alert/demo') ?>

    <div class="container">
        <h2>Buttons</h2>
        <?= $this->render('partial/buttons') ?>
    </div>

    <div class="container">
        <h2>Inputs</h2>
        <?= $this->render('partial/form') ?>
    </div>

    <div class="container">
        <h2>Dropdown</h2>
        <?= $this->render('partial/dropdown') ?>
    </div>

    <div class="container">
        <h2>Breadcrumbs</h2>
        <?= DactylKit::breadcrumb() ?>
    </div>

    <div class="container">
        <h2>Datepicker</h2>
        <?php
        $fakeDateModel = new \yii\base\DynamicModel((['dateFrom']));
        $fakeDateModel->validate();
        ?>
        <?php $form = \frontend\widgets\ActiveForm::begin() ?>
        <div class="dk--daterangepicker--button">
            <?= $form->field($fakeDateModel, 'dateFrom')
                     ->widget(\dactylcore\core\widgets\admin\daterangepicker\DateRangePicker::class, [
                         'options' => [
                             'class' => 'dk--datepicker--button',
                         ],
                         'pluginOptions' => [
                             'startDate' => (new DateTime())->modify('+1 day')->format('d.m.Y'),
                             'format' => 'dd.mm.yyyy',
                             'todayHighlight' => true,
                             'autoclose' => true,
                             'buttonClasses' => 'dk--btn dk--btn--ghost dk--btn--mini',
                         ],
                     ])
                     ->label('Modified'); ?>
        </div>
        <?php \frontend\widgets\ActiveForm::end() ?>
    </div>

    <div class="container">
        <h2>Grid view + pagination</h2>
        <?= $this->render('partial/grid') ?>
    </div>

    <div class="container">
        <h2>Badges</h2>
        <?= DactylKit::badge('Default', DactylKit::BADGE_DEFAULT) ?>
        <?= DactylKit::badge('Informational', DactylKit::BADGE_INFORMATIONAL) ?>
        <?= DactylKit::badge('Success', DactylKit::BADGE_SUCCESS) ?>
        <?= DactylKit::badge('Warning', DactylKit::BADGE_WARNING) ?>
    </div>

    <div class="container">
        <h2>Ready-made icons</h2>
        <?= $this->render('partial/icons') ?>
    </div>

    <style>
        /* styling for tooltip placeholders, only local */
        div.tooltip-holder {
            display: flex;
        }

        span.tooltip-showcase {
            text-align: center;
        }
    </style>
    <div class="container">
        <h2>Tooltips</h2>
        <div class="col-md-12 tooltip-holder">
            <div class="col-md-3 tooltip-showcase"><span <?= DactylKit::tooltip("Label") ?>>Tooltip top</span></div>
            <div class="col-md-3 tooltip-showcase"><span <?= DactylKit::tooltip("Label", DactylKit::TOOLTIP_BOTTOM) ?>>Tooltip bottom</span>
            </div>
            <div class="col-md-3 tooltip-showcase"><span <?= DactylKit::tooltip("Label", DactylKit::TOOLTIP_LEFT) ?>>Tooltip left</span>
            </div>
            <div class="col-md-3 tooltip-showcase"><span <?= DactylKit::tooltip("Label", DactylKit::TOOLTIP_RIGHT) ?>>Tooltip right</span>
            </div>
        </div>
    </div>

    <div class="container">
        <h2>Chip</h2>
        <?= DactylKit::chip('Default') ?>
        <?= DactylKit::chip('Tall', '', DactylKit::CHIP_SIZE_TALL) ?>
        <?= DactylKit::chip('Disabled', DactylKit::CHIP_TYPE_DISABLED) ?>
        <?= DactylKit::chip('Disabled Tall', DactylKit::CHIP_TYPE_DISABLED, DactylKit::CHIP_SIZE_TALL) ?>
        <?= DactylKit::chip('Selected', '', '', '', '', ['class' => 'selected']) ?>
        <?= DactylKit::chip('Selected Tall', '', DactylKit::CHIP_SIZE_TALL, '', '', ['class' => 'selected']) ?>

    </div>

    <div class="container">
        <h2>Table Ghost</h2>
        <?= $this->render('partial/table_ghost') ?>
    </div>

    <?php
    $steps = [
        1 => 'Step 1',
        2 => 'Step 2',
        3 => 'Step 3',
        4 => 'Step 4',
    ];
    ?>
    <div class="container">
        <h2>Progress bar</h2>
        <?= DactylKit::progressBar(2, $steps) ?>
    </div>
</div>