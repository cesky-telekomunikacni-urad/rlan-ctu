<?php
/*
* Copyright 2023 DactylGroup s.r.o.
*
* Licensed under the EUPL, Version 1.2 or – as soon they
will be approved by the European Commission - subsequent
versions of the EUPL (the "Licence");
* You may not use this work except in compliance with the
Licence.
* You may obtain a copy of the Licence at:
*
* https://joinup.ec.europa.eu/software/page/eupl5
*
* Unless required by applicable law or agreed to in
writing, software distributed under the Licence is
distributed on an "AS IS" basis,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
express or implied.
* See the Licence for the specific language governing
permissions and limitations under the Licence.
*/
?>


<?php

use frontend\widgets\dactylkit\DactylKit;

?>
<div class="stations-switcher">
    <div class="stations-switcher__container">
        <span class="stations-switcher__container__distance"><?= _tF('show', 'station') ?></span>
        <span class="stations-switcher__container__icon">
                        <?= DactylKit::chip(_tF('my', 'station'), '', '', '', '', ['v-on:click' => 'showMy', "v-bind:class" => "{'selected': myOnly}"]) ?>
                        <?= DactylKit::chip(_tF('all', 'station'), '', '', '', '', ['v-on:click' => 'showAll', "v-bind:class" => "{'selected': !myOnly}"]) ?>
                    </span>
    </div>
</div>