<?php
/*
* Copyright 2023 DactylGroup s.r.o.
*
* Licensed under the EUPL, Version 1.2 or – as soon they
will be approved by the European Commission - subsequent
versions of the EUPL (the "Licence");
* You may not use this work except in compliance with the
Licence.
* You may obtain a copy of the Licence at:
*
* https://joinup.ec.europa.eu/software/page/eupl5
*
* Unless required by applicable law or agreed to in
writing, software distributed under the Licence is
distributed on an "AS IS" basis,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
express or implied.
* See the Licence for the specific language governing
permissions and limitations under the Licence.
*/
?>


<?php

use frontend\widgets\ActiveForm;
use frontend\widgets\dactylkit\DactylKit;
use yii\base\DynamicModel;

?>

<div class="dk--nav-bar__container table-subheader">
    <div class="dropdown table-subheader__dropdown" v-if="types">
        <button class="btn btn-default dropdown-toggle table-subheader__dropdown__button dk--chip"
                :class="{ selected: checkedTypesLabels }" type="button"
                id="dropdownMenu1" data-toggle="dropdown"
                aria-haspopup="true" aria-expanded="true">
            <span v-if="!checkedTypesLabels" class="dk--chip__label"><?= _tF('type_headline', 'station') ?></span>
            {{checkedTypesLabels}}
            <span class="caret dk--chip__label"></span>
        </button>
        <ul class="dropdown-menu checkbox-menu allow-focus table-subheader__dropdown__menu"
            aria-labelledby="dropdownMenu1">

            <li class="checkbox" v-for="type in types">
                <label :for="type.id" class="sub-header-label">
                    <input type="checkbox" :value='type' :id="type.id" v-model="checkedTypes">
                    <span class="input-icon">
                    <?= file_get_contents(Yii::getAlias('@frontend/web/source_assets/img/icon/ic-ok-10.svg')) ?>
                </span>
                    <span :for="type.id" class="checkbox-label">{{ type.label }}</span>
                </label>
            </li>
            <li class="table-subheader__dropdown__menu__buttons">
                <button @click="clearTypes()" class="dk--btn dk--btn--ghost dk--btn--mini">
                    <span class="dk--btn__label">
                        <?= _tF('clear', 'stations-subheader') ?>
                    </span>
                </button>
                <button @click="closeDropdown('dropdownMenu1')" class="dk--btn dk--btn--primary dk--btn--mini">
                    <span class="dk--btn__label">
                        <?= _tF('save', 'stations-subheader') ?>
                    </span>
                </button>
            </li>

        </ul>
    </div>

    <?php if (userIsLogged()): ?>

        <div class="dropdown table-subheader__dropdown" v-if="statuses">
            <button class="btn btn-default dropdown-toggle table-subheader__dropdown__button dk--chip"
                    :class="{ selected: checkedStatusesLabels }" type="button"
                    id="dropdownMenu2" data-toggle="dropdown"
                    aria-haspopup="true" aria-expanded="true">
                <span v-if="!checkedStatusesLabels" class="dk--chip__label">
                    <?= _tF('status_headline', 'station') ?>
                </span>
                {{checkedStatusesLabels}}
                <span class="caret dk--chip__label"></span>
            </button>
            <ul class="dropdown-menu checkbox-menu allow-focus table-subheader__dropdown__menu"
                aria-labelledby="dropdownMenu1">

                <li class="checkbox" v-for="status in statuses">
                    <label :for="status.id" :class="status.id" class="sub-header-label">
                        <input type="checkbox" :value='status' :id="status.id" v-model="checkedStatuses">
                        <span class="input-icon">
                        <?= file_get_contents(Yii::getAlias('@frontend/web/source_assets/img/icon/ic-ok-10.svg')) ?>
                    </span>
                        <span :for="status.id" class="checkbox-label statusCheckbox" :class="status.id">{{ status.label }}</span>
                    </label>
                </li>
                <li class="table-subheader__dropdown__menu__buttons">
                    <button @click="clearStatuses()" class="dk--btn dk--btn--ghost dk--btn--mini">
                    <span class="dk--btn__label">
                        <?= _tF('clear', 'stations-subheader') ?>
                    </span>
                    </button>
                    <button @click="closeDropdown('dropdownMenu2')" class="dk--btn dk--btn--primary dk--btn--mini">
                    <span class="dk--btn__label">
                        <?= _tF('save', 'stations-subheader') ?>
                    </span>
                    </button>
                </li>
            </ul>
        </div>
        <?php if ($showMyOnly): ?>
            <?php
            $fakeModel = new DynamicModel((['myOnly']));
            $form = ActiveForm::begin();
            ?>
            <div style="border: 2px solid #cbd5e0; padding: 4px; border-radius: 4px">
                <?= $form->field($fakeModel, 'myOnly')->checkbox(['v-model' => 'myOnly'])->label(_tF('mine only',
                    'station')) ?>
            </div>
            <?php ActiveForm::end() ?>
        <?php endif; ?>

        <?php if (!$showMyOnly): ?>
            <?php
            $fakeModel = new DynamicModel((['sortByExpiration']));
            $form = ActiveForm::begin();
            ?>
            <div style="border: 2px solid #cbd5e0; padding: 4px; border-radius: 4px">
                <?= $form->field($fakeModel, 'sortByExpiration')->checkbox(['v-model' => 'sortByExpiration'])->label(_tF('sort_by_expiration',
                    'station')) ?>
            </div>
            <?php ActiveForm::end() ?>
        <?php endif; ?>

    <?php endif; ?>

    <input class="search-input" type="text" v-model="searchTerm"
           placeholder="<?= _tF('search', 'stations-subheader') ?>">
</div>
<div class="dk--nav-bar__container">
    <?php
    $fakeModel = new DynamicModel((['showList']));
    $label_text = _tF('show list', 'station');
    $label_tooltip = DactylKit::tooltip(_tF('switch map and list view', 'station'), DactylKit::TOOLTIP_LEFT);
    $label = "<span {$label_tooltip}>{$label_text}</span>";
    $form = ActiveForm::begin() ?>
    <?= $form->field($fakeModel, 'showList')
             ->checkbox(['v-model' => 'showList', 'checked' => 'checked'])
             ->label($label) ?>
    <?php ActiveForm::end() ?>
</div>

<?php if (isset($byAdmin) && $byAdmin): ?>
    <div class="dk--nav-bar__container warning-stripe other-user">
        <span><?= _tF('other_user_warning', 'user-alliance') ?></span>
    </div>
<?php endif; ?>
