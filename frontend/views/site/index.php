<?php
/*
* Copyright 2023 DactylGroup s.r.o.
*
* Licensed under the EUPL, Version 1.2 or – as soon they
will be approved by the European Commission - subsequent
versions of the EUPL (the "Licence");
* You may not use this work except in compliance with the
Licence.
* You may obtain a copy of the Licence at:
*
* https://joinup.ec.europa.eu/software/page/eupl5
*
* Unless required by applicable law or agreed to in
writing, software distributed under the Licence is
distributed on an "AS IS" basis,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
express or implied.
* See the Licence for the specific language governing
permissions and limitations under the Licence.
*/
?>


<?php

use frontend\assets\LayoutAsset;
use frontend\assets\map\AllStationsAsset;
use frontend\widgets\dactylkit\DactylKit;

LayoutAsset::register($this);
AllStationsAsset::register($this);

/**
 * @var $stationId int
 * @var $stationStep int
 */

$this->title = _tF('stations', 'station');

// Breadcrumbs
$this->breadcrumbs[] = [
    'home' => '/',
    'label' => $this->title,
];


$ofLabel = _t('of', 'stations-table');
$emptyStations = _tF('empty_table', 'stations-table');

?>


<div class="dk--column-holder row" style="margin: 0">
    <section class="dk--container col-lg-6 left-container" style="position: inherit">
        <div class="dk--container__content stations-list" id="vueapp">

            <?= DactylKit::breadcrumb() ?>

            <h2 class="stations-title"><?= $this->title ?></h2>
            <div data-link="<?= url(['station/detail', 'id' => 0]); ?>"></div>
            <vue-good-table
                    mode="remote"
                    @on-page-change="onPageChange"
                    @on-sort-change="onSortChange"
                    @on-column-filter="onColumnFilter"
                    @on-per-page-change="onPerPageChange"
                    @on-row-mouseenter="onRowMouseEnter"
                    @on-row-mouseleave="onRowMouseLeave"
                    :total-rows="totalRecords"
                    :is-loading.sync="isLoading"
                    :pagination-options="{
                            enabled: true,
                            perPage: 100,
                            dropdownAllowAll: false,
                            ofLabel: '<?= $ofLabel ?>',
                            }"
                    :rows="rows"
                    :columns="columns"
                    @on-search="onSearch"
                    :search-options="{enabled: true, externalQuery: searchTerm}"
                    styleClass="vgt-table"
            >

                <div slot="emptystate"><?= $emptyStations ?></div>

                <template slot="table-column" slot-scope="props">
                    <span v-if="props.column.label == 'name'">
                        <?= _tF('table_name', 'stations-table') ?>
                        <img class="sort-triangle-icon"
                             src="/frontend/web/source_assets/img/icon/ic-caret-down-20.svg"/>
                    </span>
                    <span v-else-if="props.column.label == 'id'">
                        <?= _tF('table_id', 'stations-table') ?>
                        <img class="sort-triangle-icon"
                             src="/frontend/web/source_assets/img/icon/ic-caret-down-20.svg"/>
                    </span>
                    <span v-else-if="props.column.label == 'type'">
                        <?= _tF('table_type', 'stations-table') ?>
                        <img class="sort-triangle-icon"
                             src="/frontend/web/source_assets/img/icon/ic-caret-down-20.svg"/>
                    </span>
                    <span v-else-if="props.column.label == 'status'">
                        <?= _tF('table_status', 'stations-table') ?>
                        <img class="sort-triangle-icon"
                             src="/frontend/web/source_assets/img/icon/ic-caret-down-20.svg"/>
                    </span>
                    <span v-else>
                        {{props.column.label}}
                        <img class="sort-triangle-icon" src="frontend/web/source_assets/img/icon/ic-caret-down-20.svg"/>
                    </span>
                </template>

                <template slot="table-row" slot-scope="props">
                    <span v-if="props.column.field == 'name'" class="col-with-link top-span name-link">
                        <a v-if="props.row.status !== 'finished'" :href="getLink(props.row.id_m)" data-livebox="1"
                           target="_blank"><span v-html="props.row.name"></span>
                          <img v-if="props.row.type == 'fs' && props.row.pair_position == 'a'"
                               :src="'/frontend/web/source_assets/img/icon/ic-position-a.svg'"
                                          <?= DactylKit::tooltip(_tF('tooltip_a_station', 'station')) ?>
                          />
                          <img v-if="props.row.type == 'fs' && props.row.pair_position == 'b'"
                               :src="'/frontend/web/source_assets/img/icon/ic-position-b.svg'"
                                          <?= DactylKit::tooltip(_tF('tooltip_b_station', 'station')) ?>
                          />
                        </a>
                        <a v-else :href="getLink(props.row.id_m)" target="_blank"><span v-html="props.row.name"></span>
                              <img v-if="props.row.type == 'fs' && props.row.pair_position == 'a'"
                                   :src="'/frontend/web/source_assets/img/icon/ic-position-a.svg'"
                                          <?= DactylKit::tooltip(_tF('tooltip_a_station', 'station')) ?>
                              />
                              <img v-if="props.row.type == 'fs' && props.row.pair_position == 'b'"
                                   :src="'/frontend/web/source_assets/img/icon/ic-position-b.svg'"
                                          <?= DactylKit::tooltip(_tF('tooltip_b_station', 'station')) ?>
                              />
                          </a>
                        <span class="icon-background">
                            <span class="icon" <?= DactylKit::tooltip(_tF('show station on map', 'station'))?> @click="flyTo(props.row.lng, props.row.lat)"></span>
                        </span>
                    </span>
                    <span v-else-if="props.column.field == 'status'" class="status-column top-span col-with-link">
                        <a :href="getLink(props.row.id)" target="_blank">
                            <span class="statusColumn" :class="props.row.status">{{props.row.statusT}}</span>

                            <span class="dropdown status-column__dropdown">
                                <span class="btn btn-default dropdown-toggle status-icon"
                                      :class="'state-'+props.row.state"
                                      data-toggle="dropdown" aria-haspopup="true" aria-expanded="true"
                                      v-html="props.row.stateIcon">
                                </span>
                                <span class="dropdown-menu status-column__dropdown__menu">
                                    <span class="status-column__dropdown__menu__item">
                                        <?= _tF('valid_until', 'stations-table') ?>:
                                        <span class="status-column__dropdown__menu__item__bold" :class="'date-state-'+props.row.valid_to_class">
                                            {{props.row.valid_to}}
                                        </span>
                                    </span>
                                    <br>
                                    <span class="status-column__dropdown__menu__item">
                                        <?= _tF('protected_until', 'stations-table') ?>:
                                        <span class="status-column__dropdown__menu__item__bold" :class="'date-state-'+props.row.protected_to_class">
                                            {{props.row.protected_to}}
                                        </span>
                                    </span>
                                </span>
                            </span>
                      </a>

                      <span v-if="props.row.can_edit" class="dropdown status-column__dropdown second_icon">
                                <span class="btn btn-default dropdown-toggle status-icon"
                                      :class="'state-'+props.row.state"
                                      data-toggle="dropdown" aria-haspopup="true" aria-expanded="true"
                                     >
                                  <img src="/img/icon/ic-more-20.svg" style="padding: 5px">
                                </span>
                                <span class="dropdown-menu status-column__dropdown__menu">
                                  <ul class="grid-menu">
                                      <li v-if="props.row.prolong"><a :href="getProlongLink(props.row.id, true)" data-livebox="1"><?= _tF('prolong', 'station'); ?></a></li>
                                      <li><a :href="getLink(props.row.id, true)" data-livebox="1"><?= _tF('edit', 'station'); ?></a></li>
                                      <li v-if="props.row.can_edit && props.row.status == '<?= \common\models\Station::STATUS_FINISHED ?>'">
                                            <a @click="unpublishStation(props.row.id)">
                                              <?= _tF('unpublish', 'station'); ?>
                                            </a>
                                      </li>
                                      <li v-if="props.row.can_edit"><a @click="deleteStation(props.row.id)"><?= _tF('delete', 'station'); ?></a></li>
                                    </ul>
                                  </span>
                       </span>
                    </span>
                    <span v-else-if="props.column.field == 'type'" class="col-with-link top-span type-link">
                        <a :href="getLink(props.row.id_m)" target="_blank">{{props.row.typeT}}</a>
                    </span>
                    <span v-else-if="props.column.field == 'id'" class="col-with-link top-span">
                        <a :href="getLink(props.row.id_m)" target="_blank">
                            {{props.formattedRow[props.column.field]}}
                        </a>
                    </span>
                    <span v-else class="top-span">
                      {{props.formattedRow[props.column.field]}}
                    </span>
                </template>

                <template slot="loadingContent">
                    <div class="stations-table-loading"><?= _Tf('table_loading', 'stations-table') ?></div>
                </template>
            </vue-good-table>
            <?php if(userIsLogged()): ?>
              <?= $this->render('./partial/switcher') ?>
            <?php endif; ?>
        </div>
    </section>
    <section class="dk--container dk--container__gray col-lg-6 right-container" style="padding: 0;">
        <div class="dk--container__content"
             style="width: 100%;     height: calc(100vh - 110px); padding: 0; max-width: 100%; position: relative">
            <div class="mapOverlay overlayIndex">
                <div class="lds-grid">
                    <div></div>
                    <div></div>
                    <div></div>
                    <div></div>
                    <div></div>
                    <div></div>
                    <div></div>
                    <div></div>
                    <div></div>
                </div>
            </div>
            <div id="map" class="half-width map-container"
                 data-stationlink="<?= url(['/station/station', 'id' => 0]) ?>"
                 data-stationlinkprolong="<?=  url(['/station/prolong-registration', 'id' => 0]) ?>"
                 data-delete-confirmation-text="<?=  _tF('Do you really want to delete station', 'station') ?>"
                 data-confirmation-unpublish-text="<?=  _tF('Do you really want to unpublish station?', 'station') ?>"
                 data-yes="<?=  _tF('yes delete', 'station') ?>"
                 data-no="<?=  _tF('no delete', 'station') ?>">
            </div>
        </div>
    </section>
</div>

<?php
$this->registerJsVar('langIso', getLangShortCode());
?>

<?php if (isset($liveboxUrl)) {
    $js = <<<JS
            let stationBox = LiveBox.create();
            $.ajax("{$liveboxUrl}").done(function(response) {
                stationBox.contentText = response;
                stationBox.show();
            });
        JS;
    $this->registerJS($js);
} ?>
