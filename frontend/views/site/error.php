<?php
/*
* Copyright 2023 DactylGroup s.r.o.
*
* Licensed under the EUPL, Version 1.2 or – as soon they
will be approved by the European Commission - subsequent
versions of the EUPL (the "Licence");
* You may not use this work except in compliance with the
Licence.
* You may obtain a copy of the Licence at:
*
* https://joinup.ec.europa.eu/software/page/eupl5
*
* Unless required by applicable law or agreed to in
writing, software distributed under the Licence is
distributed on an "AS IS" basis,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
express or implied.
* See the Licence for the specific language governing
permissions and limitations under the Licence.
*/
?>


<?php
// Sources
/* @var $this \dactylcore\core\web\FrontendView */
/* @var $message string */

// Asset Bundles
\frontend\assets\ErrorAsset::register($this);
?>

<div class="dk--column-holder row">
    <section class="dk--container col-md-6">
        <div class="dk--container__content">
            <h3><?= _tF('title', 'error') ?></h3>
            <h4><?= $message ?></h4>
        </div>
    </section>
    <section class="dk--container dk--container__gray col-md-6">
        <div class="dk--container__content" style="width: 100%; height: 100%; padding: 0; max-width: 100% ">
        </div>
    </section>
</div>