<?php
/*
* Copyright 2023 DactylGroup s.r.o.
*
* Licensed under the EUPL, Version 1.2 or – as soon they
will be approved by the European Commission - subsequent
versions of the EUPL (the "Licence");
* You may not use this work except in compliance with the
Licence.
* You may obtain a copy of the Licence at:
*
* https://joinup.ec.europa.eu/software/page/eupl5
*
* Unless required by applicable law or agreed to in
writing, software distributed under the Licence is
distributed on an "AS IS" basis,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
express or implied.
* See the Licence for the specific language governing
permissions and limitations under the Licence.
*/
?>


<?php

use frontend\widgets\dactylkit\DactylKit;

/**
 * @var $videoSlug string
 * @var $manualFile dactylcore\system\common\models\File
 */


$stationTypes = [
    [
        'label' => _tF('add_pair', 'help-box'),
        'icon-class' => 'dk--station__fs',
        'icon' => true,
    ],
    [
        'label' => _tF('add_fs', 'help-box'),
        'icon-class' => 'dk--station__fs',
        'sub-label' => _tF('add_fs_sublabel', 'help-box'),
    ],
    [
        'label' => _tF('add_wigig', 'help-box'),
        'icon-class' => 'dk--station__wigig',
        'sub-label' => _tF('add_wigig_sublabel', 'help-box'),
    ],
//    [
//        'label' => _tF('import fs', 'station'),
//        'icon-class' => 'dk--station__fs',
//        'sub-label' => _tF('add_fs_sublabel', 'station'),
//    ],
//    [
//        'label' => _tF('import wigig', 'station'),
//        'icon-class' => 'dk--station__wigig',
//        'sub-label' => _tF('add_fs_sublabel', 'station'),
//    ],
]

?>

<div class="livebox--help">
  <div class="livebox--header">
    <span><?= _tF('help', 'help-box') ?></span>
  </div>
  <div class="livebox--help--content">
      <?php if ($videoSlug): ?>
        <div class="video-player">
          <iframe width="100%" height="304"
                  src="https://www.youtube.com/embed/<?= $videoSlug ?>"
                  frameborder="0" allow="accelerometer; autoplay; encrypted-media;
                    gyroscope; picture-in-picture" allowfullscreen></iframe>
        </div>
      <?php endif; ?>
      <?php if ($manualFile) : ?>
        <h5><?= _tF('instructions', 'help-box') ?></h5>
        <a href="<?= $manualFile->getCdnAbsoluteUrl() ?>" target="_blank" class="user-guide d-flex">
            <?= DactylKit::icon('@frontend/web/source_assets/img/icon/ic-file-type-pdf-48.svg') ?>
          <div class="d-flex flex-column">
            <span class="filename"><?= _tF('user guide', 'help-box') ?></span>
            <span class="filesize"><?= $manualFile->getFileSizeFormatted() ?></span>
          </div>
        </a>
      <?php endif; ?>
    <h5><?= _tF('explanations', 'help-box') ?></h5>
    <div class="dk--station--type">
        <?php foreach ($stationTypes as $station): ?>
          <div class="dk--station--type__list-item <?= (!isset($station['sub-label']) ? "centered" : "") ?>">
            <div class="dk--station--type__list-item__icon <?= $station['icon-class'] ?>">
                <?php if (isset($station['icon'])): ?>
                    <?= DactylKit::icon('@frontend/web/source_assets/img/icon/ic-a-inner.svg') ?>
                    <?= DactylKit::icon('@frontend/web/source_assets/img/icon/ic-b-inner.svg') ?>
                <?php endif; ?>
            </div>
            <span class="dk--station--type__list-item__label">
              <?= $station['label'] ?>
                <?php if (isset($station['sub-label'])): ?>
                  <span class="dk--station--type__list-item__sub-label"><?= $station['sub-label'] ?></span>
                <?php endif; ?>
            </span>
          </div>
        <?php endforeach; ?>
    </div>
  </div>
</div>