<?php
/*
* Copyright 2023 DactylGroup s.r.o.
*
* Licensed under the EUPL, Version 1.2 or – as soon they
will be approved by the European Commission - subsequent
versions of the EUPL (the "Licence");
* You may not use this work except in compliance with the
Licence.
* You may obtain a copy of the Licence at:
*
* https://joinup.ec.europa.eu/software/page/eupl5
*
* Unless required by applicable law or agreed to in
writing, software distributed under the Licence is
distributed on an "AS IS" basis,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
express or implied.
* See the Licence for the specific language governing
permissions and limitations under the Licence.
*/
?>


<?php

use common\models\Notification;

$models = Notification::findAllSuitable($_COOKIE['seen_notifications']??'[]');
if(!$models || !userIsLogged())
    return;
?>

<div id="notifications">
    <?php foreach ($models as $model): ?>
        <div class="notification notification-<?= $model->id ?>" data-id="<?= $model->id ?>">
            <span class="close">✕</span>
            <?= $model->text ?>
        </div>
    <?php endforeach; ?>
</div>