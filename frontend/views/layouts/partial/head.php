<?php
/*
* Copyright 2023 DactylGroup s.r.o.
*
* Licensed under the EUPL, Version 1.2 or – as soon they
will be approved by the European Commission - subsequent
versions of the EUPL (the "Licence");
* You may not use this work except in compliance with the
Licence.
* You may obtain a copy of the Licence at:
*
* https://joinup.ec.europa.eu/software/page/eupl5
*
* Unless required by applicable law or agreed to in
writing, software distributed under the Licence is
distributed on an "AS IS" basis,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
express or implied.
* See the Licence for the specific language governing
permissions and limitations under the Licence.
*/
?>


<?php

/**
 * @var $this \dactylcore\core\web\FrontendView
 */

$class = 'page-' . Yii::$app->controller->id . '-' . Yii::$app->controller->action->id;
$id = Yii::$app->controller->module->id . '-' . Yii::$app->controller->id . '-' . Yii::$app->controller->action->id;
$icons = '/frontend/web/source_assets/layout/img/favicons';
$this->title = strpos($this->title, '|') === false ? $this->title . ' | ' . Yii::$app->name : $this->title;

use dactylcore\core\helpers\Html; ?>

<?php $this->beginPage(); ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language; ?>" id="<?= $id; ?>" class="<?= $class; ?>">
<head>

    <?php if ($gatc = env('GOOGLE_ANALYTICS_TRACKING_CODE')): ?>
        <!-- Google tag (gtag.js) -->
        <script async src="https://www.googletagmanager.com/gtag/js?id=<?= $gatc ?>"></script>
        <script>
            window.dataLayer = window.dataLayer || [];

            function gtag() {
                dataLayer.push(arguments);
            }

            gtag('js', new Date());

            gtag('config', '<?=$gatc?>');
        </script>
    <?php endif; ?>


    <?= Html::basicMetaTags(); ?>
    <meta name="robots" content="index, follow">
    <?= Html::csrfMetaTags(); ?>
    <title><?= Html::encode($this->title); ?></title>
    <?php if (!isAjax() && !isPjax()) : ?>

        <link rel="apple-touch-icon" sizes="57x57" href="<?= $icons ?>/apple-touch-icon-57x57.png">
        <link rel="apple-touch-icon" sizes="60x60" href="<?= $icons ?>/apple-touch-icon-60x60.png">
        <link rel="icon" type="image/png" sizes="32x32" href="<?= $icons ?>/favicon-32x32.png">
        <link rel="icon" type="image/png" sizes="16x16" href="<?= $icons ?>/favicon-16x16.png">
        <link rel="icon" type="image/x-icon" href="<?= $icons ?>/favicon.ico">
        <!--    <link rel="manifest" href="--><? //= $icons ?><!--/manifest.json">-->
        <meta name="msapplication-TileColor" content="#da532c">
        <meta name="dactyl-kit-config" content="<?= Yii::$app->params['identCSP'] ?>">
        <meta name="theme-color" content="#ffffff">
    <?php endif; ?>

    <?php $this->head(); ?>

    <link href="https://fonts.googleapis.com/css?family=Oxygen:300,400,700&amp;subset=latin-ext" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Oswald:300&amp;subset=latin-ext" rel="stylesheet">
</head>
<?= $content ?>

</html>
<?php $this->endPage(); ?>
