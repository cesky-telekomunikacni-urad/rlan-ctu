<?php
/*
* Copyright 2023 DactylGroup s.r.o.
*
* Licensed under the EUPL, Version 1.2 or – as soon they
will be approved by the European Commission - subsequent
versions of the EUPL (the "Licence");
* You may not use this work except in compliance with the
Licence.
* You may obtain a copy of the Licence at:
*
* https://joinup.ec.europa.eu/software/page/eupl5
*
* Unless required by applicable law or agreed to in
writing, software distributed under the Licence is
distributed on an "AS IS" basis,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
express or implied.
* See the Licence for the specific language governing
permissions and limitations under the Licence.
*/
?>


<?php

//Uses
use frontend\components\Menu;
use frontend\widgets\dactylkit\DactylKit;

/* items for rendering overlay menu => icon, label and link URL */
$overlayMenu = [
    [
        'icon' => '@frontend/web/source_assets/img/icon/ic-menu-60-ghz.svg',
        'label' => _tF('60ghz', 'menu'),
        'url' => '#',
    ],
//    [
//        'icon' => '@frontend/web/source_assets/img/icon/ic-menu-ap.svg',
//        'label' => _tF('access points', 'menu'),
//        'url' => '#',
//    ],
//    [
//        'icon' => '@frontend/web/source_assets/img/icon/ic-menu-toll-gate.svg',
//        'label' => _tF('toll gates', 'menu'),
//        'url' => '#',
//    ],
//    [
//        'icon' => '@frontend/web/source_assets/img/icon/ic-menu-52-ghz.svg',
//        'label' => _tF('52ghz', 'menu'),
//        'url' => '#',
//    ],
]

?>
  <header class="dk--header">
    <div class="dk--nav-bar">
      <div class="dk--nav-bar__container">
          <?php if (count($overlayMenu) > 1): ?>

              <?= DactylKit::button('',
                  DactylKit::BUTTON_TYPE_GHOST,
                  DactylKit::BUTTON_SIZE_MINI,
                  '@frontend/web/source_assets/img/icon/ic-apps-24.svg',
                  '',
                  ['id' => 'dk--menu-overlay__button', 'class' => 'dk--menu-overlay__button']
              ) ?><?php endif; ?>


        <a href="<?= Yii::$app->getHomeUrl() ?>"
           class="logo"><?= DactylKit::icon('@frontend/web/source_assets/layout/img/logo.svg') ?></a>
        <a href="<?= Yii::$app->getHomeUrl() ?>"
           class="logo logo__mobile"><?= DactylKit::icon('@frontend/web/source_assets/layout/img/logo-mobile.svg') ?></a>
      </div>
        <?php if (count($overlayMenu) > 1): ?>
          <div id="dk--menu-overlay" class="dk--menu-overlay dk--content-box">
            <div class="dk--menu-overlay__content">
                <?php
                foreach ($overlayMenu as $menuItem): ?>
                  <a href="<?= $menuItem['url'] ?>" class="dk--menu-overlay__item col-md-6">
                      <?= DactylKit::icon($menuItem['icon']) ?>
                    <span class="dk--menu-overlay__item--label"><?= $menuItem['label'] ?></span>
                  </a>
                <?php endforeach; ?>
            </div>
          </div>
        <?php endif; ?>
      <div class="dk--nav-bar__container">
          <?php if (!userIsLogged()): ?>
              <?= Menu::widget([
                  'id' => 'header-menu-public',
                  'position' => 1,
                  'type' => dactylcore\menu\frontend\widgets\menu\Menu::TYPE_NAVBAR,
                  'template' => '{items}',
                  'responsive' => true,
                  'responsiveBreakpoint' => 768,
                  'assetClass' => \frontend\assets\MenuAsset::class,
              ]); ?>

              <?= DactylKit::link(
                  '',
                  url(['/live-box/help']),
                  DactylKit::LINK_TYPE_TERTIARY,
                  DactylKit::LINK_SIZE_NORMAL, DactylKit::ICON_HELP, '', [
                  'class' => 'dk--btn dk--btn--ghost dk--btn--mini dk--nav-bar__btn-ml dk--nav-bar__hide-md',
                  'data' => [
                      'pjax' => 1,
                      'livebox' => 1,
                      'livebox-customparams' => '{"close":true}',
                  ],
              ]) ?>
          <?php else: ?>
              <?= Menu::widget([
                  'id' => 'header-menu-logged',
                  'position' => 2,
                  'type' => dactylcore\menu\frontend\widgets\menu\Menu::TYPE_NAVBAR,
                  'template' => '{items}',
                  'responsive' => true,
                  'responsiveBreakpoint' => 768,
                  'assetClass' => \frontend\assets\MenuAsset::class,
              ]); ?>
              <?= DactylKit::link(
                  '',
                  url(['/live-box/help']),
                  DactylKit::LINK_TYPE_TERTIARY,
                  DactylKit::LINK_SIZE_NORMAL, DactylKit::ICON_HELP, '', [
                  'class' => 'dk--btn dk--btn--ghost dk--btn--mini dk--nav-bar__hide-md',
                  'data' => [
                      'pjax' => 1,
                      'livebox' => 1,
                      'livebox-customparams' => '{"close":true}',
                  ],
              ]) ?>

            <a href="<?= url(['/dc-user/user/index']) ?>"
               class="btn dk--nav-bar__btn-ml"><?= loggedUser()->getInitialsIcon("small") ?></a>
          <?php endif; ?>
        <div id="menu-hamburger" class="menu-hamburger">
          <p><?= _tU('menu', 'header') ?></p>
          <div class="dc-menu-hamburger">
            <span class="top"></span>
            <span class="mid"></span>
            <span class="bot"></span>
          </div>
        </div>
      </div>
    </div>

      <?= $this->render('notifications'); ?>

      <?php if ($this->showSubHeader) : ?>
        <div class="dk--nav-bar__extended">
          <div class="dk--nav-bar" id="vueAppHeader">
              <?= $this->subHeaderContent ?>
          </div>
        </div>
      <?php endif; ?>
  </header>

<?php
$this->registerJsVar('langIso', getLangShortCode());
?>