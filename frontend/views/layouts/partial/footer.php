<?php
/*
* Copyright 2023 DactylGroup s.r.o.
*
* Licensed under the EUPL, Version 1.2 or – as soon they
will be approved by the European Commission - subsequent
versions of the EUPL (the "Licence");
* You may not use this work except in compliance with the
Licence.
* You may obtain a copy of the Licence at:
*
* https://joinup.ec.europa.eu/software/page/eupl5
*
* Unless required by applicable law or agreed to in
writing, software distributed under the Licence is
distributed on an "AS IS" basis,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
express or implied.
* See the Licence for the specific language governing
permissions and limitations under the Licence.
*/
?>


<?php

//Uses
use dactylcore\core\widgets\common\select2\Select2;
use frontend\widgets\LanguagePicker;


?>

<footer class="dk--footer" id="footer">
    <div class="dk--footer__content col-lg-6">
        <div class="dk--footer__content--half col-lg-6">
            <div class="dk--footer--language-select">
                <?= Select2::widget([
                    'id' => 'lang-select',
                    'name' => 'lang-select',
                    'data' => LanguagePicker::getLanguagePickerData(),
                    'pluginOptions' => [
                        'allowClear' => false,
                        'placeholder' => _tF('language', 'language'),
                    ],
                    'value' => Yii::$app->request->url,
                ]) ?>
            </div>
            <div class="row no-gutters justify-content-between">
                <div class="dk--footer__left-container">
                    <span class="dk--footer__copyright"><?= _tF('copyright', 'footer') ?></span>
                </div>
                <div class="dk--footer__right-container">
                    <?= \dactylcore\menu\frontend\widgets\menu\Menu::widget([
                        'id' => 'footer-menu',
                        'position' => 3,
                        'type' => dactylcore\menu\frontend\widgets\menu\Menu::TYPE_NAVBAR,
                        'template' => '{items}',
                        'responsive' => true,
                        'responsiveBreakpoint' => 1024,
                        'assetClass' => \frontend\assets\MenuAsset::class
                    ]) ?>
                </div>
            </div>
        </div>
    </div>
</footer>
