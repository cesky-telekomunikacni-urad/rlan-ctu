<?php
/*
* Copyright 2023 DactylGroup s.r.o.
*
* Licensed under the EUPL, Version 1.2 or – as soon they
will be approved by the European Commission - subsequent
versions of the EUPL (the "Licence");
* You may not use this work except in compliance with the
Licence.
* You may obtain a copy of the Licence at:
*
* https://joinup.ec.europa.eu/software/page/eupl5
*
* Unless required by applicable law or agreed to in
writing, software distributed under the Licence is
distributed on an "AS IS" basis,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
express or implied.
* See the Licence for the specific language governing
permissions and limitations under the Licence.
*/
?>


<?php
// Uses

// Sources
/* @var $this \dactylcore\core\web\FrontendView */
/* @var $content string */

\frontend\assets\LayoutAsset::register($this);

?>

<?php $this->beginContent('@frontend/views/layouts/partial/head.php'); ?>

    <body>
    <?php $this->beginBody(); ?>

    <?= $this->render('partial/header'); ?>

    <main>
        <section id="header-offset" <?= $this->showSubHeader ? 'class="extended"' : '' ?>></section>
        <div class="center-container">
        <?= $content; ?>
        </div>
    </main>

    <?= $this->render('partial/footer'); ?>

    <?php $this->endBody(); ?>
    </body>

<?php $this->endContent(); ?>

<script nonce="<?= Yii::$app->params['identCSP'] ?>">getAllFlashAlertMessages();</script>
