<?php
/*
* Copyright 2023 DactylGroup s.r.o.
*
* Licensed under the EUPL, Version 1.2 or – as soon they
will be approved by the European Commission - subsequent
versions of the EUPL (the "Licence");
* You may not use this work except in compliance with the
Licence.
* You may obtain a copy of the Licence at:
*
* https://joinup.ec.europa.eu/software/page/eupl5
*
* Unless required by applicable law or agreed to in
writing, software distributed under the Licence is
distributed on an "AS IS" basis,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
express or implied.
* See the Licence for the specific language governing
permissions and limitations under the Licence.
*/
?>


<?php
// Uses
use dactylcore\core\widgets\common\pjax\Pjax;
use frontend\assets\UserAllianceAsset;
use frontend\widgets\dactylkit\DactylKit;

// Sources
/* @var $userId int */
/* @var $byAdmin bool */


// Settings
$pageId = 'user-alliance';
$this->title = _tF('alliance_setup', 'user-alliance');
$moduleId = Yii::$app->controller->module->id;

UserAllianceAsset::register($this);
?>


<div class="livebox-user">
    <div class="livebox-user__header"></div>
    <div class="livebox-user__content">

        <?php Pjax::begin([
            'id' => "{$pageId}-pjax",
            'enablePushState' => false,
            'enableReplaceState' => false,
        ]); ?>

        <h4 class="livebox-user__content__title"><?= $this->title ?></h4>
        <div class="livebox-user__content__texts">
            <p>
                <?= _t('without_text', 'user-alliance', ['user_id' => $userId]) ?>
            </p>
        </div>
        <div class="livebox-user__content__button">
            <?= DactylKit::link(
                _tF('create_alliance', 'user-alliance'),
                ($byAdmin) ? url(['user-alliance/create', 'idUser' => $userId]) : url(['user-alliance/create']),
                DactylKit::LINK_TYPE_PRIMARY,
                DactylKit::LINK_SIZE_NORMAL, '', '', [
                'class' => 'dk--btn dk--btn--primary',
            ]) ?>
        </div>

        <?php Pjax::end(); ?>
    </div>
</div>
