<?php
/*
* Copyright 2023 DactylGroup s.r.o.
*
* Licensed under the EUPL, Version 1.2 or – as soon they
will be approved by the European Commission - subsequent
versions of the EUPL (the "Licence");
* You may not use this work except in compliance with the
Licence.
* You may obtain a copy of the Licence at:
*
* https://joinup.ec.europa.eu/software/page/eupl5
*
* Unless required by applicable law or agreed to in
writing, software distributed under the Licence is
distributed on an "AS IS" basis,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
express or implied.
* See the Licence for the specific language governing
permissions and limitations under the Licence.
*/
?>


<?php
// Uses
use common\models\Station;
use common\models\UserAlliance;
use dactylcore\core\data\ActiveDataProvider;
use dactylcore\core\widgets\common\gridview\GridView;
use dactylcore\core\widgets\common\pjax\Pjax;
use dactylcore\core\widgets\admin\select2\Select2Asset;
use dactylcore\core\widgets\common\select2\Select2;
use frontend\assets\DactylKitAsset;
use frontend\assets\LayoutAsset;
use frontend\assets\StationAsset;
use frontend\assets\UserAllianceAsset;
use frontend\assets\UserAsset;
use frontend\components\FrontendView;
use frontend\widgets\dactylkit\DactylKit;
use modules\user\common\models\User;
use yii\helpers\Html;

// Sources
/* @var $this FrontendView */
/* @var $userId int */
/* @var $model UserAlliance */
/* @var $isAdmin bool */
/* @var $byAdmin bool */
/* @var $dataProvider ActiveDataProvider */


// Settings
$pageId = 'user-alliance';
$this->title = $model->name;
$moduleId = Yii::$app->controller->module->id;

UserAllianceAsset::register($this);
Select2Asset::register($this);
LayoutAsset::register($this);
DactylKitAsset::register($this);
UserAsset::register($this);
StationAsset::register($this);
?>


<div class="livebox-user">
    <div class="livebox-user__header"></div>
    <div class="livebox-user__content">

        <?php Pjax::begin([
            'id' => "{$pageId}-pjax",
            'enablePushState' => false,
            'enableReplaceState' => false,
        ]); ?>

        <div class="livebox-user__content__title">
            <h4 id="alliance-name"><?= $this->title ?></h4>
            <?php if ($isAdmin): ?>
                <span id="change-name-request" class="livebox-user__content__title__btn"
                      <?= DactylKit::tooltip(_tF('change_alliance_name', 'user-alliance')); ?>>
                        <?= DactylKit::icon(DactylKit::ICON_EDIT_24) ?>
                    </span>
                <?= DactylKit::link(
                    DactylKit::icon(DactylKit::ICON_SUCCESS),
                    ($byAdmin) ?
                        url(['user-alliance/change-alliance-name', 'newName' => $model->name, 'idUser' => $userId,]) :
                        url(['user-alliance/change-alliance-name', 'newName' => $model->name]),
                    DactylKit::LINK_TYPE_PRIMARY,
                    DactylKit::LINK_SIZE_MINI,
                    '', '', [
                        'id' => 'name-change-link',
                        'class' => 'd-none livebox-user__content__title__btn',
                        'data' => [
                            'toggle' => "tooltip",
                            'placement' => DactylKit::TOOLTIP_TOP,
                            'original-title' => _tF('save_new_name', 'user-alliance'),
                        ],
                    ]
                ) ?>
            <?php endif; ?>
        </div>

        <div class="livebox-user__content__description">
            <p id="alliance-description">
                <?= $model->description ?>
            </p>
            <span id="change-description-request" class="livebox-user__content__description__btn"
                      <?= DactylKit::tooltip(_tF('change_alliance_description', 'user-alliance')); ?>>
                        <?= ($model->description) ?
                            _tF('update_description', 'user-alliance') :
                            _tF('create_description', 'user-alliance')
                        ?>
                    </span>
            <?= DactylKit::link(
                _tF('save_description', 'user-alliance'),
                ($byAdmin) ?
                    url(['user-alliance/change-alliance-description', 'idUser' => $userId,]) :
                    url(['user-alliance/change-alliance-description']),
                DactylKit::LINK_TYPE_PRIMARY,
                DactylKit::LINK_SIZE_MINI,
                '', '', [
                    'id' => 'description-change-link',
                    'class' => 'd-none livebox-user__content__description__btn',
                    'data' => [
                        'method' => 'POST',
                        'pjax' => "1",
                        'params' => [
                            'newDescription' => $model->description,
                        ],
                    ],
                ]
            ) ?>
        </div>

        <div class="livebox-user__content__alliance-users">
            <table class="user-list">
                <thead>
                <tr>
                    <th><?= _tU('id', 'user-alliance'); ?></th>
                    <th><?= _tU('user_name', 'user-alliance'); ?></th>
                    <th><?= _tU('role', 'user-alliance'); ?></th>
                    <th colspan="2"><?= _tU('action', 'user-alliance'); ?></th>
                </tr>
                </thead>
                <tbody>
                <?php foreach ($model->getUserList()->each() as $userArray): ?>
                    <tr class="user-list__user-item">
                        <td class="user-list__user-item__user-id"><?= str_pad($userArray['id'], 6) ?></td>
                        <td class="user-list__user-item__user-name">
                            <?php
                            if ($userArray['type'] == User::USER_TYPE_COMPANY) {
                                $userName = $userArray['companyName'];
                                if (trim($userName) == '') {
                                    $userName = "{$userArray['first_name']} {$userArray['last_name']}";
                                }
                            } else {
                                $userName = "{$userArray['first_name']} {$userArray['last_name']}";
                                if (trim($userName) == '') {
                                    $userName = $userArray['companyName'];
                                }
                            }
                            ?>
                            <?= (trim($userName) == '') ? $userArray['email'] : $userName; ?>
                        </td>
                        <td class="user-list__user-item__user-role">
                            <?= _tF("{$userArray['membership']}_role", 'user-alliance') ?>
                        </td>
                        <?php if ($isAdmin && $userArray['id'] != $userId): ?>
                            <td class="user-list__user-item__user-action__promote">
                                <?php if ($userArray['membership'] === UserAlliance::STATUS_MEMBER): ?>
                                    <?= DactylKit::link(
                                        DactylKit::icon(DactylKit::ICON_VERIFIED_24),
                                        ($byAdmin) ?
                                            url([
                                                'user-alliance/promote-member',
                                                'idUser' => $userArray['id'],
                                                'idAdmin' => $userId,
                                            ]) :
                                            url([
                                                'user-alliance/promote-member',
                                                'idUser' => $userArray['id'],
                                            ]),
                                        DactylKit::LINK_TYPE_PRIMARY,
                                        DactylKit::LINK_SIZE_NORMAL,
                                        '', '', [
                                            'class' => 'action-link',
                                            'data' => [
                                                'toggle' => "tooltip",
                                                'placement' => DactylKit::TOOLTIP_TOP,
                                                'original-title' => _tF('promote_member', 'user-alliance'),
                                            ],
                                        ]
                                    ) ?>
                                <?php else: ?>
                                    <?= DactylKit::link(
                                        DactylKit::icon(DactylKit::ICON_UNVERIFIED_24),
                                        ($byAdmin) ?
                                            url([
                                                'user-alliance/degrade-member',
                                                'idUser' => $userArray['id'],
                                                'idAdmin' => $userId,
                                            ]) :
                                            url([
                                                'user-alliance/degrade-member',
                                                'idUser' => $userArray['id'],
                                            ]),
                                        DactylKit::LINK_TYPE_PRIMARY,
                                        DactylKit::LINK_SIZE_NORMAL,
                                        '', '', [
                                            'class' => 'action-link',
                                            'data' => [
                                                'toggle' => "tooltip",
                                                'placement' => DactylKit::TOOLTIP_TOP,
                                                'original-title' => _tF('degrade_member', 'user-alliance'),
                                            ],
                                        ]
                                    ) ?>
                                <?php endif; ?>
                            </td>
                            <td class="user-list__user-item__user-action__exclude">
                                <?= DactylKit::link(
                                    DactylKit::icon(DactylKit::ICON_NO_USERS_24),
                                    ($byAdmin) ?
                                        url([
                                            'user-alliance/exclude-member',
                                            'idUser' => $userArray['id'],
                                            'idAdmin' => $userId,
                                        ]) :
                                        url([
                                            'user-alliance/exclude-member',
                                            'idUser' => $userArray['id'],
                                        ]),
                                    DactylKit::LINK_TYPE_PRIMARY,
                                    DactylKit::LINK_SIZE_NORMAL,
                                    '', '', [
                                        'class' => 'action-link',
                                        'data' => [
                                            'toggle' => "tooltip",
                                            'placement' => DactylKit::TOOLTIP_TOP,
                                            'original-title' => _tF('exclude_member', 'user-alliance'),
                                        ],
                                    ]
                                ) ?>
                            </td>
                        <?php else: ?>
                            <td></td>
                            <td></td>
                        <?php endif; ?>
                    </tr>
                <?php endforeach; ?>
                </tbody>
            </table>

            <?php if ($isAdmin): ?>
                <div class="user-invite-select">
                    <div class="row">
                        <div class="col-md-7">
                            <?= Select2::widget([
                                'name' => 'user-invite-select',
                                'id' => 'user-invite-select',
                                'hideSearch' => false,
                                'data' => [],
                                'pluginOptions' => [
                                    'allowClear' => true,
                                    'placeholder' => _tF('choose_user', 'user-alliance'),
                                    'ajax' => [
                                        'url' => url(['/dc-user/user/user-dropdown-data-ids']),
                                        'dataType' => 'json',
                                        'data' => new \yii\web\JsExpression(
                                            <<<JS
                                    function (params) {
                                        return {
                                            term: params.term,
                                            page: params.page || 1
                                        }
                                    }
JS
                                        ),
                                    ],
                                ],
                            ]) ?>
                        </div>
                        <div class="col-md-5 text-right">
                            <?= DactylKit::button(
                                _tF('invite_user', 'user-alliance'),
                                DactylKit::LINK_TYPE_SECONDARY,
                                DactylKit::LINK_SIZE_TALL, '', '', [
                                    'id' => 'user-alliance-send-invitation',
                                    'class' => 'dk--btn dk--btn--secondary',
                                    'data' => [
                                        'user-id' => ($byAdmin) ? $userId : '',
                                    ],
                                ]
                            ) ?>
                        </div>
                    </div>
                </div>
            <?php endif; ?>
        </div>
        <div class="livebox-user__content__button">
            <?php if ($isAdmin): ?>
                <?= DactylKit::button(
                    _tF('delete_alliance', 'user-alliance'),
                    DactylKit::LINK_TYPE_PRIMARY,
                    DactylKit::LINK_SIZE_NORMAL, '', '', [
                    'class' => 'dk--btn dk--btn--danger',
                    'data' => [
                        'confirm-text' => _t('want_delete_alliance', 'user-alliance'),
                        'href' => ($byAdmin) ? url(['user-alliance/delete', 'idUser' => $userId]) :
                            url(['user-alliance/delete']),
                        'confirm-true' => _tF('yes_remove', 'user-alliance'),
                        'confirm-false' => _tF('no_remove', 'user-alliance'),
                        'confirm-title' => _tF('really', 'user-alliance'),
                    ],
                ]) ?>
            <?php else: ?>
                <?= DactylKit::button(
                    _tF('resign_alliance', 'user-alliance'),
                    DactylKit::LINK_TYPE_PRIMARY,
                    DactylKit::LINK_SIZE_NORMAL, '', '', [
                    'class' => 'dk--btn dk--btn--danger',
                    'data' => [
                        'confirm-text' => _t('want_resign_alliance', 'user-alliance'),
                        'href' => ($byAdmin) ? url(['user-alliance/resign', 'idUser' => $userId]) :
                            url(['user-alliance/resign']),
                        'confirm-true' => _tF('yes_resign', 'user-alliance'),
                        'confirm-false' => _tF('no_resign', 'user-alliance'),
                        'confirm-title' => _tF('really', 'user-alliance'),
                    ],
                ]) ?>
            <?php endif; ?>
        </div>
        <div class="livebox-user__content__station-list">
            <?= GridView::widget([
                'dataProvider' => $dataProvider,
                'pjax' => false,
                'filterModel' => null,
                'layout' => '<div>{items}</div>',
                'columns' => [
                    [
                        'attribute' => 'id_user',
                        'value' => function ($model)
                        {
                            return $model->getUserIdPadded();
                        },
                    ],
                    [
                        'attribute' => 'id',
                        'label' => _tF('id', 'station'),
                        'value' => function ($model)
                        {
                            return $model->formatThisId();
                        },

                    ],
                    [
                        'attribute' => 'typeShortCut',
                        'label' => _tF('typeShortCut', 'station'),
                    ],
                    [
                        'attribute' => 'name',
                        'label' => _tF('name', 'station'),
                        'format' => 'raw',
                        'value' => function ($model)
                        {
                            /**
                             * @var $model Station
                             */
                            if ($model->isFs()) {
                                $stationSearch = new \common\models\search\StationSearch();
                                $pair = $stationSearch->getModelsFs($model->id);
                                // determine pair position based on ID's -> station A has always smaller ID
                                $stationAId = $pair->stationA->id < $pair->stationB->id ? $pair->stationA->id :
                                    $pair->stationB->id;

                                $img = $stationAId == $model->id ?
                                    DactylKit::icon('@frontend/web/source_assets/img/icon/ic-position-a.svg') :
                                    DactylKit::icon('@frontend/web/source_assets/img/icon/ic-position-b.svg');

                                $linkText = $model->name . $img;

                                return Html::a($linkText, ['station/station', 'id' => $model->id],
                                    ['target' => '_blank', 'data-pjax' => "0"]);
                            } else {
                                return Html::a($model->name, ['station/station', 'id' => $model->id],
                                    ['target' => '_blank', 'data-pjax' => "0"]);
                            }
                        },

                    ],
                    [
                        'attribute' => 'status',
                        'format' => 'raw',
                        'value' => function ($model)
                        {
                            $status = ($model->status == 'concept') ? Station::STATUS_DRAFT : $model->status;
                            return Html::tag('span', _tF("status_{$status}", 'station'), [
                                'class' => "statusColumn {$status}",
                            ]);
                        },
                    ],
                ],


            ]) ?>
        </div>

        <?php Pjax::end(); ?>
    </div>
</div>