<?php
/*
* Copyright 2023 DactylGroup s.r.o.
*
* Licensed under the EUPL, Version 1.2 or – as soon they
will be approved by the European Commission - subsequent
versions of the EUPL (the "Licence");
* You may not use this work except in compliance with the
Licence.
* You may obtain a copy of the Licence at:
*
* https://joinup.ec.europa.eu/software/page/eupl5
*
* Unless required by applicable law or agreed to in
writing, software distributed under the Licence is
distributed on an "AS IS" basis,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
express or implied.
* See the Licence for the specific language governing
permissions and limitations under the Licence.
*/
?>


<?php
// Uses
use dactylcore\core\web\AdminView;
use common\models\UserAlliance;
use dactylcore\core\widgets\common\bootstrap\Html;
use frontend\widgets\ActiveForm;
use frontend\widgets\dactylkit\DactylKit;

// Sources
/* @var $this AdminView */
/* @var $model UserAlliance */
/* @var $byAdmin bool */
/* @var $userId int */

// Settings
$pageId = 'user-alliance';
$formId = "{$pageId}-form";
$moduleId = Yii::$app->controller->module->id;
$this->title = _tF("create_user_alliance", 'user-alliance');


$saveButton = Html::button(_tF("save", "default"), [
    "type" => "submit",
    "form" => $formId,
    "class" => "btn btn-primary",
    "value" => "noRedirect",
]);

?>

<div class="livebox-user">
    <div class="livebox-user__header"></div>
    <div class="livebox-user__content register-forms">

        <?php $form = ActiveForm::begin([
            'id' => "{$pageId}",
            'pjaxOptions' => [
                'enablePushState' => false,
                'enableReplaceState' => false,
            ],
        ]); ?>

        <h4 class="livebox-user__content__title"><?= $this->title ?></h4>
        <div class="livebox-user__content__forms">

            <input name="redirect" type="hidden" value="1" class="redirect">

            <div class="portlet">
                <div class="body">
                    <?= $form->field($model, 'name')->textInput(['maxlength' => true]) ?>

                    <?= $form->field($model, 'description')->textarea(['rows' => 6]) ?>
                </div>
            </div>

            <div class="livebox-user__content__buttons">
                <?= DactylKit::link(
                    _tF('cancel', 'default'),
                    ($byAdmin) ?
                        url(['user-alliance/index', 'idUser' => $userId]) :
                        url(['user-alliance/index']),
                    DactylKit::LINK_TYPE_SECONDARY,
                    DactylKit::LINK_SIZE_NORMAL, '', '', [
                    'class' => 'dk--btn dk--btn--secondary',
                ]) ?>
                <?= DactylKit::button(
                    _tF('create_alliance', 'user-alliance'),
                    DactylKit::BUTTON_TYPE_PRIMARY,
                    DactylKit::LINK_SIZE_NORMAL,
                    '',
                    '',
                    [
                        'type' => 'submit',
                        'data-pjax' => true,
                    ])
                ?>
            </div>
        </div>

        <?php ActiveForm::end(); ?>
    </div>
</div>
