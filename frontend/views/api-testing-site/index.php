<?php
/*
* Copyright 2023 DactylGroup s.r.o.
*
* Licensed under the EUPL, Version 1.2 or – as soon they
will be approved by the European Commission - subsequent
versions of the EUPL (the "Licence");
* You may not use this work except in compliance with the
Licence.
* You may obtain a copy of the Licence at:
*
* https://joinup.ec.europa.eu/software/page/eupl5
*
* Unless required by applicable law or agreed to in
writing, software distributed under the Licence is
distributed on an "AS IS" basis,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
express or implied.
* See the Licence for the specific language governing
permissions and limitations under the Licence.
*/
?>


<?php
// Sources
/* @var $this \dactylcore\core\web\FrontendView */
/* @var $message string */

// Asset Bundles
use frontend\assets\LayoutAsset;
use frontend\widgets\dactylkit\DactylKit;

$this->title = _tF('api_testing_index_title', 'api-testing');

// Breadcrumbs
$this->breadcrumbs[] = [
    'label' => $this->title,
];

LayoutAsset::register($this);
\frontend\assets\CommonPageAsset::register($this);

?>

<div class="dk--column-holder row">
    <section id="page" class="page">
        <?= DactylKit::breadcrumb() ?>
        <h3 class="dk--page--title"><?= $this->title; ?></h3>
        <div id="page-elements" class="dk--page--content">
            <?= _tF('api_testing_index_content', 'api-testing') ?>
        </div>
    </section>
</div>