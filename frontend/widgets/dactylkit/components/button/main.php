<?php
/*
* Copyright 2023 DactylGroup s.r.o.
*
* Licensed under the EUPL, Version 1.2 or – as soon they
will be approved by the European Commission - subsequent
versions of the EUPL (the "Licence");
* You may not use this work except in compliance with the
Licence.
* You may obtain a copy of the Licence at:
*
* https://joinup.ec.europa.eu/software/page/eupl5
*
* Unless required by applicable law or agreed to in
writing, software distributed under the Licence is
distributed on an "AS IS" basis,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
express or implied.
* See the Licence for the specific language governing
permissions and limitations under the Licence.
*/
?>


<?php

use frontend\widgets\dactylkit\DactylKit;
use yii\helpers\Html;
use yii\web\View;

/* @var $this View */
/* @var $label string */
/* @var $type string */
/* @var $size string */
/* @var $iconLeftPath string */
/* @var $iconRightPath string */
/* @var $options array */
$class = $options['class'] ?? '';
$options['class'] = "dk--btn {$type} {$size} {$class}";
?>

<button <?= Html::renderTagAttributes($options) ?>>
    <?php if ($iconLeftPath): ?>
        <div class="dk--btn__icon">
            <?= DactylKit::icon($iconLeftPath); ?>
        </div>
    <?php endif; ?>

    <?php if ($label): ?>
        <span class="dk--btn__label"><?= $label ?></span>
    <?php endif; ?>

    <?php if ($iconRightPath): ?>
        <div class="dk--btn__icon">
            <?= DactylKit::icon($iconRightPath); ?>
        </div>
    <?php endif; ?>
</button>
