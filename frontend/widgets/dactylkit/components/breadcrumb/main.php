<?php
/*
* Copyright 2023 DactylGroup s.r.o.
*
* Licensed under the EUPL, Version 1.2 or – as soon they
will be approved by the European Commission - subsequent
versions of the EUPL (the "Licence");
* You may not use this work except in compliance with the
Licence.
* You may obtain a copy of the Licence at:
*
* https://joinup.ec.europa.eu/software/page/eupl5
*
* Unless required by applicable law or agreed to in
writing, software distributed under the Licence is
distributed on an "AS IS" basis,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
express or implied.
* See the Licence for the specific language governing
permissions and limitations under the Licence.
*/
?>


<?php

use frontend\components\FrontendView;
use yii\widgets\Breadcrumbs;

/* @var $this FrontendView */

$slash = file_get_contents(\Yii::getAlias('@frontend') . "/web/source_assets/img/icon/ic-slash-20.svg");
?>

<div id="breadcrumb">
    <?= Breadcrumbs::widget([
        'options' => [
            'class' => 'breadcrumb',
        ],
        'itemTemplate' => '<li class="breadcrumb__item">{link}</li><li class="breadcrumb__delimiter">' . $slash . '</li>',
        'activeItemTemplate' => '<li class="breadcrumb__item breadcrumb__item--current">{link}</li>',
        'links' => $this->breadcrumbs,
    ]); ?>
</div>
