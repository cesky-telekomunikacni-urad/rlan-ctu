<?php
/*
* Copyright 2023 DactylGroup s.r.o.
*
* Licensed under the EUPL, Version 1.2 or – as soon they
will be approved by the European Commission - subsequent
versions of the EUPL (the "Licence");
* You may not use this work except in compliance with the
Licence.
* You may obtain a copy of the Licence at:
*
* https://joinup.ec.europa.eu/software/page/eupl5
*
* Unless required by applicable law or agreed to in
writing, software distributed under the Licence is
distributed on an "AS IS" basis,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
express or implied.
* See the Licence for the specific language governing
permissions and limitations under the Licence.
*/
?>


<?php

//Uses
use dactylcore\core\helpers\Html;
use dactylcore\core\web\FrontendView;
use frontend\widgets\dactylkit\DactylKit;

/* @var $this FrontendView */
/* @var $label string */
/* @var $type string */
/* @var $size string */
/* @var $iconLeftPath string */
/* @var $iconRightPath string */
/* @var $options array */
$class = $options['class'] ?? '';
$options['class'] = "dk--chip {$type} {$size} {$class}";
?>

<div <?= Html::renderTagAttributes($options) ?>>
    <?php if ($iconLeftPath): ?>
        <div class="dk--chip__icon">
            <?= DactylKit::icon($iconLeftPath); ?>
        </div>
    <?php endif; ?>
    <span class="dk--chip__label"><?= $label ?></span>
    <?php if ($iconRightPath): ?>
    <div class="dk--chip__icon">
        <?= DactylKit::icon($iconRightPath); ?>
    </div>
    <?php endif; ?>
</div>
