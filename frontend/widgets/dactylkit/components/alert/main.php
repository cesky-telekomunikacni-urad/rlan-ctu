<?php
/*
* Copyright 2023 DactylGroup s.r.o.
*
* Licensed under the EUPL, Version 1.2 or – as soon they
will be approved by the European Commission - subsequent
versions of the EUPL (the "Licence");
* You may not use this work except in compliance with the
Licence.
* You may obtain a copy of the Licence at:
*
* https://joinup.ec.europa.eu/software/page/eupl5
*
* Unless required by applicable law or agreed to in
writing, software distributed under the Licence is
distributed on an "AS IS" basis,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
express or implied.
* See the Licence for the specific language governing
permissions and limitations under the Licence.
*/
?>


<?php

use frontend\widgets\dactylkit\DactylKit;
use yii\helpers\Html;
use yii\web\View;

/* @var $this View */
/* @var $type string */
/* @var $widerTopBorder bool */
/* @var $title string */
/* @var $text string */
/* @var $iconPath string */
/* @var $showCloseButton bool */
/* @var $options array */
/* @var $button array */

$class = $options['class'] ?? '';
if ($widerTopBorder) {
    $class .= ' dk-alert--top-border';
}

$options['class'] = "dk-alert {$type} {$class}";
?>

<div <?= Html::renderTagAttributes($options) ?>>
    <?php if ($iconPath): ?>
        <div class="dk-alert__icon">
            <?= DactylKit::icon($iconPath); ?>
        </div>
    <?php endif; ?>

    <div class="dk-alert__body">
        <?php if ($title): ?>
            <div class="dk-alert__title"><?= $title ?></div>
        <?php endif; ?>

        <?php if ($text): ?>
            <div class="dk-alert__text"><?= $text ?></div>
        <?php endif; ?>

        <?php if ($button['label']): ?>
            <?php if ($button['url']): ?>
                <?= DactylKit::link(
                    $button['label'],
                    $button['url'],
                    DactylKit::LINK_TYPE_TERTIARY,
                    DactylKit::LINK_SIZE_NORMAL,
                    '',
                    '',
                    $button['options']
                ) ?>
            <?php else: ?>
                <?= DactylKit::button(
                    $button['label'],
                    DactylKit::BUTTON_TYPE_TERTIARY,
                    DactylKit::BUTTON_SIZE_NORMAL,
                    '',
                    '',
                    $button['options']
                ) ?>
            <?php endif; ?>
        <?php endif; ?>
    </div>

    <?php if ($showCloseButton): ?>
        <div class="dk-alert__close">
            <img src="/frontend/widgets/dactylkit/components/alert/assets/img/ic-close-24.svg" alt="">
        </div>
    <?php endif; ?>

</div>
