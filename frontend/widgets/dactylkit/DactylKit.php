<?php
/*
* Copyright 2023 DactylGroup s.r.o.
*
* Licensed under the EUPL, Version 1.2 or – as soon they
will be approved by the European Commission - subsequent
versions of the EUPL (the "Licence");
* You may not use this work except in compliance with the
Licence.
* You may obtain a copy of the Licence at:
*
* https://joinup.ec.europa.eu/software/page/eupl5
*
* Unless required by applicable law or agreed to in
writing, software distributed under the Licence is
distributed on an "AS IS" basis,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
express or implied.
* See the Licence for the specific language governing
permissions and limitations under the Licence.
*/
?>


<?php

namespace frontend\widgets\dactylkit;

use dactylcore\core\helpers\Html;
use dactylcore\core\widgets\common\activeform\ActiveField;
use yii\base\View;
use Yii;

class DactylKit
{
    /**
     * Returns "view" application component.
     * @return View the view object that can be used to render views or view files.
     */
    protected static function getView(): View
    {
        return Yii::$app->getView();
    }

    /**
     * Renders partial view file.
     *
     * @param string $view path to view file
     * @param array $params view params
     *
     * @return string content
     */
    protected static function render(string $view, array $params = []): string
    {
        return static::getView()->render("@frontend/widgets/dactylkit/components/$view", $params);
    }

    const BUTTON_TYPE_PRIMARY = 'dk--btn--primary';
    const BUTTON_TYPE_SECONDARY = 'dk--btn--secondary';
    const BUTTON_TYPE_GHOST = 'dk--btn--ghost';
    const BUTTON_TYPE_CIRCLE = 'dk--btn--circle';
    const BUTTON_TYPE_FLOATING = 'dk--btn--floating';
    const BUTTON_SIZE_MINI = 'dk--btn--mini';

    public static function button(
        string $label,
        string $type = self::BUTTON_TYPE_PRIMARY,
        string $size = '',
        string $iconLeftPath = '',
        string $iconRightPath = '',
        array $options = []
    ): string {
        return static::render('button/main', [
            'label' => $label,
            'type' => $type,
            'size' => $size,
            'iconLeftPath' => $iconLeftPath,
            'iconRightPath' => $iconRightPath,
            'options' => $options,
        ]);
    }

    const SVG_MIME_TYPES = ['image/svg+xml', 'image/svg'];
    const ICON_CHEVRON_LEFT = '@frontend/web/source_assets/img/icon/ic-chevron-left-24.svg';
    const ICON_CHEVRON_RIGHT = '@frontend/web/source_assets/img/icon/ic-chevron-right-24.svg';
    const ICON_CHEVRON_DOWN = '@frontend/web/source_assets/img/icon/ic-chevron-down-24.svg';
    const ICON_SEARCH = '@frontend/web/source_assets/img/icon/ic-search-24.svg';
    const ICON_PLUS = '@frontend/web/source_assets/img/icon/ic-plus-16.svg';
    const ICON_LOCK = '@frontend/web/source_assets/img/icon/ic-lock-24.svg';
    const ICON_LIGHT_LOCK = '@frontend/web/source_assets/img/icon/ic-lock-light-24.svg';
    const ICON_SUCCESS = '@frontend/web/source_assets/img/icon/ic-check-circle-24.svg';
    const ICON_CIRCLE = '@frontend/web/source_assets/img/icon/ic-circle-20.svg';
    const ICON_SUCCESS_20 = '@frontend/web/source_assets/img/icon/ic-check-circle-20.svg';
    const ICON_WARNING = '@frontend/web/source_assets/img/icon/ic-warning-20.svg';
    const ICON_ERROR = '@frontend/web/source_assets/img/icon/ic-close-circle-20.svg';
    const ICON_HELP = '@frontend/web/source_assets/img/icon/ic-help-circle-24.svg';
    const ICON_A = '@frontend/web/source_assets/img/icon/ic-a-circle-20.svg';
    const ICON_B = '@frontend/web/source_assets/img/icon/ic-b-circle-20.svg';
    const ICON_CLOSE = '@frontend/web/source_assets/img/icon/ic-close-24.svg';
    const ICON_INFO = '@frontend/web/source_assets/img/icon/ic-info-circle-24.svg';
    const ICON_INFO_20 = '@frontend/web/source_assets/img/icon/ic-info-circle-20.svg';
    const ICON_ACCOUNT = '@frontend/web/source_assets/img/icon/ic-account-24.svg';
    const ICON_EYE = '@frontend/web/source_assets/img/icon/ic-visibility-gray-22.svg';
    const ICON_SETTINGS = '@frontend/web/source_assets/img/icon/ic-settings-16.svg';
    const ICON_WARNING_24 = '@frontend/web/source_assets/img/icon/ic-warning-24.svg';
    const ICON_ERROR_24 = '@frontend/web/source_assets/img/icon/ic-close-circle-24.svg';
    const ICON_EDIT_24 = '@frontend/web/source_assets/img/icon/ic-edit-24.svg';
    const ICON_VERIFIED_24 = '@frontend/web/source_assets/img/icon/ic-verified-24.svg';
    const ICON_UNVERIFIED_24 = '@frontend/web/source_assets/img/icon/ic-unverified-24.svg';
    const ICON_NO_USERS_24 = '@frontend/web/source_assets/img/icon/ic-no_accounts-24.svg';
    const ICON_GROUPS = '@frontend/web/source_assets/img/icon/ic-groups-24.svg';
    const ICON_IMPORT = '@frontend/web/source_assets/img/icon/ic-import-24.svg';
    const ICON_EXPORT = '@frontend/web/source_assets/img/icon/ic-export-24.svg';

    public static function icon(string $filePath): string
    {
        $icon = static::parseIconPath($filePath);

        return static::render('icon/main', [
            'icon' => $icon,
        ]);
    }

    protected static function parseIconPath(string $iconPath): array
    {
        $absoluteIconPath = Yii::getAlias($iconPath);
        if (!file_exists($absoluteIconPath)) {
            return [];
        }

        $iconMimeType = mime_content_type($absoluteIconPath);
        $iconUrl = str_replace(Yii::getAlias('@frontend'), '/frontend', $absoluteIconPath);
        return ['type' => $iconMimeType, 'url' => $iconUrl, 'path' => $absoluteIconPath];
    }

    const LINK_TYPE_PRIMARY = 'btn--primary';
    const LINK_TYPE_SECONDARY = 'btn--secondary';
    const LINK_TYPE_TERTIARY = 'btn--tertiary';
    const LINK_TYPE_GHOST = 'btn--ghost';
    const LINK_SIZE_NORMAL = 'btn--normal';
    const LINK_SIZE_MINI = 'btn--mini';
    const LINK_SIZE_TALL = 'btn--tall';

    public static function link(
        string $label,
        string $url,
        string $type = self::LINK_TYPE_PRIMARY,
        string $size = self::LINK_SIZE_NORMAL,
        string $iconLeftPath = '',
        string $iconRightPath = '',
        array $options = []
    ): string {
        return static::render('link/main', [
            'label' => $label,
            'url' => $url,
            'type' => $type,
            'size' => $size,
            'iconLeftPath' => $iconLeftPath,
            'iconRightPath' => $iconRightPath,
            'options' => $options,
        ]);
    }

    public static function breadcrumb(): string
    {
        return static::render('breadcrumb/main');
    }

    const ALERT_TYPE_DEFAULT = 'dk-alert--default';
    const ALERT_TYPE_INFO = 'dk-alert--info';
    const ALERT_TYPE_WARNING = 'dk-alert--warning';
    const ALERT_TYPE_SUCCESS = 'dk-alert--success';
    const ALERT_TYPE_CRITICAL = 'dk-alert--critical';

    /**
     * @param string $title
     * @param string $text
     * @param string $type use ALERT_TYPE_* constants
     * @param bool $widerTopBorder
     * @param string|null $iconPath null - use default icon, empty string - without icon or path to icon
     * @param bool $showCloseButton
     * @param array $options
     * @param string $buttonLabel
     * @param string $buttonUrl if set link link will be rendered, otherwise button
     * @param array $buttonOptions
     *
     * @return string
     */
    public static function alert(
        string $title = '',
        string $text = '',
        string $type = self::ALERT_TYPE_DEFAULT,
        bool $widerTopBorder = true,
        string $iconPath = null,
        bool $showCloseButton = false,
        array $options = [],
        string $buttonLabel = '',
        string $buttonUrl = '',
        array $buttonOptions = []
    ): string {
        if ($iconPath === null) {
            $iconPath = static::getDefaultAlertIconPath($type);
        }

        return static::render('alert/main', [
            'type' => $type,
            'widerTopBorder' => $widerTopBorder,
            'title' => $title,
            'text' => $text,
            'iconPath' => $iconPath,
            'showCloseButton' => $showCloseButton,
            'options' => $options,
            'button' => [
                'label' => $buttonLabel,
                'url' => $buttonUrl,
                'options' => $buttonOptions,
            ],
        ]);
    }

    protected static function getDefaultAlertIconPath(string $type): string
    {
        switch ($type) {
            case self::ALERT_TYPE_INFO:
                $iconPath = 'ic-info-24.svg';
                break;
            case self::ALERT_TYPE_WARNING:
                $iconPath = 'ic-warning-24.svg';
                break;
            case self::ALERT_TYPE_SUCCESS:
                $iconPath = 'ic-check-circle-24.svg';
                break;
            case self::ALERT_TYPE_CRITICAL:
                $iconPath = 'ic-error-24.svg';
                break;
            default:
                return '';
        }

        return "@frontend/widgets/dactylkit/components/alert/assets/img/{$iconPath}";
    }

    const BADGE_DEFAULT = 'dk--badge--default';
    const BADGE_INFORMATIONAL = 'dk--badge--informational';
    const BADGE_SUCCESS = 'dk--badge--success';
    const BADGE_WARNING = 'dk--badge--warning';

    /**
     * @param string $content
     * @param string $type use BADGE_* constants
     * @param string $tag
     *
     * @return string
     */
    public static function badge($content = '', $type = self::BADGE_DEFAULT, $tag = 'span') {
        return static::render('badge/main', [
            'content' => $content,
            'type' => $type,
            'tag' => $tag
        ]);
    }

    const TOOLTIP_TOP = 'top';
    const TOOLTIP_BOTTOM = 'bottom';
    const TOOLTIP_LEFT = 'left';
    const TOOLTIP_RIGHT = 'right';

    /**
     * Renders tooltip HTML options into element tag
     * @param $tooltip - tooltip text
     * @param string $placement - top, bottom, left, right
     *
     * @return string
     */
    public static function tooltip($tooltip , $placement = self::TOOLTIP_TOP) {
        return <<<HTML
            data-toggle="tooltip" data-placement="{$placement}" data-original-title="{$tooltip}"
        HTML;
    }

    const CHIP_TYPE_DISABLED = 'dk--chip--disabled';
    const CHIP_SIZE_TALL = 'dk--chip--tall';

    /**
     * @param string $label
     * @param string $type
     * @param string $size
     * @param string $iconLeftPath
     * @param string $iconRightPath
     * @param array $options
     *
     * @return string
     */
    public static function chip(
        string $label,
        string $type = '',
        string $size = '',
        string $iconLeftPath = '',
        string $iconRightPath = '',
        array $options = []
    ): string {
        return static::render('chip/main', [
            'label' => $label,
            'type' => $type,
            'size' => $size,
            'iconLeftPath' => $iconLeftPath,
            'iconRightPath' => $iconRightPath,
            'options' => $options
        ]);
    }

    /**
     * @param $step - current step
     * @param $steps - key-value array (index, label)
     *
     * @return string
     */
    public static function progressBar(int $step, array $steps) {
        return static::render('progress_bar/main', [
           'step' => $step,
           'steps' => $steps
        ]);
    }

}