<?php
/*
* Copyright 2023 DactylGroup s.r.o.
*
* Licensed under the EUPL, Version 1.2 or – as soon they
will be approved by the European Commission - subsequent
versions of the EUPL (the "Licence");
* You may not use this work except in compliance with the
Licence.
* You may obtain a copy of the Licence at:
*
* https://joinup.ec.europa.eu/software/page/eupl5
*
* Unless required by applicable law or agreed to in
writing, software distributed under the Licence is
distributed on an "AS IS" basis,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
express or implied.
* See the Licence for the specific language governing
permissions and limitations under the Licence.
*/
?>


<?php

namespace frontend\widgets;

use artkost\yii2\trumbowyg\Trumbowyg;
use dactylcore\core\widgets\admin\tinymce\TinyMce;
use dactylcore\core\widgets\common\select2\Select2;

class ActiveField extends \dactylcore\core\widgets\common\activeform\ActiveField
{
    /**
     * @inheritDoc
     */
    public $template = "{input}\n{label}\n{hint}\n{error}";

    /**
     * @inheritDoc
     */
    public $inputOptions = [
        'placeholder' => ' ', // placeholder is required so :placeholder-shown pseudo class can be styled
    ];

    /**
     * @inheritDoc
     */
    public function begin()
    {
        // add class to form group if label is disabled
        if (!isset($this->parts['{label}']) || $this->parts['{label}'] === '') {
            $this->options['class'] .= ' form-group--no-label';
        }

        return parent::begin();
    }

    public function widget($class, $config = [])
    {
        // add empty css class if there is no selected value
        if (is_a($class, Select2::class, true)) {
            // if attribute starts with "[_]" prefix removes it
            $attributeNameModel = preg_replace("/^\[[a-zA-Z]\]\s?/", "", $this->attribute);
            $value = $this->model->{$attributeNameModel};
            $data = $config['data'] ?? [];
            if (!array_key_exists($value, $data)) {
                $config['options']['class'] = $config['options']['class'] ?? '';
                $config['options']['class'] .= "select2--empty";
            }
        }

        return parent::widget($class, $config);
    }

    public function checkbox($options = [], $enclosedByLabel = null)
    {
        $options['template'] = '{beginLabel}{input}<span class="input-icon"></span><span class="checkbox-label">{labelTitle}{error}</span>{endLabel}';
        return parent::getToggleField(self::TYPE_CHECKBOX, $options, true);
    }

    public function switchButton($options = [], $enclosedByLabel = null) {
        $options['template'] = '{beginLabel}{input}<span class="slider round"></span><span class="slider__label">{labelTitle}{error}</span>{endLabel}';
        return parent::getToggleField(self::TYPE_CHECKBOX, $options, true);
    }

    public function tinyMce($config = [])
    {
//        $this->widget(TinyMce::class, $config);
        $this->widget(Trumbowyg::class, $config);

        return $this;
    }

}