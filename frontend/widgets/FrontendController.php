<?php
/*
* Copyright 2023 DactylGroup s.r.o.
*
* Licensed under the EUPL, Version 1.2 or – as soon they
will be approved by the European Commission - subsequent
versions of the EUPL (the "Licence");
* You may not use this work except in compliance with the
Licence.
* You may obtain a copy of the Licence at:
*
* https://joinup.ec.europa.eu/software/page/eupl5
*
* Unless required by applicable law or agreed to in
writing, software distributed under the Licence is
distributed on an "AS IS" basis,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
express or implied.
* See the Licence for the specific language governing
permissions and limitations under the Licence.
*/
?>


<?php

namespace frontend\widgets;

use dactylcore\core\web\Response;
use Yii;

class FrontendController extends \dactylcore\core\web\FrontendController
{
    /**
     * Checks if request is ActiveForm validation.
     *
     * @return bool
     * @see $ajaxParam and $enableAjaxValidation in ActiveForm
     */
    public function isAjaxActiveFormValidation(): bool
    {
        return Yii::$app->request->isAjax && Yii::$app->request->post('ajax') !== null;
    }

    /**
     * Validates model and returns errors.
     *
     * @param $model
     *
     * @return array errors
     */
    public function performAjaxActiveFormValidation($model): array
    {
        Yii::$app->response->format = Response::FORMAT_JSON;
        return ActiveForm::validate($model);
    }

}
