<?php
/*
* Copyright 2023 DactylGroup s.r.o.
*
* Licensed under the EUPL, Version 1.2 or – as soon they
will be approved by the European Commission - subsequent
versions of the EUPL (the "Licence");
* You may not use this work except in compliance with the
Licence.
* You may obtain a copy of the Licence at:
*
* https://joinup.ec.europa.eu/software/page/eupl5
*
* Unless required by applicable law or agreed to in
writing, software distributed under the Licence is
distributed on an "AS IS" basis,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
express or implied.
* See the Licence for the specific language governing
permissions and limitations under the Licence.
*/
?>


<?php

namespace frontend\widgets;

class ActiveForm extends \dactylcore\core\widgets\frontend\activeform\ActiveForm
{
    public $fieldClass = ActiveField::class;

    public function init()
    {
        if ($this->enableAjaxValidation) {
            $this->disableBrowserValidation();
        }

        parent::init();
    }

    /**
     * Adds novalidate tag to form element which disables browser validation, like special input or min, max.
     * All data will be validated only on server.
     * @see https://www.w3schools.com/tags/att_form_novalidate.asp
     */
    public function disableBrowserValidation()
    {
        $this->options['novalidate'] = 'novalidate';
    }
}