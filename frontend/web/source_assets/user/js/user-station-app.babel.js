/*
* Copyright 2023 DactylGroup s.r.o.
*
* Licensed under the EUPL, Version 1.2 or – as soon they
will be approved by the European Commission - subsequent
versions of the EUPL (the "Licence");
* You may not use this work except in compliance with the
Licence.
* You may obtain a copy of the Licence at:
*
* https://joinup.ec.europa.eu/software/page/eupl5
*
* Unless required by applicable law or agreed to in
writing, software distributed under the Licence is
distributed on an "AS IS" basis,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
express or implied.
* See the Licence for the specific language governing
permissions and limitations under the Licence.
*/


function initMap() {
    let app = new Vue({
        map: undefined,
        el: '#vueappprofile',
        data() {
            return {
                labelOneStation: '',
                labelTwoFourStation: '',
                labelFiveStation: '',

                isLoading: true,
                linkPattern: '',
                selectedIds: [], // coming from server and will be send to server WITH + added - removed
                selectedIdsStepBefore: [],  // keeps selectedIds before the step of clicking on the checkbox in order to say what was removed and what was added
                added: [],  // ids added in this page
                removed: [], // ids added in this page
                prolongLinkPattern: '',
                searchTerm: '',
                columns: [
                    {
                        label: 'id',
                        field: 'id_orig',
                    },
                    {
                        label: 'type',
                        field: 'type',
                    },
                    {
                        label: 'name',
                        field: 'name',
                    },
                    {
                        label: 'status',
                        field: 'status',
                    },
                ],
                rows: [],
                totalRecords: 10,
                serverParams: {
                    columnFilters: {},
                    selectedIds: [],
                    sort: {
                        field: '',
                        type: '',
                    },
                    search: '',
                    myOnly: true,
                    sortByExpiration: false,
                    types: [],
                    bounds: {
                        east: '',
                        west: '',
                        north: '',
                        south: '',
                    },
                    statuses: [],
                    page: 1,
                    perPage: 100
                }
            };
        },
        computed: {
            selectedIdsTitle: function () {
                let out;
                const count = this.selectedIds.length;
                switch (true) {
                    case count === 1: {
                        out = this.labelOneStation;
                        break;
                    }
                    case count >= 2 && count <= 4: {
                        out = this.labelTwoFourStation.replace('{number}', count);
                        break;
                    }
                    case count >= 5: {
                        out = this.labelFiveStation.replace('{number}', count);
                        break;
                    }
                }

                return out;
            }
        },


        methods: {
            prolongSelectedStations() {
                axios.post('/api/v1/station/prolong',
                    {
                        stationsToProlong: this.selectedIds,
                    },
                    {
                        headers: {
                            'lang': langIso
                        }
                    }).then((response) => {
                    this.selectedIds = [];
                    this.loadItems();
                    getAllFlashAlertMessages();
                });
            },
            flyTo(lng, lat) {
                const currentZoom = this.map.mapObject.getZoom();
                const zoom = currentZoom > 10 ? currentZoom : 10;
                this.map.mapObject.flyTo({
                    center: [
                        parseFloat(lng),
                        parseFloat(lat)
                    ],
                    zoom: zoom,
                    essential: true // this animation is considered essential with respect to prefers-reduced-motion
                });

            },
            getLink(id, editFinished) {
                let out = this.linkPattern.replace(0, id);
                if (editFinished) {
                    out = out + '?editFinished=true';
                }
                return out;
            },
            getProlongLink(id, editFinished) {
                return this.prolongLinkPattern.replace(0, id);
            },
            unpublishStation(id) {
                const that = this;

                bootbox.confirm({
                    message: $('#map').data('confirmation-unpublish-text'),
                    closeButton: false,
                    buttons: {
                        confirm: {
                            label: $('#map').data('yes'),
                            className: 'dk--btn dk--btn--primary',
                        },
                        cancel: {
                            label: $('#map').data('no'),
                            className: 'dk--btn dk--btn--secondary',
                        }
                    },
                    callback: function(result) {
                        if (result) {
                            $.ajax({
                                method: 'post',
                                url: "/api/v1/station/unpublish-station",
                                data: {
                                    id: id
                                }
                            }).done(function (result) {
                                if (result.data === true) {
                                    that.loadItems();
                                    getAllFlashAlertMessages();
                                }
                            });
                        }
                    }
                });
            },
            deleteStation(id) {
                const that = this;

                bootbox.confirm({
                    message: $('#map').data('confirmation-delete-text'),
                    closeButton: false,
                    buttons: {
                        confirm: {
                            label: $('#map').data('yes'),
                            className: 'dk--btn dk--btn--primary',
                        },
                        cancel: {
                            label: $('#map').data('no'),
                            className: 'dk--btn dk--btn--secondary',
                        }
                    },
                    callback: function(result) {
                        if (result) {
                            $.ajax({
                                method: 'post',
                                url: "/api/v1/station/delete-station",
                                data: {
                                    id: id
                                }
                            }).done(function (result) {
                                if (result.data === true) {
                                    that.loadItems();
                                    getAllFlashAlertMessages();
                                }
                            });
                        }
                    }
                });
            },
            updateParams(newProps) {
                this.serverParams = Object.assign({}, this.serverParams, newProps);
                this.loadItems();
            },

            onRowMouseEnter(params) {

                if (this.map.mapObject.getZoom() >= 8) {
                    this.map.mapObject.setFeatureState(
                        {source: 'stations', id: params.row.id_orig},
                        {highlight: true}
                    );
                }
            },

            onRowMouseLeave(params) {
                if (this.map.mapObject.getZoom() >= 8) {

                    this.map.mapObject.setFeatureState(
                        {source: 'stations', id: params.row.id_orig},
                        {highlight: false}
                    );
                }

            },

            onPageChange(params) {
                this.updateParams({page: params.currentPage});
            },

            onPerPageChange(params) {

                this.updateParams({perPage: params.currentPerPage});
            },

            onSortChange(params) {
                this.updateParams({
                    sort: {
                        type: params[0].type,
                        field: params[0].field,
                    },
                });
            },

            onColumnFilter(params) {
            },

            onSearch(params) {
                this.updateParams({search: params.searchTerm});
                this.loadItems();
            },

            diffArray(from, to) {
                return from.concat(arr2).filter(function (val) {
                    if (!(arr1.includes(val) && arr2.includes(val)))
                        return val;
                });
            },

            selectionChanged(event, b) {
                let current = event.selectedRows.map(item => item.id_orig);
                let removed = _.difference(this.selectedIdsStepBefore, current);
                let added = _.difference(current, this.selectedIdsStepBefore);

                if (removed.length > 0) {
                    // removing
                    let valueToRemove = removed.pop();
                    this.removed.push(valueToRemove);
                    this.added = _.without(this.added, valueToRemove);

                    this.selectedIdsStepBefore = _.without(this.selectedIdsStepBefore, valueToRemove);
                } else if (added.length > 0) {
                    // removing
                    let valueToAdd = added.pop();

                    this.added.push(valueToAdd);
                    this.removed = _.without(this.removed, valueToAdd);

                    this.selectedIdsStepBefore.push(valueToAdd);
                }
                // adding "add"
                this.selectedIdsStepBefore = this.selectedIdsStepBefore.concat(added);
                this.selectedIds = this.selectedIds.concat(this.added);
                this.selectedIds = this.selectedIds.filter((el) => !this.removed.includes(el));
                this.selectedIds = _.unique(this.selectedIds);
            },


            // load items is what brings back the rows from server
            loadItems() {
                this.serverParams.selectedIds = this.selectedIds;
                if (typeof idUserForData !== 'undefined') {
                    this.serverParams.idUserForData = idUserForData;
                }

                axios.post('/api/v1/station/data', this.serverParams, {
                    headers: {
                        'lang': langIso
                    }
                }).then((response) => {
                    this.rows = response.data.data.rows;
                    this.totalRecords = response.data.data.total;
                    this.selectedIds = response.data.data.selectedIds;
                    this.removed = [];
                    this.added = [];
                    this.selectedIdsStepBefore = [];
                });
            }
        },

        created: function () {
            this.linkPattern = $("#map").data('stationlink');
            this.prolongLinkPattern = $("#map").data('stationlinkprolong');
            this.loadItems();
            this.map = new UserMap();
            this.map.runMap('map');
            this.map.vm = this;

            this.labelFiveStation = $("#fiveStations").text();
            this.labelTwoFourStation = $("#twoFourStations").text();
            this.labelOneStation = $("#oneStation").text();

        }
    });

    return app;
}

function initHeader(mainApp) {
    let app = new Vue({
        map: undefined,
        mainApp: undefined,
        el: '#vueAppHeader',
        data() {
            return {
                showList: true,
                statuses: [],
                types: [],
                checkedTypes: [],
                checkedStatuses: [],

                sortByExpiration: false,

                searchTerm: '',
            }
        },
        computed: {
            checkedTypesLabels: function () {
                return this.checkedTypes.map(type => type.label).join(', ');
            },
            checkedStatusesLabels: function () {
                return this.checkedStatuses.map(status => status.label).join(', ');
            },
        },
        watch: {
            searchTerm: function (val) {
                this.mainApp.$data.searchTerm = val;
            },
            checkedTypes: function (val) {
                this.mainApp.updateParams({types: this.checkedTypes.map(type => type.id)});
            },
            checkedStatuses: function (val) {
                this.mainApp.updateParams({statuses: this.checkedStatuses.map(status => status.id)});
            },
            showList: function (val) {
                if (val) {
                    // divide the map to two parts
                    $("#footer").show();
                    $(".left-container").show();
                    $(".right-container").removeClass('col-md-12').addClass('col-md-6');
                    $("#map").removeClass('full-width').addClass('half-width');

                    this.mainApp.map.mapObject.resize();

                } else {
                    // show fullscreen map
                    $("#footer").hide();
                    $(".left-container").hide();
                    $(".right-container").removeClass('col-md-6').addClass('col-md-12');
                    $("#map").removeClass('half-width').addClass('full-width');

                    this.mainApp.map.mapObject.resize();
                }
            },
            sortByExpiration: function (val) {
                this.mainApp.updateParams({sortByExpiration: val, page: 1});
            }
        },
        methods: {
            setFilters: function () {
                axios.get('/api/v1/station/filters', {
                    headers: {
                        'lang': langIso
                    }
                }).then(response => {
                    this.statuses = response.data.data.statuses;
                    this.types = response.data.data.types;
                    this.status_headline = response.data.data.status_headline;
                    this.type_headline = response.data.data.type_headline;
                });
            },
            clearStatuses: function (target) {
                this.checkedStatuses = [];
            },
            clearTypes: function (target) {
                this.checkedTypes = [];
            },
            closeDropdown: function (closeTarget) {
                $('#' + closeTarget).dropdown('toggle')
            }
        }
        ,
        created: function () {
            this.mainApp = mainApp;
            this.setFilters();
            this.mainApp.map.mapObject.resize();
        }
    });
}

let mainApp = initMap();
initHeader(mainApp);