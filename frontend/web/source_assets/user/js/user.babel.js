/*
* Copyright 2023 DactylGroup s.r.o.
*
* Licensed under the EUPL, Version 1.2 or – as soon they
will be approved by the European Commission - subsequent
versions of the EUPL (the "Licence");
* You may not use this work except in compliance with the
Licence.
* You may obtain a copy of the Licence at:
*
* https://joinup.ec.europa.eu/software/page/eupl5
*
* Unless required by applicable law or agreed to in
writing, software distributed under the Licence is
distributed on an "AS IS" basis,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
express or implied.
* See the Licence for the specific language governing
permissions and limitations under the Licence.
*/


/**
 * VARIABLES
 */

var fadeSpeed = 350;


/**
 * BINDS
 */

// Select type of registered subject
$(document).on('click', '.register-forms .dk--chip', function () {
    var lastSelected = $('.register-forms .dk--chip.selected').data('form');
    var formType = $(this).data('form');

    if (formType !== lastSelected) {
        // Select the clecked one
        $('.register-forms .dk--chip').removeClass('selected');
        $(this).addClass('selected');

        // Add value to type of form
        $('#hidden-type').val(formType.substr(0, formType.indexOf('_')));

        // Fade adnimation between forms
        $('.livebox-user__content__forms').fadeOut(fadeSpeed, function () {
            $('.livebox-user__content__forms .register_inner_form').hide();
            $('.' + formType).show();
            $('.livebox-user__content__forms').fadeIn(fadeSpeed);
        });
        rescaleCaptcha();
    }
});

// Import input changing
$(document).on('change', '#importfileuploadform-importfile', function () {
    var label = $('.field-importfileuploadform-importfile label');
    label.text($('#importfileuploadform-importfile').val().replace(/^.*[\\\/]/, ''));
    label.addClass('file-selected');
});

// Change password visibility
$(document).on('click', '.toggle-password', function () {
    var input = $($(this).attr("toggle"));
    if (input.attr("type") === "password") {
        input.attr("type", "text");
    } else {
        input.attr("type", "password");
    }
});

function rescaleCaptcha(){
    var width = $('.g-recaptcha').parent().width();
    var scale;
    if (width < 360) {
        scale = width / 302;
    } else{
        scale = 1.19;
    }

    $('.g-recaptcha').css('transform', 'scale(' + scale + ')');
    $('.g-recaptcha').css('-webkit-transform', 'scale(' + scale + ')');
    $('.g-recaptcha').css('transform-origin', '0 0');
    $('.g-recaptcha').css('-webkit-transform-origin', '0 0');
}

$(document).ready(function(){
    rescaleCaptcha();
    $( window ).resize(function() { rescaleCaptcha(); });
});
