/*
* Copyright 2023 DactylGroup s.r.o.
*
* Licensed under the EUPL, Version 1.2 or – as soon they
will be approved by the European Commission - subsequent
versions of the EUPL (the "Licence");
* You may not use this work except in compliance with the
Licence.
* You may obtain a copy of the Licence at:
*
* https://joinup.ec.europa.eu/software/page/eupl5
*
* Unless required by applicable law or agreed to in
writing, software distributed under the Licence is
distributed on an "AS IS" basis,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
express or implied.
* See the Licence for the specific language governing
permissions and limitations under the Licence.
*/


/**
 * Main JS file for turf map pack.
 * Docu: https://turfjs.org/getting-started/
 *
 * 1. Install packages of need with npm : `package.json`
 * 2. Add package to thist file `module.exports`
 * 3. Bundle turf.js with browserify: `browserify frontend/web/source_assets/turf/main.js -s turf > frontend/web/source_assets/turf/turf.js`
 *    3.1  If "browserify" is not installed, do it manually with: `npm install -g browserify`
 */


module.exports = {
    helpers: require('@turf/helpers'),
    destination: require('@turf/destination'),
    rhumbBearing: require('@turf/rhumb-bearing'),
    distance: require('@turf/distance'),
    transformRotate: require('@turf/transform-rotate'),
    transformTranslate: require('@turf/transform-translate'),
    meta: require('@turf/meta')
};