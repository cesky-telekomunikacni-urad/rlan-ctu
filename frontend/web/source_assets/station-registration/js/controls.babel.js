/*
* Copyright 2023 DactylGroup s.r.o.
*
* Licensed under the EUPL, Version 1.2 or – as soon they
will be approved by the European Commission - subsequent
versions of the EUPL (the "Licence");
* You may not use this work except in compliance with the
Licence.
* You may obtain a copy of the Licence at:
*
* https://joinup.ec.europa.eu/software/page/eupl5
*
* Unless required by applicable law or agreed to in
writing, software distributed under the Licence is
distributed on an "AS IS" basis,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
express or implied.
* See the Licence for the specific language governing
permissions and limitations under the Licence.
*/


var lat = '';
var lng = '';
var needsToReload = false;

function handleLocationConfirm() {
    if (lat !== '' && lng !== '') {
        $('#confirm-location').prop('disabled', false);
    } else {
        $('#confirm-location').prop('disabled', true);
    }
}

/* FS station step4 hardware switches */
function switchHardwareFs(value, selector) {
    let cssSelectorSN = '.field-modelStation' + selector + '_sn-input';
    let cssSelectorMAC = '.field-modelStation' + selector + '_mac-input';

    if (value === "0") {
        $(cssSelectorSN).hide().find("input");
        $(cssSelectorMAC).show().find("input");
    }
    if (value === "1") {
        $(cssSelectorSN).show().find("input");
        $(cssSelectorMAC).hide().find("input");
    }
}

/* Wigig station step4 hardware switches */
function switchHardwareWigig(value) {
    if (value === "0") {
        $('.field-station-serialnumber').hide().find("input");
        $('.field-station-macaddress').show().find("input");
    }
    if (value === "1") {
        $('.field-station-macaddress').hide();
        $('.field-station-serialnumber').show();
    }
}

function setEirpVal() {
    let eirpAutoValue = parseInt($('#station-antenna_volume').val()) + parseInt($('#station-power').val());
    if (isNaN(eirpAutoValue))
        eirpAutoValue = '';
    $('#stationwigig-eirp').val(eirpAutoValue);
}

function isEirpEnabled() {
    if ($('.eirp_method_radio').find('input:checked').val() === "auto") {
        $('#stationwigig-eirp').prop('disabled', true);
        $('#station-power').prop('disabled', false);
        $('#station-antenna_volume').prop('disabled', false);
        setEirpVal();
    } else {
        $('#stationwigig-eirp').prop('disabled', false);
        $('#station-power').prop('disabled', true);
        $('#station-power').val(null);
        $('#station-antenna_volume').prop('disabled', true);
        $('#station-antenna_volume').val(null);
    }
}

$(document).on('pjax:complete', function (event, xhr) {
    // Skip getting flash messages for redirects, so they are displayed after the redirect
    if (![301, 302].includes(parseInt(xhr.status))) {
        getAllFlashAlertMessages();
    }
});

$(document).ajaxComplete(function () {

    /* wigig step4_parameters */
    $('.eirp_method_radio').find('input').change(function () {
        isEirpEnabled();
    });
    $('#station-antenna_volume, #station-power').keyup(function () {
        if ($('input[name="StationWigig[eirp_method]"]:checked').val() === "auto") {
            setEirpVal();
        }
    });
    isEirpEnabled();

    $('.hardware_identifier_input').find('input').change(function () {
        switchHardwareWigig(this.value);
    });
    switchHardwareWigig($('.hardware_identifier_input').find('input:checked').val());

    /* fs step1_name */
    $('button.loading-button').click(function () {
        var name = $('.get-mirror-name').val();
        if (name) {
            $('.mirror-name').val(name);
        }
    });

    /* fs step4_parameters */
    $('.hardware_identifier_radio_a').find('input').change(function () {
        switchHardwareFs(this.value, 'A');
    });
    $('.hardware_identifier_radio_b').find('input').change(function () {
        switchHardwareFs(this.value, 'B');
    });
    switchHardwareFs($('.hardware_identifier_radio_a').find('input:checked').val(), 'A');
    switchHardwareFs($('.hardware_identifier_radio_b').find('input:checked').val(), 'B');

    /* input masks */
    $("input[name*='macAddress']").mask('AA:AA:AA:AA:AA:AA', {
        'translation': {
            A: {pattern: /[A-Za-z0-9]/}
        },
    });

    /* tooltips */
    $('[data-toggle="tooltip"]').tooltip();
    $('.tooltip-selector').tooltip();

    $('#station-antenna_volume').keyup(function () {
        $('#stationwigig-is_ptmp').val($(this).val() > $('#stationwigig-is_ptmp').data('ptmp-treshold') ? 0 : 1);
        // $('form').dirtyFborms('rescan');
    });

});

$(".checkbox-menu").on("change", "input[type='checkbox']", function () {
    $(this).closest("li").toggleClass("active", this.checked);
});

$(document).on('click', '.allow-focus', function (e) {
    e.stopPropagation();
});

$(document).on('keyup change blur', "#station-form input[name*='macAddress']", function () {
    var macFormatRegex = /^([0-9A-F]{2}[:-]){5}([0-9A-F]{2})$/;
    var $el = $(this);
    var value = $el.val().trim().toUpperCase();
    var $form = $el.closest('form');
    var removeWarningFunc = function () {
        $el.closest('.form-group').removeClass('has-warning').find('.help-block').html('');
    };

    if (macFormatRegex.test(value)) {
        $.post(URL__STATION__IS_MAC_ADDRESS_UNIQUE, {id: $form.data('id'), value: value}, function (r) {
            if (r.answer === false) {
                $el.closest('.form-group').addClass('has-error').find('.help-block').html(r.message);
            } else {
                removeWarningFunc();
            }
        });
    } else {
        removeWarningFunc();
    }
});

$(document).on('keyup change blur', "#station-form input[name*='serialNumber']", function () {
    var $el = $(this);
    var value = $el.val().trim().toUpperCase();
    var $form = $el.closest('form');
    var removeWarningFunc = function () {
        $el.closest('.form-group').removeClass('has-warning').find('.help-block').html('');
    };

    if (!(/^\w+$/.test(value) === false || value.length>12)) {
        $.post(URL__STATION__IS_SERIAL_NUMBER_UNIQUE, {id: $form.data('id'), value: value}, function (r) {
            if (r.answer === false) {
                $el.closest('.form-group').addClass('has-warning').find('.help-block').html(SERIAL_DUPLICITY_WARNING);
            } else {
                removeWarningFunc();
            }
        });
    } else {
        removeWarningFunc();
    }
});