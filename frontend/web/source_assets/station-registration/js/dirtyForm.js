/*
* Copyright 2023 DactylGroup s.r.o.
*
* Licensed under the EUPL, Version 1.2 or – as soon they
will be approved by the European Commission - subsequent
versions of the EUPL (the "Licence");
* You may not use this work except in compliance with the
Licence.
* You may obtain a copy of the Licence at:
*
* https://joinup.ec.europa.eu/software/page/eupl5
*
* Unless required by applicable law or agreed to in
writing, software distributed under the Licence is
distributed on an "AS IS" basis,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
express or implied.
* See the Licence for the specific language governing
permissions and limitations under the Licence.
*/


var activateDangerousMode = false;
function initDirtyForms(dangerousMode) {
    activateDangerousMode = dangerousMode;
    $('form#station-form').dirtyForms({ignoreSelector: '.ignoreDirty'});

    $('form#station-form').bind('dirty.dirtyforms', function () {
        var dirtyAndDangerous = false;
        $('input.dirty').each(function (index, item) {
            if (!dirtyAndDangerous && $(item).hasClass('dangerous')) {
                dirtyAndDangerous = true;
                return true;
            }
        });

        if (dirtyAndDangerous && activateDangerousMode) {
            // showing button "continue and recount"
            $('.next-step-dirty-button').removeClass('next-step-hidden-button');

            $('.next-step-save-button').addClass('next-step-hidden-button');
            $('.next-step-button').addClass('next-step-hidden-button');
        } else {
            // showing button "save"
            $('.next-step-dirty-button').addClass('next-step-hidden-button');
            $('.next-step-button').addClass('next-step-hidden-button');

            $('.next-step-save-button').removeClass('next-step-hidden-button');
        }
    });

    $('form#station-form').bind('clean.dirtyforms', function () {
        // showing button "continue"
        $('.next-step-dirty-button').addClass('next-step-hidden-button');
        $('.next-step-save-button').addClass('next-step-hidden-button');

        $('.next-step-button').removeClass('next-step-hidden-button');
    });

    $('.next-step-dirty-button').click(function (e) {
        e.preventDefault();
        bootbox.confirm({
            message: $('.next-step-dirty-button').data('message-text'),
            closeButton: false,
            buttons: {
                confirm: {
                    label: $('.next-step-dirty-button').data('confirm-text'),
                    className: 'dk--btn dk--btn--primary',
                },
                cancel: {
                    label: $('.next-step-dirty-button').data('cancel-text'),
                    className: 'dk--btn dk--btn--secondary',
                }
            },
            callback: function (result) {
                if (result) {
                    $('form#station-form').submit();
                }
            }
        });
    });
}


