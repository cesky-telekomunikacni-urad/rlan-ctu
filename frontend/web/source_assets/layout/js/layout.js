/*
* Copyright 2023 DactylGroup s.r.o.
*
* Licensed under the EUPL, Version 1.2 or – as soon they
will be approved by the European Commission - subsequent
versions of the EUPL (the "Licence");
* You may not use this work except in compliance with the
Licence.
* You may obtain a copy of the Licence at:
*
* https://joinup.ec.europa.eu/software/page/eupl5
*
* Unless required by applicable law or agreed to in
writing, software distributed under the Licence is
distributed on an "AS IS" basis,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
express or implied.
* See the Licence for the specific language governing
permissions and limitations under the Licence.
*/


var urlBeforeLiveBox;

/**
 * Updates summary for ListView with ScrollPager (infinite scroll).
 * Source: https://github.com/kop/yii2-scroll-pager/issues/19
 * @param res content passed from eventOnLoaded event
 */
function updateListViewSummary(res) {
    var startNumber = $('.summary').text();
    startNumber = startNumber.split(' ');
    startNumber = startNumber[1].split('-');
    startNumber = startNumber[0];
    var query = ".summary:eq(0)";
    var forgedDom = $($.parseHTML(res));
    var newSummary = forgedDom.find(query);
    var newSummaryText = newSummary.text();
    newSummaryText = newSummaryText.split(' ');
    var endNumberArr = newSummaryText[1].split('-');
    endNumber = endNumberArr[1];
    //Restich the text again
    newSummaryText[1] = '' + startNumber + '-' + endNumber + '';
    newSummaryText[3] = '' + newSummaryText[3] + '';
    newSummary.html(newSummaryText.join(' '));
    $('body').find(query).html(newSummary);
}

function initLivebox() {
    LiveBox.init({
        closeViaBackdrop: false,
        loadingOptions: {
            'data-color': '#003782'
        },
        afterShown: function () {
            initBootstrapComponents();
        },
        ajaxBeforeSend: function (xhr, settings) {
            urlBeforeLiveBox = window.location.pathname;

            xhr.setRequestHeader('X-Registered-Bundles', REGISTERED_BUNDLES);

            if (this.customParams.close) {
                xhr.setRequestHeader('X-LIVEBOX-Close', true);
            }
        },
        ajaxError: function (xhr, status, error) {
            this._content = errorPageLayout;
        },
        ajaxSuccess: function (result, status, xhr) {

            var content = $('<div>' + this._content + '</div>');

            var scripts = content.find('script[src]');
            var csss = content.find('link[href]');

            $('body').append(scripts);
            $('head').append(csss);

            content = $(content.html()).not(scripts).not(csss);
            this._content = content;
        },
        getSpinnerMarkup: function () {
            return '<div class="loader"><svg class="circular" viewBox="25 25 50 50"><circle class="path" cx="50" cy="50" r="20" fill="none" stroke-width="2" stroke-miterlimit="10"/> </svg></div>'
        },
        afterClose: function () {
            if (needsToReload) {
                $("#shouldReload").fadeIn();
                if (typeof mainApp !== 'undefined' && mainApp !== null) {
                    mainApp.loadItems();
                }
            }

            if (urlBeforeLiveBox && history.pushState) {
                history.pushState(null, null, urlBeforeLiveBox); // URL is now /inbox/N
            }
        },
    });
}

$(document).on('click', '.dk-alert__close', function () {
    $(this).parents('.dk-alert').slideUp();
});

$(document).on('change', '.select2-hidden-accessible', function () {
    var element = $(this);
    if (element.val() === '') {
        element.addClass('select2--empty');
    } else {
        element.removeClass('select2--empty');
    }
});

// Refresh input mask after pjax
$(document).ajaxComplete(function () {
    $('.pjax').find('input[data-mask]').each(function (key, input) {
        $(input).mask(input.dataset.mask);
    });
});

// Select2 search clear handler
$(document).on('click', '.select2-search__clear', function () {
    $(this).parent().find('.select2-search__field').val('').trigger('input');
    $(this).remove();
});

// Select2 search input handler
$(document).on('keyup', '.select2-search__field', function () {
    var input = $(this);
    var icon = input.parent().find('.select2-search__clear');

    if (input.val() === '') {
        if (icon.length !== 0) {
            icon.remove();
        }
    } else {
        if (icon.length === 0) {
            input.parent().append('<span class="select2-search__clear"></span>');
        }
    }
});

function setCookie(name, value, days) {
    var expires = "";
    if (days) {
        var date = new Date();
        date.setTime(date.getTime() + (days * 24 * 60 * 60 * 1000));
        expires = "; expires=" + date.toUTCString();
    }
    document.cookie = name + "=" + (value || "") + expires + "; path=/";
}

function getCookie(name) {
    var nameEQ = name + "=";
    var ca = document.cookie.split(';');
    for (var i = 0; i < ca.length; i++) {
        var c = ca[i];
        while (c.charAt(0) == ' ') c = c.substring(1, c.length);
        if (c.indexOf(nameEQ) == 0) return c.substring(nameEQ.length, c.length);
    }
    return null;
}

function eraseCookie(name) {
    document.cookie = name + '=; Path=/; Expires=Thu, 01 Jan 1970 00:00:01 GMT;';
}

function getNotificationsCookieAsArray() {
    var notifs = getCookie('seen_notifications');
    if (notifs) {
        notifs = JSON.parse(notifs);
    } else {
        notifs = [];
    }

    return notifs;
}

function repositionNotifications() {
    $('#header-offset').css({paddingBottom: $('header').height()});
}

// Open menu-overlay
$(document).ready(function () {

    initLivebox();
    repositionNotifications();

    $('#dk--menu-overlay__button').click(function () {
        $('#dk--menu-overlay').fadeToggle(200);
    });

    $('#lang-select').change(function () {
        window.location.replace($(this).val());
    });


    $(document).on('click', '#notifications .notification .close', function () {
        var nid = $(this).closest('.notification').data('id');
        var notifs = getNotificationsCookieAsArray();
        notifs.push(nid);
        // eraseCookie('seen_notifications');
        setCookie('seen_notifications', JSON.stringify(notifs), 365);
        // console.log('seen_notifications', JSON.stringify(notifs), 365);

        $(this).closest('.notification').slideUp(function(){
            $(this).remove();

            if ($('#notifications .notification').length === 0) {
                $('#notifications').remove();
            }

            repositionNotifications();
        });


    });
})