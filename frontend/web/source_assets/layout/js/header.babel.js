/*
* Copyright 2023 DactylGroup s.r.o.
*
* Licensed under the EUPL, Version 1.2 or – as soon they
will be approved by the European Commission - subsequent
versions of the EUPL (the "Licence");
* You may not use this work except in compliance with the
Licence.
* You may obtain a copy of the Licence at:
*
* https://joinup.ec.europa.eu/software/page/eupl5
*
* Unless required by applicable law or agreed to in
writing, software distributed under the Licence is
distributed on an "AS IS" basis,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
express or implied.
* See the Licence for the specific language governing
permissions and limitations under the Licence.
*/


$(document).ready(function() {
    //HAMBURGER
    $("#menu-hamburger").on('click', function () {
        window.scrollTo(0, 0);
        $(".dc-menu-hamburger span").toggleClass("active");
        $("html").toggleClass("header-menu-disabled-scroll");
        $(".dc-menu").toggleClass("active");
        $(".menu-hamburger").toggleClass("active");
    });
});