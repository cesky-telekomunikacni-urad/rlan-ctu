/*
* Copyright 2023 DactylGroup s.r.o.
*
* Licensed under the EUPL, Version 1.2 or – as soon they
will be approved by the European Commission - subsequent
versions of the EUPL (the "Licence");
* You may not use this work except in compliance with the
Licence.
* You may obtain a copy of the Licence at:
*
* https://joinup.ec.europa.eu/software/page/eupl5
*
* Unless required by applicable law or agreed to in
writing, software distributed under the Licence is
distributed on an "AS IS" basis,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
express or implied.
* See the Licence for the specific language governing
permissions and limitations under the Licence.
*/


/**
 * VARIABLES
 */


/**
 * FUNCTIONS
 */

function parseParams(str) {
    return str.split('&').reduce(function (params, param) {
        var paramSplit = param.split('=').map(function (value) {
            return decodeURIComponent(value.replace(/\+/g, ' '));
        });
        params[paramSplit[0]] = paramSplit[1];
        return params;
    }, {});
}

/**
 * BINDS
 */

// Sending AJAX invitation to ID from Select
$(document).on('click', '#user-alliance-send-invitation', function (e) {
    e.preventDefault();
    e.stopPropagation();
    e.stopImmediatePropagation();

    var invite_id = $("#user-invite-select").select2("data")[0].id;
    if (invite_id) {
        var url = invite_url + "?inviteId=" + invite_id;
        var user_id = $(this).data('user-id');
        if (user_id) {
            url = url + "&idUser=" + user_id;
        }
        GET(url, null, null, null, null, getAllFlashAlertMessages);
    }
});

// Change title to input for AJAX change
$(document).on('click', '#change-name-request', function () {
    $(this).toggleClass('d-none');
    $('#name-change-link').toggleClass('d-none');

    var title = $('#alliance-name');
    title.replaceWith('<input id="alliance-name-input" class="livebox-user__content__title__input" max-length="" value="' + title.text().trim() + '">');
});

// Set URL with params to link
$(document).on('keyup', '#alliance-name-input', function () {
    var link = $('#name-change-link');
    var params = parseParams(link.attr('href'));
    params[Object.keys(params)[0]] = $(this).val();
    link.attr('href', decodeURIComponent(jQuery.param(params)));
});

// Change description to input for AJAX change
$(document).on('click', '#change-description-request', function () {
    $(this).toggleClass('d-none');
    $('#description-change-link').toggleClass('d-none');

    var title = $('#alliance-description');
    title.replaceWith('<textarea id="alliance-description-input" class="livebox-user__content__description__input">' + title.text().trim() + '</textarea>');
});

// Set URL with params to link
$(document).on('keyup', '#alliance-description-input', function () {
    var newData = {'newDescription': $(this).val()};
    $('#description-change-link').data('params', newData);
});

// Confirmation on button
$(document).on('click', '[data-confirm-text]', function () {
    if ($(this).data('confirm-text') !== undefined) {
        var url = $(this).data('href');
        bootbox.confirm({
            closeButton: false,
            message: $(this).data('confirm-text'),
            className: "bootstrap-dialog",
            title: $(this).data('confirm-title'),
            buttons: {
                confirm: {
                    label: $(this).data('confirm-true'),
                    className: 'btn btn-warning',
                },
                cancel: {
                    label: $(this).data('confirm-false'),
                    className: 'btn btn-outline-secondary',
                }
            },
            callback: function (result) {
                if (result) {
                    location.href = url;
                }
            },
        });
    }
});

// Hide all tooltips after click
$(document).on('click', '.action-link', function () {
    $(this).tooltip("hide");
});
