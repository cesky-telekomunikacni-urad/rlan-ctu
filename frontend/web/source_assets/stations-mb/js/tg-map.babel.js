/*
* Copyright 2023 DactylGroup s.r.o.
*
* Licensed under the EUPL, Version 1.2 or – as soon they
will be approved by the European Commission - subsequent
versions of the EUPL (the "Licence");
* You may not use this work except in compliance with the
Licence.
* You may obtain a copy of the Licence at:
*
* https://joinup.ec.europa.eu/software/page/eupl5
*
* Unless required by applicable law or agreed to in
writing, software distributed under the Licence is
distributed on an "AS IS" basis,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
express or implied.
* See the Licence for the specific language governing
permissions and limitations under the Licence.
*/


class TgMap extends CommonMap {

    moveExclusionZone(center) {

        var zoneCenter = {
            "type": "FeatureCollection",
            "features": [turf.helpers.point(center)]
        };

        this.mapObject.getSource('tg-circle-src').setData(zoneCenter);

    }

    renderExclusionZone(center, radiusElSelector = '#map-calc-tg') {
        var zoneCenter = {
            "type": "FeatureCollection",
            "features": [turf.helpers.point(center)]
        };

        const radius = $(radiusElSelector).data('radius');

        this.mapObject.addSource('tg-circle-src', {
            type: 'geojson',
            data: zoneCenter
        });

        this.mapObject.addLayer({
            id: 'tg-circle',
            type: 'circle',
            source: 'tg-circle-src',
            paint: {

                'circle-color': '#5A67D8',
                'circle-opacity': 0.2,
                "circle-radius": {
                    stops: [
                        [0, 0],
                        [20, this.metersToPixelsAtMaxZoom(radius, center[1])]
                    ],
                    base: 2
                }
            }
        });
    }

}