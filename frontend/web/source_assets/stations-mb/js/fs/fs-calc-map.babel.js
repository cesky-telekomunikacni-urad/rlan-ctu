/*
* Copyright 2023 DactylGroup s.r.o.
*
* Licensed under the EUPL, Version 1.2 or – as soon they
will be approved by the European Commission - subsequent
versions of the EUPL (the "Licence");
* You may not use this work except in compliance with the
Licence.
* You may obtain a copy of the Licence at:
*
* https://joinup.ec.europa.eu/software/page/eupl5
*
* Unless required by applicable law or agreed to in
writing, software distributed under the Licence is
distributed on an "AS IS" basis,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
express or implied.
* See the Licence for the specific language governing
permissions and limitations under the Licence.
*/


class MapCalcFs extends MapCalc {
// used in last step of calculator

    overlayClass = 'overlayCalcFs';


    getLinesUrl() {
        let bounds = this.getBounds();
        return '/api/v1/station/conflict-lines?sw=' + bounds[0] + '&ne=' + bounds[1];
    }

    // vvvv common stuff
    runMap(selector) {
        this.mapObject = new mapboxgl.Map({
            container: selector,
            style: 'mapbox://styles/mapbox/streets-v11',
            center: [15.2304793, 49.820068],
            zoom: 10.5,
            minZoom: 6,
        });
        this.addControls();

        this.mapObject.on('load', () => {

            let jsonData = null;
            let pairStation = {};
            let stationID = this.getStationId();
            idStation = stationID;
            let self = this;
            $.ajax({
                async: false,
                dataType: "json",
                url: '/api/v1/station/geo-station/' + stationID,
                success: function (data) {
                    pairStation = data.features.pop();
                    self.currentStation = data.features.pop();
                }
            });
            this.setBounds();
            let bounds = this.getBounds();

            $.ajax({
                async: false,
                dataType: "json",
                url: '/api/v1/station/geo-conflict-stations?id=' + stationID + '&sw=' + bounds[0] + '&ne=' + bounds[1],
                success: function (data) {
                    jsonData = data;
                }
            });


            this.mapObject.flyTo({
                    center: this.currentStation.geometry.coordinates,
                    essential: true // this animation is considered essential with respect to prefers-reduced-motion
                });

            this.mapObject.addSource('stations', {
                type: 'geojson',
                data: jsonData,

                cluster: true,
                clusterMaxZoom: 8, // Max zoom to cluster points on
                clusterRadius: 100 // Radius of each cluster when clustering points (defaults to 50)
            });

            this.initClickEvents();

            this.mapObject.on('mouseleave', 'unclustered-point-fs', () => {
                this.mapObject.getCanvas().style.cursor = '';
            });

            this.addStationLayers();

            this.initMouseMoveEvents();

            this.initMarkerEvents();

            let stationA = this.currentStation;
            let stationB = pairStation;

            if (this.currentStation.id > pairStation.id) {
                stationA = pairStation;
                stationB = this.currentStation;
            }
                    this.drawLines();

            this.mapObject.on('render', 'unclustered-point-fs', (e) => {
                $("." + this.overlayClass).fadeOut();
            });

            if (jsonData.features.length === 0) {
                $("." + this.overlayClass).fadeOut();
            }

            this.renderLine(stationA, stationB);
            this.renderExclusionZone(stationA.geometry.coordinates, true, '#319795');
            this.renderExclusionZone(stationB.geometry.coordinates, false, '#319795');
            this.setMarkerA(stationA.geometry.coordinates);
            this.setMarkerB(stationB.geometry.coordinates);

        });
    }

    renderLine(stationA, stationB) {
        let result = {
            "type": "FeatureCollection",
            "features": [turf.helpers.lineString([stationA.geometry.coordinates, stationB.geometry.coordinates])]
        };

        this.mapObject.addSource('directionSource', {
            type: 'geojson',
            data: result,
        });

        this.mapObject.addLayer({
            'id': 'directionLayer',
            'type': 'line',
            'source': 'directionSource',
            'layout': {
                'line-join': 'round',
                'line-cap': 'round'
            },
            'paint': {
                'line-color': 'rgba(49,151,149,0.6)',
                'line-width': 4,
            },
        });

        this.stationLayers.forEach( item => {
            this.mapObject.moveLayer('unclustered-point-'+item.name, 'directionLayer');
        });
    }

    setMarkerA(position) {
        let elementWigig = document.createElement('div');
        elementWigig.id = 'wigigMarker';
        elementWigig.classList.add("marker-fs-a");

        this.markerA = new mapboxgl.Marker({
            element: elementWigig,
            draggable: false,
            anchor: 'center'
        }).setLngLat(position).addTo(this.mapObject);

    }

    setMarkerB(position) {
        let elementWigig = document.createElement('div');
        elementWigig.id = 'wigigMarker';
        elementWigig.classList.add("marker-fs-b");


        this.markerB = new mapboxgl.Marker({
            element: elementWigig,
            draggable: false,
            anchor: 'center'
        }).setLngLat(position).addTo(this.mapObject);

    }

}