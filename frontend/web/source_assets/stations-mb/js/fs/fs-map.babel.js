/*
* Copyright 2023 DactylGroup s.r.o.
*
* Licensed under the EUPL, Version 1.2 or – as soon they
will be approved by the European Commission - subsequent
versions of the EUPL (the "Licence");
* You may not use this work except in compliance with the
Licence.
* You may obtain a copy of the Licence at:
*
* https://joinup.ec.europa.eu/software/page/eupl5
*
* Unless required by applicable law or agreed to in
writing, software distributed under the Licence is
distributed on an "AS IS" basis,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
express or implied.
* See the Licence for the specific language governing
permissions and limitations under the Licence.
*/


mapboxgl.accessToken = mapboxToken;

let pointClicked = false;

class MapFs extends CommonMap {
    // used when adding/editing location

    markerA;
    markerB;
    overlayClass = 'overlayFs'

    getStationsUrl() {
        return '/api/v1/station/geo-stations?idToExclude=' + idStation;
    }

    metersToPixelsAtMaxZoom = (meters, latitude) =>
        meters / 0.075 / Math.cos(latitude * Math.PI / 180)

    checkNextStep() {
        if (isInValid() || isEmpty() || this.vm.$data.distanceWarning) {
            $(".loading-button").fadeOut('slow');
        } else {
            $(".loading-button").fadeIn('slow');
        }
    }

    initMarkerEvents() {
        this.mapObject.on('click', (e) => {
            if (pointClicked) {
                return;
            }
            if (!this.markerA) {
                this.setMarkerA(e.lngLat.lng, e.lngLat.lat);
                return;
            }

            if (this.markerA && !this.markerB) {
                this.setMarkerB(e.lngLat.lng, e.lngLat.lat);
            }
        });


        if (this.vm.locationLatA && this.vm.locationLngA) {
            this.setMarkerA(this.vm.locationLngA, this.vm.locationLatA);
        }

        if (this.vm.locationLatB && this.vm.locationLngB) {
            this.setMarkerB(this.vm.locationLngB, this.vm.locationLatB);
        }
    }

    getLinesUrl() {
        return '/api/v1/station/lines?' + ((this.userOnly) ? '&userOnly=true' : '') + ((idStation) ? ('&idToExclude=' + idStation) : '');
    }

    moveLine(setPoint = true) {
        let from = this.markerA.getLngLat().toArray();
        let to = null;

        if (this.markerA && this.markerB) {

            to = this.markerB.getLngLat().toArray();
            this.mapObject.getSource('directionSource').setData({
                "type": "FeatureCollection",
                "features": [turf.helpers.lineString([from, to])]
            });
            if (setPoint) {
                this.vm.$data.locationLatB = to[1];
                this.vm.$data.locationLngB = to[0];
            }
        }
        if (setPoint) {
            this.vm.$data.locationLatA = from[1];
            this.vm.$data.locationLngA = from[0];
        }

        let distance = Math.ceil(turf.distance.default(turf.helpers.point(from), turf.helpers.point(to)));
        this.vm.$data.distance = distance;
        this.vm.$data.distanceWarning = (distance > 10);

        $('form#station-form').dirtyForms('rescan');
    }

    moveMarkerA(lng, lat) {
        if (this.markerA) {
            this.markerA.setLngLat([lng, lat]);
            this.moveLine(false)
        } else {
            this.setMarkerA(lng, lat);
        }
    }

    moveMarkerB(lng, lat) {
        if (this.markerB) {
            this.markerB.setLngLat([lng, lat]);
            this.moveLine(false);
        } else {
            this.setMarkerB(lng, lat);
        }
    }

    renderLine() {
        let result = {
            "type": "FeatureCollection",
            "features": [turf.helpers.lineString([this.markerA.getLngLat().toArray(), this.markerB.getLngLat().toArray()])]
        };

        this.mapObject.addSource('directionSource', {
            type: 'geojson',
            data: result,
        });

        this.mapObject.addLayer({
            'id': 'directionLayer',
            'type': 'line',
            'source': 'directionSource',
            'layout': {
                'line-join': 'round',
                'line-cap': 'round'
            },
            'paint': {
                'line-color': 'rgba(49,151,149,0.6)',
                'line-width': 4,
            },
        });

        // this.mapObject.moveLayer('directionLayer');

        let distance = Math.ceil(
            turf.distance.default(
                turf.helpers.point(this.markerA.getLngLat().toArray()),
                turf.helpers.point(this.markerB.getLngLat().toArray())
            )
        );
        this.vm.$data.distance = distance;
        this.vm.$data.distanceWarning = (distance > 10);
        this.vm.$data.distanceHide = false;

        this.stationLayers.forEach( item => {
            this.mapObject.moveLayer('unclustered-point-'+item.name, 'directionLayer');
        });
    }

    // vvvv common stuff
    runMap(selector) {
        this.mapObject = new mapboxgl.Map({
            container: selector,
            style: 'mapbox://styles/mapbox/streets-v11',
            center: [15.2304793, 49.820068],
            zoom: 6.5,
            minZoom: 6,
        });
        this.addControls();


        this.mapObject.on('load', () => {

            let stationID = this.getStationId();
            idStation = stationID;

            this.mapObject.addSource('stations', {
                type: 'geojson',
                data: this.getStationsUrl(),


                cluster: true,
                clusterMaxZoom: 8, // Max zoom to cluster points on
                clusterRadius: 100 // Radius of each cluster when clustering points (defaults to 50)
            });

            this.initClickEvents();

            this.mapObject.on('mouseleave', 'unclustered-point-fs', () => {
                this.mapObject.getCanvas().style.cursor = '';
            });

            this.addStationLayers();

            this.initMouseMoveEvents();

            this.initMarkerEvents();

            //click event on the cluster zooms the cluster
            this.mapObject.on('click', 'clusters', (e) => {
                let features = this.mapObject.queryRenderedFeatures(e.point, {
                    layers: ['clusters']
                });
                let clusterId = features[0].properties.cluster_id;
                this.mapObject.getSource('stations').getClusterExpansionZoom(
                    clusterId,
                    (err, zoom) => {
                        if (err) return;

                        this.mapObject.easeTo({
                            center: features[0].geometry.coordinates,
                            zoom: zoom
                        });
                    }
                );
            });

            this.mapObject.on('render', 'clusters', (e) => {
                if (!this.mapLoadDone) {
                    this.drawLines();
                    if (this.destinationToFly.length) {
                        const currentZoom = this.mapObject.getZoom();
                        const zoom = currentZoom > 10 ? currentZoom : 10;
                        this.mapObject.flyTo({
                            center: this.destinationToFly,
                            zoom: zoom,
                            essential: true // this animation is considered essential with respect to prefers-reduced-motion
                        });
                    }
                    $(".mapOverlay").fadeOut();
                    this.checkNextStep();
                    this.mapLoadDone = true;
                }
            });

        });
    }

    setMarkerA(lng, lat) {
        let elementWigig = document.createElement('div');
        elementWigig.id = 'wigigMarker';
        elementWigig.classList.add("marker-fs-a");

        this.markerA = new mapboxgl.Marker({
            element: elementWigig,
            draggable: true,
            anchor: 'center'
        }).setLngLat([lng, lat]).addTo(this.mapObject);

        this.vm.$data.locationLatA = lat;
        this.vm.$data.locationLngA = lng;
        $("#station-a-lng").change();
        $("#station-a-lat").change();
        this.markerA.on('drag', (e) => {
            // LNG LAT
            this.moveLine();
        });
        this.markerA.on('dragend', (e) => {
            // LNG LAT
            $("#station-a-lng").change();
            $("#station-a-lat").change();
            this.checkNextStep();
        });
    }

    setMarkerB(lng, lat) {
        let elementWigig = document.createElement('div');
        elementWigig.id = 'wigigMarker';
        elementWigig.classList.add("marker-fs-b");


        this.markerB = new mapboxgl.Marker({
            element: elementWigig,
            draggable: true,
            anchor: 'center'
        }).setLngLat([lng, lat]).addTo(this.mapObject);

        this.vm.$data.locationLatB = lat;
        this.vm.$data.locationLngB = lng;

        $("#station-b-lng").change();
        $("#station-b-lat").change();

        this.markerB.on('drag', (e) => {
            // LNG LAT
            this.moveLine();
        });

        this.markerB.on('dragend', (e) => {
            // LNG LAT
            $("#station-b-lng").change();
            $("#station-b-lat").change();
            this.checkNextStep();
        });
        this.renderLine();
    }

}