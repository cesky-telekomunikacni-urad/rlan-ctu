/*
* Copyright 2023 DactylGroup s.r.o.
*
* Licensed under the EUPL, Version 1.2 or – as soon they
will be approved by the European Commission - subsequent
versions of the EUPL (the "Licence");
* You may not use this work except in compliance with the
Licence.
* You may obtain a copy of the Licence at:
*
* https://joinup.ec.europa.eu/software/page/eupl5
*
* Unless required by applicable law or agreed to in
writing, software distributed under the Licence is
distributed on an "AS IS" basis,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
express or implied.
* See the Licence for the specific language governing
permissions and limitations under the Licence.
*/


mapboxgl.accessToken = mapboxToken;

let pointClicked = false;

class MapDetailFs extends CommonMap {
    overlayClass = 'overlayDetailFs'

    // vvvv common stuff
    runMap(selector) {
        this.mapObject = new mapboxgl.Map({
            container: selector,
            style: 'mapbox://styles/mapbox/streets-v11',
            center: [15.2304793, 49.820068],
            zoom: 12.5,
            minZoom: 6,
        });
        this.addControls();
        this.setBounds();

        this.mapObject.on('load', () => {
            let jsonData = null;
            let currentStation = {};
            let pairStation = {};

            let stationID = this.getStationId();

            $.ajax({
                async: false,
                dataType: "json",
                url: this.getStationsUrl(),
                success: function(data) {
                    jsonData = data;
                }
            });

            $.ajax({
                async: false,
                dataType: "json",
                url: '/api/v1/station/geo-station/' + stationID,
                success: function(data) {
                    currentStation = data.features.pop();
                    pairStation = data.features.pop();
                }
            });

            this.mapObject.addSource('stations', {
                type: 'geojson',
                data: jsonData,


                cluster: true,
                clusterMaxZoom: 8, // Max zoom to cluster points on
                clusterRadius: 100 // Radius of each cluster when clustering points (defaults to 50)
            });

            this.initClickEvents();

            this.mapObject.on('mouseleave', 'unclustered-point-fs', () => {
                this.mapObject.getCanvas().style.cursor = '';
            });

            this.addStationLayers();

            this.drawLines();

            this.initMouseMoveEvents();


            //click event on the cluster zooms the cluster
            this.mapObject.on('click', 'clusters', (e) => {
                let features = this.mapObject.queryRenderedFeatures(e.point, {
                    layers: ['clusters']
                });
                let clusterId = features[0].properties.cluster_id;
                this.mapObject.getSource('stations').getClusterExpansionZoom(
                    clusterId,
                    (err, zoom) => {
                        if (err) return;

                        this.mapObject.easeTo({
                            center: features[0].geometry.coordinates,
                            zoom: zoom
                        });
                    }
                );
            });

            let stationA = currentStation;
            let stationB = pairStation;

            if (currentStation.id > pairStation.id) {
                stationA = pairStation;
                stationB = currentStation;
            }

            this.mapObject.flyTo({
                center: stationA.geometry.coordinates,
                essential: true // this animation is considered essential with respect to prefers-reduced-motion
            });

            this.renderExclusionZone(stationA.geometry.coordinates, true, '#319795');
            this.renderExclusionZone(stationB.geometry.coordinates, false, '#319795');
            this.setMarkerA(stationA.geometry.coordinates);
            this.setMarkerB(stationB.geometry.coordinates);

            this.mapObject.on('render', 'clusters', (e) => {
                if (!this.mapLoadDone) {
                    this.drawLines();

                    this.mapLoadDone = true;
                }
            });

            this.mapObject.on('render', 'unclustered-point-fs', (e) => {
                if (!this.mapLoadDone) {
                    $(".mapOverlay").fadeOut();
                    this.mapLoadDone = true;
                }
            });

        });
    }

    setMarkerA(position) {
        let elementWigig = document.createElement('div');
        elementWigig.id = 'wigigMarker';
        elementWigig.classList.add("marker-fs-a");

        this.markerA = new mapboxgl.Marker({
            element: elementWigig,
            draggable: false,
            anchor: 'center'
        }).setLngLat(position).addTo(this.mapObject);

    }

    setMarkerB(position) {
        let elementWigig = document.createElement('div');
        elementWigig.id = 'wigigMarker';
        elementWigig.classList.add("marker-fs-b");


        this.markerB = new mapboxgl.Marker({
            element: elementWigig,
            draggable: false,
            anchor: 'center'
        }).setLngLat(position).addTo(this.mapObject);

    }
}
