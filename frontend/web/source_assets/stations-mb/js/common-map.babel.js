/*
* Copyright 2023 DactylGroup s.r.o.
*
* Licensed under the EUPL, Version 1.2 or – as soon they
will be approved by the European Commission - subsequent
versions of the EUPL (the "Licence");
* You may not use this work except in compliance with the
Licence.
* You may obtain a copy of the Licence at:
*
* https://joinup.ec.europa.eu/software/page/eupl5
*
* Unless required by applicable law or agreed to in
writing, software distributed under the Licence is
distributed on an "AS IS" basis,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
express or implied.
* See the Licence for the specific language governing
permissions and limitations under the Licence.
*/


mapboxgl.accessToken = mapboxToken;

let pointClicked = false;
let idStation;

class CommonMap {
    mapObject;
    vm;
    overlayClass = 'overlayIndex';
    userOnly = false;
    highlightStateId = null;
    normalZoomEnabled = true;
    mapLoadDone = false;
    destinationToFly = [];
    stationLayers = [
        {
            name: 'wigig',
            color: '#D53F8C'
        },
        {
            name: 'fs',
            color: '#319795'
        },
        {
            name: 'ap_58ghz',
            color: '#FF3300'
        },
        {
            name: 'tg_58ghz',
            color: '#5A67D8'
        },
        {
            name: '52ghz',
            color: '#D69E2E'
        }
    ];
    exclusionZones58GHz = null;
    exclusionZoneDiameter58GHz = exclusionZoneDiameter58GHz;

    /**
     * Verifies that GPS coordinate is in the right decimal format -> xx.abc eg. 48.12543
     *
     * @param coordinate
     * @returns boolean -> correct / incorrect coordinate format
     */
    verifyGPSFormat(coordinate) {
        // the coordinate must be in decimal format, xx.abc...; eg. 48.12543
        const regex = /^[0-9][0-9]\.[0-9]+$/g;
        return coordinate.match(regex);
    }

    /**
     * Retreive exclusion zones for 58GHz stations from config value and transform them into
     * geoJson for further map rendering
     */
    parseExclusionZones() {
        if (exclusionZones58GHz == null)
            return

        const that = this;

        var geojson = {};
        geojson['type'] = 'FeatureCollection';
        geojson['features'] = [];


        let itemLines = exclusionZones58GHz.split("\n");
        if (itemLines.length !== 0) {
            itemLines.forEach( lineItem => {
                let parsedLineItem = lineItem.split(",");

                const lat = $.trim(parsedLineItem[0]);
                const lng = $.trim(parsedLineItem[1]);

                if (that.verifyGPSFormat(lng) && that.verifyGPSFormat(lat)) {
                    let feature = turf.helpers.point([lng, lat]);
                    geojson.features.push(feature);
                }
            });
            that.exclusionZones58GHz = geojson;
        }
    }

    /**
     * Renders exclusion zones for 58GHz stations in map
     */
    renderExclusionZones58GHz() {

        //if exclusion zones object is null, or has no features try to preload features again
        if (this.exclusionZones58GHz == null || this.exclusionZones58GHz.features.length === 0) {
            this.parseExclusionZones();
        }

        // convert km to m
        const radius = this.exclusionZoneDiameter58GHz * 1000;

        this.mapObject.addSource('exclusionZone58ghzSource', {
            type: 'geojson',
            data: this.exclusionZones58GHz
        });

        this.mapObject.addLayer({
            id: 'exclusion-zone-58ghz',
            type: 'circle',
            source: 'exclusionZone58ghzSource',
            filter: ['all'],
            'paint': {
                "circle-radius": {
                    stops: [
                        [0, 0],
                        [20, this.metersToPixelsAtMaxZoom(radius, 49.45)]
                    ],
                    base: 2
                },
                'circle-color': '#ff6666',
                'circle-opacity': 0.2,
            },
        });

    }

    getStationsUrl() {
        return '/api/v1/station/geo-stations?';
    }

    setMyOrAll(my) {
        this.vm.myOnly = my;
        if (my) {
            // stations cannot be filtered by setFilter because of clustering
            this.mapObject.getSource('stations').setData(this.getStationsUrl() + '&userOnly=true');
            this.mapObject.setFilter('route', ['==', ['get', 'my'], true]);
        } else {
            this.mapObject.setFilter('route', null);
            this.mapObject.getSource('stations').setData(this.getStationsUrl());
        }
    }

    addControls() {
        this.mapObject.addControl(new mapboxgl.ScaleControl());
        this.mapObject.addControl(new mapboxgl.NavigationControl());
        this.mapObject.addControl(new mapboxgl.FullscreenControl());

        // disable mouse rotation
        this.mapObject.dragRotate.disable();
        // disable map rotation using touch rotation gesture
        this.mapObject.touchZoomRotate.disableRotation();
    }

    metersToPixelsAtMaxZoom = (meters, latitude) =>
        meters / 0.075 / Math.cos(latitude * Math.PI / 180)

    initMarkerEvents() {
    }

    initClickEvents() {
        this.stationLayers.forEach( (item) => {
            this.mapObject.on('click', 'unclustered-point-'+item.name, (e) => {
                pointClicked = true;
                this.renderPopup(e);
            });
        });

        //click event on the cluster zooms the cluster
        this.mapObject.on('click', 'clusters', (e) => {
            let features = this.mapObject.queryRenderedFeatures(e.point, {
                layers: ['clusters']
            });
            let clusterId = features[0].properties.cluster_id;
            this.mapObject.getSource('stations').getClusterExpansionZoom(
                clusterId,
                (err, zoom) => {
                    if (err) return;

                    this.mapObject.easeTo({
                        center: features[0].geometry.coordinates,
                        zoom: zoom
                    });
                }
            );
        });
    }

    addStationLayers() {
        // cluster layer
        this.mapObject.addLayer({
            id: 'clusters',
            type: 'circle',
            source: 'stations',
            filter: ['has', 'point_count'],
            paint: {
                'circle-color': [
                    'step',
                    ['get', 'point_count'],
                    '#FFFFFF',
                    100,
                    '#FFFFFF',
                    750,
                    '#FFFFFF'
                ],
                'circle-radius': [
                    'step',
                    ['get', 'point_count'],
                    20,
                    100,
                    30,
                    750,
                    40
                ],
                'circle-stroke-color': '#000000',
                'circle-stroke-width': 4,
                'circle-stroke-opacity': 0.35,
            }
        });

        // cluster count numbers layer
        this.mapObject.addLayer({
            id: 'cluster-count',
            type: 'symbol',
            source: 'stations',
            filter: ['has', 'point_count'],
            layout: {
                'text-field': '{point_count_abbreviated}',
                'text-font': ['DIN Offc Pro Medium', 'Arial Unicode MS Bold'],
                'text-size': 12
            }
        });

        this.stationLayers.forEach( (item) => {
            this.mapObject.addLayer({
                id: 'unclustered-point-'+item.name,
                type: 'circle',
                source: 'stations',
                filter: ['all',
                    ['!', ['has', 'point_count']],
                    ['==', item.name, ['get', 't']]
                ],
                'paint': {
                    'circle-radius': [
                        'case',
                        ['boolean', ['feature-state', 'hover'], false],
                        10,
                        7
                    ],
                    'circle-color': item.color,
                    'circle-stroke-color': [
                        'case',
                        ['boolean', ['feature-state', 'highlight'], false],
                        'rgba(255,59,59,0.26)',
                        'white'
                    ],
                    'circle-stroke-width': [
                        'case',
                        ['boolean', ['feature-state', 'highlight'], false],
                        15,
                        2
                    ],
                },
            });
        });
    }

    addExclusionZoneLayers() {
        // cluster layer
        this.mapObject.addLayer({
            id: 'clusters',
            type: 'circle',
            source: 'stations',
            filter: ['has', 'point_count'],
            paint: {
                'circle-color': [
                    'step',
                    ['get', 'point_count'],
                    '#FFFFFF',
                    100,
                    '#FFFFFF',
                    750,
                    '#FFFFFF'
                ],
                'circle-radius': [
                    'step',
                    ['get', 'point_count'],
                    20,
                    100,
                    30,
                    750,
                    40
                ],
                'circle-stroke-color': '#000000',
                'circle-stroke-width': 4,
                'circle-stroke-opacity': 0.35,
            }
        });

        // cluster count numbers layer
        this.mapObject.addLayer({
            id: 'cluster-count',
            type: 'symbol',
            source: 'stations',
            filter: ['has', 'point_count'],
            layout: {
                'text-field': '{point_count_abbreviated}',
                'text-font': ['DIN Offc Pro Medium', 'Arial Unicode MS Bold'],
                'text-size': 12
            }
        });

        this.stationLayers.forEach( (item) => {
            this.mapObject.addLayer({
                id: 'unclustered-point-' + item.name,
                type: 'circle',
                source: 'stations',
                filter: ['all',
                    ['!', ['has', 'point_count']],
                    ['==', item.name, ['get', 't']]
                ],
                'paint': {
                    'circle-radius': [
                        'case',
                        ['boolean', ['feature-state', 'hover'], false],
                        10,
                        7
                    ],
                    'circle-color': item.color,
                    'circle-stroke-color': [
                        'match',
                        ['get', 'c'],
                        1,
                        'red',
                        /* other */ 'white'
                    ],
                    'circle-stroke-width': [
                        'case',
                        ['boolean', ['feature-state', 'highlight'], false],
                        15,
                        2
                    ],
                },
            });
        });
    }

    initMouseMoveEvents() {
        this.stationLayers.forEach( (item) => {
            this.mapObject.on('mouseenter', 'unclustered-point-'+item.name, (e) => {
                this.mouseIn(e)
            });
            this.mapObject.on('mousemove', 'unclustered-point-'+item.name, (e) => {
                this.mouseMove(e)
            });
            this.mapObject.on('mouseleave', 'unclustered-point-'+item.name, (e) => {
                this.mouseOut(e)
            });
        });
    }

    initRenderEvents() {
        this.mapObject.once('idle', (e) => {
            if (mainApp.map.markerWigig)
                mainApp.map.moveDirectionMarker(mainApp.map.directionToBearing(mainApp.direction));
        });

        this.mapObject.on('render', 'clusters', (e) => {
            if (!this.mapLoadDone) {
                this.drawLines();
                if(this.destinationToFly.length){
                    const currentZoom = this.mapObject.getZoom();
                    const zoom = currentZoom > 10 ? currentZoom : 10;
                    this.mapObject.flyTo({
                        center: this.destinationToFly,
                        zoom: zoom,
                        essential: true // this animation is considered essential with respect to prefers-reduced-motion
                    });
                }
                $("." + this.overlayClass).fadeOut();
                this.mapLoadDone = true;
            }
        });

        this.stationLayers.forEach( (item) => {
            this.mapObject.on('render', 'unclustered-point-'+item.name, (e) => {
                if (!this.mapLoadDone) {
                    $("." + this.overlayClass).fadeOut();
                    this.mapLoadDone = true;
                }
            });
        });

        // Even when clusters are not rendered, we still need remove overlay
        this.mapObject.on('render', (e) => {
            if (this.mapObject.isSourceLoaded('stations')) {
                if (!this.mapLoadDone) {
                    $("." + this.overlayClass).fadeOut();
                    this.mapLoadDone = true;
                }
            }
        });
    }

    getBounds() {
        return [
            // defaults for Czechia
            [47.213370782634115, 11.087653930123038].reverse(), // [west, south]
            [52.622243670939596, 20.245978826876502].reverse()  // [east, north]
        ];
    }

    setBounds() {
        let bounds = this.getBounds();
        this.mapObject.setMaxBounds(bounds);
    }

    // vvvv common stuff
    runMap(selector) {

        let minZoom = 5;

        this.mapObject = new mapboxgl.Map({
            container: selector,
            style: 'mapbox://styles/mapbox/streets-v11',
            center: [15.2304793, 49.820068],
            zoom: minZoom + 0.5,
            minZoom: minZoom,
        });

        this.setBounds();
        this.addControls();

        // enable zoom with CTRL when <1024px (otherwise disabled because of fullscreen and scrolling)
        this.mapObject.on('resize', () => {
            if (window.innerWidth < 1024) {
                this.mapObject.scrollZoom.disable();
                this.normalZoomEnabled = false;
            } else {
                this.mapObject.scrollZoom.enable();
                this.normalZoomEnabled = true;
            }
        });

        this.mapObject.scrollZoom.setWheelZoomRate(0.02); // Default 1/450
        this.mapObject.on("wheel", event => {
            if (!this.normalZoomEnabled) {
                if (event.originalEvent.ctrlKey) { // Check if CTRL key is pressed
                    event.originalEvent.preventDefault(); // Prevent chrome/firefox default behavior
                    if (!this.mapObject.scrollZoom._enabled) this.mapObject.scrollZoom.enable(); // Enable zoom only if it's disabled
                } else {
                    if (this.mapObject.scrollZoom._enabled) this.mapObject.scrollZoom.disable(); // Disable zoom only if it's enabled
                }
            }
        });

        this.mapObject.on('moveend', (event) => {
            if (this.vm.updateParams) {
                this.vm.updateParams({
                    bounds: {
                        east: event.target.getBounds().getEast(),
                        west: event.target.getBounds().getWest(),
                        north: event.target.getBounds().getNorth(),
                        south: event.target.getBounds().getSouth(),
                    },
                    page: 1
                });
            }
        });

        this.mapObject.on('load', () => {
            this.mapObject.addSource('stations', {
                type: 'geojson',
                data: '/api/v1/station/geo-stations' + ((this.userOnly) ? '?userOnly=true' : ''),

                cluster: true,
                clusterMaxZoom: 8, // Max zoom to cluster points on
                clusterRadius: 100 // Radius of each cluster when clustering points (defaults to 50)
            });

            this.initClickEvents();

            this.mapObject.on('mouseleave', 'unclustered-point-fs', () => {
                this.mapObject.getCanvas().style.cursor = '';
            });

            this.addStationLayers();

            this.initMarkerEvents();

            this.initMouseMoveEvents();

            this.mapLoadDone = false;

            this.initRenderEvents();
        });
    }

    getLinesUrl() {
        return '/api/v1/station/lines?' + ((this.userOnly) ? '&userOnly=true' : '');
    }

    drawLines() {
        let url = this.getLinesUrl();

        this.mapObject.addSource('lines', {
            type: 'geojson',
            data: url
        });


        this.mapObject.addLayer({
            'id': 'route',
            'type': 'line',
            'source': 'lines',
            'layout': {
                'line-join': 'round',
                'line-cap': 'round'
            },
            'paint': {
                'line-color': 'rgba(49,151,149,0.6)',
                'line-width': 2
            },
            'minzoom': 9,
        });

        //moving routes behind (z-index) markers
        this.stationLayers.forEach( item => {
            this.mapObject.moveLayer('route', 'unclustered-point-'+item.name);
        });

        if (this.mapObject.getSource('directionLayer')) {
            this.stationLayers.forEach( item => {
                this.mapObject.moveLayer('unclustered-point-'+item.name, 'directionLayer');
            });
        }
    }


    mouseIn(e) {
        let point = e.features[0];
        this.activeStationId = point.id;
        this.mapObject.getCanvas().style.cursor = 'pointer';

        if (e.features.length > 0) {
            if (this.hoveredStateId) {
                this.mapObject.setFeatureState(
                    {source: 'stations', id: this.hoveredStateId},
                    {hover: false}
                );
            }
            this.hoveredStateId = e.features[0].id;
            this.mapObject.setFeatureState(
                {source: 'stations', id: this.hoveredStateId},
                {hover: true}
            );
        }

        if (point.properties.t === 'wigig') {
            this.renderWigigRay(point);
        }
    }

    renderWigigRay(point) {
        let result = {
            "type": "FeatureCollection",
            "features": [turf.helpers.lineString([point.geometry.coordinates, [point.properties.w_lng, point.properties.w_lat]])]
        };
        if (this.mapObject.getSource('wigigRaySource')) {
            this.mapObject.removeLayer('wigigRayLayer');
            this.mapObject.removeSource('wigigRaySource');
        }

        this.mapObject.addSource('wigigRaySource', {
            type: 'geojson',
            data: result,
        });

        this.mapObject.addLayer({
            'id': 'wigigRayLayer',
            'type': 'line',
            'source': 'wigigRaySource',
            'layout': {
                'line-join': 'round',
                'line-cap': 'round'
            },
            'paint': {
                'line-color': 'rgba(213,63,140,0.6)',
                'line-width': 4
            },
        });

        // change the direction
        this.stationLayers.forEach( item => {
            this.mapObject.moveLayer('unclustered-point-'+item.name, 'wigigRayLayer');
        });

        if (this.mapObject.getSource('directionLayer')) {
            this.stationLayers.forEach( item => {
                this.mapObject.moveLayer('unclustered-point-'+item.name, 'directionLayer');
            });
        }
    }

    mouseOut(e) {
        this.mapObject.getCanvas().style.cursor = '';
        this.activeStationId = undefined;

        if (this.hoveredStateId) {
            this.mapObject.setFeatureState(
                {source: 'stations', id: this.hoveredStateId},
                {hover: false}
            );
        }
        this.hoveredStateId = null;
        if (this.mapObject.getSource('wigigRaySource')) {
            this.mapObject.removeLayer('wigigRayLayer');
            this.mapObject.removeSource('wigigRaySource');
        }
    }

    mouseMove(e) {
        if (this.activeStationId !== e.features[0].id) {
            this.activeStationId = e.features[0].id;
            this.mouseOut();
            this.mouseIn(e);
        }
    }

    renderPopup(e) {
        e.preventDefault();
        let coordinates = e.features[0].geometry.coordinates.slice();

        while (Math.abs(e.lngLat.lng - coordinates[0]) > 180) {
            coordinates[0] += e.lngLat.lng > coordinates[0] ? 360 : -360;
        }

        let id = e.features[0].id;
        let self = this;
        $.get('/api/v1/station/station-popup/' + id, function (data, status) {
            pointClicked = false;
            let link = self.mapObject.getContainer().dataset.stationlink;
            // Replace all links with real URL
            data.data = data.data.replaceAll('special_replacable_link_to_station', link);
            let description = " <div class='stations-popup'>" + data.data + "</div>";

            new mapboxgl.Popup({maxWidth: 'none'})
                .setLngLat(coordinates)
                .setHTML(description)
                .addTo(self.mapObject);
        });
    }

    renderExclusionZone(center, stationA, color) {
        let sourceName = stationA ? 'fs-circle-a' : 'fs-circle-b';

        let zoneCenter = {
            "type": "FeatureCollection",
            "features": [turf.helpers.point(center)]
        };

        this.mapObject.addSource(sourceName, {
            type: 'geojson',
            data: zoneCenter
        });


        this.mapObject.addLayer({
            id: sourceName,
            type: 'circle',
            source: sourceName,
            paint: {
                'circle-color': color,
                'circle-opacity': 0.2,
                "circle-radius": {
                    stops: [
                        [0, 0],
                        [20, this.metersToPixelsAtMaxZoom(3500, lat)]
                    ],
                    base: 2
                }
            }
        });

        this.stationLayers.forEach( item => {
            this.mapObject.moveLayer(sourceName, 'unclustered-point-'+item.name);
        });
    }

    moveExclusionZone(stationA) {

        const sourceName = stationA ? 'fs-circle-a' : 'fs-circle-b';
        let center = [];

        if (stationA) {
            center = this.markerA.getLngLat().toArray();
        } else {
            center = this.markerB.getLngLat().toArray();
        }

        this.mapObject.getSource(sourceName).setData({
            "type": "FeatureCollection",
            "features": [turf.helpers.point(center)]
        });

    }

    getStationId() {
        let pathname = window.location.pathname;
        let strParts = pathname.split('/');
        let stationID = parseInt(strParts[strParts.length - 1]);
        if (!stationID) {
            // for UC when it comes in form /station/station?id=123
            const queryString = window.location.search;
            const urlParams = new URLSearchParams(queryString);
            stationID = urlParams.get('id');
        }
        return stationID;
    }

    renderStationaryWigigRay(point) {
        var result = {
            "type": "FeatureCollection",
            "features": [turf.helpers.lineString([point.geometry.coordinates, [point.properties.w_lng, point.properties.w_lat]])]
        };

        this.mapObject.addSource('wigigStationaryRaySource', {
            type: 'geojson',
            data: result,
        });

        this.mapObject.addLayer({
            'id': 'wigigStationaryRayLayer',
            'type': 'line',
            'source': 'wigigStationaryRaySource',
            'layout': {
                'line-join': 'round',
                'line-cap': 'round'
            },
            'paint': {
                'line-color': 'rgba(213,63,140,0.6)',
                'line-width': 4
            },
        });

        // change the direction
        this.stationLayers.forEach( item => {
            this.mapObject.moveLayer('unclustered-point-'+item.name, 'wigigStationaryRayLayer');
        });

        if (this.mapObject.getSource('directionLayer')) {
            this.stationLayers.forEach( item => {
                this.mapObject.moveLayer('unclustered-point-'+item.name, 'directionLayer');
            });
        }
    }

}