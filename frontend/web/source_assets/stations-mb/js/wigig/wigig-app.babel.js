/*
* Copyright 2023 DactylGroup s.r.o.
*
* Licensed under the EUPL, Version 1.2 or – as soon they
will be approved by the European Commission - subsequent
versions of the EUPL (the "Licence");
* You may not use this work except in compliance with the
Licence.
* You may obtain a copy of the Licence at:
*
* https://joinup.ec.europa.eu/software/page/eupl5
*
* Unless required by applicable law or agreed to in
writing, software distributed under the Licence is
distributed on an "AS IS" basis,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
express or implied.
* See the Licence for the specific language governing
permissions and limitations under the Licence.
*/


function isInValid() {
    return $("#location-lng").parent().hasClass('has-error') || $("#location-lat").parent().hasClass('has-error') || $("#stationwigig-direction").parent().hasClass('has-error');
}


function isEmpty() {
    return $("#location-lng").val() === '' || $("#location-lat").val() === '' || $("#stationwigig-direction").val() === '';
}

function initWigig() {
    let app = new Vue({
        map: undefined,
        el: '#vueapp-wigig',
        data: {
            locationLat: document.querySelector("input[name='Station[lat]']").value,
            locationLng: document.querySelector("input[name='Station[lng]']").value,
            // in human form
            direction: document.querySelector("input[name='StationWigig[direction]']").value,
            myOnly: false,
        },

        methods: {
            showAll(){
                this.map.setMyOrAll(false);
            },
            showMy(){
                this.map.setMyOrAll(true);
            },
            changePosition: function (event) {
                if (
                    this.locationLng
                    && this.locationLat
                    && this.locationLat > -180
                    && this.locationLat < 180
                    && this.locationLng > -180
                    && this.locationLng < 180) {
                    if (this.map.markerWigig) {
                        this.map.moveDirectionLine(parseFloat(this.locationLng), parseFloat(this.locationLat), true);
                    } else {
                        this.map.setWigigMarker(parseFloat(this.locationLng), parseFloat(this.locationLat), (this.direction ? this.map.directionToBearing(parseFloat(this.direction)) : 0));
                    }
                }
                // triggering the validation
                $("#location-lat").change();
                $("#location-lng").change();
            },
            changeDirection: function (event) {
                if (this.map.markerWigig) {
                    this.map.moveDirectionMarker(this.map.directionToBearing(this.direction));
                    this.map.rotateLine(this.map.directionToBearing(this.direction));
                }

                // triggering the validation
                $("#stationwigig-direction").change();
            },

            setDirectionToPartner: function (event) {
                this.map.setDirectionToPartner();
            },
        },

        created: function () {
            this.map = new MapWigig();
            if(this.locationLat && this.locationLng && this.direction){
                this.map.destinationToFly = [this.locationLng, this.locationLat];
            }
            this.map.runMap('map-wigig');
            this.map.vm = this;
        }
    });

    $(document).ready(function () {
        if (isEmpty()) {
            $(".loading-button").fadeOut('slow');
        } else {
            $(".loading-button").fadeIn('slow');
        }

        $('form#station-form').on('ajaxComplete', function () {
            if (isInValid() || isEmpty()) {
                $(".loading-button").fadeOut('slow');
            } else {
                $(".loading-button").fadeIn('slow');
            }
        });
    });

    return app;
}

let mainApp = initWigig();