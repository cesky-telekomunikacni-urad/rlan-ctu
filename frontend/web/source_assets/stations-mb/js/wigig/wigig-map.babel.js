/*
* Copyright 2023 DactylGroup s.r.o.
*
* Licensed under the EUPL, Version 1.2 or – as soon they
will be approved by the European Commission - subsequent
versions of the EUPL (the "Licence");
* You may not use this work except in compliance with the
Licence.
* You may obtain a copy of the Licence at:
*
* https://joinup.ec.europa.eu/software/page/eupl5
*
* Unless required by applicable law or agreed to in
writing, software distributed under the Licence is
distributed on an "AS IS" basis,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
express or implied.
* See the Licence for the specific language governing
permissions and limitations under the Licence.
*/


mapboxgl.accessToken = mapboxToken;


class MapWigig extends CommonMap {
    activeStationId;
    startPointLng;
    startPointLat;
    endPointLat;
    endPointLng;
    originSource;
    bearing;
    hoveredStateId = null;

    directionMarker;
    overlayClass = 'overlayWigig'

    getStationsUrl() {
        let stationID = this.getStationId();
        idStation = stationID;

        return '/api/v1/station/geo-stations?idToExclude=' + idStation;
    }

    /**
     * human -> mapbox: 190 => -170
     * human -> mapbox: 350 => -10
     * @param direction
     */
    directionToBearing(direction) {
        if (direction > 180) {
            return direction - 360;
        }
        return direction;
    }

    /**
     * mapbox -> human: -90 => 270
     * mapbox -> human: -10 => 350
     * @param bearing
     */
    bearingToDirection(bearing) {
        if (bearing < 0) {
            return 360 + bearing;
        }
        return bearing;
    }

    /**
     *
     * @param lng
     * @param lat
     * @param moveMarker
     */
    moveDirectionLine(lng, lat, moveMarker = false) {
        // startCoords = [LNG LAT]
        if ($("select[name='Station[id_station_pair]']").val() === "") {
            this.showDirectionMarker();
        } else {
            this.hideDirectionMarker();
        }

        if (moveMarker) {
            this.markerWigig.setLngLat([lng, lat]);
        }

        let diffLat = (lat - this.startPointLat);
        let diffLng = (lng - this.startPointLng);
        this.endPointLat = this.endPointLat + diffLat;
        this.endPointLng = this.endPointLng + diffLng;
        this.startPointLat = lat;
        this.startPointLng = lng;

        this.vm.locationLat = this.startPointLat;
        this.vm.locationLng = this.startPointLng;

        this.originSource.features[0].geometry.coordinates = [[lng, lat], [this.endPointLng, this.endPointLat]];

        this.mapObject.getSource('directionSource').setData(this.originSource);
        // TODO toto se chova divne
        let currentPosition = this.directionMarker.getLngLat();
        this.directionMarker.setLngLat(new mapboxgl.LngLat(currentPosition.lng + diffLng, currentPosition.lat + diffLat));


        this.stationLayers.forEach(item => {
            this.mapObject.moveLayer('unclustered-point-' + item.name, 'directionLayer');
        });

        this.setDirectionToPartner();
    }

    meterPixel(latitude, zoomLevel) {
        let earthCircumference = 40075017;
        let latitudeRadians = latitude * (Math.PI / 180);
        return (earthCircumference * Math.cos(latitudeRadians) / Math.pow(2, zoomLevel + 8));
    }

    changeBearing(coords) {
        // COORDS [LNG, LAT]
        let bearing = Math.round(turf.rhumbBearing.default(turf.helpers.point([this.startPointLng, this.startPointLat]), turf.helpers.point(coords)));

        this.rotateLine(bearing);

        this.vm.direction = this.bearingToDirection(bearing);
    }

    rotateLine(bearing) {
        turf.transformRotate(this.originSource, (bearing - this.bearing), {
            pivot: [this.startPointLng, this.startPointLat],
            mutate: true
        });

        this.endPointLat = this.originSource.features[0].geometry.coordinates[1][1];
        this.endPointLng = this.originSource.features[0].geometry.coordinates[1][0];

        this.mapObject.getSource('directionSource').setData(this.originSource);

        this.bearing = bearing;

        this.stationLayers.forEach(item => {
            this.mapObject.moveLayer('unclustered-point-' + item.name, 'directionLayer');
        });
    }

    moveDirectionMarker(bearing) {

        // let km = turf.distance.default(turf.helpers.point([this.startPointLng, this.startPointLat]), turf.helpers.point(this.directionMarker.getLngLat().toArray()));


        let mP = this.meterPixel(lat, this.mapObject.getZoom());
        let km = (mP * 30) / 1000;

        let collection = turf.helpers.featureCollection([
            turf.helpers.point([this.startPointLng, this.startPointLat]),
        ]);

        let markerCoords = turf.transformTranslate(collection, km, bearing);

        this.directionMarker.setLngLat(markerCoords.features[0].geometry.coordinates);
    }

    getBounds() {
        return [
            // defaults for Czechia
            [12, 48.5], // [west, south]
            [18.95, 51.1]  // [east, north]
        ];
    }

    /**
     *
     * @param lng
     * @param lat
     * @param bearing
     */
    renderDirectionLine(lng, lat, bearing = 0) {
        this.bearing = bearing;
        this.startPointLat = lat;
        this.startPointLng = lng;
        let point = turf.helpers.point([lng, lat]);

        this.vm.locationLat = lat;
        this.vm.locationLng = lng;

        let distance = 500;
        // let options = {units: 'kilometers'};
        let destination = turf.destination.default(point, distance, bearing);

        let result = {
            "type": "FeatureCollection",
            "features": [turf.helpers.lineString([[lng, lat], destination.geometry.coordinates])]
        };
        this.originSource = result;

        // coordinates = [ LNG  , LAT]
        this.endPointLat = destination.geometry.coordinates[1];
        this.endPointLng = destination.geometry.coordinates[0];

        this.mapObject.addSource('directionSource', {
            type: 'geojson',
            data: result,
        });

        this.mapObject.addLayer({
            'id': 'directionLayer',
            'type': 'line',
            'source': 'directionSource',
            'layout': {
                'line-join': 'round',
                'line-cap': 'round'
            },
            'paint': {
                'line-color': 'rgba(213,63,140,0.6)',
                'line-width': 4,
            },
        });

        let element6060 = document.createElement('div');
        element6060.id = 'directionMarker';
        element6060.classList.add("direction-marker");

        let mP = this.meterPixel(lat, this.mapObject.getZoom());
        let km = (mP * 30) / 1000;

        let collection = turf.helpers.featureCollection([
            turf.helpers.point([lng, lat]),
        ]);

        let markerCoords = turf.transformTranslate(collection, km, bearing);

        this.directionMarker = new mapboxgl.Marker({
            element: element6060,
            draggable: true,
            anchor: 'center'
        }).setLngLat(markerCoords.features[0].geometry.coordinates).addTo(this.mapObject);

        this.directionMarker.on('drag', (e) => {
            if ($("select[name='Station[id_station_pair]']").val() === "") {
                this.changeBearing(e.target.getLngLat().toArray());
            }
        });
        this.directionMarker.on('dragend', (e) => {
            if ($("select[name='Station[id_station_pair]']").val() === "") {
                this.mapObject.getSource('directionSource').serialize().data;
                $("#stationwigig-direction").change();
            }
        });

        this.stationLayers.forEach(item => {
            this.mapObject.moveLayer('unclustered-point-' + item.name, 'directionLayer');
        });
    }

    initMarkerEvents() {
        this.mapObject.on('click', (e) => {
            if (!this.markerWigig) {
                this.setWigigMarker(e.lngLat.lng, e.lngLat.lat, 0);
                $(".next-step-button").fadeIn('slow');
            }
        });

        if (this.vm.locationLat && this.vm.locationLng && this.vm.direction) {
            this.setWigigMarker(this.vm.locationLng, this.vm.locationLat, this.directionToBearing(this.vm.direction));
        }
    }


    setWigigMarker(lng, lat, bearing) {
        this.vm.direction = this.bearingToDirection(bearing);
        let elementWigig = document.createElement('div');
        elementWigig.id = 'wigigMarker';
        elementWigig.classList.add("wigig-marker");


        this.markerWigig = new mapboxgl.Marker({
            element: elementWigig,
            draggable: true,
            anchor: 'center'
        }).setLngLat([lng, lat]).addTo(this.mapObject);


        this.markerWigig.on('drag', (e) => {
            // LNG LAT
            let pos = e.target.getLngLat();
            this.moveDirectionLine(pos.lng, pos.lat);
            $('form#station-form').dirtyForms('rescan');
        });
        this.markerWigig.on('dragend', (e) => {
            $("#location-lat").change();
            $("#location-lng").change();
        });

        this.renderDirectionLine(lng, lat, 0);
        this.rotateLine(bearing);
        this.setDirectionToPartner();
    }

    hideDirectionMarker() {
        this.directionMarker._element.style.visibility = "hidden";
    }

    showDirectionMarker() {
        this.directionMarker._element.style.visibility = "visible";
    }

    setDirectionToPartner() {
        let partner = $.parseHTML($("select[name='Station[id_station_pair]'] option:selected").text())[0];

        if (partner) {
            if (this.vm.$data.locationLat && this.vm.$data.locationLng) {
                this.hideDirectionMarker();
                let partnerLanLng = [parseFloat($(partner).data('lng')), parseFloat($(partner).data('lat'))];
                let myLatLng = [this.vm.$data.locationLng, this.vm.$data.locationLat];

                let distance = Math.ceil(turf.distance.default(turf.helpers.point(partnerLanLng), turf.helpers.point(myLatLng)));
                if (distance > 10) {
                    // losing partner
                    $("select[name='Station[id_station_pair]']").val('').trigger('change');
                    $("input[name='StationWigig[direction]']").removeClass('as-disabled').attr('readonly', false);
                }

                let dirDeg = Math.round(turf.rhumbBearing.default(turf.helpers.point(myLatLng), turf.helpers.point(partnerLanLng)));
                this.moveDirectionMarker(dirDeg);
                this.vm.$data.direction = this.bearingToDirection(dirDeg);

                this.rotateLine(this.vm.$data.direction);
                $("#stationwigig-direction").change();
            }
        } else {
            this.showDirectionMarker();
        }
    }
}