/*
* Copyright 2023 DactylGroup s.r.o.
*
* Licensed under the EUPL, Version 1.2 or – as soon they
will be approved by the European Commission - subsequent
versions of the EUPL (the "Licence");
* You may not use this work except in compliance with the
Licence.
* You may obtain a copy of the Licence at:
*
* https://joinup.ec.europa.eu/software/page/eupl5
*
* Unless required by applicable law or agreed to in
writing, software distributed under the Licence is
distributed on an "AS IS" basis,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
express or implied.
* See the Licence for the specific language governing
permissions and limitations under the Licence.
*/


class MapDetailWigig extends CommonMap {

    overlayClass = 'overlayDetailWigig';

    // vvvv common stuff
    runMap(selector) {
        this.mapObject = new mapboxgl.Map({
            container: selector,
            style: 'mapbox://styles/mapbox/streets-v11',
            center: [15.2304793, 49.820068],
            zoom: 12.5,
            minZoom: 6,
        });
        this.addControls();
        this.setBounds();

        this.mapObject.on('load', () => {

            let jsonData = null;
            let currentStation = {};
            let pathname = window.location.pathname;
            let strParts = pathname.split('/');
            let stationID = parseInt(strParts[strParts.length-1]);

            $.ajax({
                async: false,
                dataType: "json",
                url: '/api/v1/station/geo-stations',
                success: function(data) {
                    jsonData = data;
                }
            });

            $.ajax({
                async: false,
                dataType: "json",
                url: '/api/v1/station/geo-station/' + stationID,
                success: function(data) {
                    currentStation = data.features.pop();
                }
            });

            const center = currentStation.geometry.coordinates;

            this.mapObject.flyTo({
                center: center,
                essential: true // this animation is considered essential with respect to prefers-reduced-motion
            });

            this.mapObject.addSource('stations', {
                type: 'geojson',
                data: jsonData,

                cluster: true,
                clusterMaxZoom: 8, // Max zoom to cluster points on
                clusterRadius: 100 // Radius of each cluster when clustering points (defaults to 50)
            });

            this.initClickEvents();

            this.addStationLayers();

            this.initMouseMoveEvents();

            this.initMarkerEvents();
            this.drawLines();
            this.setWigigMarker(center[0], center[1]);
            this.renderStationaryWigigRay(currentStation);
            this.renderExclusionZone(center, true, '#D53F8C');

            this.mapObject.on('render', 'clusters', (e) => {
                if (!this.mapLoadDone) {
                    this.drawLines();

                    this.mapLoadDone = true;
                }
            });

            this.mapObject.on('render', 'unclustered-point-wigig', (e) => {
                $("." + this.overlayClass).fadeOut();

            });

        });
    }

    setWigigMarker(lng, lat) {
        let elementWigig = document.createElement('div');
        elementWigig.id = 'wigigMarker';
        elementWigig.classList.add("wigig-marker");


        this.markerWigig = new mapboxgl.Marker({
            element: elementWigig,
            draggable: false,
            anchor: 'center'
        }).setLngLat([lng, lat]).addTo(this.mapObject);

    }

}