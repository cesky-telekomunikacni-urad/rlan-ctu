/*
* Copyright 2023 DactylGroup s.r.o.
*
* Licensed under the EUPL, Version 1.2 or – as soon they
will be approved by the European Commission - subsequent
versions of the EUPL (the "Licence");
* You may not use this work except in compliance with the
Licence.
* You may obtain a copy of the Licence at:
*
* https://joinup.ec.europa.eu/software/page/eupl5
*
* Unless required by applicable law or agreed to in
writing, software distributed under the Licence is
distributed on an "AS IS" basis,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
express or implied.
* See the Licence for the specific language governing
permissions and limitations under the Licence.
*/



class MapDetailWigig extends CommonMap {

    overlayClass = 'overlayDetailWigig';

    // vvvv common stuff
    runMap(selector) {
        this.mapObject = new mapboxgl.Map({
            container: selector,
            style: 'mapbox://styles/mapbox/streets-v11',
            center: [15.2304793, 49.820068],
            zoom: 12.5,
            minZoom: 6,
        });
        this.addControls();
        this.setBounds();

        this.mapObject.on('load', () => {

            let jsonData = null;
            let currentStation = {};
            let stationID = this.getStationId();

            $.ajax({
                async: false,
                dataType: "json",
                url: this.getStationsUrl(),
                success: function (data) {
                    jsonData = data;
                }
            });

            $.ajax({
                async: false,
                dataType: "json",
                url: '/api/v1/station/geo-station/' + stationID,
                success: function (data) {
                    currentStation = data.features.pop();
                }
            });
            const center = currentStation.geometry.coordinates;

            this.mapObject.flyTo({
                center: center,
                essential: true // this animation is considered essential with respect to prefers-reduced-motion
            });

            this.mapObject.addSource('stations', {
                type: 'geojson',
                data: jsonData,

                cluster: true,
                clusterMaxZoom: 8, // Max zoom to cluster points on
                clusterRadius: 100 // Radius of each cluster when clustering points (defaults to 50)
            });

            this.mapObject.on('click', 'unclustered-point-wigig', (e) => {
                pointClicked = true;
                this.renderPopup(e);
            });
            this.mapObject.on('click', 'unclustered-point-fs', (e) => {
                pointClicked = true;
                this.renderPopup(e);
            });

            this.mapObject.on('mouseleave', 'unclustered-point-fs', () => {
                this.mapObject.getCanvas().style.cursor = '';
            });

            // cluster layer
            this.mapObject.addLayer({
                id: 'clusters',
                type: 'circle',
                source: 'stations',
                filter: ['has', 'point_count'],
                paint: {
                    'circle-color': [
                        'step',
                        ['get', 'point_count'],
                        '#FFFFFF',
                        100,
                        '#FFFFFF',
                        750,
                        '#FFFFFF'
                    ],
                    'circle-radius': [
                        'step',
                        ['get', 'point_count'],
                        20,
                        100,
                        30,
                        750,
                        40
                    ],
                    'circle-stroke-color': '#000000',
                    'circle-stroke-width': 4,
                    'circle-stroke-opacity': 0.35,
                }
            });

            // cluster count numbers layer
            this.mapObject.addLayer({
                id: 'cluster-count',
                type: 'symbol',
                source: 'stations',
                filter: ['has', 'point_count'],
                layout: {
                    'text-field': '{point_count_abbreviated}',
                    'text-font': ['DIN Offc Pro Medium', 'Arial Unicode MS Bold'],
                    'text-size': 12
                }
            });

            this.mapObject.addLayer({
                id: 'unclustered-point-fs',
                type: 'circle',
                source: 'stations',
                filter: ['all',
                    ['!', ['has', 'point_count']],
                    ['==', 'fs', ['get', 't']]
                ],
                'paint': {
                    'circle-radius': [
                        'case',
                        ['boolean',
                            ['feature-state', 'hover'],
                            false
                        ],
                        10,
                        7
                    ],
                    'circle-color': '#319795',
                    'circle-stroke-color': [
                        'match',
                        ['get', 'c'],
                        1,
                        'red',
                        /* other */ 'white'
                    ],
                    'circle-stroke-width': 2,
                },
            });

            this.mapObject.addLayer({
                id: 'unclustered-point-wigig',
                type: 'circle',
                source: 'stations',
                filter: ['all',
                    ['!', ['has', 'point_count']],
                    ['==', 'wigig', ['get', 't']]
                ],
                'paint': {
                    'circle-radius': [
                        'case',
                        ['boolean',
                            ['feature-state', 'hover'],
                            false
                        ],
                        10,
                        7
                    ],
                    'circle-color': '#D53F8C',
                    'circle-stroke-color': [
                        'match',
                        ['get', 'c'],
                        1,
                        'red',
                        /* other */ 'white'
                    ],
                    'circle-stroke-width': 2,
                },
            });

            this.drawLines();

            this.mapObject.on('mouseenter', 'unclustered-point-wigig', (e) => {
                this.mouseIn(e)
            });
            this.mapObject.on('mousemove', 'unclustered-point-wigig', (e) => {
                this.mouseMove(e)
            });
            this.mapObject.on('mouseleave', 'unclustered-point-wigig', (e) => {
                this.mouseOut(e)
            });

            this.mapObject.on('mouseenter', 'unclustered-point-fs', (e) => {
                this.mouseIn(e)
            });
            this.mapObject.on('mousemove', 'unclustered-point-fs', (e) => {
                this.mouseMove(e)
            });
            this.mapObject.on('mouseleave', 'unclustered-point-fs', (e) => {
                this.mouseOut(e)
            });

            this.initMarkerEvents();

            //click event on the cluster zooms the cluster
            this.mapObject.on('click', 'clusters', (e) => {
                let features = this.mapObject.queryRenderedFeatures(e.point, {
                    layers: ['clusters']
                });
                let clusterId = features[0].properties.cluster_id;
                this.mapObject.getSource('stations').getClusterExpansionZoom(
                    clusterId,
                    (err, zoom) => {
                        if (err) return;

                        this.mapObject.easeTo({
                            center: features[0].geometry.coordinates,
                            zoom: zoom
                        });
                    }
                );
            });

            this.setWigigMarker(center[0], center[1]);
            this.renderStationaryWigigRay(currentStation);
            this.renderExclusionZone(center, true, '#D53F8C');

            this.mapObject.on('render', 'unclustered-point-wigig', (e) => {
                $("." + this.overlayClass).fadeOut();

            });

        });
    }

    setWigigMarker(lng, lat) {
        let elementWigig = document.createElement('div');
        elementWigig.id = 'wigigMarker';
        elementWigig.classList.add("wigig-marker");


        this.markerWigig = new mapboxgl.Marker({
            element: elementWigig,
            draggable: false,
            anchor: 'center'
        }).setLngLat([lng, lat]).addTo(this.mapObject);

    }

    renderStationaryWigigRay(point) {
        let result = {
            "type": "FeatureCollection",
            "features": [turf.helpers.lineString([point.geometry.coordinates, [point.properties.w_lng, point.properties.w_lat]])]
        };

        this.mapObject.addSource('wigigStationaryRaySource', {
            type: 'geojson',
            data: result,
        });

        this.mapObject.addLayer({
            'id': 'wigigStationaryRayLayer',
            'type': 'line',
            'source': 'wigigStationaryRaySource',
            'layout': {
                'line-join': 'round',
                'line-cap': 'round'
            },
            'paint': {
                'line-color': 'rgba(213,63,140,0.6)',
                'line-width': 4
            },
        });

        // change the direction
        this.mapObject.moveLayer('unclustered-point-wigig', 'wigigStationaryRayLayer');
        this.mapObject.moveLayer('unclustered-point-fs', 'wigigStationaryRayLayer');

        if (this.mapObject.getSource('directionLayer')) {
            this.mapObject.moveLayer('unclustered-point-wigig', 'directionLayer');
            this.mapObject.moveLayer('unclustered-point-fs', 'directionLayer');

        }
    }

}