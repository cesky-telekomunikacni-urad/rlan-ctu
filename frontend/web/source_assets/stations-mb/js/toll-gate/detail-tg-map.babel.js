/*
* Copyright 2023 DactylGroup s.r.o.
*
* Licensed under the EUPL, Version 1.2 or – as soon they
will be approved by the European Commission - subsequent
versions of the EUPL (the "Licence");
* You may not use this work except in compliance with the
Licence.
* You may obtain a copy of the Licence at:
*
* https://joinup.ec.europa.eu/software/page/eupl5
*
* Unless required by applicable law or agreed to in
writing, software distributed under the Licence is
distributed on an "AS IS" basis,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
express or implied.
* See the Licence for the specific language governing
permissions and limitations under the Licence.
*/


mapboxgl.accessToken = 'pk.eyJ1IjoiamlyaXNlbW1sZXIiLCJhIjoiY2s3MGVuYzBvMDVncDNkcDgxbGdkbmVoeCJ9.MEjjIsO-Ou6PUlc74EFEcA';

class MapDetailTG extends TgMap {
    marker;

    overlayClass = 'overlayDetail58';

    setMarker(lng, lat) {

        let elementWigig = document.createElement('div');
        elementWigig.id = 'wigigMarker';
        elementWigig.classList.add("marker-tg");

        this.marker = new mapboxgl.Marker({
            element: elementWigig,
            draggable: false,
            anchor: 'center'
        }).setLngLat([lng, lat]).addTo(this.mapObject);

    }

    // vvvv common stuff
    runMap(selector) {
        this.mapObject = new mapboxgl.Map({
            container: selector,
            style: 'mapbox://styles/mapbox/streets-v11',
            center: [15.2304793, 49.820068],
            zoom: 10.5,
            minZoom: 6,
        });
        this.addControls();
        this.setBounds();


        this.mapObject.on('load', () => {

            let jsonData = null;
            let currentStation = {};
            let pathname = window.location.pathname;
            let strParts = pathname.split('/');
            let stationID = parseInt(strParts[strParts.length-1]);

            $.ajax({
                async: false,
                dataType: "json",
                url: '/api/v1/station/geo-stations',
                success: function(data) {
                    jsonData = data;
                }
            });

            $.ajax({
                async: false,
                dataType: "json",
                url: '/api/v1/station/geo-station/' + stationID,
                success: function(data) {
                    currentStation = data.features.pop();
                }
            });

            this.mapObject.flyTo({
                center: currentStation.geometry.coordinates,
                essential: true // this animation is considered essential with respect to prefers-reduced-motion
            });

            this.mapObject.addSource('stations', {
                type: 'geojson',
                data: jsonData,

                cluster: true,
                clusterMaxZoom: 8, // Max zoom to cluster points on
                clusterRadius: 100 // Radius of each cluster when clustering points (defaults to 50)
            });

            this.initClickEvents();

            this.mapObject.on('mouseleave', 'unclustered-point-fs', () => {
                this.mapObject.getCanvas().style.cursor = '';
            });

            this.addStationLayers();

            this.initMouseMoveEvents();
            this.drawLines();
            this.initMarkerEvents();

            //click event on the cluster zooms the cluster
            this.mapObject.on('click', 'clusters', (e) => {
                var features = this.mapObject.queryRenderedFeatures(e.point, {
                    layers: ['clusters']
                });
                var clusterId = features[0].properties.cluster_id;
                this.mapObject.getSource('stations').getClusterExpansionZoom(
                    clusterId,
                    (err, zoom) => {
                        if (err) return;

                        this.mapObject.easeTo({
                            center: features[0].geometry.coordinates,
                            zoom: zoom
                        });
                    }
                );
            });

            const coordinates = currentStation.geometry.coordinates;
            this.setMarker(coordinates[0], coordinates[1]);
            this.renderExclusionZone(coordinates, '#map-detail-58');

            this.mapObject.on('render', 'clusters', (e) => {
                if (!this.mapLoadDone) {
                    this.drawLines();

                    $(".mapOverlay").fadeOut();

                    this.mapLoadDone = true;
                }
            });

            this.mapObject.on('render', 'unclustered-point-tg', (e) => {
                $("." + this.overlayClass).fadeOut();
            });

            this.mapObject.on('render', 'unclustered-point-ap', (e) => {
                $("." + this.overlayClass).fadeOut();
            });

            this.mapObject.on('render', 'unclustered-point-wigig', (e) => {
                $("." + this.overlayClass).fadeOut();
            });

            this.mapObject.on('render', 'unclustered-point-fs', (e) => {
                $("." + this.overlayClass).fadeOut();
            });

        });
    }
}