/*
* Copyright 2023 DactylGroup s.r.o.
*
* Licensed under the EUPL, Version 1.2 or – as soon they
will be approved by the European Commission - subsequent
versions of the EUPL (the "Licence");
* You may not use this work except in compliance with the
Licence.
* You may obtain a copy of the Licence at:
*
* https://joinup.ec.europa.eu/software/page/eupl5
*
* Unless required by applicable law or agreed to in
writing, software distributed under the Licence is
distributed on an "AS IS" basis,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
express or implied.
* See the Licence for the specific language governing
permissions and limitations under the Licence.
*/


function initMap() {
    let app = new Vue({
        map: undefined,
        el: '#vueapp',
        data() {
            return {
                isLoading: true,
                linkPattern: '',
                prolongLinkPattern: '',
                searchTerm: '',
                myOnly: false,
                columns: [
                    {
                        label: 'id',
                        field: 'id',
                    },
                    {
                        label: 'type',
                        field: 'type',
                    },
                    {
                        label: 'name',
                        field: 'name',
                    },
                    {
                        label: 'status',
                        field: 'status',
                        thClass: 'col-status',
                        html: true,
                    },
                ],
                rows: [],
                totalRecords: 1000,
                serverParams: {
                    columnFilters: {},
                    sort: {
                        field: '',
                        type: '',
                    },
                    search: '',
                    myOnly: false,
                    types: [],
                    bounds: {
                        east: '',
                        west: '',
                        north: '',
                        south: '',
                    },
                    statuses: [],
                    page: 1,
                    perPage: 100
                }
            };
        },


        methods: {
            showAll(){
                this.map.setMyOrAll(false);
            },
            showMy(){
                this.map.setMyOrAll(true);
            },
            flyTo(lng, lat) {
                const currentZoom = this.map.mapObject.getZoom();
                const zoom = currentZoom > 10 ? currentZoom : 10;
                this.map.mapObject.flyTo({
                    center: [
                        parseFloat(lng),
                        parseFloat(lat)
                    ],
                    zoom: zoom,
                    essential: true // this animation is considered essential with respect to prefers-reduced-motion
                });

                this.updateParams({
                    bounds: {
                        east: this.map.mapObject.getBounds().getEast(),
                        west: this.map.mapObject.getBounds().getWest(),
                        north: this.map.mapObject.getBounds().getNorth(),
                        south: this.map.mapObject.getBounds().getSouth(),
                    },
                    page: 1
                })

            },
            getLink(id, editFinished) {
                let out = this.linkPattern.replace(0, id);
                if (editFinished) {
                    out = out + '?editFinished=true';
                }
                return out;
            },
            getProlongLink(id, editFinished) {
                return this.prolongLinkPattern.replace(0, id);
            },
            unpublishStation(id) {
                const that = this;

                bootbox.confirm({
                    message: $('#map').data('confirmation-unpublish-text'),
                    closeButton: false,
                    buttons: {
                        confirm: {
                            label: $('#map').data('yes'),
                            className: 'dk--btn dk--btn--primary',
                        },
                        cancel: {
                            label: $('#map').data('no'),
                            className: 'dk--btn dk--btn--secondary',
                        }
                    },
                    callback: function(result) {
                        if (result) {
                            $.ajax({
                                method: 'post',
                                url: "/api/v1/station/unpublish-station",
                                data: {
                                    id: id
                                }
                            }).done(function (result) {
                                if (result.data === true) {
                                    that.loadItems();
                                    getAllFlashAlertMessages();
                                }
                            });
                        }
                    }
                });
            },
            deleteStation(id) {
                const that = this;

                bootbox.confirm({
                    message: $('#map').data('delete-confirmation-text'),
                    closeButton: false,
                    buttons: {
                        confirm: {
                            label: $('#map').data('yes'),
                            className: 'dk--btn dk--btn--primary',
                        },
                        cancel: {
                            label: $('#map').data('no'),
                            className: 'dk--btn dk--btn--secondary',
                        }
                    },
                    callback: function(result) {
                        if (result) {
                            $.ajax({
                                method: 'post',
                                url: "/api/v1/station/delete-station",
                                data: {
                                    id: id
                                }
                            }).done(function (result) {
                                if (result.data === true) {
                                    that.loadItems();
                                    getAllFlashAlertMessages();
                                }
                            });
                        }
                    }
                });
            },
            updateParams(newProps) {
                this.serverParams = Object.assign({}, this.serverParams, newProps);
                this.loadItems();
            },

            onRowMouseEnter(params) {
                if (this.map.mapObject.getZoom() >= 8) {
                    this.map.mapObject.setFeatureState(
                        // id_orig because ID is the same for both stations in the pair
                        {source: 'stations', id: params.row.id_orig},
                        {highlight: true}
                    );
                }
            },

            onRowMouseLeave(params) {
                if (this.map.mapObject.getZoom() >= 8) {

                    this.map.mapObject.setFeatureState(
                        {source: 'stations', id: params.row.id_orig},
                        {highlight: false}
                    );
                }

            },

            onPageChange(params) {
                this.updateParams({page: params.currentPage});
            },

            onPerPageChange(params) {
                this.updateParams({perPage: params.currentPerPage});
            },

            onSortChange(params) {
                this.updateParams({
                    sort: {
                        type: params[0].type,
                        field: params[0].field,
                    },
                });
            },

            onColumnFilter(params) {
            },

            onSearch(params) {
                this.updateParams({search: params.searchTerm});
                this.loadItems();
            },

            // load items is what brings back the rows from server
            loadItems() {
                axios.post('/api/v1/station/data', this.serverParams, {
                    headers: {
                        'lang': langIso
                    }
                }).then((response) => {
                    this.rows = response.data.data.rows;
                    this.totalRecords = response.data.data.total;

                    $('[data-toggle="tooltip"]').tooltip();
                });
            }
        },

        created: function () {
            this.linkPattern = $("#map").data('stationlink');
            this.prolongLinkPattern = $("#map").data('stationlinkprolong');
            this.loadItems();
            this.map = new CommonMap();
            this.map.runMap('map');
            this.map.vm = this;

        }
    });

    return app;
}

function initHeader(mainApp) {
    let app = new Vue({
        map: undefined,
        mainApp: undefined,
        el: '#vueAppHeader',
        data() {
            return {
                showList: true,
                myOnly: false,
                statuses: [],
                types: [],
                checkedTypes: [],
                checkedStatuses: [],

                searchTerm: '',
            }
        },
        computed: {
            checkedTypesLabels: function () {
                return this.checkedTypes.map(type => type.label).join(', ');
            },
            checkedStatusesLabels: function () {
                return this.checkedStatuses.map(status => status.label).join(', ');
            },
        },
        watch: {
            searchTerm: function (val) {
                this.mainApp.$data.searchTerm = val;
            },
            checkedTypes: function (val) {
                this.mainApp.updateParams({types: this.checkedTypes.map(type => type.id), page: 1});
            },
            checkedStatuses: function (val) {
                this.mainApp.updateParams({statuses: this.checkedStatuses.map(status => status.id), page: 1});
            },
            showList: function (val) {
                if (val) {
                    // divide the map to two parts
                    $("#footer").show();
                    $(".left-container").show();
                    $(".right-container").removeClass('col-md-12').addClass('col-md-6');
                    $("#map").removeClass('full-width').addClass('half-width');

                    this.mainApp.map.mapObject.resize();

                } else {
                    // show fullscreen map
                    $("#footer").hide();
                    $(".left-container").hide();
                    $(".right-container").removeClass('col-md-6').addClass('col-md-12');
                    $("#map").removeClass('half-width').addClass('full-width');

                    this.mainApp.map.mapObject.resize();
                }
            },
            myOnly: function (val) {
                this.mainApp.updateParams({myOnly: val, page: 1});
            }
        },
        methods: {
            setFilters: function () {
                axios.get('/api/v1/station/filters', {
                    headers: {
                        'lang': langIso
                    }
                }).then(response => {
                    this.statuses = response.data.data.statuses;
                    this.types = response.data.data.types;
                    this.status_headline = response.data.data.status_headline;
                    this.type_headline = response.data.data.type_headline;
                });
            },
            clearStatuses: function (target) {
                this.checkedStatuses = [];
            },
            clearTypes: function (target) {
                this.checkedTypes = [];
            },
            closeDropdown: function (closeTarget) {
                $('#' + closeTarget).dropdown('toggle')
            }
        }
        ,
        created: function () {
            this.mainApp = mainApp;
            this.setFilters();
            this.mainApp.map.mapObject.resize();
        }
    });
}

let mainApp = initMap();
initHeader(mainApp);