/*
* Copyright 2023 DactylGroup s.r.o.
*
* Licensed under the EUPL, Version 1.2 or – as soon they
will be approved by the European Commission - subsequent
versions of the EUPL (the "Licence");
* You may not use this work except in compliance with the
Licence.
* You may obtain a copy of the Licence at:
*
* https://joinup.ec.europa.eu/software/page/eupl5
*
* Unless required by applicable law or agreed to in
writing, software distributed under the Licence is
distributed on an "AS IS" basis,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
express or implied.
* See the Licence for the specific language governing
permissions and limitations under the Licence.
*/


class MapCalcFs extends MapCalc {
// used in last step of calculator

    overlayClass = 'overlayCalcFs';


    getLinesUrl() {
        let bounds = this.getBounds();
        return '/api/v1/station/conflict-lines?sw=' + bounds[0] + '&ne=' + bounds[1];
    }

    // vvvv common stuff
    runMap(selector) {
        this.mapObject = new mapboxgl.Map({
            container: selector,
            style: 'mapbox://styles/mapbox/streets-v11',
            center: [15.2304793, 49.820068],
            zoom: 10.5,
            minZoom: 6,
        });
        this.addControls();

        this.mapObject.on('load', () => {

            let jsonData = null;
            let pairStation = {};
            let stationID = this.getStationId();
            idStation = stationID;
            let self = this;
            $.ajax({
                async: false,
                dataType: "json",
                url: '/api/v1/station/geo-station/' + stationID,
                success: function (data) {
                    pairStation = data.features.pop();
                    self.currentStation = data.features.pop();
                }
            });
            this.setBounds();
            let bounds = this.getBounds();

            $.ajax({
                async: false,
                dataType: "json",
                url: '/api/v1/station/geo-conflict-stations?id=' + stationID + '&sw=' + bounds[0] + '&ne=' + bounds[1],
                success: function (data) {
                    jsonData = data;
                }
            });


            if (this.currentStation.properties.t === "fs") {
                let pairId = null;
                $.ajax({
                    async: false,
                    url: '/api/v1/station/pair-id?id=' + stationID,
                    success: function (data) {
                        pairId = parseInt(data);
                    }
                });
                $.each(jsonData.features, function (i, item) {
                    if (item.id === pairId) {
                        pairStation = item;
                        return false;
                    }
                });
            }

            this.mapObject.flyTo({
                center: this.currentStation.geometry.coordinates,
                essential: true // this animation is considered essential with respect to prefers-reduced-motion
            });

            this.mapObject.addSource('stations', {
                type: 'geojson',
                data: jsonData,

                cluster: true,
                clusterMaxZoom: 8, // Max zoom to cluster points on
                clusterRadius: 100 // Radius of each cluster when clustering points (defaults to 50)
            });

            this.mapObject.on('click', 'unclustered-point-wigig', (e) => {
                pointClicked = true;
                this.renderPopup(e);
            });
            this.mapObject.on('click', 'unclustered-point-fs', (e) => {
                pointClicked = true;
                this.renderPopup(e);
            });

            this.mapObject.on('mouseleave', 'unclustered-point-fs', () => {
                this.mapObject.getCanvas().style.cursor = '';
            });

            // cluster layer
            this.mapObject.addLayer({
                id: 'clusters',
                type: 'circle',
                source: 'stations',
                filter: ['has', 'point_count'],
                paint: {
                    'circle-color': [
                        'step',
                        ['get', 'point_count'],
                        '#FFFFFF',
                        100,
                        '#FFFFFF',
                        750,
                        '#FFFFFF'
                    ],
                    'circle-radius': [
                        'step',
                        ['get', 'point_count'],
                        20,
                        100,
                        30,
                        750,
                        40
                    ],
                    'circle-stroke-color': '#000000',
                    'circle-stroke-width': 4,
                    'circle-stroke-opacity': 0.35,
                }
            });

            // cluster count numbers layer
            this.mapObject.addLayer({
                id: 'cluster-count',
                type: 'symbol',
                source: 'stations',
                filter: ['has', 'point_count'],
                layout: {
                    'text-field': '{point_count_abbreviated}',
                    'text-font': ['DIN Offc Pro Medium', 'Arial Unicode MS Bold'],
                    'text-size': 12
                }
            });

            this.mapObject.addLayer({
                id: 'unclustered-point-fs',
                type: 'circle',
                source: 'stations',
                filter: ['all',
                    ['!', ['has', 'point_count']],
                    ['==', 'fs', ['get', 't']]
                ],
                'paint': {
                    'circle-radius': [
                        'case',
                        ['boolean',
                            ['feature-state', 'hover'],
                            false
                        ],
                        10,
                        7
                    ],
                    'circle-color': '#319795',
                    'circle-stroke-color': [
                        'match',
                        ['get', 'c'],
                        1,
                        'red',
                        /* other */ 'white'
                    ],
                    'circle-stroke-width': 2,
                },
            });

            this.mapObject.addLayer({
                id: 'unclustered-point-wigig',
                type: 'circle',
                source: 'stations',
                filter: ['all',
                    ['!', ['has', 'point_count']],
                    ['==', 'wigig', ['get', 't']]
                ],
                'paint': {
                    'circle-radius': [
                        'case',
                        ['boolean',
                            ['feature-state', 'hover'],
                            false
                        ],
                        10,
                        7
                    ],
                    'circle-color': '#D53F8C',
                    'circle-stroke-color': [
                        'match',
                        ['get', 'c'],
                        1,
                        'red',
                        /* other */ 'white'
                    ],
                    'circle-stroke-width': 2,
                },
            });

            this.mapObject.on('mouseenter', 'unclustered-point-wigig', (e) => {
                this.mouseIn(e)
            });
            this.mapObject.on('mousemove', 'unclustered-point-wigig', (e) => {
                this.mouseMove(e)
            });
            this.mapObject.on('mouseleave', 'unclustered-point-wigig', (e) => {
                this.mouseOut(e)
            });

            this.mapObject.on('mouseenter', 'unclustered-point-fs', (e) => {
                this.mouseIn(e)
            });
            this.mapObject.on('mousemove', 'unclustered-point-fs', (e) => {
                this.mouseMove(e)
            });
            this.mapObject.on('mouseleave', 'unclustered-point-fs', (e) => {
                this.mouseOut(e)
            });

            //click event on the cluster zooms the cluster
            this.mapObject.on('click', 'clusters', (e) => {
                let features = this.mapObject.queryRenderedFeatures(e.point, {
                    layers: ['clusters']
                });
                let clusterId = features[0].properties.cluster_id;
                this.mapObject.getSource('stations').getClusterExpansionZoom(
                    clusterId,
                    (err, zoom) => {
                        if (err) return;

                        this.mapObject.easeTo({
                            center: features[0].geometry.coordinates,
                            zoom: zoom
                        });
                    }
                );
            });

            let stationA = this.currentStation;
            let stationB = pairStation;

            if (this.currentStation.id > pairStation.id) {
                stationA = pairStation;
                stationB = this.currentStation;
            }
            this.drawLines();

            this.mapObject.on('render', 'unclustered-point-fs', (e) => {
                $("." + this.overlayClass).fadeOut();
            });

            this.renderLine(stationA, stationB);
            this.renderExclusionZone(stationA.geometry.coordinates, true, '#319795');
            this.renderExclusionZone(stationB.geometry.coordinates, false, '#319795');
            this.setMarkerA(stationA.geometry.coordinates);
            this.setMarkerB(stationB.geometry.coordinates);

        });
    }

    renderLine(stationA, stationB) {
        let result = {
            "type": "FeatureCollection",
            "features": [turf.helpers.lineString([stationA.geometry.coordinates, stationB.geometry.coordinates])]
        };

        this.mapObject.addSource('directionSource', {
            type: 'geojson',
            data: result,
        });

        this.mapObject.addLayer({
            'id': 'directionLayer',
            'type': 'line',
            'source': 'directionSource',
            'layout': {
                'line-join': 'round',
                'line-cap': 'round'
            },
            'paint': {
                'line-color': 'rgba(49,151,149,0.6)',
                'line-width': 4,
            },
        });

        this.mapObject.moveLayer('unclustered-point-wigig', 'directionLayer');
        this.mapObject.moveLayer('unclustered-point-fs', 'directionLayer');
    }

    setMarkerA(position) {
        let elementWigig = document.createElement('div');
        elementWigig.id = 'wigigMarker';
        elementWigig.classList.add("marker-fs-a");

        this.markerA = new mapboxgl.Marker({
            element: elementWigig,
            draggable: false,
            anchor: 'center'
        }).setLngLat(position).addTo(this.mapObject);

    }

    setMarkerB(position) {
        let elementWigig = document.createElement('div');
        elementWigig.id = 'wigigMarker';
        elementWigig.classList.add("marker-fs-b");


        this.markerB = new mapboxgl.Marker({
            element: elementWigig,
            draggable: false,
            anchor: 'center'
        }).setLngLat(position).addTo(this.mapObject);

    }

}