/*
* Copyright 2023 DactylGroup s.r.o.
*
* Licensed under the EUPL, Version 1.2 or – as soon they
will be approved by the European Commission - subsequent
versions of the EUPL (the "Licence");
* You may not use this work except in compliance with the
Licence.
* You may obtain a copy of the Licence at:
*
* https://joinup.ec.europa.eu/software/page/eupl5
*
* Unless required by applicable law or agreed to in
writing, software distributed under the Licence is
distributed on an "AS IS" basis,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
express or implied.
* See the Licence for the specific language governing
permissions and limitations under the Licence.
*/


class MapCalc extends CommonMap {

    currentStation = {};
    bounds = [];

    getBounds() {
        if (this.bounds.length === 0) {
            let point = turf.helpers.point(this.currentStation.geometry.coordinates);
            // square of 20km -> corner is in distance 28.29 km because diagonal^2 = distance^2 + distance^2
            let distance = 28.29;

            /*
            --------------NE---------
                         /
                        / bearing = 45
                       x
                      /
                     /  bearing = 90 + 90 + 45
                    SW
            ------------------------
             */
            let destinationNE = turf.destination.default(point, distance, 45, {});
            let destinationSW = turf.destination.default(point, distance, 225, {});

            this.bounds = [
                destinationSW.geometry.coordinates,
                destinationNE.geometry.coordinates,
            ];
        }
        return this.bounds;
    }
}