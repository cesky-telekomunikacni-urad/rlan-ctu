/*
* Copyright 2023 DactylGroup s.r.o.
*
* Licensed under the EUPL, Version 1.2 or – as soon they
will be approved by the European Commission - subsequent
versions of the EUPL (the "Licence");
* You may not use this work except in compliance with the
Licence.
* You may obtain a copy of the Licence at:
*
* https://joinup.ec.europa.eu/software/page/eupl5
*
* Unless required by applicable law or agreed to in
writing, software distributed under the Licence is
distributed on an "AS IS" basis,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
express or implied.
* See the Licence for the specific language governing
permissions and limitations under the Licence.
*/


function isInValid() {
    return $("#location-lng").parent().hasClass('has-error')
        || $("#location-lat").parent().hasClass('has-error');
}

function isEmpty() {
    return $("#location-lng").val() === ''
        || $("#location-lat").val() === '';
}

function initStation52() {
    let app = new Vue({
        map: undefined,
        el: '#vueapp-52',
        data: {
            locationLat: document.querySelector("input[name='Station[lat]']").value,
            locationLng: document.querySelector("input[name='Station[lng]']").value,
        },

        methods: {
            changePosition: function (event) {

                if (
                    this.locationLng
                    && this.locationLat
                    && this.locationLat > -180
                    && this.locationLat < 180
                    && this.locationLng > -180
                    && this.locationLng < 180) {
                    this.map.moveMarker(parseFloat(this.locationLng), parseFloat(this.locationLat));
                }
                // triggering the validation
                $("#location-lat").change();
                $("#location-lng").change();
            },
        },

        created: function () {
            this.map = new MapStation52();
            if(this.locationLat && this.locationLng){
                this.map.destinationToFly = [this.locationLng, this.locationLat];
            }
            this.map.runMap('map-52');
            this.map.vm = this;
        }
    });

    $(document).ready(function () {
        if (isEmpty()) {
            $(".next-step-button").fadeOut('slow');
        } else {
            $(".next-step-button").fadeIn('slow');
        }

        $('form#station-form').on('ajaxComplete', function () {
            if (isInValid() || isEmpty()) {
                $(".next-step-button").fadeOut('slow');
            } else {
                $(".next-step-button").fadeIn('slow');
            }
        });
    });
}

initStation52();
