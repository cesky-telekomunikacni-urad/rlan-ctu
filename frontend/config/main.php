<?php
/*
* Copyright 2023 DactylGroup s.r.o.
*
* Licensed under the EUPL, Version 1.2 or – as soon they
will be approved by the European Commission - subsequent
versions of the EUPL (the "Licence");
* You may not use this work except in compliance with the
Licence.
* You may obtain a copy of the Licence at:
*
* https://joinup.ec.europa.eu/software/page/eupl5
*
* Unless required by applicable law or agreed to in
writing, software distributed under the Licence is
distributed on an "AS IS" basis,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
express or implied.
* See the Licence for the specific language governing
permissions and limitations under the Licence.
*/
?>


<?php
//Uses
use dactylcore\system\admin\helpers\RssHelper;
use dactylcore\system\admin\helpers\SitemapHelper;

$frontendConfig = [
    'params' => require __DIR__ . '/params.php',
    'bootstrap' => [
        'dc-menu',
        'dc-page',
        'dc-user',
//        'headers',
        function () {

            Yii::$app->urlManager->addRules([
                _t('select-user-type', 'url_rules') => '/dc-user/user/select-user-type',
                _t('register-company', 'url_rules') => '/dc-user/user/register-company',
                _t('login_url', 'url_rules') => '/dc-user/user/login',
                _t('logout_url', 'url_rules') => '/dc-user/user/logout',
                _t('forgotten_password_url', 'url_rules') => '/dc-user/user/forgotten-password',
//                _t('profile_url', 'url_rules') => '/dc-user/user/index',

                // Admin user actions
//                _t('users', 'url_rules') => '/dc-user/user/index',
                _t('users/create', 'url_rules') => '/dc-user/user/create',
                _t('users/update', 'url_rules') => '/dc-user/user/update',
                _t('users/delete', 'url_rules') => '/dc-user/user/delete',

                // Admin user role actions
                _t('user-roles', 'url_rules') => '/dc-user/user-role/index',
                _t('user-roles/create', 'url_rules') => '/dc-user/user-role/create',
                _t('user-roles/update', 'url_rules') => '/dc-user/user-role/update',
                _t('user-roles/delete', 'url_rules') => '/dc-user/user-role/delete',
            ]);

            Yii::$app->urlManager->addRules([
                Yii::createObject([
                    'class' => dactylcore\core\web\FrontendUrlRule::class,
                    'pattern' => ['station', 'url', '/<id:\d+>'],
                    'route' => 'station/station',
                ]),
                Yii::createObject([
                    'class' => dactylcore\core\web\FrontendUrlRule::class,
                    'pattern' => ['station/create_fs', 'url'],
                    'route' => 'station/create-fs',
                ]),
                Yii::createObject([
                    'class' => dactylcore\core\web\FrontendUrlRule::class,
                    'pattern' => ['station/create_wigig', 'url'],
                    'route' => 'station/create-wigig',
                ]),
                Yii::createObject([
                    'class' => dactylcore\core\web\FrontendUrlRule::class,
                    'pattern' => ['station/create-ap', 'url'],
                    'route' => 'station/create-access-point',
                ]),
                Yii::createObject([
                    'class' => dactylcore\core\web\FrontendUrlRule::class,
                    'pattern' => ['station/create-tg', 'url'],
                    'route' => 'station/create-toll-gate',
                ]),
                Yii::createObject([
                    'class' => dactylcore\core\web\FrontendUrlRule::class,
                    'pattern' => ['station/create-52-ghz', 'url'],
                    'route' => 'station/create-station-52-ghz',
                ]),
                Yii::createObject([
                    'class' => dactylcore\core\web\FrontendUrlRule::class,
                    'pattern' => ['station/edit', 'url', '/<id:\d+>'],
                    'route' => 'station/edit',
                ]),
                Yii::createObject([
                    'class' => dactylcore\core\web\FrontendUrlRule::class,
                    'pattern' => ['station/publish', 'url', '/<id:\d+>'],
                    'route' => 'station/publish',
                ]),
                Yii::createObject([
                    'class' => dactylcore\core\web\FrontendUrlRule::class,
                    'pattern' => ['stations', 'url'],
                    'route' => 'station/stations',
                ]),
                Yii::createObject([
                    'class' => dactylcore\core\web\FrontendUrlRule::class,
                    'pattern' => ['prolong-registration', 'url', '/<id:\d+>'],
                    'route' => 'station/prolong-registration',
                ]),
                Yii::createObject([
                    'class' => dactylcore\core\web\FrontendUrlRule::class,
                    'pattern' => ['station/declaration', 'url', '/<id:\d+>'],
                    'route' => 'station/declaration',
                ]),
                Yii::createObject([
                    'class' => dactylcore\core\web\FrontendUrlRule::class,
                    'pattern' => ['station/declare', 'url', '/<id:\d+>'],
                    'route' => 'station/declare',
                ]),
                Yii::createObject([
                    'class' => dactylcore\core\web\FrontendUrlRule::class,
                    'pattern' => ['conversations', 'url'],
                    'route' => 'messaging/index',
                ]),
                Yii::createObject([
                    'class' => dactylcore\core\web\FrontendUrlRule::class,
                    'pattern' => ['conversations/thread', 'url', '/<id:\d+>'],
                    'route' => 'messaging/thread',
                ]),
                Yii::createObject([
                    'class' => dactylcore\core\web\FrontendUrlRule::class,
                    'pattern' => ['conversations/generic-question', 'url'],
                    'route' => 'messaging/generic-question',
                ]),
                Yii::createObject([
                    'class' => dactylcore\core\web\FrontendUrlRule::class,
                    'pattern' => ['conversations/disturbance-question', 'url'],
                    'route' => 'messaging/disturbance-question',
                ]),
                Yii::createObject([
                    'class' => dactylcore\core\web\FrontendUrlRule::class,
                    'pattern' => ['profile_url', 'url_rules'],
                    'route' => '/dc-user/user/index',
                ]),
                Yii::createObject([
                    'class' => dactylcore\core\web\FrontendUrlRule::class,
                    'pattern' => ['profile_url', 'url', '/<id:\d+>'],
                    'route' => '/dc-user/user/user-index',
                ]),
                Yii::createObject([
                    'class' => dactylcore\core\web\FrontendUrlRule::class,
                    'pattern' => ['stations_import_url', 'url'],
                    'route' => '/dc-user/user/file-import',
                ]),
                Yii::createObject([
                    'class' => dactylcore\core\web\FrontendUrlRule::class,
                    'pattern' => ['stations_export_url', 'url'],
                    'route' => '/dc-user/user/stations-export',
                ]),
                Yii::createObject([
                    'class' => dactylcore\core\web\FrontendUrlRule::class,
                    'pattern' => ['station/unpublish', 'url', '/<id:\d+>'],
                    'route' => 'station/unpublish',
                ]),
                Yii::createObject([
                    'class' => dactylcore\core\web\FrontendUrlRule::class,
                    'pattern' => ['station/delete', 'url', '/<id:\d+>'],
                    'route' => 'station/delete',
                ]),
                Yii::createObject([
                    'class' => dactylcore\core\web\FrontendUrlRule::class,
                    'pattern' => ['station/change-type-58-to-52', 'url', '/<id:\d+>'],
                    'route' => 'station/change-type-58-to-52',
                ]),
            ]);

            Yii::$app->getModule('dc-user')->getAccessManager()->addPermissions([
                'app-frontend' => [
                    'station' => [
                        'index',
                        'create',
                        'update',
                        'delete',
                        'add_toll_gate',
                        'add_52ghz',
                        'add_access_point',
                    ],
                ],
            ]);

            Yii::$app->getModule('dc-user')->getAccessManager()->addPermissions([
                'dc-user' => [
                    'user' => [
                        'index',
                        'create',
                        'update',
                        'delete',
                    ],
                    'user-role' => [
                        'index',
                        'create',
                        'update',
                        'delete',
                    ],
                    'gdpr-agreement' => [
                        'index',
                        'delete',
                    ],
                ],
                'dc-system' => [
                    'config' => [
                        'update_dc-user',
                    ],
                ],
            ]);
        },
    ],
    'components' => [
        'session' => [
            'name' => 'app',
        ],
        'formatter' => [
            'decimalSeparator' => '.',
            'thousandSeparator' => '',
        ],
        'swaggerDocs' => [
            'class' => '',
            'excludeDirs' => [// TODO remove users APIs
                '@dc-user/common',
                '@vendor/dactylcore/dc-user',
            ],
            'includeDirs' => [
            ],
        ],
        'menu' => [
            'class' => 'dactylcore\menu\frontend\components\Menu',
        ],
        'assetManager' => [
            'bundles' => [
                'yii\bootstrap\BootstrapPluginAsset' => [
                    'sourcePath' => '@vendor/npm-asset/bootstrap/dist/',
                    'js' => ['js/bootstrap.bundle.js'],
                ],
                'yii\bootstrap\BootstrapAsset' => [
                    'css' => [],
                ],
            ],
        ],
        'user' => [
            'loginUrl' => ['/dc-user/user/login'],
            'enableAutoLogin' => false,
            'absoluteAuthTimeout' => 21600, // 6h
        ],
        'reCaptcha' => [
            'name' => 'reCaptcha',
            'class' => himiklab\yii2\recaptcha\ReCaptcha::class,
            'siteKey' => '6LdqEDUUAAAAAAllnigtBffLqiR7wvdnlG_Cx2U8',
            'secret' => '6LdqEDUUAAAAAFpZr-cE-1btaY1kV07AU2K8XQDG',
        ],
        'view' => [
            'class' => \frontend\components\FrontendView::class,
        ],
    ],
    'modules' => [
        'dc-menu' => [
            'class' => dactylcore\menu\frontend\Module::class,
        ],
        'dc-page' => [
            'class' => dactylcore\page\frontend\Module::class,
        ],
        'dc-user' => [
            'class' => dactylcore\user\frontend\Module::class,
        ],
    ],
];

if (env("API_TESTING_ENVIRONMENT")) {
    $frontendConfig = array_merge($frontendConfig, [
        'defaultRoute' => 'api-testing-site',
        'on beforeAction' => function ($event) {
            if (Yii::$app->request->pathInfo !== '') {
                Yii::$app->response->redirect('/')->send();
                $event->handled = true;
            }
        }
    ]);
}

return $frontendConfig;
