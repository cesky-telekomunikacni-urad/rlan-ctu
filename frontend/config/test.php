<?php
/*
* Copyright 2023 DactylGroup s.r.o.
*
* Licensed under the EUPL, Version 1.2 or – as soon they
will be approved by the European Commission - subsequent
versions of the EUPL (the "Licence");
* You may not use this work except in compliance with the
Licence.
* You may obtain a copy of the Licence at:
*
* https://joinup.ec.europa.eu/software/page/eupl5
*
* Unless required by applicable law or agreed to in
writing, software distributed under the Licence is
distributed on an "AS IS" basis,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
express or implied.
* See the Licence for the specific language governing
permissions and limitations under the Licence.
*/
?>


<?php

$frontendTestConfig = [
    'class' => '\dactylcore\core\web\Application',
    'params' => require __DIR__ . '/params.php',
];

return \yii\helpers\ArrayHelper::merge(
    require(__DIR__ . '/../../vendor/dactylcore/core/src/configs/common/main.php'), // core common
    require(__DIR__ . '/../../vendor/dactylcore/core/src/configs/frontend/main.php'), // core frontend
    require(__DIR__ . '/../../common/config/main.php'), // app common
    require(__DIR__ . '/../../common/config/main-local.php'), // app common local
    require(__DIR__ . '/main.php'), // app frontend
    require(__DIR__ . '/main-local.php'), // app frontend local
    $frontendTestConfig,
    require(__DIR__ . '/test-local.php')
);
