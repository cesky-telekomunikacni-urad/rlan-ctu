<?php
/*
* Copyright 2023 DactylGroup s.r.o.
*
* Licensed under the EUPL, Version 1.2 or – as soon they
will be approved by the European Commission - subsequent
versions of the EUPL (the "Licence");
* You may not use this work except in compliance with the
Licence.
* You may obtain a copy of the Licence at:
*
* https://joinup.ec.europa.eu/software/page/eupl5
*
* Unless required by applicable law or agreed to in
writing, software distributed under the Licence is
distributed on an "AS IS" basis,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
express or implied.
* See the Licence for the specific language governing
permissions and limitations under the Licence.
*/
?>


<?php

namespace frontend\assets;

use dactylcore\core\admin\assets\MaterialIconsAsset;
use dactylcore\core\web\AssetBundle;
use dactylcore\core\web\FrontendAsset;
use dactylcore\core\widgets\common\pjax\PjaxAsset;
use dactylcore\core\widgets\frontend\cookieagreement\CookieAgreementAsset;
use yii\bootstrap\BootstrapAsset;
use yii\web\JqueryAsset;
use yii\web\YiiAsset;

class LayoutAsset extends AssetBundle
{
    public $sourcePath = '@frontend/web/source_assets/layout';

    public $css = [
        'scss/layout.scss',
        'scss/map-popup.scss',
    ];

    public $js = [
        'js/layout.js',
        'js/header.babel.js',
    ];

    public $depends = [
        YiiAsset::class,
        BootstrapAsset::class,
        FrontendAsset::class,
        MaterialIconsAsset::class,
        CookieAgreementAsset::class,
        DactylKitAsset::class,
        StationRegistrationAsset::class,
        LiveBoxAsset::class,
        PjaxAsset::class,
        JqueryAsset::class,
        UserAsset::class,
        BootBoxAsset::class,

        \kartik\select2\ThemeDefaultAsset::class,
        \kartik\select2\Select2Asset::class,
        \dactylcore\core\widgets\common\select2\Select2Asset::class,
    ];
}
