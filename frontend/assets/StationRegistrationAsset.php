<?php
/*
* Copyright 2023 DactylGroup s.r.o.
*
* Licensed under the EUPL, Version 1.2 or – as soon they
will be approved by the European Commission - subsequent
versions of the EUPL (the "Licence");
* You may not use this work except in compliance with the
Licence.
* You may obtain a copy of the Licence at:
*
* https://joinup.ec.europa.eu/software/page/eupl5
*
* Unless required by applicable law or agreed to in
writing, software distributed under the Licence is
distributed on an "AS IS" basis,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
express or implied.
* See the Licence for the specific language governing
permissions and limitations under the Licence.
*/
?>


<?php

namespace frontend\assets;

use dactylcore\core\web\AssetBundle;

class StationRegistrationAsset extends AssetBundle
{
    public $sourcePath = '@frontend/web/source_assets/station-registration';
    public $css
        = [
            'scss/index.scss',
        ];
    public $js
        = [
            'js/controls.babel.js',
            'js/dirtyForm.js',
        ];
    public $depends
        = [
            \dactylcore\core\web\FrontendAsset::class,
            \dactylcore\core\widgets\common\livebox\LiveBoxAsset::class,
            InputMaskAsset::class,
            ChromeVHFixAsset::class,
            DirtyFormsAsset::class,
        ];

    public function init()
    {
        parent::init();

        \Yii::$app->view->registerJsVar('URL__STATION__IS_MAC_ADDRESS_UNIQUE', url('/station/is-mac-address-unique'));
        \Yii::$app->view->registerJsVar('MAC_DUPLICITY_WARNING', _tF('mac_duplicity_warning', 'station'));

        \Yii::$app->view->registerJsVar('URL__STATION__IS_SERIAL_NUMBER_UNIQUE', url('/station/is-serial-number-unique'));
        \Yii::$app->view->registerJsVar('SERIAL_DUPLICITY_WARNING', _tF('serial_duplicity_warning', 'station'));
    }
}
