<?php
/*
* Copyright 2023 DactylGroup s.r.o.
*
* Licensed under the EUPL, Version 1.2 or – as soon they
will be approved by the European Commission - subsequent
versions of the EUPL (the "Licence");
* You may not use this work except in compliance with the
Licence.
* You may obtain a copy of the Licence at:
*
* https://joinup.ec.europa.eu/software/page/eupl5
*
* Unless required by applicable law or agreed to in
writing, software distributed under the Licence is
distributed on an "AS IS" basis,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
express or implied.
* See the Licence for the specific language governing
permissions and limitations under the Licence.
*/
?>


<?php

namespace frontend\assets;

use dactylcore\core\web\AssetBundle;
use dactylcore\core\widgets\admin\daterangepicker\DateRangePickerAsset;
use dactylcore\core\widgets\common\select2\Select2Asset;
use kartik\grid\ExpandRowColumnAsset;
use kartik\select2\ThemeDefaultAsset;

class DactylKitAsset extends AssetBundle
{
    public $sourcePath = '@frontend/web/source_assets/dactyl-kit';

    public $css = [
        'scss/dactyl-kit.scss',
    ];

    public $js = [
        'js/dactyl-kit.js'
    ];

    public $depends = [
        \dactylcore\core\web\FrontendAsset::class,
        // classes for select2 so layout asset can change styling
        ThemeDefaultAsset::class,
        \kartik\select2\Select2Asset::class,
        Select2Asset::class,
        DateRangePickerAsset::class,
        ExpandRowColumnAsset::class,
    ];
}
