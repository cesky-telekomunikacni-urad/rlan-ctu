<?php
/*
* Copyright 2023 DactylGroup s.r.o.
*
* Licensed under the EUPL, Version 1.2 or – as soon they
will be approved by the European Commission - subsequent
versions of the EUPL (the "Licence");
* You may not use this work except in compliance with the
Licence.
* You may obtain a copy of the Licence at:
*
* https://joinup.ec.europa.eu/software/page/eupl5
*
* Unless required by applicable law or agreed to in
writing, software distributed under the Licence is
distributed on an "AS IS" basis,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
express or implied.
* See the Licence for the specific language governing
permissions and limitations under the Licence.
*/
?>


<?php

namespace api\helpers;

use yii\base\Model;

class ApiHelper
{
    public static function removeTags($text): string
    {
        $text = preg_replace('#<[^>]+>#', ', ', $text);
        return str_replace(', , ', ', ', $text);
    }

    public static function simplifyModelErrors(Model $model): ?string
    {
        if ($model->hasErrors()) {
            $tmp = array_values($model->getFirstErrors());
            return preg_replace('#<[^>]+>#', ' ', $tmp[0]);
        }

        return 'Unknown error';
    }

    public static function appendStationTypeFields($stationTypeModelClass, $stationId, &$fields, $nestUnder = null)
    {
        $stationTypeObject = $stationTypeModelClass::findOne(['id_station' => $stationId]);

        if(!$nestUnder) {
            $newAttrs = [];
            foreach ($stationTypeObject->fields() as $fieldName) {
                $newAttrs[$fieldName] = function () use ($stationTypeObject, $fieldName)
                {
                    return $stationTypeObject->{$fieldName};
                };
            }
            $fields = merge($fields, $newAttrs);
        }else{
            $newAttrs = [];
            foreach ($stationTypeObject->fields() as $fieldName) {
                $newAttrs[$fieldName] = $stationTypeObject->{$fieldName};
            }
            $fields[$nestUnder] = function () use ($newAttrs)
            {
                return $newAttrs;
            };
        }
    }
}
