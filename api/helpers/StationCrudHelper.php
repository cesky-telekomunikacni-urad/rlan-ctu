<?php
/*
* Copyright 2023 DactylGroup s.r.o.
*
* Licensed under the EUPL, Version 1.2 or – as soon they
will be approved by the European Commission - subsequent
versions of the EUPL (the "Licence");
* You may not use this work except in compliance with the
Licence.
* You may obtain a copy of the Licence at:
*
* https://joinup.ec.europa.eu/software/page/eupl5
*
* Unless required by applicable law or agreed to in
writing, software distributed under the Licence is
distributed on an "AS IS" basis,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
express or implied.
* See the Licence for the specific language governing
permissions and limitations under the Licence.
*/
?>


<?php


namespace api\helpers;

use common\models\search\StationSearch;
use common\models\Station;
use common\models\Station52;
use common\models\Station58;
use common\models\StationFs;
use common\models\StationFsPair;
use common\models\StationWigig;
use dactylcore\log\LogManager;
use frontend\controllers\StationController;
use frontend\controllers\StationController as FrontendStationController;
use Yii;
use yii\base\BaseObject;
use yii\helpers\Json;
use yii\web\BadRequestHttpException;
use yii\web\ConflictHttpException;
use yii\web\NotFoundHttpException;

class StationCrudHelper
{
    const RESULT_SUCCESS = 0;
    const RESULT_ERROR = -1;
    const RESULT_WARNING = 1;
    const RESULT_LIMIT = 2;
    const USER_LIMIT = 100;
    //
    public $action;
    public $inputData;
    public $type;
    public $stationData;
    public $stationAData;
    public $stationBData;
    public $errors;
    public $warning;
    public $updateWhenAlreadyPublishedMode;
    public $conflictStations;
    //
    public ?Station $station = null; // updating/deleting station
    //
    public $resultStationObject;
    //
    static array $typeMapping
        = [
            'wifi_5_2' => StationController::TYPE_52,
            'wifi_5_8' => StationController::TYPE_58_AP,
            'fsptp' => StationController::TYPE_FS,
            'fs' => StationController::TYPE_FS,
            'wigig' => StationController::TYPE_WIGIG,
        ];

    protected static function ensureMacAddressAndSerialNumberBothAttrs(&$data)
    {
        if (isset($data['mac_address'])) {
            $data['macAddress'] = $data['mac_address'];
        }
        if (isset($data['serial_number'])) {
            $data['serialNumber'] = $data['serial_number'];
        }
    }

    protected function verifyProperInput()
    {
        if ($this->action == 'update' && ($s = $this->station)) {
            if ($s->isFs()) {
                if (empty($this->inputData['stationA']) && empty($this->inputData['stationB'])) {
                    throw new BadRequestHttpException(_t('Updating FS station requires different input format: either "stationA" or "stationB" array key required'));
                }
            } else {
                if (empty($this->inputData['station'])) {
                    throw new BadRequestHttpException(_t('Updating this station requires different input format: "station" array key required'));
                }
            }
        } elseif ($this->action == 'create') {
            if ($this->type == StationController::TYPE_FS) {
                if (empty($this->inputData['stationA']) || empty($this->inputData['stationB'])) {
                    throw new BadRequestHttpException(_t('Creating FS station requires different input format: "stationA" and "stationB" array keys required'));
                }
            } else {
                if (empty($this->inputData['station'])) {
                    throw new BadRequestHttpException(_t('Creating this station requires different input format: "station" array key required'));
                }
            }
        }
    }

    protected function parseInputData($inputData, $idStation = null)
    {
        $this->inputData = $inputData;

        if ($idStation) {
            $this->station = Station::findOne($idStation);
            $this->type = $this->station->type;
        } else {
            $this->type = $this->getType($this->inputData['type']);
        }

        $this->stationData = $this->inputData['station'] ?? null;
        $this->stationAData = $this->inputData['stationA'] ?? null;
        $this->stationBData = $this->inputData['stationB'] ?? null;

        self::ensureMacAddressAndSerialNumberBothAttrs($this->stationData);
        self::ensureMacAddressAndSerialNumberBothAttrs($this->stationAData);
        self::ensureMacAddressAndSerialNumberBothAttrs($this->stationBData);
    }

    protected function doAction($action, $inputData, $idStation = null): ?Station
    {
        $this->action = $action;
        $this->parseInputData($inputData, $idStation);

        $user = loggedUser();
        if (!$user) {
            throw new NotFoundHttpException();
        }

        $this->errors = [];
        $importedStation = null;

        $t = \Yii::$app->db->beginTransaction();
        try {
            $this->verifyProperInput();

            switch ($action) {
                case 'create':
                case 'update':
                    if ($this->station && $this->station->isPublished()) {
                        $this->updateWhenAlreadyPublishedMode = true;
                    }

                    $importedStation = $this->importStation($this->station);

                    if ($importedStation &&
                        !$importedStation->hasErrors() &&
                        empty($this->errors) &&
                        $this->updateWhenAlreadyPublishedMode) {
                        $t->rollBack();
                        $t = \Yii::$app->db->beginTransaction();

                        $this->station->waiting_api_changes_json = Json::encode($this->inputData);
                        $saved = $this->station->save(true, ['waiting_api_changes_json', 'updated_at']);

                        if ($saved) {
                            $t->commit();
                            $t = \Yii::$app->db->beginTransaction();
                            $importedStation = $this->station;
                            $this->warning
                                = _t('Station is published so these changes were only marked as waiting and can be published manually via /publish endpoint');
                        } else {
                            $this->errors[] = _tF('cannot_save_waiting_api_changes', 'station');
                        }
                    }


                    break;
                case 'delete':
                    if (!Station::deleteStation($this->station->id)) {
                        $this->errors[] = _tF('delete_error_caused_by_failed_delete');
                    } else {
                        $importedStation = $this->station;
                    }

                    break;
            }

            LogManager::getInstance()->saveOperations(false);

            if ($importedStation) {

                // Ensure returning object is from API
                $q = \api\versions\v1\models\Station::find();
                $q->appendDeletedConditions = false;
                $importedStation = $q->andWhere(['id' => $importedStation->id])->one();
                $this->resultStationObject = $importedStation;

                if ($action != 'delete') {
                    // check excluded zones
                    if ($this->type == StationController::TYPE_58_AP) {
                        $search = new StationSearch();
                        $search->checkConflictsWithExclusionZones($importedStation->lng, $importedStation->lat);
                        if ($search->anyInConflict) {
                            throw new BadRequestHttpException(
                                _t('Station is in conflict with exclusion zones, please change parameters')
                            );
                        }
                    }

                    // check conflicts
                    if ($importedStation->hasConflictsWithOthers($conflictStations)) {
                        $this->conflictStations = $conflictStations;
                    }
                }


                if (!$this->updateWhenAlreadyPublishedMode) {
                    $t->commit();
                }


                return $this->resultStationObject;
            }
        } catch (\Throwable $e) {

            if ($e->getCode() != 400 && $e->getCode() != 422) {
                throw $e;
            }

            $this->errors[] = $e->getMessage();
        }

        $t->rollBack();

        return null;
    }

    public function getResponse($code = null)
    {
        if ($this->conflictStations) {
            $mode = 'warning';
            if (!$code) {
                $code = 200;
            }
            if ($code > 400) {
                $mode = 'error';
            }

            $text = "has_conflicts_{$mode}";
            if ($this->updateWhenAlreadyPublishedMode) {
                $text = "has_conflicts_on_update_when_published";
            }

            Yii::$app->response->setStatusCode($code, $mess = _tF($text, 'station'));
            return [
                $mode => $mess,
                "{$mode}Data" => [
                    'conflictStations' => map($this->conflictStations, 'id', function (Station $m)
                    {
                        return [
                            'id' => $m->id_master,
                            'ownerId' => $m->id_user,
                            'ownerName' => $m->user->name,
                            'ownerEmail' => $m->user->email,
                        ];
                    }, true),
                    'solution1_viaParametersChange' => [
                        'how' => _t('solution1_how', 'station'),
                    ],
                    'solution2_viaDeclaration' => [
                        'how' => _t('solution2_how', 'station'),
                        'whatYouDeclare' => str_replace(':', ': ',
                            str_replace('&nbsp;', ' ', strip_tags(c('DECLARATION_TEXT')))),
                    ],
                ],
                $this->updateWhenAlreadyPublishedMode ? 'published' : 'concept' => $this->resultStationObject,
            ];
        }

        if ($this->updateWhenAlreadyPublishedMode && $this->warning) {
            return [
                'warning' => $this->warning,
            ];
        }

        if ($this->resultStationObject) {
            if ($this->resultStationObject->deleted == 1) {
                return [
                    'deleted' => $this->resultStationObject->id,
                ];
            }

            return $this->resultStationObject;
        }

        $attrErrors = $this->getAttributeErrors($mainMessages);
        $mainMessage = _t('Data Validation Failed');
        if (!empty($mainMessages)) {
            $mainMessage = implode(' + ', $mainMessages);
        }

        \Yii::$app->response->setStatusCode(422, $mainMessage);
        return $attrErrors;
    }

    protected function getAttributeErrors(&$mainMessages)
    {
        $tmp = $this->errors;
        $attributeErrors = [];
        $mainMessages = [];

        $fixDoubledAttrs = function ($attrName)
        {
            if ($attrName == 'macAddress') {
                $attrName = 'mac_address';
            }
            if ($attrName == 'serialNumber') {
                $attrName = 'serial_number';
            }

            return $attrName;
        };

        foreach ($tmp as $groupIndex => $groupData) {

            if (is_array($groupData)) {
                if ($groupIndex === 'A' || $groupIndex === 'B') {
                    foreach ($groupData as $group) {
                        foreach ($group as $attr => $textErrors) {
                            if (!is_array($textErrors)) {
                                $textErrors = [$textErrors];
                            }

                            $attributeErrors[] = [
                                'field' => $fixDoubledAttrs($attr),
                                'in' => "station{$groupIndex}",
                                'message' => implode(' + ', $textErrors),
                            ];
                        }
                    }
                } else {
                    foreach ($groupData as $attr => $textErrors) {
                        if (!is_array($textErrors)) {
                            $textErrors = [$textErrors];
                        }
                        $attributeErrors[] = [
                            'field' => $fixDoubledAttrs($attr),
                            'message' => implode(' + ', $textErrors),
                        ];
                    }
                }
            } else {
                $mainMessages[] = $groupData;
            }
        }

        return $attributeErrors;
    }

    public function create($data)
    {
        return $this->doAction('create', $data);
    }

    public function update($id, $data)
    {
        return $this->doAction('update', $data, $id);
    }

    public function delete($id, $data = [])
    {
        return $this->doAction('delete', $data, $id);
    }

    /**
     * @param Station|null $station
     *
     * @return Station
     * @throws \yii\db\Exception
     */
    protected function importStation(?Station $station = null): ?Station
    {
        switch (mb_strtolower($this->type)) {
            case StationController::TYPE_FS:
                return $this->importFs($station);

            case StationController::TYPE_WIGIG:
                return $this->importWigig($station);

            case StationController::TYPE_52:
                return $this->import52($station);

            case StationController::TYPE_58_AP:
            case StationController::TYPE_58_TG:
                return $this->import58Ap($station);
        }

        $this->errors[] = _tF('unknown_type {type}', 'user-import', ['type' => $this->type]);

        return null;
    }

    protected function import52(?Station $station = null): ?Station
    {
        $mappingMain = [
            'lat' => 'B',
            'lng' => 'C',
            'name' => 'G',
            'mac_address' => 'E',
            'macAddress' => 'E',
        ];

        // Add station
        if ($station === null) {
            $modelStation = (new Station());
            $modelStation->status = Station::STATUS_DRAFT;
            $modelStation->hardware_identifier = Station::MAC_ADDRESS;
            $modelStationType = new Station52();

            $mainData = $this->mapData($mappingMain, $this->stationData);
        } else {
            $modelStation = $station;
            $modelStationType = $modelStation->getStationTypeObject();

            $mainData = $this->mapData($mappingMain, array_merge(
                $modelStation->attributes,
                $modelStation->getStationTypeObject()->attributes,
                $this->stationData
            ));
        }

        $modelStation->type = StationController::TYPE_52;
        $modelStation->id_user = loggedUser()->id;
        $modelStation->setChecksum();
        $modelStationType->setChecksum();

        foreach ([
                     StationController::STEP_1_NAME,
                     StationController::STEP_2_LOCATION,
                     StationController::STEP_3_PARAMS,
                 ]
                 as $step) {
            $modelStation->setScenario($step);
            if ($modelStation->load(['Station' => $mainData])) {
                if ($modelStation->isNewRecord) {
                    $modelStation->initRegistrations();
                }

                if ($modelStation->save()) {
                    $modelStationType->id_station = $modelStation->id;

                    if ($modelStationType->save()) {
                        if (!$modelStationType->hasErrors()) {
                            if ($step == StationController::STEP_3_PARAMS) {
                                $modelStation->moveToWaiting();
                            }
                            $step += 1;
                            $modelStation->storeStationProgress($step);
                        }
                    } else {
                        $this->errors[] = $modelStationType->getErrors();
                        if (isset($modelStation->id) && $station === null) {
                            Station::deleteStation($modelStation->id);
                        }
                        return null;
                    }
                } else {
                    $this->errors[] = $modelStation->getErrors();
                    if (isset($modelStation->id) && $station === null) {
                        Station::deleteStation($modelStation->id);
                    }
                    return null;
                }
            }
        }

        return $modelStation;
    }

    /**
     * @param Station|null $station
     *
     * @return bool
     * @throws \yii\db\Exception
     */
    protected function import58Ap(?Station $station = null): ?Station
    {
        $mappingMain = [
            'lat' => 'B',
            'lng' => 'C',
            'name' => 'G',
            'mac_address' => 'E',
            'macAddress' => 'E',
        ];

        // Add station
        if ($station === null) {
            $modelStation = (new Station());
            $modelStation->status = Station::STATUS_DRAFT;
            $modelStation->hardware_identifier = Station::MAC_ADDRESS;
            $modelStationType = new Station58();

            $mainData = $this->mapData($mappingMain, $this->stationData);
        } else {
            $modelStation = $station;
            $modelStationType = $modelStation->getStationTypeObject();

            $mainData = $this->mapData($mappingMain, array_merge(
                $modelStation->attributes,
                $modelStation->getStationTypeObject()->attributes,
                $this->stationData
            ));
        }

        $modelStation->type = StationController::TYPE_58_AP;
        $modelStation->id_user = loggedUser()->id;
        $modelStation->setChecksum();

        $modelStationType->is_ap = 1;
        $modelStationType->setChecksum();

        foreach ([StationController::STEP_1_NAME, StationController::STEP_2_LOCATION, StationController::STEP_3_PARAMS,]
                 as $step) {
            $modelStation->setScenario($step);
            if ($modelStation->load(['Station' => $mainData])) {
                if ($modelStation->isNewRecord) {
                    $modelStation->initRegistrations();
                }

                if ($modelStation->save()) {
                    $modelStationType->id_station = $modelStation->id;

                    if ($modelStationType->save()) {
                        if (!$modelStationType->hasErrors()) {
                            if ($step == StationController::STEP_3_PARAMS) {
                                $modelStation->moveToWaiting();
                            }
                            $step += 1;
                            $modelStation->storeStationProgress($step);
                        }
                    } else {
                        $this->errors[] = $modelStationType->getErrors();
                        if (isset($modelStation->id) && $station === null) {
                            Station::deleteStation($modelStation->id);
                        }
                        return null;
                    }
                } else {
                    $this->errors[] = $modelStation->getErrors();
                    if (isset($modelStation->id) && $station === null) {
                        Station::deleteStation($modelStation->id);
                    }
                    return null;
                }
            }
        }

        return $modelStation;
    }

    /**
     * @param Station|null $station
     *
     * @return int
     * @throws \yii\db\Exception
     */
    protected function importWigig(?Station $station = null): ?Station
    {
        $mappingMain = [
            'lat' => 'B',
            'lng' => 'C',
            'mac_address' => 'E',
            'macAddress' => 'E',
            'serial_number' => 'F',
            'serialNumber' => 'F',
            'name' => 'G',
            'channel_width' => 'K',
            'antenna_volume' => 'M',
            'power' => 'N',
            'id_station_pair' => '',
        ];

        $mappingMainType = [
            'direction' => 'I',
            'eirp' => 'L',
            'eirp_method' => '',
        ];

        if ($station !== null) {
            $mainStation = $station;
            $mainTypeStation = $mainStation->getStationTypeObject();

            $mainData = $this->mapData($mappingMain, array_merge(
                $mainStation->attributes,
                $this->stationData
            ));
            $typeData = $this->mapData($mappingMainType, array_merge(
                $mainTypeStation->attributes,
                $this->stationData
            ));
        } else {
            $mainStation = (new Station());
            $mainTypeStation = (new StationWigig());

            $mainData = $this->mapData($mappingMain, $this->stationData);
            $typeData = $this->mapData($mappingMainType, $this->stationData);
        }

        // Default direction
        if ($typeData['direction']) {
            $typeData['direction'] = round($typeData['direction']);
        }

        // EIRP data set
        $typeData['eirp_method'] = StationWigig::EIRP_METHOD_MANUAL;
        if ($mainData['antenna_volume'] && $mainData['power']) {
            $typeData['eirp'] = $mainData['antenna_volume'] + $mainData['power'];
            $typeData['eirp_method'] = StationWigig::EIRP_METHOD_AUTO;
        }

        $mainStation->status = Station::STATUS_DRAFT;
        $mainStation->hardware_identifier = Station::MAC_ADDRESS;
        $mainStation->setChecksum();
        $mainTypeStation->setChecksum();

        // Load methods from controller
        foreach ([
                     StationController::STEP_1_NAME,
                     StationController::STEP_2_LOCATION,
                     StationController::STEP_3_PARAMS,
                 ]
                 as $step) {
            $mainStation->setScenario($step);
            $mainTypeStation->setScenario($step);
            $mainStation->type = StationController::TYPE_WIGIG;
            $mainStation->setUserId(loggedUser()->id);

            if ($mainStation->load(['Station' => $mainData])) {
                if ($mainStation->isNewRecord) {
                    $mainStation->initRegistrations();
                }
                $mainStation->typeStation = $mainTypeStation;

                if ($step == StationController::STEP_3_PARAMS && empty($mainStation->mac_address)) {
                    $mainStation->hardware_identifier = Station::SERIAL_NUMBER;
                }

                if ($mainStation->save()) {
                    $mainTypeStation->id_station = $mainStation->id;

                    if ($mainTypeStation->load(['StationWigig' => $typeData])) {
                        $mainTypeStation->setPtmp($mainStation->antenna_volume);
                        if ($mainTypeStation->getScenario() == StationController::STEP_2_LOCATION) {
                            $mainTypeStation->setAngleDirections();
                        }
                        if ($mainTypeStation->getScenario() == StationController::STEP_3_PARAMS) {
                            if ($mainTypeStation->eirp_method == StationWigig::EIRP_METHOD_MANUAL) {
                                $mainStation->power = null;
                                $mainStation->antenna_volume = null;
                                $mainStation->save();
                            } elseif ($mainStation->antenna_volume != null && $mainStation->power != null) {
                                $mainTypeStation->eirp = $mainStation->power + $mainStation->antenna_volume;
                            } else {
                                $mainTypeStation->eirp = null;
                            }
                        }
                        if (!$mainTypeStation->save()) {
                            $this->errors[] = $mainTypeStation->getErrors();
                            if ($station === null) {
                                Station::deleteStation($mainStation->id);
                            }
                            return null;
                        }
                    } else {
                        $this->errors[] = _tf('load_error', 'user-import');
                        if (isset($mainStation->id) && $station === null) {
                            Station::deleteStation($mainStation->id);
                        }
                        return null;
                    }
                } else {
                    $this->errors[] = $mainStation->getErrors();
                    if (isset($mainStation->id) && $station === null) {
                        Station::deleteStation($mainStation->id);
                    }
                    return null;
                }
            } else {
                $this->errors[] = $mainStation->getErrors();
                if (isset($mainStation->id) && $station === null) {
                    Station::deleteStation($mainStation->id);
                }
                return null;
            }
        }

        $mainStation->storeStationProgress(StationController::STEP_4_SUMMARY);
        $mainStation->moveToWaiting();

        return $mainStation;
    }

    /**
     * Creates whole new pair of FS's
     *
     * @param Station|null $station
     *
     * @return Station|null
     * @throws \yii\db\Exception
     */
    protected function importFs(?Station $station = null): ?Station
    {
        $fsPair = new StationFsPair();
        $fsPair->initNew();

        if (!$station) {
            if (empty($this->getGPSFromData($this->stationBData))) {
                $this->errors[] = _tF('pair_gps_error', 'user-import');
                return null;
            }
        } else {
            $mainStation = $station;
            $mainStationType = $mainStation->getStationTypeObject();
            $mainStation->pointerToPairStation = $mainStation->getPairStation();

            $mainStationA = $mainStation;
            $mainStationB = $mainStation->pointerToPairStation;

            $fsPair->stationA = $mainStationA;
            $fsPair->stationB = $mainStationB;
        }

        $mapping = [
            'lat' => 'B',
            'lng' => 'C',
            'mac_address' => 'E',
            'macAddress' => 'E',
            'serial_number' => 'F',
            'serialNumber' => 'F',
            'name' => 'G',
            'channel_width' => 'K',
            'antenna_volume' => 'M',
            'power' => 'N',
            'frequency' => '',
        ];

        if ($station) {
            $dataA = $this->mapData($mapping, array_merge(
                $mainStationA->attributes,
                $mainStationA->getStationTypeObject()->attributes,
                $this->stationAData ?? []
            ));
            $dataB = $this->mapData($mapping, array_merge(
                $mainStationB->attributes,
                $mainStationB->getStationTypeObject()->attributes,
                $this->stationBData ?? []
            ));
        } else {
            $dataA = $this->mapData($mapping, $this->stationAData);
            $dataB = $this->mapData($mapping, $this->stationBData);
        }

        // Set serial, if MAC is not filled
        if (!$dataA['mac_address']) {
            $fsPair->stationA->hardware_identifier = Station::SERIAL_NUMBER;
        }
        if (!$dataB['mac_address']) {
            $fsPair->stationB->hardware_identifier = Station::SERIAL_NUMBER;
        }

        $data = [
            Station::FORM_NAME => [
                Station::POSITION_A => $dataA,
                Station::POSITION_B => $dataB,
            ],
        ];

        $dataFs = [
            StationFs::FORM_NAME => [
                Station::POSITION_A => [
                    'frequency' => $dataA['frequency'] ?? '',
                    'ratio_signal_interference' => $this->getClosestCI($dataA['ratio_signal_interference']
                        ??
                        ''),
                ],
                Station::POSITION_B => [
                    'frequency' => $dataB['frequency'] ?? '',
                    'ratio_signal_interference' => $this->getClosestCI($dataB['ratio_signal_interference']
                        ??
                        ''),
                ],
            ],
        ];

        // User
        if (!$station) {
            $fsPair->setUser(loggedUser()->id);
        }

        foreach ([
                     StationController::STEP_1_NAME,
                     StationController::STEP_2_LOCATION,
                     StationController::STEP_3_PARAMS,
                 ]
                 as $step) {

            $fsPair->setStep($step);

            if (!$fsPair->setData($data) || !$fsPair->save()) {
                if (!isset($this->errors['A'])) {
                    $this->errors['A'] = [];
                }
                if (!isset($this->errors['B'])) {
                    $this->errors['B'] = [];
                }

                $this->errors['A'][] = ($fsPair->stationA->hasErrors()) ? $fsPair->stationA->getErrors() :
                    [_tF('pair_station_errors', 'user-import')];
                $this->errors['B'][] = ($fsPair->stationB->hasErrors()) ? $fsPair->stationB->getErrors() :
                    [_tF('pair_station_errors', 'user-import')];
                if ($step > StationController::STEP_1_NAME) {
                    Station::deleteStation($fsPair->stationA->id);
                }
                return null;
            }

            if ($step == StationController::STEP_3_PARAMS &&
                (!$fsPair->setFsData($dataFs, $step) || !$fsPair->FsSave())) {
                if (!isset($this->errors['A'])) {
                    $this->errors['A'] = [];
                }
                if (!isset($this->errors['B'])) {
                    $this->errors['B'] = [];
                }

                $this->errors['A'][] = ($fsPair->stationA->getStationTypeObject()->hasErrors()) ?
                    $fsPair->stationA->getStationTypeObject()->getErrors() :
                    [_tF('pair_station_errors', 'user-import')];
                $this->errors['B'][] = ($fsPair->stationB->getStationTypeObject()->hasErrors()) ?
                    $fsPair->stationB->getStationTypeObject()->getErrors() :
                    [_tF('pair_station_errors', 'user-import')];
                Station::deleteStation($fsPair->stationA->id);
                return null;
            }
        }

        $fsPair->stationA->moveToWaiting();
        $fsPair->stationA->storeStationProgress(StationController::STEP_4_SUMMARY);
        $fsPair->stationB->storeStationProgress(StationController::STEP_4_SUMMARY);

        return $fsPair->stationA;
    }

    /**
     * @param $ciOrig
     *
     * @return mixed|null
     */
    protected function getClosestCI($ciOrig)
    {
        $ciValues = array_keys(StationFs::$qamValues);
        $ci = min($ciValues);

        try {
            if ((int)($ciOrig)) {
                $closest = null;
                foreach ($ciValues as $ciValue) {
                    if ($closest === null || abs($ciOrig - $closest) > abs($ciValue - $ciOrig)) {
                        $closest = $ciValue;
                    }
                }
                $ci = $closest;
            }
            return $ci;
        } catch (\Exception $e) {
            // If number evaluation collapse, use minimal value
            return min($ciValues);
        }
    }

    protected function mapData($mapping, $stationData)
    {
        $out = [];
        foreach ($mapping as $attribute => $void) {
            // Have to remove spaces on the end of numbers
            $value = $stationData[$attribute]??null;
            if (is_string($value)) {
                $value = trim($value);
            }

            $out[$attribute] = $value;

            if ($attribute == 'direction') {
                if ($out[$attribute] == 360) {
                    $out[$attribute] = 0;
                }
            }
        }

        self::ensureMacAddressAndSerialNumberBothAttrs($out);

        return $out;
    }

    /**
     * Count decimal GPS from columns
     *
     *
     * @return array
     */
    protected function getGPSFromData($stationInputArray)
    {
        $lat = $stationInputArray['lat'] ?? null;
        $lng = $stationInputArray['lng'] ?? null;

        if (!$lat || !$lng) {
            return [];
        }

        return [$lat, $lng];
    }

    /**
     * Translate type
     *
     * @param $type
     *
     * @return string|null
     */
    protected function getType($type)
    {
        if (!array_key_exists(mb_strtolower($type), self::$typeMapping)) {
            return $type;
        }
        return self::$typeMapping[mb_strtolower($type)];
    }
}