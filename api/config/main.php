<?php
/*
* Copyright 2023 DactylGroup s.r.o.
*
* Licensed under the EUPL, Version 1.2 or – as soon they
will be approved by the European Commission - subsequent
versions of the EUPL (the "Licence");
* You may not use this work except in compliance with the
Licence.
* You may obtain a copy of the Licence at:
*
* https://joinup.ec.europa.eu/software/page/eupl5
*
* Unless required by applicable law or agreed to in
writing, software distributed under the Licence is
distributed on an "AS IS" basis,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
express or implied.
* See the Licence for the specific language governing
permissions and limitations under the Licence.
*/
?>


<?php

use dactylcore\core\web\RestUrlRule;
use yii\web\Response;

$apiConfig = [
    'params' => require __DIR__ . '/params.php',
    'bootstrap' => [
        'v1/dc-system',
        'v1/dc-user',
    ],
    'modules' => [
        'v1' => [
            'class' => api\versions\v1\Module::class,
            'modules' => [
                'dc-system' => [
                    'class' => dactylcore\system\api\versions\v1\Module::class,
                ],
                'dc-user' => [
                    'class' => modules\user\api\versions\v2\Module::class,
                ],
            ],
        ],
    ],
    'components' => [
        'session' => [
            'name' => 'app',
        ],
        'request' => [
            'parsers' => [
                'multipart/form-data' => \yii\web\MultipartFormDataParser::class,
                'application/json' => yii\web\JsonParser::class,
            ],
        ],
        'response' => [
            'on beforeSend' => function ($event)
            {
                // change response only if JSON
                if (in_array(\Yii::$app->response->format, [Response::FORMAT_JSON, Response::FORMAT_JSONP])) {
                    $response = $event->sender;

                    $data = [
                        'status' => $response->code ?? $response->statusCode,
                    ];

                    if (($response->statusCode >= 200 && $response->statusCode <= 210)) {
                        $data['data'] = $response->data;

                        if ($response->data['warning'] ?? null) {
                            $data['warning'] = $response->data['warning'];
                            if (isset($data['data']['warning'])) {
                                unset($data['data']['warning']);
                            }
                        }
                    } else {
                        $data['error'] = $response->data['message'] ?? $response->statusText;
                    }

                    if ($response->data['errorData'] ?? null) {
                        $data['errorData'] = $response->data['errorData'];
                    }
                    if ($response->data['warningData'] ?? null) {
                        $data['warningData'] = $response->data['warningData'];
                        if (isset($data['data']['warningData'])) {
                            unset($data['data']['warningData']);
                        }
                    }

                    if ($response->statusCode == 422) {
                        $data['attributeErrors'] = $response->data;
                    }

                    if ($response->statusCode == 500) {
                        $data['error'] = _t('flash_error');

                        if (YII_ENV_DEV) {
                            $data['exception'] = $response->data;
                        }
                    }

                    if (isset($data['data'][0]) && $data['data'][0] === null) {
                        unset($data['data']);
                    }

                    $response->data = $data;
                }
            },
        ],
        'swaggerDocs' => [
            'class' => '',
            'v1' => [
                'includeDirs' => [
                    '@dc-user/api/versions/v1',
                    '@dc-user/api/versions/v2',
                    '@modules/dc-user/api/versions/v2',
                ],
                'excludeDirs' => [
                    '@dc-user/api/versions/v2/controllers/UserRoleController.php',
                    '@dc-user/api/versions/v2/controllers/UsersController.php',
                ],
            ],
        ],
        'user' => [
            'class' => \dactylcore\user\common\components\UserComponent::class,
            'identityClassVersions' => [
                'v1' => dactylcore\user\api\versions\v2\models\User::class,
            ],
        ],
    ],
];

return $apiConfig;