<?php
/*
* Copyright 2023 DactylGroup s.r.o.
*
* Licensed under the EUPL, Version 1.2 or – as soon they
will be approved by the European Commission - subsequent
versions of the EUPL (the "Licence");
* You may not use this work except in compliance with the
Licence.
* You may obtain a copy of the Licence at:
*
* https://joinup.ec.europa.eu/software/page/eupl5
*
* Unless required by applicable law or agreed to in
writing, software distributed under the Licence is
distributed on an "AS IS" basis,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
express or implied.
* See the Licence for the specific language governing
permissions and limitations under the Licence.
*/
?>


<?php

namespace api\versions\v1;

use Codeception\Util\HttpCode;
use modules\user\api\versions\v2\tests\api\ApiTester;
use modules\user\common\models\User;

class UserAllianceCest
{
    protected $time;
    protected static $accessToken = null;
    protected static $userData
        = [
            'first_name' => 'Jan',
            'last_name' => 'Novak',
            'street' => 'Mala 18',
            'city' => 'Brno',
            'zipCode' => '63500',
            'email' => 'tester1@dactylgroup.com',
            'password' => 'MirkoAneta7777?',
            'conditionsConfirmed' => true,
        ];
    protected static $data
        = [
            'name' => 'Testovaci zdruzeni 1',
            'description' => 'Super skupina...',
        ];

    /**
     * @param ApiTester $I
     */
    protected function haveAccessTokenInHeader($I, $userLoginData = null)
    {
        if ($userLoginData) {
            self::$accessToken = null;
        }

        if (!self::$accessToken) {
            if (!$userLoginData) {
                $userLoginData = self::$userData;
            }

            if (!User::findOne(['email' => self::$userData['email']])) {
                $I->haveRecord(User::class, merge(self::$userData, [
                    'password_hash' => '$2y$13$06ce2nIgxB4s7AM7nymX0u7xZJFbRpKQJJQ0npeZ1MhouGU6OXhc2',
                    'auth_key' => 'nwifFVoE1OEzkYLJdo3fhwp2ETEM6Wxa',
                    'role' => 3,
                    'created_at' => time(),
                    'updated_at' => time(),
                ]));
            }

            \Yii::$app->db->createCommand()
                          ->update('user', [
                              'email_verification_token' => null,
                              'can_use_api' => true
                          ], ['email' => $userLoginData['email']])
                          ->execute();
            $I->sendPOST('/user/login', [
                'email' => $userLoginData['email'],
                'password' => $userLoginData['password'],
            ]);
            $response = json_decode($I->grabResponse());
            self::$accessToken = $response->data->access_token->token;
        }

        $I->haveHttpHeader('access-token', self::$accessToken);
    }

    /**
     * @param ApiTester $I
     */
    public function create($I)
    {
        $this->haveAccessTokenInHeader($I);

        $I->sendPOST('/user-alliance', self::$data);
        $I->seeResponseContainsJson(['status' => '200']);
    }

    /**
     * @param ApiTester $I
     */
    public function view($I)
    {
        $this->haveAccessTokenInHeader($I);

        $I->sendGET('/user-alliance');
        $I->seeResponseContainsJson(['status' => '200']);
        $I->seeResponseContainsJson(['name' => self::$data['name']]);
    }

    /**
     * @param ApiTester $I
     */
    public function update($I)
    {
        $this->haveAccessTokenInHeader($I);

        $I->sendPATCH('/user-alliance', ['name' => self::$data['name'].' NEW']);
        $I->seeResponseContainsJson(['status' => '200']);
        $I->seeResponseContainsJson(['name' => self::$data['name'].' NEW']);
    }

    /**
     * @param ApiTester $I
     */
    public function invite($I)
    {
        $this->haveAccessTokenInHeader($I);

        $I->sendPOST('/user-alliance/invite/3');
        $I->seeResponseContainsJson(['status' => '200']);
    }

    /**
     * @param ApiTester $I
     */
    public function promote($I)
    {
        $this->haveAccessTokenInHeader($I);

        $I->sendPATCH('/user-alliance/promote/3');
        $I->seeResponseContainsJson(['status' => '200']);
    }

    /**
     * @param ApiTester $I
     */
    public function degrade($I)
    {
        $this->haveAccessTokenInHeader($I);

        $I->sendPATCH('/user-alliance/degrade/3');
        $I->seeResponseContainsJson(['status' => '200']);
    }

    /**
     * @param ApiTester $I
     */
    public function exclude($I)
    {
        $this->haveAccessTokenInHeader($I);

        $I->sendPATCH('/user-alliance/exclude/3');
        $I->seeResponseContainsJson(['status' => '200']);
    }

    /**
     * @param ApiTester $I
     */
    public function delete($I)
    {
        $this->haveAccessTokenInHeader($I);

        $I->sendDELETE('/user-alliance');
        $I->seeResponseContainsJson(['status' => '200']);
    }
}
