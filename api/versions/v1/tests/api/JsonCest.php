<?php
/*
* Copyright 2023 DactylGroup s.r.o.
*
* Licensed under the EUPL, Version 1.2 or – as soon they
will be approved by the European Commission - subsequent
versions of the EUPL (the "Licence");
* You may not use this work except in compliance with the
Licence.
* You may obtain a copy of the Licence at:
*
* https://joinup.ec.europa.eu/software/page/eupl5
*
* Unless required by applicable law or agreed to in
writing, software distributed under the Licence is
distributed on an "AS IS" basis,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
express or implied.
* See the Licence for the specific language governing
permissions and limitations under the Licence.
*/
?>


<?php

namespace api\versions\v1;

class JsonCest
{
    protected static $accessToken = null;

    /**
     * log in before action
     * @param ApiTester $I
     */
    protected function login(ApiTester $I)
    {
        if (!self::$accessToken) {
            $I->sendPOST('/user/login', ['email' => 'admin@dactylgroup.com', 'password' => '111111']);
            $response = json_decode($I->grabResponse());
            self::$accessToken = $response['data']['user']['access_token'];
        }

        $I->haveHttpHeader('access-token', self::$accessToken);
    }

    public function _before(ApiTester $I)
    {

    }

    // tests
    public function Json(ApiTester $I)
    {
        $I->sendGET('/docs/json');
        $I->seeResponseCodeIs(200); // 200
        $I->seeResponseIsJson();

        $I->seeResponseContainsJson([
            'openapi' => '3.0.0'
        ]);
        $I->seeResponseMatchesJsonType([
            'openapi' => 'string',
            'info' => 'array',
            'servers' => 'array',
            'paths' => 'array',
            'components' => 'array',
        ]);
    }
}
