<?php
/*
* Copyright 2023 DactylGroup s.r.o.
*
* Licensed under the EUPL, Version 1.2 or – as soon they
will be approved by the European Commission - subsequent
versions of the EUPL (the "Licence");
* You may not use this work except in compliance with the
Licence.
* You may obtain a copy of the Licence at:
*
* https://joinup.ec.europa.eu/software/page/eupl5
*
* Unless required by applicable law or agreed to in
writing, software distributed under the Licence is
distributed on an "AS IS" basis,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
express or implied.
* See the Licence for the specific language governing
permissions and limitations under the Licence.
*/
?>


<?php

namespace api\versions\v1;

use common\models\Message;
use common\models\Station;
use common\models\Station52;
use common\models\Thread;
use dactylcore\core\test\apiTester\ApiTester;
use dactylcore\user\api\versions\v1\models\UserAccessToken;

class ThreadCest
{
    protected static string $accessToken = 'TEST_ACCESS_TOKEN';
    protected int $idStation;
    protected int $idThread;

    protected function createAccessToken(ApiTester $I)
    {
        $I->haveRecord(UserAccessToken::class, [
            'id_user' => 2,
            'access_token' => self::$accessToken,
            'expires' => time() + 1000,
            'deleted' => 0,
        ]);
    }

    protected function testErrorResponse(ApiTester $I, int $statusCode)
    {
        $I->seeResponseCodeIs($statusCode);
        $I->seeResponseIsJson();
        $I->seeResponseMatchesJsonType([
            'status' => 'integer',
            'error' => 'string',
        ]);
    }

    /**
     * @param ApiTester $I
     */
    public function missingAccessToken(ApiTester $I)
    {
        $I->sendGet('/thread');
        $this->testErrorResponse($I, 400);
        $I->sendGet('/thread/1');
        $this->testErrorResponse($I, 400);
        $I->sendPost('/thread');
        $this->testErrorResponse($I, 400);
        $I->sendPut('/thread/1');
        $this->testErrorResponse($I, 400);
        $I->sendGet('/thread/1/message');
        $this->testErrorResponse($I, 400);
        $I->sendPost('/thread/1/message');
        $this->testErrorResponse($I, 400);
    }

    /**
     * @param ApiTester $I
     */
    public function badAccessToken(ApiTester $I)
    {
        $I->haveHttpHeader('access-token', 'BAD_ACCESS_TOKEN');
        $I->sendGet('/thread');
        $this->testErrorResponse($I, 401);
        $I->sendGet('/thread/1');
        $this->testErrorResponse($I, 401);
        $I->sendPost('/thread');
        $this->testErrorResponse($I, 401);
        $I->sendPut('/thread/1');
        $this->testErrorResponse($I, 401);
        $I->sendGet('/thread/1/message');
        $this->testErrorResponse($I, 401);
        $I->sendPost('/thread/1/message');
        $this->testErrorResponse($I, 401);
    }

    /**
     * @param ApiTester $I
     */
    public function badMethodTypes(ApiTester $I)
    {
        $I->sendPut('/thread');
        $this->testErrorResponse($I, 405);
        $I->sendPatch('/thread');
        $this->testErrorResponse($I, 405);
        $I->sendDelete('/thread');
        $this->testErrorResponse($I, 405);
        $I->sendDelete('/thread/1');
        $this->testErrorResponse($I, 405);
        $I->sendPut('/thread/1/message');
        $this->testErrorResponse($I, 404);
        $I->sendDelete('/thread/1/message');
        $this->testErrorResponse($I, 404);
    }

    /**
     * @param ApiTester $I
     *
     * @throws \Exception
     */
    public function list(ApiTester $I)
    {
        $this->idStation = $I->haveRecord(Station::class, [
            'id_user' => 3,
            'type' => \frontend\controllers\StationController::TYPE_52,
            'lng' => 18.45600465924360,
            'lat' => 49.89107052817137,
            'channel_width' => 80,
            'registered_at' => 1578488633,
            'valid_to' => 1610111033,
            'protected_to' => 1625745833,
            'status' => 'finished',
            'deleted' => 0,
            'name' => 'TEST_STANICE',
            'published_by' => 'publication',
            'hardware_identifier' => 1,
            'mac_address' => 'AA:00:00:00:00:00',
            'serial_number' => 'TEST_SERIAL',
            'macAddress' => 'AA:00:00:00:00:00',
            'serialNumber' => 'TEST_SERIAL',
        ]);

        $I->haveRecord(Station52::class, [
            'id_station' => $this->idStation,
            'deleted' => 0,
        ]);

        $idThread = $I->haveRecord(Thread::class, [
            'id_recipient' => 2,
            'id_sender' => 1,
            'id_station' => $this->idStation,
            'subject' => "TEST_SUBJECT",
            'show_sender_info' => false,
            'show_recipient_info' => false,
            'deleted' => 0,
            'reason' => Thread::REASON_GENERIC,
            'status_sender' => Thread::STATUS_READ,
            'status_recipient' => Thread::STATUS_NEW,
        ]);

        $this->createAccessToken($I);
        $I->haveHttpHeader('access-token', self::$accessToken);
        $I->sendGet('/thread');
        $I->seeResponseCodeIs(200);
        $I->seeResponseIsJson();
        $I->seeResponseMatchesJsonType([
            'status' => 'integer',
            'data' => [
                'items' => 'array',
            ],
        ]);
        $items = json_decode($I->grabResponse(), true);
        $items = $items['data']['items'];

        $I->assertNotEmpty($items);
        foreach ($items as $item) {
            $I->assertArrayHasKey('id', $item);
            $I->assertInternalType('integer', $item['id']);
            $I->assertArrayHasKey('recipient', $item);
            $I->assertInternalType('string', $item['recipient']);
            $I->assertArrayHasKey('sender', $item);
            $I->assertInternalType('string', $item['sender']);
            $I->assertArrayHasKey('id_station', $item);
            $I->assertInternalType('integer', $item['id_station']);
            $I->assertArrayHasKey('subject', $item);
            $I->assertInternalType('string', $item['subject']);
            $I->assertArrayHasKey('id_station_disturber', $item);
        }

        Thread::findOne($idThread)->hardDelete();
    }

    public function create(ApiTester $I)
    {
        $this->createAccessToken($I);
        $I->haveHttpHeader('access-token', self::$accessToken);
        $I->sendPost('/thread', [
            'id_station' => $this->idStation,
        ]);
        $I->seeResponseCodeIs(201);
        $I->seeResponseIsJson();
        $I->seeResponseMatchesJsonType([
            'status' => 'integer',
            'data' => [
                'id' => 'integer',
                'id_station' => 'integer',
                'sender' => 'string',
                'recipient' => 'string',
                'subject' => 'string',
                'created_at' => 'integer',
                'updated_at' => 'integer',
            ],
        ]);
        $this->idThread = json_decode($I->grabResponse(), true)['data']['id'];
    }

    public function view(ApiTester $I)
    {
        $this->createAccessToken($I);
        $I->haveHttpHeader('access-token', self::$accessToken);
        $I->sendGet("/thread/{$this->idThread}");
        $I->seeResponseCodeIs(200);
        $I->seeResponseIsJson();
        $I->seeResponseMatchesJsonType([
            'status' => 'integer',
            'data' => [
                'id' => 'integer',
                'id_station' => 'integer',
                'sender' => 'string',
                'recipient' => 'string',
                'subject' => 'string',
                'created_at' => 'integer',
                'updated_at' => 'integer',
            ],
        ]);
        $item = json_decode($I->grabResponse(), true);
        $I->assertEquals($this->idThread, $item['data']['id']);
    }

    /**
     * I cannot send message about station, that is not mine
     *
     * @param ApiTester $I
     */
    public function createNotMyStation(ApiTester $I)
    {
        $idStation = $I->haveRecord(Station::class, [
            'id_user' => 1,
            'type' => \frontend\controllers\StationController::TYPE_52,
            'lng' => 18.45600465924360,
            'lat' => 49.89107052817137,
            'channel_width' => 80,
            'registered_at' => 1578488633,
            'valid_to' => 1610111033,
            'protected_to' => 1625745833,
            'status' => 'finished',
            'deleted' => 0,
            'name' => 'TEST_STANICE',
            'published_by' => 'publication',
            'hardware_identifier' => 1,
            'mac_address' => 'AA:BB:CC:00:00:00',
            'serial_number' => 'TEST_SERIAL',
            'macAddress' => 'AA:BB:CC:00:00:00',
            'serialNumber' => 'TEST_SERIAL',
        ]);

        $I->haveRecord(Station52::class, [
            'id_station' => $idStation,
            'deleted' => 0,
        ]);

        $this->createAccessToken($I);
        $I->haveHttpHeader('access-token', self::$accessToken);
        $I->sendPost('/thread', [
            'id_station' => $idStation,
            'id_station_disturber' => $this->idStation,
        ]);
        $I->seeResponseCodeIs(422);
        $I->seeResponseIsJson();
        $I->seeResponseMatchesJsonType([
            'status' => 'integer',
            'error' => 'string',
        ]);

        Station52::findOne(['id_station' => $idStation])->hardDelete();
        Station::findOne($idStation)->hardDelete();
    }

    /**
     * I cannot send message to myself
     *
     * @param ApiTester $I
     */
    public function createOnMyStation(ApiTester $I)
    {
        $idStation = $I->haveRecord(Station::class, [
            'id_user' => 2,
            'type' => \frontend\controllers\StationController::TYPE_52,
            'lng' => 18.45600425924360,
            'lat' => 49.89107042817137,
            'channel_width' => 80,
            'registered_at' => 1578488633,
            'valid_to' => 1610111033,
            'protected_to' => 1625745833,
            'status' => 'finished',
            'deleted' => 0,
            'name' => 'TEST_STANICE',
            'published_by' => 'publication',
            'hardware_identifier' => 1,
            'mac_address' => 'AA:FF:CC:00:00:00',
            'serial_number' => 'TEST_SERIAL',
            'macAddress' => 'AA:FF:CC:00:00:00',
            'serialNumber' => 'TEST_SERIAL',
        ]);

        $I->haveRecord(Station52::class, [
            'id_station' => $idStation,
            'deleted' => 0,
        ]);

        $this->createAccessToken($I);
        $I->haveHttpHeader('access-token', self::$accessToken);
        $I->sendPost('/thread', [
            'id_station' => $idStation,
        ]);
        $I->seeResponseCodeIs(422);
        $I->seeResponseIsJson();
        $I->seeResponseMatchesJsonType([
            'status' => 'integer',
            'error' => 'string',
        ]);

        Station52::findOne(['id_station' => $idStation])->hardDelete();
        Station::findOne($idStation)->hardDelete();
    }

    public function createNoData(ApiTester $I)
    {
        $this->createAccessToken($I);
        $I->haveHttpHeader('access-token', self::$accessToken);
        $I->sendPost('/thread');
        $I->seeResponseCodeIs(422);
        $I->seeResponseIsJson();
        $I->seeResponseMatchesJsonType([
            'status' => 'integer',
            'error' => 'string',
        ]);
    }

    public function update(ApiTester $I)
    {
        $this->createAccessToken($I);
        $I->haveHttpHeader('access-token', self::$accessToken);
        $I->sendPut("/thread/{$this->idThread}", [
            'showMyInfo' => true,
        ]);
        $I->seeResponseCodeIs(200);
        $I->seeResponseIsJson();
        $I->seeResponseMatchesJsonType([
            'status' => 'integer',
            'data' => [
                'id' => 'integer',
                'id_station' => 'integer',
                'sender' => 'string',
                'recipient' => 'string',
                'subject' => 'string',
                'created_at' => 'integer',
                'updated_at' => 'integer',
            ],
        ]);
        $item = json_decode($I->grabResponse(), true);
        $I->assertEquals($this->idThread, $item['data']['id']);
    }

    // Message create

    public function createMessage(ApiTester $I)
    {
        $testContent = 'TEST MESSAGE CONTENT';
        $this->createAccessToken($I);
        $I->haveHttpHeader('access-token', self::$accessToken);
        $I->sendPost("/thread/{$this->idThread}/message", [
            'content' => $testContent,
        ]);
        $I->seeResponseCodeIs(201);
        $I->seeResponseIsJson();
        $I->seeResponseMatchesJsonType([
            'status' => 'integer',
            'data' => [
                'items' => 'array',
            ],
        ]);
        $items = json_decode($I->grabResponse(), true);
        $items = $items['data']['items'];

        $I->assertNotEmpty($items);
        foreach ($items as $item) {
            $I->assertArrayHasKey('id', $item);
            $I->assertInternalType('integer', $item['id']);
            $I->assertArrayHasKey('id_author', $item);
            $I->assertInternalType('integer', $item['id_author']);
            $I->assertArrayHasKey('author', $item);
            $I->assertInternalType('string', $item['author']);
            $I->assertArrayHasKey('content', $item);
            $I->assertInternalType('string', $item['content']);
        }
        $I->assertEquals($testContent, $items[0]['content']);
    }

    // Message index
    public function listMessage(ApiTester $I)
    {
        $this->createAccessToken($I);
        $I->haveHttpHeader('access-token', self::$accessToken);
        $I->sendGet("/thread/{$this->idThread}/message");
        $I->seeResponseCodeIs(200);
        $I->seeResponseIsJson();
        $I->seeResponseMatchesJsonType([
            'status' => 'integer',
            'data' => [
                'items' => 'array',
            ],
        ]);
        $items = json_decode($I->grabResponse(), true);
        $items = $items['data']['items'];

        $I->assertNotEmpty($items);
        foreach ($items as $item) {
            $I->assertArrayHasKey('id', $item);
            $I->assertInternalType('integer', $item['id']);
            $I->assertArrayHasKey('id_author', $item);
            $I->assertInternalType('integer', $item['id_author']);
            $I->assertArrayHasKey('author', $item);
            $I->assertInternalType('string', $item['author']);
            $I->assertArrayHasKey('content', $item);
            $I->assertInternalType('string', $item['content']);

            Message::findOne($item['id'])->hardDelete();
        }

        // Delete all?
        Thread::findOne($this->idThread)->hardDelete();
        Station52::findOne(['id_station' => $this->idStation])->hardDelete();
        Station::findOne($this->idStation)->hardDelete();
    }
}
