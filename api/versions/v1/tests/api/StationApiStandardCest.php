<?php
/*
* Copyright 2023 DactylGroup s.r.o.
*
* Licensed under the EUPL, Version 1.2 or – as soon they
will be approved by the European Commission - subsequent
versions of the EUPL (the "Licence");
* You may not use this work except in compliance with the
Licence.
* You may obtain a copy of the Licence at:
*
* https://joinup.ec.europa.eu/software/page/eupl5
*
* Unless required by applicable law or agreed to in
writing, software distributed under the Licence is
distributed on an "AS IS" basis,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
express or implied.
* See the Licence for the specific language governing
permissions and limitations under the Licence.
*/
?>


<?php

namespace api\versions\v1;

use api\versions\v1\models\Station;
use dactylcore\core\test\apiTester\ApiTester;
use dactylcore\user\api\versions\v1\models\UserAccessToken;

class StationApiStandardCest
{
    protected static string $accessToken = 'TEST_ACCESS_TOKEN_STATION';
    protected int $idStation;
    protected array $resourceData;
    protected array $stations
        = [
            'fsa' => null,
            'fsb' => null,
            'wig' => null,
            '58a' => null,
            '58t' => null,
            '52' => null,
        ];

    public function init()
    {
        $this->resourceData = require __DIR__ . '/../fixtures/allStation.php';
    }

    protected function createAccessToken(ApiTester $I)
    {
        $I->haveRecord(UserAccessToken::class, [
            'id_user' => 2,
            'access_token' => self::$accessToken,
            'expires' => time() + 1000,
            'deleted' => 0,
        ]);
    }

    protected function testErrorResponse(ApiTester $I, int $statusCode)
    {
        $I->seeResponseCodeIs($statusCode);
        $I->seeResponseIsJson();
        $I->seeResponseMatchesJsonType([
            'status' => 'integer',
            'error' => 'string',
        ]);
    }

    /**
     * @param ApiTester $I
     */
    public function missingAccessToken(ApiTester $I)
    {
        $I->sendGet('/station');
        $this->testErrorResponse($I, 400);
        $I->sendGet('/station/1');
        $this->testErrorResponse($I, 400);
        $I->sendPost('/station');
        $this->testErrorResponse($I, 400);
        $I->sendPatch('/station/1');
        $this->testErrorResponse($I, 400);
        $I->sendDELETE('/station/1');
        $this->testErrorResponse($I, 400);
    }

    /**
     * @param ApiTester $I
     */
    public function badAccessToken(ApiTester $I)
    {
        $I->haveHttpHeader('access-token', 'BAD_ACCESS_TOKEN');
        $I->sendGet('/station');
        $this->testErrorResponse($I, 401);
        $I->sendGet('/station/1');
        $this->testErrorResponse($I, 401);
        $I->sendPost('/station');
        $this->testErrorResponse($I, 401);
        $I->sendPatch('/station/1');
        $this->testErrorResponse($I, 401);
        $I->sendDELETE('/station/1');
        $this->testErrorResponse($I, 401);
    }

    /**
     * @param ApiTester $I
     */
    public function badMethodTypes(ApiTester $I)
    {
        $I->sendPut('/station');
        $this->testErrorResponse($I, 405);
        $I->sendPatch('/station');
        $this->testErrorResponse($I, 405);
        $I->sendDelete('/station');
        $this->testErrorResponse($I, 405);
        $I->sendPost('/station/1');
        $this->testErrorResponse($I, 400);
        $I->sendPut('/station/1');
        $this->testErrorResponse($I, 400);
    }

    // CREATE

    public function createFs(ApiTester $I)
    {
        $this->createAccessToken($I);
        $I->haveHttpHeader('access-token', self::$accessToken);
        $I->sendPost('/station', $this->resourceData['fs']);
        $I->seeResponseCodeIs(200);
        $I->seeResponseIsJson();
        $I->seeResponseMatchesJsonType($this->resourceData['typeFs']);
        $this->stations['fsa'] = json_decode($I->grabResponse(), true)['data']['stationA']['id'];
        $this->stations['fsb'] = json_decode($I->grabResponse(), true)['data']['stationB']['id'];
    }

    public function createWigig(ApiTester $I)
    {
        $this->createAccessToken($I);
        $I->haveHttpHeader('access-token', self::$accessToken);
        $I->sendPost('/station', $this->resourceData['wigig']);
        $I->seeResponseCodeIs(200);
        $I->seeResponseIsJson();
        $I->seeResponseMatchesJsonType($this->resourceData['typeWigig']);
        $this->stations['wig'] = json_decode($I->grabResponse(), true)['data']['id'];
    }

    public function createAccessPoint(ApiTester $I)
    {
        $this->createAccessToken($I);
        $I->haveHttpHeader('access-token', self::$accessToken);
        $I->sendPost('/station', $this->resourceData['ap']);
        $I->seeResponseCodeIs(200);
        $I->seeResponseIsJson();
        $I->seeResponseMatchesJsonType($this->resourceData['type58']);
        $this->stations['58a'] = json_decode($I->grabResponse(), true)['data']['id'];
    }

    public function createTollGate(ApiTester $I)
    {
        $this->createAccessToken($I);
        $I->haveHttpHeader('access-token', self::$accessToken);
        $I->sendPost('/station', $this->resourceData['tg']);
        $I->seeResponseCodeIs(200);
        $I->seeResponseIsJson();
        $I->seeResponseMatchesJsonType($this->resourceData['type58']);
        $this->stations['58t'] = json_decode($I->grabResponse(), true)['data']['id'];
    }

    public function create52(ApiTester $I)
    {
        $this->createAccessToken($I);
        $I->haveHttpHeader('access-token', self::$accessToken);
        $I->sendPost('/station', $this->resourceData['52']);
        $I->seeResponseCodeIs(200);
        $I->seeResponseIsJson();
        $I->seeResponseMatchesJsonType($this->resourceData['type52']);
        $this->stations['52'] = json_decode($I->grabResponse(), true)['data']['id'];
    }

    /**
     * @param ApiTester $I
     *
     * @throws \Exception
     */
    public function list(ApiTester $I)
    {
        $this->createAccessToken($I);
        $I->haveHttpHeader('access-token', self::$accessToken);
        $I->sendGet('/station');
        $I->seeResponseCodeIs(200);
        $I->seeResponseIsJson();
        $I->seeResponseMatchesJsonType([
            'status' => 'integer',
            'data' => [
                'items' => 'array',
            ],
        ]);
        $items = json_decode($I->grabResponse(), true);
        $items = $items['data']['items'];

        $I->assertNotEmpty($items);
        foreach ($items as $item) {

            if (isset($item['stationA'])) {
                // It's FS
                $this->checkStationType($I, $item['stationA']);
                $this->checkStationType($I, $item['stationB']);

                $I->assertArrayHasKey('ratio_signal_interference', $item['stationA']);
                $I->assertArrayHasKey('frequency', $item['stationA']);
                $I->assertArrayHasKey('ratio_signal_interference', $item['stationB']);
                $I->assertArrayHasKey('frequency', $item['stationB']);
            } elseif (isset($item['station'])) {
                $this->checkStationType($I, $item['station']);
                switch ($item['station']['type']) {
                    case 'wigig':
                        $I->assertArrayHasKey('direction', $item['station']);
                        $I->assertArrayHasKey('eirp', $item['station']);
                        break;
                    case 'wifi_5_8':
                        $I->assertArrayHasKey('is_ap', $item['station']);
                        break;
                    case 'wifi_5_2':
                        break;
                    default:
                        $I->assertTrue(false, "Station has wrong type: {$item['station']['type']}");
                }
            }
        }
    }

    /**
     * Function for checking type of station structure, that is same for all station types
     *
     * @param ApiTester $I
     * @param array $item
     */
    protected function checkStationType(ApiTester $I, array $item)
    {
        $I->assertArrayHasKey('id', $item);
        $I->assertInternalType('integer', $item['id']);
        $I->assertArrayHasKey('id_user', $item);
        $I->assertInternalType('integer', $item['id_user']);
        $I->assertArrayHasKey('type', $item);
        $I->assertInternalType('string', $item['type']);
        $I->assertArrayHasKey('name', $item);
        $I->assertInternalType('string', $item['name']);
        $I->assertArrayHasKey('lng', $item);
        $I->assertInternalType('string', $item['lng']);
        $I->assertArrayHasKey('lat', $item);
        $I->assertInternalType('string', $item['lat']);
        $I->assertArrayHasKey('status', $item);
        $I->assertInternalType('string', $item['status']);
        $I->assertArrayHasKey('registered_at', $item);
        $I->assertInternalType('integer', $item['registered_at']);
        $I->assertArrayHasKey('valid_to', $item);
        $I->assertInternalType('integer', $item['valid_to']);
        $I->assertArrayHasKey('protected_to', $item);
        $I->assertInternalType('integer', $item['protected_to']);
        $I->assertArrayHasKey('waiting_api_changes_json', $item);
        $I->assertInternalType('array', $item['waiting_api_changes_json']);

        $I->assertArrayHasKey('antenna_volume', $item);
        $I->assertArrayHasKey('channel_width', $item);
        $I->assertArrayHasKey('mac_address', $item);
        $I->assertArrayHasKey('serial_number', $item);
        $I->assertArrayHasKey('published_by', $item);
        $I->assertArrayHasKey('power', $item);
        $I->assertArrayHasKey('created_at', $item);
        $I->assertArrayHasKey('updated_at', $item);
        $I->assertArrayHasKey('updated_at', $item);
    }

    // UPDATE

    public function updateFS(ApiTester $I)
    {
        $updatedName = 'TEST_FS_UPDATE';
        $this->createAccessToken($I);
        $I->haveHttpHeader('access-token', self::$accessToken);
        $I->sendPatch("/station/{$this->stations['fsa']}", [
            'stationA' => [
                'name' => $updatedName,
            ],
            'stationB' => [
                'name' => $updatedName,
            ],
        ]);
        $I->seeResponseCodeIs(200);
        $I->seeResponseIsJson();
        $I->seeResponseMatchesJsonType($this->resourceData['typeFs']);
        $item = json_decode($I->grabResponse(), true);
        $I->assertEquals($this->stations['fsa'], $item['data']['stationA']['id']);
        $I->assertEquals($this->stations['fsb'], $item['data']['stationB']['id']);
        $I->assertEquals($updatedName, $item['data']['stationA']['name']);
        $I->assertEquals($updatedName, $item['data']['stationB']['name']);
    }

    public function updateWigig(ApiTester $I)
    {
        $updatedName = 'TEST_WIGIG_UPDATE';
        $this->createAccessToken($I);
        $I->haveHttpHeader('access-token', self::$accessToken);
        $I->sendPatch("/station/{$this->stations['wig']}", [
            'station' => [
                'name' => $updatedName,
            ],
        ]);
        $I->seeResponseCodeIs(200);
        $I->seeResponseIsJson();
        $I->seeResponseMatchesJsonType($this->resourceData['typeWigig']);
        $item = json_decode($I->grabResponse(), true);
        $I->assertEquals($this->stations['wig'], $item['data']['id']);
        $I->assertEquals($updatedName, $item['data']['name']);
    }

    public function updateAccessPoint(ApiTester $I)
    {
        $updatedName = 'TEST_AP_UPDATE';
        $this->createAccessToken($I);
        $I->haveHttpHeader('access-token', self::$accessToken);
        $I->sendPatch("/station/{$this->stations['58a']}", [
            'station' => [
                'name' => $updatedName,
            ],
        ]);
        $I->seeResponseCodeIs(200);
        $I->seeResponseIsJson();
        $I->seeResponseMatchesJsonType($this->resourceData['type58']);
        $item = json_decode($I->grabResponse(), true);
        $I->assertEquals($this->stations['58a'], $item['data']['id']);
        $I->assertEquals($updatedName, $item['data']['name']);
    }

    public function updateTollGate(ApiTester $I)
    {
        $updatedName = 'TEST_TG_UPDATE';
        $this->createAccessToken($I);
        $I->haveHttpHeader('access-token', self::$accessToken);
        $I->sendPatch("/station/{$this->stations['58t']}", [
            'station' => [
                'name' => $updatedName,
            ],
        ]);
        $I->seeResponseCodeIs(200);
        $I->seeResponseIsJson();
        $I->seeResponseMatchesJsonType($this->resourceData['type58']);
        $item = json_decode($I->grabResponse(), true);
        $I->assertEquals($this->stations['58t'], $item['data']['id']);
        $I->assertEquals($updatedName, $item['data']['name']);
    }

    public function update52(ApiTester $I)
    {
        $updatedName = 'TEST_52_UPDATE';
        $this->createAccessToken($I);
        $I->haveHttpHeader('access-token', self::$accessToken);
        $I->sendPatch("/station/{$this->stations['52']}", [
            'station' => [
                'name' => $updatedName,
            ],
        ]);
        $I->seeResponseCodeIs(200);
        $I->seeResponseIsJson();
        $I->seeResponseMatchesJsonType($this->resourceData['type52']);
        $item = json_decode($I->grabResponse(), true);
        $I->assertEquals($this->stations['52'], $item['data']['id']);
        $I->assertEquals($updatedName, $item['data']['name']);
    }

    // VIEW

    public function viewFS(ApiTester $I)
    {
        $updatedName = 'TEST_FS_UPDATE';
        $this->createAccessToken($I);
        $I->haveHttpHeader('access-token', self::$accessToken);
        $I->sendGet("/station/{$this->stations['fsa']}");
        $I->seeResponseCodeIs(200);
        $I->seeResponseIsJson();
        $I->seeResponseMatchesJsonType($this->resourceData['typeFs']);
        $item = json_decode($I->grabResponse(), true);
        $I->assertEquals($this->stations['fsa'], $item['data']['stationA']['id']);
        $I->assertEquals($this->stations['fsb'], $item['data']['stationB']['id']);
        $I->assertEquals($updatedName, $item['data']['stationA']['name']);
        $I->assertEquals($updatedName, $item['data']['stationB']['name']);
    }

    public function viewWigig(ApiTester $I)
    {
        $updatedName = 'TEST_WIGIG_UPDATE';
        $this->createAccessToken($I);
        $I->haveHttpHeader('access-token', self::$accessToken);
        $I->sendGet("/station/{$this->stations['wig']}");
        $I->seeResponseCodeIs(200);
        $I->seeResponseIsJson();
        $I->seeResponseMatchesJsonType($this->resourceData['typeWigig']);
        $item = json_decode($I->grabResponse(), true);
        $I->assertEquals($this->stations['wig'], $item['data']['id']);
        $I->assertEquals($updatedName, $item['data']['name']);
    }

    public function viewAccessPoint(ApiTester $I)
    {
        $updatedName = 'TEST_AP_UPDATE';
        $this->createAccessToken($I);
        $I->haveHttpHeader('access-token', self::$accessToken);
        $I->sendGet("/station/{$this->stations['58a']}");
        $I->seeResponseCodeIs(200);
        $I->seeResponseIsJson();
        $I->seeResponseMatchesJsonType($this->resourceData['type58']);
        $item = json_decode($I->grabResponse(), true);
        $I->assertEquals($this->stations['58a'], $item['data']['id']);
        $I->assertEquals($updatedName, $item['data']['name']);
    }

    public function viewTollGate(ApiTester $I)
    {
        $updatedName = 'TEST_TG_UPDATE';
        $this->createAccessToken($I);
        $I->haveHttpHeader('access-token', self::$accessToken);
        $I->sendGet("/station/{$this->stations['58t']}");
        $I->seeResponseCodeIs(200);
        $I->seeResponseIsJson();
        $I->seeResponseMatchesJsonType($this->resourceData['type58']);
        $item = json_decode($I->grabResponse(), true);
        $I->assertEquals($this->stations['58t'], $item['data']['id']);
        $I->assertEquals($updatedName, $item['data']['name']);
    }

    public function view52(ApiTester $I)
    {
        $updatedName = 'TEST_52_UPDATE';
        $this->createAccessToken($I);
        $I->haveHttpHeader('access-token', self::$accessToken);
        $I->sendGet("/station/{$this->stations['52']}");
        $I->seeResponseCodeIs(200);
        $I->seeResponseIsJson();
        $I->seeResponseMatchesJsonType($this->resourceData['type52']);
        $item = json_decode($I->grabResponse(), true);
        $I->assertEquals($this->stations['52'], $item['data']['id']);
        $I->assertEquals($updatedName, $item['data']['name']);
    }

    public function changeType58To52(ApiTester $I)
    {
        $this->createAccessToken($I);
        $I->haveHttpHeader('access-token', self::$accessToken);
        $I->sendPost('/station', $this->resourceData['ap2']);
        $I->seeResponseCodeIs(200);
        $id = json_decode($I->grabResponse(), true)['data']['id'];

        $I->haveHttpHeader('access-token', self::$accessToken);
        $I->sendPATCH("/station/{$id}/change-type-58-to-52");
        $I->seeResponseCodeIs(200);

        $I->haveHttpHeader('access-token', self::$accessToken);
        $I->sendGET("/station/{$id}");
        $I->seeResponseCodeIs(200);
        $I->canSeeResponseContainsJson([
            'data' => [
                'id' => $id,
                'type' => '52ghz',
            ],
        ]);
    }

    /*****************
     * DELETE
     *****************/
    public function deleteFs(ApiTester $I)
    {
        $I->canSeeRecord(Station::class, ['id' => $this->stations['fsa']]);
        $I->canSeeRecord(Station::class, ['id' => $this->stations['fsb']]);
        $stationA = Station::findOne($this->stations['fsa']);
        $stationAObj = $stationA->getStationTypeObject();
        $stationB = Station::findOne($this->stations['fsb']);
        $stationBObj = $stationB->getStationTypeObject();
        $this->createAccessToken($I);
        $I->haveHttpHeader('access-token', self::$accessToken);
        // If we delete only one of FS pair, both should be deleted
        $I->sendDelete("/station/{$this->stations['fsb']}");

        $I->seeResponseCodeIs(200);
        $I->cantSeeRecord(Station::class, ['id' => $this->stations['fsa']]);
        $I->cantSeeRecord(Station::class, ['id' => $this->stations['fsb']]);

        // Cannot be hardDeleted with foreign key on it's partner
        \Yii::$app->db->createCommand("SET foreign_key_checks = 0;")->execute();
        $stationAObj->hardDelete();
        $stationA->hardDelete();
        $stationBObj->hardDelete();
        $stationB->hardDelete();
        \Yii::$app->db->createCommand("SET foreign_key_checks = 1;")->execute();
    }

    public function deleteWigig(ApiTester $I)
    {
        $I->canSeeRecord(Station::class, ['id' => $this->stations['wig']]);
        $this->createAccessToken($I);
        $I->haveHttpHeader('access-token', self::$accessToken);
        $station = Station::findOne($this->stations['wig']);
        $stationObj = $station->getStationTypeObject();
        $I->sendDelete("/station/{$this->stations['wig']}");
        $I->seeResponseCodeIs(200);

        $stationObj->hardDelete();
        $station->hardDelete();
    }

    public function deleteAccessPoint(ApiTester $I)
    {
        $I->canSeeRecord(Station::class, ['id' => $this->stations['58a']]);
        $this->createAccessToken($I);
        $I->haveHttpHeader('access-token', self::$accessToken);
        $station = Station::findOne($this->stations['58a']);
        $stationObj = $station->getStationTypeObject();
        $I->sendDelete("/station/{$this->stations['58a']}");
        $I->seeResponseCodeIs(200);

        $stationObj->hardDelete();
        $station->hardDelete();
    }

    public function deleteTollGate(ApiTester $I)
    {
        $I->canSeeRecord(Station::class, ['id' => $this->stations['58t']]);
        $this->createAccessToken($I);
        $I->haveHttpHeader('access-token', self::$accessToken);
        $station = Station::findOne($this->stations['58t']);
        $stationObj = $station->getStationTypeObject();
        $I->sendDelete("/station/{$this->stations['58t']}");
        $I->seeResponseCodeIs(200);

        $stationObj->hardDelete();
        $station->hardDelete();
    }

    public function delete52(ApiTester $I)
    {
        $I->canSeeRecord(Station::class, ['id' => $this->stations['52']]);
        $this->createAccessToken($I);
        $I->haveHttpHeader('access-token', self::$accessToken);
        $station = Station::findOne($this->stations['52']);
        $stationObj = $station->getStationTypeObject();
        $I->sendDelete("/station/{$this->stations['52']}");
        $I->seeResponseCodeIs(200);

        $stationObj->hardDelete();
        $station->hardDelete();
    }
}
