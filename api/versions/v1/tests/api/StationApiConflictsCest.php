<?php
/*
* Copyright 2023 DactylGroup s.r.o.
*
* Licensed under the EUPL, Version 1.2 or – as soon they
will be approved by the European Commission - subsequent
versions of the EUPL (the "Licence");
* You may not use this work except in compliance with the
Licence.
* You may obtain a copy of the Licence at:
*
* https://joinup.ec.europa.eu/software/page/eupl5
*
* Unless required by applicable law or agreed to in
writing, software distributed under the Licence is
distributed on an "AS IS" basis,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
express or implied.
* See the Licence for the specific language governing
permissions and limitations under the Licence.
*/
?>


<?php

namespace api\versions\v1;

use dactylcore\core\test\apiTester\ApiTester;
use dactylcore\user\api\versions\v1\models\UserAccessToken;

class StationApiConflictsCest
{
    protected static string $accessToken = 'TEST_ACCESS_TOKEN_STATION';
    protected int $idStation;
    protected array $resourceData;

    public function init()
    {
        $this->resourceData = require __DIR__ . '/../fixtures/allConflictsStation.php';
    }

    protected function createAccessToken(ApiTester $I)
    {
        $I->haveRecord(UserAccessToken::class, [
            'id_user' => 2,
            'access_token' => self::$accessToken,
            'expires' => time() + 1000,
            'deleted' => 0,
        ]);
    }

    protected function testErrorResponse(ApiTester $I, int $statusCode)
    {
        $I->seeResponseCodeIs($statusCode);
        $I->seeResponseIsJson();
        $I->seeResponseMatchesJsonType([
            'status' => 'integer',
            'error' => 'string',
        ]);
    }
/*
    public function fsAndWigigCanHaveConflict(ApiTester $I)
    {
        $this->createAccessToken($I);

        // create FS
        $I->haveHttpHeader('access-token', self::$accessToken);
        $I->sendPost('/station', $this->resourceData['fs1']);
        $I->seeResponseCodeIs(200);

        // create wigig3 conflicting FS
        $I->haveHttpHeader('access-token', self::$accessToken);
        $I->sendPost('/station', $this->resourceData['wigig3']);
        $this->testErrorResponse($I, 409);
    }

    public function wigigAndWigigCannotHaveConflict(ApiTester $I)
    {
        $this->createAccessToken($I);

        // create wigig1
        $I->haveHttpHeader('access-token', self::$accessToken);
        $I->sendPost('/station', $this->resourceData['wigig1']);
        $I->seeResponseCodeIs(200);

        // create wigig2 which conflicts
        $I->haveHttpHeader('access-token', self::$accessToken);
        $I->sendPost('/station', $this->resourceData['wigig2']);

        $I->seeResponseCodeIs(200);
//        $wrongStationId = json_decode($I->grabResponse(), true)['data']['id'];
        $this->testErrorResponse($I, 409);
    }*/
}
