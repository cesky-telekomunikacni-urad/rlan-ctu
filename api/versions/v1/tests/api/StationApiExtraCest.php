<?php
/*
* Copyright 2023 DactylGroup s.r.o.
*
* Licensed under the EUPL, Version 1.2 or – as soon they
will be approved by the European Commission - subsequent
versions of the EUPL (the "Licence");
* You may not use this work except in compliance with the
Licence.
* You may obtain a copy of the Licence at:
*
* https://joinup.ec.europa.eu/software/page/eupl5
*
* Unless required by applicable law or agreed to in
writing, software distributed under the Licence is
distributed on an "AS IS" basis,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
express or implied.
* See the Licence for the specific language governing
permissions and limitations under the Licence.
*/
?>


<?php

namespace api\versions\v1;

use dactylcore\core\test\apiTester\ApiTester;
use dactylcore\user\api\versions\v1\models\UserAccessToken;

class StationApiExtraCest
{
    protected static string $accessToken = 'TEST_ACCESS_TOKEN_STATION';
    protected int $idStation;
    protected array $resourceData;
    protected int $wigigToPair;

    public function init()
    {
        $this->resourceData = require __DIR__ . '/../fixtures/allExtraStation.php';
    }

    protected function createAccessToken(ApiTester $I)
    {
        $I->haveRecord(UserAccessToken::class, [
            'id_user' => 2,
            'access_token' => self::$accessToken,
            'expires' => time() + 1000,
            'deleted' => 0,
        ]);
    }

    protected function testErrorResponse(ApiTester $I, int $statusCode)
    {
        $I->seeResponseCodeIs($statusCode);
        $I->seeResponseIsJson();
        $I->seeResponseMatchesJsonType([
            'status' => 'integer',
            'error' => 'string',
        ]);
    }

    public function cannotCreateFsOutOfBoundaries(ApiTester $I)
    {
        $this->createAccessToken($I);
        $I->haveHttpHeader('access-token', self::$accessToken);
        $I->sendPost('/station', $this->resourceData['outOfBound']);
        $this->testErrorResponse($I, 422);
    }

    public function cannotCreate58OnExcludedZone(ApiTester $I)
    {
        // Get excluded zones
        $excluded = c('EXCLUDED_ZONES_58_GHZ');
        // Set our excluded zone
        \HConfig::setValue('EXCLUDED_ZONES_58_GHZ', '49.6775608,17.5395731');

        $this->createAccessToken($I);
        $I->haveHttpHeader('access-token', self::$accessToken);
        $I->sendPost('/station', $this->resourceData['onExcluded']);
        $this->testErrorResponse($I, 400);

        // Load old zones
        \HConfig::setValue('EXCLUDED_ZONES_58_GHZ', $excluded);
    }

    // Wigigs and their partners
    public function wigigCanPairOnlyWigig(ApiTester $I)
    {
        $this->createAccessToken($I);

        // create wigig
        $I->haveHttpHeader('access-token', self::$accessToken);
        $I->sendPost('/station', $this->resourceData['wigigToPair']);
        $I->seeResponseCodeIs(200);
        $I->seeResponseIsJson();
        $this->wigigToPair = json_decode($I->grabResponse(), true)['data']['id'];

        // create 52
        $I->haveHttpHeader('access-token', self::$accessToken);
        $I->sendPost('/station', $this->resourceData['52ToPair']);
        $I->seeResponseCodeIs(200);
        $I->seeResponseIsJson();
        $wrongStationId = json_decode($I->grabResponse(), true)['data']['id'];

        // trying pair wrong station to wigig
        $I->haveHttpHeader('access-token', self::$accessToken);
        $I->sendPATCH("/station/pair-wigigs/{$this->wigigToPair}/{$wrongStationId}");

        // We cannot pair Wigig to other type than Wigig!!!
        $this->testErrorResponse($I, 409);


//TODO:
//  create one wig 1
//  create 5.2
//  try pair 1 with 5.2
//  create wig 2 and add pair to first one
//  change one of them <-- see results
// ----
//  2 have to has 310 direction
//  1 after save have to has 130 direction and id pair added :)
    }
}
