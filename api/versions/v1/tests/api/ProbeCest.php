<?php
/*
* Copyright 2023 DactylGroup s.r.o.
*
* Licensed under the EUPL, Version 1.2 or – as soon they
will be approved by the European Commission - subsequent
versions of the EUPL (the "Licence");
* You may not use this work except in compliance with the
Licence.
* You may obtain a copy of the Licence at:
*
* https://joinup.ec.europa.eu/software/page/eupl5
*
* Unless required by applicable law or agreed to in
writing, software distributed under the Licence is
distributed on an "AS IS" basis,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
express or implied.
* See the Licence for the specific language governing
permissions and limitations under the Licence.
*/
?>


<?php


namespace api\versions\v1;


use common\models\Probe;
use dactylcore\core\test\apiTester\ApiTester;
use Nette\Utils\DateTime;

class ProbeCest
{

    public function ProbeScan(ApiTester $I)
    {

        $probe = $this->buildProbe();

        $wlanArray = [
            'BSSID' => 'bssid',
            'RSSI' => 42,
            'SSID' => 'wlan ssid',
            'capabilities' => 'can cook minute rice in 58 seconds',
            'frequency' => 1500
        ];
        $wlanObj = (object) $wlanArray;

        $I->haveHttpHeader('access-token', $probe->access_token);
        $I->sendPOST('/probe/scan-info', [
            'address' => 'BOX 41 Bulonzi Road, Kasambya, Uganda',
            'time' => (new DateTime())->format('Y-m-d H:i:s'),
            'wlans' => [
                $wlanObj
            ]
        ]);

        $I->seeResponseCodeIs(200); // 200

        $this->deleteProbe($probe);

    }

    protected function buildProbe() {
        $probe = new Probe();
        $probe->name = "TestProbe";
        $probe->description = "TestProbe description";
        $probe->access_token = \Yii::$app->security->generateRandomString();
        $probe->status = Probe::STATUS_INACTIVE;
        $probe->save();

        return $probe;
    }

    protected function deleteProbe($probe) {
        foreach ($probe->probeScans as $scan) {
            foreach ($scan->probeScanWlans as $scanWlan) {
                $scanWlan->hardDelete();
            }
            $scan->hardDelete();
        }
        $probe->hardDelete();
    }

}