<?php
/*
* Copyright 2023 DactylGroup s.r.o.
*
* Licensed under the EUPL, Version 1.2 or – as soon they
will be approved by the European Commission - subsequent
versions of the EUPL (the "Licence");
* You may not use this work except in compliance with the
Licence.
* You may obtain a copy of the Licence at:
*
* https://joinup.ec.europa.eu/software/page/eupl5
*
* Unless required by applicable law or agreed to in
writing, software distributed under the Licence is
distributed on an "AS IS" basis,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
express or implied.
* See the Licence for the specific language governing
permissions and limitations under the Licence.
*/
?>


<?php

namespace api\versions\v1;

use common\models\Station;
use common\models\Station58;
use common\models\StationFs;
use common\models\StationFsPair;
use common\models\StationWigig;
use dactylcore\core\test\apiTester\ApiTester;
use frontend\controllers\StationController;
use modules\user\common\models\User;
use Yii;

class StationCest
{
    // tests
    public function FS(ApiTester $I)
    {
        $pair = $this->buildFs();

        $I->sendGET("/station/station/{$pair->stationA->id}");
        $I->seeResponseCodeIs(200); // 200
        $I->seeResponseIsJson();
        $response = json_decode($I->grabResponse(), true);
        $obj = $response['data'][$pair->stationA->id];
        $I->assertEquals($pair->stationA->id_user, $obj['id_user']);
        $I->assertEquals('fs', $obj['type']);
        $I->assertCount(2, $response['data']);

        $this->deleteFs($pair);
    }

    public function Wigig(ApiTester $I)
    {
        $station = $this->buildWigig();

        $I->sendGET("/station/station/{$station->id}");
        $I->seeResponseCodeIs(200); // 200
        $I->seeResponseIsJson();
        $response = json_decode($I->grabResponse(), true);
        $obj = $response['data'][$station->id];
        $I->assertEquals($station->id_user, $obj['id_user']);
        $I->assertEquals('wigig', $obj['type']);
        $I->assertCount(1, $response['data']);

        $this->deleteWigig($station);
    }

    public function AccessPoint(ApiTester $I)
    {
        $station = $this->buildAP();

        $I->sendGET("/station/station/{$station->id}");
        $I->seeResponseCodeIs(200); // 200
        $I->seeResponseIsJson();
        $response = json_decode($I->grabResponse(), true);
        $obj = $response['data'][$station->id];
        $I->assertEquals($station->id_user, $obj['id_user']);
        $I->assertEquals('ap_58ghz', $obj['type']);
        $I->assertCount(1, $response['data']);

        $this->deleteAP($station);
    }

    public function TollGate(ApiTester $I)
    {
        $station = $this->buildTG();

        $I->sendGET("/station/station/{$station->id}");
        $I->seeResponseCodeIs(200); // 200
        $I->seeResponseIsJson();
        $response = json_decode($I->grabResponse(), true);
        $obj = $response['data'][$station->id];
        $I->assertEquals($station->id_user, $obj['id_user']);
        $I->assertEquals('tg_58ghz', $obj['type']);
        $I->assertCount(1, $response['data']);

        $this->deleteTG($station);
    }

    public function WigigPopup(ApiTester $I)
    {
        $station = $this->buildWigig();

        $I->sendGET("/station/station-popup/{$station->id}");
        $I->seeResponseCodeIs(200); // 200
        $I->seeResponseIsJson();
        $response = json_decode($I->grabResponse(), true);
        $obj = $response['data'];

        # API returns HTML code!
        $I->assertContains('DRAKKNET2', $obj);
        $I->assertContains($station->getFormattedId(), $obj);
        $I->assertContains($station::getStaticTypeName($station->type, $station->id), $obj);
        $I->assertContains((string)$station->getStationTypeObject()->direction, $obj);

        $this->deleteWigig($station);
    }

    public function Lines(ApiTester $I)
    {
        /*
         {
          "type": "FeatureCollection",
          "features": [
            {
              "type": "Feature",
              "geometry": {
                "type": "LineString",
                "coordinates": [
                  [
                    18.19264620231913,
                    49.64416724369607
                  ],
                  [
                    18.1904686513846,
                    49.64159849456431
                  ]
                ]
              },
              "properties": [
                    my: false
                ]
            },
         */
        $pair = $this->buildFs();

        $I->sendGET("/station/lines/");
        $I->seeResponseCodeIs(200); // 200
        $I->seeResponseIsJson();
        $response = json_decode($I->grabResponse(), true);
        $I->assertEquals('FeatureCollection', $response['type']);
        $I->assertEquals('Feature', $response['features'][0]['type']);
        $I->assertEquals('LineString', $response['features'][0]['geometry']['type']);
        $I->assertEquals(['my' => false], $response['features'][0]['properties']);
        $I->assertCount(2, $response['features'][0]['geometry']['coordinates']);
        $this->deleteFs($pair);
    }

    public function GeoJson(ApiTester $I)
    {
        /*
         * Example
            {
            "type": "FeatureCollection",
            "features": [
              {
                "type": "Feature",
                "geometry": {
                  "type": "Point",
                  "coordinates": [
                    "18.19264620231913",
                    "49.64416724369607"
                  ]
                },
                "properties": {
                  "t": "fs",
                  "w_lat": null,
                  "w_lng": null
                },
                "id": 3
              },
        ...
         */
        $pair = $this->buildFs();

        $I->sendGET("/station/geo-stations/");
        $I->seeResponseCodeIs(200); // 200
        $I->seeResponseIsJson();
        $response = json_decode($I->grabResponse(), true);
        $I->assertEquals('FeatureCollection', $response['type']);
        $I->assertEquals('Feature', $response['features'][0]['type']);
        $I->assertEquals('Point', $response['features'][0]['geometry']['type']);
        $I->assertNotEmpty($response['features'][0]['properties']);
        $I->assertCount(2, $response['features'][0]['geometry']['coordinates']);
        $I->assertCount(3, $response['features'][0]['properties']);
        $I->assertNotNull($response['features'][0]['id']);
        $this->deleteFs($pair);
    }

    public function geoExclusionZone(ApiTester $I) {
        $tollGate = $this->buildTG();
        $accessPoint = $this->buildAP();

        $I->sendGET("/station/geo-exclusion-zone-stations/".$accessPoint->id);
        $I->seeResponseCodeIs(200); // 200
        $I->seeResponseIsJson();
        $response = json_decode($I->grabResponse(), true);
        $I->assertEquals('FeatureCollection', $response['type']);
        $I->assertEquals('Feature', $response['features'][0]['type']);
        $I->assertEquals('Point', $response['features'][0]['geometry']['type']);
        $I->assertNotEmpty($response['features'][0]['properties']);
        $I->assertCount(2, $response['features'][0]['geometry']['coordinates']);
        $I->assertCount(2, $response['features'][0]['properties']);
        $I->assertEquals(1, $response['features'][0]['properties']['c']);
        $I->assertNotNull($response['features'][0]['id']);

        $this->deleteTG($tollGate);
        $this->deleteAP($accessPoint);
    }

    protected function buildWigig($path =  '/../fixtures/stationWigig.json')
    {
        $data = json_decode(file_get_contents(__DIR__ . $path), true);
        $station = new Station();
        $station->setAttributes($data['station']);
        $station->status = Station::STATUS_FINISHED;
        $station->save();

        $wigig = new StationWigig();
        $wigig->setAttributes($data['wigig']);

        $wigig->id_station = $station->id;
        $wigig->save();

        return $station;
    }

    protected function deleteWigig(Station $station)
    {
        $wigig = $station->getStationTypeObject();
        $wigig->hardDelete();
        $station->hardDelete();
    }

    protected function buildAP()
    {
        $data = json_decode(file_get_contents(__DIR__ . '/../fixtures/stationAccessPoint.json'), true);
        $station = new Station();
        $station->setAttributes($data['station']);
        codecept_debug($station->type);
        $station->setScenario(StationController::STEP_4_SUMMARY);
        $station->save();
        $station->moveToFinished();

        $ap = new Station58();
        $ap->setAttributes($data['58ghz']);

        $ap->id_station = $station->id;
        $ap->save();

        return $station;
    }

    protected function deleteAP(Station $station)
    {
        $ap = $station->getStationTypeObject();
        $ap->hardDelete();
        $station->hardDelete();
    }

    protected function buildTG()
    {
        $data = json_decode(file_get_contents(__DIR__ . '/../fixtures/stationTollGate.json'), true);
        $station = new Station();
        $station->setAttributes($data['station']);
        $station->setScenario(StationController::STEP_4_SUMMARY);
        $station->save();
        $station->moveToFinished();

        $tg = new Station58();
        $tg->setAttributes($data['58ghz']);

        $tg->id_station = $station->id;
        $tg->save();

        return $station;
    }

    protected function deleteTG(Station $station)
    {
        $tg = $station->getStationTypeObject();
        $tg->hardDelete();
        $station->hardDelete();
    }

    protected function buildFs($path =  '/../fixtures/stationFs.json')
    {
        $data = json_decode(file_get_contents(__DIR__ . $path), true);
        $pair = new StationFsPair();
        $pair->initNew();
        $pair->setData([Station::FORM_NAME => [Station::POSITION_A => $data['a'], Station::POSITION_B => $data['b'],]]);

        $pair->stationB->pointerToPairStation = $pair->stationA;
        $pair->stationA->pointerToPairStation = $pair->stationB;
        $pair->save();


        $pair->setFsData([StationFs::FORM_NAME => [Station::POSITION_A => $data['typeA'], Station::POSITION_B => $data['typeB'],]], StationController::STEP_1_NAME);
        $pair->setFsData([StationFs::FORM_NAME => [Station::POSITION_A => $data['typeA'], Station::POSITION_B => $data['typeB'],]], StationController::STEP_3_PARAMS);

        $pair->FsSave();
        $pair->stationA->moveToFinished();
        $pair->stationB->moveToFinished();
        return $pair;
    }

    protected function deleteFs($pair)
    {
        $pair->stationA->typeStation->hardDelete();
        $pair->stationB->typeStation->hardDelete();
        Yii::$app->db->createCommand('SET FOREIGN_KEY_CHECKS=0')->execute();

        $pair->stationB->hardDelete();
        $pair->stationA->hardDelete();

        Yii::$app->db->createCommand('SET FOREIGN_KEY_CHECKS=1')->execute();
    }

    public function Prolong(ApiTester $I)
    {
        // prolong endpoint can not be tested with meaningful test because it has FE validation which I cannot perform from API because of CSRF
        $data = ['stationsToProlong' => [1]];
        $I->sendPOST("/station/prolong", $data);
        $I->seeResponseCodeIs(200); // 200
        $I->seeResponseIsJson();
    }

    public function Perimeter(ApiTester $I)
    {
        $pair = $this->buildFs('/../fixtures/fsLibava.json');

        // A = 49.68257822623614, 17.425807224087905
        // B = 49.687465000156344, 17.497218352765657

        // C = 49.69035128287678, 17.60669466420729

        // A-B = 5.18km
        // B-C = 8.64
        // A-C = 13.82
        /*         perimeter
         *      |-------|
         *    A---B-----C
         */

        // asking on stations from position C in perimeter 10km -> should get B (because close) and A because pair
        $I->sendGET("/station/stations-from-position/49.69235128287678/17.61669466420729/8.0");
        $I->seeResponseCodeIs(200); // 200
        $response = $I->grabResponse();

        $I->assertTrue(isset($pair->stationA->id, json_decode($response, true)['data']));
        $I->assertTrue(isset($pair->stationB->id, json_decode($response, true)['data']));

        $this->deleteFs($pair);
    }

}
