<?php
/*
* Copyright 2023 DactylGroup s.r.o.
*
* Licensed under the EUPL, Version 1.2 or – as soon they
will be approved by the European Commission - subsequent
versions of the EUPL (the "Licence");
* You may not use this work except in compliance with the
Licence.
* You may obtain a copy of the Licence at:
*
* https://joinup.ec.europa.eu/software/page/eupl5
*
* Unless required by applicable law or agreed to in
writing, software distributed under the Licence is
distributed on an "AS IS" basis,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
express or implied.
* See the Licence for the specific language governing
permissions and limitations under the Licence.
*/
?>


<?php

namespace api\versions\v1;

use dactylcore\core\test\apiTester\ApiTester;
use dactylcore\user\api\versions\v1\models\UserAccessToken;

class StationApiPublicationCest
{
    protected static string $accessToken = 'TEST_ACCESS_TOKEN_STATION';
    protected int $idStation;
    protected array $resourceData;
    protected int $wigigId;
    protected int $fsId;

    public function init()
    {
        $this->resourceData = require __DIR__ . '/../fixtures/stationApiPublication.php';
    }

    protected function createAccessToken(ApiTester $I)
    {
        $I->haveRecord(UserAccessToken::class, [
            'id_user' => 2,
            'access_token' => self::$accessToken,
            'expires' => time() + 1000,
            'deleted' => 0,
        ]);
    }

    protected function testErrorResponse(ApiTester $I, int $statusCode)
    {
        $I->seeResponseCodeIs($statusCode);
        $I->seeResponseIsJson();
        $I->seeResponseMatchesJsonType([
            'status' => 'integer',
            'error' => 'string',
        ]);
    }

    public function publish(ApiTester $I)
    {
        $this->createAccessToken($I);

        // create FS
        $I->haveHttpHeader('access-token', self::$accessToken);
        $I->sendPOST('/station', $this->resourceData['fs']);
        $I->seeResponseCodeIs(200);
        $this->fsId = json_decode($I->grabResponse(), true)['data']['stationA']['id_master'];

        // create wigig
        $I->haveHttpHeader('access-token', self::$accessToken);
        $I->sendPOST('/station', $this->resourceData['wigig']);
        $I->seeResponseCodeIs(200);
        $this->wigigId = json_decode($I->grabResponse(), true)['data']['id'];

        // publish FS
        $I->haveHttpHeader('access-token', self::$accessToken);
        $I->sendPATCH("/station/{$this->fsId}/publish");
        $I->seeResponseCodeIs(200);

        // try publish wigig, should be error with conflict
        $I->haveHttpHeader('access-token', self::$accessToken);
        $I->sendPATCH("/station/{$this->wigigId}/publish");
        $I->seeResponseCodeIs(409);

        // solve conflict with sending declaration
        $I->haveHttpHeader('access-token', self::$accessToken);
        $I->sendPATCH("/station/{$this->wigigId}/publish", ['solveConflictByDeclaration' => true]);
        $I->seeResponseCodeIs(200);

        // verify change
        $I->haveHttpHeader('access-token', self::$accessToken);
        $I->sendGET("/station/{$this->wigigId}");
        $I->seeResponseCodeIs(200);
        $I->canSeeResponseContainsJson([
            'data' => [
                'id' => $this->wigigId,
                'status' => 'finished',
            ],
        ]);
    }

    public function publishWaitingChangesOnWigig(ApiTester $I)
    {
        // update wigig, cause waiting changes logic
        $I->haveHttpHeader('access-token', self::$accessToken);
        $I->sendPATCH("/station/{$this->wigigId}", [
            'station' => [
                'channel_width' => $updatedChannelWidth = 1111,
            ],
        ]);
        $I->seeResponseCodeIs(200);

        // verify waiting changes creation
        $savedChannelWidth = json_decode($I->grabResponse(),
            true)['data']['published']['waiting_api_changes_json']['station']['channel_width'];
        $I->assertEquals($updatedChannelWidth, $savedChannelWidth);

        // publish changes
        $I->haveHttpHeader('access-token', self::$accessToken);
        $I->sendPATCH("/station/{$this->wigigId}/publish", ['solveConflictByDeclaration' => true]);
        $I->seeResponseCodeIs(200);

        $I->haveHttpHeader('access-token', self::$accessToken);
        $I->sendGET("/station/{$this->wigigId}");
        $I->seeResponseCodeIs(200);
        $savedChannelWidth = json_decode($I->grabResponse(), true)['data']['channel_width'];
        $I->assertEquals($updatedChannelWidth, round($savedChannelWidth));
        $I->canSeeResponseContainsJson([
            'data' => [
                'status' => 'finished',
            ],
        ]);
    }

    public function publishWaitingChangesOnFS(ApiTester $I)
    {
        // update fs, cause waiting changes logic
        $I->haveHttpHeader('access-token', self::$accessToken);
        $I->sendPATCH("/station/{$this->fsId}", [
            'stationA' => [
                'channel_width' => $updatedChannelWidthA = 998,
            ],
            'stationB' => [
                'channel_width' => $updatedChannelWidthB = 999,
            ],
        ]);
        $I->seeResponseCodeIs(200);

        // verify waiting changes creation
        $savedChannelWidthA = json_decode($I->grabResponse(),
            true)['data']['published']['stationA']['waiting_api_changes_json']['stationA']['channel_width'];
        $I->assertEquals($updatedChannelWidthA, round($savedChannelWidthA));

        // publish changes
        $I->haveHttpHeader('access-token', self::$accessToken);
        $I->sendPATCH("/station/{$this->fsId}/publish", ['solveConflictByDeclaration' => true]);
        $I->seeResponseCodeIs(200);

        $I->haveHttpHeader('access-token', self::$accessToken);
        $I->sendGET("/station/{$this->fsId}");
        $I->seeResponseCodeIs(200);
        $savedChannelWidthA = json_decode($I->grabResponse(), true)['data']['stationA']['channel_width'];
        $savedChannelWidthB = json_decode($I->grabResponse(), true)['data']['stationB']['channel_width'];
        $I->assertEquals($updatedChannelWidthA, round($savedChannelWidthA));
        $I->assertEquals($updatedChannelWidthB, round($savedChannelWidthB));
        $I->canSeeResponseContainsJson([
            'data' => [
                'stationA' => [
                    'status' => 'finished',
                ],
                'stationB' => [
                    'status' => 'finished',
                ],
            ],
        ]);
    }

    public function relength(ApiTester $I)
    {
        // Expire station
        \Yii::$app->db->createCommand()->update(
            'station',
            [
                'valid_to' => $oldValidTotime = time() - 24 * 60 * 60,
                'protected_to' => $oldValidTotime,
                'status' => 'expired',
            ],
            ['id' => $this->wigigId]
        )->execute();

        $I->haveHttpHeader('access-token', self::$accessToken);
        $I->sendPATCH("/station/{$this->wigigId}/relength");
        $I->seeResponseCodeIs(200);
        $newValidTo = json_decode($I->grabResponse(), true)['data']['valid_to'];

        $I->assertGreaterThan($oldValidTotime, $newValidTo);
    }
}
