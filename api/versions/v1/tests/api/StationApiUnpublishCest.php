<?php
/*
* Copyright 2023 DactylGroup s.r.o.
*
* Licensed under the EUPL, Version 1.2 or – as soon they
will be approved by the European Commission - subsequent
versions of the EUPL (the "Licence");
* You may not use this work except in compliance with the
Licence.
* You may obtain a copy of the Licence at:
*
* https://joinup.ec.europa.eu/software/page/eupl5
*
* Unless required by applicable law or agreed to in
writing, software distributed under the Licence is
distributed on an "AS IS" basis,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
express or implied.
* See the Licence for the specific language governing
permissions and limitations under the Licence.
*/
?>


<?php

namespace api\versions\v1;

use api\versions\v1\models\Station;
use dactylcore\core\test\apiTester\ApiTester;
use dactylcore\user\api\versions\v1\models\UserAccessToken;

class StationApiUnpublishCest
{
    protected static string $accessToken = 'TEST_ACCESS_TOKEN_STATION';
    protected int $idStation;
    protected array $resourceData;
    protected array $stations
        = [
            'fsa' => null,
            'fsb' => null,
            'wig' => null,
            '58a' => null,
            '58t' => null,
            '52' => null,
        ];

    public function init()
    {
        $this->resourceData = require __DIR__ . '/../fixtures/allStation.php';
    }

    protected function createAccessToken(ApiTester $I)
    {
        $I->haveRecord(UserAccessToken::class, [
            'id_user' => 2,
            'access_token' => self::$accessToken,
            'expires' => time() + 1000,
            'deleted' => 0,
        ]);
    }

    protected function testErrorResponse(ApiTester $I, int $statusCode)
    {
        $I->seeResponseCodeIs($statusCode);
        $I->seeResponseIsJson();
        $I->seeResponseMatchesJsonType([
            'status' => 'integer',
            'error' => 'string',
        ]);
    }

    /*************
     *  CREATE
     *************/
    public function createForUnpublishing(ApiTester $I)
    {
        // FS
        $this->createAccessToken($I);
        $I->haveHttpHeader('access-token', self::$accessToken);
        $I->sendPost('/station', $this->resourceData['fs']);
        $I->seeResponseCodeIs(200);
        $I->seeResponseIsJson();
        $I->seeResponseMatchesJsonType($this->resourceData['typeFs']);
        $this->stations['fsa'] = json_decode($I->grabResponse(), true)['data']['stationA']['id'];
        $this->stations['fsb'] = json_decode($I->grabResponse(), true)['data']['stationB']['id'];
        
        // WIGIG
        $I->haveHttpHeader('access-token', self::$accessToken);
        $I->sendPost('/station', $this->resourceData['wigig']);
        $I->seeResponseCodeIs(200);
        $I->seeResponseIsJson();
        $I->seeResponseMatchesJsonType($this->resourceData['typeWigig']);
        $this->stations['wig'] = json_decode($I->grabResponse(), true)['data']['id'];

        // AP
        $I->haveHttpHeader('access-token', self::$accessToken);
        $I->sendPost('/station', $this->resourceData['ap']);
        $I->seeResponseCodeIs(200);
        $I->seeResponseIsJson();
        $I->seeResponseMatchesJsonType($this->resourceData['type58']);
        $this->stations['58a'] = json_decode($I->grabResponse(), true)['data']['id'];

        // TG
        $I->haveHttpHeader('access-token', self::$accessToken);
        $I->sendPost('/station', $this->resourceData['tg']);
        $I->seeResponseCodeIs(200);
        $I->seeResponseIsJson();
        $I->seeResponseMatchesJsonType($this->resourceData['type58']);
        $this->stations['58t'] = json_decode($I->grabResponse(), true)['data']['id'];

        // 52
        $I->haveHttpHeader('access-token', self::$accessToken);
        $I->sendPost('/station', $this->resourceData['52']);
        $I->seeResponseCodeIs(200);
        $I->seeResponseIsJson();
        $I->seeResponseMatchesJsonType($this->resourceData['type52']);
        $this->stations['52'] = json_decode($I->grabResponse(), true)['data']['id'];
    }

    /**
     * @param ApiTester $I
     */
    public function missingAccessToken(ApiTester $I)
    {
        $I->sendPATCH("/station/{$this->stations["fsa"]}/unpublish");
        $this->testErrorResponse($I, 400);
    }

    /**
     * @param ApiTester $I
     */
    public function badAccessToken(ApiTester $I)
    {
        $I->haveHttpHeader('access-token', 'BAD_ACCESS_TOKEN');
        $I->sendPATCH("/station/{$this->stations["fsa"]}/unpublish");
        $this->testErrorResponse($I, 401);
    }

    /**
     * @param ApiTester $I
     */
    public function badMethodTypes(ApiTester $I)
    {
        $I->sendPut("/station/{$this->stations["fsa"]}/unpublish");
        $this->testErrorResponse($I, 404);
        $I->sendDelete("/station/{$this->stations["fsa"]}/unpublish");
        $this->testErrorResponse($I, 404);
        $I->sendPost("/station/{$this->stations["fsa"]}/unpublish");
        $this->testErrorResponse($I, 404);
        $I->sendPut("/station/{$this->stations["fsa"]}/unpublish");
        $this->testErrorResponse($I, 404);
    }

    public function unpublishWaiting(ApiTester $I)
    {
        $I->haveHttpHeader('access-token', self::$accessToken);
        $I->sendPatch("/station/{$this->stations['52']}/unpublish");
        $this->testErrorResponse($I, 409);

        foreach ($this->stations as $key => $stationId) {
            if ($key != 'fsb') {
                $I->sendPATCH("/station/{$stationId}/publish", ['solveConflictByDeclaration' => true]);
                $I->seeResponseCodeIs(200);
            }
        }
    }

    /*************
     * UNPUBLISH
     *************/
    public function unpublishStations(ApiTester $I)
    {
        // FS
        $this->createAccessToken($I);
        $I->haveHttpHeader('access-token', self::$accessToken);
        $I->sendPatch("/station/{$this->stations['fsa']}/unpublish");
        $I->seeResponseCodeIs(200);
        $I->seeResponseIsJson();
        $I->seeResponseMatchesJsonType($this->resourceData['typeFs']);
        $item = json_decode($I->grabResponse(), true);
        $I->assertEquals($this->stations['fsa'], $item['data']['stationA']['id']);
        $I->assertEquals($this->stations['fsb'], $item['data']['stationB']['id']);
        $I->assertEquals(Station::STATUS_UNPUBLISHED, $item['data']['stationA']['status']);
        $I->assertEquals(Station::STATUS_UNPUBLISHED, $item['data']['stationB']['status']);

        // WIGIG
        $I->haveHttpHeader('access-token', self::$accessToken);
        $I->sendPatch("/station/{$this->stations['wig']}/unpublish");
        $I->seeResponseCodeIs(200);
        $I->seeResponseIsJson();
        $I->seeResponseMatchesJsonType($this->resourceData['typeWigig']);
        $item = json_decode($I->grabResponse(), true);
        $I->assertEquals($this->stations['wig'], $item['data']['id']);
        $I->assertEquals(Station::STATUS_UNPUBLISHED, $item['data']['status']);

        // AP
        $I->haveHttpHeader('access-token', self::$accessToken);
        $I->sendPatch("/station/{$this->stations['58a']}/unpublish");
        $I->seeResponseCodeIs(200);
        $I->seeResponseIsJson();
        $I->seeResponseMatchesJsonType($this->resourceData['type58']);
        $item = json_decode($I->grabResponse(), true);
        $I->assertEquals($this->stations['58a'], $item['data']['id']);
        $I->assertEquals(Station::STATUS_UNPUBLISHED, $item['data']['status']);

        // TG
        $I->haveHttpHeader('access-token', self::$accessToken);
        $I->sendPatch("/station/{$this->stations['58t']}/unpublish");
        $I->seeResponseCodeIs(200);
        $I->seeResponseIsJson();
        $I->seeResponseMatchesJsonType($this->resourceData['type58']);
        $item = json_decode($I->grabResponse(), true);
        $I->assertEquals($this->stations['58t'], $item['data']['id']);
        $I->assertEquals(Station::STATUS_UNPUBLISHED, $item['data']['status']);

        // 52
        $I->haveHttpHeader('access-token', self::$accessToken);
        $I->sendPatch("/station/{$this->stations['52']}/unpublish");
        $I->seeResponseCodeIs(200);
        $I->seeResponseIsJson();
        $I->seeResponseMatchesJsonType($this->resourceData['type52']);
        $item = json_decode($I->grabResponse(), true);
        $I->assertEquals($this->stations['52'], $item['data']['id']);
        $I->assertEquals(Station::STATUS_UNPUBLISHED, $item['data']['status']);
    }

    public function unpublishUnpublished(ApiTester $I)
    {
        $I->haveHttpHeader('access-token', self::$accessToken);
        $I->sendPatch("/station/{$this->stations['52']}/unpublish");
        $this->testErrorResponse($I, 409);
    }

    public function publishUnpublished(ApiTester $I)
    {
        $I->haveHttpHeader('access-token', self::$accessToken);
        $I->sendPATCH("/station/{$this->stations['52']}/publish", ['solveConflictByDeclaration' => true]);
        $this->testErrorResponse($I, 409);
    }

    public function publishUpdated(ApiTester $I)
    {
        $updatedName = 'TEST_52_UPDATE';
        $this->createAccessToken($I);
        $I->haveHttpHeader('access-token', self::$accessToken);
        $I->sendPatch("/station/{$this->stations['52']}", [
            'station' => [
                'name' => $updatedName,
            ],
        ]);
        $I->seeResponseCodeIs(200);

        $I->haveHttpHeader('access-token', self::$accessToken);
        $I->sendPATCH("/station/{$this->stations['52']}/publish", ['solveConflictByDeclaration' => true]);
        $I->seeResponseCodeIs(200);

        // remove all created stations
        $this->deleteAll($I);
    }

    protected function deleteAll($I)
    {
        foreach ($this->stations as $stationId) {
            $I->comment("Deleting station id={$stationId}");
            if ($station = Station::findOne($stationId)) {
                $station->delete();
            }
        }
    }
}
