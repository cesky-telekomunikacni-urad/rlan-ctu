<?php
/*
* Copyright 2023 DactylGroup s.r.o.
*
* Licensed under the EUPL, Version 1.2 or – as soon they
will be approved by the European Commission - subsequent
versions of the EUPL (the "Licence");
* You may not use this work except in compliance with the
Licence.
* You may obtain a copy of the Licence at:
*
* https://joinup.ec.europa.eu/software/page/eupl5
*
* Unless required by applicable law or agreed to in
writing, software distributed under the Licence is
distributed on an "AS IS" basis,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
express or implied.
* See the Licence for the specific language governing
permissions and limitations under the Licence.
*/
?>


<?php
return [
    "wigig" => [
        'type' => 'wigig',
        'station' => [
            "direction" => 65,
            "serial_number" => 'QQQQ',
            "antenna_volume" => 28,
            "power" => 17.25,
            "channel_width" => 2200,
            "name" => "Wigig Publish test",
            "lng" => 15.170946,
            "lat" => 50.729315,
        ],
    ],
    "fs" => [
        'type' => 'fs',
        'stationA' => [
            'name' => 'fs Publish test A',
            'lng' => 15.170946,
            'lat' => 50.729315,
            'mac_address' => '00:15:6D:5E:F4:55',
            'frequency' => 60000,
            'antenna_volume' => 30,
            'channel_width' => 120,
        ],
        'stationB' => [
            'name' => 'fs Publish test B',
            'lng' => 15.182529,
            'lat' => 50.728203,
            'mac_address' => '00:15:6D:5E:F4:44',
            'antenna_volume' => 30,
            'channel_width' => 120,
            'frequency' => 60000,
        ],
    ],
];