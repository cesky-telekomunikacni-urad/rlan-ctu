<?php
/*
* Copyright 2023 DactylGroup s.r.o.
*
* Licensed under the EUPL, Version 1.2 or – as soon they
will be approved by the European Commission - subsequent
versions of the EUPL (the "Licence");
* You may not use this work except in compliance with the
Licence.
* You may obtain a copy of the Licence at:
*
* https://joinup.ec.europa.eu/software/page/eupl5
*
* Unless required by applicable law or agreed to in
writing, software distributed under the Licence is
distributed on an "AS IS" basis,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
express or implied.
* See the Licence for the specific language governing
permissions and limitations under the Licence.
*/
?>


<?php
return [
    "outOfBound" => [
        'type' => 'fs',
        'stationA' => [
            'ratio_signal_interference' => 40,
            'frequency' => 60480.0,
            'power' => 10,
            'serial_number' => null,
            'antenna_volume' => 42.0,
            'channel_width' => 2160.0,
            'name' => 'TEST_FS_OUT',
            'lng' => 11.99416946308699,
            'lat' => 51.11830318455594,
            'mac_address' => 'AA:FF:AA:CC:DC:11',
        ],
        'stationB' => [
            'ratio_signal_interference' => 12,
            'frequency' => 60480.0,
            'lng' => 18.90918624161034,
            'lat' => 49.64159849456431,
            'antenna_volume' => 42.0,
            'channel_width' => 2160.0,
            'name' => 'TEST_FS_OUT',
            'mac_address' => 'AA:FF:AA:AC:4B:11',
            'serial_number' => null,
        ],
    ],
    'onExcluded' => [
        'type' => 'wifi_5_8',
        'station' => [
            'is_ap' => true,
            'name' => 'TEST_AP_EXCLUDED',
            'lng' => 17.53957310000000,
            'lat' => 49.67756080000000,
            "mac_address" => "AA:EC:AA:DD:62:AA",
        ],
    ],
    "wigigToPair" => [
        'type' => 'wigig',
        'station' => [
            "direction" => 65,
            "serial_number" => 'QQQQ',
            "antenna_volume" => 28,
            "power" => 17.25,
            "channel_width" => 2200,
            "name" => "TEST_WIGIG_TO_PAIR",
            "lng" => 14.746182885905455,
            "lat" => 50.38662883945966,
        ],
    ],
    "52ToPair" => [
        'type' => 'wifi_5_2',
        'station' => [
            'name' => 'TEST_5_2_TO_PAIR',
            'lng' => 14.867142634893042,
            'lat' => 50.385243353539266,
            "mac_address" => "DD:EC:07:EE:FF:CD",
        ],
    ],
    "wigigPairer" => [
        'type' => 'wigig',
        'station' => [
//            "direction" => 65,
            "serial_number" => 'ZZZZ',
            "antenna_volume" => 29,
            "power" => 17.25,
            "channel_width" => 2200,
            "name" => "TEST_WIGIG_PAIRER",
            "lng" => 14.798624545823884,
            "lat" => 50.35827885304485,
        ],
    ],

];