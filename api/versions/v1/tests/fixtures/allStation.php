<?php
/*
* Copyright 2023 DactylGroup s.r.o.
*
* Licensed under the EUPL, Version 1.2 or – as soon they
will be approved by the European Commission - subsequent
versions of the EUPL (the "Licence");
* You may not use this work except in compliance with the
Licence.
* You may obtain a copy of the Licence at:
*
* https://joinup.ec.europa.eu/software/page/eupl5
*
* Unless required by applicable law or agreed to in
writing, software distributed under the Licence is
distributed on an "AS IS" basis,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
express or implied.
* See the Licence for the specific language governing
permissions and limitations under the Licence.
*/
?>


<?php
return [
    "fs" => [
        'type' => 'fs',
        'stationA' => [
            'ratio_signal_interference' => 40,
            'frequency' => 60480.0,
            'power' => 10,
            'serial_number' => null,
            'antenna_volume' => 42.0,
            'channel_width' => 2160.0,
            'name' => 'TEST_FS_OK',
            'lng' => 18.19264620231913,
            'lat' => 49.64416724369607,
            'mac_address' => 'AA:D6:AA:CC:DC:FA',
        ],
        'stationB' => [
            'ratio_signal_interference' => 12,
            'frequency' => 60480.0,
            'lng' => 18.1904686513846,
            'lat' => 49.64159849456431,
            'antenna_volume' => 42.0,
            'channel_width' => 2160.0,
            'name' => 'TEST_FS_OK',
            'mac_address' => 'AA:D6:AA:AC:4B:09',
            'serial_number' => null,
        ],
    ],
    "wigig" => [
        'type' => 'wigig',
        'station' => [
            "direction" => 286,
            "eirp" => 40.00,
            "serial_number" => null,
            "antenna_volume" => null,
            "channel_width" => 80.00,
            "name" => "TEST_WIGIG_OK",
            "lng" => 18.45592680342281,
            "lat" => 49.89107250618832,
            "mac_address" => "AA:EC:07:D2:62:C5",
        ],
    ],
    "ap" => [
        'type' => 'wifi_5_8',
        'station' => [
            'is_ap' => true,
            'name' => 'TEST_AP_OK',
            'lng' => 18.45592680342281,
            'lat' => 49.89107250618832,
            "mac_address" => "AA:EC:07:D2:62:C5",
        ],
    ],
    "ap2" => [
        'type' => 'wifi_5_8',
        'station' => [
            'is_ap' => true,
            'name' => 'TEST_AP2_OK',
            'lng' => 18.55592680342281,
            'lat' => 49.99107250618832,
            "mac_address" => "AA:EC:07:D2:62:C6",
        ],
    ],
    "tg" => [
        'type' => 'wifi_5_8',
        'station' => [
            'is_ap' => false,
            'name' => 'TEST_TG_OK',
            'lng' => 18.45592670342281,
            'lat' => 49.89109250618832,
            "mac_address" => "AA:FF:07:D2:62:C5",
        ],
    ],
    "52" => [
        'type' => 'wifi_5_2',
        'station' => [
            'name' => 'TEST_5_2_OK',
            'lng' => 18.45592680442281,
            'lat' => 49.89107250718832,
            "mac_address" => "AA:EC:07:EE:62:C5",
        ],
    ],
    'typeFs' => [
        'status' => 'integer',
        'data' => [
            'stationA' => [
                "ratio_signal_interference" => 'integer',
                "frequency" => 'string',
                "id" => 'integer',
                "id_user" => 'integer',
                "power" => 'string',
                "type" => 'string',
                "name" => 'string',
                "mac_address" => 'string',
                "serial_number" => 'null',
                "lng" => 'string',
                "lat" => 'string',
                "antenna_volume" => 'string',
                "channel_width" => 'string',
                "status" => 'string',
                "registered_at" => 'integer',
                "valid_to" => 'integer',
                "protected_to" => 'integer',
                "created_at" => 'integer',
                "updated_at" => 'integer',
                "published_by" => 'null',
                "waiting_api_changes_json" => 'array',
            ],
            'stationB' => [
                "ratio_signal_interference" => 'integer',
                "frequency" => 'string',
                "id" => 'integer',
                "id_user" => 'integer',
                "power" => 'null',
                "type" => 'string',
                "name" => 'string',
                "mac_address" => 'string',
                "serial_number" => 'null',
                "lng" => 'string',
                "lat" => 'string',
                "antenna_volume" => 'string',
                "channel_width" => 'string',
                "status" => 'string',
                "registered_at" => 'integer',
                "valid_to" => 'integer',
                "protected_to" => 'integer',
                "created_at" => 'integer',
                "updated_at" => 'integer',
                "published_by" => 'null',
                "waiting_api_changes_json" => 'array',
            ],
        ],
    ],
    'typeWigig' => [
        'status' => 'integer',
        'data' => [
            "id" => 'integer',
            "id_user" => 'integer',
            "type" => 'string',
            "name" => 'string',
            "mac_address" => 'string',
            "serial_number" => 'null',
            "lng" => 'string',
            "lat" => 'string',
            "antenna_volume" => 'null',
            "channel_width" => 'string',
            "status" => 'string',
            "registered_at" => 'integer',
            "valid_to" => 'integer',
            "protected_to" => 'integer',
            "created_at" => 'integer',
            "updated_at" => 'integer',
            "published_by" => 'null',
            "waiting_api_changes_json" => 'array',
            "power" => 'null',
            "direction" => 'integer',
            "eirp" => 'string',
        ],
    ],
    'type58' => [
        'status' => 'integer',
        'data' => [
            "id" => 'integer',
            "id_user" => 'integer',
            "type" => 'string',
            "name" => 'string',
            "mac_address" => 'string',
            "serial_number" => 'null',
            "lng" => 'string',
            "lat" => 'string',
            "antenna_volume" => 'null',
            "channel_width" => 'null',
            "status" => 'string',
            "registered_at" => 'integer',
            "valid_to" => 'integer',
            "protected_to" => 'integer',
            "created_at" => 'integer',
            "updated_at" => 'integer',
            "published_by" => 'null',
            "waiting_api_changes_json" => 'array',
            "power" => 'null',
            "is_ap" => 'integer',
        ],
    ],
    'type52' => [
        'status' => 'integer',
        'data' => [
            "id" => 'integer',
            "id_user" => 'integer',
            "type" => 'string',
            "name" => 'string',
            "mac_address" => 'string',
            "serial_number" => 'null',
            "lng" => 'string',
            "lat" => 'string',
            "antenna_volume" => 'null',
            "channel_width" => 'null',
            "status" => 'string',
            "registered_at" => 'integer',
            "valid_to" => 'integer',
            "protected_to" => 'integer',
            "created_at" => 'integer',
            "updated_at" => 'integer',
            "published_by" => 'null',
            "waiting_api_changes_json" => 'array',
            "power" => 'null',
        ],
    ],
];