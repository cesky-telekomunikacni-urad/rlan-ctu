<?php
/*
* Copyright 2023 DactylGroup s.r.o.
*
* Licensed under the EUPL, Version 1.2 or – as soon they
will be approved by the European Commission - subsequent
versions of the EUPL (the "Licence");
* You may not use this work except in compliance with the
Licence.
* You may obtain a copy of the Licence at:
*
* https://joinup.ec.europa.eu/software/page/eupl5
*
* Unless required by applicable law or agreed to in
writing, software distributed under the Licence is
distributed on an "AS IS" basis,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
express or implied.
* See the Licence for the specific language governing
permissions and limitations under the Licence.
*/
?>


<?php

namespace api\versions\v1\controllers;

use api\versions\v1\controllers\traits\OldStationApiTrait;
use api\helpers\StationCrudHelper;
use common\models\StationWigig;
use dactylcore\core\api\RestApiController;
use Yii;
use yii\filters\AccessControl;
use yii\helpers\ArrayHelper;

use api\versions\v1\models\Station;
use api\versions\v1\models\search\StationSearch;
use yii\web\ConflictHttpException;
use yii\web\ForbiddenHttpException;
use yii\web\NotFoundHttpException;
use yii\web\ServerErrorHttpException;
use frontend\controllers\StationController as FrontendStationController;
use yii\web\UnauthorizedHttpException;

class StationController extends RestApiController
{
    use OldStationApiTrait;

    public const CACHE_GEO_STATIONS = 'geoStations';
    public const CACHE_GEO_CONFLICT_STATIONS = 'geoConflictStations_';
    public const CACHE_GEO_EXCLUSION_ZONE = 'geoExclusionZone_';
    public const CACHE_LINES = 'lines';
    //
    public $modelClass = Station::class;
    public $searchModelClass = StationSearch::class;
    //
    public $pluralize = false;

    public function init()
    {
        parent::init();
    }

    /**
     * @inheritDoc
     */
    public function checkModelOwner()
    {
        if ($this->checkAccessToken() && !hasAdminRights()) {

            if (!$id = Yii::$app->request->getQueryParam('id')) {
                throw new \yii\web\BadRequestHttpException(_t('The request does not include field \'id\''));
            }

            if (!$model = ($this->modelClass)::findOne($id)) {
                throw new NotFoundHttpException(_t('The requested resource does not exist.'));
            }

            if (!isset($model->id_user)) {
                return true;
            }

            if ($model->id_user !== $this::$user->id) {
                throw new ForbiddenHttpException(_t('User has no priviledge for this model.'));
            }
        }

        return true;
    }

    public function behaviors()
    {
        return ArrayHelper::merge([
            'access' => [
                'class' => AccessControl::class,
                'rules' => [
                    [
                        'actions' => [
                            'all-my-stations',
                        ],
                        'allow' => true,
                        'matchCallback' => function ()
                        {
                            return $this->checkAccessToken(true);
                        },
                    ],
                    [
                        'actions' => [
                            'index',
                            'view',
                            'create',
                            'search',
                            'pair-wigigs',
                            'unpair-wigigs',
                        ],
                        'allow' => true,
                        'matchCallback' => function ()
                        {
                            return $this->checkAccessToken();
                        },
                    ],
                    [
                        'actions' => [
                            'update',
                            'delete',
                            'unpublish',
                            'publish',
                            'change-type-58-to-52',
                            'relength',
                        ],
                        'allow' => true,
                        'matchCallback' => function ()
                        {
                            return $this->checkModelOwner();
                        },
                    ],
                    [
                        // TODO add restrictions
                        'actions' => [
                            'station',
                            'prolong',
                            'geo-station',
                            'geo-stations',
                            'geo-conflict-stations',
                            'geo-exclusion-zone-stations',
                            'conflict-lines',
                            'lines',
                            'pair-id',
                            'all-stations',
                            'station-popup',
                            'data',
                            'filters',
                            'unpublish-station',
                            'delete-station',
                            'stations-from-position',
                            'options',
                        ],
                        'allow' => true,
                    ],

                ],
            ],
            'verbs' => [
                'actions' => $this->verbs(),
            ],
        ], parent::behaviors());
    }

    /**
     * {@inheritdoc}
     */
    public function actions()
    {
        $actions = parent::actions();

        foreach ([
                     'create',
                     'update',
                     'delete',
                     'index',
                 ] as $a) {
            unset($actions[$a]);
        }

        return $actions;
    }

    /**
     * @OA\Post(
     *     path="/station",
     *     summary="Create new station",
     *     tags={"Station"},
     *     @OA\RequestBody(
     *          @OA\MediaType(
     *              mediaType="application/json",
     *
     *              @OA\Schema(
     *                  oneOf = {
     *                     @OA\Schema(ref = "#/components/schemas/stationForm_fs"),
     *                     @OA\Schema(ref = "#/components/schemas/stationForm_wigig"),
     *                     @OA\Schema(ref = "#/components/schemas/stationForm_wifi_5_2"),
     *                     @OA\Schema(ref = "#/components/schemas/stationForm_wifi_5_8"),
     *                  },
     *              )
     *          )
     *     ),
     *
     *     @OA\Response(
     *       response=200,
     *       description="",
     *       @OA\JsonContent(
     *           @OA\Property(
     *               property="status",
     *               title="Response status code",
     *               type="integer",
     *           ),
     *           @OA\Property(
     *               property="data",
     *               title="Response data",
     *               ref="#/components/schemas/station",
     *           ),
     *       ),
     *     ),
     *
     *     @OA\Response(response=400, ref="#/components/responses/AccessTokenMissingResponse"),
     *     @OA\Response(response=422, ref="#/components/responses/DataValidationFailed"),
     *
     *     security={
     *         {"access-token":{}}
     *     },
     * )
     */
    public function actionCreate()
    {
        $post = \Yii::$app->request->post();

        $sco = new StationCrudHelper();
        $sco->create($post);

        return $sco->getResponse();
    }

    /**
     * @OA\Get(
     *     path="/station",
     *     summary="List my stations (including ones from user alliance)",
     *     tags={"Station"},
     *     security={
     *         {"access-token":{}}
     *     },
     *
     *     @OA\Response(
     *         response=200,
     *         description="List",
     *         @OA\JsonContent(
     *             @OA\Property(
     *                 property="status",
     *                 title="Response status code",
     *                 type="integer",
     *                 example=200,
     *             ),
     *             @OA\Property(
     *                 property="data",
     *                 type="object",
     *
     *                 @OA\Property(
     *                     property="items",
     *                     title="Array of stations",
     *                     type="array",
     *                     @OA\Items(
     *                          oneOf = {
     *                              @OA\Schema(ref = "#/components/schemas/stationDetail_fs"),
     *                              @OA\Schema(ref = "#/components/schemas/stationDetail_wigig_item"),
     *                              @OA\Schema(ref = "#/components/schemas/stationDetail_wifi_5_8_item"),
     *                              @OA\Schema(ref = "#/components/schemas/stationDetail_wifi_5_2_item"),
     *                          },
     *                      ),
     *                 ),
     *
     *                 @OA\Property(
     *                     property="_links",
     *                     ref="#/components/schemas/actionPaginationLinks"),
     *                 ),
     *                 @OA\Property(
     *                     property="_meta",
     *                     ref="#/components/schemas/actionPaginationMeta"),
     *                 ),
     *             ),
     *         ),
     *     ),
     */
    public function actionIndex()
    {
        /* @var $searchModel StationSearch */
        $searchModel = new $this->searchModelClass();

        return $searchModel->searchMy();
    }


    /**
     *
     * @OA\Get(
     *     path="/station/{id}",
     *     summary="Detail",
     *     tags={"Station"},
     *
     *     @OA\Parameter(
     *         name="id",
     *         in="path",
     *         description="Station id",
     *         required=true,
     *         @OA\Schema(
     *           type="integer",
     *         ),
     *         style="form"
     *     ),
     *
     *     @OA\Response(
     *       response=200,
     *       description="",
     *       @OA\JsonContent(
     *           @OA\Property(property="status", title="Response status code", type="integer", example=200),
     *           @OA\Property(property="data", title="Response data", type="object",
     *              oneOf = {
     *                  @OA\Schema(ref = "#/components/schemas/stationDetail_fs"),
     *                  @OA\Schema(ref = "#/components/schemas/stationDetail_wigig"),
     *                  @OA\Schema(ref = "#/components/schemas/stationDetail_wifi_5_8"),
     *                  @OA\Schema(ref = "#/components/schemas/stationDetail_wifi_5_2"),
     *              },
     *          ),
     *       ),
     *     ),
     *     security={
     *         {"access-token":{}}
     *     },
     * )
     *
     */

    /**
     * @OA\Delete(path="/station/{id}",
     *   summary="Delete",
     *   tags={"Station"},
     *     @OA\Parameter(
     *         name="id",
     *         in="path",
     *         description="ID",
     *         required=true,
     *         @OA\Schema(
     *           type="integer",
     *         ),
     *         style="form"
     *     ),
     *     @OA\Response(
     *         response=200,
     *         description="",
     *         @OA\JsonContent(
     *             @OA\Property(
     *                 property="status",
     *                 title="Response status code",
     *                 type="integer",
     *                 example=200,
     *             ),
     *             @OA\Property(
     *                 property="data",
     *                 title="Response data",
     *                 ref="#/components/schemas/commonDeletedResponse",
     *             ),
     *         ),
     *     ),
     *     security={
     *         {"access-token":{}}
     *     },
     * )
     *
     */
    public function actionDelete($id)
    {
        $sco = new StationCrudHelper();
        $sco->delete($id);

        return $sco->getResponse();
    }

    /**
     * @OA\Patch(
     *     path="/station/{id}",
     *     summary="Update",
     *     tags={"Station"},
     *
     *     @OA\Parameter(
     *         name="id",
     *         in="path",
     *         description="ID",
     *         required=true,
     *         @OA\Schema(
     *           type="integer",
     *         ),
     *         style="form"
     *     ),
     *
     *     @OA\RequestBody(
     *          @OA\MediaType(
     *              mediaType="application/json",
     *
     *              @OA\Schema(
     *                  oneOf = {
     *                     @OA\Schema(ref = "#/components/schemas/stationForm_fs_forUpdate"),
     *                     @OA\Schema(ref = "#/components/schemas/stationForm_wigig_forUpdate"),
     *                     @OA\Schema(ref = "#/components/schemas/stationForm_wifi_5_2_forUpdate"),
     *                     @OA\Schema(ref = "#/components/schemas/stationForm_wifi_5_8_forUpdate"),
     *                  },
     *              )
     *          )
     *     ),
     *
     *     @OA\Response(
     *       response=200,
     *       description="",
     *       @OA\JsonContent(
     *           @OA\Property(property="status", title="Response status code", type="integer", example=200),
     *           @OA\Property(property="data", title="Response data", type="object",
     *              oneOf = {
     *                  @OA\Schema(ref = "#/components/schemas/stationDetail_fs"),
     *                  @OA\Schema(ref = "#/components/schemas/stationDetail_wigig"),
     *                  @OA\Schema(ref = "#/components/schemas/stationDetail_wifi_5_8"),
     *                  @OA\Schema(ref = "#/components/schemas/stationDetail_wifi_5_2"),
     *              },
     *          ),
     *       ),
     *     ),
     *     @OA\Response(response="200 (Conflict)", ref="#/components/responses/StationConflictOnUpdateWhenPublished"),
     *     @OA\Response(response=400, ref="#/components/responses/AccessTokenMissingResponse"),
     *     @OA\Response(response=422, ref="#/components/responses/DataValidationFailed"),
     *
     *     security={
     *         {"access-token":{}}
     *     },
     * )
     */
    public function actionUpdate($id)
    {
        $post = \Yii::$app->request->post();

        $sco = new StationCrudHelper();
        $sco->update($id, $post);

        return $sco->getResponse();
    }

    /**
     * @OA\Patch(
     *     path="/station/{id}/unpublish",
     *     summary="Unpublish station",
     *     tags={"Station"},
     *
     *     @OA\Parameter(
     *         name="id",
     *         in="path",
     *         description="ID",
     *         required=true,
     *         @OA\Schema(
     *           type="integer",
     *         ),
     *         style="form"
     *     ),
     *
     *     @OA\Response(
     *       response=200,
     *       description="",
     *       @OA\JsonContent(
     *           @OA\Property(property="status", title="Response status code", type="integer", example=200),
     *           @OA\Property(property="data", title="Response data", type="object",
     *              oneOf = {
     *                  @OA\Schema(ref = "#/components/schemas/stationDetail_fs"),
     *                  @OA\Schema(ref = "#/components/schemas/stationDetail_wigig"),
     *                  @OA\Schema(ref = "#/components/schemas/stationDetail_wifi_5_8"),
     *                  @OA\Schema(ref = "#/components/schemas/stationDetail_wifi_5_2"),
     *              },
     *          ),
     *       ),
     *     ),
     *     security={
     *         {"access-token":{}}
     *     },
     * )
     */
    public function actionUnpublish($id)
    {
        $station = $this->findModel($id);

        if(!$station->isPublished()){
            throw new ConflictHttpException(_t('Only published station can be unpublished', 'station'));
        }

        if ($station->moveToUnpublished()) {
            return $station;
        }

        throw new ServerErrorHttpException(_t('Unpublishing failed', 'station'));
    }

    /**
     * @OA\Patch(
     *     path="/station/{id}/publish",
     *     summary="Publish station",
     *     tags={"Station"},
     *
     *     @OA\Parameter(
     *         name="id",
     *         in="path",
     *         description="ID",
     *         required=true,
     *         @OA\Schema(
     *           type="integer",
     *         ),
     *         style="form"
     *     ),
     *
     *     @OA\RequestBody(
     *          @OA\MediaType(
     *              mediaType="application/json",
     *              @OA\Schema(
     *                 @OA\Property(property="solveConflictByDeclaration", title="Solving conflict by declaration",
     *     type="boolean", example=true),
     *              )
     *          )
     *     ),
     *
     *     @OA\Response(
     *       response=200,
     *       description="",
     *       @OA\JsonContent(
     *           @OA\Property(property="status", title="Response status code", type="integer", example=200),
     *           @OA\Property(property="data", title="Response data", type="object",
     *              oneOf = {
     *                  @OA\Schema(ref = "#/components/schemas/stationDetail_fs"),
     *                  @OA\Schema(ref = "#/components/schemas/stationDetail_wigig"),
     *                  @OA\Schema(ref = "#/components/schemas/stationDetail_wifi_5_8"),
     *                  @OA\Schema(ref = "#/components/schemas/stationDetail_wifi_5_2"),
     *              },
     *          ),
     *       ),
     *     ),
     *
     *     @OA\Response(response=400, ref="#/components/responses/AccessTokenMissingResponse"),
     *     @OA\Response(response=422, ref="#/components/responses/DataValidationFailed"),
     *     @OA\Response(response=409, ref="#/components/responses/StationConflict"),
     *
     *     security={
     *         {"access-token":{}}
     *     },
     * )
     */
    public function actionPublish($id)
    {
        $model = $this->findModel($id);

        $model->published_by = Station::PUBLISHED_BY_PUBLICATION;

        if ($model->type == FrontendStationController::TYPE_58_TG ||
            $model->type == FrontendStationController::TYPE_58_AP ||
            $model->type == FrontendStationController::TYPE_52) {
            $model->setScenario(FrontendStationController::STEP_4_SUMMARY);
        }

        if (!$model->isReadyToBePublished()) {
            throw new ConflictHttpException(_tF('not ready to be published', 'station'));
        }

        $t = Yii::$app->db->beginTransaction();
        try {

            if ($model->status == Station::STATUS_FINISHED && $model->hasWaitingApiChanges()) {
                //
                //  if already published but some changes are waiting for publication

                // temporarily lower status
                $model->moveToStatus($model::STATUS_WAITING);

                // apply waiting changes as update operation
                $sch = new StationCrudHelper();
                if ($sch->update($model->id_master, $model->getWaitingApiChangesAsArray())) {
                    // if its ok, discard waiting changes and continue with Publish logic
                    $model->waiting_api_changes_json = null;
                    if (!$model->save(true, ['waiting_api_changes_json'])) {
                        throw new ServerErrorHttpException(_t('Discarding already applied changes failed', 'station'));
                    }
                } else {
                    $t->rollBack();
                    return $sch->getResponse();
                }
            } elseif (!(Station::$statusOrder[$model->status] < Station::$statusOrder[Station::STATUS_FINISHED])) {
                //
                //  if already published or expired
                throw new ConflictHttpException(_tF('station is already published or expired', 'station'));
            }


            // Check conflicts
            $hasConflicts = $model->hasConflictsWithOthers($conflictStations);
            if ($hasConflicts && Yii::$app->request->post('solveConflictByDeclaration')) {
                $model->published_by = Station::PUBLISHED_BY_DECLARATION;
                $hasConflicts = false;
            }
            if ($hasConflicts) {
                $sch = new StationCrudHelper();
                $sch->conflictStations = $conflictStations;

                $t->rollBack();
                return $sch->getResponse(409);
            }

            // If wigig, verify partner
            if ($model->type == FrontendStationController::TYPE_WIGIG) {
                if (!$model->saveMyPartner()) {
                    throw new ConflictHttpException(_tF('partner_is_not_suitable', 'station'));
                }
            }

            if ($model->moveToFinished()) {
                $t->commit();

                return [
                    'published' => $model->id,
                ];
            }
        } catch (\Throwable $e) {
            $t->rollBack();
            throw $e;
        }

        $t->rollBack();
        throw new ServerErrorHttpException(_t('Publishing failed', 'station'));
    }

    /**
     * @OA\Patch(
     *     path="/station/{id}/change-type-58-to-52",
     *     summary="Changing type from 5.8Ghz to 5.2Ghz",
     *     tags={"Station"},
     *
     *     @OA\Parameter(
     *         name="id",
     *         in="path",
     *         description="ID",
     *         required=true,
     *         @OA\Schema(
     *           type="integer",
     *         ),
     *         style="form"
     *     ),
     *
     *     @OA\Response(
     *       response=200,
     *       description="",
     *       @OA\JsonContent(
     *           @OA\Property(property="status", title="Response status code", type="integer", example=200),
     *           @OA\Property(property="data", title="Response data", type="object",
     *              oneOf = {
     *                  @OA\Schema(ref = "#/components/schemas/stationDetail_fs"),
     *                  @OA\Schema(ref = "#/components/schemas/stationDetail_wigig"),
     *                  @OA\Schema(ref = "#/components/schemas/stationDetail_wifi_5_8"),
     *                  @OA\Schema(ref = "#/components/schemas/stationDetail_wifi_5_2"),
     *              },
     *          ),
     *       ),
     *     ),
     *     security={
     *         {"access-token":{}}
     *     },
     * )
     */
    public function actionChangeType58To52($id)
    {
        $station = $this->findModel($id);

        if ($station->changeType58to52()) {
            $station->refresh();

            return $station;
        }

        throw new ServerErrorHttpException(_t('Type changing failed', 'station'));
    }

    /**
     * @OA\Patch(
     *     path="/station/pair-wigigs/{id1}/{id2}",
     *     summary="Pair 2 wigig stations",
     *     tags={"Station"},
     *
     *     @OA\Parameter(
     *         name="id1",
     *         in="path",
     *         description="ID1",
     *         required=true,
     *         @OA\Schema(
     *           type="integer",
     *         ),
     *         style="form"
     *     ),
     *     @OA\Parameter(
     *         name="id2",
     *         in="path",
     *         description="ID2",
     *         required=true,
     *         @OA\Schema(
     *           type="integer",
     *         ),
     *         style="form"
     *     ),
     *
     *     @OA\Response(
     *       response=200,
     *       description="",
     *       @OA\JsonContent(
     *           @OA\Property(property="status", title="Response status code", type="integer", example=200),
     *       ),
     *     ),
     *     security={
     *         {"access-token":{}}
     *     },
     * )
     */
    public function actionPairWigigs($id1, $id2)
    {
        $station1 = $this->findModel($id1);
        $station2 = $this->findModel($id2);

        $t = Yii::$app->db->beginTransaction();
        try {
            // station2 must be published
            if (!$station1->isPublished() || !$station2->isPublished()) {
                throw new ConflictHttpException(_tF('Both stations have to be published', 'station'));
            }

            if (StationWigig::verifyPairingCompatibility($station1, $station2, $error)) {
                $station1->id_station_pair = $station2->id;

                if ($station1->saveMyPartner() && $station1->save()) {
                    $station1->refresh();
                    $station2->refresh();

                    if (
                        $station1->moveToStatus(Station::STATUS_FINISHED) &&
                        $station2->moveToStatus(Station::STATUS_FINISHED)
                    ) {
                        $t->commit();
                        return [null];
                    }
                }
            } else {
                throw new ConflictHttpException($error);
            }

            throw new ServerErrorHttpException(_t('Pairing failed', 'station'));
        } catch (\Throwable $e) {
            $t->rollBack();
            throw $e;
        }
    }

    /**
     * @OA\Patch(
     *     path="/station/unpair-wigigs/{id1}/{id2}",
     *     summary="Unpair 2 wigig stations",
     *     tags={"Station"},
     *
     *     @OA\Parameter(
     *         name="id1",
     *         in="path",
     *         description="ID1",
     *         required=true,
     *         @OA\Schema(
     *           type="integer",
     *         ),
     *         style="form"
     *     ),
     *     @OA\Parameter(
     *         name="id2",
     *         in="path",
     *         description="ID2",
     *         required=true,
     *         @OA\Schema(
     *           type="integer",
     *         ),
     *         style="form"
     *     ),
     *
     *     @OA\Response(
     *       response=200,
     *       description="",
     *       @OA\JsonContent(
     *           @OA\Property(property="status", title="Response status code", type="integer", example=200),
     *       ),
     *     ),
     *     security={
     *         {"access-token":{}}
     *     },
     * )
     */
    public function actionUnpairWigigs($id1, $id2)
    {
        $station1 = $this->findModel($id1);
        $station2 = $this->findModel($id2);

        $t = Yii::$app->db->beginTransaction();
        try {
            // station2 must be published
            if (!$station1->isPublished() || !$station2->isPublished()) {
                throw new ConflictHttpException(_tF('Both stations have to be published', 'station'));
            }

            // verify its a pair
            if (!($station1->id_station_pair == $station2->id && $station2->id_station_pair == $station1->id)) {
                throw new ConflictHttpException(_tF('Stations are not a wigig pair', 'station'));
            }

            // remove link
            $station1->id_station_pair = null;
            $station2->id_station_pair = null;
            if (
                $station1->save(true, ['id_station_pair']) &&
                $station2->save(true, ['id_station_pair'])
            ) {
                $t->commit();
                return [null];
            }

            throw new ServerErrorHttpException(_t('Unpairing failed', 'station'));
        } catch (\Throwable $e) {
            $t->rollBack();
            throw $e;
        }
    }

    /**
     * @OA\Patch(
     *     path="/station/{id}/relength",
     *     summary="Relength",
     *     tags={"Station"},
     *
     *     @OA\Parameter(
     *         name="id",
     *         in="path",
     *         description="ID",
     *         required=true,
     *         @OA\Schema(
     *           type="integer",
     *         ),
     *         style="form"
     *     ),
     *
     *     @OA\Response(
     *       response=200,
     *       description="",
     *       @OA\JsonContent(
     *           @OA\Property(property="status", title="Response status code", type="integer", example=200),
     *           @OA\Property(property="data", title="Response data", type="object",
     *              oneOf = {
     *                  @OA\Schema(ref = "#/components/schemas/stationDetail_fs"),
     *                  @OA\Schema(ref = "#/components/schemas/stationDetail_wigig"),
     *                  @OA\Schema(ref = "#/components/schemas/stationDetail_wifi_5_8"),
     *                  @OA\Schema(ref = "#/components/schemas/stationDetail_wifi_5_2"),
     *              },
     *          ),
     *       ),
     *     ),
     *     security={
     *         {"access-token":{}}
     *     },
     * )
     */
    public function actionRelength($id)
    {
        $station = $this->findModel($id);

        if (!$station->isForProlongation()) {
            throw new ConflictHttpException(_tF('station is not for prolongation', 'station'));
        }

        if ($station->prolongRegistration()) {
            return $station;
        }

        throw new ServerErrorHttpException(_t('Prolonging failed', 'station'));
    }

    protected function findModel($id): ?Station
    {
        $modelClass = $this->modelClass;
        $model = $modelClass::findOne($id);
        if (!$model) {
            throw new NotFoundHttpException(_t('The requested resource does not exist.'));
        }

        return $model;
    }

    /**
     * @OA\Post(
     *     path="/station/search",
     *     summary="Search and filter all stations",
     *     tags={"Station"},
     *     security={
     *         {"access-token":{}}
     *     },
     *
     *     @OA\RequestBody(
     *          @OA\MediaType(
     *              mediaType="application/json",
     *              @OA\Schema(
     *                  ref="#/components/schemas/stationSearchForm"
     *              )
     *          )
     *     ),
     *
     *     @OA\Response(
     *         response=200,
     *         description="List",
     *         @OA\JsonContent(
     *             @OA\Property(
     *                 property="status",
     *                 title="Response status code",
     *                 type="integer",
     *                 example=200,
     *             ),
     *             @OA\Property(
     *                 property="data",
     *                 type="object",
     *
     *                 @OA\Property(
     *                     property="items",
     *                     title="Array of stations",
     *                     type="array",
     *                     @OA\Items(
     *                          oneOf = {
     *                              @OA\Schema(ref = "#/components/schemas/stationDetail_fs"),
     *                              @OA\Schema(ref = "#/components/schemas/stationDetail_wigig_item"),
     *                              @OA\Schema(ref = "#/components/schemas/stationDetail_wifi_5_8_item"),
     *                              @OA\Schema(ref = "#/components/schemas/stationDetail_wifi_5_2_item"),
     *                          },
     *                      ),
     *                 ),
     *
     *                 @OA\Property(
     *                     property="_links",
     *                     ref="#/components/schemas/actionPaginationLinks"),
     *                 ),
     *                 @OA\Property(
     *                     property="_meta",
     *                     ref="#/components/schemas/actionPaginationMeta"),
     *                 ),
     *             ),
     *         ),
     *     ),
     */
    public function actionSearch()
    {
        /* @var $searchModel StationSearch */
        $searchModel = new $this->searchModelClass();

        return $searchModel->search([], Yii::$app->request->post());
    }

    /**
     * We need check access according to explicit allowance
     *
     * @return bool
     * @throws UnauthorizedHttpException
     * @throws \yii\web\HttpException
     */
    public function checkAccessToken($disableCanUseApi = false)
    {
        if (parent::checkAccessToken()) {
            if (!$disableCanUseApi && !static::$user->canUseApi()) {
                throw new UnauthorizedHttpException(\Yii::t('yii', 'You are not allowed to perform this action.'));
            }
            return true;
        }
        return false;
    }
}
