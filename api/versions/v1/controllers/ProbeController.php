<?php
/*
* Copyright 2023 DactylGroup s.r.o.
*
* Licensed under the EUPL, Version 1.2 or – as soon they
will be approved by the European Commission - subsequent
versions of the EUPL (the "Licence");
* You may not use this work except in compliance with the
Licence.
* You may obtain a copy of the Licence at:
*
* https://joinup.ec.europa.eu/software/page/eupl5
*
* Unless required by applicable law or agreed to in
writing, software distributed under the Licence is
distributed on an "AS IS" basis,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
express or implied.
* See the Licence for the specific language governing
permissions and limitations under the Licence.
*/
?>


<?php


namespace api\versions\v1\controllers;


use common\models\Probe;
use common\models\ProbeScan;
use common\models\ProbeScanWlan;
use dactylcore\core\api\RestApiController;
use yii\filters\AccessControl;
use yii\helpers\ArrayHelper;
use yii\web\BadRequestHttpException;
use yii\web\ForbiddenHttpException;
use yii\web\HttpException;

class ProbeController extends RestApiController
{

    public static Probe $probe;

    public static string $access_token;


    public function init()
    {
        parent::init();
    }

    public function behaviors()
    {
        return ArrayHelper::merge(parent::behaviors(), [
            'access' => [
                'class' => AccessControl::class,
                'rules' => [
                    [
                        // TODO add restrictions
                        'actions' => [
                            'scan-info',
                        ],
                        'allow' => true,
                        'matchCallback' => function ($rule, $action) {
                            return $this->checkAccessToken();
                        }
                    ],

                ],
            ],
        ]);
    }

    public function actionScanInfo(string $address, string $time, array $wlans)
    {
        $trans = \Yii::$app->db->beginTransaction();

        # set probe to active state
        if (static::$probe->status == Probe::STATUS_INACTIVE) {
            static::$probe->status = Probe::STATUS_ACTIVE;
            if (!static::$probe->save()) {
                throw new HttpException(500, implode(";", self::$probe->getErrors()));
            }
        }

        # create scan
        $scan = new ProbeScan();
        $scan->id_probe = static::$probe->id;
        $scan->address = $address;
        $scan->time = $time;

        if (!$scan->save()) {
            throw new HttpException(501, implode(";", $scan->getErrors()));
        }

        # create scanned wlans
        foreach ($wlans as $wlan) {
            $scanInfo = new ProbeScanWlan();
            $scanInfo->setAttributes([
                'id_probe_scan' => $scan->id,
                'bssid' => $wlan['BSSID'],
                'rssi' => $wlan['RSSI'],
                'ssid' => $wlan['SSID'],
                'capabilities' => $wlan['capabilities'],
                'frequency' => $wlan['frequency'],
            ]);

            if (!$scanInfo->save()) {
                throw new HttpException(500, self::$probe->errors);
            }
        }

        $trans->commit();
    }

    public function checkAccessToken(): bool
    {
        $access_token = \Yii::$app->request->headers->get('access-token');

        if (is_null($access_token) || empty($access_token) || $access_token == '') {
            throw new BadRequestHttpException('Access token is missing.');
        }

        if ($probe = Probe::find()->andWhere(['access_token' => $access_token, 'deleted' => 0])->one()) {
            self::$access_token = $access_token;
            self::$probe = $probe;

            return true;

            # Expire for access tokens if needed
            /*
            if(time() <= $user->accessToken[$access_token]) {
                return true;
            }*/
        }

        throw new ForbiddenHttpException('Access token is not valid.');
    }



}