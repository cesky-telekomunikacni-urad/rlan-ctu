<?php
/*
* Copyright 2023 DactylGroup s.r.o.
*
* Licensed under the EUPL, Version 1.2 or – as soon they
will be approved by the European Commission - subsequent
versions of the EUPL (the "Licence");
* You may not use this work except in compliance with the
Licence.
* You may obtain a copy of the Licence at:
*
* https://joinup.ec.europa.eu/software/page/eupl5
*
* Unless required by applicable law or agreed to in
writing, software distributed under the Licence is
distributed on an "AS IS" basis,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
express or implied.
* See the Licence for the specific language governing
permissions and limitations under the Licence.
*/
?>


<?php

namespace api\versions\v1\controllers;

use api\versions\v1\models\UserAlliance;
use api\versions\v1\models\search\UserAllianceSearch;
use dactylcore\core\api\RestApiController;
use Yii;
use yii\helpers\ArrayHelper;
use yii\web\ConflictHttpException;
use yii\web\ForbiddenHttpException;
use yii\web\NotFoundHttpException;
use yii\web\ServerErrorHttpException;
use yii\web\UnauthorizedHttpException;

class UserAllianceController extends RestApiController
{
    public $modelClass = UserAlliance::class;
    public $searchModelClass = UserAllianceSearch::class;

    /**
     * @inheritDoc
     */
    public function behaviors(): array
    {
        return ArrayHelper::merge([
            'access' => [
                'rules' => [
                    [
                        'actions' => [
                            'view',
                            'create',
                            'delete',
                            'update',
                            'invite',
                            'promote',
                            'degrade',
                            'exclude',
                        ],
                        'allow' => true,
                        'matchCallback' => function ()
                        {
                            return $this->checkAccessToken();
                        },
                    ],
                    [
                        'actions' => [

                        ],
                        'allow' => true,
                        'matchCallback' => function ()
                        {
                            return $this->checkModelOwner();
                        },
                    ],
                ],
            ],
            'verbs' => [
                'actions' => $this->verbs(),
            ],
        ], parent::behaviors());
    }

    /**
     * {@inheritdoc}
     */
    public function actions()
    {
        return [];
    }

    /**
     * @OA\Post(
     *     path="/user-alliance",
     *     summary="Create new alliance",
     *     tags={"UserAlliance"},
     *     @OA\RequestBody(
     *          @OA\MediaType(
     *              mediaType="application/json",
     *              @OA\Schema(ref="#/components/schemas/userAllianceForm"),
     *          )
     *     ),
     *
     *     @OA\Response(response=200, description="", @OA\JsonContent(
     *           @OA\Property(property="status", title="Response status code", type="integer",),
     *           @OA\Property(property="data", title="Response data", ref="#/components/schemas/userAlliance",),
     *           ),
     *     ),
     *     @OA\Response(response=400, ref="#/components/responses/AccessTokenMissingResponse"),
     *     @OA\Response(response=422, ref="#/components/responses/DataValidationFailed"),
     *     @OA\Response(response="409",description="Only one alliance allowed",@OA\JsonContent(
     *           @OA\Property(property="status",title="Response status code",type="integer",example=409),
     *           @OA\Property(property="error",title="Text error",type="string",
     *                 example="You are already in the user association, it is not possible to create another one."),
     *           ),
     *     ),
     *
     *     security={
     *         {"access-token":{}}
     *     },
     * )
     */
    public function actionCreate()
    {
        if (loggedUser()->isInAlliance()) {
            throw new ConflictHttpException(_tF('cannot_create_alliance_because_already_present', 'user-alliance'));
        }

        $model = new UserAlliance();

        if (
            $model->load(Yii::$app->request->post(), '') &&
            $model->save() &&
            $model->addUserAsAdmin(loggedUser()->id)
        ) {
            return $model;
        }

        return $model->hasErrors() ? $model : [];
    }

    /**
     *
     * @OA\Get(
     *     path="/user-alliance",
     *     summary="Detail of my alliance",
     *     tags={"UserAlliance"},
     *
     *
     *     @OA\Response(
     *       response=200,
     *       description="",
     *       @OA\JsonContent(
     *           @OA\Property(property="status", title="Response status code", type="integer"),
     *           @OA\Property(property="data", title="Response data", ref="#/components/schemas/userAlliance"),
     *       ),
     *     ),
     *     @OA\Response(response=400, ref="#/components/responses/AccessTokenMissingResponse"),
     *     @OA\Response(response=404, ref="#/components/responses/ResourceDoesNotExistResponse"),
     *
     *     security={
     *         {"access-token":{}}
     *     },
     * )
     *
     */
    public function actionView()
    {
        if ($model = loggedUser()->alliance) {
            return $model;
        }

        throw new NotFoundHttpException();
    }

    /**
     * @OA\Delete(path="/user-alliance",
     *   summary="Delete my alliance",
     *   tags={"UserAlliance"},
     *
     *     @OA\Response(response=200, ref="#/components/responses/CommonSuccessResponse"),
     *     @OA\Response(response=400, ref="#/components/responses/AccessTokenMissingResponse"),
     *     @OA\Response(response=404, ref="#/components/responses/ResourceDoesNotExistResponse"),
     *     @OA\Response(response="403",description="Forbidden access (if non-admin alliance member approaches)",
     *      @OA\JsonContent(
     *           @OA\Property(property="status",title="Response status code",type="integer",example=403),
     *           @OA\Property(property="error",title="Text error",type="string",
     *                  example="Only alliance admin can delete this alliance"),
     *      ),
     *     ),
     *
     *     security={
     *         {"access-token":{}}
     *     },
     * )
     *
     */
    public function actionDelete()
    {
        $model = loggedUser()->alliance;

        if ($model) {
            if ($model->isUserAdmin(loggedUser()->id) && $model->delete()) {
                return [];
            } else {
                throw new ForbiddenHttpException(_t('Only alliance admin can delete this alliance', 'user-alliance'));
            }
        }

        throw new NotFoundHttpException();
    }

    /**
     * @OA\Patch(
     *     path="/user-alliance",
     *     summary="Update my alliance",
     *     tags={"UserAlliance"},
     *
     *     @OA\RequestBody(
     *          @OA\MediaType(
     *              mediaType="application/json",
     *              @OA\Schema(
     *                  ref="#/components/schemas/userAllianceForm"
     *              )
     *          )
     *     ),
     *     @OA\Response(response=200,description="",@OA\JsonContent(
     *           @OA\Property(property="status",title="Response status code",type="integer",),
     *           @OA\Property(property="data",title="Response data",ref="#/components/schemas/userAlliance",),
     *       ),
     *     ),
     *     @OA\Response(response="403",description="Forbidden access (if alliance non-member approaches)",
     *      @OA\JsonContent(
     *           @OA\Property(property="status",title="Response status code",type="integer",example=403),
     *           @OA\Property(property="error",title="Text error",type="string",
     *                     example="Only alliance member can update this alliance"),
     *      ),
     *     ),
     *     @OA\Response(response=404, ref="#/components/responses/ResourceDoesNotExistResponse"),
     *
     *     security={
     *         {"access-token":{}}
     *     },
     * )
     */
    public function actionUpdate()
    {
        $model = loggedUser()->alliance;

        if ($model) {
            if ($model->isUserMember(loggedUser()->id)) {

                if ($model->load(Yii::$app->request->post(), '') && $model->save()) {
                    return $model;
                }
            } else {
                throw new ForbiddenHttpException(_t('Only alliance member can update this alliance', 'user-alliance'));
            }
        }

        throw new NotFoundHttpException();
    }

    /**
     * @OA\Post(
     *     path="/user-alliance/invite/{idUser}",
     *     summary="Invite user to alliance",
     *     tags={"UserAlliance"},
     *
     *     @OA\Parameter(name="idUser",in="path",description="User ID",required=true,@OA\Schema(type="integer",),style="form"),
     *
     *     @OA\Response(response=200, ref="#/components/responses/CommonSuccessResponse"),
     *     @OA\Response(response=404, ref="#/components/responses/ResourceDoesNotExistResponse"),
     *     @OA\Response(response="403",description="Forbidden access (if alliance non-member approaches)",
     *      @OA\JsonContent(
     *           @OA\Property(property="status",title="Response status code",type="integer",example=403),
     *           @OA\Property(property="error",title="Text error",type="string",
     *                  example="Only alliance member can update this alliance"),
     *      ),
     *     ),
     *
     *     security={
     *         {"access-token":{}}
     *     },
     * )
     */
    public function actionInvite($idUser)
    {
        $model = loggedUser()->alliance;

        if ($model) {
            if ($model->isUserMember(loggedUser()->id)) {
                if ($model->sendInvitationTo($idUser)) {
                    return [];
                }
            } else {
                throw new ForbiddenHttpException(_t('Only alliance member can update this alliance', 'user-alliance'));
            }
        } else {
            throw new NotFoundHttpException();
        }

        throw new ServerErrorHttpException();
    }

    /**
     * @OA\Patch(
     *     path="/user-alliance/promote/{idUser}",
     *     summary="Promote user in my alliance",
     *     tags={"UserAlliance"},
     *
     *     @OA\Parameter(name="idUser",in="path",description="User ID",required=true,@OA\Schema(type="integer",),style="form"),
     *
     *     @OA\Response(response=200, ref="#/components/responses/CommonSuccessResponse"),
     *     @OA\Response(response=404, ref="#/components/responses/ResourceDoesNotExistResponse"),
     *     @OA\Response(response="403",description="Forbidden access (if non-admin alliance member approaches)",
     *      @OA\JsonContent(
     *           @OA\Property(property="status",title="Response status code",type="integer",example=403),
     *           @OA\Property(property="error",title="Text error",type="string",
     *                  example="Only alliance admin can delete this alliance"),
     *      ),
     *     ),
     *
     *     security={
     *         {"access-token":{}}
     *     },
     * )
     */
    public function actionPromote($idUser)
    {
        $model = loggedUser()->alliance;

        if ($model) {
            if ($model->isUserAdmin(loggedUser()->id)) {

                if ($model->promoteMember($idUser)) {
                    return [];
                }
            } else {
                throw new ForbiddenHttpException(_t('Only alliance admin can update this alliance', 'user-alliance'));
            }
        }

        throw new NotFoundHttpException();
    }

    /**
     * @OA\Patch(
     *     path="/user-alliance/degrade/{idUser}",
     *     summary="Degrade user in my alliance",
     *     tags={"UserAlliance"},
     *
     *     @OA\Parameter(name="idUser",in="path",description="User ID",required=true,@OA\Schema(type="integer",),style="form"),
     *
     *     @OA\Response(response=200, ref="#/components/responses/CommonSuccessResponse"),
     *     @OA\Response(response=404, ref="#/components/responses/ResourceDoesNotExistResponse"),
     *     @OA\Response(response="403",description="Forbidden access (if non-admin alliance member approaches)",
     *      @OA\JsonContent(
     *           @OA\Property(property="status",title="Response status code",type="integer",example=403),
     *           @OA\Property(property="error",title="Text error",type="string",
     *                 example="Only alliance admin can delete this alliance"),
     *      ),
     *     ),
     *
     *     security={
     *         {"access-token":{}}
     *     },
     * )
     */
    public function actionDegrade($idUser)
    {
        $model = loggedUser()->alliance;

        if ($model) {
            if ($model->isUserAdmin(loggedUser()->id)) {

                if ($model->degradeMember($idUser)) {
                    return [];
                }
            } else {
                throw new ForbiddenHttpException(_t('Only alliance admin can update this alliance', 'user-alliance'));
            }
        }

        throw new NotFoundHttpException();
    }

    /**
     * @OA\Patch(
     *     path="/user-alliance/exclude/{idUser}",
     *     summary="Exclude user from my alliance",
     *     tags={"UserAlliance"},
     *
     *     @OA\Parameter(name="idUser",in="path",description="User ID",required=true,@OA\Schema(type="integer",),style="form"),
     *
     *     @OA\Response(response=200, ref="#/components/responses/CommonSuccessResponse"),
     *     @OA\Response(response=404, ref="#/components/responses/ResourceDoesNotExistResponse"),
     *     @OA\Response(response="403",description="Forbidden access (if non-admin alliance member approaches)",
     *      @OA\JsonContent(
     *           @OA\Property(property="status",title="Response status code",type="integer",example=403),
     *           @OA\Property(property="error",title="Text error",type="string",
     *                      example="Only alliance admin can delete this alliance"),
     *      ),
     *     ),
     *
     *     security={
     *         {"access-token":{}}
     *     },
     * )
     */
    public function actionExclude($idUser)
    {
        $model = loggedUser()->alliance;

        if ($model) {
            if ($model->isUserAdmin(loggedUser()->id)) {

                if ($model->excludeMember($idUser)) {
                    return [];
                }
            } else {
                throw new ForbiddenHttpException(_t('Only alliance admin can update this alliance', 'user-alliance'));
            }
        }

        throw new NotFoundHttpException();
    }

    /**
     * We need check access according to explicit allowance
     *
     * @return bool
     * @throws UnauthorizedHttpException
     * @throws \yii\web\HttpException
     */
    public function checkAccessToken()
    {
        if (parent::checkAccessToken()) {
            if (!static::$user->canUseApi()) {
                throw new UnauthorizedHttpException(\Yii::t('yii', 'You are not allowed to perform this action.'));
            }
            return true;
        }
        return false;
    }
}
