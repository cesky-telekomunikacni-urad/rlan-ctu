<?php
/*
* Copyright 2023 DactylGroup s.r.o.
*
* Licensed under the EUPL, Version 1.2 or – as soon they
will be approved by the European Commission - subsequent
versions of the EUPL (the "Licence");
* You may not use this work except in compliance with the
Licence.
* You may obtain a copy of the Licence at:
*
* https://joinup.ec.europa.eu/software/page/eupl5
*
* Unless required by applicable law or agreed to in
writing, software distributed under the Licence is
distributed on an "AS IS" basis,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
express or implied.
* See the Licence for the specific language governing
permissions and limitations under the Licence.
*/
?>


<?php

namespace api\versions\v1\controllers;

use api\versions\v1\components\MessageCreateAction;
use api\versions\v1\components\MessageIndexAction;
use api\versions\v1\models\Message;
use api\versions\v1\models\search\ThreadSearch;
use api\versions\v1\models\Thread;
use dactylcore\core\api\RestApiController;
use Yii;
use yii\helpers\ArrayHelper;
use yii\web\UnauthorizedHttpException;

class ThreadController extends RestApiController
{
    public $modelClass = Thread::class;
    public $searchModelClass = ThreadSearch::class;

    /**
     * @inheritDoc
     */
    public function behaviors(): array
    {
        return ArrayHelper::merge([
            'access' => [
                'rules' => [
                    [
                        'actions' => [
                            'index',
                            'view',
                            'create',
                            'update',

                            'message-index',
                            'message-create',
                        ],
                        'allow' => true,
                        'matchCallback' => function () {
                            return $this->checkAccessToken();
                        },
                    ],
                ],
            ],
            'verbs' => [
                'actions' => ArrayHelper::merge(
                    $this->verbs(),
                    [
                        'message-index' => ['get', 'options'],
                        'message-create' => ['post', 'options'],
                        'options' => ['options'],
                    ]
                ),
            ],
        ], parent::behaviors());
    }

    /**
     * {@inheritdoc}
     */
    public function actions()
    {
        // Special actions for User operations on specific Customer
        return merge(parent::actions(), [
                'create' => [
                    'scenario' => Thread::SCENARIO_API_CREATE
                ],
                'update' => [
                    'scenario' => Thread::SCENARIO_API_UPDATE
                ],
                'message-index' => [
                    'class' => MessageIndexAction::class,
                    'modelClass' => Thread::class,
                    'checkAccess' => [$this, 'checkAccess'],
                ],
                'message-create' => [
                    'class' => MessageCreateAction::class,
                    'modelClass' => Thread::class,
                    'checkAccess' => [$this, 'checkAccess'],
                ],
                'options' => [
                    'resourceOptions' => ['GET', 'PUT', 'DELETE', 'HEAD', 'OPTIONS'],
                ],
            ]
        );
    }


    /**
     * @param string $action
     * @param null|Thread $model
     * @param array $params
     *
     * @throws UnauthorizedHttpException
     */
    public function checkAccess($action = "", $model = null, $params = [])
    {
        if (in_array($action, ['index', 'create'])) {
            return;
        }

        if (in_array($action,
                [
                    'update',
                    'view',
                    'message-index',
                    'message-create',
                ]) &&
            !is_null($model) &&
            (loggedUser()->id == $model->id_sender || loggedUser()->id == $model->id_recipient || loggedUser()->can_use_api)
        ) {
            return;
        }

        throw new UnauthorizedHttpException(\Yii::t('yii', 'You are not allowed to perform this action.'));
    }

    /**
     *
     * @OA\Get(
     *     path="/thread",
     *     summary="List all Threads",
     *     tags={"Thread"},
     *     security={
     *         {"access-token":{}}
     *     },
     *     @OA\Parameter(ref="#/components/parameters/filter"),
     *     @OA\Parameter(ref="#/components/parameters/fields"),
     *     @OA\Parameter(ref="#/components/parameters/expand"),
     *     @OA\Parameter(ref="#/components/parameters/sort"),
     *     @OA\Parameter(ref="#/components/parameters/page"),
     *     @OA\Response(
     *      response=200,
     *      description="List of Threads",
     *      @OA\JsonContent(
     *           @OA\Property(property="status", title="Response status code", type="integer"),
     *           @OA\Property(
     *               property="data",
     *               title="Response items",
     *               type="object",
     *               @OA\Property(
     *                   property="items",
     *                   type="array",
     *                   @OA\Items(
     *                      ref="#/components/schemas/threadSchema"
     *                   ),
     *               ),
     *           ),
     *       ),
     *      ),
     *     @OA\Response(response=400, ref="#/components/responses/AccessTokenMissingResponse"),
     * )
     *
     * @return Thread[]
     */

    /**
     * @OA\Get(
     *     path="/thread/{idThread}",
     *     summary="Detail of communication thread",
     *     tags={"Thread"},
     *     security={
     *         {"access-token":{}}
     *     },
     *     @OA\Parameter(
     *         name="idThread",
     *         in="path",
     *         description="Numeric ID of the Thread",
     *         required=true,
     *         @OA\Schema(
     *           type="integer",
     *         ),
     *         style="form"
     *     ),
     *     @OA\Response(
     *       response=200,
     *       description="",
     *       @OA\JsonContent(
     *           @OA\Property(property="status", title="Response status code", type="integer"),
     *           @OA\Property(property="data", title="Response data", ref="#/components/schemas/threadSchema"),
     *       ),
     *     ),
     *     @OA\Response(response=400, ref="#/components/responses/AccessTokenMissingResponse"),
     *     @OA\Response(response=401, ref="#/components/responses/UnauthorizedAccessResponse"),
     *     @OA\Response(response=404, ref="#/components/responses/ResourceDoesNotExistResponse"),
     *
     *     security={
     *         {"access-token":{}}
     *     },
     * )
     *
     */

    /**
     *
     * @OA\Post(
     *     path="/thread",
     *     summary="Create thread",
     *     tags={"Thread"},
     *     security={
     *         {"access-token":{}}
     *     },
     *     @OA\RequestBody(
     *          @OA\MediaType(
     *              mediaType="multipart/form-data",
     *              @OA\Schema(
     *                  ref="#/components/schemas/threadCreateSchema"
     *              )
     *          )
     *     ),
     *     @OA\Response(
     *       response=201,
     *       description="Data for the new Thread",
     *       @OA\JsonContent(
     *           @OA\Property(property="status", title="Response status code", type="integer"),
     *           @OA\Property(property="data", title="Response data", ref="#/components/schemas/threadSchema"),
     *       ),
     *     ),
     *     @OA\Response(response=400, ref="#/components/responses/AccessTokenMissingResponse"),
     *     @OA\Response(response=422, ref="#/components/responses/DataValidationFailed"),
     * )
     *
     */

    /**
     *
     * @OA\Put(
     *     path="/thread/{idThread}",
     *     summary="Update thread",
     *     tags={"Thread"},
     *     security={
     *         {"access-token":{}}
     *     },
     *     @OA\Parameter(
     *         name="idThread",
     *         in="path",
     *         description="Numeric ID of the Thread",
     *         required=true,
     *         @OA\Schema(
     *           type="integer",
     *         ),
     *         style="form"
     *     ),
     *     @OA\RequestBody(
     *          @OA\MediaType(
     *              mediaType="multipart/form-data",
     *              @OA\Schema(
     *                  ref="#/components/schemas/threadUpdateSchema"
     *              )
     *          )
     *     ),
     *     @OA\Response(
     *       response=201,
     *       description="Data for updated Thread",
     *       @OA\JsonContent(
     *           @OA\Property(property="status", title="Response status code", type="integer"),
     *           @OA\Property(property="data", title="Response data", ref="#/components/schemas/threadSchema"),
     *       ),
     *     ),
     *     @OA\Response(response=400, ref="#/components/responses/AccessTokenMissingResponse"),
     *     @OA\Response(response=401, ref="#/components/responses/UnauthorizedAccessResponse"),
     *     @OA\Response(response=404, ref="#/components/responses/ResourceDoesNotExistResponse"),
     *     @OA\Response(response=422, ref="#/components/responses/DataValidationFailed"),
     * )
     *
     */


    /**
     * @OA\Get(
     *     path="/thread/{idThread}/message",
     *     summary="List all Mesasges of selected Thread",
     *     tags={"Thread"},
     *     security={
     *         {"access-token":{}}
     *     },
     *     @OA\Parameter(ref="#/components/parameters/filter"),
     *     @OA\Parameter(ref="#/components/parameters/fields"),
     *     @OA\Parameter(ref="#/components/parameters/expand"),
     *     @OA\Parameter(ref="#/components/parameters/sort"),
     *     @OA\Parameter(ref="#/components/parameters/page"),
     *     @OA\Parameter(
     *         name="idThread",
     *         in="path",
     *         description="Numeric ID of the Thread of which shows Messages",
     *         required=true,
     *         @OA\Schema(
     *           type="integer",
     *         ),
     *         style="form"
     *     ),
     *     @OA\Response(
     *      response=200,
     *      description="List of messages",
     *      @OA\JsonContent(
     *           @OA\Property(property="status", title="Response status code", type="integer"),
     *           @OA\Property(
     *               property="data",
     *               title="Response items",
     *               type="object",
     *               @OA\Property(
     *                   property="items",
     *                   type="array",
     *                   @OA\Items(
     *                      ref="#/components/schemas/messageSchema"
     *                   ),
     *               ),
     *           ),
     *       ),
     *      ),
     *     @OA\Response(response=400, ref="#/components/responses/AccessTokenMissingResponse"),
     *     @OA\Response(response=401, ref="#/components/responses/UnauthorizedAccessResponse"),
     *     @OA\Response(response=404, ref="#/components/responses/ResourceDoesNotExistResponse"),
     * )
     *
     * @return Message[]
     */

    /**
     *
     * @OA\Post(
     *     path="/thread/{idThread}/message",
     *     summary="Create new Message in selected Thread",
     *     tags={"Thread"},
     *     security={
     *         {"access-token":{}}
     *     },
     *     @OA\Parameter(
     *         name="idThread",
     *         in="path",
     *         description="Numeric ID of the Thread for the new Messages",
     *         required=true,
     *         @OA\Schema(
     *           type="integer",
     *         ),
     *         style="form"
     *     ),
     *     @OA\RequestBody(
     *          @OA\MediaType(
     *              mediaType="multipart/form-data",
     *              @OA\Schema(
     *                  ref="#/components/schemas/messageCreateSchema"
     *              )
     *          )
     *     ),
     *     @OA\Response(
     *      response=201,
     *      description="List of messages",
     *      @OA\JsonContent(
     *           @OA\Property(property="status", title="Response status code", type="integer"),
     *           @OA\Property(
     *               property="data",
     *               title="Response items",
     *               type="object",
     *               @OA\Property(
     *                   property="items",
     *                   type="array",
     *                   @OA\Items(
     *                      ref="#/components/schemas/messageSchema"
     *                   ),
     *               ),
     *           ),
     *       ),
     *      ),
     *     @OA\Response(response=400, ref="#/components/responses/AccessTokenMissingResponse"),
     *     @OA\Response(response=401, ref="#/components/responses/UnauthorizedAccessResponse"),
     *     @OA\Response(response=404, ref="#/components/responses/ResourceDoesNotExistResponse"),
     *     @OA\Response(response=422, ref="#/components/responses/DataValidationFailed"),
     * )
     *
     */
}
