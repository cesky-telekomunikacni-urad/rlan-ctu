<?php
/*
* Copyright 2023 DactylGroup s.r.o.
*
* Licensed under the EUPL, Version 1.2 or – as soon they
will be approved by the European Commission - subsequent
versions of the EUPL (the "Licence");
* You may not use this work except in compliance with the
Licence.
* You may obtain a copy of the Licence at:
*
* https://joinup.ec.europa.eu/software/page/eupl5
*
* Unless required by applicable law or agreed to in
writing, software distributed under the Licence is
distributed on an "AS IS" basis,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
express or implied.
* See the Licence for the specific language governing
permissions and limitations under the Licence.
*/
?>


<?php

namespace api\versions\v1\controllers\traits;

use api\versions\v1\components\SimpleJsonSerializer;
use common\models\search\StationSearch;
use common\models\Station;
use common\models\Station58;
use common\models\StationFs;
use common\models\StationWigig;
use common\models\UserAlliance;
use dactylcore\core\api\RestApiController;
use DateTime;
use frontend\widgets\dactylkit\DactylKit;
use Yii;
use yii\db\Expression;
use yii\filters\AccessControl;
use yii\helpers\ArrayHelper;
use yii\web\BadRequestHttpException;
use yii\web\ForbiddenHttpException;
use yii\web\NotFoundHttpException;
use yii\web\Response;
use frontend\controllers\StationController as FrontendController;

trait OldStationApiTrait
{
    /**
     * @OA\Get(path="/station/all-my-stations",
     *   summary="Finds all my stations",
     *   tags={"Station - older endpoints"},
     *   security={
     *       {"access-token":{}}
     *   },
     *   @OA\Response(
     *     response=200,
     *     description="all available stations for current user",
     *    @OA\JsonContent(
     *             type="array",
     *             @OA\Items(ref="#/components/schemas/myStationItemShort")
     *         ),
     *    ),
     * )
     * Returns list of stations.
     *
     */
    public function actionAllMyStations()
    {
        $fsName = _tF('type_fs', 'station');
        $wigigNamePTMP = _tF('type_wigig_ptmp', 'station');
        $wigigNamePTP = _tF('type_wigig_ptp', 'station');
        $userId = \Yii::$app->getUser()->id;
        $condition = ['id_user' => $userId];

        ini_set('memory_limit', '5128MB');
        $stationsSql = Station::find()->select([
            'id',
            'lat AS lt',
            'lng AS lg',
            'id_station_pair AS ip',
            'id_master AS m',
            'id_m' => 'LPAD(id_master, 7, 0)',
            'type AS t',
            'name AS n',
            'id_user AS u',
            'pair_position AS pp',
            'mac_address AS mac',
            'status AS s',
        ])->where($condition)->createCommand()->rawSql;

        $stations = \Yii::$app->db->createCommand($stationsSql, [])->queryAll();


        $wigigsSql = StationWigig::find()->select(['id_station', 'direction', 'is_ptmp'])->createCommand()->rawSql;
        $wigigsData = \Yii::$app->db->createCommand($wigigsSql, [])->queryAll();
        $wigigs = [];

        foreach ($wigigsData as $item) {
            $wigigs[$item['id_station']] = $item;
        }

        $station58Sql = Station58::find()->select(['id_station', 'is_ap'])->createCommand()->rawSql;
        $station58Data = Yii::$app->db->createCommand($station58Sql, [])->queryAll();
        $stations58 = [];

        foreach ($station58Data as $item) {
            $stations58[$item['id_station']] = $item;
        }

        $out = [];

        foreach ($stations as $station) {
            $station['s'] = _tF("status_" . $station['s'], 'station');
            switch ($station['t']) {
                case 'wigig':
                    $wigig = $wigigs[$station['id']];
                    $station['a'] = $wigig['direction'];

                    $station['tn'] = (($wigig['is_ptmp']) ? $wigigNamePTMP : $wigigNamePTP) . " #{$station['id_m']}";
                    break;
                case 'fs':
                    $station['tn'] = "{$fsName} " . ($station['pp'] == 'a' ? 'A' : 'B') . " #{$station['id_m']}";
                    break;
                case 'ap_58ghz':
                case 'tg_58ghz':
                    $station58 = $stations58[$station['id']];
                    $station['ap'] = $station58['is_ap'];
                    $station['tn'] = $station58['is_ap'] ? 'ap' : 'tg';
                    break;
                default:
                    break;
            }
            $station['lt'] = (float)$station['lt'];
            $station['lg'] = (float)$station['lg'];
            $station['id_m'] = (int)$station['id_m'];
            $station['id'] = (int)$station['id'];
            $station['u'] = (int)$station['u'];


            $out[$station['id']] = $station;
        }

        Yii::$app->response->format = Response::FORMAT_JSON;
        Yii::$app->response->formatters = [
            Response::FORMAT_JSON => [
                'class' => SimpleJsonSerializer::class,
                'prettyPrint' => YII_DEBUG, // use "pretty" output in debug mode
            ],
        ];

        return $out;
    }

    /**
     * @OA\Get(path="/station/all-stations",
     *   summary="Finds all stations",
     *   tags={"Station - older endpoints"},
     *   @OA\Response(
     *     response=200,
     *     description="all available stations for current user (or all finished is no user)",
     *    @OA\JsonContent(
     *             type="array",
     *             @OA\Items(ref="#/components/schemas/stationItemShort")
     *         ),
     *         ),
     * )
     * Returns list of sizes.
     *
     */
    public function actionAllStations()
    {
        $fsName = _tF('type_fs', 'station');
        $wigigNamePTMP = _tF('type_wigig_ptmp', 'station');
        $wigigNamePTP = _tF('type_wigig_ptp', 'station');
        $userId = \Yii::$app->getUser()->id;
        if (!$userId) {
            $condition = ['status' => Station::STATUS_FINISHED];
        } else {
            $condition = ['or', ['status' => Station::STATUS_FINISHED], ['id_user' => $userId]];
        }
        ini_set('memory_limit', '1024MB');
        $stationsSql = Station::find()->select([
            'id',
            'lat AS lt',
            'lng AS lg',
            'id_station_pair AS ip',
            'id_master AS m',
            'id_m' => 'LPAD(id_master, 7, 0)',
            'type AS t',
            'name AS n',
            'id_user AS u',
            'pair_position AS pp',
        ])->where($condition)->createCommand()->rawSql;

        $stations = \Yii::$app->db->createCommand($stationsSql, [])->queryAll();


        $wigigsSql = StationWigig::find()->select(['id_station', 'direction', 'is_ptmp'])->createCommand()->rawSql;
        $wigigsData = \Yii::$app->db->createCommand($wigigsSql, [])->queryAll();
        $wigigs = [];

        foreach ($wigigsData as $item) {
            $wigigs[$item['id_station']] = $item;
        }

        $station58Sql = Station58::find()->select(['id_station', 'is_ap'])->createCommand()->rawSql;
        $station58Data = Yii::$app->db->createCommand($station58Sql, [])->queryAll();
        $stations58 = [];

        foreach ($station58Data as $item) {
            $stations58[$item['id_station']] = $item;
        }

        $out = [];

        foreach ($stations as $station) {
            switch ($station['t']) {
                case 'wigig':
                    $wigig = $wigigs[$station['id']];
                    $station['a'] = $wigig['direction'];

                    $station['tn'] = (($wigig['is_ptmp']) ? $wigigNamePTMP : $wigigNamePTP) . " #{$station['id_m']}";
                    break;
                case 'fs':
                    $station['tn'] = "{$fsName} " . ($station['pp'] == 'a' ? 'A' : 'B') . " #{$station['id_m']}";
                    break;
                case 'ap_58ghz':
                case 'tg_58ghz':
                    $station58 = $stations58[$station['id']];
                    $station['ap'] = $station58['is_ap'];
                    $station['tn'] = $station58['is_ap'] ? 'ap' : 'tg';
                    break;
                default:
                    break;
            }
            $station['lt'] = (float)$station['lt'];
            $station['lg'] = (float)$station['lg'];
            $station['id_m'] = (int)$station['id_m'];
            $station['id'] = (int)$station['id'];
            $station['u'] = (int)$station['u'];


            $out[$station['id']] = $station;
        }

        Yii::$app->response->format = Response::FORMAT_JSON;
        Yii::$app->response->formatters = [
            Response::FORMAT_JSON => [
                'class' => SimpleJsonSerializer::class,
                'prettyPrint' => YII_DEBUG, // use "pretty" output in debug mode
            ],
        ];

        return $out;
    }

    /**
     * @OA\Get(path="/station/geo-stations",
     *   summary="Finds all stations in for of GeoJSON",
     *   tags={"Station - older endpoints"},
     *   @OA\Parameter(
     *         name="userOnly",
     *         in="path",
     *         description="for logged user only",
     *         @OA\Schema(
     *           type="boolean",
     *         ),
     *     ),
     *   @OA\Parameter(
     *         name="idToExclude",
     *         in="path",
     *         description="Station ID to exclude from the list",
     *         @OA\Schema(
     *           type="string",
     *         ),
     *     ),
     *   @OA\Response(
     *     response=200,
     *     description="all available stations for current user (or all finished is no user) in form of GeoJSON",
     *    @OA\JsonContent(ref="#/components/schemas/stationGeoJSON")
     *   ),
     * )
     * Returns list of sizes.
     *
     */
    public function actionGeoStations()
    {
        $key = self::CACHE_GEO_STATIONS;
        $useCache = false;
        $outData = null;

        $userId = \Yii::$app->getUser()->id;
        if (!$userId) {
            $useCache = true;
            $condition = ['status' => Station::STATUS_FINISHED];
        } else {
            $userOnly = \Yii::$app->request->get('userOnly');
            $condition = $userOnly ? ['id_user' => $userId] :
                ['or', ['status' => Station::STATUS_FINISHED], ['id_user' => $userId]];
        }
        if ($useCache) {
            $outData = getCommonCache($key);
        }

        $idToExclude = \Yii::$app->request->get('idToExclude');

        if (!$outData) {
            $q = Station::find()
                        ->select([
                            'id',
                            'lat AS lt',
                            'lng AS lg',
                            'type AS t',
                            'station_wigig.angle_point_lat AS w_lat',
                            'station_wigig.angle_point_lng AS w_lng',
                            'station_wigig.deleted',
                        ])
                        ->leftJoin('station_wigig', 'station_wigig.id_station = station.id')
                        ->leftJoin('station_58', 'station_58.id_station = station.id')
                        ->where($condition)
                        ->andWhere(['IS', 'lat', new Expression('NOT NULL')]);

            if ($idToExclude) {
                $q->andWhere(['!=', 'id_master', $idToExclude]);
            }

            $stationsSql = $q->createCommand()->rawSql;
            $stations = \Yii::$app->db->createCommand($stationsSql, [])->queryAll();

            $out = [];

            if (YII_ENV_DEV && YII_DEBUG) {
                $stations = array_slice($stations, 0, 10000);
            }

            foreach ($stations as $station) {
                $ar = [
                    'type' => 'Feature',
                    'geometry' => [
                        'type' => 'Point',
                        'coordinates' => [$station['lg'], $station['lt']],
                    ],
                    'properties' => [
                        't' => (string)$station['t'],
                        'w_lat' => $station['w_lat'], // wigig angle point
                        'w_lng' => $station['w_lng'], // wigig angle point
                    ],
                    'id' => (int)$station['id'],
                ];

                $out[] = $ar;
            }

            Yii::$app->response->format = Response::FORMAT_JSON;
            Yii::$app->response->formatters = [
                Response::FORMAT_JSON => [
                    'class' => SimpleJsonSerializer::class,
                    'prettyPrint' => YII_DEBUG, // use "pretty" output in debug mode
                ],
            ];

            $outData = json_encode([
                'type' => 'FeatureCollection',
                'features' => $out,
            ]);
            if ($useCache) {
                setCommonCache($key, $outData);
            }
        }

        echo $outData;
        // exit because of avoiding status/data attributes.
        exit();
    }

    /**
     * @OA\Get(path="/station/geo-conflict-stations",
     *   summary="GeoJSON data about vicinity of selected 60GHz station",
     *   tags={"Station - older endpoints"},
     *   @OA\Parameter(
     *         name="id",
     *         in="query",
     *         description="for logged user only",
     *         @OA\Schema(
     *           type="integer",
     *         ),
     *     ),
     *   @OA\Response(
     *     response=200,
     *     description="stations with conflicts in vicinity in form of GeoJSON",
     *    @OA\JsonContent(ref="#/components/schemas/stationGeoJSON")
     *   ),
     * )
     * Returns list of sizes.
     *
     */
    public function actionGeoConflictStations($id)
    {
        $mainModel = Station::find()->andWhere(['id' => $id])->one();
        $swData = Yii::$app->request->get('sw');
        $neData = Yii::$app->request->get('ne');


        if (!$mainModel) {
            throw new NotFoundHttpException('Wrong data. Model doesnt exist');
        }
        if (!$swData || !$neData) {
            throw new BadRequestHttpException('Missing bounds');
        }

        $modelToAdd[$mainModel->id] = $mainModel;
        $pair = null;

        if ($mainModel->isFS()) {
            $pair = StationSearch::getModelsFs($mainModel->id);
            $modelToAdd[$pair->stationB->id] = $pair->stationB;
            $positions[] = [$pair->stationA->lat, $pair->stationA->lng];
            $positions[] = [$pair->stationB->lat, $pair->stationB->lng];
        } else {
            $wigig = $mainModel;
            $positions[] = [$wigig->lat, $wigig->lng];
        }

        $sw = explode(',', $swData);
        $ne = explode(',', $neData);

        $params = [
            'and',
            ['>=', 'lng', (float)$sw[0]],
            ['<=', 'lng', (float)$ne[0]],
            ['>=', 'lat', (float)$sw[1]],
            ['<=', 'lat', (float)$ne[1]],
        ];


        $conditionFinished = ['status' => Station::STATUS_FINISHED];
//        $conditionFinished = [];

        $closestStationsQuery = Station::find()
                                       ->where($conditionFinished)
                                       ->andWhere($params);

        if (YII_ENV_DEV) {
            $closestStationsQuery->limit(10);
        }

        $closestStations = $closestStationsQuery->all();

        $usedIds = [];
        $requestedIds = [];
        foreach ($closestStations as $id => $item) {
            /**
             * @var Station $item
             */
            $requestedIds[] = $item->id_station_pair;
            $usedIds[] = $id;
        }
        $missingIds = array_diff($requestedIds, $usedIds);

        $missingStations = Station::find()->where(['IN', 'id', $missingIds])->indexBy('id')->all();

        /**
         * @var $closeStation Station
         */
        foreach ($closestStations as $closeStation) {
            if ($closeStation->isFs() && !$closeStation->antenna_volume) {
                continue;
            }
            if ($mainModel->isFs()) {
                if ($this->isWithin($pair->stationA, $closeStation)) {
                    $closeStation->compareWith($pair->stationA);
                }
                if ($this->isWithin($pair->stationB, $closeStation)) {
                    $closeStation->compareWith($pair->stationB);
                }
            } else {
                if ($this->isWithin($mainModel, $closeStation)) {
                    $closeStation->compareWith($mainModel);
                }
            }
        }

        $data = $modelToAdd + $closestStations + $missingStations;

        foreach ($data as $station) {
            /* @var $station Station */
            $station->getStationTypeObject();
            $ar = [
                'type' => 'Feature',
                'geometry' => [
                    'type' => 'Point',
                    'coordinates' => [$station->lng, $station->lat],
                ],
                'properties' => [
                    't' => (string)$station->type,
                    'w_lat' => $station->type == 'wigig' ? $station->typeStation->angle_point_lat : '',
                    // wigig angle point
                    'w_lng' => $station->type == 'wigig' ? $station->typeStation->angle_point_lng : '',
                    // wigig angle point*/
                    'c' => $station->hasConflicts,
                ],
                'id' => (int)$station->id,
            ];

            $out[] = $ar;
        }

        Yii::$app->response->format = Response::FORMAT_JSON;
        Yii::$app->response->formatters = [
            Response::FORMAT_JSON => [
                'class' => SimpleJsonSerializer::class,
                'prettyPrint' => YII_DEBUG, // use "pretty" output in debug mode
            ],
        ];

        $outData = json_encode([
            'type' => 'FeatureCollection',
            'features' => $out,
        ]);

        echo $outData;
        // exit because of avoiding status/data attributes.
        exit();
    }

    protected function isWithin(Station $stationA, Station $stationB)
    {

        // 3.5km as max distance of GPS coordinates
        // 40075/360 = 111.3194 = 1 point = 111km
        // 111.3194/3.5 = 31.4056x in one point
        // 1/31.4056 = 0.0317
        // but after testing set to 0.045
        $maxDistance = 0.045;

        $distance = sqrt(pow($stationA->lat - $stationB->lat, 2)) + sqrt(pow($stationA->lng - $stationB->lng, 2));

        return $distance <= $maxDistance;
    }

    /**
     * @OA\Get(path="/station/pair-id/",
     *   summary="Returns an ID of pair station (if any)",
     *   tags={"Station - older endpoints"},
     *     @OA\Parameter(
     *         name="id",
     *         in="query",
     *         description="ID of station which you want the pair for",
     *         required=true,
     *         @OA\Schema(
     *           type="integer",
     *         ),
     *         style="form"
     *     ),
     *   @OA\Response(
     *     response=200,
     *     description="pair station ID",
     *    @OA\JsonContent(
     *             type="integer",
     *             description="pair station ID",
     *    ),
     *   ),
     * )
     */
    public function actionPairId($id)
    {
        $model = Station::find()->andWhere(['id' => $id])->one();

        if ($model == null) {
            throw new NotFoundHttpException();
        }

        if ($model->type == FrontendController::TYPE_FS) {
            $pair = StationSearch::getModelsFs($model->id);
            echo $pair->stationA->id == $id ? $pair->stationB->id : $pair->stationA->id;
        }

        exit();
    }

    /**
     * @OA\Get(path="/station/station-popup/{id}",
     *   summary="Returns data needed for the popup in the map",
     *   tags={"Station - older endpoints"},
     *     @OA\Parameter(
     *         name="id",
     *         in="path",
     *         description="ID of station which you want the popup for",
     *         required=true,
     *         @OA\Schema(
     *           type="integer",
     *         ),
     *         style="form"
     *     ),
     *   @OA\Response(
     *     response=200,
     *     description="HTML with data for popup",
     *     @OA\JsonContent(
     *             type="string",
     *             description="HTML with data for the popup",
     *    ),
     *   ),
     * )
     */
    public function actionStationPopup($id)
    {
        /** @var Station $station */
        $station = Station::find()->where(['id' => $id])->one();
        if (!$station) {
            throw new NotFoundHttpException();
        }

        $stationSpecific = $station->getStationTypeObject();
        $models[] = [
            'name' => (trim($station->name) == false) ? $station->getTypeName() : $station->name,
            'typeName' => $station::getStaticTypeName($station->type, $station->id),
            'id' => $station->formatThisId(),
            'realId' => $station->id,
            'lat' => $station->lat,
            'lng' => $station->lng,
            'position' => $station->pair_position,
            'direction' => (isset($stationSpecific->direction) ? $stationSpecific->direction : null),
        ];

        // Get all on the same GPS
        $stationsOnGPS = Station::find()
                                ->andWhere(['lng' => $station->lng, 'lat' => $station->lat])
                                ->andWhere(['<>', 'id', $id])
                                ->all();

        foreach ($stationsOnGPS as $station) {
            $stationSpecific = $station->getStationTypeObject();
            $models[] = [
                'name' => (trim($station->name) == false) ? $station->getTypeName() : $station->name,
                'typeName' => $station::getStaticTypeName($station->type, $station->id),
                'id' => $station->formatThisId(),
                'realId' => $station->id,
                'lat' => $station->lat,
                'lng' => $station->lng,
                'position' => $station->pair_position,
                'direction' => (isset($stationSpecific->direction) ? $stationSpecific->direction : null),
            ];
        }

        return $this->renderPartial('popup', [
            'models' => $models,
        ]);
    }

    public function actionLines()
    {
        // use cache when the request wants just the ALL stations (because it is manageable from Station::save). We don't have the wildcard for cache deleting
        $key = self::CACHE_LINES;
        $useCache = false;
        $outData = null;
        $idToExclude = \Yii::$app->request->get('idToExclude');

        $userId = \Yii::$app->getUser()->id;
        if (!$userId) {
            $useCache = true;
            $condition = ['status' => Station::STATUS_FINISHED];
        } else {
            $userOnly = \Yii::$app->request->get('userOnly');
            $condition = $userOnly ? ['id_user' => $userId] :
                ['or', ['status' => Station::STATUS_FINISHED], ['id_user' => $userId]];
        }
        if ($useCache) {
            $outData = getCommonCache($key);
        }

        if (!$outData) {
            $q = Station::find()->select(
                [
                    'id',
                    'id_user AS iu',
                    'lat AS lt',
                    'lng AS lg',
                    'id_station_pair AS ip',
                    'id_master AS m',
                    'id_m' => 'LPAD(id_master, 7, 0)',
                    'type AS t',
                    'name AS n',
                    'id_user AS u',
                    'pair_position AS pp',
                ])->where($condition);

            if ($idToExclude) {
                $q->andWhere(['!=', 'id_master', $idToExclude]);
            }

            if (YII_ENV_DEV) {
                $q->limit(50000);
            }

            $stationsSql = $q->createCommand()->rawSql;

            $stations = \Yii::$app->db->createCommand($stationsSql, [])->queryAll();

            $lines = [];
            $userId = Yii::$app->getUser()->id ?: 0;

            foreach ($stations as $station) {

                if ($station['t'] == 'fs') {
                    if (isset($lines[$station['m']])) {
                        $lines[$station['m']]['gps'][] = [(float)$station['lg'], (float)$station['lt']];
                    } else {
                        $lines[$station['m']] = [
                            'gps' =>
                                [[(float)$station['lg'], (float)$station['lt']]],
                            'my' => $userId == $station['iu'],
                        ];
                    }
                }
            }

            $lines = array_map(function ($item)
            {
                return [
                    'type' => 'Feature',
                    'geometry' => [
                        'type' => 'LineString',
                        'coordinates' =>
                            $item['gps'],
                    ],
                    'properties' => [
                        'my' => $item['my'],
                    ],
                ];
            }, array_values($lines));

            Yii::$app->response->format = Response::FORMAT_JSON;
            Yii::$app->response->formatters = [
                Response::FORMAT_JSON => [
                    'class' => SimpleJsonSerializer::class,
                    'prettyPrint' => YII_DEBUG, // use "pretty" output in debug mode
                ],
            ];

            $outData = json_encode([
                'type' => 'FeatureCollection',
                'features' => $lines,
            ]);
            if ($useCache) {
                setCommonCache($key, $outData);
            }
        }

        echo $outData;
        // exit because of avoiding status/data attributes.
        exit();
    }

    public function actionConflictLines()
    {
        $sw = explode(',', Yii::$app->request->get('sw'));
        $ne = explode(',', Yii::$app->request->get('ne'));

        $params = [
            'and',
            ['>=', 'lng', (float)$sw[0]],
            ['<=', 'lng', (float)$ne[0]],
            ['>=', 'lat', (float)$sw[1]],
            ['<=', 'lat', (float)$ne[1]],
            ['status' => Station::STATUS_FINISHED],
        ];

        $stationsSql = Station::find()->select(
            [
                'id',
                'lat AS lt',
                'lng AS lg',
                'id_station_pair AS ip',
                'id_master AS m',
                'id_m' => 'LPAD(id_master, 7, 0)',
                'type AS t',
                'name AS n',
                'id_user AS u',
                'pair_position AS pp',
            ])->where($params)->createCommand()->rawSql;

        $stations = \Yii::$app->db->createCommand($stationsSql, [])->queryAll();

        $lines = [];

        foreach ($stations as $station) {

            if ($station['t'] == 'fs') {
                if (isset($lines[$station['m']])) {
                    $lines[$station['m']][] = [(float)$station['lg'], (float)$station['lt']];
                } else {
                    $lines[$station['m']] = [
                        [(float)$station['lg'], (float)$station['lt']],
                    ];
                }
            }
        }

        $lines = array_map(function ($item)
        {
            return [
                'type' => 'Feature',
                'geometry' => [
                    'type' => 'LineString',
                    'coordinates' =>
                        $item,
                ],
                'properties' => [],
            ];
        }, array_values($lines));

        Yii::$app->response->format = Response::FORMAT_JSON;
        Yii::$app->response->formatters = [
            Response::FORMAT_JSON => [
                'class' => SimpleJsonSerializer::class,
                'prettyPrint' => YII_DEBUG, // use "pretty" output in debug mode
            ],
        ];

        $outData = json_encode([
            'type' => 'FeatureCollection',
            'features' => $lines,
        ]);

        echo $outData;
        // exit because of avoiding status/data attributes.
        exit();
    }

    public function actionData()
    {
        $post = Yii::$app->request->post();
        $pageSize = $post['perPage'];
        $selectedIds = $post['selectedIds'] ?? [];
        $q = Station::find()->where([])->select([
            'lng',
            'lat',
            'id',
            'id_user',
            'id_master',
            'id_m' => 'LPAD(id_master, 7, 0)',
            'name',
            'type',
            'status',
            'pair_position',
            'valid_to',
            'protected_to',
            'hardware_identifier_code' => 'IF(hardware_identifier, serial_number, mac_address)',
        ]);

        // Is Admin?
        if (($post['idUserForData'] ?? false) && hasAccessTo('app-frontend_station_update-all')) {
            $userId = $post['idUserForData'];
        } else {
            $userId = \Yii::$app->getUser()->id;
        }

        if ($post['search']) {
            $searchTerm = $post['search'];
            $conds = [
                'or',
                ['LIKE', 'name', $searchTerm],
                ['LIKE', 'id', $searchTerm],
            ];

            if (userIsLogged()) {
                $conds = array_merge($conds, [
                    [
                        'AND',
                        ['LIKE', 'mac_address', $searchTerm],
                        ['id_user' => $userId],
                    ],
                    [
                        'AND',
                        ['LIKE', 'serial_number', $searchTerm],
                        ['id_user' => $userId],
                    ],
                ]);
            }

            $q->andFilterWhere($conds);
        }

        if ($post['types']) {
            $q->andFilterWhere(['IN', 'type', $post['types']]);
        }

        if (!$userId) {
            // public
            $q->andFilterWhere(['status' => Station::STATUS_FINISHED]);
        } else {
            if ($post['statuses']) {
                if ($post['myOnly']) {
                    $q->andFilterWhere(['AND', ['IN', 'status', $post['statuses']], ['id_user' => $userId]]);
                } else {
                    $publicStatuses = array_intersect($post['statuses'], [Station::STATUS_FINISHED]);
                    // want by all users
                    $q->andFilterWhere([
                        'OR',

                        // all statuses but it has to be mine
                        ['AND', ['IN', 'status', $post['statuses']], ['id_user' => $userId]],

                        // other users but public statuses
                        ['IN', 'status', $publicStatuses],
                    ]);
                }
            } else {
                if ($post['myOnly']) {
                    $q->andFilterWhere(['id_user' => $userId]);
                } else {
                    $q->andFilterWhere(['OR', ['status' => Station::STATUS_FINISHED], ['id_user' => $userId]]);
                }
            }
        }


        if ($post['bounds']['east']) {
            $q->andFilterWhere([
                'or',
                ['IS', 'lat', new Expression('NULL')],
                ['IS', 'lng', new Expression('NULL')],
                [
                    'and',
                    ['<=', 'lng', $post['bounds']['east']],
                    ['>=', 'lng', $post['bounds']['west']],
                    ['<=', 'lat', $post['bounds']['north']],
                    ['>=', 'lat', $post['bounds']['south']],
                ],
            ]);
        }

        if (!(isset($post['sortByExpiration']) && $post['sortByExpiration'])) {
            if ($post['sort']['field']) {
                $direction = $post['sort']['type'] == 'asc' ? SORT_ASC : SORT_DESC;
                // in case that sort by id is requested from user profile, rename id attribute id_orig => id
                if ($post['sort']['field'] == 'id_orig') {
                    $q->orderBy(['id' => $direction]);
                } else {
                    $q->orderBy([$post['sort']['field'] => $direction]);
                }
            } else {
                $q->orderBy(['id' => SORT_DESC]);
            }
        } else {
            $q->orderBy(['valid_to' => SORT_ASC]);
        }

        $total = $q->count();
        $q->offset(($post['page'] - 1) * ($pageSize))->limit($pageSize);

        $data = $q->asArray()->all();

        // TODO nacachovat
        $translatedStatuses = [];
        foreach (Station::AVAILABLE_STATUSES as $status) {
            $translatedStatuses[$status] = _tF("status_{$status}", 'station');
        }
        $stateIconMap = [
            'danger' => DactylKit::icon(DactylKit::ICON_ERROR_24),
            'warning' => DactylKit::icon(DactylKit::ICON_WARNING_24),
            'ok' => DactylKit::icon(DactylKit::ICON_SUCCESS),
            'na' => DactylKit::icon(DactylKit::ICON_CIRCLE),
        ];
        $magicRights = hasAccessTo('app-frontend_station_update-all');

        $timeWarningReg = strtotime("+" . c('REGISTRATION_PERIOD_WARNING') . " months", time());

        $data = array_map(function ($item) use (
            $translatedStatuses,
            $stateIconMap,
            $magicRights,
            $userId,
            $timeWarningReg,
            $selectedIds
        )
        {
            $item['statusT'] = $translatedStatuses[$item['status']];
            $item['typeT'] = Station::getStaticTypeName($item['type'], $item['id']);
            $item['id_orig'] = $item['id'];
            $item['id'] = Station::formatId($item['id_master']);
            $item['valid_to_class'] = self::defineGridValidClass($item['status'], $item['valid_to']);
            $item['protected_to_class'] = self::defineGridProtectedClass($item['status'], $item['protected_to']);
            $item['can_edit'] = $magicRights || ($userId == $item['id_user']);
            $item['prolong'] = ($item['valid_to'] < $timeWarningReg);
            $item['selectable'] = ($item['valid_to'] < $timeWarningReg);
            $item['vgtSelected'] = in_array($item['id_orig'], $selectedIds);

            $item['state'] = self::defineGridIcon($item['status'], $item['valid_to']);
            $item['stateIcon'] = $stateIconMap[$item['state']];

            if (empty($item['name'])) {
                $item['name'] = "<i>{$item['typeT']} " . mb_ucfirst($item['pair_position']) . " #{$item['id_m']}</i>";
            }

            if (
                $item['status'] == Station::STATUS_DRAFT ||
                $item['status'] == Station::STATUS_WAITING ||
                $item['status'] == Station::STATUS_UNPUBLISHED
            ) {
                $item['valid_to'] = '-';
                $item['protected_to'] = '-';
            } else {
                $item['valid_to'] = _tF(strftime(_t('date_format', 'stations-table'), $item['valid_to']));
                $item['protected_to'] = _tF(strftime(_t('date_format', 'stations-table'), $item['protected_to']));
            }

            return $item;
        }, $data);

        return [
            'rows' => $data,
            'total' => $total,
            'selectedIds' => $selectedIds,
        ];
    }

    public function actionProlong()
    {
        $post = Yii::$app->request->post();
        $idsToProlong = $post['stationsToProlong'];

        if ($idsToProlong) {
            $stations = Station::find()->where([
                'id' => $idsToProlong,
            ]);

            $out = true;
            $unableToProlong = false;
            /** @var Station $station */
            foreach ($stations->each() as $station) {
                if ($station->isForProlongation() && $this->canEdit($station)) {
                    $out &= $station->prolongRegistration();
                } else {
                    $unableToProlong = true;
                }
            }

            if ($unableToProlong) {
                setInfoFlash(_tF('some stations are not to be prolonged', 'station'));
            }

            if ($out) {
                setSuccessFlash(_tF('stations successfully prolonged', 'station'));
            } else {
                setErrorFlash(_tF('stations failed to be prolonged', 'station'));
            }
        }
        return ['status' => $out];
    }

    public function actionFilters()
    {
        $statuses = [];
        foreach (Station::AVAILABLE_STATUSES as $status) {
            $statuses[]
                = [
                'id' => $status,
                'label' => _tF("status_{$status}", 'station'),
            ];
        }

        return [
            'statuses' => $statuses,
            'types' => [
                [
                    'id' => 'fs',
                    'label' => _tF('type_fs_filter', 'station'),
                ],
                [
                    'id' => 'wigig',
                    'label' => _tF('wigig_filter', 'station'),
                ],
                [
                    'id' => 'ap_58ghz',
                    'label' => _tF('type_58_ap_filter', 'station'),
                ],
                [
                    'id' => 'tg_58ghz',
                    'label' => _tF('type_58_tg_filter', 'station'),
                ],
                [
                    'id' => '52ghz',
                    'label' => _tF('type_52', 'station'),
                ],
            ],
        ];
    }

    /**
     * @OA\Get(path="/station/station/{id}",
     *   summary="Finds station (with pair station)",
     *   tags={"Station - older endpoints"},
     *     @OA\Parameter(
     *         name="id",
     *         in="path",
     *         description="ID of station for which we are searching other stations",
     *         required=true,
     *         @OA\Schema(
     *           type="integer",
     *         ),
     *         style="form"
     *     ),
     *   @OA\Response(
     *     response=200,
     *     description="data for sizes",
     *    @OA\JsonContent(
     *             type="array",
     *             @OA\Items(ref="#/components/schemas/stationItemLong")
     *         ),
     *    ),
     * )
     * Returns list of sizes.
     *
     * @param $id
     *
     * @return array|\yii\db\ActiveRecord[]
     * @throws NotFoundHttpException
     */
    public function actionStation($id)
    {

        $station = Station::findOne(['id' => $id]);

        if (!$station) {
            throw new NotFoundHttpException('station doesnt exist');
        }

        $out = [$station->id => $station];
        if ($station->isFs()) {
            $pair = $station->getStationPair()->one();
            $out[$pair->id] = $pair;
        }
        return $out;
    }

    /**
     * @OA\Get(path="/station/geo-station/{id}",
     *   summary="GeoJSON data about selected station",
     *   tags={"Station - older endpoints"},
     *   @OA\Parameter(
     *         name="id",
     *         in="path",
     *         description="for logged user only",
     *         @OA\Schema(
     *           type="integer",
     *         ),
     *     ),
     *   @OA\Response(
     *     response=200,
     *     description="selected stations in form of GeoJSON",
     *    @OA\JsonContent(ref="#/components/schemas/stationGeoJSON")
     *   ),
     * )
     * Returns list of sizes.
     *
     */
    public function actionGeoStation($id)
    {

        $condition = ['id' => $id];

        $stationSql = Station::find()
                             ->select([
                                 'id',
                                 'id_user AS iu',
                                 'lat AS lt',
                                 'lng AS lg',
                                 'type AS t',
                                 'station_wigig.angle_point_lat AS w_lat',
                                 'station_wigig.angle_point_lng AS w_lng',
                                 'station_wigig.deleted',
                             ])
                             ->leftJoin('station_wigig', 'station_wigig.id_station = station.id')
                             ->where($condition)->createCommand()->rawSql;

        $stations[] = \Yii::$app->db->createCommand($stationSql, [])->queryOne();

        if (!$stations) {
            throw new NotFoundHttpException('station doesnt exist');
        }

        if ($stations[0]['t'] == "fs") {
            $stationFS = Station::find()->where(['id' => $id])->one();
            $stationPairId = $stationFS->getStationPair()->one()->id;

            $stationPairSql = Station::find()
                                     ->select([
                                         'id',
                                         'id_user AS iu',
                                         'lat AS lt',
                                         'lng AS lg',
                                         'type AS t',
                                         'station_wigig.angle_point_lat AS w_lat',
                                         'station_wigig.angle_point_lng AS w_lng',
                                         'station_wigig.deleted',
                                     ])
                                     ->leftJoin('station_wigig', 'station_wigig.id_station = station.id')
                                     ->where(['id' => $stationPairId])->createCommand()->rawSql;

            $stationPair = \Yii::$app->db->createCommand($stationPairSql, [])->queryOne();

            $stations[] = $stationPair;
        }

        $userId = Yii::$app->getUser()->id ?: 0;

        foreach ($stations as $station) {
            $ar = [
                'type' => 'Feature',
                'geometry' => [
                    'type' => 'Point',
                    'coordinates' => [$station['lg'], $station['lt']],
                ],
                'properties' => [
                    'my' => $station['iu'] == $userId,
                    't' => (string)$station['t'],
                    'w_lat' => $station['w_lat'], // wigig angle point
                    'w_lng' => $station['w_lng'], // wigig angle point
                ],
                'id' => (int)$station['id'],
            ];

            $out[] = $ar;
        }

        Yii::$app->response->format = Response::FORMAT_JSON;
        Yii::$app->response->formatters = [
            Response::FORMAT_JSON => [
                'class' => SimpleJsonSerializer::class,
                'prettyPrint' => YII_DEBUG, // use "pretty" output in debug mode
            ],
        ];

        $outData = json_encode([
            'type' => 'FeatureCollection',
            'features' => $out,
        ]);

        echo $outData;
        exit();
    }

    /**
     * @OA\Get(path="/station/geo-exclusion-zone-stations/{id}",
     *   summary="GeoJSON data about vicinity of selected 5.8GHz station",
     *   tags={"Station - older endpoints"},
     *   @OA\Parameter(
     *         name="id",
     *         in="path",
     *         description="for logged user only",
     *         @OA\Schema(
     *           type="integer",
     *         ),
     *     ),
     *   @OA\Response(
     *     response=200,
     *     description="stations in vicinity in form of GeoJSON",
     *    @OA\JsonContent(ref="#/components/schemas/stationGeoJSON")
     *   ),
     * )
     * Returns list of sizes.
     *
     */
    public function actionGeoExclusionZoneStations($id)
    {

        $key = self::CACHE_GEO_EXCLUSION_ZONE . $id;
        $outData = getCommonCache($key);

        if (!$outData) {

            $mainModel = Station::find()->andWhere(['id' => $id])->one();

            $lng = null;
            $lat = null;

            if ($mainModel->type == FrontendController::TYPE_58_AP) {
                $lat = $mainModel->lat;
                $lng = $mainModel->lng;
            } else {
                $lat = $mainModel->lat;
                $lng = $mainModel->lng;
            }

            // 3.5km as max distance of GPS coordinates
            // 40075/360 = 111.3194 = 1 point = 111km
            // 111.3194/3.5 = 31.6056x in one point
            // 1/31.6056 = 0.031441048
            $metreRadius = c('TOLL_GATE_RADIUS');
            $kmRadius = intval($metreRadius) / 1000;

            $params = ['or'];
            $params[] = [
                '<',
                "ACOS(COS(RADIANS({$lat})) * COS(RADIANS(`lat`)) * COS(RADIANS({$lng}) - RADIANS(`lng`))  + SIN(RADIANS({$lat})) * SIN(RADIANS(`lat`))  ) * 6371",
                $kmRadius,
            ];


            $conditionFinished = ['status' => Station::STATUS_FINISHED];
            $conditionId = ['!=', 'id_master', $mainModel->id_master];
            $conditionType = [
                'or',
                ['=', 'type', FrontendController::TYPE_58_AP],
                ['=', 'type', FrontendController::TYPE_58_TG],
            ];

            $closestStationsSql = Station::find()
                                         ->select([
                                             'id',
                                             'lat',
                                             'lng',
                                             'type',
                                         ])
                                         ->where($conditionFinished)
                                         ->andWhere($params)
                                         ->andWhere($conditionId)
                                         ->andWhere($conditionType)
                                         ->createCommand()->rawSql;

            $closestStations = Yii::$app->db->createCommand($closestStationsSql, [])->queryAll();


            $out = [];

            foreach ($closestStations as $station) {

                $station['c'] = false;

                if ($mainModel->type == FrontendController::TYPE_58_AP) {
                    if ($station['type'] == FrontendController::TYPE_58_TG) {
                        $station['c'] = true;
                    }
                } else {
                    if ($station['type'] == FrontendController::TYPE_58_AP) {
                        $station['c'] = true;
                    }
                }

                $ar = [
                    'type' => 'Feature',
                    'geometry' => [
                        'type' => 'Point',
                        'coordinates' => [$station['lng'], $station['lat']],
                    ],
                    'properties' => [
                        't' => $station['type'],
                        'c' => (int)$station['c'],
                    ],
                    'id' => (int)$station['id'],
                ];

                $out[] = $ar;
            }

            Yii::$app->response->format = Response::FORMAT_JSON;
            Yii::$app->response->formatters = [
                Response::FORMAT_JSON => [
                    'class' => SimpleJsonSerializer::class,
                    'prettyPrint' => YII_DEBUG, // use "pretty" output in debug mode
                ],
            ];

            $outData = json_encode([
                'type' => 'FeatureCollection',
                'features' => $out,
            ]);
            setCommonCache($key, $outData);
        }

        echo $outData;
        // exit because of avoiding status/data attributes.
        exit();
    }

    public function actionDeleteStation($id)
    {
        $station = Station::findOne(['id' => $id]);

        if ($this->canEdit($station)) {
            setSuccessFlash(_tF('flash_successfully_deleted', 'station'));
            return Station::deleteStation($id);
        } else {
            setErrorFlash(_tF('error while deleting', 'station'));
            throw new ForbiddenHttpException();
        }
    }

    /*
     * ------------------------|---------------------------------------|-------------------|-------------------------
     * PROTECTED_TO STYLE -    |              normal                   |     warning       |         danger
     * ------------------------|------------------------|--------------|-------------------|-------------------------
     * VALID_TO STYLE     -    |       normal           |    warning   |              danger
     * ------------------------|----------|-------------|--------------|-------------------|-------------------------
     * ICON        NA          | (I.1) OK | (I.2) WARN  |  (I.3)    DANGER     DANGER        DANGER
     * ----|--------|----------|----------|-------------|--------------|-------------------|-------------------------
     *   draft    wait      active       w_v          valid_to        w_p                protected_to
     * w_v = warning before valid_to (x months before)
     * w_p = warning before protected_to (x months before)
     */
    public static function defineGridIcon($status, $validTo)
    {
        if ($status == Station::STATUS_DRAFT ||
            $status == Station::STATUS_WAITING ||
            $status == Station::STATUS_UNPUBLISHED) {
            return Station::GRID_ICON_TYPE_NA;
        } else {
            $time = time();
            if ($time >= $validTo) {
                // after valid_to => problematic (event status is still active) => I.3
                return Station::GRID_ICON_TYPE_DANGER;
            } else {
                $monthsBeforeValidTo = c(Station::PERIOD_WARNING_VALID);
                $dateTimeValidTo = DateTime::createFromFormat('U', $validTo);
                $dateTimeValidTo->modify("-{$monthsBeforeValidTo} months");

                if ($time < $dateTimeValidTo->getTimestamp()) {
                    return Station::GRID_ICON_TYPE_OK;
                } else {
                    return Station::GRID_ICON_TYPE_WARNING;
                }
            }
        }
    }

    public static function defineGridValidClass($status, $validTo)
    {
        if ($status == Station::STATUS_DRAFT ||
            $status == Station::STATUS_WAITING ||
            $status == Station::STATUS_UNPUBLISHED) {
            return Station::GRID_DATE_CLASS_NA;
        } else {
            $time = time();
            if ($time >= $validTo) {
                return Station::GRID_DATE_CLASS_DANGER;
            } else {
                $monthsBeforeValidTo = c(Station::PERIOD_WARNING_VALID);
                $dateTimeValidTo = DateTime::createFromFormat('U', $validTo);

                $dateTimeValidTo->modify("-{$monthsBeforeValidTo} months");

                if ($time < $dateTimeValidTo->getTimestamp()) {
                    return Station::GRID_DATE_CLASS_OK;
                } else {
                    return Station::GRID_DATE_CLASS_WARNING;
                }
            }
        }
    }

    public static function defineGridProtectedClass($status, $protectedTo)
    {
        if ($status == Station::STATUS_DRAFT ||
            $status == Station::STATUS_WAITING ||
            $status == Station::STATUS_UNPUBLISHED) {
            return Station::GRID_DATE_CLASS_NA;
        } else {
            $time = time();
            if ($time >= $protectedTo) {
                return Station::GRID_DATE_CLASS_DANGER;
            } else {
                $monthsBeforeProtectedTo = c(Station::PERIOD_WARNING_PROT);
                $dateTimeProtectedTo = DateTime::createFromFormat('U', $protectedTo);

                $dateTimeProtectedTo->modify("-{$monthsBeforeProtectedTo} months");

                if ($time < $dateTimeProtectedTo->getTimestamp()) {
                    return Station::GRID_DATE_CLASS_OK;
                } else {
                    return Station::GRID_DATE_CLASS_WARNING;
                }
            }
        }
    }

//    TODO jak overit a nemuset povolovat login endpointy

    protected function canEdit(Station $model)
    {
        if ($model->id_user == null) {
            return false;
        }
        return $model->id_user == Yii::$app->getUser()->id ||
            hasAccessTo('app-frontend_station_update-all') ||
            UserAlliance::areInOneAlliance($model->id_user, Yii::$app->getUser()->id);
    }

    /**
     * @OA\Get(path="/station/stations-from-position/{lat}/{lng}/{perimeter}",
     *   summary="Returns stations which are locates X km from selected GPS",
     *   tags={"Station - older endpoints"},
     *     @OA\Parameter(
     *         name="lat",
     *         in="path",
     *         description="LAT in float",
     *         required=true,
     *         @OA\Schema(
     *           type="number",
     *         ),
     *         style="form"
     *     ),
     *     @OA\Parameter(
     *         name="lng",
     *         in="path",
     *         description="LNG in float",
     *         required=true,
     *         @OA\Schema(
     *           type="number",
     *         ),
     *         style="form"
     *     ),
     *     @OA\Parameter(
     *         name="perimeter",
     *         in="path",
     *         description="perimeter in KM as float",
     *         required=true,
     *         @OA\Schema(
     *           type="number",
     *         ),
     *         style="form"
     *     ),
     *   @OA\Response(
     *     response=200,
     *     description="all available stations for current user (or all finished is no user)",
     *     @OA\JsonContent(
     *             type="array",
     *              @OA\Items(
     *                    @OA\Property( title="id",  property="id", type="integer"),
     *                    @OA\Property( title="id_pair",  property="id_pair", type="integer"),
     *                    @OA\Property( title="id_master",  property="id_master", type="integer"),
     *                    @OA\Property( title="type",  property="type", type="string"),
     *                    @OA\Property( title="lat",  property="lat", type="number"),
     *                    @OA\Property( title="lng",  property="lng", type="number"),
     *                    @OA\Property( title="antenna_volume",  property="antenna_volume", type="number"),
     *                    @OA\Property( title="channel_width",  property="channel_width", type="number"),
     *                    @OA\Property( title="power",  property="power", type="number"),
     *                    @OA\Property( title="frequency",  property="frequency", type="number"),
     *                    @OA\Property( title="specific_run_down",  property="specific_run_down", type="number"),
     *                    @OA\Property( title="type_station",property="type_station",  type="object",
     *                       @OA\Property( title="direction",property="direction",  type="number"),
     *                       @OA\Property( title="eirp", property="eirp", type="number"),
     *                       @OA\Property( title="is_ptmp", property="is_ptmp", type="boolean"),
     *                       @OA\Property( title="ratio_signal_interference", property="ratio_signal_interference",
     *     type="number"),
     *                    ),
     *                ),
     *   ),
     *   ),
     * )
     * Returns list of sizes.
     *
     */
    public function actionStationsFromPosition($lat, $lng, $perimeter)
    {
        $stations = StationSearch::findStationsInPerimeter($lat, $lng, $perimeter);
        $out = [];
        $stationIds = array_keys($stations);
        $wigigs = StationWigig::find()
                              ->where(['IN', 'id_station', $stationIds])
                              ->indexBy('id_station')
                              ->asArray()
                              ->all();
        $fss = StationFs::find()->where(['IN', 'id_station', $stationIds])->indexBy('id_station')->asArray()->all();

        foreach ($stations as $station) {

            if ($station['type'] == 'fs') {
                $type = $fss[$station['id']];
                $typeOut = [
                    'position' => $station['pair_position'],
                    'ratio_signal_interference' => $type['ratio_signal_interference'],
                ];
            } else {
                $type = $wigigs[$station['id']];
                $typeOut = [
                    'direction' => $type['direction'],
                    'eirp' => $type['eirp'],
                    'is_ptmp' => $type['is_ptmp'],
                ];
            }

            $out[$station['id']] = [
                'id' => $station['id'],
                'lng' => $station['lng'],
                'lat' => $station['lat'],
                'type' => $station['type'],
                'id_pair' => $station['id_station_pair'],
                'id_master' => $station['id_master'],
                'antenna_volume' => $station['antenna_volume'],
                'channel_width' => $station['channel_width'],
                'power' => $station['power'],

                'frequency' => $station['type'] == Station::COMPARE_TYPE_FS ? $type['frequency'] : '0',
                'specific_run_down' => $station['type'] == Station::COMPARE_TYPE_FS ? $type['specific_run_down'] : '0',
                'type_station' => $typeOut,
            ];
        }

        return $out;
    }

    public function actionUnpublishStation($id)
    {
        $station = Station::findOne(['id' => $id]);

        if ($this->canEdit($station)) {
            setSuccessFlash(_tF('flash_successfully_unpublished', 'station'));
            return $station->moveToUnpublished();
        } else {
            setErrorFlash(_tF('error_while_unpublishing', 'station'));
            throw new ForbiddenHttpException();
        }
    }
}
