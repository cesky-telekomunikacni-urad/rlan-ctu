<?php
/*
* Copyright 2023 DactylGroup s.r.o.
*
* Licensed under the EUPL, Version 1.2 or – as soon they
will be approved by the European Commission - subsequent
versions of the EUPL (the "Licence");
* You may not use this work except in compliance with the
Licence.
* You may obtain a copy of the Licence at:
*
* https://joinup.ec.europa.eu/software/page/eupl5
*
* Unless required by applicable law or agreed to in
writing, software distributed under the Licence is
distributed on an "AS IS" basis,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
express or implied.
* See the Licence for the specific language governing
permissions and limitations under the Licence.
*/
?>


<?php

namespace api\versions\v1;

use dactylcore\core\web\RestUrlRule;

/**
 * @OA\Info(
 *   version="1.0",
 *   title="CTU 60 GHz API",
 *   description="
REST API uses token-based authentication with Access Tokens and Refresh Tokens

Users can authenticate by providing a user email and password to the REST API login resource /user/login with the HTTP POST method.
Access token is then generated that enables the user to authenticate future requests on all non-public endpoints marked with lock button.

All endpoints with data input require data to be formatted as JSON body (Content-Type application/json)
",
 *   @OA\Contact(
 *     name="Český telekomunikační úřad",
 *     email="60ghz@ctu.cz",
 *   ),
 * ),
 * @OA\Server(
 *   url="/api/v1/",
 *   description="First version of API",
 * )
 * @OA\PathItem(path="/docs"),
 */

/**
 * @OA\SecurityScheme(
 *   securityScheme="access-token",
 *   type="apiKey",
 *   in="header",
 *   name="access-token"
 * )
 */

/**
 * @OA\Response(
 *      response="UnauthorizedAccessResponse",
 *      description="Invalid access token",
 *      @OA\JsonContent(
 *          @OA\Property(
 *              property="status",
 *              title="Response status code",
 *              type="integer",
 *              example=401
 *          ),
 *          @OA\Property(
 *              property="error",
 *              title="Error message",
 *              type="string",
 *              example="Access token is not valid"
 *          ),
 *      ),
 * ),
 */

/**
 * @OA\Response(
 *      response="CommonSuccessResponse",
 *      description="Common Success Response",
 *      @OA\JsonContent(
 *           @OA\Property(
 *               property="status",
 *               title="Response status code",
 *               type="integer",
 *               example=200
 *           ),
 *           @OA\Property(
 *               property="data",
 *               title="Additional data",
 *               type="object",
 *               example={}
 *           ),
 *      ),
 * ),
 */

/**
 * @OA\Response(
 *      response="AccessTokenMissingResponse",
 *      description="Access Token is missing in header",
 *      @OA\JsonContent(
 *           @OA\Property(
 *               property="status",
 *               title="Response status code",
 *               type="integer",
 *               example=400
 *           ),
 *           @OA\Property(
 *               property="error",
 *               title="Error message",
 *               type="string",
 *               example="Access token is missing."
 *           ),
 *      ),
 * ),
 */

/**
 * @OA\Response(
 *      response="UnauthorizedAccessResponse",
 *      description="Invalid access token",
 *      @OA\JsonContent(
 *          @OA\Property(
 *              property="status",
 *              title="Response status code",
 *              type="integer",
 *              example=401
 *          ),
 *          @OA\Property(
 *              property="error",
 *              title="Error message",
 *              type="string",
 *              example="Access token is not valid"
 *          ),
 *      ),
 * ),
 */

/**
 * @OA\Response(
 *      response="ResourceDoesNotExistResponse",
 *      description="Requested resource does not exist",
 *      @OA\JsonContent(
 *           @OA\Property(
 *               property="status",
 *               title="Response status code",
 *               type="integer",
 *               example=404
 *           ),
 *           @OA\Property(
 *               property="error",
 *               title="Optional message",
 *               type="string",
 *               example=""
 *           ),
 *      ),
 * ),
 */

/**
 * @OA\Response(
 *      response="DataValidationFailed",
 *      description="Data Validation Failed",
 *      @OA\JsonContent(
 *           @OA\Property(
 *               property="status",
 *               title="Response status code",
 *               type="integer",
 *               example="422"
 *           ),
 *           @OA\Property(
 *               property="error",
 *               title="Text error",
 *               type="string",
 *               example=""
 *           ),
 *           @OA\Property(
 *               property="attributeErrors",
 *               title="Specific attribute errors",
 *               type="array",
 *               @OA\Items(
 *                     @OA\Property(property="field", description="", type="string"),
 *                     @OA\Property(property="message", description="", type="string"),
 *               ),
 *           ),
 *      ),
 * ),
 */

/**
 * @OA\Parameter(
 *         name="filter",
 *         in="query",
 *         description="filter collumns query",
 *         required=false,
 *         @OA\Schema(
 *           type="object",
 *           @OA\Property(property="id", type="integet", example=3),
 *           @OA\Property(property="name][like", type="string", example="Nov"),
 *           @OA\Property(property="created_at][gt", type="integer", example="254654"),
 *         ),
 *         style="deepObject",
 *     ),
 * @OA\Parameter(
 *         name="fields",
 *         in="query",
 *         description="list of fields to return (fields=id,name)",
 *         required=false,
 *         @OA\Schema(
 *           type="string",
 *         ),
 *         style="form"
 *     ),
 * @OA\Parameter(
 *         name="expand",
 *         in="query",
 *         description="expandable relations",
 *         required=false,
 *         @OA\Schema(
 *           type="string",
 *         ),
 *         style="form"
 *     ),
 * @OA\Parameter(
 *         name="sort",
 *         in="query",
 *         description="sorting by columns (sort=-id,name)",
 *         required=false,
 *         @OA\Schema(
 *           type="string",
 *         ),
 *         style="form"
 *     ),
 * @OA\Parameter(
 *         name="page",
 *         in="query",
 *         description="get explicit page",
 *         required=false,
 *         @OA\Schema(
 *           type="integer",
 *         ),
 *         style="form"
 *     ),
 *
 */

/**
 * @OA\Schema(
 *     schema="commonDeletedResponse",
 *     description="",
 *     type="object",
 *     @OA\Property(
 *       title="ID of deleted item",
 *       property="deleted",
 *       type="integer",
 *       example=25411
 *     )
 * )
 */
class Module extends \dactylcore\core\api\Module
{
    public function init()
    {
        parent::init();

        \Yii::setAlias('@api/v1', __DIR__);

        $this->addUrlRules();
    }

    public function addUrlRules()
    {
        $this->addUrlManagerRules([
            'probe' => [
                'class' => RestUrlRule::class,
                'extraPatterns' => [
                    'POST scan-info' => 'scan-info',
                ],
            ],
            'user-alliance' => [
                'class' => RestUrlRule::class,
                'extraPatterns' => [
                    'GET' => 'view',
                    'POST' => 'create',
                    'PATCH' => 'update',
                    'DELETE' => 'delete',
                    'POST invite/{idUser}' => 'invite',
                    'PATCH promote/{idUser}' => 'promote',
                    'PATCH degrade/{idUser}' => 'degrade',
                    'PATCH exclude/{idUser}' => 'exclude',
                ],
                'tokens' => [
                    '{idUser}' => '<idUser:\\d[\\d,]*>',
                ],
            ],
            'station' => [
                'class' => RestUrlRule::class,
                'extraPatterns' => [
                    // old
                    'GET all-my-stations' => 'all-my-stations',
                    'GET all-stations' => 'all-stations',
                    'GET geo-stations' => 'geo-stations',
                    'GET geo-conflict-stations' => 'geo-conflict-stations',
                    'GET geo-exclusion-zone-stations/{id}' => 'geo-exclusion-zone-stations',
                    'GET conflict-lines' => 'conflict-lines',
                    'GET pair-id' => 'pair-id',
                    'GET lines' => 'lines',
                    'POST data' => 'data',
                    'POST prolong' => 'prolong',
                    'GET filters' => 'filters',
                    'GET station/{id}' => 'station',
                    'GET geo-station/{id}' => 'geo-station',
                    'GET station-popup/{id}' => 'station-popup',
                    'GET stations-from-position/<lat:\\d+\\.\\d+>/<lng:\\d+\\.\\d+>/<perimeter:\\d+\\.\\d+>' => 'stations-from-position',
                    'POST delete-station' => 'delete-station',
                    'POST unpublish-station' => 'unpublish-station',

                    // new
                    'GET' => 'index',
                    'GET {id}' => 'view',
                    'POST' => 'create',
                    'POST {id}' => 'update',
                    'DELETE {id}' => 'delete',
                    'PATCH {id}/unpublish' => 'unpublish',
                    'PATCH {id}/publish' => 'publish',
                    'PATCH {id}/relength' => 'relength',
                    'PATCH {id}/change-type-58-to-52' => 'change-type-58-to-52',
                    'POST search' => 'search',
                    'PATCH pair-wigigs/{id1}/{id2}' => 'pair-wigigs',
                    'PATCH unpair-wigigs/{id1}/{id2}' => 'unpair-wigigs',
                ],
                'tokens' => [
                    '{id}' => '<id:\\d[\\d,]*>',
                    '{id1}' => '<id1:\\d[\\d,]*>',
                    '{id2}' => '<id2:\\d[\\d,]*>',
                ],
            ],
            'thread' => [
                'class' => RestUrlRule::class,
                'extraPatterns' => [
                    'GET {idThread}/message' => 'message-index',
                    'POST {idThread}/message' => 'message-create',
                ],
                'tokens' => [
                    '{idThread}' => '<idThread:\\d[\\d,]*>',
                    '{id}' => '<id:\\d[\\d,]*>',
                ],
                'except' => ['delete'],
            ],
        ]);
    }
}
