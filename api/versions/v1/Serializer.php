<?php
/*
* Copyright 2023 DactylGroup s.r.o.
*
* Licensed under the EUPL, Version 1.2 or – as soon they
will be approved by the European Commission - subsequent
versions of the EUPL (the "Licence");
* You may not use this work except in compliance with the
Licence.
* You may obtain a copy of the Licence at:
*
* https://joinup.ec.europa.eu/software/page/eupl5
*
* Unless required by applicable law or agreed to in
writing, software distributed under the Licence is
distributed on an "AS IS" basis,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
express or implied.
* See the Licence for the specific language governing
permissions and limitations under the Licence.
*/
?>


<?php

namespace api\versions\v1;

use api\helpers\ApiHelper;

class Serializer extends \yii\rest\Serializer
{
    public $collectionEnvelope = 'items';

    protected static function getSerializeModelErrors($model)
    {
        $result = [];
        foreach ($model->getFirstErrors() as $name => $message) {
            $result[] = [
                'field' => $name,
                'message' => ApiHelper::removeTags($message),
            ];
        }

        return $result;
    }

    /**
     * @inheritDoc
     */
    protected function serializeModelErrors($model)
    {
        $result = self::getSerializeModelErrors($model);

        $mainMessage = _t('Data Validation Failed');
        if (count($result) == 1) {
            $mainMessage = $result[0]['message'];
        }

        $this->response->setStatusCode(422, $mainMessage);

        return $result;
    }
}
