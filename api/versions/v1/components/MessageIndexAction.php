<?php
/*
* Copyright 2023 DactylGroup s.r.o.
*
* Licensed under the EUPL, Version 1.2 or – as soon they
will be approved by the European Commission - subsequent
versions of the EUPL (the "Licence");
* You may not use this work except in compliance with the
Licence.
* You may obtain a copy of the Licence at:
*
* https://joinup.ec.europa.eu/software/page/eupl5
*
* Unless required by applicable law or agreed to in
writing, software distributed under the Licence is
distributed on an "AS IS" basis,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
express or implied.
* See the Licence for the specific language governing
permissions and limitations under the Licence.
*/
?>


<?php
/**
 * @link http://www.dactylgroup.com/
 */

namespace api\versions\v1\components;

use api\versions\v1\models\search\MessageSearch;
use api\versions\v1\models\Thread;
use Yii;
use yii\data\ActiveDataProvider;
use yii\rest\Action;

/**
 * IndexAction implements the API endpoint for listing multiple models.
 *
 * For more details and usage information on IndexAction, see the [guide article on rest controllers](guide:rest-controllers).
 *
 */
class MessageIndexAction extends Action
{
    /**
     * Returns messages of this Thread
     *
     * @param integer $idThread the primary key of the model.
     *
     * @return ActiveDataProvider
     */
    public function run(int $idThread): ActiveDataProvider
    {
        /* @var $thread Thread */
        $thread = $this->findModel($idThread);

        if ($this->checkAccess) {
            call_user_func($this->checkAccess, $this->id, $thread);
        }

        $provider = new MessageSearch();
        $requestParams = Yii::$app->getRequest()->getBodyParams();
        if (empty($requestParams)) {
            $requestParams = Yii::$app->getRequest()->getQueryParams();
        }

        // We have to cut filter for only THIS ONE thread
        if (isset($requestParams['filter'])) {
            $requestParams['filter'] = array_merge($requestParams['filter'], ['id_thread' => $idThread]);
        } else {
            $requestParams['filter'] = ['id_thread' => $idThread];
        }

        return $provider->search($requestParams);
    }
}
