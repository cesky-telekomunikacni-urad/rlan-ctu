<?php
/*
* Copyright 2023 DactylGroup s.r.o.
*
* Licensed under the EUPL, Version 1.2 or – as soon they
will be approved by the European Commission - subsequent
versions of the EUPL (the "Licence");
* You may not use this work except in compliance with the
Licence.
* You may obtain a copy of the Licence at:
*
* https://joinup.ec.europa.eu/software/page/eupl5
*
* Unless required by applicable law or agreed to in
writing, software distributed under the Licence is
distributed on an "AS IS" basis,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
express or implied.
* See the Licence for the specific language governing
permissions and limitations under the Licence.
*/
?>


<?php
/**
 * @link http://www.dactylgroup.com/
 */

namespace api\versions\v1\components;

use api\versions\v1\models\Message;
use api\versions\v1\models\search\MessageSearch;
use api\versions\v1\models\Thread;
use console\tasks\MessageNotificationJob;
use Yii;
use yii\data\ActiveDataFilter;
use yii\helpers\Url;
use yii\rest\Action;
use yii\web\ServerErrorHttpException;

/**
 * CreateAction implements the API endpoint for creating a new model from the given data.
 *
 * For more details and usage information on CreateAction, see the [guide article on rest controllers](guide:rest-controllers).
 *
 */
class MessageCreateAction extends Action
{
    /**
     * @var string the name of the view action. This property is needed to create the URL when the model is successfully created.
     */
    public $viewAction = 'message-index';


    /**
     * Creates a new model.
     *
     * @param $idThread
     * @return ActiveDataFilter|Message
     * @return \yii\db\ActiveRecordInterface the model newly created
     * @throws ServerErrorHttpException if there is any error when creating the model
     */
    public function run($idThread)
    {
        /* @var $thread Thread */
        $thread = $this->findModel($idThread);

        if ($this->checkAccess) {
            call_user_func($this->checkAccess, $this->id, $thread);
        }

        $model = new Message();
        $model->load(Yii::$app->getRequest()->getBodyParams(), '');
        $model->id_thread = $idThread;
        $model->id_author = loggedUser()->id;
        $model->status = Message::MESSAGE_STATUS_NEW;
        if ($model->save()) {
            MessageNotificationJob::pushEmailJob($model->id, "RE: {$thread->subject}", loggedUser()->id);
            $response = Yii::$app->getResponse();
            $response->setStatusCode(201);
            $response->getHeaders()->set('Location', Url::toRoute([$this->viewAction, 'idThread' => $idThread], true));
        } elseif (!$model->hasErrors()) {
            throw new ServerErrorHttpException('Failed to create the object for unknown reason.');
        } else {
            return $model;
        }

        // Return index of messages in the thread - with the newest message in it
        $provider = new MessageSearch();
        // We have to cut filter for only THIS ONE thread
        $requestParams['filter'] = ['id_thread' => $idThread];

        return $provider->search($requestParams);
    }
}
