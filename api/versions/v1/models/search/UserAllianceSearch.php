<?php
/*
* Copyright 2023 DactylGroup s.r.o.
*
* Licensed under the EUPL, Version 1.2 or – as soon they
will be approved by the European Commission - subsequent
versions of the EUPL (the "Licence");
* You may not use this work except in compliance with the
Licence.
* You may obtain a copy of the Licence at:
*
* https://joinup.ec.europa.eu/software/page/eupl5
*
* Unless required by applicable law or agreed to in
writing, software distributed under the Licence is
distributed on an "AS IS" basis,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
express or implied.
* See the Licence for the specific language governing
permissions and limitations under the Licence.
*/
?>


<?php

namespace api\versions\v1\models\search;

use api\versions\v1\models\UserAlliance;
use dactylcore\core\api\RestSearchModelInterface;
use dactylcore\core\api\helpers\QueryHelper;
use dactylcore\core\data\ActiveDataProvider;

class UserAllianceSearch extends UserAlliance implements RestSearchModelInterface
{
    public function rules()
    {
        return [
            [
                [
                    'id',
                    'deleted',
                ],
                'integer',
            ],
            [
                [
                    'name',
                    'description',
                ],
                'safe',
            ],
        ];
    }

    /**
     * @param array $params query params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $this->setAttributes($params);

        $query = UserAlliance::find();

        $query->joinWith('userAllianceConnectors');
        $query->andWhere(['user_alliance_connector.id_user' => loggedUser()->id]);

        QueryHelper::applyLastsyncToQuery($query, $this);

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'deleted' => $this->deleted,
        ]);

        $query->andFilterWhere(['like', 'name', $this->name])
              ->andFilterWhere(['like', 'description', $this->description]);

        return $dataProvider;
    }
}
