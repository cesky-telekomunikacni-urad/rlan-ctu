<?php
/*
* Copyright 2023 DactylGroup s.r.o.
*
* Licensed under the EUPL, Version 1.2 or – as soon they
will be approved by the European Commission - subsequent
versions of the EUPL (the "Licence");
* You may not use this work except in compliance with the
Licence.
* You may obtain a copy of the Licence at:
*
* https://joinup.ec.europa.eu/software/page/eupl5
*
* Unless required by applicable law or agreed to in
writing, software distributed under the Licence is
distributed on an "AS IS" basis,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
express or implied.
* See the Licence for the specific language governing
permissions and limitations under the Licence.
*/
?>


<?php

namespace api\versions\v1\models\search;

use api\versions\v1\models\Message;
use api\versions\v1\models\Thread;
use dactylcore\core\api\RestSearchModelInterface;
use dactylcore\core\data\ActiveDataProvider;
use yii\data\ActiveDataFilter;

class MessageSearch extends Message implements RestSearchModelInterface
{
    public function rules()
    {
        return [
            [['id', 'id_author', 'id_thread',], 'integer',],
            [['content'], 'string',],
        ];
    }

    /**
     * @param array $params query params
     *
     * @return ActiveDataProvider|ActiveDataFilter|null
     */
    public function search($params)
    {
        // Use ActiveFilter to search data, instead of custom created query
        $dataFilter = new \yii\data\ActiveDataFilter([
            // Trait returns class of where is it used
            'searchModel' => self::class,
        ]);

        if ($dataFilter->load($params)) {
            $filter = $dataFilter->build();
            if ($filter === false) {
                return $dataFilter;
            }
        }

        // Special function to get query od specific Model
        $query = Message::find();

        if (!empty($filter)) {
            $query->andWhere($filter);
        }

        return new ActiveDataProvider([
            'query' => $query,
        ]);
    }
}
