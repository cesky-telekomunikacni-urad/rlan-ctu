<?php
/*
* Copyright 2023 DactylGroup s.r.o.
*
* Licensed under the EUPL, Version 1.2 or – as soon they
will be approved by the European Commission - subsequent
versions of the EUPL (the "Licence");
* You may not use this work except in compliance with the
Licence.
* You may obtain a copy of the Licence at:
*
* https://joinup.ec.europa.eu/software/page/eupl5
*
* Unless required by applicable law or agreed to in
writing, software distributed under the Licence is
distributed on an "AS IS" basis,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
express or implied.
* See the Licence for the specific language governing
permissions and limitations under the Licence.
*/
?>


<?php

namespace api\versions\v1\models\search;

use api\versions\v1\models\Station;
use dactylcore\core\api\RestSearchModelInterface;
use dactylcore\core\api\helpers\QueryHelper;
use dactylcore\core\data\ActiveDataProvider;
use yii\base\BaseObject;
use yii\db\Expression;

class StationSearch extends Station implements RestSearchModelInterface
{
    public function rules()
    {
        return [
            [
                [
                    'id',
                    'id_user',
                    'id_station_pair',
                    'id_master',
                    'registered_at',
                    'valid_to',
                    'protected_to',
                    'deleted',
                    'created_at',
                    'updated_at',
                    'hardware_identifier',
                ],
                'integer',
            ],
            [
                [
                    'pair_position',
                    'type',
                    'status',
                    'name',
                    'published_by',
                    'mac_address',
                    'serial_number',
                ],
                'safe',
            ],
            [
                [
                    'lng',
                    'lat',
                    'antenna_volume',
                    'channel_width',
                    'power',
                ],
                'number',
            ],
        ];
    }

    /**
     * @param array $params query params
     *
     * @return ActiveDataProvider
     */
    public function search($params = [], $post = [])
    {
        $user = loggedUser();

        $q = Station::find();

        $this->setAttributes($params);

        $dataProvider = new ActiveDataProvider([
            'query' => $q,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            return $dataProvider;
        }

        if ($post['search'] ?? null) {
            $searchTerm = $post['search'];
            $q->andFilterWhere(['or', ['LIKE', 'name', $searchTerm], ['LIKE', 'id', $searchTerm]]);
        }

        if ($post['types'] ?? null) {
            $q->andFilterWhere(['IN', 'type', $post['types']]);
        }

        if ($post['statuses'] ?? null) {
            $publicStatuses = array_intersect($post['statuses'], [Station::STATUS_FINISHED]);
            // want by all users
            $q->andFilterWhere([
                'OR',

                // all statuses but it has to be mine
                ['AND', ['IN', 'status', $post['statuses']], ['id_user' => $user->id]],

                // other users but public statuses
                ['IN', 'status', $publicStatuses],
            ]);
        } else {
            $q->andFilterWhere(['OR', ['status' => Station::STATUS_FINISHED], ['id_user' => $user->id]]);
        }

        // Sorting
        if (!(isset($post['sortByExpiration']) && $post['sortByExpiration'])) {
            if ($post['sort']['field'] ?? null) {
                $direction = $post['sort']['type'] == 'asc' ? SORT_ASC : SORT_DESC;
                // in case that sort by id is requested from user profile, rename id attribute id_orig => id
                if ($post['sort']['field'] == 'id_orig') {
                    $q->orderBy(['id' => $direction]);
                } else {
                    $q->orderBy([$post['sort']['field'] => $direction]);
                }
            } else {
                $q->orderBy(['id' => SORT_DESC]);
            }
        } else {
            $q->orderBy(['valid_to' => SORT_ASC]);
        }

        // this prevent FS duplication in response
        $q->andWhere([
            'OR',
            ['pair_position' => 'a'],
            ['pair_position' => null],
        ]);


        return $dataProvider;
    }

    /**
     * @param array $params query params
     *
     * @return ActiveDataProvider
     */
    public function searchMy($params = [])
    {
        $query = Station::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        if (!$this->validate()) {
            return $dataProvider;
        }

        // Limit access to only my or alliance stations
        $user = loggedUser();
        $allianceUsersIds = [];
        if ($user->isInAlliance()) {
            $allianceUsersIds = map($user->alliance->getUserList()->select('user.id')->all(), 'id', 'id');
        }
        $query->andWhere([
            'OR',
            ['id_user' => $user->id],
            ['id_user' => $allianceUsersIds],
        ]);

        // this prevent FS duplication in response
        $query->andWhere([
            'OR',
            ['pair_position' => 'a'],
            ['pair_position' => null],
        ]);

        return $dataProvider;
    }
}
