<?php
/*
* Copyright 2023 DactylGroup s.r.o.
*
* Licensed under the EUPL, Version 1.2 or – as soon they
will be approved by the European Commission - subsequent
versions of the EUPL (the "Licence");
* You may not use this work except in compliance with the
Licence.
* You may obtain a copy of the Licence at:
*
* https://joinup.ec.europa.eu/software/page/eupl5
*
* Unless required by applicable law or agreed to in
writing, software distributed under the Licence is
distributed on an "AS IS" basis,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
express or implied.
* See the Licence for the specific language governing
permissions and limitations under the Licence.
*/
?>


<?php

namespace api\versions\v1\models;

/**
 * @OA\Schema(schema="stationForm_wigig_item",
 *     required={"direction", "channel_width"},
 *     allOf = {
 *        @OA\Schema(ref = "#/components/schemas/stationForm_base"),
 *     },
 *     @OA\Property(property="direction", description="", type="integer"),
 *     @OA\Property(property="eirp_method", description="", type="string", enum={"auto","manual"}),
 *     @OA\Property(property="serial_number", description="", type="string"),
 *     @OA\Property(property="antenna_volume", description="", type="number"),
 *     @OA\Property(property="channel_width", description="", type="number"),
 * )
 */
/**
 * @OA\Schema(schema="stationForm_wigig_item_forUpdate",
 *     required={},
 *     allOf = {
 *        @OA\Schema(ref = "#/components/schemas/stationForm_base_forUpdate"),
 *     },
 *     @OA\Property(property="direction", description="", type="integer"),
 *     @OA\Property(property="eirp_method", description="", type="string", enum={"auto","manual"}),
 *     @OA\Property(property="serial_number", description="", type="string"),
 *     @OA\Property(property="antenna_volume", description="", type="number"),
 *     @OA\Property(property="channel_width", description="", type="number"),
 * )
 */
/**
 * @OA\Schema(schema="stationForm_wigig",
 *     required={"type", "station"},
 *     @OA\Property(property="type", description="", type="string", example="wigig"),
 *     @OA\Property(property="station", description="", ref="#/components/schemas/stationForm_wigig_item"),
 * )
 */
/**
 * @OA\Schema(schema="stationForm_wigig_forUpdate",
 *     required={},
 *     @OA\Property(property="station", description="", ref="#/components/schemas/stationForm_wigig_item_forUpdate"),
 * )
 */

/**
 * @OA\Schema(schema="stationDetail_wigig_item",
 *     allOf = {
 *        @OA\Schema(ref = "#/components/schemas/stationDetail_base"),
 *     },
 *     @OA\Property(property="direction", description="", type="integer"),
 *     @OA\Property(property="eirp_method", description="", type="string", enum={"auto","manual"}),
 *     @OA\Property(property="serial_number", description="", type="string"),
 *     @OA\Property(property="antenna_volume", description="", type="number"),
 *     @OA\Property(property="channel_width", description="", type="number"),
 * )
 */
/**
 * @OA\Schema(schema="stationDetail_wigig",
 *     @OA\Property(property="station", description="", ref="#/components/schemas/stationDetail_wigig_item"),
 * )
 */

class StationWigig extends \common\models\StationWigig
{
    public function fields()
    {
        $fields = [
            'direction',
            'eirp_method',
            'eirp'
        ];

        return $fields;
    }
}
