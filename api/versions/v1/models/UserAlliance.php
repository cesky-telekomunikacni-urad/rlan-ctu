<?php
/*
* Copyright 2023 DactylGroup s.r.o.
*
* Licensed under the EUPL, Version 1.2 or – as soon they
will be approved by the European Commission - subsequent
versions of the EUPL (the "Licence");
* You may not use this work except in compliance with the
Licence.
* You may obtain a copy of the Licence at:
*
* https://joinup.ec.europa.eu/software/page/eupl5
*
* Unless required by applicable law or agreed to in
writing, software distributed under the Licence is
distributed on an "AS IS" basis,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
express or implied.
* See the Licence for the specific language governing
permissions and limitations under the Licence.
*/
?>


<?php

namespace api\versions\v1\models;

/**
 * @OA\Schema(schema="userAllianceMember",
 *     @OA\Property(property="id", description="", type="string"),
 *     @OA\Property(property="companyName", description="", type="string"),
 *     @OA\Property(property="first_name", description="", type="string"),
 *     @OA\Property(property="last_name", description="", type="string"),
 *     @OA\Property(property="type", description="", type="string"),
 *     @OA\Property(property="email", description="", type="string"),
 *     @OA\Property(property="membership", description="", type="string"),
 * )
 */

/**
 * @OA\Schema(schema="userAlliance",
 *     @OA\Property(property="id", description="", type="integer"),
 *     @OA\Property(property="name", description="", type="string"),
 *     @OA\Property(property="description", description="", type="string"),
 *     @OA\Property(property="deleted", description="", type="boolean"),
 *     @OA\Property(property="created_at", description="", type="integer"),
 *     @OA\Property(property="updated_at", description="", type="integer"),
 *     @OA\Property(property="members", description="", type="array", @OA\Items(ref="#/components/schemas/userAllianceMember")),
 * )
 */

/**
 * @OA\Schema(schema="userAllianceForm",
 *     @OA\Property(property="name", description="", type="string", example="My New Alliance 1"),
 *     @OA\Property(property="description", description="", type="string", example="Best user alliance ever..."),
 * )
 */

/**
 * This is the api model class for table "user_alliance".
 */
class UserAlliance extends \common\models\UserAlliance
{
    public function fields()
    {
        $fields = [
            'id',
            'name',
            'description',
            'deleted' => fn() => (bool)$this->deleted,
            'created_at',
            'updated_at',
            'members' => fn() => $this->getUserList()->all(),
        ];

        return $fields;
    }
}