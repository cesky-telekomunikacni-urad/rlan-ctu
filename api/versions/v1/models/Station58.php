<?php
/*
* Copyright 2023 DactylGroup s.r.o.
*
* Licensed under the EUPL, Version 1.2 or – as soon they
will be approved by the European Commission - subsequent
versions of the EUPL (the "Licence");
* You may not use this work except in compliance with the
Licence.
* You may obtain a copy of the Licence at:
*
* https://joinup.ec.europa.eu/software/page/eupl5
*
* Unless required by applicable law or agreed to in
writing, software distributed under the Licence is
distributed on an "AS IS" basis,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
express or implied.
* See the Licence for the specific language governing
permissions and limitations under the Licence.
*/
?>


<?php

namespace api\versions\v1\models;

/**
 * @OA\Schema(schema="stationForm_wifi_5_8_item",
 *     required={"mac_address"},
 *     allOf = {
 *        @OA\Schema(ref = "#/components/schemas/stationForm_base"),
 *     },
 *     @OA\Property(property="is_ap", description="", type="boolean"),
 * )
 */
/**
 * @OA\Schema(schema="stationForm_wifi_5_8_item_forUpdate",
 *     required={},
 *     allOf = {
 *        @OA\Schema(ref = "#/components/schemas/stationForm_base_forUpdate"),
 *     },
 *     @OA\Property(property="is_ap", description="", type="boolean"),
 * )
 */
/**
 * @OA\Schema(schema="stationForm_wifi_5_8",
 *     required={"type", "station"},
 *     @OA\Property(property="type", description="", type="string", example="wifi_5_8"),
 *     @OA\Property(property="station", description="", ref="#/components/schemas/stationForm_wifi_5_8_item"),
 * )
 */
/**
 * @OA\Schema(schema="stationForm_wifi_5_8_forUpdate",
 *     required={},
 *     @OA\Property(property="station", description="", ref="#/components/schemas/stationForm_wifi_5_8_item_forUpdate"),
 * )
 */

/**
 * @OA\Schema(schema="stationDetail_wifi_5_8_item",
 *     allOf = {
 *        @OA\Schema(ref = "#/components/schemas/stationDetail_base"),
 *     },
 *     @OA\Property(property="is_ap", description="", type="boolean"),
 * )
 */
/**
 * @OA\Schema(schema="stationDetail_wifi_5_8",
 *     @OA\Property(property="station", description="", ref="#/components/schemas/stationDetail_wifi_5_8_item"),
 * )
 */

class Station58 extends \common\models\Station58
{
    public function fields()
    {
        $fields = [
            'is_ap',
        ];

        return $fields;
    }
}
