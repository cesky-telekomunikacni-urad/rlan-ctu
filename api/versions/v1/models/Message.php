<?php
/*
* Copyright 2023 DactylGroup s.r.o.
*
* Licensed under the EUPL, Version 1.2 or – as soon they
will be approved by the European Commission - subsequent
versions of the EUPL (the "Licence");
* You may not use this work except in compliance with the
Licence.
* You may obtain a copy of the Licence at:
*
* https://joinup.ec.europa.eu/software/page/eupl5
*
* Unless required by applicable law or agreed to in
writing, software distributed under the Licence is
distributed on an "AS IS" basis,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
express or implied.
* See the Licence for the specific language governing
permissions and limitations under the Licence.
*/
?>


<?php

namespace api\versions\v1\models;

/**
 * @OA\Schema(schema="messageSchema",
 *     @OA\Property(property="id", description="", type="integer"),
 *     @OA\Property(property="id_author", description="", type="integer"),
 *     @OA\Property(property="author", description="", type="string"),
 *     @OA\Property(property="content", description="", type="string"),
 *     @OA\Property(property="created_at", description="", type="integer"),
 * )
 */

/**
 * @OA\Schema(
 *     schema="messageCreateSchema",
 *     description="data for creation of the Message",
 *     type="object",
 *     required={"content"},
 *     @OA\Property(property="content", type="string"),
 * )
 */

/**
 * This is the api model class for table "message".
 */
class Message extends \common\models\Message
{
    public function getAuthorIdentification() {
        $showName = false;
        if ($this->id_author == $this->thread->id_sender) {
            $showName = $this->thread->show_sender_info;
        } elseif ($this->id_author == $this->thread->id_recipient) {
            $showName = $this->thread->show_recipient_info;
        }
        return ($showName) ? $this->author->email : Thread::getPaddedId($this->id_author);
    }

    public function fields()
    {
        $fields = [
            'id',
            'id_author',
            'author' => fn() => $this->getAuthorIdentification(),
            'content',
            'created_at',
        ];

        return $fields;
    }
}