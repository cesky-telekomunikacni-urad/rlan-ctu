<?php
/*
* Copyright 2023 DactylGroup s.r.o.
*
* Licensed under the EUPL, Version 1.2 or – as soon they
will be approved by the European Commission - subsequent
versions of the EUPL (the "Licence");
* You may not use this work except in compliance with the
Licence.
* You may obtain a copy of the Licence at:
*
* https://joinup.ec.europa.eu/software/page/eupl5
*
* Unless required by applicable law or agreed to in
writing, software distributed under the Licence is
distributed on an "AS IS" basis,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
express or implied.
* See the Licence for the specific language governing
permissions and limitations under the Licence.
*/
?>


<?php

namespace api\versions\v1\models;

/**
 * @OA\Schema(schema="threadSchema",
 *     @OA\Property(property="id", description="", type="integer"),
 *     @OA\Property(property="subject", description="", type="string"),
 *     @OA\Property(property="sender", description="", type="string"),
 *     @OA\Property(property="recipient", description="", type="string"),
 *     @OA\Property(property="id_station", description="", type="integer"),
 *     @OA\Property(property="id_station_disturber", description="", type="integer"),
 *     @OA\Property(property="created_at", description="", type="integer"),
 *     @OA\Property(property="updated_at", description="", type="integer"),
 * )
 */

/**
 * @OA\Schema(
 *     schema="threadCreateSchema",
 *     description="data for creation of the Thread",
 *     type="object",
 *     required={"id_station"},
 *     @OA\Property(property="id_station", type="integer"),
 *     @OA\Property(property="id_station_disturber", type="integer"),
 *     @OA\Property(property="showMyInfo", type="integer", enum={0, 1}),
 * )
 */


/**
 * @OA\Schema(
 *     schema="threadUpdateSchema",
 *     description="data for creation of the Thread",
 *     type="object",
 *     @OA\Property(property="showMyInfo", type="integer", enum={0, 1}),
 * )
 */

/**
 * This is the api model class for table "thread".
 */
class Thread extends \common\models\Thread
{
    public const SCENARIO_API_UPDATE = 'api-update';
    public const SCENARIO_API_CREATE = 'api-create';

    public bool $showMyInfo = false;

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return merge(parent::rules(), [
            [['status_sender'], 'filter', 'filter' => fn($value) => self::STATUS_READ],
            [['status_recipient'], 'filter', 'filter' => fn($value) => self::STATUS_NEW],
            [['reason'], 'filter', 'filter' => fn($value) => self::REASON_GENERIC],
            // sender is always me
            [['id_sender'], 'filter', 'filter' => fn($value) => loggedUser()->id],
            [
                ['id_station'],
                'unique',
                'targetAttribute' => ['id_station', 'id_sender'],
                'when' => fn() => empty($this->id_station_disturber)
            ],
            [
                ['id_station'],
                'unique',
                'targetAttribute' => ['id_station', 'id_sender', 'id_station_disturber'],
                'when' => fn() => !empty($this->id_station_disturber)
            ],
            [
                ['id_station'],
                function ($attribute, $params, $validator) {
                    if (loggedUser()->id != Station::findOne($this->id_station)->id_user) {
                        $this->addError($attribute, _tF('not_my_station_error', 'station'));
                    }
                },
                'when' => fn() => !empty($this->id_station_disturber)
            ],

            // Recipient is owner of disturbing station (your station is disturbing me),
            // OR it is owner of station I am writing about (I found your station and wanted to write you about it).
            //    it depends on if Disturber station is filled or not.
            [
                ['id_recipient'],
                'filter',
                'filter' => fn($value) => Station::findOne($this->id_station)->id_user,
                'when' => fn() => empty($this->id_station_disturber)
            ],
            [
                ['id_recipient'],
                'filter',
                'filter' => fn($value) => Station::findOne($this->id_station_disturber)->id_user,
                'when' => fn() => !empty($this->id_station_disturber)
            ],
            // Recipient and Sender cannot be the same user
            ['id_recipient', 'compare', 'compareAttribute' => 'id_sender', 'operator' => '!='],

            // We can show our info
            [['showMyInfo'], 'boolean'],
        ]);
    }


    /**
     * {@inheritDoc}
     */
    public function scenarios()
    {
        // We cannot change show_info attributes
        return array_merge(parent::scenarios(), [
            self::SCENARIO_API_CREATE =>
                [
                    'created_at',
                    'updated_at',
                    'deleted',
                    'id_recipient',
                    'id_sender',
                    'id_station',
                    'id_station_disturber',
                    'reason',
                    'status_sender',
                    'status_recipient',
                    'showMyInfo',
                ],
            self::SCENARIO_API_UPDATE => [
                'showMyInfo',
            ],
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function afterSave($insert, $changedAttributes)
    {
        parent::afterSave($insert, $changedAttributes);

        // We have to set right subject
        if (empty($this->subject)) {
            $this->subject = empty($this->id_station_disturber) ?
                _tF('message on station {stationName}', 'messaging',
                    ['stationName' => $this->station->getTypeName()])
                :
                _tF('message on disturbing {stationName} by {stationAggressorName}', 'messaging',
                    [
                        'stationName' => $this->station->getTypeName(),
                        'stationAggressorName' => $this->stationDisturber->getTypeName(),
                    ]
                );

            $this->save(true, ['subject']);
        }

        if (!is_null($this->showMyInfo)) {
            if ($this->id_sender == loggedUser()->id) {
                $this->show_sender_info = $this->showMyInfo;
            } elseif ($this->id_recipient == loggedUser()->id) {
                $this->show_recipient_info = $this->showMyInfo;
            }

            $this->save(true, ['show_sender_info', 'show_recipient_info']);
        }
    }

    public function fields()
    {
        $fields = [
            'id',
            'subject',
            'sender' => fn() => ($this->show_sender_info) ? $this->sender->email : Thread::getPaddedId($this->id_sender),
            'recipient' => fn() => ($this->show_recipient_info) ? $this->recipient->email : Thread::getPaddedId($this->id_recipient),
            'created_at',
            'updated_at',
            'id_station' => fn() => (int) $this->id_station,
            'id_station_disturber',
        ];

        return $fields;
    }
}