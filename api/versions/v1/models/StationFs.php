<?php
/*
* Copyright 2023 DactylGroup s.r.o.
*
* Licensed under the EUPL, Version 1.2 or – as soon they
will be approved by the European Commission - subsequent
versions of the EUPL (the "Licence");
* You may not use this work except in compliance with the
Licence.
* You may obtain a copy of the Licence at:
*
* https://joinup.ec.europa.eu/software/page/eupl5
*
* Unless required by applicable law or agreed to in
writing, software distributed under the Licence is
distributed on an "AS IS" basis,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
express or implied.
* See the Licence for the specific language governing
permissions and limitations under the Licence.
*/
?>


<?php

namespace api\versions\v1\models;

/**
 * @OA\Schema(schema="stationForm_fs_item",
 *     required={"antenna_volume", "channel_width", "frequency", "ratio_signal_interference"},
 *     allOf = {
 *        @OA\Schema(ref = "#/components/schemas/stationForm_base"),
 *     },
 *     @OA\Property(property="ratio_signal_interference", description="", type="integer"),
 *     @OA\Property(property="frequency", description="", type="integer"),
 *     @OA\Property(property="serial_number", description="", type="string"),
 *     @OA\Property(property="antenna_volume", description="", type="number"),
 *     @OA\Property(property="channel_width", description="", type="number"),
 * )
 */
/**
 * @OA\Schema(schema="stationForm_fs_item_forUpdate",
 *     required={},
 *     allOf = {
 *        @OA\Schema(ref = "#/components/schemas/stationForm_base_forUpdate"),
 *     },
 *     @OA\Property(property="ratio_signal_interference", description="", type="integer"),
 *     @OA\Property(property="frequency", description="", type="integer"),
 *     @OA\Property(property="serial_number", description="", type="string"),
 *     @OA\Property(property="antenna_volume", description="", type="number"),
 *     @OA\Property(property="channel_width", description="", type="number"),
 * )
 */
/**
 * @OA\Schema(schema="stationForm_fs",
 *     required={"type", "stationA", "stationB"},
 *     @OA\Property(property="type", description="", type="string", example="fs"),
 *     @OA\Property(property="stationA", description="", ref="#/components/schemas/stationForm_fs_item"),
 *     @OA\Property(property="stationB", description="", ref="#/components/schemas/stationForm_fs_item"),
 * )
 */
/**
 * @OA\Schema(schema="stationForm_fs_forUpdate",
 *     required={},
 *     @OA\Property(property="stationA", description="", ref="#/components/schemas/stationForm_fs_item_forUpdate"),
 *     @OA\Property(property="stationB", description="", ref="#/components/schemas/stationForm_fs_item_forUpdate"),
 * )
 */

/**
 * @OA\Schema(schema="stationDetail_fs_item",
 *     allOf = {
 *        @OA\Schema(ref = "#/components/schemas/stationDetail_base"),
 *     },
 *     @OA\Property(property="ratio_signal_interference", description="", type="integer"),
 *     @OA\Property(property="frequency", description="", type="integer"),
 *     @OA\Property(property="serial_number", description="", type="string"),
 *     @OA\Property(property="antenna_volume", description="", type="number"),
 *     @OA\Property(property="channel_width", description="", type="number"),
 * )
 */

/**
 * @OA\Schema(schema="stationDetail_fs",
 *     @OA\Property(property="stationA", description="", ref="#/components/schemas/stationDetail_fs_item"),
 *     @OA\Property(property="stationB", description="", ref="#/components/schemas/stationDetail_fs_item"),
 * )
 */
class StationFs extends \common\models\StationFs
{
    public function fields()
    {
        $fields = [
            'ratio_signal_interference',
            'frequency',
        ];

        foreach (array_merge(Station::defaultFields(), ['power']) as $attr) {
            $attr2 = $attr;
            if ($attr == 'waiting_api_changes_json') {
                $attr2 = 'waitingApiChangesAsArray';
            }
            $fields[$attr] = fn() => $this->station->{$attr2};
        }

        return $fields;
    }
}
