<?php
/*
* Copyright 2023 DactylGroup s.r.o.
*
* Licensed under the EUPL, Version 1.2 or – as soon they
will be approved by the European Commission - subsequent
versions of the EUPL (the "Licence");
* You may not use this work except in compliance with the
Licence.
* You may obtain a copy of the Licence at:
*
* https://joinup.ec.europa.eu/software/page/eupl5
*
* Unless required by applicable law or agreed to in
writing, software distributed under the Licence is
distributed on an "AS IS" basis,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
express or implied.
* See the Licence for the specific language governing
permissions and limitations under the Licence.
*/
?>


<?php

namespace api\versions\v1\models;


use api\helpers\ApiHelper;
use frontend\controllers\StationController;

/**
 * @OA\Schema(schema="stationGeoJSON",
 *     description="GeoJSON for model Station",
 *     type="object",
 *     @OA\Property(
 *        title="type",
 *        property="type",
 *        type="string"
 *     ),
 *     @OA\Property(
 *        title="features",
 *        property="features",
 *        type="array",
 *        @OA\Items(
 *           @OA\Property(
 *              title="type",
 *              property="type",
 *              type="string"
 *           ),
 *           @OA\Property(
 *              title="geometry",
 *              property="geometry",
 *              type="object",
 *              @OA\Property(
 *                 title="type",
 *                 property="type",
 *                 type="string"
 *              ),
 *              @OA\Property(
 *                 title="coordinates in form [lng, lat]",
 *                 property="coordinates",
 *                 type="array",
 *                 @OA\Items()
 *              ),
 *           ),
 *           @OA\Property(
 *              title="properties",
 *              property="properties",
 *              type="object",
 *              @OA\Property(
 *                 title="t",
 *                 property="coordinates",
 *                 type="string"
 *              ),
 *              @OA\Property(
 *                 title="Wigig LAT point",
 *                 property="w_lat",
 *                 type="number"
 *              ),
 *              @OA\Property(
 *                 title="Wigig LNG point",
 *                 property="w_lng",
 *                 type="number"
 *              ),
 *           ),
 *           @OA\Property(
 *              title="id",
 *              property="id",
 *              type="string",
 *           ),
 *       )
 *   )
 *)
 */
/**
 * @OA\Schema(schema="stationItemLong",
 *     description="data for model Station",
 *     type="object",
 *     @OA\Property(
 *       title="id",
 *       property="id",
 *       type="integer"
 *     ),
 *     @OA\Property(
 *       title="id_station_pair",
 *       property="id_station_pair",
 *       type="integer"
 *     ),
 *     @OA\Property(
 *       title="position in FS pair A or B",
 *       property="pair_position",
 *       type="string"
 *     ),
 *     @OA\Property(
 *       title="type - wigig or fs",
 *       property="type",
 *       type="string"
 *     ),
 *     @OA\Property(
 *       title="type",
 *       property="typeName",
 *       type="string"
 *     ),
 *     @OA\Property(
 *       title="formattedId",
 *       property="formattedId",
 *       type="string"
 *     ),
 *     @OA\Property(
 *       title="name",
 *       property="name",
 *       type="string"
 *     ),
 *     @OA\Property(
 *       title="lat",
 *       property="lat",
 *       type="number"
 *     ),
 *     @OA\Property(
 *       title="lng",
 *       property="lng",
 *       type="number"
 *     ),
 *     @OA\Property(
 *       title="id_user",
 *       property="id_user",
 *       type="integer"
 *     ),
 *     @OA\Property(
 *       title="angle",
 *       property="angle",
 *       type="number"
 *     ),
 * )
 */
/**
 * @OA\Schema(schema="myStationItemShort",
 *     description="data for model Station",
 *     type="object",
 *     @OA\Property(
 *       title="id",
 *       property="id",
 *       type="integer"
 *     ),
 *     @OA\Property(
 *       title="id_station_pair",
 *       property="ip",
 *       type="integer"
 *     ),
 *     @OA\Property(
 *       title="id master",
 *       property="m",
 *       type="integer"
 *     ),
 *     @OA\Property(
 *       title="id master",
 *       property="id_m",
 *       type="integer"
 *     ),
 *     @OA\Property(
 *       title="position in FS pair A or B",
 *       property="pp",
 *       type="string"
 *     ),
 *     @OA\Property(
 *       title="type - wigig or fs",
 *       property="t",
 *       type="string"
 *     ),
 *     @OA\Property(
 *       title="type",
 *       property="tn",
 *       type="string",
 *       example="tg_58ghz"
 *     ),
 *     @OA\Property(
 *       title="name",
 *       property="t",
 *       type="string",
 *       example="Nazev stanice"
 *     ),
 *     @OA\Property(
 *       title="lat",
 *       property="lt",
 *       type="number",
 *       example="49.74743215351123"
 *     ),
 *     @OA\Property(
 *       title="lng",
 *       property="lg",
 *       type="number",
 *       example="13.90275175449975"
 *     ),
 *     @OA\Property(
 *       title="id_user",
 *       property="u",
 *       type="integer"
 *     ),
 *     @OA\Property(
 *       title="angle of wigig",
 *       property="a",
 *       type="number"
 *     ),
 *     @OA\Property(
 *       title="nac address",
 *       property="mac",
 *       type="string",
 *       example="00:AA:BB:CC:DD:FF"
 *     ),
 *     @OA\Property(
 *       title="status of the station",
 *       property="s",
 *       type="string",
 *       example="Čeká"
 *     ),
 * )
 */
/**
 * @OA\Schema(schema="stationItemShort",
 *     description="data for model Station",
 *     type="object",
 *     @OA\Property(
 *       title="id",
 *       property="id",
 *       type="integer"
 *     ),
 *     @OA\Property(
 *       title="id_station_pair",
 *       property="ip",
 *       type="integer"
 *     ),
 *     @OA\Property(
 *       title="id master",
 *       property="m",
 *       type="integer"
 *     ),
 *     @OA\Property(
 *       title="id master",
 *       property="id_m",
 *       type="integer"
 *     ),
 *     @OA\Property(
 *       title="position in FS pair A or B",
 *       property="pp",
 *       type="string"
 *     ),
 *     @OA\Property(
 *       title="type - wigig or fs",
 *       property="t",
 *       type="string"
 *     ),
 *     @OA\Property(
 *       title="type",
 *       property="tn",
 *       type="string"
 *     ),
 *     @OA\Property(
 *       title="name",
 *       property="t",
 *       type="string"
 *     ),
 *     @OA\Property(
 *       title="lat",
 *       property="lt",
 *       type="number"
 *     ),
 *     @OA\Property(
 *       title="lng",
 *       property="lg",
 *       type="number"
 *     ),
 *     @OA\Property(
 *       title="id_user",
 *       property="u",
 *       type="integer"
 *     ),
 *     @OA\Property(
 *       title="angle of wigig",
 *       property="a",
 *       type="number"
 *     ),
 * )
 */
/**
 * @OA\Schema(schema="stationExclusionZone",
 *     description="data for model Station",
 *     type="object",
 *     @OA\Property(
 *        title="type",
 *        property="type",
 *        type="string"
 *     ),
 *     @OA\Property(
 *        title="features",
 *        property="features",
 *        type="array",
 *        @OA\Items(
 *           @OA\Property(
 *              title="type",
 *              property="type",
 *              type="string"
 *           ),
 *           @OA\Property(
 *              title="geometry",
 *              property="geometry",
 *              type="object",
 *              @OA\Property(
 *                 title="type",
 *                 property="type",
 *                 type="string"
 *              ),
 *              @OA\Property(
 *                 title="coordinates in form [lng, lat]",
 *                 property="coordinates",
 *                 type="array",
 *                 @OA\Items()
 *              ),
 *           ),
 *           @OA\Property(
 *              title="properties",
 *              property="properties",
 *              type="object",
 *              @OA\Property(
 *                 title="type",
 *                 property="t",
 *                 type="string"
 *              ),
 *              @OA\Property(
 *                 title="conflict",
 *                 property="c",
 *                 type="integer"
 *              ),
 *           ),
 *           @OA\Property(
 *              title="id",
 *              property="id",
 *              type="integer",
 *           ),
 *       )
 *   )
 *)
 */

/**
 * @OA\Schema(schema="station",
 *     @OA\Property(property="id", description="", type="integer"),
 *     @OA\Property(property="id_user", description="", type="integer"),
 *     @OA\Property(property="id_station_pair", description="", type="integer"),
 *     @OA\Property(property="id_master", description="", type="integer"),
 *     @OA\Property(property="pair_position", description="", type="string"),
 *     @OA\Property(property="type", description="", type="string"),
 *     @OA\Property(property="lng", description="", type="number"),
 *     @OA\Property(property="lat", description="", type="number"),
 *     @OA\Property(property="antenna_volume", description="", type="number"),
 *     @OA\Property(property="channel_width", description="", type="number"),
 *     @OA\Property(property="power", description="", type="number"),
 *     @OA\Property(property="registered_at", description="", type="integer"),
 *     @OA\Property(property="valid_to", description="", type="integer"),
 *     @OA\Property(property="protected_to", description="", type="integer"),
 *     @OA\Property(property="status", description="", type="string"),
 *     @OA\Property(property="deleted", description="", type="integer"),
 *     @OA\Property(property="created_at", description="", type="integer"),
 *     @OA\Property(property="updated_at", description="", type="integer"),
 *     @OA\Property(property="name", description="", type="string"),
 *     @OA\Property(property="published_by", description="", type="string"),
 *     @OA\Property(property="hardware_identifier", description="", type="integer"),
 *     @OA\Property(property="mac_address", description="", type="string"),
 *     @OA\Property(property="serial_number", description="", type="string"),
 * )
 */

/**
 * @OA\Schema(schema="stationDetail_base",
 *     @OA\Property(property="id", description="", type="integer"),
 *     @OA\Property(property="id_user", description="", type="integer"),
 *     @OA\Property(property="id_station_pair", description="", type="integer"),
 *     @OA\Property(property="id_master", description="", type="integer"),
 *     @OA\Property(property="pair_position", description="", type="string"),
 *     @OA\Property(property="type", description="", type="string"),
 *     @OA\Property(property="lng", description="", type="number"),
 *     @OA\Property(property="lat", description="", type="number"),
 *     @OA\Property(property="antenna_volume", description="", type="number"),
 *     @OA\Property(property="channel_width", description="", type="number"),
 *     @OA\Property(property="power", description="", type="number"),
 *     @OA\Property(property="registered_at", description="", type="integer"),
 *     @OA\Property(property="valid_to", description="", type="integer"),
 *     @OA\Property(property="protected_to", description="", type="integer"),
 *     @OA\Property(property="status", description="", type="string"),
 *     @OA\Property(property="deleted", description="", type="integer"),
 *     @OA\Property(property="created_at", description="", type="integer"),
 *     @OA\Property(property="updated_at", description="", type="integer"),
 *     @OA\Property(property="name", description="", type="string"),
 *     @OA\Property(property="published_by", description="", type="string"),
 *     @OA\Property(property="mac_address", description="", type="string"),
 *     @OA\Property(property="serial_number", description="", type="string"),
 * )
 */

/**
 * @OA\Schema(schema="stationForm_base",
 *     required={"name", "lng", "lat"},
 *     @OA\Property(property="name", description="", type="string"),
 *     @OA\Property(property="lng", description="", type="number"),
 *     @OA\Property(property="lat", description="", type="number"),
 *     @OA\Property(property="mac_address", description="", type="string"),
 *
 * )
 */
/**
 * @OA\Schema(schema="stationForm_base_forUpdate",
 *     required={},
 *     @OA\Property(property="name", description="", type="string"),
 *     @OA\Property(property="lng", description="", type="number"),
 *     @OA\Property(property="lat", description="", type="number"),
 *     @OA\Property(property="mac_address", description="", type="string"),
 *
 * )
 */

/**
 * @OA\Schema(schema="stationSearchForm",
 *     @OA\Property(property="search", description="Search term (searching in Name or Id)", type="string"),
 *     @OA\Property(property="types", description="Array of types", type="array", @OA\Items(type="string")),
 *     @OA\Property(property="statuses", description="Array of statuses", type="array", @OA\Items(type="string")),
 *           @OA\Property(
 *              title="properties",
 *              property="sort",
 *              type="object",
 *              @OA\Property(
 *                 title="Sort attribute",
 *                 property="field",
 *                 type="string",
 *                 example="status"
 *              ),
 *              @OA\Property(
 *                 title="sort direction",
 *                 property="type",
 *                 type="string",
 *                 enum={"asc", "desc"}
 *              ),
 *           ),
 * )
 */


/**
 * @OA\Response(
 *      response="StationConflictOnUpdateWhenPublished",
 *      description="Station Conflict",
 *      @OA\JsonContent(
 *           @OA\Property(
 *               property="status",
 *               title="Response status code",
 *               type="integer",
 *               example="200"
 *           ),
 *           @OA\Property(
 *               property="warning",
 *               title="Text error",
 *               type="string",
 *               example="Změny byly uloženy stranou jako čekající, protože stanice je již publikována. Stanice má také konflikty, které lze vyřešit možnými řešeními popsanými v této odpovědi."
 *           ),
 *           @OA\Property(property="data", title="Data", type="object",
 *              oneOf = {
 *                  @OA\Schema(ref = "#/components/schemas/stationDetail_fs"),
 *                  @OA\Schema(ref = "#/components/schemas/stationDetail_wigig"),
 *                  @OA\Schema(ref = "#/components/schemas/stationDetail_wifi_5_8"),
 *                  @OA\Schema(ref = "#/components/schemas/stationDetail_wifi_5_2"),
 *              },
 *          ),
 *           @OA\Property(
 *               property="warningData",
 *               title="Warning data",
 *               type="object",
 *               @OA\Property(property="conflictStations", description="", type="array",
 *                  @OA\Items(
 *                      @OA\Property(property="id", description="Conflict station id", type="string"),
 *                      @OA\Property(property="ownerId", description="Station owner id", type="string"),
 *                      @OA\Property(property="ownerName", description="Station owner name", type="string"),
 *                      @OA\Property(property="ownerEmail", description="Station owner email", type="string"),
 *                  ),
 *               ),
 *               @OA\Property(property="solution1_viaParametersChange", description="", type="object",
 *                  @OA\Property(property="how", description="", type="string", example="Upravte parametry stanice tak, aby ke konfliktu již nedocházelo (můžete použít chatovou komunikaci s vlastníkem konfliktní stanice přes Thread endpointy)"),
 *               ),
 *               @OA\Property(property="solution2_viaDeclaration", description="", type="object",
 *                  @OA\Property(property="how", description="", type="string", example="V endpointe /publish odešlete parametr „solveConflictByDeclaration“ nastavený na hodnotu true"),
 *                  @OA\Property(property="whatYouDeclare", description="Declare text", type="string"),
 *               ),
 *           ),
 *      ),
 * ),
 */

/**
 * @OA\Response(
 *      response="StationConflict",
 *      description="Station Conflict",
 *      @OA\JsonContent(
 *           @OA\Property(
 *               property="status",
 *               title="Response status code",
 *               type="integer",
 *               example="409"
 *           ),
 *           @OA\Property(
 *               property="error",
 *               title="Text error",
 *               type="string",
 *               example="Stanici nebude možné publikovat protože má konflikty, které lze vyřešit možnými řešeními popsanými v této odpovědi."
 *           ),
 *           @OA\Property(
 *               property="errorData",
 *               title="Error data",
 *               type="object",
 *               @OA\Property(property="conflictStations", description="", type="array",
 *                  @OA\Items(
 *                      @OA\Property(property="id", description="Conflict station id", type="string"),
 *                      @OA\Property(property="ownerId", description="Station owner id", type="string"),
 *                      @OA\Property(property="ownerName", description="Station owner name", type="string"),
 *                      @OA\Property(property="ownerEmail", description="Station owner email", type="string"),
 *                  ),
 *               ),
 *               @OA\Property(property="solution1_viaParametersChange", description="", type="object",
 *                  @OA\Property(property="how", description="", type="string", example="Upravte parametry stanice tak, aby ke konfliktu již nedocházelo (můžete použít chatovou komunikaci s vlastníkem konfliktní stanice přes Thread endpointy)"),
 *               ),
 *               @OA\Property(property="solution2_viaDeclaration", description="", type="object",
 *                  @OA\Property(property="how", description="", type="string", example="V endpointe /publish odešlete parametr „solveConflictByDeclaration“ nastavený na hodnotu true"),
 *                  @OA\Property(property="whatYouDeclare", description="Declare text", type="string"),
 *               ),
 *           ),
 *      ),
 * ),
 */

class Station extends \common\models\Station
{
    public static function defaultFields()
    {
        return [
            'id',
            'id_master',
            'id_station_pair',
            'id_user',
            'type',
            'name',
            'mac_address',
            'serial_number',
            'lng',
            'lat',
            'antenna_volume',
            'channel_width',
            'status',
            'registered_at',
            'valid_to',
            'protected_to',
            'created_at',
            'updated_at',
            'published_by',
            'waiting_api_changes_json',
        ];
    }

    public function fields()
    {
        $fields = merge(static::defaultFields(), [
            'power',
        ]);

        // Adjust response by station type
        switch (mb_strtolower($this->type)) {
            case StationController::TYPE_FS:
                $fields = [
                    'stationA' => fn() => StationFs::findOne(['id_station' => $this->id]),
                    'stationB' => fn() => StationFs::findOne(['id_station' => $this->id_station_pair]),
                ];
                break;
            case StationController::TYPE_WIGIG:
                ApiHelper::appendStationTypeFields(StationWigig::class, $this->id, $fields);
                break;
            case StationController::TYPE_52:
                ApiHelper::appendStationTypeFields(Station52::class, $this->id, $fields);
                break;
            case StationController::TYPE_58_AP:
            case StationController::TYPE_58_TG:
                ApiHelper::appendStationTypeFields(Station58::class, $this->id, $fields);
                break;
        }

        if (!isset($fields['waiting_api_changes_json']) &&
            (!isset($fields['stationA']) && !isset($fields['stationB']))) {
            $fields['waiting_api_changes_json'] = function ($m)
            {
                return $m->getWaitingApiChangesAsArray();
            };
        }

        return $fields;
    }
}
