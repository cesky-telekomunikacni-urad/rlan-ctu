<?php
/*
* Copyright 2023 DactylGroup s.r.o.
*
* Licensed under the EUPL, Version 1.2 or – as soon they
will be approved by the European Commission - subsequent
versions of the EUPL (the "Licence");
* You may not use this work except in compliance with the
Licence.
* You may obtain a copy of the Licence at:
*
* https://joinup.ec.europa.eu/software/page/eupl5
*
* Unless required by applicable law or agreed to in
writing, software distributed under the Licence is
distributed on an "AS IS" basis,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
express or implied.
* See the Licence for the specific language governing
permissions and limitations under the Licence.
*/
?>


<?php

// Uses

// Variables
/**
 * @var $models array
 */

?>

<?php
foreach ($models as $model) : ?>

<a class="station-popup" href="special_replacable_link_to_station<?= $model['realId'] ?>" target="_blank" data-pjax="0">
  <h6 class="station-popup__name"><?= $model['name'] ?>
      <?php if ($model['position'] != null): ?>
        <i class="station-popup__position">
          <img src="/frontend/web/source_assets/img/icon/ic-position-<?= $model['position'] ?>.svg"/>
        </i>
      <?php endif; ?>
  </h6>
  <span class="station-popup__type"><?= $model['typeName'] ?>&nbsp;&nbsp; • &nbsp;&nbsp;<?= $model['id'] ?></span>
  <span class="station-popup__gps">
    <span class="station-popup__gps__label">GPS: </span>
    <?= round($model['lat'], 8) ?>°, <?= round($model['lng'], 8) ?>°
  </span>
    <?php if ($model['direction'] !== null): ?>
    <span class="station-popup__direction"><span class="station-popup__direction__label"><?= _tF('popup_direction',
                'station') ?>: </span><?= $model['direction'] ?>°
    </span>
    <?php endif; ?>
</a>
<?php endforeach; ?>