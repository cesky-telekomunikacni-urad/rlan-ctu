# Registrace v 60 GHz, 5,8 GHz a 5,2 GHz

Na tomto Portálu registrují uživatelé kmitočtů v pásmu 57–66 GHz, 5,8 GHz a 5,2 GHz pevně instalované venkovní rádiové Stanice (bod-bod, bod-více bodů) a stanice RLAN, a dále Stanice pevných vysokorychlostních spojů typu bod-bod v pásmu 60 GHz. Podmínky využití kmitočtů upravuje všeobecné oprávnění VO-R/12. Registrace stanic je bezplatná

### Autor: DactylGroup s.r.o.
https://www.dactylgroup.com


